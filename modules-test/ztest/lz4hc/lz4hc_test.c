#include <linux/init.h>
#include <linux/module.h>
#include <linux/string.h>
#include <linux/slab.h>
#include <linux/lz4.h>

static int __init test_init(void)
{
	int ret;
	const unsigned char *src = "This is a test module for lz4hc compress! This is a test module for lz4hc compress!";
	size_t slen = strlen(src), tlen, dlen;
	unsigned char *wrkmem = kmalloc(LZ4HC_MEM_COMPRESS, GFP_KERNEL);
	size_t len = LZ4_compressBound(1024);
	unsigned char *tmp = kzalloc(len, GFP_KERNEL);
	unsigned char *dst = kzalloc(len, GFP_KERNEL);

	if (!wrkmem || !tmp || !dst)
		return -ENOMEM;
	
	tlen = len;
	ret = LZ4_compress_HC(src, tmp, slen, tlen, LZ4HC_DEFAULT_CLEVEL, wrkmem);
	if (!ret) {
		pr_err("lz4_compress error %d!\n", ret);
		kfree(dst);
		kfree(tmp);
		kfree(wrkmem);
		return -EINVAL;
	}

	dlen = len;
	tlen = ret;
	ret = LZ4_decompress_safe(tmp, dst, tlen, dlen);
	if (ret < 0) {
		pr_err("lz4_decompress error %d!\n", ret);
		kfree(dst);
		kfree(tmp);
		kfree(wrkmem);
		return -EINVAL;
	}

	pr_info("dst: %s, slen: %d, tlen: %d, dlen: %d\n", dst, (int)slen, (int)tlen, (int)dlen);

	kfree(dst);
	kfree(tmp);
	kfree(wrkmem);

	return 0;
}

static void __exit test_exit(void)
{
}

module_init(test_init);
module_exit(test_exit);
MODULE_LICENSE("GPL");
