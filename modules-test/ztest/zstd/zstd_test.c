#include <linux/init.h>
#include <linux/module.h>
#include <linux/string.h>
#include <linux/slab.h>
#include <linux/zstd.h>

#define ZSTD_DEF_LEVEL 3

static int __init test_init(void)
{
	int ret;
	const unsigned char *src = "This is a test module for lz4 compress! This is a test module for lz4 compress!";
	unsigned len = ZSTD_compressBound(1024);
	unsigned long int slen = strlen(src), tlen = len, dlen = len;
	unsigned char *tmp = kzalloc(len, GFP_KERNEL);
	unsigned char *dst = kzalloc(len, GFP_KERNEL);
	const ZSTD_parameters params = ZSTD_getParams(ZSTD_DEF_LEVEL, 0, 0);
	const size_t wksp_size = ZSTD_CCtxWorkspaceBound(params.cParams);
	const size_t dwksp_size = ZSTD_DCtxWorkspaceBound();
	ZSTD_CCtx *cctx;
	void *cwksp;
	ZSTD_DCtx *dctx;
	void *dwksp;

	cwksp = vzalloc(wksp_size);
	if (!cwksp)
              return -ENOMEM;

      	cctx = ZSTD_initCCtx(cwksp, wksp_size);
      	if (!cctx)
              return -EINVAL;

	dwksp = vzalloc(dwksp_size);
	if (!dwksp)
		return -ENOMEM;

	dctx = ZSTD_initDCtx(dwksp, dwksp_size);
	if (!dctx)
		return -EINVAL;

	if (!cwksp || !dwksp || !tmp || !dst)
		return -ENOMEM;
	
	ret = ZSTD_compressCCtx(cctx, tmp, tlen, src, slen, params);
	if (!ret) {
		pr_err("ZSTD_compressCCtx error %d!\n", ret);
		return -EINVAL;
	}

	tlen = ret;
	ret = ZSTD_decompressDCtx(dctx, dst, dlen, tmp, tlen);
	if (ret < 0) {
		pr_err("ZSTD_decompressDCtx error %d!\n", ret);
		return -EINVAL;
	}

	pr_info("dst: %s, slen: %d, tlen: %d, dlen: %d\n", dst, (int)slen, (int)tlen, (int)dlen);

	kfree(dst);
	kfree(tmp);
	vfree(cwksp);
	vfree(dwksp);

	return 0;
}

static void __exit test_exit(void)
{
}

module_init(test_init);
module_exit(test_exit);
MODULE_LICENSE("GPL");
