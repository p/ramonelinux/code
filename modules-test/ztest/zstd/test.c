#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <zstd.h>

int main()
{
	int ret;
	const unsigned char *src = "This is a test module for lz4 compress! This is a test module for lz4 compress!";
	unsigned len = ZSTD_compressBound(1024);
	unsigned long int slen = strlen(src), tlen = len, dlen = len;
	unsigned char *tmp = malloc(len);
	unsigned char *dst = malloc(len);

	if (!tmp || !dst)
		return -1;
	
	ret = ZSTD_compress(tmp, tlen, src, slen, 1);
	if (!ret) {
		printf("ZSTD_compressCCtx error %d!\n", ret);
		return -1;
	}

	tlen = ret;
	ret = ZSTD_decompress(dst, dlen, tmp, tlen);
	if (ret < 0) {
		printf("ZSTD_decompressDCtx error %d!\n", ret);
		return -1;
	}

	printf("dst: %s, slen: %d, tlen: %d, dlen: %d\n", dst, (int)slen, (int)tlen, (int)dlen);

	free(dst);
	free(tmp);

	return 0;
}
