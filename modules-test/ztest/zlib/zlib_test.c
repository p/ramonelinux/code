#include <linux/init.h>
#include <linux/module.h>
#include <linux/string.h>
#include <linux/slab.h>
#include <linux/zlib.h>

#define COMPR_LEVEL 6
#define WINDOW_BITS 12
#define MEM_LEVEL 4
static struct z_stream_s stream;

static int __init test_init(void)
{
	int ret;
	const unsigned char *src = "This is a test module for zlib compress! This is a test module for zlib compress!";
	size_t slen = strlen(src), tlen = 1024, dlen = 1024, size;
	unsigned char *tmp = kzalloc(tlen, GFP_KERNEL);
	unsigned char *dst = kzalloc(dlen, GFP_KERNEL);

	size = max(zlib_deflate_workspacesize(WINDOW_BITS, MEM_LEVEL),
		   zlib_inflate_workspacesize());
	stream.workspace = kmalloc(size, GFP_KERNEL);

	if (!tmp || !dst || !stream.workspace) {
		return -ENOMEM;
		goto out;
	}

	// compress
	ret = zlib_deflateInit2(&stream, COMPR_LEVEL, Z_DEFLATED, WINDOW_BITS,
	                                        MEM_LEVEL, Z_DEFAULT_STRATEGY);
	if (ret != Z_OK) {
	        pr_info("zlib_deflateInit2 error\n");
		goto out;
	}
	
	stream.next_in = src;
	stream.avail_in = slen;
	stream.total_in = 0;
	stream.next_out = tmp;
	stream.avail_out = tlen;
	stream.total_out = 0;

	ret = zlib_deflate(&stream, Z_FINISH);
	if (ret != Z_STREAM_END) {
		pr_err("zlib_deflate error %d!\n", ret);
		goto out;
	}

	ret = zlib_deflateEnd(&stream);
	if (ret != Z_OK) {
		pr_err("zlib_deflateEnd error %d!\n", ret);
		goto out;
	}

	if (stream.total_out >= stream.total_in) {
		pr_err("out >= in\n");
		goto out;
	}

	// decompress
	ret = zlib_inflateInit2(&stream, WINDOW_BITS);
	if (ret != Z_OK) {
	        pr_err("zlib_inflateInit2 error\n");
		goto out;
	}

	stream.next_in = tmp;
	stream.avail_in = tlen;
	stream.total_in = 0;
	stream.next_out = dst;
	stream.avail_out = dlen;
	stream.total_out = 0;
	
	ret = zlib_inflate(&stream, Z_FINISH);
	if (ret != Z_STREAM_END) {
	        pr_err("zlib_inflate error %d\n", ret);
		goto out;
	}
	
	ret = zlib_inflateEnd(&stream);
	if (ret != Z_OK) {
	        pr_err("zlib_inflateEnd error\n");
		goto out;
	}

	pr_info("dst: %s, slen: %d, tlen: %d, dlen: %d\n", dst, (int)slen, (int)tlen, (int)dlen);

	kfree(dst);
	kfree(tmp);
	kfree(stream.workspace);

out:
	return ret;
}

static void __exit test_exit(void)
{
}

module_init(test_init);
module_exit(test_exit);
MODULE_LICENSE("GPL");
