#include <linux/init.h>
#include <linux/module.h>
#include <linux/string.h>
#include <linux/slab.h>
#include <linux/lz4.h>

static int __init test_init(void)
{
	int ret;
	const unsigned char *src = "This is a test module for lz4 compress! This is a test module for lz4 compress!";
	size_t slen = strlen(src), tlen, dlen;
	unsigned char *wrkmem = kmalloc(LZ4_MEM_COMPRESS, GFP_KERNEL);
	size_t len = lz4_compressbound(1024);
	unsigned char *tmp = kzalloc(len, GFP_KERNEL);
	unsigned char *dst = kzalloc(len, GFP_KERNEL);

	if (!wrkmem || !tmp || !dst)
		return -ENOMEM;
	
	ret = lz4_compress(src, slen, tmp, &tlen, wrkmem);
	if (ret) {
		pr_err("lz4_compress error %d!\n", ret);
		return -EINVAL;
	}

	ret = lz4_decompress_unknownoutputsize(tmp, tlen, dst, &dlen);
	if (ret) {
		pr_err("lz4_decompress_unknownoutputsize error %d!\n", ret);
		return -EINVAL;
	}

	pr_info("dst: %s, slen: %d, tlen: %d, dlen: %d\n", dst, (int)slen, (int)tlen, (int)dlen);

	kfree(dst);
	kfree(tmp);
	kfree(wrkmem);

	return 0;
}

static void __exit test_exit(void)
{
}

module_init(test_init);
module_exit(test_exit);
MODULE_LICENSE("GPL");
