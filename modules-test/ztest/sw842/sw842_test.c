#include <linux/init.h>
#include <linux/module.h>
#include <linux/string.h>
#include <linux/slab.h>
#include <linux/sw842.h>

static int __init test_init(void)
{
	int ret;
	const unsigned char *src = "This is a test module for sw842 compress! This is a test module for sw842 compress!";
	size_t slen = strlen(src);
	unsigned int tlen = 1024, dlen = 1024;
	unsigned char *wrkmem = kmalloc(SW842_MEM_COMPRESS, GFP_KERNEL);
	unsigned char *tmp = kzalloc(1024, GFP_KERNEL);
	unsigned char *dst = kzalloc(1024, GFP_KERNEL);

	if (!wrkmem || !tmp || !dst)
		return -ENOMEM;
	
	ret = sw842_compress(src, slen, tmp, &tlen, wrkmem);
	if (ret) {
		pr_err("sw842_compress error %d!\n", ret);
		return -EINVAL;
	}

	ret = sw842_decompress(tmp, tlen, dst, &dlen);
	if (ret) {
		pr_err("sw842_decompress error %d!\n", ret);
		return -EINVAL;
	}

	pr_info("dst: %s, slen: %d, tlen: %d, dlen: %d\n", dst, (int)slen, (int)tlen, (int)dlen);

	kfree(dst);
	kfree(tmp);
	kfree(wrkmem);

	return 0;
}

static void __exit test_exit(void)
{
}

module_init(test_init);
module_exit(test_exit);
MODULE_LICENSE("GPL");
