#include <linux/init.h>
#include <linux/module.h>
#include <linux/string.h>
#include <linux/slab.h>
#include <linux/lzo.h>

static int __init test_init(void)
{
	int ret;
	const unsigned char *src = "This is a test module for lzo compress! This is a test module for lzo compress!";
	size_t slen = strlen(src), tlen, dlen = slen;
	unsigned char *wrkmem = kmalloc(LZO1X_MEM_COMPRESS, GFP_KERNEL);
	size_t len = lzo1x_worst_compress(1024);
	unsigned char *tmp = kzalloc(len, GFP_KERNEL);
	unsigned char *dst = kzalloc(len, GFP_KERNEL);

	if (!wrkmem || !tmp || !dst)
		return -ENOMEM;
	
	ret = lzo1x_1_compress(src, slen, tmp, &tlen, wrkmem);
	if (ret != LZO_E_OK)
		pr_err("lzo1x_1_compress error %d!\n", ret);

	ret = lzo1x_decompress_safe(tmp, tlen, dst, &dlen);
	if (ret != LZO_E_OK)
		pr_err("lzo1x_decompress_safe error %d!\n", ret);

	pr_info("dst: %s, slen: %d, tlen: %d, dlen: %d\n", dst, (int)slen, (int)tlen, (int)dlen);

	kfree(dst);
	kfree(tmp);
	kfree(wrkmem);

	return 0;
}

static void __exit test_exit(void)
{
}

module_init(test_init);
module_exit(test_exit);
MODULE_LICENSE("GPL");
