#include <linux/init.h>
#include <linux/module.h>
#include <linux/bitops.h>

static void test1(void)
{
	int bit;
	DECLARE_BITMAP(_mask, 64);
	u64 mask = 0x8F;
	size_t size = sizeof(mask) * BITS_PER_BYTE;

	bitmap_from_u64(_mask, mask);

	pr_info("size=%ld\n", size);

	for_each_set_bit(bit, _mask, size) {
		pr_info("1 %d\n", bit);
	}
}

static void test2(void)
{
	int bit;
	size_t size = 32;
	DECLARE_BITMAP(_mask, 32);

	*_mask = 0x8F;

	bit = find_first_bit(_mask, size);
	while (bit < size) {
		pr_info("2 %d\n", bit);
		bit = find_next_bit(_mask, size, bit + 1);
	}
}

static void test3(void)
{
	int bit;
	DECLARE_BITMAP(_mask, 8);
	size_t size = 8;

	*_mask = 0x8F;

	for (bit = find_first_bit(_mask, size);
	     bit < size;
	     bit = find_next_bit(_mask, size, bit + 1)) {
		pr_info("3 %d\n", bit);
	}
}

static int __init test_init(void)
{
	test1();
	test2();
	test3();

	return 0;
}

static void __exit test_exit(void)
{
}

module_init(test_init);
module_exit(test_exit);
MODULE_LICENSE("GPL");
