#include <linux/init.h>
#include <linux/module.h>
#include <linux/string.h>
#include <linux/rslib.h>

/* the Reed Solomon control structure */
static struct rs_control *rs_decoder;

static void encode8(uint8_t *data, size_t len, uint16_t *ecc)
{
	int i;
	/* Parity buffer. Size = number of roots */
	uint16_t par[6];

	/* Initialize the parity buffer */
	memset(par, 0, sizeof(par));

	/* Encode 512 byte in data8. Store parity in buffer par */
	encode_rs8(rs_decoder, data, len, par, 0);

	for (i = 0; i < 6; i++)
		ecc[i] = par[i];
}

static int decode8(void *data, size_t len, uint16_t *ecc)
{
	int i;
	uint16_t par[6];

	for (i = 0; i < 6; i++)
		par[i] = ecc[i];

	/* Decode 512 byte in data8.*/
	return decode_rs8(rs_decoder, data, par, len,
			  NULL, 0, NULL, 0, NULL);
}

static void data_error(uint8_t *data, size_t len)
{
	data[len - 3] = 'e';
	data[len - 2] = 'e';
	data[len - 1] = 'r';
	pr_info("error data: %s\n", data);
}

static int __init test_init(void)
{
	char *data = "hello, world";
	uint16_t par[6];
	int numerr;

	/* Symbolsize is 10 (bits)
	 * Primitive polynomial is x^10+x^3+1
	 * first consecutive root is 0
	 * primitive element to generate roots = 1
	 * generator polynomial degree (number of roots) = 6
	 */
	rs_decoder = init_rs(10, 0x409, 0, 1, 6);

	encode8(data, strlen(data), par);

	data_error(data, strlen(data));

	numerr = decode8(data, strlen(data), par);
	pr_info("right data: %s\n", data);
	pr_info("numerr: %d\n", numerr);

	return 0;
}

static void __exit test_exit(void)
{
	/* Release resources */
	free_rs(rs_decoder);
}

module_init(test_init);
module_exit(test_exit);
MODULE_LICENSE("GPL");
