#include <linux/init.h>
#include <linux/module.h>
#include <linux/pci.h>

static void pci_bus_walk(struct pci_bus *bus)
{
	struct pci_dev *dev;
	struct pci_bus *child;

	list_for_each_entry(dev, &bus->devices, bus_list) {
		pr_info("devfn: %d, vendor: %d, device: %d\n", dev->devfn, dev->vendor, dev->device);
		child = dev->subordinate;
		if (child) {
			pr_info("child->name: %s, child->number: %d\n", child->name, child->number);
			pci_bus_walk(child);
		}
	}
}

static int __init test_init(void)
{
	struct pci_bus *bus;

	list_for_each_entry(bus, &pci_root_buses, node) {
		pr_info("bus->name: %s, bus->number: %d\n", bus->name, bus->number);
		pci_bus_walk(bus);
	}

	return 0;
}

static void __exit test_exit(void)
{
}

module_init(test_init);
module_exit(test_exit);
MODULE_LICENSE("GPL");
