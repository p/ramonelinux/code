#!/usr/bin/python
#
# sudo dnf install bcc-tools
# sudo ./hello_world.py

from bcc import BPF

BPF(text='int kprobe__sys_clone(void *ctx) { bpf_trace_printk("Hello, World!\\n"); return 0; }').trace_print()
