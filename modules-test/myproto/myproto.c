#include <linux/init.h>
#include <linux/module.h>
#include <linux/socket.h>
#include <net/protocol.h>

#define PF_MYPROTO 45

/* Protocol specific socket structure */
struct my_sock {
	struct inet_sock isk;
	/* Add the Protocol implementation specific data members per socket here from here on */
};

struct proto my_proto = {
	.name		= "MYPROTO",
	.owner		= THIS_MODULE,
	.obj_size	= sizeof(struct my_sock),
};

static int my_release(struct socket *sock)
{
	struct sock *sk = sock->sk;

	if (!sk)
		return 0;

	sock_put(sk);
	return 0;
}

static int my_sendmsg(struct socket *sock, struct msghdr *msg, size_t len)
{
	pr_info("%s %ld\n", __func__, len);

	return 0;
}

static int my_recvmsg(struct socket *sock, struct msghdr *msg, size_t len, int flags)
{
	pr_info("%s\n", __func__);

	return 0;
}

static struct proto_ops my_proto_ops = {
	.family		= PF_MYPROTO,
	.owner		= THIS_MODULE,
	.release	= my_release,
	.sendmsg	= my_sendmsg,
	.recvmsg	= my_recvmsg,
};

static int my_create(struct net *net, struct socket *sock, int protocol, int kern)
{
	struct sock *sk;

	sk = sk_alloc(net, PF_MYPROTO, GFP_KERNEL, &my_proto, kern);
	if (!sk) {
		pr_err("failed to allocate socket.\n");
		return -ENOMEM;
	}

	sock_init_data(sock, sk);
	sk->sk_protocol = 0x0;

	sock->ops = &my_proto_ops;
	sock->state = SS_UNCONNECTED;

	/* Do the protocol specific socket object initialization */
	return 0;
};

struct net_proto_family my_net_proto = {
	.family = PF_MYPROTO,
	.create = my_create,
	.owner = THIS_MODULE,
};

static int __init test_init(void)
{
	int rc = proto_register(&my_proto, 0);
	if (rc)
		return rc;

	return sock_register(&my_net_proto);
}

static void __exit test_exit(void)
{
	sock_unregister(PF_MYPROTO);
	proto_unregister(&my_proto);
}

module_init(test_init);
module_exit(test_exit);
MODULE_LICENSE("GPL");
