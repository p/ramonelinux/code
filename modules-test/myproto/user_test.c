#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <linux/if_ether.h>

#define PF_MYPROTO 45

int main(int argc, char *argv[])
{
	int sock, n;
	char buffer[2048];
	char *m = "hello";

	if ((sock = socket(PF_MYPROTO, SOCK_RAW, htons(ETH_P_IP))) < 0) {
		perror("socket");
		exit(1);
	}

	n = sendto(sock, m, strlen(m), 0, NULL, 0);
	printf("%d bytes write\n", n);

	n = recvfrom(sock, buffer, 2048, 0, NULL, NULL);
	printf("%d bytes read\n", n);

	close(sock);

	return 0;
}
