/* Documentation/DocBook/writing_usb_driver.tmpl */
#include <linux/init.h>
#include <linux/module.h>
#include <linux/usb.h>
#include <linux/hid.h>

static int skel_probe(struct usb_interface *interface,
		const struct usb_device_id *id)
{
	return 0;
}

static void skel_disconnect(struct usb_interface *interface)
{
}

/* table of devices that work with this driver */
static struct usb_device_id skel_table [] = {
	//{ USB_INTERFACE_INFO(USB_INTERFACE_CLASS_HID, USB_INTERFACE_SUBCLASS_BOOT,
	//	USB_INTERFACE_PROTOCOL_MOUSE) },
	{ USB_INTERFACE_INFO(USB_INTERFACE_CLASS_HID,
			USB_INTERFACE_SUBCLASS_BOOT,
			USB_INTERFACE_PROTOCOL_KEYBOARD)},
	//{ USB_DEVICE(USB_SKEL_VENDOR_ID, USB_SKEL_PRODUCT_ID) },
	{ }                      /* Terminating entry */
};
MODULE_DEVICE_TABLE(usb, skel_table);

static struct usb_driver skel_driver = {
	.name        = "skeleton",
	.probe       = skel_probe,
	.disconnect  = skel_disconnect,
	//.fops        = &skel_fops,
	//.minor       = USB_SKEL_MINOR_BASE,
	.id_table    = skel_table,
};

static int __init usb_skel_init(void)
{
	int result;

	/* register this driver with the USB subsystem */
	result = usb_register(&skel_driver);
	if (result < 0) {
		pr_err("usb_register failed for the " __FILE__ " driver. "
				"Error number %d.", result);
		return -1;
	}

	return 0;
}

static void __exit usb_skel_exit(void)
{
	/* deregister this driver with the USB subsystem */
	usb_deregister(&skel_driver);
}

module_init(usb_skel_init);
module_exit(usb_skel_exit);

MODULE_AUTHOR("Geliang Tang <tanggeliang@gmail.com>");
MODULE_DESCRIPTION("USB driver skeleton");
MODULE_LICENSE("GPL");
