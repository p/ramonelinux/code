//Taken from https://stackoverflow.com/questions/15215865/netlink-sockets-in-c-using-the-3-x-linux-kernel?lq=1

#include <linux/module.h>
#include <linux/netlink.h>
#include <linux/skbuff.h>
#include <net/sock.h>

#define NETLINK_USER 31

struct sock *nl_sk = NULL;

static void hello_nl_recv_msg(struct sk_buff *skb)
{
	struct nlmsghdr *nlh;
	int pid;
	struct sk_buff *skb_out;
	char *msg = "Hello from kernel";
	int res;

	nlh = (struct nlmsghdr*)skb->data;
	pr_info("Netlink received msg payload: %s\n", (char*)nlmsg_data(nlh));

	pid = nlh->nlmsg_pid; /*pid of sending process */

	skb_out = nlmsg_new(strlen(msg), 0);
	if(!skb_out) {
		pr_err("Failed to allocate new skb\n");
		return;
	}

	nlh = nlmsg_put(skb_out, 0, 0, NLMSG_DONE, strlen(msg), 0);  
	NETLINK_CB(skb_out).dst_group = 0; /* not in mcast group */
	strncpy(nlmsg_data(nlh), msg, strlen(msg));

	res = nlmsg_unicast(nl_sk, skb_out, pid);
	if(res < 0)
		pr_err("Error while sending bak to user\n");
}

static int __init hello_init(void)
{
	struct netlink_kernel_cfg cfg = {
		.input = hello_nl_recv_msg,
	};

	nl_sk = netlink_kernel_create(&init_net, NETLINK_USER, &cfg);
	if(!nl_sk) {
		pr_err("Error creating socket.\n");
		return -10;
	}

	return 0;
}

static void __exit hello_exit(void)
{
	netlink_kernel_release(nl_sk);
}

module_init(hello_init);
module_exit(hello_exit);
MODULE_LICENSE("GPL");
