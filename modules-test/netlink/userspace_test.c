#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stddef.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <linux/netlink.h>
#include "test.h"

int main()
{
	int ret;

	int sockfd = socket(PF_NETLINK, SOCK_RAW, NETLINK_TEST);
	if (sockfd < -1) {
		perror("create socket");
		return -1;
	}

	struct sockaddr_nl saddr;
	bzero(&saddr, sizeof(saddr));
	saddr.nl_family = PF_NETLINK;
	saddr.nl_pid = getpid();
	saddr.nl_groups = 0;

	bind(sockfd, (struct sockaddr *)&saddr, sizeof(saddr));

	char *str = "hello, world!";
	struct my_msg msg;
	bzero(&msg, sizeof(msg));
	msg.len = strlen(str) + offsetof(struct my_msg, data) + 1;
	memcpy(msg.data, str, strlen(str));

	struct nlmsghdr *nlmh = malloc(NLMSG_SPACE(msg.len));
	nlmh->nlmsg_len = NLMSG_SPACE(msg.len);
	nlmh->nlmsg_flags = 0;
	nlmh->nlmsg_pid = getpid();
	memcpy(NLMSG_DATA(nlmh), &msg, msg.len);

	struct iovec iov;
	iov.iov_base = (void *)nlmh;
	iov.iov_len = nlmh->nlmsg_len;

	struct msghdr msgh;
	bzero(&msgh, sizeof(msgh));
	msgh.msg_iov = &iov;
	msgh.msg_iovlen = 1;

	printf("Sending message to kernel\n");
	ret = sendmsg(sockfd, &msgh, 0);
	if (ret < 0) {
		perror("sendmsg");
		return -1;
	}

	printf("receiving message from kernel\n");
	ret = recvmsg(sockfd, &msgh, 0);
	if (ret < 0) {
		perror("recvmsg");
		return -1;
	}

	printf("%s\n", (char *)NLMSG_DATA(nlmh));

	close(sockfd);

	return 0;
}
