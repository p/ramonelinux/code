#include <linux/init.h>
#include <linux/module.h>
#include <linux/netlink.h>
#include <linux/seq_file_net.h>
#include <net/sock.h>
#include "test.h"

static struct sock *nl_sk;

static void test_rcv(struct sk_buff *skb)
{
	struct nlmsghdr *nlmh;
        struct my_msg *msg, *msg_out;
	struct sk_buff *skb_out;
	char *msg_str = "Hello from kernel";
	int pid;
	int ret;

	nlmh = nlmsg_hdr(skb);
	msg = (struct my_msg *)NLMSG_DATA(nlmh);
	pr_info("msg->data: %s, msg->len: %d, skb->len: %d\n",
			msg->data, msg->len, skb->len);

	pid = nlmh->nlmsg_pid;

	msg_out = kmalloc(sizeof(struct my_msg), GFP_KERNEL);
	if (!msg_out)
		return;

	strncpy(msg_out->data, msg_str, strlen(msg_str));
	msg_out->len = strlen(msg_str);

	skb_out = nlmsg_new(msg_out->len, 0);
	if (!skb_out)
		return;

	nlmh = nlmsg_put(skb_out, 0, 0, NLMSG_DONE, msg_out->len, 0);
	NETLINK_CB(skb_out).dst_group = 0;
	strncpy(nlmsg_data(nlmh), msg_out->data, msg_out->len);

	ret = nlmsg_unicast(nl_sk, skb_out, pid);
	if (ret < 0) {
		pr_err("nlmsg_unicast error\n");
	}
}

static int __init test_init(void)
{
	struct netlink_kernel_cfg cfg = {
		.groups = 1,
		.input	= test_rcv,
		.flags	= NL_CFG_F_NONROOT_RECV,
	};

	nl_sk = netlink_kernel_create(&init_net, NETLINK_TEST, &cfg);
	if (!nl_sk) {
		pr_err("netlink create failed\n");
		return -1;
	}

	return 0;
}

static void __exit test_exit(void)
{
	netlink_kernel_release(nl_sk);
}

module_init(test_init);
module_exit(test_exit);
MODULE_LICENSE("GPL");
