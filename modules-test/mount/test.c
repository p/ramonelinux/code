#include <linux/init.h>
#include <linux/module.h>
#include <linux/namei.h>
#include <linux/mount.h>
#include "mount.h"

static char *upath = "/bin/bash";
module_param(upath, charp, S_IRUGO);

static int __init test_init(void)
{
	int err;
	struct path p;
	char buf[1024];
	char *s;
	struct mount *mnt, *tmp;

	err = kern_path(upath, LOOKUP_FOLLOW, &p);
	if (err) {
		pr_err("kern_path failed\n");
		return err;
	}

	s = d_path(&p, buf, 1024);
	mnt = real_mount(p.mnt);

	pr_info("vfsmount->mnt_root->d_iname: %s\n", p.mnt->mnt_root->d_iname);
	pr_info("dentry->d_iname: %s\n", p.dentry->d_iname);
	pr_info("mnt->mnt_mountpoint->d_iname: %s\n", mnt->mnt_mountpoint->d_iname);
	pr_info("mnt->mnt_devname: %s\n", mnt->mnt_devname);

	list_for_each_entry(tmp, &mnt->mnt_mounts, mnt_child) {
		pr_info("%s %s\n", tmp->mnt_mountpoint->d_iname, tmp->mnt_devname);
	}

	return 0;
}

static void __exit test_exit(void)
{
}

module_init(test_init);
module_exit(test_exit);
MODULE_LICENSE("GPL");
