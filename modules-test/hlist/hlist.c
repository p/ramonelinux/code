#include <linux/module.h>
#include <linux/slab.h>
#include <linux/list.h>
#include <linux/sched.h>

#define HSIZE ('z' - 'A' + 1) //58

struct foo {
	struct hlist_node node;
	char *name;
};

static struct hlist_head foo_list[HSIZE];

static unsigned int hash(char *name)
{
	return name[0] - 'A';
}

static void add(void)
{
	struct task_struct *task;

	for_each_process(task) {
		struct foo *f = kmalloc(sizeof(*f), GFP_KERNEL);
		f->name = task->comm;
		hlist_add_head(&f->node, &foo_list[hash(f->name)]);
	}
}

static void search(void)
{
	struct foo *f;
	int i = hash("c");

	hlist_for_each_entry(f, &foo_list[i], node)
		pr_info("%s\n", f->name);
}

static void delete(void)
{
	struct foo *f;
	int i;

	for (i = 0; i < HSIZE; i++) {
		hlist_for_each_entry(f, &foo_list[i], node) {
			pr_info("delete %d %s\n", i, f->name);
			__hlist_del(&f->node);
			kfree(f);
		}
	}
}

static int __init test_init(void)
{
	int i;

	for(i = 0; i < HSIZE; i++)
		INIT_HLIST_HEAD(&foo_list[i]);
	
	add();
	search();

	return 0;
}

static void __exit test_exit(void)
{
	int i;

	delete();

	for(i = 0; i < HSIZE; i++)
		WARN_ON(!hlist_empty(&foo_list[i]));
}

module_init(test_init);
module_exit(test_exit);
MODULE_LICENSE("GPL");
