/*
 * 下面我给出的例子，就是一个用不同list_head和hash list将用户数据struct foo同时串起来的例子。
 * i_list是普通list的串。i_hash是hash list的串
 * （实际是多个串，被HASH分散在数组中,HASH关键字即是数组下标）。
 *
 * 你可以想象数据结构是一个个货场的相同型号的分散放置的集装箱，为了将他们串起来，
 * 必须在箱子上安装上有孔的铁环，然后用铁丝通过这些安装上铁环就可以将他们串起来。
 * 数据结构中的定义struct list_head就相当于铁环，可以焊接到箱子的任何位置。头部中间尾部都可以。
 *
 * 本例子中，一个箱子上安装了2个环，一个是i_list, 将箱子用一个长铁丝全部穿起来的。
 * 还安了一个环i_hash, 这个环的用途是将箱子分组串到不同的串中
 * （每个串使用一个独立的铁丝，当然每根铁丝长度就短了）。
 * 不同串的各个铁丝的头部排列放到一个hlist_head的数组中。

 * 为了查找一个箱子，可以顺着长铁丝找（遍历），一个一个比较，直到找到箱子。但这样慢。
 * 还可以直接顺着某个短铁丝找，如果事先可以确定要找的箱子必定在该端铁丝串中－－这是可以做到的，hash的作用。
 * struct hlist_head仅仅用了一个指针，占4个字节，因为hash数组往往相当大，而每个串长度很短（hash的目的）。
 * 这样可以节省4个字节乘以数据大小的空间。
 */

#include <linux/init.h>
#include <linux/module.h>
#include <linux/slab.h>
#include <linux/list.h>
#include <linux/ctype.h>

#define LISTSIZE 400
#define HSIZE 256

struct foo {
	unsigned char ch;
	struct list_head  i_list;
	struct hlist_node i_hash;
};

static struct list_head head;
static struct hlist_head hlist[HSIZE];

static unsigned int gethash(int c)
{
	return (c & 0xff);
}

static void test(void)
{
	struct hlist_node *hp;
	unsigned int hash;

	//通过hash串查找内容'C'的箱子
	hash = gethash('c');
	hlist_for_each(hp, &hlist[hash]) {
		struct foo *p = hlist_entry(hp, struct foo, i_hash);
		pr_info("hlist: %c\n", p->ch);
	}
}

static int __init test_init(void)
{
	int i;
	unsigned int hash;

	INIT_LIST_HEAD(&head);
	for(hash = 0; hash < HSIZE; hash++)
		INIT_HLIST_HEAD(&hlist[hash]);

	for(i = 0; i < LISTSIZE; i++) {
		struct foo *p = kmalloc(sizeof(*p), GFP_KERNEL);
		if(!p)
			return -ENOMEM;

		p->ch = 'a' + i;
		//串入长串
		list_add(&p->i_list, &head);
		//串入HASH短串
		hash = gethash(p->ch);
		if (isprint(p->ch))
			pr_info("ALLOC %x %d %p %u\n", p->ch, i, p, hash);
		hlist_add_head(&p->i_hash, &hlist[hash]);
	}

	test();

	return 0;
}

static void __exit test_exit(void)
{
	struct list_head *list;
	int i = 0;

	//通过长铁丝遍历
	list_for_each(list, &head) {
		struct foo *p = list_entry(list, struct foo, i_list);
		if (isprint(p->ch))
			pr_info("%p value %d = %c\n", p, i, p->ch);
		i++;
		kfree(p);
	}
	pr_info("total %d\n", i);
}

module_init(test_init);
module_exit(test_exit);

MODULE_LICENSE("GPL");
