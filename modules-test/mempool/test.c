#include <linux/init.h>
#include <linux/module.h>
#include <linux/mempool.h>
#include <linux/slab.h>
#include <linux/highmem.h>

struct test_struct {
	unsigned int test;
};

static void test1(void)
{
	int i;
	struct test_struct *element[10];
	mempool_t *_pool;
	unsigned int pool_size = 32;
	struct kmem_cache *_cache;

	_cache = KMEM_CACHE(test_struct, 0);
	_pool = mempool_create_slab_pool(pool_size, _cache);
	
	for (i = 0; i < 10; i++) {
		element[i] = mempool_alloc(_pool, GFP_KERNEL);
		element[i]->test = i;
	}

	for (i = 0; i < 10; i++) {
		pr_info("%d\n", element[i]->test);
		mempool_free(element[i], _pool);
	}

	mempool_destroy(_pool);
	kmem_cache_destroy(_cache);
}

static void test2(void)
{
	int i;
	struct test_struct *element[10];
	struct kmem_cache *_cache;

	_cache = KMEM_CACHE(test_struct, 0);
	
	for (i = 0; i < 10; i++) {
		element[i] = mempool_alloc_slab(GFP_KERNEL, _cache);
		element[i]->test = i * 10;
	}

	for (i = 0; i < 10; i++) {
		pr_info("%d\n", element[i]->test);
		mempool_free_slab(element[i], _cache);
	}

	kmem_cache_destroy(_cache);
}

static void test3(void)
{
	int i;
	struct test_struct *element[10];
	mempool_t *_pool;
	unsigned int pool_size = 32;

	_pool = mempool_create_kmalloc_pool(pool_size, sizeof(struct test_struct));

	for (i = 0; i < 10; i++) {
		element[i] = mempool_alloc(_pool, GFP_KERNEL);
		element[i]->test = i * 100;
	}

	for (i = 0; i < 10; i++) {
		pr_info("%d\n", element[i]->test);
		mempool_free(element[i], _pool);
	}

	mempool_destroy(_pool);
}

static void test4(void)
{
	int i;
	struct page *page[10];
	struct test_struct *element[10];
	mempool_t *_pool;
	unsigned int pool_size = 32;

	_pool = mempool_create_page_pool(pool_size, 1);

	for (i = 0; i < 10; i++) {
		page[i] = mempool_alloc(_pool, GFP_KERNEL);
		element[i] = kmap_atomic(page[i]);
		element[i]->test = i * 1000;
	}

	for (i = 0; i < 10; i++) {
		pr_info("%d\n", element[i]->test);
		mempool_free(element[i], _pool);
		kunmap_atomic(element[i]);
	}

	mempool_destroy(_pool);
}

static int __init test_init(void)
{
	test1();
	test2();
	test3();
	test4();
	return 0;
}

static void __exit test_exit(void)
{
}

module_init(test_init);
module_exit(test_exit);
MODULE_LICENSE("GPL");
