#include <linux/kernel.h>
#include <linux/module.h>

#ifdef CONFIG_KEYS
#include <linux/key-type.h>
#include <linux/fs.h>
#include <linux/seq_file.h>
#include <linux/proc_fs.h>
#include <linux/sched.h>
#include <linux/sched/signal.h>
#endif

#include "../common.h"

#define MY_PROC_DIR  "learning-key"
#define PROC_ENTRY_NAME "gateway"
#define DRIVER_AUTHOR "Avinesh Kumar <avinesh.kumar@in.ibm.com>"
#define DRIVER_DESC "Getting started with keys"
#define LICENSE "http://www.openafs.org/dl/license10.html"

#ifdef CONFIG_KEYS
struct proc_dir_entry *myroot, *myentry;
struct key_type *key_type_keyring_orig;

/* This function creates a new keyring and overrides the session keyring
 * of calling task.
 */
static int
install_session_keyring(struct task_struct *task, struct key *keyring)
{
	char desc[20];
	int retval;
	struct key *orig;

	sprintf(desc, "session.%u", task->tgid);
	/* Get the key tpye of original keyring of calling task */
	key_type_keyring_orig = task->cred->user->session_keyring->type;

	/* Allocate a key for new session-keyring */
	keyring = key_alloc(key_type_keyring_orig,	/* Key type*/
			    desc,			/* Key's description */
			    task->cred->uid,		/* UID */
			    task->cred->gid,		/* GID */
			    task->cred,
			    KEY_POS_ALL | KEY_USR_ALL,	/* perms */
			    KEY_ALLOC_IN_QUOTA,		/* Flags */
			    NULL
			    );

	if(IS_ERR(keyring)) {
		retval = PTR_ERR(keyring);
		goto out;
	}
	pr_info("keyring allocated successfully.\n");
	retval = key_instantiate_and_link(keyring,	/* key */
					  NULL,		/* data */
					  0,		/* data length */
					  NULL,		/* target key ring */
					  NULL		/* Authorisation Key */
					  );
	if(retval < 0 ){
		key_put(keyring);
		goto out;
	}
	pr_info("keyring instantiated and linked successfully.\n");

	/* install the keyring */
	spin_lock_irq(&task->sighand->siglock);
	orig = task->cred->user->session_keyring;
	task->cred->user->session_keyring = keyring;

	/* On SMP systems, we need this for (write) memory barrier protection */
	smp_wmb();

	spin_unlock_irq(&task->sighand->siglock);

	if(orig){
		key_put(orig);
	}
out:
	return(retval);
}

static int instantiate_key(struct key *key, struct key_preparsed_payload *prep)
{
	return(SUCCESS);
}

static void key_desc(const struct key *key, struct seq_file *p)
{
	seq_puts(p, key->description);
	seq_printf(p, ": %u", key->datalen);
}

static void key_destroy(struct key *key)
{
	key_put(key);
}

static bool match(const struct key *key, const struct key_match_data *match_data)
{
	return strcmp(key->description, match_data->raw_data) == 0;
}

static int key_match_preparse(struct key_match_data *match_data)
{
	match_data->cmp = match;
	return 0;
}

static struct key_type new_key_type = {
	.name = "mykey",
	.instantiate = instantiate_key,
	.describe = key_desc,
	.destroy = key_destroy,
	.match_preparse = key_match_preparse,
};

void fresh_session(void)
{
	key_perm_t perms;
	struct key *key;

	pr_info("Installing session keyring:\n");
	install_session_keyring(current, NULL);
	pr_info("New session keyring installed successfully.\n");

	perms = KEY_POS_ALL | KEY_USR_ALL;

	key = key_alloc(&new_key_type,
			"New key type",
			current->cred->uid,
			current->cred->gid,
			current->cred,
			perms,
			KEY_ALLOC_IN_QUOTA,
			NULL
		       );
	pr_info("key of new type allocated successfully.\n");
	if(!IS_ERR(key)){
		key_instantiate_and_link(key,
					 NULL,
					 sizeof(int),
					 current->cred->user->session_keyring,
					 NULL
					 );
		pr_info("New key type linked to current session.\n");
	}

}

static long proc_ioctl(struct file *file, unsigned int cmd, unsigned long arg)
{
	switch(cmd) {
	case FRESH_SESSION:
		fresh_session();
		break;

	default:
		pr_err("Bad Argument.\n");
	}

	return(SUCCESS);
}

static struct file_operations gateway_fops = {
	.unlocked_ioctl = proc_ioctl,
};
#endif /*ifdef CONFIG_KEYS*/

static int __init test_init(void)
{
	pr_info("Loading the module ...\n");

#ifdef CONFIG_KEYS
	myroot = proc_mkdir(MY_PROC_DIR, NULL);
	if(!myroot) {
		pr_err("Proc-dir creation failed.\n");
		goto err;
	}

	myentry = proc_create(PROC_ENTRY_NAME, 0666, myroot, &gateway_fops);
	if(!myentry) {
		pr_err("Proc-entry creation failed.\n");
		goto err;
	}

	register_key_type(&new_key_type);
	pr_info("Registered \"learning_key\"\n");
#else
	pr_info("Key Retention Service is not available.\n");
	pr_info("Get latest kernel from http://kernel.org\n");
	pr_info("Enable this feature at kernel configuration time.\n");
#endif
	return(SUCCESS);
err:
	return(FAILURE);
}

static void __exit test_exit(void)
{
	pr_info("Unloading the module.\n");

#ifdef CONFIG_KEYS
	remove_proc_entry(PROC_ENTRY_NAME, myroot);
	remove_proc_entry(MY_PROC_DIR, NULL);

	unregister_key_type(&new_key_type);
	pr_info("Unregistered \"learning_key\"\n");
#endif
}

module_init(test_init);
module_exit(test_exit);

MODULE_LICENSE(LICENSE);
MODULE_AUTHOR(DRIVER_AUTHOR);
MODULE_DESCRIPTION(DRIVER_DESC);
