#include <stdio.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <stdlib.h>
#include <unistd.h>

#include "../common.h"

int main(int argc, char *argv[])
{
	int retval;
	char *myshell = "/bin/bash";
	int fd = open(MY_PROC_INTERFACE, O_RDWR);
	if(fd < 0) {
		perror("proc-file failed to open");
		exit(1);
	}

	retval = ioctl(fd, FRESH_SESSION);
	if(retval == -1) {
		perror("ioctl failed");
		close(fd);
		exit(1);
	}
	close(fd);

	argv[0] = myshell;
	execvp(myshell, argv);

	perror(myshell);
	printf("You don't have bash ???\n");
	exit(1);
}
