#include <linux/init.h>
#include <linux/module.h>
#include <linux/slab.h>

static void test(const char *fmt)
{
	unsigned long l = 0;
	char *p;
	int ret;
	int i;

	switch (fmt[3]) {
	case '0' ... '9':
		pr_info("fmt[3] = %c %d\n", fmt[3], fmt[3]);
		p = (char *)&fmt[3];
		l = simple_strtol(p, &p, 0);
		break;
	case 'a' ... 'f':
		pr_info("fmt[3] = %c %d\n", fmt[3], fmt[3]);
		l = fmt[3] - 87;
		break;
	default:
		break;
	}

	pr_info("fmt=%c l=%ld\n", fmt[3], l);
	for (i = 0; i < 4; i++) {
		ret = test_bit(i, &l);
		pr_info("i=%d ret=%d\n", i, ret);
		//if (ret)
	}
}

static int __init test_init(void)
{
	test("I4o8"); // 1000
	test("I4o9"); // 1001
	test("I4oa"); // 1010

	return 0;
}

static void __exit test_exit(void)
{
}

module_init(test_init);
module_exit(test_exit);
MODULE_LICENSE("GPL");
