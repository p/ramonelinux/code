#include <linux/init.h>
#include <linux/module.h>
#include <linux/slab.h>

static void ip_tokens(int addr, char *ret)
{
	char *token, *tokens[4];
	char *string;
	int i = 0;

	string = kasprintf(GFP_KERNEL, "%pI4", &addr);

	pr_info("str=%s\n", string);

	while ((token = strsep(&string, ".")) != NULL)
		tokens[i++] = token;

	sprintf(ret, "%s.*.*.%s", tokens[0], tokens[3]);

	kfree(string);
}

static int __init test_init(void)
{
	int addr = 856222986;
	char s_addr[128];

	ip_tokens(addr, s_addr);

	pr_info("%s\n", s_addr);

	return 0;
}

static void __exit test_exit(void)
{
}

module_init(test_init);
module_exit(test_exit);
MODULE_LICENSE("GPL");
