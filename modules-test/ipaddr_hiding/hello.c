#include <linux/init.h>
#include <linux/module.h>

#define DEFAULT_CTL	15 // 1111 -> 1.2.3.4

static void test(const char *fmt)
{
	unsigned long flags;
	int ret;
	int i;

	pr_info("fmt[3] = %c %d\n", fmt[3], fmt[3]);
	switch (fmt[3]) {
	case '0' ... '9':
		flags = fmt[3] - '0';
		break;
	case 'A' ... 'F':
		flags = fmt[3] - 'A' + 10;
		break;
	case 'a' ... 'f':
		flags = fmt[3] - 'a' + 10;
		break;
	default:
		flags = DEFAULT_CTL; // 0xF or 0xf
		break;
	}

	pr_info("fmt=%c flags=%ld\n", fmt[3], flags);
	for (i = 0; i < 4; i++) {
		ret = test_bit(i, &flags);
		pr_info("i=%d ret=%d\n", i, ret);
		//if (ret)
	}
}

static int __init test_init(void)
{
	test("I4o0"); // 0000
	test("I4o1"); // 0001
	test("I4o2"); // 0010
	test("I4o3"); // 0011
	test("I4o4"); // 0100
	test("I4o5"); // 0101
	test("I4o6"); // 0110
	test("I4o7"); // 0111
	test("I4o8"); // 1000
	test("I4o9"); // 1001
	
	test("I4oA"); // 1010
	test("I4oB"); // 1011
	test("I4oC"); // 1100
	test("I4oD"); // 1101
	test("I4oE"); // 1110
	test("I4oF"); // 1111

	test("I4oa"); // 1010
	test("I4ob"); // 1011
	test("I4oc"); // 1100
	test("I4od"); // 1101
	test("I4oe"); // 1110
	test("I4of"); // 1111

	return 0;
}

static void __exit test_exit(void)
{
}

module_init(test_init);
module_exit(test_exit);
MODULE_LICENSE("GPL");
