#include <linux/init.h>
#include <linux/module.h>

#define PREFIX "TEST: "

static int __init test_init(void)
{
	WARN(1, "hello %s\n", __func__);

	WARN("world %s\n", __func__); // error, only print "test_init", no "world"

	WARN(1, KERN_ERR "hello1 %s\n", __func__);
	WARN(1, KERN_INFO "hello2 %s\n", __func__);

	WARN(1, "hello3 %s", __func__); // no end of '\n' is all right.

	WARN(1, "hello4\n");
	
	WARN(1, PREFIX "hello5 %s\n", __func__);

	return 0; 
}

static void __exit test_exit(void) 
{
} 

module_init(test_init); 
module_exit(test_exit);
MODULE_LICENSE("GPL"); 
