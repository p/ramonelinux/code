#include <linux/init.h>
#include <linux/module.h>
#include <linux/namei.h>
#include <linux/mount.h>
#include <linux/kthread.h>
#include <linux/fs.h>
#include <linux/blkdev.h>
#include <linux/delay.h>

static struct task_struct *rqtest;

static int rqtest_thread(void *data)
{
	struct request_queue *q = bdev_get_queue(data);
	struct request *rq;
	struct req_iterator iter;
	struct bio_vec bvec;

	while (!kthread_should_stop()) {
		if (q) {
			spin_lock_irq(q->queue_lock);
			rq = blk_peek_request(q);
			if (rq) {
				pr_info("request sector: %ld nr_sorted: %u\n",
					(unsigned long)rq->__sector, q->nr_sorted);
				rq_for_each_segment(bvec, rq, iter)
					pr_info("bv_len: %u\n", bvec.bv_len);
			}
			spin_unlock_irq(q->queue_lock);
		}
		msleep_interruptible(100);
	}

	return 0;
}

static int __init rqtest_init(void)
{
	int err;
	struct path p;
	struct block_device *bdev;

	err = kern_path("/", LOOKUP_FOLLOW, &p);
	if (err) {
		pr_err("kern_path failed\n");
		return err;
	}
	
	bdev = p.mnt->mnt_sb->s_bdev;
	pr_info("bd_dev: %x\n", bdev->bd_dev);

	rqtest = kthread_run(rqtest_thread, bdev, "rqtest");
	if (IS_ERR(rqtest)) {
		pr_err("create kthread failed!\n");
		return PTR_ERR(rqtest);
	}

	return 0;
}

static void __exit rqtest_exit(void)
{
	kthread_stop(rqtest);
}

module_init(rqtest_init);
module_exit(rqtest_exit);
MODULE_LICENSE("GPL");
