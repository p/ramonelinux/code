#include <linux/init.h>
#include <linux/module.h>
#include <linux/gfp.h>

/* Convert GFP flags to their corresponding migrate type */
static int my_allocflags_to_migratetype(gfp_t gfp_flags)
{    
    /* Group based on mobility */
    return (((gfp_flags & __GFP_MOVABLE) != 0) << 1) |
        ((gfp_flags & __GFP_RECLAIMABLE) != 0);
}

static int __init test_init(void)
{
    int migratetype0 = my_allocflags_to_migratetype(GFP_KERNEL);
    int migratetype1 = my_allocflags_to_migratetype(GFP_KERNEL | __GFP_RECLAIMABLE);
    int migratetype2 = my_allocflags_to_migratetype(GFP_KERNEL | __GFP_MOVABLE);
    int migratetype3 = my_allocflags_to_migratetype(GFP_KERNEL | __GFP_MOVABLE | __GFP_RECLAIMABLE);

    enum zone_type high_zoneidx0 = gfp_zone(__GFP_DMA);
    enum zone_type high_zoneidx1 = gfp_zone(__GFP_HIGHMEM);
    enum zone_type high_zoneidx2 = gfp_zone(__GFP_DMA32);
    enum zone_type high_zoneidx3 = gfp_zone(__GFP_HIGH);

    printk(KERN_ALERT "migratetype0: %x %d\n", migratetype0, migratetype0);
    printk(KERN_ALERT "migratetype1: %x %d\n", migratetype1, migratetype1);
    printk(KERN_ALERT "migratetype2: %x %d\n", migratetype2, migratetype2);
    printk(KERN_ALERT "migratetype3: %x %d\n", migratetype3, migratetype3);
    printk(KERN_ALERT "high_zoneidx0: %x %d\n", high_zoneidx0, high_zoneidx0);
    printk(KERN_ALERT "high_zoneidx1: %x %d\n", high_zoneidx1, high_zoneidx1);
    printk(KERN_ALERT "high_zoneidx2: %x %d\n", high_zoneidx2, high_zoneidx2);
    printk(KERN_ALERT "high_zoneidx3: %x %d\n", high_zoneidx3, high_zoneidx3);

    return 0;
}

static void __exit test_exit(void)
{
}

module_init(test_init);
module_exit(test_exit);

MODULE_LICENSE("GPL");
