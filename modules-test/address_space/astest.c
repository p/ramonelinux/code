#include <linux/init.h>
#include <linux/module.h>
#include <linux/namei.h>
#include <linux/fs.h>
#include <linux/mm.h>

static char *upath = "/bin/bash";
module_param(upath, charp, S_IRUGO);

static int __init astest_init(void)
{
	int err;
	struct path p;
	struct address_space *as;
	struct page *pg;

	if (!upath) {
		pr_err("Usage: insmod astest.ko upath=file\n");
		return -EINVAL;
	}

	err = kern_path(upath, LOOKUP_FOLLOW, &p);
	if (err) {
		pr_err("kern_path failed\n");
		return err;
	}
	
	as = p.dentry->d_inode->i_mapping;
	pg = (struct page *)radix_tree_lookup(&as->page_tree, 0);
	if (pg)
		pr_info("The first page of %s:\n%s\n", upath, (char *)page_address(pg));
	else
		pr_info("The first page of %s is not in memory yet!\n", upath);

	return 0;
}

static void __exit astest_exit(void)
{
}

module_init(astest_init);
module_exit(astest_exit);
MODULE_LICENSE("GPL");
