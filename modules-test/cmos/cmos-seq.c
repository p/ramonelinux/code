#include <linux/module.h>	/* for init_module() */
#include <linux/proc_fs.h>	/* for create_proc_info_entry() */
#include <linux/io.h>		/* for inb(), outb() */
#include <linux/seq_file.h>

#define MAX_SIZE 1
char modname[] = "cmos";
unsigned char cmos[10];
char *day[] = { "", "MON", "TUE", "WED", "THU", "FRI", "SAT", "SUN" };
char *month[] = { "", "JAN", "FEB", "MAR", "APR", "MAY", "JUN",
		"JUL", "AUG", "SEP", "OCT", "NOV", "DEC" };

static int show_cmos(struct seq_file *buf, void *v)
{
	int i, len = 0;
	int month_index;

	/* input and store the first ten CMOS entries */
	for (i = 0; i < 10; i++) {
		outb(i, 0x70);
		cmos[i] = inb(0x71);
	}

	/* show the current time and date */
	len += seq_printf(buf + len, "\n\t CMOS Real-Time Clock/Calendar:");
	len += seq_printf(buf + len, " %02X", cmos[4]);	/* current hour */
	len += seq_printf(buf + len, ":%02X", cmos[2]);	/* current minutes */
	len += seq_printf(buf + len, ":%02X", cmos[0]);	/* current seconds */
	len += seq_printf(buf + len, " on");
	len += seq_printf(buf + len, " %s, ", day[cmos[6]]);	/* day-name */
	len += seq_printf(buf + len, "%02X", cmos[7]);		/* day-number */
	/* BUG! len += sprintf(buf + len, " %s", month[cmos[8]]); // month-name */
	/* bug fix: convert 'cmos[8]' from BCD-format to integer-format */
	month_index = ((cmos[8] & 0xF0) >> 4) * 10 + (cmos[8] & 0x0F);
	len += seq_printf(buf + len, " %s", month[month_index]);	/* month-name */
	len += seq_printf(buf + len, " 20%02X ", cmos[9]);		/* year-number */
	len += seq_printf(buf + len, "\n");
	len += seq_printf(buf + len, "\n");

	return 0;
}

static void *t_start(struct seq_file *m, loff_t *pos)
{
	return (*pos < MAX_SIZE) ? pos : NULL;
}

static void *t_next(struct seq_file *m, void *v, loff_t *pos)
{
	(*pos)++;
	return t_start(m, pos);
}

static void t_stop(struct seq_file *m, void *v)
{
}

const struct seq_operations cmos_op = {
	.start  = t_start,
	.next   = t_next,
	.stop   = t_stop,
	.show   = show_cmos,
};

static int cmos_open(struct inode *inode, struct file *file)
{
	return seq_open(file, &cmos_op);
}

const struct file_operations cmos_fop = { 
	.open       = cmos_open,
	.read       = seq_read,
	.llseek     = seq_lseek,
	.release    = seq_release,
};

static int __init my_init(void)
{
	pr_info("Installing '%s' module\n", modname);
	proc_create(modname, 0, NULL, &cmos_fop);
	return 0;
}

static void __exit my_exit(void)
{
	remove_proc_entry(modname, NULL);
	pr_info("Removing '%s' module\n", modname);
}

module_init(my_init);
module_exit(my_exit);
MODULE_LICENSE("GPL");
