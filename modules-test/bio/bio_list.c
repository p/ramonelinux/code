#include <linux/init.h>
#include <linux/module.h>
#include <linux/bio.h>
#include <linux/kthread.h>
#include <linux/delay.h>
/* struct bio defined in linux/blk_types.h */
#include <linux/genhd.h>

static struct task_struct *biotest;

static int biotest_thread(void *data)
{
	struct bio *bio;
	struct task_struct *tsk;

	while (!kthread_should_stop()) {
		for_each_process(tsk) {
			if (tsk->bio_list) {
				if (!bio_list_empty(tsk->bio_list)) {
					bio = bio_list_peek(tsk->bio_list);
					if (bio)
						pr_info("%s tsk->comm: %s, disk_name: %s\n",
							__func__, tsk->comm, bio->bi_bdev->bd_disk->disk_name);
				} else {
						pr_info("%s tsk->comm: %s, bio list is empty!\n",
							__func__, tsk->comm);
				}
			} else {
				pr_info("%s tsk->comm: %s, bio list is NULL!\n",
					__func__, tsk->comm);
			}
		}
		msleep_interruptible(100);
	}

	return 0;
}

static int __init biotest_init(void)
{
	biotest = kthread_run(biotest_thread, NULL, "biotest");
	if (IS_ERR(biotest)) {
		pr_err("create kthread failed!\n");
		return PTR_ERR(biotest);
	}

	return 0;
}

static void __exit biotest_exit(void)
{
	kthread_stop(biotest);
}

module_init(biotest_init);
module_exit(biotest_exit);
MODULE_LICENSE("GPL");
