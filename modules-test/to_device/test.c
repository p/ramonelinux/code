#include <linux/init.h>
#include <linux/module.h>
#include <linux/sysfs.h>
#include <linux/slab.h>
#include <linux/device.h>

struct test_device {
	unsigned int test;
	struct device dev;
};
static struct test_device *tdev;

static inline struct test_device *to_test_device(struct device *dev)
{
	return dev ? container_of(dev, struct test_device, dev) : NULL;
}

static ssize_t foo_show(struct kobject *kobj, struct kobj_attribute *attr,
		char *buf)
{
	struct test_device *t;

	t = to_test_device(kobj_to_dev(kobj));
	return sprintf(buf, "%d\n", t->test);
}

static ssize_t foo_store(struct kobject *kobj, struct kobj_attribute *attr,
		const char *buf, size_t count)
{
	struct test_device *t;

	t = container_of(kobj, struct test_device, dev.kobj);
	sscanf(buf, "%du", &t->test);
	return count;
}

static struct kobj_attribute dev_attr_foo =
__ATTR(foo, 0664, foo_show, foo_store);

static struct attribute *foo_attrs[] = {
	&dev_attr_foo.attr,
	NULL,
};

static struct attribute_group foo_attr_group = {
	.attrs = foo_attrs,
};

static void test_release(struct device *dev)
{
}

static int __init test_init(void)
{
	int ret;
	char *name = "test";

	tdev = kzalloc(sizeof(struct test_device), GFP_KERNEL);
	if (!tdev) 
		return -ENOMEM;

	dev_set_name(&tdev->dev, "%s", name);
	tdev->dev.release = test_release;
	tdev->test = 100;

	ret = device_register(&tdev->dev);
	if (ret)
		return ret;

	ret = sysfs_create_group(&tdev->dev.kobj, &foo_attr_group);
	if (ret)
		return ret;

	return 0;
}

static void __exit test_exit(void)
{
	device_unregister(&tdev->dev);
}

module_init(test_init);
module_exit(test_exit);

MODULE_LICENSE("GPL");
