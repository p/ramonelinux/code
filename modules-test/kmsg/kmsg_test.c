#include <linux/init.h>
#include <linux/module.h>
#include <linux/kmsg_dump.h>

static void test_dump(struct kmsg_dumper *dumper,
		      enum kmsg_dump_reason reason)
{
	char out[1024];
	size_t size = 1024, len;
	int rc;

	pr_info("test_dump!\n");

	rc = kmsg_dump_get_buffer(dumper, true, out, size, &len);
	if (rc)
		pr_err("kmsg_dump_get_buffer error!\n");
	else
		pr_info("len: %ld\n", len);
}

static struct kmsg_dumper test_dumper = {
	.dump = test_dump,
};

static int __init test_init(void)
{
	pr_info("registe kmsg dump\n");
	kmsg_dump_register(&test_dumper);

	return 0;
}

static void __exit test_exit(void)
{
	pr_info("unregiste kmsg dump\n");
	kmsg_dump_unregister(&test_dumper);
}

module_init(test_init);
module_exit(test_exit);
MODULE_LICENSE("GPL");
