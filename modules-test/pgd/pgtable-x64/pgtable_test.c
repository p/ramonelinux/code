#include <linux/module.h>
#include <linux/sched.h>
#include <linux/highmem.h>
#include <linux/pfn.h>
#include <asm/pgtable_types.h>

/* 4level 9 + 9 + 9 + 9 + 12 = 48 */
static int __init pgtable_test_init(void)
{
	unsigned long addr = current->mm->mmap->vm_start;
	struct page *page = NULL;
	pgd_t *pgd = pgd_offset(current->mm, addr);
	unsigned long pfn;

	pr_info("addr: %lx pgd: %lx pud: %lx pmd: %lx pte: %lx\n", addr, pgd_index(addr), pud_index(addr), pmd_index(addr), pte_index(addr));
	pr_info("pgd: %lx\n", pgd->pgd);
	if (!pgd_none(*pgd)) {
		pud_t *pud = pud_offset(pgd, addr);
		pr_info("pud: %lx\n", pud->pud);
		if (!pud_none(*pud)) {
			pmd_t *pmd = pmd_offset(pud, addr);
			pr_info("pmd: %lx\n", pmd->pmd);
			if (!pmd_none(*pmd)) {
				pte_t *pte = pte_offset_map(pmd, addr);
				pr_info("pte: %lx\n", pte->pte);
				if (pte_present(*pte)) {
					page = pte_page(*pte);
					pfn = page_to_pfn(page);
					pr_info("pfn: %lx\n", pfn);
				}
				pte_unmap(pte);
			}
		}
	}

	return 0;
}

static void __exit pgtable_test_exit(void)
{
}

module_init(pgtable_test_init);
module_exit(pgtable_test_exit);
