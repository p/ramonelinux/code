#include <linux/init.h>
#include <linux/module.h>
#include <linux/highmem.h>
#include <linux/gfp.h>
#include <linux/kallsyms.h>

static int __init test_init(void)
{
	int i;
	pgd_t *pgdp;

	pgd_t *pgd_base_kern = (pgd_t *)kallsyms_lookup_name("swapper_pg_dir");
	pgd_t *pgd_base_user = current->mm->pgd;
	if (!pgd_base_kern) {
		pr_err("set CONFIG_KALLSYMS_ALL=y\n");
		return -1;
	}

	for (i = 0x0; i < 0x400; i++) {
		pgdp = pgd_base_kern + i;
		pr_info("pgd_kern[%x]=%lx\n", i, pgdp->pgd);
		pgdp = pgd_base_user + i;
		pr_info("pgd_user[%x]=%lx\n", i, pgdp->pgd);
	}
/*
 * 结论：
 * 1 从300-3ff内核页表和进程页表完全一样；
 * 2 从0-2ff内存页表全为空；
 * 3 进程页表在0-2ff中零星的有值。
 */

	return 0; 
}

static void __exit test_exit(void) 
{
} 

module_init(test_init); 
module_exit(test_exit);
MODULE_LICENSE("GPL"); 
