#include <linux/module.h>
#include <linux/sched.h>
#include <linux/highmem.h>
#include <linux/pfn.h>
#include <asm/pgtable_types.h>
#include <linux/vmalloc.h>

static struct task_struct *pid_to_task(pid_t pid)
{
	struct task_struct *tsk;
	for_each_process(tsk) {
		if (tsk->pid == pid)
			return tsk;
	}
	return 0;
}

static int __init pgtable_test_init(void)
{
	pgd_t *pgd;
	pud_t *pud;
	pmd_t *pmd;
	pte_t *pte;
	struct task_struct *task = pid_to_task(2772);
	unsigned long va = 0x1334010;//task->mm->mmap->vm_start;

	pgd = pgd_offset(task->mm, va);
	if (pgd_none(*pgd)) {
		pr_err("pgd_none\n");
		return -1;
	}
	pud = pud_offset(pgd, va);
	if (pud_none(*pud)) {
		pr_err("pud_none\n");
		return -1;
	}
	pmd = pmd_offset(pud, va);
	if (pmd_none(*pmd)) {
		pr_err("pmd_none\n");
		return -1;
	}
	pte = pte_offset_kernel(pmd, va);
	if (pte_none(*pte)) {
		pr_err("pte_none\n");
		return -1;
	}
	if (!pte_present(*pte)) {
		pr_err("pte_present\n");
		return -1;
	}
	pr_info("va: %lx pgd: %lx pud: %lx pmd: %lx pte: %lx\n", va, pgd_index(va), pud_index(va), pmd_index(va), pte_index(va));
	pr_info("va: %lx, pgd: %lx, pud: %lx, pmd: %lx, pte: %lx\n", va, pgd->pgd, pud->pud, pmd->pmd, pte->pte);

	return 0;
}

static void __exit pgtable_test_exit(void)
{
}

module_init(pgtable_test_init);
module_exit(pgtable_test_exit);
