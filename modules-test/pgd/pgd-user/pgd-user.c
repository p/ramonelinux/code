#include <linux/module.h>
#include <linux/sched.h>
#include <linux/highmem.h>
#include <linux/pfn.h>
#include <asm/pgtable_types.h>

/* 2level 10 + 10 + 12 = 32 */
static int __init pgtable_test_init(void)
{
	int i;
	unsigned long addr = current->mm->mmap->vm_start;
	struct page *page = NULL;
	unsigned long pfn;
	pgd_t *pgd = pgd_offset(current->mm, addr);
	pgd_t *pgd_base = current->mm->pgd, *pgdp;
	pmd_t *pmd = (pmd_t *)pgd;
	pte_t *pte_base = (pte_t *)pmd_page_vaddr(*pmd), *ptep;

	pr_info("va: %lx pgd_idx: %lx pte_idx: %lx\n", addr, pgd_index(addr), pte_index(addr));
	pr_info("pgd: %lx mm->pgd: %lx\n", pgd->pgd, current->mm->pgd->pgd);
	for (i = 0x0; i < 0x400; i++) {
		pgdp = pgd_base + i;
		pr_info("pgd[%x]=%lx ", i, pgdp->pgd);
	}
	for (i = 0x0; i < 0x400; i++) {
		ptep = pte_base + i;
		pr_info("pte[%x]=%lx ", i, ptep->pte);
	}

	if (!pgd_none(*pgd)) {
		pud_t *pud = pud_offset(pgd, addr);
		if (!pud_none(*pud)) {
			pmd_t *pmd = pmd_offset(pud, addr);
			if (!pmd_none(*pmd)) {
				pte_t *pte = pte_offset_map(pmd, addr);
				pr_info("pte: %lx\n", pte->pte);
				if (pte_present(*pte)) {
					page = pte_page(*pte);
					addr = (unsigned long)page_address(page);
					pfn = page_to_pfn(page);
					pr_info("pfn: %lx, addr: %lx, pa: %lx\n", pfn, addr, __pa(addr));
				}
				pte_unmap(pte);
			}
		}
	}

	return 0;
}

static void __exit pgtable_test_exit(void)
{
}

module_init(pgtable_test_init);
module_exit(pgtable_test_exit);

MODULE_LICENSE("GPL");
