#include <linux/module.h>
#include <linux/sched.h>
#include <asm/pgtable_types.h>

static struct task_struct *pid_to_task(pid_t pid)
{
    struct task_struct *tsk;
    for_each_process(tsk) {
        if (tsk->pid == pid)
            return tsk;
    }
    return 0;
}

void print_pgd(pid_t pid)
{
    struct task_struct *task = pid_to_task(pid);
    pgd_t *pgd = task->mm->pgd;

    printk(KERN_ALERT "pgd: %lx %lx %p, pid: %d\n", pgd->pgd, __pa(pgd->pgd), __va(pgd->pgd), pid);
    printk(KERN_ALERT "pfn = %lx\n", pgd->pgd / PAGE_SIZE);
    printk(KERN_ALERT "pfn = %lx\n", pgd->pgd >> PAGE_SHIFT);
}

static int __init pgd_test_init(void)
{
//    print_pgd(1956);
    print_pgd(current->pid);
    
    return 0;
}

static void __exit pgd_test_exit(void)
{
}

module_init(pgd_test_init);
module_exit(pgd_test_exit);
