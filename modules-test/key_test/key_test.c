#include <linux/init.h>
#include <linux/module.h>
#include <linux/key.h>
#include <linux/key-type.h>
#include <linux/cred.h>
#include <linux/seq_file.h>

static int test_instantiate(struct key *key, struct key_preparsed_payload *prep)
{
	return 0;
}

static void test_describe(const struct key *key, struct seq_file *m)
{
	seq_puts(m, key->description);
}

struct key_type test_key_type = {
        .name           = "test",
	.instantiate	= test_instantiate,
        .describe       = test_describe,
};

static int __init test_init(void)
{
	const struct cred *cred = current_cred();
	struct key *key;
	int ret;

	register_key_type(&test_key_type);

	key = key_alloc(&test_key_type, "test",
			GLOBAL_ROOT_UID, GLOBAL_ROOT_GID, cred,
			KEY_POS_SEARCH, KEY_ALLOC_NOT_IN_QUOTA, NULL);
	if (IS_ERR(key))
		return -1;

	pr_info("keyring allocated successfully.\n");

	ret = key_instantiate_and_link(key,
				       NULL,
				       sizeof(int),
				       current->cred->user->session_keyring,
				       NULL);
	if (ret < 0) {
		key_revoke(key);
		key_put(key);
		return ret;
	}

	return 0;
}

static void __exit test_exit(void)
{
	unregister_key_type(&test_key_type);
}

module_init(test_init);
module_exit(test_exit);

MODULE_LICENSE("GPL");
