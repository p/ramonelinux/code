#include <linux/init.h>
#include <linux/module.h>
#include <linux/slab.h>
#include <linux/radix-tree.h>

struct test_struct {
	int test;
};

static int __init test_init(void)
{
	int err;
	struct radix_tree_root r_tree;
	struct radix_tree_node *node;
	void **slot;
	int index = 0;
	struct test_struct *t, *p;

	err = __radix_tree_create(&r_tree, index, 0, &node, &slot);
	if (err)
		return err;

	for (index = 1; index < 10; index++) {
		t = kmalloc(sizeof(*t), GFP_KERNEL);
		if (!t)
			return -1;

		t->test = 1000 * index;
		radix_tree_insert(&r_tree, index, t);
	}

	for (index = 1; index < 10; index++) {
		p = (struct test_struct *)radix_tree_lookup(&r_tree, index);
		if (!p)
			return -1;
		pr_info("p[%d]->test=%d\n", index, p->test);

		p = radix_tree_delete(&r_tree, index);
		kfree(p);
	}

	return 0;
}
core_initcall(test_init);
