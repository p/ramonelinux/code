/*
 * http://blog.csdn.net/bullbat/article/details/7407194
 */
#include <linux/init.h>
#include <linux/module.h>
#include <linux/mutex.h>
#include <linux/proc_fs.h>
#include <linux/seq_file.h>
#include <linux/slab.h>

#define N 10

static DEFINE_MUTEX(lock);
static LIST_HEAD(head);

struct my_data {
	struct list_head list;
	int value;
};

/*打印，传入参数v为open函数返回的，链表需要操作的节点*/
static int my_show(struct seq_file *file, void *v)
{
	struct list_head *l = (struct list_head *)v;
	struct my_data *md = list_entry(l, struct my_data, list);

	seq_printf(file, "The value of my data is: %d\n", md->value);

	return 0;
}

static void *my_start(struct seq_file *file, loff_t *pos)
{
	mutex_lock(&lock);
	return seq_list_start(&head, *pos);
}

static void *my_next(struct seq_file *file, void *v, loff_t *pos)
{
	return seq_list_next(v, &head, pos);
}

static void my_stop(struct seq_file *file, void *v)
{
	mutex_unlock(&lock);
}

static const struct seq_operations my_seq_ops = {
	.start	= my_start,
	.next	= my_next,
	.stop	= my_stop,
	.show	= my_show,
};

static int my_open(struct inode *inode, struct file *file)
{
	return seq_open(file, &my_seq_ops);
}

static const struct file_operations my_file_ops = {
	.owner		= THIS_MODULE,
	.open		= my_open,
	.read		= seq_read,
	.llseek		= seq_lseek,
	.release	= seq_release,
};

/*链表的插入元素*/
struct list_head *insert_list(struct list_head *head, int value)
{
	struct my_data *md = NULL;

	mutex_lock(&lock);
	md = kmalloc(sizeof(struct my_data), GFP_KERNEL);
	if (md) {
		md->value = value;
		list_add(&md->list, head);
	}
	mutex_unlock(&lock);

	return head;
}

static int __init test_init(void)
{
	struct proc_dir_entry *entry;
	int i;

	entry = proc_create_data("list_seq", 0, NULL, &my_file_ops, NULL);
	for (i = 0; i < N; i++)
		head = *(insert_list(&head, i));

	return 0;
}

static void __exit test_exit(void)
{
	struct my_data *md = NULL;

	while (!list_empty(&head)) {
		md = list_entry((&head)->next, struct my_data, list);
		list_del(&md->list);
		kfree(md);
	}
	remove_proc_entry("list_seq", NULL);
}

module_init(test_init);
module_exit(test_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Mike Feng");
