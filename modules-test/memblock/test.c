#include <linux/init.h>
#include <linux/module.h>
#include <linux/memblock.h>

static phys_addr_t start, end;
static int size;

static int __init test_init(void)
{	
	start = memblock_start_of_DRAM();
	end = memblock_end_of_DRAM();

	memblock_reserve(end - size, size);

	pr_info("%llx %llx\n", start, end);

	return 0;
}

static void __exit test_exit(void)
{
	memblock_free(end - size, size);
}

module_init(test_init);
module_exit(test_exit);
MODULE_LICENSE("GPL");
