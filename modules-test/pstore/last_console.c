#include <linux/init.h>
#include <linux/slab.h>
#include <linux/vmalloc.h>
#include <linux/memblock.h>
#include <linux/proc_fs.h>

static char *console_buffer;
static ssize_t console_bufsize;

#define PERSISTENT_RAM_SIG (0x43474244) /* DBGC */

static ssize_t last_kmsg_read(struct file *file, char __user *buf,
		size_t len, loff_t *offset)
{
	return simple_read_from_buffer(buf, len, offset,
			console_buffer, console_bufsize);
}

static const struct file_operations last_kmsg_fops = {
	.owner          = THIS_MODULE,
	.read           = last_kmsg_read,
};

struct persistent_ram_buffer {
	uint32_t    sig;
	uint32_t    start;
	uint32_t    size;
	uint8_t     data[0];
};

static int __init last_kmsg(void)
{
	char *size = "4M";
	unsigned long mem_size;
	unsigned long mem_address;
	struct page **pages;
	phys_addr_t page_start;
	unsigned int page_count;
	pgprot_t prot;
	void *vaddr;
	struct persistent_ram_buffer *buffer;
	int i;
	struct proc_dir_entry *entry = NULL;

	entry = proc_create_data("last_kmsg", S_IFREG | S_IRUGO,
				 NULL, &last_kmsg_fops, NULL);

	mem_size = memparse(size, &size) & PAGE_MASK;
	mem_address = 0x40000000;//memblock_end_of_DRAM() - mem_size;

	page_start = mem_address - offset_in_page(mem_address);
	page_count = DIV_ROUND_UP(mem_size + offset_in_page(mem_address), PAGE_SIZE);

	prot = pgprot_noncached(PAGE_KERNEL);

	pages = kmalloc_array(page_count, sizeof(struct page *), GFP_KERNEL);
	if (!pages) {
		pr_err("%s: Failed to allocate array for %u pages\n",
			__func__, page_count);
		return -1;
	}

	for (i = 0; i < page_count; i++) {
	        phys_addr_t addr = page_start + i * PAGE_SIZE;
	        pages[i] = pfn_to_page(addr >> PAGE_SHIFT);
	}
	vaddr = vmap(pages, page_count, VM_MAP, prot);
	kfree(pages);

	buffer = (struct persistent_ram_buffer *)vaddr;
	if (buffer->sig != PERSISTENT_RAM_SIG) {
		pr_err("sig error!\n");
		return -1;
	}

	console_bufsize = buffer->size;
	console_buffer = kmalloc(console_bufsize, GFP_KERNEL);
	memcpy(console_buffer, &buffer->data[0], console_bufsize);

	return 0;
}
postcore_initcall(last_kmsg);
