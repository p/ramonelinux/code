#include <linux/init.h>
#include <linux/pstore.h>
#include <linux/pstore_ram.h>
#include <linux/memblock.h>
#include <linux/platform_device.h>

static struct ramoops_platform_data ramoops_data;

static struct platform_device ramoops_dev = {
	.name = "ramoops",
	.dev = {
		.platform_data = &ramoops_data,
	},
};

static int __init ramoops_memreserve(void)
{
	char *p = "4M";
	unsigned long size;

	size = memparse(p, &p) & PAGE_MASK;
	ramoops_data.mem_size = size;
	ramoops_data.mem_address = 0x40000000;//memblock_end_of_DRAM() - size;
	ramoops_data.console_size = size / 4;
	ramoops_data.ftrace_size = size / 4;
	ramoops_data.pmsg_size = size / 4;
	ramoops_data.dump_oops = 1;

	memblock_reserve(ramoops_data.mem_address, ramoops_data.mem_size);

	return 0;
}

static int __init register_ramoops_device(void)
{
	ramoops_memreserve();
	return platform_device_register(&ramoops_dev);
}
core_initcall(register_ramoops_device);
