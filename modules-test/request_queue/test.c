#include <linux/init.h>
#include <linux/module.h>
#include <linux/fs.h>
#include <linux/blkdev.h>
#include <linux/delay.h>

static int __init test_init(void)
{
	struct block_device *bdev;
	int counter = 0;

	bdev = lookup_bdev("/dev/sdb10");
	if (!IS_ERR(bdev)) {
		//pr_info("", bdev->bd_dev);
		pr_info("bdev->bd_openers=%d\n", bdev->bd_openers);
		//pr_info("%d\n", bdev->bd_inode);
		pr_info("bdev->bd_holders=%d\n", bdev->bd_holders);
		if (bdev->bd_queue) {
			pr_info("bdev->bd_queue->elevator->type->elevator_name=%s",
				bdev->bd_queue->elevator->type->elevator_name);
			if (bdev->bd_disk) {
				pr_info("bdev->bd_disk->major=%d\n", bdev->bd_disk->major);
				if (bdev->bd_queue == bdev->bd_disk->queue)
					pr_info("same queue\n");
				else
					pr_info("Not the same\n");

				for (;;) {
					if (bdev->bd_queue->last_merge)
						pr_info("bdev->bd_queue->last_merge->__sector=%ld",
							bdev->bd_queue->last_merge->__sector);
					counter++;
					msleep_interruptible(1000);
					if (counter >=100)
						break;
				}
			} else {
				pr_info("bdev->bd_disk is NULL\n");
			}
		}
	}

	return 0;
}

static void __exit test_exit(void)
{
}

module_init(test_init);
module_exit(test_exit);
MODULE_LICENSE("GPL");
