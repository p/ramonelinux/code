#include <linux/init.h>
#include <linux/module.h>
#include <linux/sched.h>

static int id;
module_param(id, int, 0644);

static int is_running(pid_t pid)
{
	struct task_struct *tsk;
	for_each_process(tsk) {
		if (tsk->pid == pid)
			return 1;
	}
	return 0;
}

static int __init test_init(void)
{
	if (is_running(id))
		pr_info("pid %d is running.\n", id);
	else
		pr_info("pid %d is not running.\n", id);
	return 0;
}

static void __exit test_exit(void)
{
}

module_init(test_init);
module_exit(test_exit);
MODULE_LICENSE("GPL");
