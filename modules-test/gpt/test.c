#include <linux/init.h>
#include <linux/module.h>
#include "check.h"

static int __init test_init(void)
{
	dev_t dev = MKDEV(8, 16);
	struct parsed_partitions *state;
	struct block_device *bdev;
	
	bdev = bdget(dev);
	if (!IS_ERR(bdev)) {
		state = check_partition(bdev->bd_disk, bdev);
		if (state) {
			pr_info("name: %s\n", state->name);
			pr_info("pp_buf: %s", state->pp_buf);
			free_partitions(state);
		}
	}
	bdput(bdev);
	return 0;
}

static void __exit test_exit(void)
{
}

module_init(test_init);
module_exit(test_exit);
MODULE_LICENSE("GPL");
