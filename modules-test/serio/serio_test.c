#include <linux/init.h>
#include <linux/module.h>
#include <linux/serio.h>
#include <linux/slab.h>

struct serio *serio;
static struct serio_driver drv = {
        .driver         = {
                .name   = "test",
        },
        .description    = "serio test",
};

static int __init test_init(void)
{
	serio = kzalloc(sizeof(struct serio), GFP_KERNEL);
	strlcpy(serio->name, "test", sizeof(serio->name));
	serio_register_port(serio);
	return serio_register_driver(&drv);
}

static void __exit test_exit(void)
{
	serio_unregister_port(serio);
	serio_unregister_driver(&drv);
}

module_init(test_init);
module_exit(test_exit);
MODULE_LICENSE("GPL");
