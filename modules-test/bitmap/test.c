#include <linux/module.h>
#include <linux/init.h>
#include <linux/bitmap.h>

#define MAX_SLOT_TABLE 256
#define TABLE_SZ DIV_ROUND_UP(MAX_SLOT_TABLE, 8*sizeof(long))
struct pm_nl_pernet {
	u32		max_id;
	unsigned long	ids_map[TABLE_SZ];
};

static int __init listest_init(void)
{
	struct pm_nl_pernet pernet = { 0 };
	u32 id;
	int i;

	for (i = 0; i < 256; i++)
		__set_bit(i, pernet.ids_map);

	id = find_first_zero_bit(pernet.ids_map, 256);
	pr_info("%s id=%u\n", __func__, id);
	__clear_bit(50, pernet.ids_map);
	id = find_first_zero_bit(pernet.ids_map, 255);
	pr_info("%s id=%u\n", __func__, id);

	return 0;
}

static void __exit listest_exit(void)
{
}

module_init(listest_init);
module_exit(listest_exit);
MODULE_LICENSE("GPL");
