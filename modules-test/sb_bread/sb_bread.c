#include <linux/init.h>
#include <linux/module.h>
#include <linux/namei.h>
#include <linux/mount.h>
#include <linux/fs.h>
#include <linux/buffer_head.h>

static int __init super_init(void)
{
	struct path p;
	struct dentry *d;
	struct inode *ino;
	struct super_block *sb;
	struct buffer_head *bh;

	int err = kern_path("/bin/bash", LOOKUP_FOLLOW, &p);
	if (err) {
		pr_err("kern_path failed\n");
		return err;
	}

	d = p.dentry;

	ino = d->d_inode;
	pr_info("i_ino: %ld\n", ino->i_ino);

	sb = ino->i_sb;
	pr_info("s_blocksize: %ld\n", sb->s_blocksize);
	pr_info("s_dev: %d\n", sb->s_dev);
	pr_info("s_type: %s\n", sb->s_type->name);
	pr_info("s_type: %lx\n", sb->s_magic);

	bh = sb_bread(sb, 1);
	if (bh) {
		pr_info("b_blocknr: [%ld]\n", bh->b_blocknr);
		pr_info("b_size: [%ld]\n", bh->b_size);
		pr_info("b_data: [%s]\n", bh->b_data);
	}

	brelse(bh);

	return 0;
}

static void __exit super_exit(void)
{
}

module_init(super_init);
module_exit(super_exit);
MODULE_LICENSE("GPL");
