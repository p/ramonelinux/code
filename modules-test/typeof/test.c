#include <linux/init.h>
#include <linux/module.h>
#include <linux/vmalloc.h>

static int __init test_init(void)
{
	struct vmap_area *first;
	typeof(*(first)) f;

	first = &f;
	return 0;
}

static void __exit test_exit(void)
{
}

module_init(test_init);
module_exit(test_exit);
MODULE_LICENSE("GPL");
