#include <linux/init.h>
#include <linux/module.h>
#include <linux/sched.h>
#include <linux/slab.h>
#include <linux/kthread.h>

int my_function(void *arg)
{
	unsigned long j1;
	int i;
	int *p = arg;

	for (i = 0; i < *p; i++) {
		j1 = jiffies + HZ;
	
		while (time_before(jiffies, j1)) {
			schedule();
		}
		printk(KERN_ALERT "The current pid is %d\n", current->pid);
	}
	return 0;	
}

static int __init kthread_test_init(void)
{
	unsigned long cpu;
	struct task_struct *thread;
	int *times = kzalloc(sizeof(*times), GFP_ATOMIC);

	*times = 5;
	for_each_online_cpu(cpu) {
		thread = kthread_create_on_node(my_function, times, cpu_to_node(cpu), "my_thread/%ld", cpu);
		/* bind to cpu here */
		if (likely(!IS_ERR(thread))) {
			kthread_bind(thread, cpu);
			wake_up_process(thread);
		}
	}
	return 0;
}

static void __exit kthread_test_exit(void)
{
}

module_init(kthread_test_init);
module_exit(kthread_test_exit);

MODULE_LICENSE("GPL");
