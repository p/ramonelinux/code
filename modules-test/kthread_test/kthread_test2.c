#include <linux/init.h>
#include <linux/module.h>
#include <linux/sched.h>
#include <linux/kthread.h>

int thread(void *arg)
{
	printk(KERN_ALERT "thread: %s\n", (char *)arg);
	return 0;	
}

static void my_kernel_thread(int (*fn)(void *), void *arg, const char namefmt[])
{
	unsigned long cpu;
	struct task_struct *thread;

	for_each_online_cpu(cpu) {
		thread = kthread_create_on_node(fn, arg, cpu_to_node(cpu), "namefmt/%ld", cpu);
		/* bind to cpu here */
		if (likely(!IS_ERR(thread))) {
			kthread_bind(thread, cpu);
			wake_up_process(thread);
		}
	}
}

static int __init kthread_test_init(void)
{
	char s[10] = "hello";
	char *p = kstrdup("hello world!", GFP_ATOMIC);
	my_kernel_thread(thread, s, "my_thread1");
	my_kernel_thread(thread, "world", "my_thread2");
	my_kernel_thread(thread, p, "my_thread3");
	return 0;
}

static void __exit kthread_test_exit(void)
{
}

module_init(kthread_test_init);
module_exit(kthread_test_exit);

MODULE_LICENSE("GPL");
