#include <linux/init.h>
#include <linux/module.h>
#include <linux/sched.h>
#include <linux/slab.h>
#include <linux/kthread.h>

int my_function(void *arg)
{
	printk(KERN_ALERT "%d\n", *(int *)arg);
	return 0;	
}

static void my_kernel_thread(int (*fn)(void *), void *arg, const char namefmt[])
{
	unsigned long cpu;
	struct task_struct *thread;

	for_each_online_cpu(cpu) {
		thread = kthread_create_on_node(fn, arg, cpu_to_node(cpu), "namefmt/%ld", cpu);
		/* bind to cpu here */
		if (likely(!IS_ERR(thread))) {
			kthread_bind(thread, cpu);
			wake_up_process(thread);
		}
	}
}

static int __init kthread_test_init(void)
{
	int i = 10;
	int *p = &i;

	int *pi = kzalloc(sizeof(*pi), GFP_ATOMIC);
	*pi = 10;

	my_kernel_thread(my_function, &i, "my_thread1");
	my_kernel_thread(my_function, p, "my_thread2");
	my_kernel_thread(my_function, pi, "my_thread3");
	return 0;
}

static void __exit kthread_test_exit(void)
{
}

module_init(kthread_test_init);
module_exit(kthread_test_exit);

MODULE_LICENSE("GPL");
