#include <linux/init.h>
#include <linux/module.h>
#include <linux/bio.h>
#include <linux/kthread.h>
#include <linux/delay.h>
#include <linux/blkdev.h>

static struct task_struct *biotest;

static int biotest_thread(void *data)
{
	struct task_struct *tsk;

	while (!kthread_should_stop()) {
		for_each_process(tsk) {
			//blk_start_plug(tsk->plug);
			if (tsk->plug) {
				if (!list_empty(&tsk->plug->list)) {
					struct request *rq = list_entry_rq(tsk->plug->list.prev);
					if (rq)
						pr_info("%s tsk->comm: %s, disk_name: %ld\n",
							__func__, tsk->comm, rq->__sector);
				}
			}
			//blk_finish_plug(tsk->plug);
		}
		msleep_interruptible(100);
	}

	return 0;
}

static int __init biotest_init(void)
{
	biotest = kthread_run(biotest_thread, NULL, "biotest");
	if (IS_ERR(biotest)) {
		pr_err("create kthread failed!\n");
		return PTR_ERR(biotest);
	}

	return 0;
}

static void __exit biotest_exit(void)
{
	kthread_stop(biotest);
}

module_init(biotest_init);
module_exit(biotest_exit);
MODULE_LICENSE("GPL");
