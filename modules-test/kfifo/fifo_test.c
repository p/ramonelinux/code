#include <linux/init.h>
#include <linux/module.h>
#include <linux/kfifo.h>

static int __init test_init(void)
{
	struct kfifo fifo;
	int ret;
	unsigned int i;
	unsigned int val;

	ret = kfifo_alloc(&fifo, PAGE_SIZE, GFP_KERNEL);
	if (ret)
		return ret;

	for (i = 0; i < 32; i++)
		kfifo_in(&fifo, &i, sizeof(i));

	ret = kfifo_out_peek(&fifo, &val, sizeof(val));
	if (ret != sizeof(val))
		return -EINVAL;
	pr_info("peek: %u\n", val);

	while (!kfifo_is_empty(&fifo)) {
		ret = kfifo_out(&fifo, &val, sizeof(val));
		if (ret != sizeof(val))
			return -EINVAL;
		pr_info("%u\n", val);
	}

	kfifo_free(&fifo);

	return 0;
}

static void __exit test_exit(void)
{
}

module_init(test_init);
module_exit(test_exit);

MODULE_LICENSE("GPL");
