#include <linux/init.h>
#include <linux/module.h>
#include <linux/string.h>

static int __init test_init(void)
{
	char *str = "abcde!@#$%^&*12345";

	pr_info("str: %s\n", str);
	print_hex_dump(KERN_INFO, "str: ", DUMP_PREFIX_NONE, 16, 1, str, strlen(str), true);
	return 0;
}

static void __exit test_exit(void)
{
}

module_init(test_init);
module_exit(test_exit);
MODULE_LICENSE("GPL");
