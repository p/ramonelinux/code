#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/mman.h>

#define handle_error(msg) \
	do { perror(msg); exit(EXIT_FAILURE); } while (0)

int main(int argc, char *argv[])
{
	void *addr;
	int fd;
	char hello[20] = "hello, world!";
	size_t length = sizeof(hello);

	fd = open("/dev/test", O_RDONLY);
	if (fd == -1)
		handle_error("open");

	addr = mmap(NULL, length, PROT_READ | PROT_WRITE, MAP_PRIVATE, fd, 0);
	if (addr == MAP_FAILED)
		handle_error("mmap");

	memcpy(addr, hello, length);

	printf("addr=%lx, %s\n", (long unsigned int)addr, (char *)addr);

	munmap(addr, length);
	close(fd);

	exit(EXIT_SUCCESS);
}
