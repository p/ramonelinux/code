#include <linux/init.h>
#include <linux/module.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/mm_types.h>
#include <linux/mm.h>
#include <linux/memblock.h>
#include <linux/slab.h>
#include <linux/uaccess.h>

#define TEST_MAJOR 261

static struct cdev test_cdev;
static struct class *test_class;

static void simple_vma_open(struct vm_area_struct *vma)
{
}

static void simple_vma_close(struct vm_area_struct *vma)
{
}

static struct vm_operations_struct simple_remap_vm_ops = {
	.open	= simple_vma_open,
	.close	= simple_vma_close,
};

static int test_mmap(struct file *filp, struct vm_area_struct *vma)
{
	if (remap_pfn_range(vma, vma->vm_start, vma->vm_pgoff,
			    vma->vm_end - vma->vm_start, vma->vm_page_prot))
		return -EAGAIN;

	vma->vm_ops = &simple_remap_vm_ops;
	simple_vma_open(vma);

	return 0;
}

static int test_open(struct inode *inode, struct file *filp)
{
	return 0;
}

static int test_release(struct inode *inode, struct file *filp)
{
	return 0;
}

static struct file_operations test_fops = {
	.owner		= THIS_MODULE,
	.mmap		= test_mmap,
	.open		= test_open,
	.release	= test_release,
};

static int __init test_init(void)
{
	int ret;
	dev_t dev = MKDEV(TEST_MAJOR, 0);

	ret = register_chrdev_region(dev, 1, "test");
	if (ret) {
		pr_err("register_chrdev fail!\n");
		return ret;
	}

	cdev_init(&test_cdev, &test_fops);
	ret = cdev_add(&test_cdev, dev, 1);
	if (ret) {
		pr_err("cdev_add fail!\n");
		return ret;
	}

	test_class = class_create(THIS_MODULE, "test_class");
	device_create(test_class, NULL, dev, NULL, "test");

	return 0;
}

static void __exit test_exit(void)
{
	device_destroy(test_class, MKDEV(TEST_MAJOR, 0));
	class_destroy(test_class);
	cdev_del(&test_cdev);
	unregister_chrdev_region(MKDEV(TEST_MAJOR, 0), 1);
}

module_init(test_init);
module_exit(test_exit);

MODULE_LICENSE("GPL");
