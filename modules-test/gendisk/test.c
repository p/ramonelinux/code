#include <linux/init.h>
#include <linux/module.h>
#include <linux/blkdev.h>

static int vdisk_major;
static int vdisk_minors = 16;

struct vdisk_dev {
	struct gendisk *disk;
	struct request_queue *queue;
};

static struct vdisk_dev *dev;

static blk_qc_t vdisk_make_request(struct request_queue *queue, struct bio *bio)
{
	bio_endio(bio);

	return BLK_QC_T_NONE;
}

struct block_device_operations vdisk_ops = {
	.owner = THIS_MODULE,
};

static int __init test_init(void)
{
	dev = kmalloc(sizeof(*dev), GFP_KERNEL);

	vdisk_major = register_blkdev(0, "vdisk");
	if (vdisk_major < 0) {
		return -EBUSY;
	}

	dev->queue = blk_alloc_queue(GFP_KERNEL);
	if (!dev->queue) {
		return -1;
	}

	blk_queue_make_request(dev->queue, vdisk_make_request);
	dev->disk = alloc_disk(vdisk_minors);
	if (!dev->disk) {
		pr_err("alloc_disk failed\n");
		return -1;
	}
	dev->disk->major = vdisk_major;
	dev->disk->first_minor = vdisk_minors;
	dev->disk->fops = &vdisk_ops;
	dev->disk->queue = dev->queue;
	dev->disk->private_data = dev;
	snprintf(dev->disk->disk_name, 32, "vdisk%c", '0');
	set_capacity(dev->disk, 1024);
	add_disk(dev->disk);

	return 0;
}

static void __exit test_exit(void)
{
	del_gendisk(dev->disk);
	put_disk(dev->disk);
	blk_cleanup_queue(dev->queue);
	unregister_blkdev(vdisk_major, "vdisk");
	kfree(dev);
}

module_init(test_init);
module_exit(test_exit);
MODULE_LICENSE("GPL");
