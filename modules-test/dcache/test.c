#include <linux/init.h>
#include <linux/module.h>
#include <linux/sched.h>
#include <linux/fs.h>
#include <linux/fs_struct.h>
#include <linux/dcache.h>
#include <linux/seq_file.h>
#include <linux/mount.h>

struct dentry *dentry;

static int test1_open(struct inode *inode, struct file *file)
{
	return 0;
}

static const struct file_operations test1_file_operations = {
	.open	= test1_open,
	.read	= seq_read,
	.release = seq_release,	
};

static int __init test_init(void)
{
	struct dentry *root = current->fs->root.dentry;
	struct super_block *sb = current->fs->root.mnt->mnt_sb;
	struct inode *inode;
	struct qstr q;

	inode = new_inode(sb);
	if (!inode) {
		pr_err("new_inode error\n");
		return -1;
	}
	inode->i_ino = get_next_ino();
	inode->i_atime = inode->i_mtime = inode->i_ctime = CURRENT_TIME;
	inode->i_mode = S_IFREG | 0444;
	inode->i_fop = &test1_file_operations;

	q.name = "test2";
	q.hash_len = hashlen_string(root, q.name);

	inode_lock(d_inode(root));
	dentry = d_alloc(root, &q);
	if (!dentry) {
		pr_err("d_alloc error\n");
		return -1;
	}
	d_add(dentry, inode);
	inode_unlock(d_inode(root));

	return 0;
}

static void __exit test_exit(void)
{
	d_drop(dentry);
}

module_init(test_init);
module_exit(test_exit);
