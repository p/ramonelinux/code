#include <linux/init.h>
#include <linux/module.h>

static int __init test_init(void)
{
	int i, r;

	for (i = 0; i < 20; i++) {
		r = roundup(i - 1, 4) + 1;
		pr_info("%s i=%d r=%d\n", __func__, i, r);
	}
	return 0;
}

static void __exit test_exit(void)
{
}

module_init(test_init);
module_exit(test_exit);
MODULE_LICENSE("GPL");
