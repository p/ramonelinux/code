#include <linux/init.h>
#include <linux/module.h>
#include <linux/scatterlist.h>
#include <linux/gfp.h>
#include <linux/slab.h>
#include <linux/list.h>

#define MAX_PAGES 10

static struct sg_table *sgt;

static int __init test_init(void)
{
	struct sg_table *table;
	struct scatterlist *sg;
	struct page *page, *tmp;
	void *addr;
	/* Number of entries in sg list */
	unsigned int nents = 0;
	struct list_head pages;
	int i;

	INIT_LIST_HEAD(&pages);

	for (i = 0; i < MAX_PAGES; i++) {
		page = alloc_page(GFP_KERNEL);
		addr = page_address(page);
		sprintf((char *)addr, "%d", i);
		list_add_tail(&page->lru, &pages);
		nents++;
	}

	table = kmalloc(sizeof(struct sg_table), GFP_KERNEL);
	sg_alloc_table(table, nents, GFP_KERNEL);

	sg = table->sgl;
	list_for_each_entry_safe(page, tmp, &pages, lru) {
		sg_set_page(sg, page, PAGE_SIZE, 0);
		pr_info("%s sg->page_link=%lu, sg->offset=%u, sg->length=%u\n",
				__func__, sg->page_link, sg->offset, sg->length);
		sg = sg_next(sg);
		list_del(&page->lru);
	}

	sgt = table;
	return 0;
}

static void __exit test_exit(void)
{
	struct sg_table *table = sgt;
	struct scatterlist *sg;
	struct page *page;
	void *addr;
	int i;

	for_each_sg(table->sgl, sg, table->nents, i) {
		page = sg_page(sg);
		addr = page_address(page);
		pr_info("%s %s sg->page_link=%lu, sg->offset=%u, sg->length=%u\n",
				__func__, (char *)addr, sg->page_link, sg->offset, sg->length);
		__free_page(page);
	}

	sg_free_table(table);
	kfree(table);
}

module_init(test_init);
module_exit(test_exit);
MODULE_LICENSE("GPL");
