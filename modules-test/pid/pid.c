#include <linux/module.h>
#include <linux/init.h>
#include <linux/sched.h>

static int __init test_init(void)
{
	pr_info("comm: %s\n", current->comm);
	pr_info("tgid: %d\n", current->tgid);	// TGID
	pr_info("pid: %d\n", current->pid);	// PID
	// PGID
	// SID
	return 0;
}

static void __exit test_exit(void)
{
}

module_init(test_init);
module_exit(test_exit);
