#include <linux/module.h>
#include <linux/init.h>
#include <linux/crypto.h>

static int __init test_init(void)
{
	struct crypto_cipher *tfm;
	int ret;
	char result[128];
	char temp[128];
	char *code = "Hello everyone, I'm robert! Hello everyone, I'm robert! Hello everyone, I'm robert!";
	char key[32] = "0123456789";
	unsigned int blocksize;
	int i;

	/* Allocate transform for AES ECB mode */
	tfm = crypto_alloc_cipher("aes", 0, 0);
	if (IS_ERR(tfm)) {
		pr_err("failed to load transform for aes ECB mode!\n");
		return -1;
	}

	ret = crypto_cipher_setkey(tfm, key, sizeof(key));
	if (ret) {
		pr_err("failed to setkey\n");
		return -1;
	}

	blocksize = crypto_cipher_blocksize(tfm);

	pr_info("code:\t%s\n", code);

	memset(temp, 0, sizeof(temp));
	
	/* start encrypt */
	for (i = 0; i < sizeof(temp); i += blocksize) {
		crypto_cipher_encrypt_one(tfm, &temp[i], &code[i]);
	}

	print_hex_dump(KERN_INFO, "temp: ", DUMP_PREFIX_NONE, 16, 1, temp, strlen(temp), true);

	memset(result, 0, sizeof(result));

	/* start dencrypt */
	for (i = 0; i < sizeof(temp); i += blocksize) {
		crypto_cipher_decrypt_one(tfm, &result[i], &temp[i]);
	}

	pr_info("result:\t%s\n", result);

	crypto_free_cipher(tfm);

	return 0;
}

static void __exit test_exit(void)
{
}

module_init(test_init);
module_exit(test_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("robert@cm");  
