#include <linux/init.h>
#include <linux/module.h>
#include <linux/slab.h>
#include <crypto/hash.h>

static int do_digest(char* src, char *dst)
{
	struct crypto_shash *tfm;
	struct shash_desc *desc;

	tfm = crypto_alloc_shash("sha1", 0, CRYPTO_ALG_ASYNC); /* "crc32" or "md5" */
	if (IS_ERR(tfm))
		return -1;

	desc = kmalloc(sizeof(*desc) + crypto_shash_descsize(tfm), GFP_KERNEL);
	desc->tfm = tfm;
	desc->flags = CRYPTO_TFM_REQ_MAY_SLEEP;

	crypto_shash_digest(desc, src, strlen(src), dst);

	kfree(desc);
	crypto_free_shash(tfm);
	return 0;
}

static int __init test_init(void)
{
	char *code1 = "234123132513451345";
	char *code2 = "234123132513451345";
	char result1[50], result2[50];
	int ret;

	memset(result1, 0, 50);
	memset(result2, 0, 50);

	ret = do_digest(code1, result1);
	if (ret)
		return -1;

	ret = do_digest(code2, result2);
	if (ret)
		return -1;

	if (memcmp(result1, result2, 50) != 0)
		pr_info("code1 != code2\n");
	else
		pr_info("code1 == code2\n");

	return 0;
}

static void __exit test_exit(void)
{
}

module_init(test_init);
module_exit(test_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("robert@cm");
