#include <linux/module.h>
#include <linux/init.h>
#include <linux/crypto.h>
#include <linux/scatterlist.h>

static int __init test_init(void)
{
	struct crypto_blkcipher *tfm;
	int ret;
	//static const char blkcipher_alg[] = "cbc(aes)"; //"ecb(des)";
	static const char blkcipher_alg[] = "ecb(des)";
	//char *in = "Hello everyone, I'm robert! Hello everyone, I'm robert! Hello everyone, I'm robert!";
	char *in = "hello5678";
	char out[8] = { 0 }, tmp[8] = { 0 };
	unsigned char key[8] = "01234567";
	//unsigned int blocksize;
	u8 iv[32];
	struct scatterlist sgin, sgout, sgtmp;
	struct blkcipher_desc desc;
	//void *addr;
	unsigned int iv_len;

	memset(tmp, 0, 8);
	memset(out, 0, 8);

	/* Allocate transform for AES CBC mode */
	tfm = crypto_alloc_blkcipher(blkcipher_alg, 0, CRYPTO_ALG_ASYNC);
	if (IS_ERR(tfm)) {
		pr_err("failed to load transform for aes ECB mode!\n");
		return -1;
	}

	desc.tfm = tfm;

	ret = crypto_blkcipher_setkey(tfm, key, 8);
	if (ret) {
		pr_err("failed to setkey\n");
		crypto_free_blkcipher(tfm);
		return -1;
	}

	//iv_len = crypto_blkcipher_ivsize(tfm);
	//if (iv_len)
	//	memset(&iv, 0xff, iv_len);

	//crypto_blkcipher_get_iv(tfm, iv, 32);
	//pr_info("iv: %s\n", iv);

	//blocksize = crypto_blkcipher_blocksize(tfm);

	sg_init_one(&sgin, in, 8);
	sg_init_one(&sgtmp, tmp, 8);
	sg_init_one(&sgout, out, 8);
	pr_info("in: %s\n", in);
	//addr = sg_virt(sg1);

	/* start encrypt */
	ret = crypto_blkcipher_encrypt(&desc, &sgtmp, &sgin, 8);
	if (ret)
		pr_err("encrypt error, ret = %d\n", ret);

	/* start dencrypt */
	ret = crypto_blkcipher_decrypt(&desc, &sgout, &sgtmp, 8);
	if (ret)
		pr_err("decrypt error\n");

	pr_info("out: %s\n", out);

	crypto_free_blkcipher(tfm);

	return 0;
}

static void __exit test_exit(void)
{
}

module_init(test_init);
module_exit(test_exit);
MODULE_LICENSE("GPL");
