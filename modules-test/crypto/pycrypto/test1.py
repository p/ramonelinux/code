#!/usr/bin/python

from Crypto.Cipher import AES
from Crypto import Random

message = "The answer is no, it is just a test for pycrypto"
print message

key = b'must be 16 bytes'
iv = Random.new().read(AES.block_size)

enc = AES.new(key, AES.MODE_CBC, iv)
ciphertext = enc.encrypt(message)
print ciphertext

dec = AES.new(key, AES.MODE_CBC, iv)
print dec.decrypt(ciphertext)
