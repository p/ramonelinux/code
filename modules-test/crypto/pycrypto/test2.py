#!/usr/bin/python

from Crypto.Cipher import AES
from Crypto import Random

message = "This is just a test for pycrypto"
print message

key = b'must be 16 bytes'

enc = AES.new(key, AES.MODE_ECB)
ciphertext = enc.encrypt(message)
print ciphertext

dec = AES.new(key, AES.MODE_ECB)
print dec.decrypt(ciphertext)
