#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <sys/socket.h>
#include <linux/if_alg.h>

int main(int argc, char *argv[])
{
	int opfd, tfmfd;
	struct sockaddr_alg sa = {
		.salg_family = AF_ALG,
		.salg_type = "compress",
		.salg_name = "deflate"
	};
	char buf[20];
	int i;

	if (argc != 2)
		return -1;

	tfmfd = socket(AF_ALG, SOCK_SEQPACKET, 0);

	bind(tfmfd, (struct sockaddr *)&sa, sizeof(sa));

	opfd = accept(tfmfd, NULL, 0);

	// TODO

	write(opfd, argv[1], strlen(argv[1]));
	read(opfd, buf, sizeof(buf));

	for (i = 0; i < sizeof(buf); i++)
		printf("%02x", (unsigned char)buf[i]);
	printf("\n");

	close(opfd);
	close(tfmfd);

	return 0;
}
