#include <linux/init.h>
#include <linux/module.h>
#include <linux/slab.h>
#include <linux/scatterlist.h>
#include <linux/crypto.h>

struct crypto_comp *tfm;
char *code = "Hello everyone, I'm robert from cm!";

static inline void hexdump(unsigned char *buf, unsigned int len)
{
	while (len--)
		printk("0x%02x, ", *buf++);
	printk("\n");
}

static int __init test_init(void)
{
	int ret,result_len,temp_len;
	char result[512];
	char temp[512];

	pr_info("%s\n", code);

	if (!crypto_has_comp("deflate", 0, 0)) {
		pr_err("No deflate comp\n");
		return -1;
	}

	/* Allocate transform for deflate */
	tfm = crypto_alloc_comp("deflate", 0, 0);
	if (IS_ERR_OR_NULL(tfm)) {
		pr_err("Failed to load transform for deflate!\n");
		return 0;
	}

	memset(result, 0, sizeof(result));

	temp_len = 512;
	ret = crypto_comp_compress(tfm, code, strlen(code), temp, &temp_len);
	if (ret) {
		pr_err("Failed to compress!\n");
		return 0;
	}

	hexdump(temp, strlen(temp));

	memset(result, 0, sizeof(result));

	result_len = 512;
	ret = crypto_comp_decompress(tfm, temp, strlen(temp), result, &result_len);
	if (ret) {
		pr_err("Failed to decompress!\n");
		return 0;
	}

	pr_info("%s\n", result);

	if (memcmp(code, result, strlen(code)))
		pr_info("Decompressed was not successful\n");
	else
		pr_info("Decompressed was successful\n");

	crypto_free_comp(tfm);
	return 0;
}

static void __exit test_exit(void)
{
}

module_init(test_init);
module_exit(test_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("robert@cm");
