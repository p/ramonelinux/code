#include <linux/init.h>
#include <linux/module.h>
#include <linux/miscdevice.h>
#include <linux/fs.h>

static char tmp[32];

ssize_t echo_read(struct file *file, char __user *buf,
		size_t count, loff_t *ppos)
{
	return simple_read_from_buffer(buf, count, ppos, tmp, strlen(tmp));
}

ssize_t echo_write(struct file *file, const char __user *buf,
		size_t count, loff_t *ppos)
{
	return simple_write_to_buffer(tmp, sizeof(tmp) - 1, ppos, buf, count);
}

static const struct file_operations echo_fops = {
	.owner		= THIS_MODULE,
	.read		= echo_read,
	.write		= echo_write,
};

static struct miscdevice echo_device = {
	.minor		= MISC_DYNAMIC_MINOR,
	.name		= "echo",
	.nodename	= "ctl/echo",
	.fops		= &echo_fops,
	.mode		= 0666,
};

static int __init test_init(void)
{
	return misc_register(&echo_device);
}

static void __exit test_exit(void)
{
	misc_deregister(&echo_device);
}

module_init(test_init);
module_exit(test_exit);
MODULE_LICENSE("GPL");
