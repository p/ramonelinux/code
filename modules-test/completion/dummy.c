#include <linux/init.h>
#include <linux/module.h>
#include <linux/miscdevice.h>
#include <linux/fs.h>
#include <linux/completion.h>
#include <linux/rwlock.h>

#define DUMMY_NAME "dummy"

static DECLARE_COMPLETION(comp);
static char foo_buf[PAGE_SIZE];
static DEFINE_RWLOCK(foo_buf_lock);

ssize_t dummy_read(struct file *file, char __user *buf,
		size_t count, loff_t *ppos)
{
	int rc;

	if (*ppos != 0)
		return 0;

	wait_for_completion(&comp);

	read_lock(&foo_buf_lock);
	rc = simple_read_from_buffer(buf, count, ppos,
			foo_buf, strlen(foo_buf));
	read_unlock(&foo_buf_lock);
	return rc;
}

ssize_t dummy_write(struct file *file, const char __user *buf,
		size_t count, loff_t *ppos)
{
	int len;

	write_lock(&foo_buf_lock);
	len = (count > PAGE_SIZE) ? PAGE_SIZE : count;
	len = simple_write_to_buffer(foo_buf, sizeof(foo_buf) - 1,
			ppos, buf, len);
	foo_buf[len] = '\0';
	write_unlock(&foo_buf_lock);

	complete(&comp);

	return count;
}

static const struct file_operations  dummy_ops = {
	.owner	=	THIS_MODULE,
	.read	=	dummy_read,
	.write	=	dummy_write,
};

static struct miscdevice dummy_device = {
	MISC_DYNAMIC_MINOR, DUMMY_NAME, &dummy_ops
};

static int __init dummy_init(void)
{
	return misc_register(&dummy_device);
}

static void __exit dummy_exit(void)
{
	misc_deregister(&dummy_device);
}

module_init(dummy_init);
module_exit(dummy_exit);

MODULE_AUTHOR("Geliang Tang <tanggeliang@gmail.com>");
MODULE_DESCRIPTION("completion demo");
MODULE_LICENSE("GPL");
