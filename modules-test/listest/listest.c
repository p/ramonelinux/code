#include <linux/module.h>
#include <linux/slab.h>
#include <linux/list.h>

struct fox {
	unsigned long tail_length;
	unsigned long weight;
	bool is_fantastic;
	struct list_head list;
};

static LIST_HEAD(fox_list);

struct fox fire_fox = {
	.tail_length = 20,
	.weight = 2,
	.is_fantastic = true,
	.list = LIST_HEAD_INIT(fire_fox.list),
};

static void print1(void)
{
	struct list_head *p;
	struct fox *f;
	list_for_each(p, &fox_list) {
		f = list_entry(p, struct fox, list);
		pr_info("print1 - tail_length: %ld, weight: %ld\n", f->tail_length, f->weight);
	}
}

static void print2(void)
{
	struct fox *f;
	list_for_each_entry(f, &fox_list, list) {
		pr_info("print2 - tail_length: %ld, weight: %ld\n", f->tail_length, f->weight);
	}
}

static void print3(void)
{
	struct fox *f;
	list_for_each_entry_reverse(f, &fox_list, list) {
		pr_info("print3 - tail_length: %ld, weight: %ld\n", f->tail_length, f->weight);
	}
}

static void print4(void)
{
	struct list_head *this, *next;
	struct fox *f;
	list_for_each_safe(this, next, &fox_list) {
		f = list_entry(this, struct fox, list);
		pr_info("print4 - tail_length: %ld, weight: %ld\n", f->tail_length, f->weight);
	}
}

static void print5(void)
{
	struct fox *f, *next;
	list_for_each_entry_safe(f, next, &fox_list, list) {
		pr_info("print4 - tail_length: %ld, weight: %ld\n", f->tail_length, f->weight);
	}
}

static int __init listest_init(void)
{
	struct fox *red_fox;
	red_fox = kmalloc(sizeof(*red_fox), GFP_KERNEL);
	red_fox->tail_length = 10;
	red_fox->weight = 1;
	red_fox->is_fantastic = false;
	INIT_LIST_HEAD(&red_fox->list);

	list_add(&red_fox->list, &fox_list);
	list_add(&fire_fox.list, &fox_list);

	print1();
	print2();
	print3();
	print4();
	print5();

	list_del(&red_fox->list);

	return 0;
}

static void __exit listest_exit(void)
{
	list_del(&fire_fox.list);
}

module_init(listest_init);
module_exit(listest_exit);
MODULE_LICENSE("GPL");
