#include <linux/init.h>
#include <linux/module.h>
#include <linux/slab.h>
#include <linux/sched.h>

struct test_struct {
	int val;
};

static struct kmem_cache *cachep;
static void *object;
#if 1
static int n;

static void slab_ctor(void *cachep)
{
	pr_info("slab_ctor is called! object %d has been inited!\n", n);
	n++;
}
#endif

static int __init test_init(void)
{
#if 1
	cachep = kmem_cache_create("my_cache", sizeof(struct test_struct), 0,
			(SLAB_HWCACHE_ALIGN | SLAB_RECLAIM_ACCOUNT), slab_ctor);
#else
	cachep = KMEM_CACHE(test_struct, SLAB_HWCACHE_ALIGN | SLAB_RECLAIM_ACCOUNT);
#endif
	if (!cachep) {
		pr_err("kmem_cache_create failed!\n");
		return -ENOMEM;
	}

	pr_info("Cache size is: %d\n", kmem_cache_size(cachep));

	object = kmem_cache_alloc(cachep, GFP_KERNEL);
	if (!object) {
		pr_err("kmem_cache_alloc failed!\n");
		return -ENOMEM;
	}

	pr_info("allocate object is 0x%lx\n", (unsigned long)object);

	return 0;
}

static void __exit test_exit(void)
{
	kmem_cache_free(cachep, object);
	kmem_cache_destroy(cachep);
}

module_init(test_init);
module_exit(test_exit);

MODULE_LICENSE("GPL");
