#include <linux/module.h>
#include <linux/kernel.h>

static int __init timer_init(void)
{
	unsigned long time = jiffies + HZ;
	pr_info("jiffies: %lu, jiffies_64: %llu, HZ: %d, USER_HZ: %d, time: %lu\n",
		jiffies, jiffies_64, HZ, USER_HZ, time);
	return 0;
}

static void __exit timer_cleanup(void)
{
}

module_init(timer_init);
module_exit(timer_cleanup);

MODULE_LICENSE("GPL");
