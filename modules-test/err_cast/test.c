#include <linux/init.h>
#include <linux/module.h>
#include <linux/slab.h>

struct test_obj {
	int i;
};

static struct test_obj *alloc_obj(void)
{
	struct test_obj *obj;

	obj = kzalloc(sizeof(*obj), GFP_KERNEL);
	if (IS_ERR(obj))
		return ERR_PTR(PTR_ERR(obj));
	if (IS_ERR(obj))
		return ERR_CAST(obj);

	return obj;
}

static void free_obj(struct test_obj *obj)
{
	kfree(obj);
}

static int __init test_init(void)
{
	struct test_obj *obj;

	obj = alloc_obj();

	free_obj(obj);

	return 0;
}

static void __exit test_exit(void)
{
}

module_init(test_init);
module_exit(test_exit);
MODULE_LICENSE("GPL");
