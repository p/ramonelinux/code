#include <linux/init.h>
#include <linux/module.h>
#include <linux/bio.h>
#include <linux/blkdev.h>

static blk_qc_t make_request(struct request_queue *queue, struct bio *bio)
{
	pr_info("%s\n", __func__);
	bio_endio(bio);
	return BLK_QC_T_NONE;
}

static int __init test_init(void)
{
	dev_t dev = MKDEV(8, 26);
	struct block_device *bdev;
	struct request_queue *q;

	bdev = bdget(dev);
	if (!IS_ERR(bdev)) {
		q = bdev_get_queue(bdev);
		blk_queue_make_request(q, make_request);
	}
	bdput(bdev);
	return 0;
}

static void __exit test_exit(void)
{
}

module_init(test_init);
module_exit(test_exit);
MODULE_LICENSE("GPL");
