#include <linux/init.h>
#include <linux/module.h>
#include <crypto/sha.h>

static int __init test_init(void)
{
	struct sha256_state state;
	u64 key = 12345;
	uint8_t hashed_key[SHA256_DIGEST_SIZE / 4];

	sha256_init(&state);
	sha256_update(&state, (__force u8 *)&key, sizeof(key));
	sha256_final(&state, hashed_key);

	pr_info("%d\n", hashed_key[0]);
	pr_info("%d\n", hashed_key[1]);
	pr_info("%d\n", hashed_key[2]);
	pr_info("%d\n", hashed_key[3]);
	pr_info("%d\n", hashed_key[4]);
	pr_info("%d\n", hashed_key[5]);
	pr_info("%d\n", hashed_key[6]);
	pr_info("%d\n", hashed_key[7]);
	return 0;
}

static void __exit test_exit(void)
{
}

module_init(test_init);
module_exit(test_exit);
MODULE_LICENSE("GPL");
