#include <linux/init.h>
#include <linux/module.h>
#include <linux/shrinker.h>

static unsigned long shrink_count(struct shrinker *s, struct shrink_control *sc)
{
	return 0;
}

static unsigned long shrink_scan(struct shrinker *s, struct shrink_control *sc)
{
	printk(KERN_ALERT "shrink_scan\n");
	return 0;
}

static struct shrinker lowmem_shrinker = {
	.count_objects = shrink_count,
	.scan_objects = shrink_scan,
	.seeks = DEFAULT_SEEKS * 10,
};

static int __init lowmem_init(void)
{
	register_shrinker(&lowmem_shrinker);
	return 0;
}

static void __exit lowmem_exit(void)
{
	unregister_shrinker(&lowmem_shrinker);
}

module_init(lowmem_init);
module_exit(lowmem_exit);
