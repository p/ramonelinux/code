#include <linux/init.h>
#include <linux/module.h>
#include <linux/buffer_head.h>

struct my_struct {
	int id;
	char *data;
	struct buffer_head head;
};

static int __init bh_test_init(void)
{
	return 0;
}

static void __exit bh_test_exit(void)
{
}

module_init(bh_test_init);
module_exit(bh_test_exit);
