#include <linux/init.h>
#include <linux/module.h>
#include <linux/slab.h>
#include <linux/fs.h>
#include <linux/buffer_head.h>
#include "testfs.h"

ssize_t testfs_read(struct file *file, char __user *buf,
	  	    size_t count, loff_t *ppos)
{
	struct buffer_head *bh;
	char tmp[1024];
	struct inode *inode = file_inode(file);
	struct super_block *sb = inode->i_sb;

	if (!(bh = sb_bread(sb, 1000))) {
		return -1;
	}

	memcpy(tmp, (char *)bh->b_data, inode->i_size);
	brelse(bh);

	return simple_read_from_buffer(buf, count, ppos, tmp, inode->i_size);
}

ssize_t testfs_write(struct file *file, const char __user *buf,
		     size_t count, loff_t *ppos)
{
	struct buffer_head *bh;
	char tmp[1024];
	struct inode *inode = file_inode(file);
	struct super_block *sb = inode->i_sb;
	size_t ret;

	if (!(bh = sb_bread(sb, 1000))) {
		return -1;
	}

	ret = simple_write_to_buffer(tmp, sizeof(tmp) - 1, ppos, buf, count);
	memcpy((char *)bh->b_data, tmp, ret);
	mark_buffer_dirty(bh);
	brelse(bh);

	inode->i_size = ret;

	return ret;
}

static const struct file_operations testfs_file_operations = {
	.read   = testfs_read,
	.write  = testfs_write,
};

static int testfs_mkfiles(struct inode *dir, char *name, umode_t mode)
{
	struct super_block *sb = dir->i_sb;
	struct dentry *root = sb->s_root;
	struct dentry *dentry;
	struct inode *inode;

	inode = new_inode(sb);
	if (inode) {
		inode->i_ino = get_next_ino();
		inode->i_atime = inode->i_mtime = inode->i_ctime = CURRENT_TIME;
		inode->i_mode = S_IFREG | 0444;
		inode->i_fop = &testfs_file_operations;
		insert_inode_hash(inode);
		mark_inode_dirty(inode);
	}

	dentry = d_alloc_name(root, name);
	if (!dentry) {
		return -1;
	}

	d_add(dentry, inode);

	return 0;
}

static int testfs_create(struct inode * dir, struct dentry *dentry, umode_t mode, bool excl)
{
	return testfs_mkfiles(dir, dentry->d_iname, mode);
}

static const struct inode_operations testfs_dir_inode_operations = {
	.create	= testfs_create,
	.lookup	= simple_lookup,
	.unlink	= simple_unlink,
};

static void testfs_evict_inode(struct inode *inode)
{
	truncate_inode_pages_final(&inode->i_data);
	invalidate_inode_buffers(inode);
	clear_inode(inode);
}

static int testfs_remount(struct super_block *sb, int *flags, char *data)
{
	sync_filesystem(sb);

	return 0;
}

static const struct super_operations testfs_sops = {
        .statfs         = simple_statfs,
        .drop_inode     = generic_delete_inode,
        .evict_inode    = testfs_evict_inode,
	.remount_fs	= testfs_remount,
        .show_options   = generic_show_options,
};

static int testfs_fill_super(struct super_block *sb, void *data, int silent)
{
	struct inode *root_inode;
	struct buffer_head *bh;
	struct testfs_sb_info *sbi;
	struct testfs_super_block *ts;

	sbi = kzalloc(sizeof(struct testfs_sb_info), GFP_KERNEL);
	if (!sbi)
		return -ENOMEM;
	sb->s_fs_info = sbi;

	if (!(bh = sb_bread(sb, 100)))
		goto out;

	ts = (struct testfs_super_block *)bh->b_data;
	sbi->s_ts = ts;
	sbi->s_ninodes = ts->s_ninodes;
	sbi->s_max_size = ts->s_max_size;

	sb->s_maxbytes          = MAX_LFS_FILESIZE;
	sb->s_blocksize		= PAGE_SIZE;
	sb->s_blocksize_bits	= PAGE_SHIFT;
	sb->s_magic		= TESTFS_MAGIC;
	sb->s_op		= &testfs_sops;
	sb->s_time_gran		= 1;

	root_inode = new_inode(sb);
	if (root_inode) {
		root_inode->i_ino = get_next_ino();
		root_inode->i_atime = root_inode->i_mtime = root_inode->i_ctime = CURRENT_TIME;
		root_inode->i_mode = S_IFDIR | 0755;
		root_inode->i_op = &testfs_dir_inode_operations;
		root_inode->i_fop = &simple_dir_operations;
		inc_nlink(root_inode);
	}
	sb->s_root = d_make_root(root_inode);
	if (!sb->s_root)
		return -ENOMEM;

	brelse(bh);
	return 0;
out:
	sb->s_fs_info = NULL;
	kfree(sbi);
	return -1;
}

static struct dentry *testfs_mount(struct file_system_type *fs_type,
	int flags, const char *dev_name, void *data)
{
	return mount_bdev(fs_type, flags, dev_name, data, testfs_fill_super);
}

static void testfs_kill_sb(struct super_block *sb)
{
	kill_litter_super(sb);
}

static struct file_system_type testfs_type = {
	.owner		= THIS_MODULE,
	.name		= "testfs",
	.mount		= testfs_mount,
	.kill_sb	= testfs_kill_sb,
	.fs_flags	= FS_REQUIRES_DEV,
};

static int __init testfs_init(void)
{
	register_filesystem(&testfs_type);

	return 0;
}

static void __exit testfs_exit(void)
{
	unregister_filesystem(&testfs_type);
}

module_init(testfs_init);
module_exit(testfs_exit);
