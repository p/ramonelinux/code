
#define TESTFS_MAGIC 0x12345678

struct testfs_super_block {
	__u16 s_ninodes;
	__u32 s_max_size;
	__u16 s_magic;
};

struct testfs_sb_info {
	unsigned long s_ninodes;
	unsigned long s_max_size;
	struct testfs_super_block *s_ts;
};

