#include <linux/init.h>
#include <linux/module.h>
#include <linux/fs.h>

#define TESTFS_MAGIC 0x12345678

static struct super_block *testfs_sb;

static int testfs_mkfiles(void)
{
	struct dentry *root = testfs_sb->s_root;
	struct dentry *dentry;
	struct inode *inode;

	inode = new_inode(testfs_sb);
	if (inode) {
		inode->i_ino = get_next_ino();
		inode->i_atime = inode->i_mtime = inode->i_ctime = CURRENT_TIME;
		inode->i_mode = S_IFREG | 0444;
	}

	inode_lock(d_inode(root));
	dentry = d_alloc_name(root, "test1");
	if (!dentry) {
		return -1;
	}

	d_add(dentry, inode);
	inode_unlock(d_inode(root));

	return 0;
}

static const struct inode_operations testfs_dir_inode_operations = {
	.lookup		= simple_lookup,
};

static void testfs_evict_inode(struct inode *inode)
{
	clear_inode(inode);
}

static const struct super_operations testfs_sops = {
        .statfs         = simple_statfs,
        .drop_inode     = generic_delete_inode,
        .evict_inode    = testfs_evict_inode,
        .show_options   = generic_show_options,
};

static int testfs_fill_super(struct super_block *sb, void *data, int silent)
{
	struct inode *inode;

	testfs_sb = sb;

	sb->s_maxbytes          = MAX_LFS_FILESIZE;
	sb->s_blocksize		= PAGE_SIZE;
	sb->s_blocksize_bits	= PAGE_SHIFT;
	sb->s_magic		= TESTFS_MAGIC;
	sb->s_op		= &testfs_sops;
	sb->s_time_gran		= 1;

	inode = new_inode(sb);
	if (inode) {
		inode->i_ino = get_next_ino();
		inode->i_atime = inode->i_mtime = inode->i_ctime = CURRENT_TIME;
		inode->i_mode = S_IFDIR | 0755;
		inode->i_op = &testfs_dir_inode_operations;
		inode->i_fop = &simple_dir_operations;
		inc_nlink(inode);
	}
	sb->s_root = d_make_root(inode);
	if (!sb->s_root)
		return -ENOMEM;

	testfs_mkfiles();

	return 0;
}

static struct dentry *testfs_mount(struct file_system_type *fs_type,
	int flags, const char *dev_name, void *data)
{
	return mount_single(fs_type, flags, data, testfs_fill_super);
}

static void testfs_kill_sb(struct super_block *sb)
{
	kill_litter_super(sb);
	testfs_sb = NULL;
}

static struct file_system_type testfs_type = {
	.owner	= THIS_MODULE,
	.name	= "testfs",
	.mount	= testfs_mount,
	.kill_sb = testfs_kill_sb,
};

static int __init testfs_init(void)
{
	register_filesystem(&testfs_type);

	return 0;
}

static void __exit testfs_exit(void)
{
	unregister_filesystem(&testfs_type);
}

module_init(testfs_init);
module_exit(testfs_exit);
