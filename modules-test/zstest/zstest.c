#include <linux/module.h>
#include <linux/init.h>
#include <linux/zsmalloc.h>

static struct zs_pool *mem_pool;

static int __init zstest_init(void)
{
	unsigned long handle[10];
	size_t len = 20;
	unsigned char src[20];
	unsigned char *wmem, *rmem;
	u64 total_pages = 0;
	int i;

	mem_pool = zs_create_pool("test");
	if (!mem_pool) {
		printk("Error creating memory pool\n");
		return -ENOMEM;
	}

	for (i = 0; i < 10; i++) {
		handle[i] = zs_malloc(mem_pool, len,
				__GFP_KSWAPD_RECLAIM |
				__GFP_NOWARN |
				__GFP_HIGHMEM |
				__GFP_MOVABLE);
		if (!handle[i]) {
			printk("Error allocating memory");
			return -ENOMEM;
		}
		wmem = zs_map_object(mem_pool, handle[i], ZS_MM_WO);
		sprintf(src, "%d %s", i, "hello, world");
		memcpy(wmem, src, strlen(src));
		zs_unmap_object(mem_pool, handle[i]);
	}

	total_pages = zs_get_total_pages(mem_pool);
	printk("total_pages: %llu\n", total_pages);
	for (i = 0; i < 10; i++)
		printk("handle[%d]: %lx\n", i, handle[i]);
	for (i = 0; i < 10; i++) {
		rmem = zs_map_object(mem_pool, handle[i], ZS_MM_RO);
		printk("rmem: %s\n", rmem);
		zs_unmap_object(mem_pool, handle[i]);
		zs_free(mem_pool, handle[i]);
	}
	return 0;
}

static void zstest_exit(void)
{
	zs_destroy_pool(mem_pool);
}

module_init(zstest_init);
module_exit(zstest_exit);
MODULE_LICENSE("Dual BSD/GPL");
