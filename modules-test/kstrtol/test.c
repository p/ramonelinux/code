#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>

/**
 * include/linux/kernel.h
 * static inline int __must_check kstrtol(const char *s, unsigned int base, long *res)
 *
 * kstrtol - convert a string to a long
 * @s: The start of the string. The string must be null-terminated, and may also
 *  include a single newline before its terminating null. The first character
 *  may also be a plus sign or a minus sign.
 * @base: The number base to use. The maximum supported base is 16. If base is
 *  given as 0, then the base of the string is automatically detected with the
 *  conventional semantics - If it begins with 0x the number will be parsed as a
 *  hexadecimal (case insensitive), if it otherwise begins with 0, it will be
 *  parsed as an octal number. Otherwise it will be parsed as a decimal.
 * @res: Where to write the result of the conversion on success.
 *
 * Returns 0 on success, -ERANGE on overflow and -EINVAL on parsing error.
 * Used as a replacement for the obsolete simple_strtoull. Return code must
 * be checked.
 */

static char* str;
module_param(str, charp, S_IRUGO);

static int __init test_init(void)
{
	long val;
	int ret;
	
	if (!str)
		return -EINVAL;

	ret = kstrtol(str, 0, &val);
	if (!ret)
		pr_info("str=%s, val=%ld\n", str, val);

	return 0;
}

static void __exit test_exit(void)
{
}

module_init(test_init);
module_exit(test_exit);
MODULE_LICENSE("GPL");
