#include <linux/module.h>
#include <linux/init.h>
#include <linux/blkdev.h>
#include <linux/vmalloc.h>

static int vmem_disk_major = 0;
static int vmem_disk_minors = 16;
static int hardsect_size = 512;
static int nsectors = 1024;     /* How big the drive is */

/*
 * We can tweak our hardware sector size, but the kernel talks to us
 * in terms of small sectors, always.
 */
#define KERNEL_SECTOR_SIZE      512

/*
 * The internal representation of our device.
 */
struct vmem_disk_dev {
	int size;                       /* Device size in sectors */
	u8 *data;                       /* The data array */
	struct rw_semaphore lock;       /* For mutual exclusion */
	struct request_queue *queue;    /* The device request queue */
	struct gendisk *disk;           /* The gendisk structure */
};
static struct vmem_disk_dev *devices;

static int vmem_disk_bvec_read(struct vmem_disk_dev *dev, struct bio_vec *bvec, unsigned long sector)
{
	unsigned long offset = sector * KERNEL_SECTOR_SIZE;
	unsigned long nbytes = bvec->bv_len;
	struct page *page = bvec->bv_page;
	unsigned char *user_mem = kmap_atomic(page);

	if ((offset + nbytes) > dev->size) {
		pr_err("Beyond-end write (%ld %ld)\n", offset, nbytes);
		return -1;
	}

	memcpy(user_mem + bvec->bv_offset, dev->data + offset, nbytes);
	kunmap_atomic(user_mem);

	return nbytes;
}

static int vmem_disk_bvec_write(struct vmem_disk_dev *dev, struct bio_vec *bvec, unsigned long sector)
{
	unsigned long offset = sector * KERNEL_SECTOR_SIZE;
	unsigned long nbytes = bvec->bv_len;
	struct page *page = bvec->bv_page;
	unsigned char *user_mem = kmap_atomic(page);

	if ((offset + nbytes) > dev->size) {
		pr_err("Beyond-end write (%ld %ld)\n", offset, nbytes);
		return -1;
	}

	memcpy(dev->data + offset, user_mem + bvec->bv_offset, nbytes);
	kunmap_atomic(user_mem);

	return nbytes;
}

static int vmem_disk_bvec_rw(struct vmem_disk_dev *dev, struct bio_vec *bvec, unsigned long sector, int rw)
{
	int ret;

	if (rw == READ) {
		down_read(&dev->lock);
		ret = vmem_disk_bvec_read(dev, bvec, sector);
		up_read(&dev->lock);
	} else if (rw == WRITE) {
		down_write(&dev->lock);
		ret = vmem_disk_bvec_write(dev, bvec, sector);
		up_write(&dev->lock);
	}

	return ret;
}

static int __vmem_disk_make_request(struct vmem_disk_dev *dev, struct bio *bio)
{
	struct bio_vec bvec;
	struct bvec_iter iter;
	sector_t sector = bio->bi_iter.bi_sector;

	bio_for_each_segment(bvec, bio, iter) {
		if (vmem_disk_bvec_rw(dev, &bvec, sector, bio_data_dir(bio)) < 0)
			goto out;

		sector += bio_sectors(bio);
	}
	return 0;

out:
	bio_io_error(bio);
	return -1;
}

static blk_qc_t vmem_disk_make_request(struct request_queue *queue, struct bio *bio)
{
	struct vmem_disk_dev *dev = queue->queuedata;
	int status;

	status = __vmem_disk_make_request(dev, bio);
	bio_endio(bio);

	return BLK_QC_T_NONE;
}

/*
 * The device operations structure.
 */
struct block_device_operations vmem_disk_ops = {
	.owner = THIS_MODULE,
};

/*
 * Set up our internal device.
 */
static void setup_device(struct vmem_disk_dev *dev)
{
	/*
	 * Get some memory.
	 */
	memset(dev, 0, sizeof (struct vmem_disk_dev));
	dev->size = nsectors * hardsect_size;
	dev->data = vmalloc(dev->size);
	if (!dev->data) {
		pr_err("vmalloc failure.\n");
		return;
	}
	init_rwsem(&dev->lock);

	dev->queue = blk_alloc_queue(GFP_KERNEL);
	if (!dev->queue)
		goto out_vfree;
	blk_queue_make_request(dev->queue, vmem_disk_make_request);
	blk_queue_logical_block_size(dev->queue, hardsect_size);
	dev->queue->queuedata = dev;

	/*
	 * And the gendisk structure.
	 */
	dev->disk = alloc_disk(vmem_disk_minors);
	if (!dev->disk) {
		pr_err("alloc_disk failure\n");
		goto out_vfree;
	}
	dev->disk->major = vmem_disk_major;
	dev->disk->first_minor = vmem_disk_minors;
	dev->disk->fops = &vmem_disk_ops;
	dev->disk->queue = dev->queue;
	dev->disk->private_data = dev;
	snprintf(dev->disk->disk_name, 32, "vmem_disk%c", '0');
	set_capacity(dev->disk, nsectors * (hardsect_size / KERNEL_SECTOR_SIZE));
	add_disk(dev->disk);
	return;

out_vfree:
	if (dev->data)
		vfree(dev->data);
}

static int __init vmem_disk_init(void)
{
	/*
	 * Get registered.
	 */
	vmem_disk_major = register_blkdev(vmem_disk_major, "vmem_disk");
	if (vmem_disk_major <= 0) {
		pr_err("vmem_disk: unable to get major number\n");
		return -EBUSY;
	}

	/*
	 * Allocate the device array, and initialize each one.
	 */
	devices = kmalloc(sizeof(struct vmem_disk_dev), GFP_KERNEL);
	if (!devices)
		goto out_unregister;
	setup_device(devices);

	return 0;

out_unregister:
	unregister_blkdev(vmem_disk_major, "sbd");
	return -ENOMEM;
}

static void vmem_disk_exit(void)
{
	struct vmem_disk_dev *dev = devices;

	if (dev->disk) {
		del_gendisk(dev->disk);
		put_disk(dev->disk);
	}
	if (dev->queue) {
		blk_cleanup_queue(dev->queue);
	}
	if (dev->data)
		vfree(dev->data);

	unregister_blkdev(vmem_disk_major, "vmem_disk");
	kfree(devices);
}

module_init(vmem_disk_init);
module_exit(vmem_disk_exit);
MODULE_LICENSE("Dual BSD/GPL");
