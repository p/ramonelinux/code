#include <linux/init.h>
#include <linux/module.h>
#include <linux/fs.h>
#include <linux/kdev_t.h>

static int __init test_init(void)
{
	dev_t dev = MKDEV(8, 1);
	struct block_device *bdev;
	char buf[32];

	bdev = bdget(dev);
	if (!IS_ERR(bdev)) {
		bdevname(bdev, buf);
		pr_info("buf: %s\n", buf);
	}
	bdput(bdev);

	return 0;
}

static void __exit test_exit(void)
{
}

module_init(test_init);
module_exit(test_exit);
MODULE_LICENSE("GPL");
