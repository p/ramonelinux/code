#undef TRACE_SYSTEM
#define TRACE_SYSTEM trace-sample

#undef TRACE_SYSTEM_VAR
#define TRACE_SYSTEM_VAR trace_sample

#if !defined(_TRACE_SAMPLE_H) || defined(TRACE_HEADER_MULTI_READ)
#define _TRACE_SAMPLE_H

#include <linux/tracepoint.h>

TRACE_EVENT(simple_thread_func,

	TP_PROTO(const char *foo, int bar,
		 const char *string),

	TP_ARGS(foo, bar, string),

	TP_STRUCT__entry(
		__array(	char,	foo,    10		)
		__field(	int,	bar			)
		__string(	str,	string			)
	),

	TP_fast_assign(
		strlcpy(__entry->foo, foo, 10);
		__entry->bar	= bar;
		__assign_str(str, string);
	),

	TP_printk("foo %s %d %s %s", __entry->foo, __entry->bar,

		  __print_flags(__entry->bar, "|",
				{ 1, "BIT1" },
				{ 2, "BIT2" },
				{ 4, "BIT3" },
				{ 8, "BIT4" }
			  ),

		  __get_str(str))
);

#endif

#undef TRACE_INCLUDE_PATH
#undef TRACE_INCLUDE_FILE
#define TRACE_INCLUDE_PATH .
#define TRACE_INCLUDE_FILE trace-sample
#include <trace/define_trace.h>
