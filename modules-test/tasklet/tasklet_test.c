/*
 * http://blog.csdn.net/bullbat/article/details/7423321
 */
#include <linux/init.h>
#include <linux/module.h>
#include <linux/interrupt.h>

static void tasklet_func(unsigned long value)
{
	printk("This is tasklet function!\n");
}

/*用静态方式声明并定义一个tasklet，动态方式一样*/
DECLARE_TASKLET(my_tasklet, &tasklet_func, 0);

static int __init my_tasklet_init(void)
{
	/*直接调度我们的函数*/
	tasklet_schedule(&my_tasklet);
	return 0;
}

static void __exit my_tasklet_exit(void)
{
	tasklet_kill(&my_tasklet);
}

module_init(my_tasklet_init);
module_exit(my_tasklet_exit);

MODULE_AUTHOR("Mike Feng");
MODULE_LICENSE("GPL");
