#include <linux/module.h>
#include <linux/init.h>
#include <linux/zpool.h>

static struct zpool *pool;
static char *zpool_type = "zsmalloc";

static int __init test_init(void)
{
	unsigned long handle[10];
	size_t len = 20;
	unsigned char src[20];
	unsigned char *wmem, *rmem;
	u64 total_size = 0;
	int i, ret;

	if (zpool_has_pool(zpool_type)) {
		pool = zpool_create_pool(zpool_type, "test", 0, NULL);
		if (!pool) {
			pr_err("Error creating memory pool\n");
			return -ENOMEM;
		}

		for (i = 0; i < 10; i++) {
			ret = zpool_malloc(pool, len,
					__GFP_KSWAPD_RECLAIM |
					__GFP_NOWARN |
					__GFP_HIGHMEM |
					__GFP_MOVABLE, &handle[i]);
			if (!handle[i]) {
				pr_err("Error allocating memory");
				return -ENOMEM;
			}
			wmem = zpool_map_handle(pool, handle[i], ZPOOL_MM_WO);
			sprintf(src, "%d %s", i, "hello, world");
			memcpy(wmem, src, strlen(src));
			zpool_unmap_handle(pool, handle[i]);
		}

		total_size = zpool_get_total_size(pool);
		printk("total_pages: %llu\n", total_size);
		for (i = 0; i < 10; i++)
			printk("handle[%d]: %lx\n", i, handle[i]);
		for (i = 0; i < 10; i++) {
			rmem = zpool_map_handle(pool, handle[i], ZPOOL_MM_RO);
			printk("rmem: %s\n", rmem);
			zpool_unmap_handle(pool, handle[i]);
			zpool_free(pool, handle[i]);
		}
	} else {
		pr_info("no type zsmalloc\n");
	}

	return 0;
}

static void test_exit(void)
{
	zpool_destroy_pool(pool);
}

module_init(test_init);
module_exit(test_exit);
MODULE_LICENSE("Dual BSD/GPL");
