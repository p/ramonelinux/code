#include <linux/module.h>
#include <linux/init.h>
#include <linux/zpool.h>

static int __init test_init(void)
{
	if (zpool_has_pool("zsmalloc"))
		pr_info("type zsmalloc\n");
	return 0;
}

static void test_exit(void)
{
}

module_init(test_init);
module_exit(test_exit);
MODULE_LICENSE("Dual BSD/GPL");
