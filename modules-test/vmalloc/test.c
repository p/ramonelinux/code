#include <linux/init.h>
#include <linux/module.h>
#include <linux/string.h>
#include <linux/vmalloc.h>

static char *addr;

static int __init test_init(void)
{
	addr = (char *)vmalloc(100);
	pr_info("0x%p\n", addr);
	strcpy(addr, "hello");
	return 0;
}

static void __exit test_exit(void)
{
	pr_info("addr: %s\n", addr);
	vfree(addr);
}

module_init(test_init);
module_exit(test_exit);
MODULE_LICENSE("GPL");
