#include <linux/init.h>
#include <linux/module.h>
#include <linux/string.h>
#include <linux/slab.h>

static int __init test_init(void)
{
	char *s = kstrndup("hello", 20, GFP_KERNEL);
	strcpy(s + 5, ", world");
	pr_info("s: %s, len: %lu\n", s, strlen(s));
	kfree(s);
	s = kmemdup("hello", 20, GFP_KERNEL);
	strcpy(s + 5, ", world");
	pr_info("s: %s, len: %lu\n", s, strlen(s));
	kfree(s);
	return 0;
}

static void __exit test_exit(void)
{
}

module_init(test_init);
module_exit(test_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("foo");
MODULE_DESCRIPTION("A kernel rbtree test module");
