#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/rbtree.h>
#include <linux/string.h>
#include <linux/slab.h>

struct mytype {
	struct rb_node node;
	char *keystring;
};

struct rb_root mytree = RB_ROOT;

struct mytype *my_search(struct rb_root *root, char *string)
{
	struct rb_node *node = root->rb_node;

	while (node) {
		struct mytype *data = container_of(node, struct mytype, node);
		int result;

		result = strcmp(string, data->keystring);

		if (result < 0)
			node = node->rb_left;
		else if (result > 0)
			node = node->rb_right;
		else
			return data;
	}
	return NULL;
}

int my_insert(struct rb_root *root, struct mytype *data)
{
	struct rb_node **new = &(root->rb_node), *parent = NULL;

	/* Figure out where to put new node */
	while (*new) {
		struct mytype *this = container_of(*new, struct mytype, node);
		int result = strcmp(data->keystring, this->keystring);

		parent = *new;
		if (result < 0)
			new = &((*new)->rb_left);
		else if (result > 0)
			new = &((*new)->rb_right);
		else
			return false;
	}

	/* Add new node and rebalance tree. */
	rb_link_node(&data->node, parent, new);
	rb_insert_color(&data->node, root);

	return true;
}

void print_all(void)
{
	struct rb_node *node;

	for (node = rb_first(&mytree); node; node = rb_next(node))
		pr_info("key=%s\n", rb_entry(node, struct mytype, node)->keystring);
}

void rbtest(void)
{
	struct mytype *data;
	int i;
	char buf[10];

	for (i = 0; i < 10; i++) {
		data = kzalloc(sizeof(*data), GFP_ATOMIC);
		sprintf(buf, "%d", i);
		data->keystring = kstrdup(buf, GFP_ATOMIC);
		my_insert(&mytree, data);
	}

	print_all();

	data = my_search(&mytree, "2");
	if (data) {
		pr_info("find: %s\n", data->keystring);
		rb_erase(&data->node, &mytree);
		kzfree(data);
	} else {
		pr_err("not find\n");
	}

	print_all();
}

static int __init rbtest_init(void)
{
	rbtest();
	return 0;
}

static void __exit rbtest_exit(void)
{
	pr_info("rbtest exit!\n");
}

module_init(rbtest_init);
module_exit(rbtest_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("foo");
MODULE_DESCRIPTION("A kernel rbtree test module");
