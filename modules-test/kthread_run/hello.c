/*
 * 运行不息的内核线程kthread
 * http://zhangwenxin82.blog.163.com/blog/static/114595956201211493426236/
 */
#include <linux/init.h>
#include <linux/module.h>
#include <linux/kthread.h>
#include <linux/delay.h>

static struct task_struct *tsk;

static int thread_function(void *data)
{
	int time_count = 0;
	do {
		pr_info("thread_function: %d times\n", ++time_count);
		msleep(1000);
	} while(!kthread_should_stop() && time_count <= 30);
	return time_count;
}

static int hello_init(void)
{
	pr_info("Hello, world!\n");

	tsk = kthread_run(thread_function, NULL, "mythread%d", 1);
	if (IS_ERR(tsk))
		pr_err("create kthread failed!\n");
	else
		pr_info("create ktrhead ok!\n");
	return 0;
}

static void hello_exit(void)
{
	pr_info("Hello, exit!\n");

	if (!IS_ERR(tsk)) {
		int ret = kthread_stop(tsk);
		pr_info("thread function has run %ds\n", ret);
	}
}

module_init(hello_init);
module_exit(hello_exit);
MODULE_LICENSE("Dual BSD/GPL");
