#include <linux/init.h>
#include <linux/module.h>
#include <linux/kthread.h>

int my_thread(void *arg)
{
	pr_info("my_thread: %s\n", (char *)arg);
	return 0;	
}

static int __init test_init(void)
{
	kthread_run(my_thread, "hello, world", "mythread");
	return 0;
}

static void __exit test_exit(void)
{
}

module_init(test_init);
module_exit(test_exit);

MODULE_LICENSE("GPL");
