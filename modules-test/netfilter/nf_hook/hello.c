#include <linux/init.h>
#include <linux/module.h>
#include <linux/netfilter.h>
#include <linux/netfilter_ipv4.h>

static unsigned int hello_hook(void *priv,
		struct sk_buff *skb,
		const struct nf_hook_state *state)
{
	pr_info("%s\n", __func__);

	return NF_ACCEPT;
}

static const struct nf_hook_ops hello_ops __read_mostly = {
	.hook		= hello_hook,
	.pf		= NFPROTO_IPV4,
	.hooknum	= NF_INET_LOCAL_IN,
	.priority	= NF_IP_PRI_FIRST,
};

static int __init hello_init(void)
{
	return nf_register_net_hook(&init_net, &hello_ops);
}

static void __exit hello_exit(void)
{
	nf_unregister_net_hook(&init_net, &hello_ops);
}

module_init(hello_init);
module_exit(hello_exit);
MODULE_LICENSE("GPL");
