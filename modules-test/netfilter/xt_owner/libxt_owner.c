/*
 *	libxt_owner - iptables addon for xt_owner
 *
 *	Copyright © CC Computer Consultants GmbH, 2007 - 2008
 *	Jan Engelhardt <jengelh@computergmbh.de>
 */
#include <grp.h>
#include <pwd.h>
#include <stdbool.h>
#include <stdio.h>
#include <limits.h>
#include <xtables.h>
#include <linux/netfilter/xt_owner.h>

/*
 *	Note: "UINT32_MAX - 1" is used in the code because -1 is a reserved
 *	UID/GID value anyway.
 */

enum {
	O_USER = 0,
	O_GROUP,
	O_SOCK_EXISTS,
	O_PROCESS,
	O_SESSION,
	O_COMM,
};

static void owner_mt_help(void)
{
	printf(
"owner match options:\n"
"[!] --uid-owner userid[-userid]      Match local UID\n"
"[!] --gid-owner groupid[-groupid]    Match local GID\n"
"[!] --socket-exists                  Match if socket exists\n");
}

static const struct xt_option_entry owner_mt_opts[] = {
	{.name = "uid-owner", .id = O_USER, .type = XTTYPE_STRING,
	 .flags = XTOPT_INVERT},
	{.name = "gid-owner", .id = O_GROUP, .type = XTTYPE_STRING,
	 .flags = XTOPT_INVERT},
	{.name = "socket-exists", .id = O_SOCK_EXISTS, .type = XTTYPE_NONE,
	 .flags = XTOPT_INVERT},
	XTOPT_TABLEEND,
};

static void owner_parse_range(const char *s, unsigned int *from,
                              unsigned int *to, const char *opt)
{
	char *end;

	/* -1 is reversed, so the max is one less than that. */
	if (!xtables_strtoui(s, &end, from, 0, UINT32_MAX - 1))
		xtables_param_act(XTF_BAD_VALUE, "owner", opt, s);
	*to = *from;
	if (*end == '-' || *end == ':')
		if (!xtables_strtoui(end + 1, &end, to, 0, UINT32_MAX - 1))
			xtables_param_act(XTF_BAD_VALUE, "owner", opt, s);
	if (*end != '\0')
		xtables_param_act(XTF_BAD_VALUE, "owner", opt, s);
}

static void owner_mt_parse(struct xt_option_call *cb)
{
	struct xt_owner_match_info *info = cb->data;
	struct passwd *pwd;
	struct group *grp;
	unsigned int from, to;

	xtables_option_parse(cb);
	switch (cb->entry->id) {
	case O_USER:
		if ((pwd = getpwnam(cb->arg)) != NULL)
			from = to = pwd->pw_uid;
		else
			owner_parse_range(cb->arg, &from, &to, "--uid-owner");
		if (cb->invert)
			info->invert |= XT_OWNER_UID;
		info->match  |= XT_OWNER_UID;
		info->uid_min = from;
		info->uid_max = to;
		break;
	case O_GROUP:
		if ((grp = getgrnam(cb->arg)) != NULL)
			from = to = grp->gr_gid;
		else
			owner_parse_range(cb->arg, &from, &to, "--gid-owner");
		if (cb->invert)
			info->invert |= XT_OWNER_GID;
		info->match  |= XT_OWNER_GID;
		info->gid_min = from;
		info->gid_max = to;
		break;
	case O_SOCK_EXISTS:
		if (cb->invert)
			info->invert |= XT_OWNER_SOCKET;
		info->match |= XT_OWNER_SOCKET;
		break;
	}
}

static void owner_mt_check(struct xt_fcheck_call *cb)
{
	if (cb->xflags == 0)
		xtables_error(PARAMETER_PROBLEM, "owner: At least one of "
		           "--uid-owner, --gid-owner or --socket-exists "
		           "is required");
}

static void
owner_mt_print_item(const struct xt_owner_match_info *info, const char *label,
                    uint8_t flag, bool numeric)
{
	if (!(info->match & flag))
		return;
	if (info->invert & flag)
		printf(" !");
	printf(" %s", label);

	switch (info->match & flag) {
	case XT_OWNER_UID:
		if (info->uid_min != info->uid_max) {
			printf(" %u-%u", (unsigned int)info->uid_min,
			       (unsigned int)info->uid_max);
			break;
		} else if (!numeric) {
			const struct passwd *pwd = getpwuid(info->uid_min);

			if (pwd != NULL && pwd->pw_name != NULL) {
				printf(" %s", pwd->pw_name);
				break;
			}
		}
		printf(" %u", (unsigned int)info->uid_min);
		break;

	case XT_OWNER_GID:
		if (info->gid_min != info->gid_max) {
			printf(" %u-%u", (unsigned int)info->gid_min,
			       (unsigned int)info->gid_max);
			break;
		} else if (!numeric) {
			const struct group *grp = getgrgid(info->gid_min);

			if (grp != NULL && grp->gr_name != NULL) {
				printf(" %s", grp->gr_name);
				break;
			}
		}
		printf(" %u", (unsigned int)info->gid_min);
		break;
	}
}

static void owner_mt_print(const void *ip, const struct xt_entry_match *match,
                           int numeric)
{
	const struct xt_owner_match_info *info = (void *)match->data;

	owner_mt_print_item(info, "owner socket exists", XT_OWNER_SOCKET, numeric);
	owner_mt_print_item(info, "owner UID match",     XT_OWNER_UID,    numeric);
	owner_mt_print_item(info, "owner GID match",     XT_OWNER_GID,    numeric);
}

static void owner_mt_save(const void *ip, const struct xt_entry_match *match)
{
	const struct xt_owner_match_info *info = (void *)match->data;

	owner_mt_print_item(info, "--socket-exists",  XT_OWNER_SOCKET, true);
	owner_mt_print_item(info, "--uid-owner",      XT_OWNER_UID,    true);
	owner_mt_print_item(info, "--gid-owner",      XT_OWNER_GID,    true);
}

static int
owner_mt_print_uid_xlate(const struct xt_owner_match_info *info,
			 struct xt_xlate *xl)
{
	xt_xlate_add(xl, "skuid%s ", info->invert ? " !=" : "");

	if (info->uid_min != info->uid_max)
		xt_xlate_add(xl, "%u-%u", (unsigned int)info->uid_min,
			     (unsigned int)info->uid_max);
	else
		xt_xlate_add(xl, "%u", (unsigned int)info->uid_min);

	return 1;
}

static int
owner_mt_print_gid_xlate(const struct xt_owner_match_info *info,
			 struct xt_xlate *xl)
{
	xt_xlate_add(xl, "skgid%s ", info->invert ? " !=" : "");

	if (info->gid_min != info->gid_max)
		xt_xlate_add(xl, "%u-%u", (unsigned int)info->gid_min,
			     (unsigned int)info->gid_max);
	else
		xt_xlate_add(xl, "%u", (unsigned int)info->gid_min);

	return 1;
}

static int owner_mt_xlate(struct xt_xlate *xl,
			  const struct xt_xlate_mt_params *params)
{
	const struct xt_owner_match_info *info = (void *)params->match->data;
	int ret;

	switch (info->match) {
	case XT_OWNER_UID:
		ret = owner_mt_print_uid_xlate(info, xl);
		break;
	case XT_OWNER_GID:
		ret = owner_mt_print_gid_xlate(info, xl);
		break;
	default:
		ret = 0;
	}

	return ret;
}

static struct xtables_match owner_mt_reg = {
	.version       = XTABLES_VERSION,
	.name          = "owner",
	.revision      = 1,
	.family        = NFPROTO_UNSPEC,
	.size          = XT_ALIGN(sizeof(struct xt_owner_match_info)),
	.userspacesize = XT_ALIGN(sizeof(struct xt_owner_match_info)),
	.help          = owner_mt_help,
	.x6_parse      = owner_mt_parse,
	.x6_fcheck     = owner_mt_check,
	.print         = owner_mt_print,
	.save          = owner_mt_save,
	.x6_options    = owner_mt_opts,
	.xlate	       = owner_mt_xlate,
};

void _init(void)
{
	xtables_register_match(&owner_mt_reg);
}
