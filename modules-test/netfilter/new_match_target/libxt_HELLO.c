#include <xtables.h>
#include <linux/netfilter/x_tables.h>

static struct xtables_target hello_target = {
	.family		= NFPROTO_UNSPEC,
	.name		= "HELLO",
	.version	= XTABLES_VERSION,
	.size		= XT_ALIGN(0),
	.userspacesize	= XT_ALIGN(0),
};

void _init(void)
{
	xtables_register_target(&hello_target);
}
