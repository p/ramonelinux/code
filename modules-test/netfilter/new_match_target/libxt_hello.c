#include <xtables.h>
#include "xt_hello.h"

static struct xtables_match hello_mt_reg = {
	.version	= XTABLES_VERSION,
	.name 		= "hello",
	.revision	= 1,
	.family		= NFPROTO_UNSPEC,
	.size		= XT_ALIGN(sizeof(struct xt_hello_info)),
	.userspacesize	= XT_ALIGN(sizeof(struct xt_hello_info)),
};

void _init(void)
{
	xtables_register_match(&hello_mt_reg);
}
