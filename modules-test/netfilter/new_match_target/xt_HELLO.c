#include <linux/module.h>
#include <linux/skbuff.h>

#include <linux/netfilter/x_tables.h>

static int hello_tg_check(const struct xt_tgchk_param *par)
{
	return 0;
}

static void hello_tg_destroy(const struct xt_tgdtor_param *par)
{
}

static unsigned int
hello_tg(struct sk_buff *skb, const struct xt_action_param *par)
{
	pr_info("%s\n", __func__);
	return XT_CONTINUE;
}

static struct xt_target hello_tg_reg __read_mostly = {
	.name		= "HELLO",
	.revision	= 0,
	.family		= NFPROTO_UNSPEC,
	.table		= "filter",
	.target		= hello_tg,
	.checkentry	= hello_tg_check,
	.destroy	= hello_tg_destroy,
	.me		= THIS_MODULE,
};

static int __init hello_init(void)
{
	return xt_register_target(&hello_tg_reg);
}

static void __exit hello_exit(void)
{
	xt_unregister_target(&hello_tg_reg);
}

module_init(hello_init);
module_exit(hello_exit);
MODULE_LICENSE("GPL");
