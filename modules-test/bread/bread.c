#include <linux/init.h>
#include <linux/module.h>
#include <linux/fs.h>
#include <linux/buffer_head.h>

static int __init test_init(void)
{
	struct block_device *bdev;
	struct buffer_head *bh;

	bdev = lookup_bdev("/dev/mapper/ubuntu--vg-root");
	if (!IS_ERR(bdev)) {
		bh = __bread(bdev, 1, 4096);
		if (bh) {
			pr_info("b_blocknr: [%ld]\n", bh->b_blocknr);
			pr_info("b_size: [%ld]\n", bh->b_size);
			pr_info("b_data: [%s]\n", bh->b_data);
		}
		brelse(bh);
	}

	return 0;
}

static void __exit test_exit(void)
{
}

module_init(test_init);
module_exit(test_exit);
MODULE_LICENSE("GPL");
