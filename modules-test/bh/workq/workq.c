#include <linux/init.h>
#include <linux/module.h>
#include <linux/workqueue.h>

static void do_work(struct work_struct *);
static DECLARE_WORK(my_work, do_work);
static struct workqueue_struct *wq;

static void do_work(struct work_struct *work)
{
	pr_info("in do_work!\n");
}

static int __init test_init(void)
{
	wq = create_workqueue("my_wq");
	queue_work(wq, &my_work);

	return 0;
}

static void __exit test_exit(void)
{
	destroy_workqueue(wq);
}

module_init(test_init);
module_exit(test_exit);
MODULE_LICENSE("GPL");
