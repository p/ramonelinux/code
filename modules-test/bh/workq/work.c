#include <linux/init.h>
#include <linux/module.h>
#include <linux/workqueue.h>

static void do_work(struct work_struct *);
static DECLARE_WORK(mywork, do_work);

static void do_work(struct work_struct *work)
{
	if (in_interrupt())
		pr_info("In interrupt\n");
	else
		pr_info("Not in interrupt\n");

	pr_info("in do_work!\n");
}

static int __init test_init(void)
{
	schedule_work(&mywork);
	return 0;
}

static void __exit test_exit(void)
{
}

module_init(test_init);
module_exit(test_exit);
MODULE_LICENSE("GPL");
