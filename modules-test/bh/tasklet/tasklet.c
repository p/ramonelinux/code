#include <linux/init.h>
#include <linux/module.h>
#include <linux/interrupt.h>

static void tasklet_func(unsigned long value);
static DECLARE_TASKLET(my_tasklet, &tasklet_func, 0);

static void tasklet_func(unsigned long value)
{
	if (in_interrupt())
		pr_info("In interrupt\n");
	else
		pr_info("Not in interrupt\n");

	printk("This is tasklet function!\n");
}


static int __init test_init(void)
{
	tasklet_schedule(&my_tasklet);
	return 0;
}

static void __exit test_exit(void)
{
	tasklet_kill(&my_tasklet);
}

module_init(test_init);
module_exit(test_exit);
MODULE_LICENSE("GPL");
