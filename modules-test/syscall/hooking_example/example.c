#include <linux/init.h>
#include <linux/module.h>
#include <linux/syscalls.h>
#include <asm/pgtable_types.h>

static ulong mem_address = 0xc15c4240;
module_param(mem_address, ulong, S_IRUGO);
void **sys_call_table;
asmlinkage int (*original_call) (const char*, int, int);

asmlinkage int our_sys_open(const char* file, int flags, int mode)
{
	printk("A file was opened: %s\n", file);
	return original_call(file, flags, mode);
}

static void set_addr_rw(unsigned long addr)
{
	unsigned int level;
	pte_t *pte = lookup_address(addr, &level);

	if (pte->pte &~ _PAGE_RW)
		pte->pte |= _PAGE_RW;
}

static void set_addr_ro(unsigned long addr)
{
	unsigned int level;
	pte_t *pte = lookup_address(addr, &level);

	pte->pte = pte->pte &~_PAGE_RW;
}

static int __init example_init(void)
{
	// sys_call_table address in System.map
	sys_call_table = (void*)mem_address;
	original_call = sys_call_table[__NR_open];

	set_addr_rw(mem_address);
	sys_call_table[__NR_open] = our_sys_open;
	set_addr_ro(mem_address);
	return 0;
}

static void __exit example_exit(void)
{
	// Restore the original call
	set_addr_rw(mem_address);
	sys_call_table[__NR_open] = original_call;
	set_addr_ro(mem_address);
}

module_init(example_init);
module_exit(example_exit);
MODULE_LICENSE("GPL");
