#include <stdio.h>
#include <unistd.h>
#include <sys/syscall.h>

#define __NR_foo 358

int main(int argc, char *argv[])
{
	long stack_size;

	stack_size = syscall(__NR_foo);
	printf("The kernel stack size is %ld.\n", stack_size);

	return 0;
}
