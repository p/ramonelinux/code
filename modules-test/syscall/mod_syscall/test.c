#include <stdio.h>
#include <stdlib.h>

#define __NR_mycall 223

int main()
{
	unsigned long x = 0;
	
	x = syscall(__NR_mycall);
	printf("hello, %ld\n", x);
	
	return 0;
}        
