#include <linux/init.h>
#include <linux/module.h>
#include <linux/syscalls.h>
#include <asm/syscall.h>

static ulong mem_address;
module_param(mem_address, ulong, S_IRUGO);

static sys_call_ptr_t *syscall_table;
static sys_call_ptr_t orig_sys_open;

static asmlinkage long my_sys_open(const char __user *filename, int flags, umode_t mode)
{
	typedef long (*open_func)(const char __user *filename, int flags, umode_t mode);
	open_func o = (open_func)orig_sys_open;
	pr_info("file %s has been opened with mode %d\n", filename, mode);
	return o(filename, flags, mode);
}

static void set_open_syscall(sys_call_ptr_t syscall)
{
	unsigned long cr0;

	cr0 = read_cr0();
	write_cr0(cr0 & ~X86_CR0_WP);
	syscall_table[__NR_open] = syscall;
	write_cr0(cr0);
}

static int __init hook_init(void)
{
	if (!mem_address) {
		pr_err("No syscall table address\n");
		return -EFAULT;
	}
	syscall_table = (sys_call_ptr_t *)mem_address;
	orig_sys_open = syscall_table[__NR_open];
	set_open_syscall((sys_call_ptr_t)my_sys_open);

	return 0;
}

static void __exit hook_exit(void)
{
	set_open_syscall((sys_call_ptr_t)orig_sys_open);
}

module_init(hook_init);
module_exit(hook_exit);

MODULE_LICENSE("GPL");
