#include <linux/module.h>
#include <linux/init.h>
#include <linux/syscalls.h>
#include <linux/kallsyms.h>

static void **syscall_table;
asmlinkage long (*orig_sys_open)(const char __user *filename, int flags, umode_t mode);

asmlinkage long my_sys_open(const char __user *filename, int flags, umode_t mode)
{
	pr_info("file %s has been opened with mode %d\n", filename, mode);
	return orig_sys_open(filename, flags, mode);
}

static int __init syscall_init(void)
{
	unsigned long cr0;

	/* CONFIG_KALLSYMS_ALL=y */
	syscall_table = (void **)kallsyms_lookup_name("sys_call_table");
	if (!syscall_table) {
		pr_err("Cannot find the system call address\n"); 
		return -EFAULT;
	}
	orig_sys_open = syscall_table[__NR_open];

	cr0 = read_cr0();
	write_cr0(cr0 & ~X86_CR0_WP);
	syscall_table[__NR_open] = my_sys_open;
	write_cr0(cr0);

	return 0;
}

static void __exit syscall_release(void)
{
	unsigned long cr0;

	cr0 = read_cr0();
	write_cr0(cr0 & ~X86_CR0_WP);
	syscall_table[__NR_open] = orig_sys_open;
	write_cr0(cr0);
}

module_init(syscall_init);
module_exit(syscall_release);
MODULE_LICENSE("GPL");
