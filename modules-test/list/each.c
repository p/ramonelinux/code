#include <linux/module.h>
#include <linux/slab.h>
#include <linux/list.h>

struct fox {
	char *name;
	struct list_head list;
};

static LIST_HEAD(fox_list);

/**
 * list_is_first - tests whether @list is the first entry in list @head
 * @list: the entry to test
 * @head: the head of the list
 */
static inline int list_is_first(const struct list_head *list,
                                const struct list_head *head)
{
        return list->prev == head;
}

#define list_for_each_entry_new(pos, head, member)                          \
        for (pos = list_first_entry(head, typeof(*pos), member);        \
             !list_is_last(&pos->member, head);                                    \
             pos = list_next_entry(pos, member))

static void print_first_and_last(void)
{
	struct fox *f;

	list_for_each_entry_new(f, &fox_list, list)
		pr_info("%s\n", f->name);
}

struct fox *alloc_add_fox(char *name)
{
	struct fox *fox;
	fox = kmalloc(sizeof(*fox), GFP_KERNEL);
	fox->name = kstrdup(name, GFP_KERNEL);
	INIT_LIST_HEAD(&fox->list);
	list_add_tail(&fox->list, &fox_list);

	return fox;
}

static void clean(void)
{
	while (!list_empty(&fox_list)) {
		struct fox *f = list_first_entry_or_null(&fox_list, struct fox, list);
		if (f) {
			kfree(f->name);
			list_del(&f->list);
		}
	}
}

static int __init listest_init(void)
{
	alloc_add_fox("red fox");
	alloc_add_fox("green fox");
	alloc_add_fox("blue fox");

	print_first_and_last();

	return 0;
}

static void __exit listest_exit(void)
{
	clean();
	BUG_ON(!list_empty(&fox_list));
}

module_init(listest_init);
module_exit(listest_exit);
MODULE_LICENSE("GPL");
