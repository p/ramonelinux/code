#include <linux/module.h>
#include <linux/slab.h>
#include <linux/list.h>

struct fox {
	char *name;
	struct list_head list;
};

static LIST_HEAD(fox_list);

/**
 * list_is_first - tests whether @list is the first entry in list @head
 * @list: the entry to test
 * @head: the head of the list
 */
static inline int list_is_first(const struct list_head *list,
                                const struct list_head *head)
{
        return list->prev == head;
}

static void print_first_and_last(void)
{
	struct fox *f;

	list_for_each_entry(f, &fox_list, list) {
		if (list_is_first(&f->list, &fox_list))
			pr_info("list_is_first %s\n", f->name);

		if (list_is_last(&f->list, &fox_list))
			pr_info("list_is_last %s\n", f->name);
	}
}

static void print_first(void)
{
	struct fox *f = list_first_entry(&fox_list, struct fox, list);
	pr_info("list_first_entry: %s\n", f->name);
}

static void print_last(void)
{
	struct fox *f = list_last_entry(&fox_list, struct fox, list);
	pr_info("list_last_entry: %s\n", f->name);
}

static void print_all(void)
{
	struct fox *f = list_first_entry(&fox_list, struct fox, list);
	do {
		pr_info("%s: %s\n", __func__, f->name);
	} while (!list_is_last(&f->list, &fox_list) &&
		 (f = list_next_entry(f, list)));
}

static void print_all_reverse(void)
{
	struct fox *f = list_last_entry(&fox_list, struct fox, list);
	do {
		pr_info("%s: %s\n", __func__, f->name);
	} while (!list_is_first(&f->list, &fox_list) &&
		 (f = list_prev_entry(f, list)));
}

struct fox *alloc_fox(char *name)
{
	struct fox *fox;
	fox = kmalloc(sizeof(*fox), GFP_KERNEL);
	fox->name = kstrdup(name, GFP_KERNEL);
	INIT_LIST_HEAD(&fox->list);

	return fox;
}

static void clean(void)
{
	while (!list_empty(&fox_list)) {
		struct fox *f = list_first_entry_or_null(&fox_list, struct fox, list);
		if (f) {
			kfree(f->name);
			list_del(&f->list);
		}
	}
}

static int __init listest_init(void)
{
	struct fox *red_fox = alloc_fox("red fox");
	struct fox *green_fox = alloc_fox("green fox");
	struct fox *blue_fox = alloc_fox("blue fox");
	list_add_tail(&red_fox->list, &fox_list);
	if (list_is_singular(&fox_list))
		pr_info("list_is_singular\n");
	list_add_tail(&green_fox->list, &fox_list);
	list_add_tail(&blue_fox->list, &fox_list);

	print_first_and_last();

	print_first();
	print_last();

	print_all();
	print_all_reverse();

	return 0;
}

static void __exit listest_exit(void)
{
	clean();
	BUG_ON(!list_empty(&fox_list));
}

module_init(listest_init);
module_exit(listest_exit);
MODULE_LICENSE("GPL");
