#include <linux/module.h>
#include <linux/slab.h>
#include <linux/list.h>

struct fox {
	char *name;
	int id;
	struct list_head list;
};

static LIST_HEAD(fox_list1);
static LIST_HEAD(fox_list2);

struct fox *alloc_add_fox(char *name, int id)
{
	struct fox *fox;

	fox = kmalloc(sizeof(*fox), GFP_KERNEL);
	fox->name = kstrdup(name, GFP_KERNEL);
	fox->id = id;
	INIT_LIST_HEAD(&fox->list);
	list_add_tail(&fox->list, &fox_list1);

	return fox;
}

static void print_all(void)
{
	struct fox *f;

	pr_info(">>>>>>>>>>>>>>>>>>>>>>>\n");
	list_for_each_entry(f, &fox_list1, list)
		pr_info("list1: %s %d\n", f->name, f->id);

	list_for_each_entry(f, &fox_list2, list)
		pr_info("list2: %s %d\n", f->name, f->id);
	pr_info("<<<<<<<<<<<<<<<<<<<<<<<<\n");
}

static void move(void)
{
	struct fox *f, *n;

	list_for_each_entry_safe(f, n, &fox_list1, list) {
		if (f->id < 5)
			list_move_tail(&f->list, &fox_list2);
	}
	pr_info("%s\n", __func__);
}

static void splice(void)
{
	list_splice_tail_init(&fox_list1, &fox_list2);
	pr_info("%s\n", __func__);
}

static void cut(void)
{
	struct fox *f;

	list_for_each_entry(f, &fox_list2, list) {
		if (f->id == 4)
			break;
	}
	list_cut_position(&fox_list1, &fox_list2, &f->list);
	pr_info("%s\n", __func__);
}

static void replace(void)
{
	struct fox *old, *new;

	old = list_last_entry(&fox_list2, struct fox, list);
	new = kmalloc(sizeof(*new), GFP_KERNEL);
	new->name = kstrdup("new", GFP_KERNEL);
	new->id = 100;
	INIT_LIST_HEAD(&new->list);

	list_replace(&old->list, &new->list);
	pr_info("%s\n", __func__);
}

static void rotate(void)
{
	list_rotate_left(&fox_list1);
	pr_info("%s\n", __func__);
}

static void clean(void)
{
	struct fox *f;

	while (!list_empty(&fox_list1)) {
		f = list_first_entry(&fox_list1, struct fox, list);
		kfree(f->name);
		list_del(&f->list);
	}

	while (!list_empty(&fox_list2)) {
		f = list_first_entry(&fox_list2, struct fox, list);
		kfree(f->name);
		list_del(&f->list);
	}
}

static int __init test_init(void)
{
	int i;
	char name[10];

	for (i = 0; i < 10; i++) {
		sprintf(name, "fox-%d", i);
		alloc_add_fox(name, i);
	}

	print_all();

	move();
	print_all();

	splice();
	print_all();

	cut();
	print_all();

	replace();
	print_all();

	rotate();
	print_all();

	return 0;
}

static void __exit test_exit(void)
{
	clean();
	WARN_ON(!list_empty(&fox_list1));
	WARN_ON(!list_empty(&fox_list2));
}

module_init(test_init);
module_exit(test_exit);
MODULE_LICENSE("GPL");
