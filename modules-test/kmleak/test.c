#include <linux/init.h>
#include <linux/module.h>
#include <linux/slab.h>

static int leak_func(void)
{
	char *p;
	char mark[] = "memory leak test";

	p = kmalloc(sizeof(mark), GFP_KERNEL);
	if (!p) {
		pr_err("kmleak can't allocate memory\n");
		return -ENOMEM;
	}
	pr_info("pointer to the allocated memory: %p\n", p);
	memcpy(p, &mark, sizeof(mark));

	if (!p)
		kfree(p);

	return 0;
}

static int __init test_init(void)
{
	leak_func();
	return 0;
}

static void __exit test_exit(void)
{
}

module_init(test_init);
module_exit(test_exit);
MODULE_LICENSE("GPL");
