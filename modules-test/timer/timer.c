/* 
 * http://www.cs.bham.ac.uk/~exr/lectures/opsys/12_13/examples/
 */
#include <linux/module.h>  /* Needed by all modules */
#include <linux/kernel.h>  /* Needed for KERN_ALERT */
#include <linux/timer.h>   /* Needed for timer */
#include <asm/spinlock.h>

DEFINE_SPINLOCK(my_lock);	/* the lock for the counter */

/* setup timer */
struct timer_list myTimer;
int i;

void timerFun (unsigned long arg)
{
	int tmp;
	/* In this simple example, locking is an overkill - this operation should finish within a second  - used here to demonstrate how it works */
	spin_lock(&my_lock);
	i++;
	tmp = i;
	spin_unlock(&my_lock);
	printk(KERN_ALERT "Called timer %d times\n", tmp); 
	mod_timer(&myTimer, jiffies + HZ);	/* setup the timer again */
}

static int __init timer_init(void)
{
	/* pre-defined kernel variable jiffies gives current value of ticks */
	unsigned long currentTime = jiffies; 
	unsigned long expiryTime = currentTime + HZ; /* HZ gives number of ticks per second */

	init_timer(&myTimer);
	myTimer.function = timerFun;
	myTimer.expires = expiryTime;
	myTimer.data = 0;

	add_timer(&myTimer);
	printk(KERN_INFO "timer added\n");
	return 0;
}

static void __exit timer_cleanup(void)
{
	if (!del_timer(&myTimer))
		printk(KERN_INFO "Couldn't remove timer!!\n");
	else
		printk(KERN_INFO "timer removed\n");
}

module_init(timer_init);
module_exit(timer_cleanup);

MODULE_LICENSE("GPL");
MODULE_AUTHOR ("Eike Ritter <E.Ritter@cs.bham.ac.uk>");
MODULE_DESCRIPTION ("Simple timer usage") ;
