#include <linux/init.h>
#include <linux/module.h>
#include <linux/hash.h>
#include <linux/random.h>

static int __init test_init(void)
{
	int bits = 3;
	int elem_num = 1 << bits;
	int vec[elem_num];
	int i;
       	
	for (i = 0; i < elem_num; ++i)
		vec[i] = 0;

	for (i = 0; i < 10240; i++) {
		unsigned int num = prandom_u32();
		int elem_idx = hash_long(num, bits);
		vec[elem_idx]++;
		pr_info("%10u->%d\n", num, elem_idx);
	}

	for (i = 0; i < elem_num; ++i)
		pr_info("vec[%d]: %d\n", i, vec[i]);

	return 0;
}

static void __exit test_exit(void)
{
}

module_init(test_init);
module_exit(test_exit);

MODULE_LICENSE("GPL");
