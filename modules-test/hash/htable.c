#include <linux/init.h>
#include <linux/module.h>
#include <linux/slab.h>
#include <linux/hashtable.h>
#include <linux/random.h>

#define FOO_BITS 2
static DEFINE_HASHTABLE(foo_table, FOO_BITS);

struct foo {
	struct hlist_node node;
	unsigned int num;
};

static int __init test_init(void)
{
	int i;

	hash_init(foo_table);
	for (i = 0; i < 100; i++) {
		struct foo *f = kzalloc(sizeof(*f), GFP_KERNEL);
		INIT_HLIST_NODE(&f->node);
		f->num = prandom_u32();
		hash_add(foo_table, &f->node, f->num);
	}

	return 0;
}

static void __exit test_exit(void)
{
	struct foo *f;
	int i = 0;

	hash_for_each(foo_table, i, f, node) {
		pr_info("%d %u\n", i, f->num);
		__hlist_del(&f->node);
		kfree(f);
	}
}

module_init(test_init);
module_exit(test_exit);

MODULE_LICENSE("GPL");
