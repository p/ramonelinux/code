#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/rbtree.h>
#include <linux/slab.h>

struct data_struct {
	struct rb_node rb_node;
	int key;
};

struct rb_root_cached my_root = RB_ROOT_CACHED;

struct data_struct *rb_find(struct rb_root_cached *root, int key)
{
	struct rb_node *n = root->rb_root.rb_node;
	struct data_struct *data;

	while (n) {
		data = rb_entry(n, struct data_struct, rb_node);
		if (key < data->key)
			n = n->rb_left;
		else if (key > data->key)
			n = n->rb_right;
		else
			return data;
	}
	return NULL;
}

int rb_add(struct rb_root_cached *root, struct data_struct *data)
{
	struct rb_node **p = &root->rb_root.rb_node;
	struct rb_node *parent = NULL;
	struct data_struct *__data;
	bool leftmost = true;

	while (*p) {
		parent = *p;
		__data = rb_entry(parent, struct data_struct, rb_node);

		if (data->key < __data->key) {
			p = &((*p)->rb_left);
		} else if (data->key > __data->key) {
			p = &((*p)->rb_right);
			leftmost = false;
		} else {
			return false;
		}
	}

	rb_link_node(&data->rb_node, parent, p);
	rb_insert_color_cached(&data->rb_node, root, leftmost);

	return true;
}

void rb_del(struct rb_root_cached *root, struct data_struct *data)
{
	rb_erase_cached(&data->rb_node, root);
}

void print_all_next(struct rb_root_cached *root)
{
	struct rb_node *n;

	for (n = rb_first_cached(root); n; n = rb_next(n))
		pr_info("key=%d\n", rb_entry(n, struct data_struct, rb_node)->key);
}

void print_all_prev(struct rb_root_cached *root)
{
	struct rb_node *n;

	for (n = rb_last(&root->rb_root); n; n = rb_prev(n))
		pr_info("key=%d\n", rb_entry(n, struct data_struct, rb_node)->key);
}

void rbtest(void)
{
	struct data_struct *data;
	int i;

	for (i = 0; i < 10; i++) {
		data = kzalloc(sizeof(*data), GFP_ATOMIC);
		data->key = i;
		rb_add(&my_root, data);
	}

	print_all_next(&my_root);
	print_all_prev(&my_root);

	data = rb_find(&my_root, 2);
	if (data) {
		pr_info("find: %d\n", data->key);
		rb_del(&my_root, data);
		kzfree(data);
	} else {
		pr_err("not find\n");
	}

	print_all_next(&my_root);
	print_all_prev(&my_root);
}

static int __init rbtest_init(void)
{
	rbtest();
	return 0;
}

static void __exit rbtest_exit(void)
{
	pr_info("rbtest exit!\n");
}

module_init(rbtest_init);
module_exit(rbtest_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("foo");
MODULE_DESCRIPTION("A kernel rbtree test module");
