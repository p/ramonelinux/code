#include <linux/init.h>
#include <linux/module.h>
#include <linux/ftrace.h>

static void notrace hello_call(unsigned long ip,
			       unsigned long parent_ip,
			       struct ftrace_ops *op,
			       struct pt_regs *regs)
{
	unsigned long flags;

	local_irq_save(flags);
	//pr_info("hello\n");
	local_irq_restore(flags);
}

static struct ftrace_ops hello_ops __read_mostly = {
	.func	= hello_call,
};

static int __init hello_init(void)
{
	return register_ftrace_function(&hello_ops);
}

static void __exit hello_exit(void)
{
	unregister_ftrace_function(&hello_ops);
}

module_init(hello_init);
module_exit(hello_exit);
MODULE_LICENSE("GPL");
