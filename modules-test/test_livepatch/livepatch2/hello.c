#include <linux/init.h>
#include <linux/module.h>
#include <linux/irqflags.h>
#include <linux/ftrace.h>
#include <linux/seq_file.h>

static int hello_cmdline_proc_show(struct seq_file *m, void *v)
{
        seq_printf(m, "%s\n", "this has been live patched!");
        return 0;
}

static inline void arch_set_pc(struct pt_regs *regs, unsigned long ip)
{
        regs->ip = ip;
}

static void notrace hello_call(unsigned long ip,
			       unsigned long parent_ip,
			       struct ftrace_ops *op,
			       struct pt_regs *regs)
{
	unsigned long flags;

	local_irq_save(flags);
	arch_set_pc(regs, (unsigned long)hello_cmdline_proc_show);
	local_irq_restore(flags);
}

static struct ftrace_ops hello_ops __read_mostly = {
	.func	= hello_call,
	.flags	= FTRACE_OPS_FL_SAVE_REGS |
		  FTRACE_OPS_FL_DYNAMIC |
		  FTRACE_OPS_FL_IPMODIFY,
};

static int __init hello_init(void)
{
	int ret;
	char *old_name = "cmdline_proc_show";
	unsigned long old_addr = kallsyms_lookup_name(old_name);

	ret = ftrace_set_filter_ip(&hello_ops, old_addr, 0, 0);
	if (ret) {
		pr_err("failed to set ftrace filter\n");
		return -1;
	}
	return register_ftrace_function(&hello_ops);
}

static void __exit hello_exit(void)
{
	unregister_ftrace_function(&hello_ops);
}

module_init(hello_init);
module_exit(hello_exit);
MODULE_LICENSE("GPL");
