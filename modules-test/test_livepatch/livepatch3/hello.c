#include <linux/init.h>
#include <linux/module.h>
#include <linux/ftrace.h>
#include <linux/kallsyms.h>

#include <linux/seq_file.h>
static int livepatch_cmdline_proc_show(struct seq_file *m, void *v)
{
	seq_printf(m, "%s\n", "this has been live patched!");
	return 0;
}

static inline void arch_set_pc(struct pt_regs *regs, unsigned long ip)
{
	regs->ip = ip;
}

static void notrace klp_ftrace_handler(unsigned long ip,
				       unsigned long parent_ip,
				       struct ftrace_ops *fops,
				       struct pt_regs *regs)
{
	rcu_read_lock();
	arch_set_pc(regs, (unsigned long)livepatch_cmdline_proc_show);
	rcu_read_unlock();
}

static struct ftrace_ops fops;

struct klp_find_arg {
	const char *objname;
	const char *name;
	unsigned long addr;
	/*
	 * If count == 0, the symbol was not found. If count == 1, a unique
	 * match was found and addr is set.  If count > 1, there is
	 * unresolvable ambiguity among "count" number of symbols with the same
	 * name in the same object.
	 */
	unsigned long count;
};

static int klp_find_callback(void *data, const char *name,
			     struct module *mod, unsigned long addr)
{
	struct klp_find_arg *args = data;

	if ((mod && !args->objname) || (!mod && args->objname))
		return 0;

	if (strcmp(args->name, name))
		return 0;

	if (args->objname && strcmp(args->objname, mod->name))
		return 0;

	/*   
	 * args->addr might be overwritten if another match is found
	 * but klp_find_object_symbol() handles this and only returns the
	 * addr if count == 1.
	 */
	args->addr = addr;
	args->count++;

	return 0;
}

static int __init hello_init(void)
{
	int ret;
	char *old_name = "cmdline_proc_show";

        struct klp_find_arg args = {
                .objname = NULL,
                .name = old_name,
                .addr = 0,
                .count = 0
        };
	kallsyms_on_each_symbol(klp_find_callback, &args);

	fops.func = klp_ftrace_handler;
	fops.flags = FTRACE_OPS_FL_SAVE_REGS |
		     FTRACE_OPS_FL_DYNAMIC |
		     FTRACE_OPS_FL_IPMODIFY;

	ret = ftrace_set_filter_ip(&fops, args.addr, 0, 0);
	if (ret) {
		pr_err("failed to set ftrace filter for function '%s' (%d)\n",
			old_name, ret);
		return ret;
	}

	return register_ftrace_function(&fops);
}

static void __exit hello_exit(void)
{
	unregister_ftrace_function(&fops);
}

module_init(hello_init);
module_exit(hello_exit);
MODULE_LICENSE("GPL");
