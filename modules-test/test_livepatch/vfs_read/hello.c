#include <linux/init.h>
#include <linux/module.h>
#include <linux/ftrace.h>
#include <linux/kallsyms.h>
#include <linux/fs.h>

ssize_t my_read(struct file *file, char __user *buf, size_t count, loff_t *pos)
{
	return __vfs_read(file, buf, count, pos);
}

static inline void arch_set_pc(struct pt_regs *regs, unsigned long ip)
{
	regs->ip = ip;
}

static void notrace klp_ftrace_handler(unsigned long ip,
				       unsigned long parent_ip,
				       struct ftrace_ops *fops,
				       struct pt_regs *regs)
{
	rcu_read_lock();
	arch_set_pc(regs, (unsigned long)my_read);
	rcu_read_unlock();
}

static struct ftrace_ops fops;

static int __init hello_init(void)
{
	int ret;
	char *old_name = "vfs_read";
	unsigned long old_addr = kallsyms_lookup_name(old_name);

	fops.func = klp_ftrace_handler;
	fops.flags = FTRACE_OPS_FL_SAVE_REGS |
		     FTRACE_OPS_FL_DYNAMIC |
		     FTRACE_OPS_FL_IPMODIFY;

	if (!old_addr)
		return -1;

	ret = ftrace_set_filter_ip(&fops, old_addr, 0, 0);
	if (ret) {
		pr_err("failed to set ftrace filter for function '%s' (%d)\n",
			old_name, ret);
		return ret;
	}

	return register_ftrace_function(&fops);
}

static void __exit hello_exit(void)
{
	unregister_ftrace_function(&fops);
}

module_init(hello_init);
module_exit(hello_exit);
MODULE_LICENSE("GPL");
