#include <linux/init.h>
#include <linux/module.h>
#include <linux/slab.h>
#include <linux/workqueue.h>
#include <linux/sched.h>
#include <linux/jiffies.h>

struct my_data {
	struct work_struct my_work;
	int val;
};

static void work_func(struct work_struct *work)
{
	struct my_data *d = container_of(work, struct my_data, my_work);
	int i;
	unsigned long j;

	for (i = 0; i < d->val; i++) {
		j = jiffies + HZ;

		while (time_before(jiffies, j))
			schedule();

		pr_info("i: %d\n", i);
	}
	kfree(d);
	pr_info("End work\n");
}

static int __init work_init(void)
{
	struct my_data *d = kmalloc(sizeof(struct my_data), GFP_KERNEL);
	d->val = 10;
	INIT_WORK(&d->my_work, work_func);
	schedule_work(&d->my_work);

	pr_info("Begin work\n");

	return 0;
}

static void __exit work_exit(void)
{
}

module_init(work_init);
module_exit(work_exit);
MODULE_LICENSE("GPL");
