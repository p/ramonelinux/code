/*
 * http://blog.csdn.net/bullbat/article/details/7410563
 */
#include <linux/init.h>
#include <linux/module.h>
#include <linux/slab.h>

/*测试数据结构*/
struct my_data {
	struct work_struct my_work;
	int value;
};

struct workqueue_struct *wq = NULL;
struct work_struct work_queue;

/*初始化我们的测试数据*/
struct my_data *init_data(struct my_data *md)
{
	md = kmalloc(sizeof(struct my_data), GFP_KERNEL);
	md->value = 1;
	md->my_work = work_queue;
	return md;
}

/*工作队列函数*/
static void work_func(struct work_struct *work)
{
	struct my_data *md = container_of(work, struct my_data, my_work);

	pr_err("The value of my data is:%d\n", md->value);
}

static int __init work_init(void)
{
	struct my_data *md1 = NULL;
	struct my_data *md2 = NULL;

	md1 = init_data(md1);
	md1->value = 10;
	md2 = init_data(md2);
	md2->value = 20;

	/* 第一种方式：
	 * 使用统默认的workqueue队列 - system_wq，直接调度 
	 * 实现为queue_work(system_wq, work) */
	INIT_WORK(&md1->my_work, work_func);
	schedule_work(&md1->my_work);

	/* 第二种方式：
	 * 创建自己的工作队列，
	 * 加入工作到工作队列（加入内核就对其调度执行） */
	wq = create_workqueue("my_wq");
	INIT_WORK(&md2->my_work, work_func);
	queue_work(wq, &md2->my_work);

	return 0;
}

static void __exit work_exit(void)
{
	/*工作队列销毁*/
	destroy_workqueue(wq);
}

module_init(work_init);
module_exit(work_exit);

MODULE_AUTHOR("Mike Feng");
MODULE_LICENSE("GPL");
