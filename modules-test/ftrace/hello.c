#include <linux/init.h>
#include <linux/module.h>
#include <linux/ftrace.h>
#include <linux/proc_fs.h>
#include <linux/string.h>

static int counter;
static char buffer[1024];

static void notrace hello_call(unsigned long ip,
			       unsigned long parent_ip,
			       struct ftrace_ops *op,
			       struct pt_regs *regs)
{
	unsigned long flags;

	local_irq_save(flags);
	counter++;
	memset(buffer, 0, sizeof(buffer));
	sprintf(buffer, "[%d] %08lx  %08lx  %pf <- %pF\n", counter,
		ip, parent_ip, (void *)ip, (void *)parent_ip);
	local_irq_restore(flags);
}

static struct ftrace_ops hello_ops __read_mostly = {
	.func	= hello_call,
};

static ssize_t hello_read(struct file *file, char __user *buf,
			  size_t len, loff_t *offset)
{
	return simple_read_from_buffer(buf, len, offset,
			buffer, sizeof(buffer));
}

static const struct file_operations hello_fops = {
	.owner          = THIS_MODULE,
	.read           = hello_read,
};

static int __init hello_init(void)
{
	struct proc_dir_entry *entry;

	entry = proc_create_data("hello", S_IFREG | S_IRUGO,
				 NULL, &hello_fops, NULL);

	return register_ftrace_function(&hello_ops);
}

static void __exit hello_exit(void)
{
	unregister_ftrace_function(&hello_ops);

	remove_proc_entry("hello", NULL);
}

module_init(hello_init);
module_exit(hello_exit);
MODULE_LICENSE("GPL");
