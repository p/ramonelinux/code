#include <multiboot.h>
#include <printk.h>
#include <traps.h>
#include <timer.h>
#include <pmm.h>
#include <vmm.h>
#include <kmalloc.h>
#include <sched.h>
#include <task.h>
#include <initrd.h>

char kern_stack[STACK_SIZE] __attribute__((aligned(16)));
uint32_t kern_stack_top;

multiboot_info_t *glb_mboot_ptr;
uint32_t initrd_location;

void start_kernel()
{
    kern_stack_top = ((uint32_t)kern_stack + STACK_SIZE);
    asm volatile("movl %0, %%esp\n\t"
                  "xorl %%ebp, %%ebp" : : "r" (kern_stack_top));

    glb_mboot_ptr = mboot_ptr_tmp + PAGE_OFFSET;

    /* Clear the screen. */
    cls();
    printk("hello, kernel\n");
    
    trap_init();
    timer_init(200);

    asm volatile("int $0x0");
    asm volatile("int $0x4");

    //multiboot_info();
    initrd_location = get_initrd_location();

    printk("kernel in memory start: 0x%x\n", kern_start);
    printk("kernel in memory end: 0x%x\n", kern_end);
    printk("kernel in memory used: %d KB\n", (kern_end - kern_start + 1023) / 1024);

    pmm_init();
    vmm_init();
    kmalloc_init();
    sched_init();
    fs_root = initrd_init(initrd_location);

    enable_intr();

    initrd_test();
    page_alloc_test();
    kmalloc_test();
    thread_test();

    while(1) {
        asm volatile("hlt");
    }
}
