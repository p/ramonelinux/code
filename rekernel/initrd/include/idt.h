#ifndef __IDT_H__
#define __IDT_H__

#include <types.h>

#define IDT_LENGTH  256

typedef struct idt_entry_s {
    uint16_t base_low;
    uint16_t selector;
    uint8_t always0;
    uint8_t flags;
    uint16_t base_high;
} __attribute__((packed)) idt_entry_t;

// IDTR
typedef struct idt_ptr_s {
    uint16_t limit;
    uint32_t base;
} __attribute__((packed)) idt_ptr_t;

typedef struct pt_regs_s {
    uint32_t ds;
    uint32_t edi;
    uint32_t esi;
    uint32_t ebp;
    uint32_t esp;
    uint32_t ebx;
    uint32_t edx;
    uint32_t ecx;
    uint32_t eax;
    uint32_t int_no;
    uint32_t err_code;
    uint32_t eip;
    uint32_t cs;
    uint32_t eflags;
    uint32_t useresp;
    uint32_t ss;
} pt_regs_t;

typedef void (*interrupt_handler_t)(pt_regs_t *);

void idt_init();
void register_interrupt_handler(uint8_t num, interrupt_handler_t handler);
void isr_handler(pt_regs_t *regs);

void irq_handler(pt_regs_t *regs);

void idt_flush();
void enable_intr();
void disable_intr();

#endif /* __IDT_H__ */
