#include <types.h>

static inline void outb(uint8_t value, uint16_t port)
{
    asm volatile("outb %0, %1" : : "a" (value), "dN" (port));
}
