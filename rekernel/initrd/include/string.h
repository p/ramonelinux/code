#include <types.h>

void memcpy(uint8_t *dest, const uint8_t *src, uint32_t len);
void bzero(void *dest, uint32_t len);
int strcmp(const char *str1, const char *str2);
char *strcpy(char *dest, const char *src);
int strlen(const char *src);
