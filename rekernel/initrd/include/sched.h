#ifndef __SCHED_H__
#define __SCHED_H__

#include <task.h>

extern struct task_struct *running_proc_head;
extern struct task_struct *wait_proc_head;
extern struct task_struct *current;

void sched_init();
void schedule();
void change_task_to(struct task_struct *next);
void switch_to(struct context *prev, struct context *next);

#endif /* __SCHED_H__ */
