#ifndef __KMALLOC_H__
#define __KMALLOC_H__

#include <types.h>

#define HEAP_START 0xE0000000

typedef struct header {
    struct header *prev;
    struct header *next;
    uint32_t allocated : 1;
    uint32_t length : 31;
} header_t;

void kmalloc_init();
void *kmalloc(uint32_t len);
void kfree(void *p);
void kmalloc_test();

#endif /* __KMALLOC_H__ */
