mkdir mnt
dd if=/dev/zero of=initrd bs=4096 count=100
mkfs.ext2 initrd
mount initrd mnt/
echo "hello, initrd!" > hello
cp hello mnt/
mkdir mnt/dir
umount mnt
rm -rf mnt hello

