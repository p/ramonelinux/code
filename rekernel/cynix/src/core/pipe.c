/*
 *  core/pipe.c
 *
 *  Copyright (C) 2010 stateless
 */

#include <pipe.h>
#include <ext2.h>
#include <common.h>
#include <errno.h>
#include <string.h>
#include <heap.h>
#include <x86.h>
#include <tss.h>

extern spinlock_t flist_lock;
extern struct list_head flist;

static int
pipealloc(void)
{
	struct file_t *f;
	int i;

	acquire(&flist_lock);
	for (i = 3; i < NR_MAX_OPEN_FILES; ++i) {
		if (curr_proc->fdtable[i]
				&& curr_proc->fdtable[i]->f_state != FILE_NONE)
			continue;
		if (!curr_proc->fdtable[i]) {
			f = kmalloc(sizeof(struct file_t));
			if (IS_ERR(f)) {
				release(&flist_lock);
				return PTR_ERR(f);
			}
			f->f_state = FILE_NONE;
			list_add_tail(&f->f_listopen, &flist);
			curr_proc->fdtable[i] = f;
		}
		f = curr_proc->fdtable[i];
		assert(f->f_state == FILE_NONE);
		f->f_refcount = 1;
		f->f_off = 0;
		f->f_state = FILE_ALLOC;
		f->f_type = FILE_PIPE;
		f->f_inode = NULL;
		f->f_inode_nr = 0;
		curr_proc->pipebufs[i] = NULL;
		release(&flist_lock);
		return i;
	}
	release(&flist_lock);
	return -EMFILE;
}

int
pipe(int pipefd[2])
{
	int ret;
	char *buf;

	pipefd[0] = pipealloc();
	if (pipefd[0] < 0)
		return pipefd[0];
	pipefd[1] = pipealloc();
	if (pipefd[1] < 0) {
		ret = pipefd[1];
		goto err;
	}
	buf = kmalloc(PIPE_SIZ);
	if (IS_ERR(buf)) {
		ret = PTR_ERR(buf);
		goto err1;
	}
	curr_proc->pipebufs[pipefd[0]] = buf;
	curr_proc->pipebufs[pipefd[1]] = buf;
	return 0;
err1:
	fileclose(pipefd[1]);
err:
	fileclose(pipefd[0]);
	return ret;
}

/* must be called with flist_lock acquired */
ssize_t
piperead(int fd, void *buf, size_t count)
{
	size_t i;
	struct file_t *f = NULL;
	char *pbuf;

	pbuf = curr_proc->pipebufs[fd];
	for (i = 0; i < NR_MAX_OPEN_FILES; ++i)
		if (curr_proc->pipebufs[i] == pbuf && i != (size_t)fd) {
			f = curr_proc->fdtable[i];
			break;
		}
	if (!f) f = curr_proc->fdtable[i];
	for (i = 0; i < count && i < (size_t)f->f_off; ++i) {
		((char *)buf)[i] = *pbuf;
		memcpy(pbuf, pbuf + 1, PIPE_SIZ - 1);
	}
	f->f_off -= i;
	return i;
}

/* must be called with flist_lock acquired */
ssize_t
pipewrite(int fd, const void *buf, size_t count)
{
	size_t i;
	char *pbuf;
	ssize_t r;

	pbuf = curr_proc->pipebufs[fd];
	for (i = 0; i < NR_MAX_OPEN_FILES; ++i)
		if (curr_proc->pipebufs[i] == pbuf && i != (size_t)fd)
			break;
	/* read end has been closed */
	if (i != NR_MAX_OPEN_FILES
			&& curr_proc->fdtable[i]->f_state == FILE_NONE)
		return -EPIPE;
	for (i = 0; i < count && curr_proc->fdtable[fd]->f_off + i < PIPE_SIZ; ++i)
		pbuf[curr_proc->fdtable[fd]->f_off + i] = ((char *)buf)[i];
	curr_proc->fdtable[fd]->f_off += i;
	r = i;
	for (i = 0; i < NR_MAX_OPEN_FILES; ++i)
		if (curr_proc->pipebufs[i] == pbuf && i != (size_t)fd)
			curr_proc->fdtable[i]->f_off = curr_proc->fdtable[fd]->f_off;
	return r;
}

