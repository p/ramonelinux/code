/*
 *  core/pic.c
 *
 *  Copyright (C) 2009 stateless
 */

#include <pic.h>
#include <x86.h>
#include <common.h>

enum {
	BASE_IRQ_PIC1 = 0x20,
	BASE_IRQ_PIC2 = BASE_IRQ_PIC1 + 8
};

void
remap_pic(void)
{
	/* ICW 1 */
	outb(0x20, 0x11);
	outb(0xa0, 0x11);

	/* ICW  2 */
	outb(0x21, BASE_IRQ_PIC1);
	outb(0xa1, BASE_IRQ_PIC2);

	/* ICW 3 */
	outb(0x21, 0x4);
	outb(0xa1, 0x2);

	/* ICW 4 */
	outb(0x21, 0x1);
	outb(0xa1, 0x1);

	/* nullify the data registers */
	outb(0x21, 0x0);
	outb(0xa1, 0x0);
}

void
enable_irq(uint32_t irq)
{
	uint8_t imr;

	imr = (irq <= 7) ? inb(0x21) : inb(0xa1);
	irq %= 8;
	imr &= ~(1 << irq);
	if (irq <= 7) {
		outb(0x21, imr);
		return;
	}
	outb(0xa1, imr);
}

void
disable_irq(uint32_t irq)
{
	uint8_t imr;

	imr = (irq <= 7) ? inb(0x21) : inb(0xa1);
	irq %= 8;
	imr |= (1 << irq);
	if (irq <= 7) {
		outb(0x21, imr);
		return;
	}
	outb(0xa1, imr);
}

void
pic_eoi(uint32_t int_no)
{
	if (int_no > 31) {
		if (int_no >= 40)
			outb(0xa0, 0x20);
		outb(0x20, 0x20);
	}
}

