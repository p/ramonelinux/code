/*
 *  core/main.c
 *
 *  Copyright (C) 2009 stateless
 */

#include <tty.h>
#include <rtc.h>
#include <keyboard.h>
#include <common.h>
#include <x86.h>
#include <idt.h>
#include <gdt.h>
#include <pic.h>
#include <string.h>
#include <multiboot.h>
#include <elf.h>
#include <mm.h>
#include <heap.h>
#include <tss.h>
#include <syscall.h>
#include <errno.h>
#include <ext2.h>
#include <serial.h>
#include <pci.h>
#include <rtl8139.h>
#include <version.h>

extern struct fs_node_t *vfs_root;
extern struct task_t *current_task;
extern unsigned long int initial_esp;
extern unsigned long int addr_info;
extern uint8_t multiboot_header[0];
extern uint8_t __code[0];
extern uint8_t __data[0];
extern uint8_t __bss[0];
extern uint8_t __end[0];

multiboot_info_t *info_boot = NULL;
multiboot_header_t *mb_header = NULL;
uint32_t initrd_base;
uint32_t initrd_end;

static void syscall(void);
static void move_kernel_stack(uint32_t new_stack_addr);

int
main(void)
{
	int ret;

	serial_init();

	ccurr_col(BLACK, WHITE);
	clear_scr();

	info_boot = (multiboot_info_t *) addr_info;
	mb_header = (multiboot_header_t *)multiboot_header;
	if (mb_header->magic != MULTIBOOT_HEADER_MAGIC)
		panic("invalid multiboot header number");
	if (!(info_boot->flags & (1 << 3)))
		panic("modules can't be located!");
	if (!info_boot->mods_count)
		panic("can't locate initrd, modules count is 0!");

	initrd_base = *(uint32_t *)info_boot->mods_addr;
	initrd_end = *(uint32_t *)(info_boot->mods_addr + 4);

	if (prepare_elf() < 0)
		info("could not locate either the symbol table or the string table!\n");

	initialize_gdt();
	remap_pic();
	initialize_idt();
	init_rtc(100);
	initialize_keyboard();
	sti();

	if ((ret = init_mm()) < 0)
		kerror(-ret);
	if ((ret = init_vm()) < 0)
		kerror(-ret);

	move_kernel_stack(0xe0000000);

	if (!pci_init()) {
		pci_enumerate();
		if (rtl8139_init() < 0)
			info("failed to initialize the rtl8139\n");
	} else {
		info("pci failed to init\n");
	}

	ext2_mount((char *)initrd_base);

	if (strlen(REVISION) > 0)
		printf("Revision: %s\n", REVISION);
	printf("Built on %s by %s on host %s\n\n", DATE, USER, HOST);
	printf("Press F12 to drop into the kernel debugger.");
	update_cursor(cursor_y, cursor_x);
	if ((ret = init_tss()) < 0)
		kerror(-ret);
	kick_tss();
	panic("WTF?");
	return 0xdeadc0de;
}

static void
move_kernel_stack(uint32_t new_stack_addr)
{
	uint32_t curr_esp, i, *ptr, offset;

	GET_ESP(curr_esp);
	offset = (initial_esp - curr_esp);
	memcpy((void *)(new_stack_addr - offset), (const void *)curr_esp, offset);
	ptr = (uint32_t *)new_stack_addr;
	for (i = 0; i < offset; i += 4) {
		--ptr;
		if (*ptr < initial_esp && *ptr > curr_esp)
			*ptr = *ptr + (new_stack_addr - initial_esp);
	}
	initial_esp = new_stack_addr;
	new_stack_addr -= offset;
	PUT_ESP(new_stack_addr);
}

uint32_t
get_fnv_hash(const char *buf, size_t count, uint32_t hash)
{
	static uint32_t fnv_offset_basis = 2166136261;
	static uint32_t fnv_prime = 16777619;

	hash = (!hash) ? fnv_offset_basis : hash;
	while (count--) {
		hash ^= *buf++;
		hash *= fnv_prime;
	}

	return hash;
}

__attribute__ ((unused)) static void
syscall(void)
{
	char buf[256];
	uint32_t hash = 0;
	int fd, oldfd;
	ssize_t ret;

	oldfd = sys_open("/a.out", O_RDONLY, 0);
	if (oldfd < 0)
		kerror(-oldfd);
	if ((fd = sys_dup(oldfd)) < 0)
		kerror(-fd);
	sys_close(oldfd);
	do {
		ret = sys_read(fd, buf, 255);
		if (ret < 0) kerror(-ret);
		if (!ret) break;
		buf[ret] = '\0';
		hash = get_fnv_hash(buf, ret, hash);
		ret = sys_write(fd, buf, ret);
		if (ret < 0) kerror(-ret);
	} while (1);
	serial_dump("hash = 0x%08lx\n", hash);
	sys_close(fd);
}

