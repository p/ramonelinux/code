/*
 *  core/kdb.c
 *
 *  Copyright (C) 2010 stateless
 */

#include <common.h>
#include <kdb.h>
#include <tty.h>
#include <heap.h>
#include <string.h>
#include <errno.h>
#include <syscall.h>
#include <x86.h>
#include <pci.h>
#include <tss.h>
#include <stdlib.h>
#include <ctype.h>
#include <elf.h>
#include <rtl8139.h>
#include <pipe.h>
#include <rtc.h>

#define ENV_REPORT

enum {
	CMD_SIZE = 512
};

static void kdb_loop(void);

static int kdb_cmd_help(int argc, char **argv);
static int kdb_cmd_exit(int argc, char **argv);
static int kdb_cmd_reboot(int argc, char **argv);
static int kdb_cmd_lspci(int argc, char **argv);
static int kdb_cmd_ps(int argc, char **argv);
static int kdb_cmd_cpuid(int argc, char **argv);
static int kdb_cmd_heapdump(int argc, char **argv);
static int kdb_cmd_hexdump(int argc, char **argv);
static int kdb_cmd_write(int argc, char **argv);
static int kdb_cmd_symlookup(int argc, char **argv);
static int kdb_cmd_dumpregs(int argc, char **argv);
static int kdb_cmd_exec(int argc, char **argv);
static int kdb_cmd_ls(int argc, char **argv);
static int kdb_cmd_cat(int argc, char **argv);
static int kdb_cmd_dumpframe(int argc, char **argv);
static int kdb_cmd_macaddr(int argc, char **argv);
static int kdb_cmd_test(int argc, char **argv);
static int kdb_cmd_dumpmap(int argc, char **argv);

static struct trapframe_t cframe;
static char *cmd;

static struct {
	const char *name;
	const char *desc;
	int (*fnp)(int argc, char **argv);
} kdb_cmds[] = {
	{ "help", "Show this help menu.", kdb_cmd_help },
	{ "exit", "Quit the debugger.", kdb_cmd_exit },
	{ "reboot", "Reboot the machine.", kdb_cmd_reboot },
	{ "lspci", "Perform a PCI enumeration.", kdb_cmd_lspci },
	{ "ps", "Dump the scheduler's runqueue.", kdb_cmd_ps },
	{ "cpuid", "Dump CPU info.", kdb_cmd_cpuid },
	{ "heapdump", "Dump the kernel's heap allocation/deallocation lists.", kdb_cmd_heapdump },
	{ "hexdump", "Do a hexdump of a specific memory range.", kdb_cmd_hexdump },
	{ "write", "Write a specific value in a range of addresses.", kdb_cmd_write },
	{ "symlookup", "Match an address to a symbol name.", kdb_cmd_symlookup },
	{ "dumpregs", "Dump the system's registers.", kdb_cmd_dumpregs },
	{ "exec", "Execute an ELF file.", kdb_cmd_exec },
	{ "ls", "List directory contents.", kdb_cmd_ls },
	{ "cat", "Concatenate file and print.", kdb_cmd_cat },
	{ "dumpframe", "Dump the last saved trapframe.", kdb_cmd_dumpframe },
	{ "mac-addr", "Dump the mac-address.", kdb_cmd_macaddr },
	{ "dumpmap", "Dump memory mappings.", kdb_cmd_dumpmap },
	{ "test", "Testing routine.", kdb_cmd_test }
};

void
set_trapframe(struct trapframe_t *tframe)
{
	cframe = *tframe;
}

void
kdb_enter(void)
{
	cmd = kmalloc(CMD_SIZE);
	if (IS_ERR(cmd))
		return;
	memset(cmd, 0, CMD_SIZE);
	ccurr_col(BLACK, WHITE);
	clear_scr();
	printf("type help for a list of commands.\n");
	kdb_loop();
}

static char **
parse_cmd(int *argc)
{
	int f, i;
	char *p, **argv, *tmp, *arg;
	*argc = 0;

	for (f = 1, p = cmd; *p; ++p) {
		if (!f && *p == ' ') {
			++*argc;
			f = 1;
			continue;
		}
		if (f && *p != ' ')
			f = 0;
	}
	if (!f)
		++*argc;
	argv = kmalloc((*argc + 1) * sizeof(char *));
	if (IS_ERR(argv))
		panic("oom");
	for (p = cmd; *p; ++p, tmp = p)
		; /* nothing */
	for (p = cmd; *p; ++p)
		if (*p == ' ')
			*p = '\0';
	for (i = 0, f = 1, p = cmd; p < tmp; ++p) {
		if (!f && *p == '\0') {
			f = 1;
			argv[i++] = arg;
			continue;
		}
		if (f && *p) {
			f = 0;
			arg = p;
		}
	}
	if (!f)
		argv[i] = arg;
	argv[*argc] = NULL;
	return argv;
}

static char *
prepare_cmd(void)
{
	char *p;

	p = &cmd[CMD_SIZE - 1];
	while (p != cmd) {
		if (*p == '\n') {
			*p-- = '\0';
			continue;
		}
		--p;
	}
	while (*p != ' ') ++p;
	*p = '\0';
	return p;
}

static void
trimstr(char *s)
{
	char *ptr = s;

	while (*ptr == ' ') ++ptr;
	memmove(s, ptr, strlen(s) - (ptr - s));
	memset(s + strlen(s) - (ptr - s), 0, ptr - s);
	ptr = s + strlen(s) - 1;
	while (*ptr == ' ') --ptr;
	if (*ptr) {
		++ptr;
		*ptr = '\0';
	}
}

static void
kdb_loop(void)
{
	size_t i;
	char *p;
	ssize_t r;
	int argc, ret;
	char **argv;

	do {
again:
		update_cursor(cursor_y, 2);
		printf("> ");
		r = sys_read(0, cmd, CMD_SIZE - 1);
		cmd[r] = '\0';
		p = cmd;
		trimstr(p);
		for (; *p; ++p)
			if (!isspace(*p))
				break;
		if (*p == '\0')
			goto again;
		p = prepare_cmd();
		for (i = 0; i <
				sizeof(kdb_cmds) / sizeof(kdb_cmds[0]); ++i) {
			if (!strcmp(cmd, kdb_cmds[i].name)) {
				*p = ' ';
				argv = parse_cmd(&argc);
				if (!kdb_cmds[i].fnp)
					panic("%s: is not initialized properly!",
					      kdb_cmds[i].name);
				ret = kdb_cmds[i].fnp(argc, argv);
#ifdef ENV_REPORT
				if (ret < 0) {
					printf("FAIL: %d", ret);
					if (ret <= -1 && ret >= -44)
						printf(":%s", sys_errlist[-ret - 1]);
					putchar('\n');
				}
#endif
				kfree(argv);
				goto again;
			}
		}
		p = cmd;
		while (isspace(*p)) ++p;
		printf("%s: command not found\n", p);
	} while (1);
}

static int
kdb_cmd_help(int argc, char **argv)
{
	size_t i;

	if (argc != 1) {
		printf("usage: %s\n", argv[0]);
		return -EINVAL;
	}

	printf("Command list\n");
	for (i = 0; i < sizeof(kdb_cmds) / sizeof(kdb_cmds[0]); ++i)
		printf("  %s -- %s\n", kdb_cmds[i].name, kdb_cmds[i].desc);
	return 0;
}

static int
kdb_cmd_exit(int argc, char **argv)
{
	if (argc != 1) {
		printf("usage: %s\n", argv[0]);
		return -EINVAL;
	}
	sys_exit(0);
	return 0;
}

static int
kdb_cmd_reboot(int argc, char **argv)
{
	if (argc != 1) {
		printf("usage: %s\n", argv[0]);
		return -EINVAL;
	}
	outb(0x64, 0xfe);
	cli();
	hlt();
	return 0;
}

static int
kdb_cmd_lspci(int argc, char **argv)
{
	if (argc != 1) {
		printf("usage: %s\n", argv[0]);
		return -EINVAL;
	}
	lspci();
	return 0;
}

static int
kdb_cmd_ps(int argc, char **argv)
{
	if (argc != 1) {
		printf("usage: %s\n", argv[0]);
		return -EINVAL;
	}
	ps();
	return 0;
}

static int
kdb_cmd_cpuid(int argc, char **argv)
{
	if (argc != 1) {
		printf("usage: %s\n", argv[0]);
		return -EINVAL;
	}
	do_cpuid();
	return 0;
}

static int
kdb_cmd_heapdump(int argc, char **argv)
{
	if (argc != 1) {
		printf("usage: %s\n", argv[0]);
		return -EINVAL;
	}
	walk_heap_lists();
	return 0;
}

static int
kdb_cmd_hexdump(int argc, char **argv)
{
	uint32_t addr, len;

	if (argc != 3) {
		printf("usage: %s memory-address len\n", argv[0]);
		printf("NOTE: all arguments are specified in hex\n");
		return -EINVAL;
	}
	addr = strtoul(argv[1], NULL, 16);
	len = strtoul(argv[2], NULL, 16);
	hexdump((const void *)addr, len);
	return 0;
}

static int
kdb_cmd_write(int argc, char **argv)
{
	uint32_t val, low, high, *p;

	if (argc != 4) {
		printf("usage: %s value low-addr high-addr\n", argv[0]);
		printf("NOTE: the range is [low-addr, high-addr), all arguments are specified in hex\n");
		return -EINVAL;
	}
	val = strtoul(argv[1], NULL, 16);
	low = strtoul(argv[2], NULL, 16);
	high = strtoul(argv[3], NULL, 16);
	if (high <= low) {
		printf("high-addr can't be lower or equal to low-addr\n");
		return -EINVAL;
	}
	p = (uint32_t *)low;
	while (p < (uint32_t *)high)
		*p++ = val;
	return 0;
}

static int
kdb_cmd_symlookup(int argc, char **argv)
{
	uint32_t addr;
	const char *sym;

	if (argc != 2) {
		printf("usage: %s symbol-address\n", argv[0]);
		printf("NOTE: the address must be specified in hex\n");
		return -EINVAL;
	}
	addr = strtoul(argv[1], NULL, 16);
	sym = find_symbol(addr);
	printf("%s => %s\n", argv[1], (!sym) ? "unknown" : sym);
	return 0;
}

/* TODO: add more regs */
static int
kdb_cmd_dumpregs(int argc, char **argv)
{
	uint32_t eax, ebx, ecx, edx, esp, ebp;
	uint32_t edi, esi;

	if (argc != 1) {
		printf("usage: %s\n", argv[0]);
		return -EINVAL;
	}

	asm volatile ("movl %%eax, %0" : "=r"(eax));
	asm volatile ("movl %%ebx, %0" : "=r"(ebx));
	asm volatile ("movl %%ecx, %0" : "=r"(ecx));
	asm volatile ("movl %%edx, %0" : "=r"(edx));
	asm volatile ("movl %%esp, %0" : "=r"(esp));
	asm volatile ("movl %%ebp, %0" : "=r"(ebp));
	asm volatile ("movl %%esi, %0" : "=r"(esi));
	asm volatile ("movl %%edi, %0" : "=r"(edi));

	printf("eax = 0x%08lx\nebx = 0x%08lx\necx = 0x%08lx\n",
	       eax, ebx, ecx);
	printf("edx = 0x%08lx\nesp = 0x%08lx\nebp = 0x%08lx\n",
	       edx, esp, ebp);
	printf("esi = 0x%08lx\nedi = 0x%08lx\n",
	       esi, edi);
	return 0;
}

static int
kdb_cmd_exec(int argc, char **argv)
{
	pid_t pid;
	int ret, status;

	if (argc != 2) {
		printf("usage: %s filename\n", argv[0]);
		return -EINVAL;
	}

	pid = sys_fork();
	if (pid < 0)
		return pid;
	if (!pid) {
		ret = sys_execve(argv[1], NULL, NULL);
		sys_exit(ret);
	} else {
		while (1) {
			if ((ret = sys_waitpid(-1, &status, WNOHANG)) < 0)
				break;
		}
	}
	printf("child exited with error code %d\n", status);
	return 0;
}

static int
kdb_cmd_ls(int argc, char **argv)
{
	char buf[256];
	DIR *dirp;
	struct dirent_t *dirent;
	struct stat stbuf;
	int ret;

	if (argc != 1) {
		printf("usage: %s\n", argv[0]);
		return -EINVAL;
	}

	dirp = opendir("/");
	if (!dirp)
		return -errno;
	dirent = sys_readdir(dirp);
	printf("%-12s%-12s%-12s%-12s\n", "FILENAME", "TYPE", "UID", "GID");
	while (dirent) {
		if (!strcmp(dirent->d_name, ".")
				|| !strcmp(dirent->d_name, ".."))
			goto out;
		bzero(buf, 256);
		buf[0] = '/';
		strncpy(buf + 1, dirent->d_name, 255);
		buf[255] = '\0';
		if (!stat(buf, &stbuf)) {
			printf("%-12s%-12s%-12d%-12d\n",
			       dirent->d_name,
			       S_ISREG(stbuf.st_mode) ? "FILE" : "DIR",
			       stbuf.st_uid,
			       stbuf.st_gid);
		}
out:
		dirent = readdir(dirp);
	}
	ret = closedir(dirp);
	if (ret < 0)
		return -errno;
	return 0;
}

static int
kdb_cmd_cat(int argc, char **argv)
{
	int fd;
	int n;
	char buf[256];

	fd = open(argv[1], 0, 0);

	while ((n = read(fd, buf, 256)) > 0) {
		printf("%s\n", buf);
	}

	return 0;
}

static int
kdb_cmd_dumpframe(int argc, char **argv)
{
	if (argc != 1) {
		printf("usage: %s\n", argv[0]);
		return -EINVAL;
	}
	DUMP_STACK_REGS(cframe);
	return 0;
}

static int
kdb_cmd_macaddr(int argc, char **argv)
{
	const uint8_t *mac_addr;

	if (argc != 1) {
		printf("usage: %s\n", argv[0]);
		return -EINVAL;
	}
	mac_addr = get_mac_addr();
	if (!mac_addr) {
		info("no mac-addr?\n");
		return -ENODEV;
	}
	printf("%02x:%02x:%02x:%02x:%02x:%02x\n",
	       mac_addr[0],
	       mac_addr[1],
	       mac_addr[2],
	       mac_addr[3],
	       mac_addr[4],
	       mac_addr[5]
	      );
	return 0;
}

static int
kdb_cmd_dumpmap(int argc, char **argv)
{
	if (argc != 1) {
		printf("usage: %s\n", argv[0]);
		return -EINVAL;
	}
	dump_mappings();
	return 0;
}

static int
kdb_cmd_test(int argc, char **argv)
{
	ssize_t r;
	int pipefd[2];
	char buf[32];
	pid_t pid;

	if (argc != 1) {
		printf("usage: %s\n", argv[0]);
		return -EINVAL;
	}

	bzero(buf, sizeof buf);
	if (sys_pipe(pipefd) < 0)
		panic("sys_pipe failed");
	pid = sys_fork();
	if (!pid) {
		assert(!sys_close(pipefd[1]));
		sleep(1);
		r = sys_read(pipefd[0], buf, 13);
		if (r < 0)
			kerror(-r);
		if (r)
			printf("%s", buf);
		sys_exit(0);
	}
	assert(!sys_close(pipefd[0]));
	assert(sys_write(pipefd[1], "hello", 5) == 5);
	assert(sys_write(pipefd[1], ", world\n", 8) == 8);
	return 0;
}

