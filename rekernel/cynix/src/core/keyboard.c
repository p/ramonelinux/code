/*
 *  core/keyboard.c
 *
 *  Copyright (C) 2009 stateless
 */

#include <keyboard.h>
#include <x86.h>
#include <idt.h>
#include <common.h>
#include <kdb.h>
#include <tty.h>

enum { KBD_BUF_SIZE = 4096 };
enum { CMD_PORT = 0x64, DATA_PORT = 0x60 /* to/from the kbd */, STATUS_PORT = 0x64 };

/* special keys */
enum {
	LALT, RALT,
	LCTRL, RCTRL,
	LSHIFT, RSHIFT,
	F1, F2, F3, F4, F5, F6, F7, F8, F9, F10, F11, F12,
	CAPSLK, NUMLK, SCRLK, SYSRQ,
	ESC = 27,
	INSERT, DEL, HOME, END, PGUP, PGDN, LEFT, RIGHT, UP, DOWN,
	NUM_DOT, NUM_ENTER, NUM_PLUS, NUM_MINUS, NUM_MUL, NUM_DIV,
	NUM_0, NUM_1, NUM_2, NUM_3, NUM_4, NUM_5, NUM_6, NUM_7, NUM_8, NUM_9,
	BACKSP = 127
};

int f_kdb = 0;

static uint8_t kbd_buffer[KBD_BUF_SIZE]; /* plain ascii 0 - 127 */
static uint32_t head = 0, tail = 0;
static bool shift_st = 0;

static int keycodes_lower[] = {
	0, ESC, '1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '-', '=', BACKSP,      /* 0 - e   */
	'\t', 'q', 'w', 'e', 'r', 't', 'y', 'u', 'i', 'o', 'p', '[', ']', '\r',          /* f - 1c  */
	LCTRL, 'a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', ';', '\'', '`',              /* 1d - 29 */
	LSHIFT, '\\', 'z', 'x', 'c', 'v', 'b', 'n', 'm', ',', '.', '/', RSHIFT,          /* 2a - 36 */
	NUM_MUL, LALT, ' ', CAPSLK, F1, F2, F3, F4, F5, F6, F7, F8, F9, F10,             /* 37 - 44 */
	NUMLK, SCRLK, NUM_7, NUM_8, NUM_9, NUM_MINUS, NUM_4, NUM_5, NUM_6, NUM_PLUS,     /* 45 - 4e */
	NUM_1, NUM_2, NUM_3, NUM_0, NUM_DOT, SYSRQ, 0, 0, F11, F12,                      /* 4d - 58 */
	0, 0, 0, 0, 0, 0, 0,                                                             /* 59 - 5f */
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,                                  /* 60 - 6f */
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0                                   /* 70 - 7f */
};

static int keycodes_upper[] = {
	0, ESC, '!', '@', '#', '$', '%', '^', '&', '*', '(', ')', '_', '+', BACKSP,      /* 0 - e   */
	'\t', 'Q', 'W', 'E', 'R', 'T', 'Y', 'U', 'I', 'O', 'P', '{', '}', '\r',          /* f - 1c  */
	LCTRL, 'A', 'S', 'D', 'F', 'G', 'H', 'J', 'K', 'L', ':', '"', '~',               /* 1d - 29 */
	LSHIFT, '|', 'Z', 'X', 'C', 'V', 'B', 'N', 'M', '<', '>', '?', RSHIFT,           /* 2a - 36 */
	NUM_MUL, LALT, ' ', CAPSLK, F1, F2, F3, F4, F5, F6, F7, F8, F9, F10,             /* 37 - 44 */
	NUMLK, SCRLK, NUM_7, NUM_8, NUM_9, NUM_MINUS, NUM_4, NUM_5, NUM_6, NUM_PLUS,     /* 45 - 4e */
	NUM_1, NUM_2, NUM_3, NUM_0, NUM_DOT, SYSRQ, 0, 0, F11, F12,                      /* 4d - 58 */
	0, 0, 0, 0, 0, 0, 0,                                                             /* 59 - 5f */
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,                                  /* 60 - 6f */
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0                                   /* 70 - 7f */
};

static inline void
wait_cmd_buffer(void)
{
	while (inb(STATUS_PORT) & 0x2)
		;
}

static inline void
wait_result_buffer(void)
{
	while (!(inb(STATUS_PORT) & 0x1))
		;
}

static inline void
send_kbd_cmd(uint8_t byte)
{
	wait_cmd_buffer();
	outb(CMD_PORT, byte);
	wait_cmd_buffer();
}

static inline uint8_t
recv_kbd_res(void)
{
	uint8_t byte;
	wait_result_buffer();
	byte = inb(DATA_PORT);
	return byte;
}

void
keyboard_callback(__attribute__ ((unused)) struct trapframe_t *regs)
{
	wait_result_buffer();
	uint8_t scancode = inb(DATA_PORT);

	if (!(scancode & 0x80)) {
		if (tail != KBD_BUF_SIZE) {
			if (scancode == 0x2a)
				shift_st = 1;
			if (scancode == 0x58) {
				if (!f_kdb) {
					f_kdb = 1;
					set_trapframe(regs);
					assert(!IS_ERR(create_kthread("kdb", kdb_enter)));
					freeze_tasks();
				}
				return;
			}
			if (!is_tty_attached()) return;
			kbd_buffer[tail++] = (shift_st) ? keycodes_upper[scancode]
					     : keycodes_lower[scancode];
			if (scancode == 0x1c) kbd_buffer[tail - 1] = '\n';
			resume_task(tty_get_sleep_chan());
		}
	} else if (scancode == 0xaa)
		shift_st = 0;
}

int
kbd_getchar(void)
{
	int ch;
	uint32_t state;

	save_flags(&state);
	cli();
	if (head == tail) {
		if (tail == KBD_BUF_SIZE)
			head = tail = 0;
		load_flags(state);
		return -1;
	}
	ch = kbd_buffer[head];
	++head;
	load_flags(state);
	return ch;
}

void
initialize_keyboard(void)
{
	uint8_t byte;

	/* disable keyboard */
	send_kbd_cmd(0xad);

	/* perform a self-test */
	send_kbd_cmd(0xaa);
	if (recv_kbd_res() != 0x55)
		panic("keyboard self-test failed!");

	/* read the CCB */
	send_kbd_cmd(0x20);
	byte = recv_kbd_res();

	/* if keyboard translation is not enabled, try to enable it */
	if (!(byte & (1 << 6))) {
		info("translation is not enabled, attempting to enable it ...\n");
		byte |= (1 << 6);
		send_kbd_cmd(0x60);
		outb(DATA_PORT, byte);
		wait_cmd_buffer();
		send_kbd_cmd(0x20);
		if (!(recv_kbd_res() & (1 << 6))) {
			printf("failed!\n");
			hlt();
		}
		printf("OK!\n");
	}

	/* check if we are in polling mode and if so enable interrupts on IRQ1 */
	if (!(byte & 0x1)) {
		byte |= 0x1;
		send_kbd_cmd(0x60);
		outb(DATA_PORT, byte);
		wait_cmd_buffer();
	}

	/* enable keyboard */
	send_kbd_cmd(0xae);
	register_isr_handler(33, keyboard_callback);
}

