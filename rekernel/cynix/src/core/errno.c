/*
 *  core/errno.c
 *
 *  Copyright (C) 2009 stateless
 */

#include <errno.h>

int errno;

const char *sys_errlist [] = {
	"argument list too long",                           /* E2BIG */
	"permission denied",                                /* EACCES */
	"resource temporarily unavailable",                 /* EAGAIN */
	"bad file descriptor",                              /* EBADF */
	"bad message",                                      /* EBADMSG */
	"device or resource busy",                          /* EBUSY */
	"operation canceled",                               /* ECANCELED */
	"no child processes",                               /* ECHILD */
	"resource deadlock avoided",                        /* EDEADLK */
	"mathematics argument out of domain of function",   /* EDOM */
	"file exists",                                      /* EEXIST */
	"bad address",                                      /* EFAULT */
	"file too large",                                   /* EFBIG */
	"illegal byte sequence ",                           /* EILSEQ */
	"operation in progress",                            /* EINPROGRESS */
	"interrupted function call",                        /* EINTR */
	"invalid argument",                                 /* EINVAL */
	"input/output error",                               /* EIO */
	"is a directory",                                   /* EISDIR */
	"too many open files",                              /* EMFILE */
	"too many links",                                   /* EMLINK */
	"message too long",                                 /* EMSGSIZE */
	"filename too long",                                /* ENAMETOOLONG */
	"too many open files in system",                    /* ENFILE */
	"no such device",                                   /* ENODEV */
	"no such file or directory",                        /* ENOENT */
	"exec format error",                                /* ENOEXEC */
	"no locks available",                               /* ENOLCK */
	"not enough space",                                 /* ENOMEM */
	"no space left on device",                          /* ENOSPC */
	"function not implemented",                         /* ENOSYS */
	"not a directory",                                  /* ENOTDIR */
	"directory not empty",                              /* ENOTEMPTY */
	"operation not supported",                          /* ENOTSUP */
	"inappropriate I/O control",                        /* ENOTTY */
	"no such device or address",                        /* ENXIO */
	"operation not permitted",                          /* EPERM */
	"broken pipe",                                      /* EPIPE */
	"result too large",                                 /* ERANGE */
	"read-only file system",                            /* EROFS */
	"invalid seek",                                     /* ESPIPE */
	"no such process",                                  /* ESRCH */
	"connection timed out",                             /* ETIMEDOUT */
	"improper link"                                     /* EXDEV */
};

