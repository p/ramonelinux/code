/*
 *  core/gdt.c
 *
 *  Copyright (C) 2009 stateless
 */

#include <gdt.h>
#include <common.h>

extern void gdt_flush(addr_t);

static uint64_t gdt[] = {
	0x0000000000000000llu,
	0x00cf9a000000ffffllu,
	0x00cf92000000ffffllu,
	0x00cffa000000ffffllu,
	0x00cff2000000ffffllu
};
static struct gdt_selector_t gdt_sel;

void
initialize_gdt(void)
{
	gdt_sel.limit = (sizeof(struct gdt_selector_t) * len(gdt)) - 1;
	gdt_sel.base = (addr_t)gdt;
	gdt_flush((addr_t)&gdt_sel);
}

