/*
 *  core/mm.c
 *
 *  Copyright (C) 2009 stateless
 */

#include <mm.h>
#include <multiboot.h>
#include <common.h>
#include <string.h>
#include <idt.h>
#include <x86.h>
#include <heap.h>
#include <errno.h>
#include <tss.h>
#include <serial.h>

#define SET_PTE(pte, p, rw_mode, priv, frame_addr) \
	({ \
		pte.present = p; \
		pte.rw = rw_mode; \
		pte.privilege = priv; \
		pte.frame = frame_addr; \
	})

#define SET_PDE(pde, p, rw_mode, priv, pt) \
	({ \
		pde.present = p; \
		pde.rw = rw_mode; \
		pde.privilege = priv; \
		pde.pt_addr = pt; \
	})

#define PTABLE_BASE_ADDR 0xffc00000
#define PDIR_BASE_ADDR 0xfffff000

enum page_fl { P_FL = 1 << 0, RW_FL = 1 << 1, US_FL = 1 << 2 };

extern multiboot_info_t *info_boot;
extern uint32_t initrd_end;

struct page_directory_t *new_kernel_pdir = NULL;

static uint32_t mm_phys_addr = 0, mm_phys_addr_end = 0;
static uint8_t *framemap = NULL, *pagemap_pa = NULL;
static struct page_directory_t *kernel_page_dir = NULL;
static struct page_table_t *kernel_page_table = NULL;
static uint32_t curr_kernel_heap_addr = KERNEL_HEAP, total_frames = 0;

static inline uint32_t
get_frame_block(uint32_t frame)
{
	return frame / 8;
}

static inline uint32_t
get_frame_offset(uint32_t frame)
{
	return frame % 8;
}

static inline bool
is_frame_free(uint32_t frame)
{
	return !(framemap[get_frame_block(frame)] & (1 << get_frame_offset(frame)));
}

static inline uint32_t
get_page_block(uint32_t page)
{
	return page / 8;
}

static inline uint32_t
get_page_offset(uint32_t page)
{
	return page % 8;
}

static inline bool
is_page_free(uint32_t page)
{
	return !(pagemap_pa[get_page_block(page)] & (1 << get_page_offset(page)));
}

static inline void
enable_paging(void)
{
	uint32_t cr0;

	asm volatile ("movl %%cr0, %0": "=r"(cr0));
	cr0 |= 0x80000000;
	asm volatile ("movl %0, %%cr0":: "r"(cr0));
}

static inline void
disable_paging(void)
{
	uint32_t cr0;

	asm volatile ("movl %%cr0, %0": "=r"(cr0));
	cr0 &= 0x7fffffff;
	asm volatile ("movl %0, %%cr0":: "r"(cr0));
}

static inline uint32_t
get_fault_addr(void)
{
	uint32_t cr2;

	asm volatile ("movl %%cr2, %0" : "=r"(cr2));
	return cr2;
}

static uint32_t
get_free_frame(void)
{
	void *page;

	page = palloc(PAGE_SIZE);
	if (IS_ERR(page)) {
		errno = -PTR_ERR(page);
		return 0;
	}
	return (uint32_t)page;
}

static inline uint32_t
get_pte_index(uint32_t virt_addr)
{
	return (virt_addr & 0x3ff000) >> 12;
}

static inline uint32_t
get_pde_index(uint32_t virt_addr)
{
	return (virt_addr & PTABLE_BASE_ADDR) >> 22;
}

static inline struct page_directory_t *
curr_pdir(void) {
	return (struct page_directory_t *)PDIR_BASE_ADDR;
}

static inline struct page_table_t *
get_ptable(uint32_t index) {
	return (struct page_table_t *)(PTABLE_BASE_ADDR + index);
}

void
switch_page_dir(struct page_directory_t *pagedir)
{
	uint32_t phys;
	bool val = false;

	if (!pagedir)
		panic("invalid pagedir!");
	phys = virt_to_phys((uint32_t)pagedir, &val);
	if (!val)
		panic("virt_to_phys failed!");
	if (phys % PAGE_SIZE)
		panic("address of pagedir [0x%08lx] is not page aligned!",
		      (void *)pagedir);
	asm volatile ("movl %0, %%cr3":: "r"(phys));
	flush_tlb();
}

void
pagefault_callback(struct trapframe_t *regs)
{
	uint32_t errcode = regs->err_code, addr, page;
	int ret = -ENOMEM;

	addr = get_fault_addr();
	serial_dump("mm: faulting address is [0x%08lx]", addr);
	if (curr_proc) serial_dump(", task-id is [%lu]", curr_proc->pid);
	switch (errcode & 0x7) {
	case P_FL:
		panic("supervisory process tried to read a page and caused a protection fault");
		break;
	case RW_FL:
		page = get_free_frame();
		if (!page) kerror(errno);
		if ((ret = mmap(addr, page)) < 0)
			kerror(-ret);
		serial_dump(", found free frame at [0x%08lx]\n", page);
		break;
	case RW_FL | P_FL:
		panic("supervisory process tried to write a page and caused a protection fault");
		break;
	case US_FL:
		panic("user process tried to read a non-present page entry");
		break;
	case US_FL | P_FL:
		panic("user process tried to read a page and caused a protection fault");
		break;
	case US_FL | RW_FL:
		panic("user process tried to write to a non-present page entry");
		break;
	case US_FL | RW_FL | P_FL:
		panic("user process tried to write a page and caused a protection fault");
		break;
	default:
		panic("supervisory process tried to read a non-present page entry");
		break;
	}
}

/* retrieve the memory map from GRUB and initialize the physical memory allocator */
int
init_mm(void)
{
	int mm_phys_addr_valid = 0, ret = -EFAULT;
	unsigned long base_addr, i, framemap_size; /* in frames */
	memory_map_t *memmap;
	uint32_t state;

	save_flags(&state);
	cli();
	if (!info_boot) {
		ret = -EINVAL;
		goto err;
	}

	if (!(info_boot->flags & (1 << 6))) {
		ret = -ENOTSUP;
		goto err;
	}

	mm_phys_addr = (initrd_end + PAGE_SIZE) & ~(PAGE_SIZE - 1);
	base_addr = info_boot->mmap_addr;
	do {
		memmap = (memory_map_t *)base_addr;
		if (memmap->type == 1) {
			if (memmap->base_addr_low >= 0x100000) {
				if (!mm_phys_addr_valid && mm_phys_addr < memmap->base_addr_low
						+ memmap->length_low) {
					mm_phys_addr_valid = 1;
					total_frames = (memmap->base_addr_low + memmap->length_low
							- mm_phys_addr) >> PAGE_SHIFT;
					framemap_size = (total_frames >> 3);
					framemap_size = roundup_pagesize(framemap_size);
					framemap_size >>= PAGE_SHIFT;
					total_frames -= framemap_size;
					if ((total_frames >> 3) > memmap->length_low) {
						ret = -ENOMEM;
						goto err;
					}
					break;
				}
			}
		}
		base_addr += memmap->size + sizeof(uint32_t);
	} while (base_addr < info_boot->mmap_addr + info_boot->mmap_length);

	if (!mm_phys_addr_valid) {
		ret = -EFAULT;
		goto err;
	}

	framemap = (uint8_t *)mm_phys_addr;
	for (i = 0; i < total_frames >> 3; ++i)
		framemap[i] = 0;

	/* first bit in bitmap refers to this frame */
	mm_phys_addr = ((uint32_t)framemap + (total_frames >> 3) + PAGE_SIZE)
		       & ~(PAGE_SIZE - 1);
	serial_dump("mm: # of available frames = %lu, total free mem = %luK\n",
		    total_frames,
		    (total_frames * PAGE_SIZE) >> 10);
	/* nullify all available memory, just in case */
	for (i = 0; i < (total_frames - 1) >> 3; ++i)
		memset((void *)(mm_phys_addr + (i << PAGE_SHIFT)), 0, PAGE_SIZE);
	load_flags(state);
	return 0;
err:
	load_flags(state);
	return ret;
}

/* initialize the virtual memory manager */
int
init_vm(void)
{
	int ret = -ENOMEM;
	uint32_t i, state, addr, virt_addr;
	struct page_directory_t *tmp;
	struct page_table_t *ptable;

	save_flags(&state);
	cli();
	kernel_page_dir = palloc(sizeof(*kernel_page_dir));
	if (IS_ERR(kernel_page_dir)) {
		ret = PTR_ERR(kernel_page_dir);
		goto error;
	}
	memset(kernel_page_dir, 0, sizeof(*kernel_page_dir));

	kernel_page_table = palloc(sizeof(*kernel_page_table));
	if (IS_ERR(kernel_page_table)) {
		ret = PTR_ERR(kernel_page_table);
		goto error;
	}
	memset(kernel_page_table, 0, sizeof(*kernel_page_table));

	addr = ((uint32_t)kernel_page_table > (uint32_t)kernel_page_dir) ?
	       (uint32_t)kernel_page_table : (uint32_t)kernel_page_dir;
	mm_phys_addr_end = roundup_pagesize(addr + PAGE_SIZE);

	for (i = 0; i < 1024; ++i) {
		SET_PTE(kernel_page_table->pages[i],
			(i < (mm_phys_addr_end >> PAGE_SHIFT)) ? 1 : 0, 1, 0, i);
	}

	SET_PDE(kernel_page_dir->ptables[0], 1, 1, 0,
		(uint32_t)kernel_page_table >> PAGE_SHIFT);
	SET_PDE(kernel_page_dir->ptables[1023], 1, 1, 0,
		(uint32_t)kernel_page_dir->ptables >> PAGE_SHIFT);
	register_isr_handler(14, pagefault_callback);
	asm volatile ("movl %0, %%cr3":: "r"(kernel_page_dir->ptables));
	enable_paging();

	/* preallocate the page tables for the kernel heap */
	tmp = curr_pdir();
	for (i = 0; i < (KERNEL_HEAP_END - KERNEL_HEAP) >> PAGE_SHIFT; ++i) {
		virt_addr = get_pde_index(KERNEL_HEAP + (i << PAGE_SHIFT));
		ptable = get_ptable(virt_addr << PAGE_SHIFT);
		if (!tmp->ptables[virt_addr].present) {
			addr = (uint32_t)palloc(sizeof(struct page_table_t));
			if (IS_ERR((void *)addr)) {
				ret = PTR_ERR((void *)addr);
				goto error;
			}
			SET_PDE(tmp->ptables[virt_addr], 1, 1, 0, addr >> PAGE_SHIFT);
			memset(ptable, 0, sizeof(*ptable));
		}
	}

	init_heap();
	if ((ret = init_page_pa_allocator()) < 0)
		goto error;
	new_kernel_pdir = clone_page_dir();
	if (IS_ERR(new_kernel_pdir)) {
		ret = PTR_ERR(new_kernel_pdir);
		goto error;
	}
	switch_page_dir(new_kernel_pdir);
	load_flags(state);
	return 0;
error:
	load_flags(state);
	return ret;
}

void *
sbrk(intptr_t incr)
{
	uint32_t page_num;
	uint32_t kernel_heap_addr_old = curr_kernel_heap_addr, state;
	int ret = -ENOMEM;

	if (incr < 0)
		panic("%ld negative offset given!", incr);

	incr = roundup_pagesize(incr);
	page_num = incr / PAGE_SIZE;
	save_flags(&state);
	cli();
	if ((curr_kernel_heap_addr + incr >= KERNEL_HEAP_END)
			|| (ret = mmap_range(curr_kernel_heap_addr,
					     curr_kernel_heap_addr + incr)) < 0)
		goto err;
	curr_kernel_heap_addr += incr;
	load_flags(state);
	return (void *)kernel_heap_addr_old;
err:
	load_flags(state);
	return ERR_PTR(ret);
}

static struct page_directory_t *
create_pdir(uint32_t *addr_phys) {
	struct page_directory_t *new_pdir;

	if (addr_phys) {
		new_pdir = alloc_page_pa(addr_phys, ALLOC_PHYS);
		if (IS_ERR(new_pdir))
			return new_pdir;
		memset(new_pdir, 0, PAGE_SIZE);
		SET_PDE(new_pdir->ptables[1023], 1, 1, 0, *addr_phys >> PAGE_SHIFT);
		return new_pdir;
	}
	return ERR_PTR(-EINVAL);
}

static int
destroy_pdir(struct page_directory_t *pdir, uint32_t *phys, enum page_pa_fl flags)
{
	if (!pdir || !phys)
		return -EINVAL;
	free_page_pa(pdir, phys, flags);
	return 0;
}

static int
clone_page_table(struct page_table_t *ptable, uint32_t *phys)
{
	uint32_t src_frame_addr_phys, dst_frame_addr_phys, frame, i;
	uint32_t state;
	void *src_frame_addr_virt, *dst_frame_addr_virt;
	struct page_table_t *new_ptable;
	int ret;

	if (!ptable || !phys)
		return -EINVAL;

	save_flags(&state);
	cli();
	new_ptable = alloc_page_pa(phys, ALLOC_PHYS);
	if (IS_ERR(new_ptable)) {
		ret = PTR_ERR(new_ptable);
		goto out;
	}
	memset(new_ptable, 0, PAGE_SIZE);
	for (i = 0; i < 1024; ++i) {
		if (!ptable->pages[i].present)
			continue;
		frame = (uint32_t)palloc(PAGE_SIZE);
		if (!IS_ERR((void *)frame)) {
			*(uint32_t *)&new_ptable->pages[i] = *(uint32_t *) & ptable->pages[i];
			new_ptable->pages[i].frame = frame >> PAGE_SHIFT;
			src_frame_addr_phys = ptable->pages[i].frame << PAGE_SHIFT;
			dst_frame_addr_phys = new_ptable->pages[i].frame << PAGE_SHIFT;
			src_frame_addr_virt = alloc_page_pa(&src_frame_addr_phys, DONT_ALLOC_PHYS);
			if (!IS_ERR(src_frame_addr_virt)) {
				dst_frame_addr_virt = alloc_page_pa(&dst_frame_addr_phys, DONT_ALLOC_PHYS);
				if (!IS_ERR(dst_frame_addr_virt)) {
					memcpy(dst_frame_addr_virt, src_frame_addr_virt, PAGE_SIZE);
					free_page_pa(
						src_frame_addr_virt,
						&src_frame_addr_phys,
						DONT_FREE_PHYS
					);
					free_page_pa(
						dst_frame_addr_virt,
						&dst_frame_addr_phys,
						DONT_FREE_PHYS
					);
				} else { ret = PTR_ERR(dst_frame_addr_virt); goto out3; }
			} else { ret = PTR_ERR( src_frame_addr_virt); goto out2; }
		} else { ret = PTR_ERR((void *)frame); goto out1; }
	}
	free_page_pa(new_ptable, phys, DONT_FREE_PHYS);
	load_flags(state);
	return 0;
out3:
	free_page_pa(src_frame_addr_virt, &src_frame_addr_phys, FREE_PHYS);
out2:
	pfree((void *)frame, PAGE_SIZE);
out1:
	free_page_pa(new_ptable, phys, FREE_PHYS);
out:
	load_flags(state);
	return ret;
}

struct page_directory_t *
clone_page_dir(void) {
	uint32_t new_pdir_addr_phys, state, i;
	uint32_t new_ptable_addr_phys;
	struct page_directory_t *new_pdir, *pdir;
	int ret;

	save_flags(&state);
	cli();
	new_pdir = create_pdir(&new_pdir_addr_phys);
	if (IS_ERR(new_pdir)) {
		ret = PTR_ERR(new_pdir);
		goto out;
	}
	pdir = curr_pdir();
	for (i = 0; i < 1023; ++i) { /* exclude recursive mapping */
		if (!pdir->ptables[i].present) continue;
		if (*(uint32_t *)&kernel_page_dir->ptables[i]
				== *(uint32_t *)&pdir->ptables[i]) {
			*(uint32_t *)&new_pdir->ptables[i] = *(uint32_t *) & pdir->ptables[i];
			continue;
		}
		if ((ret = clone_page_table(get_ptable(i << PAGE_SHIFT),
					    &new_ptable_addr_phys)) < 0)
			goto out1;
		SET_PDE(new_pdir->ptables[i], 1, 1, 0,
			new_ptable_addr_phys >> PAGE_SHIFT);
	}
	load_flags(state);
	return new_pdir;
out1:
	destroy_pdir(new_pdir, &new_pdir_addr_phys, FREE_PHYS);
out:
	load_flags(state);
	return ERR_PTR(ret);
}

void
flush_tlb(void)
{
	uint32_t val, state;

	save_flags(&state);
	cli();
	asm volatile ("movl %%cr3, %0" : "=r" (val));
	asm volatile ("movl %0, %%cr3" :: "r" (val));
	load_flags(state);
}

uint32_t
virt_to_phys(uint32_t virt_addr, bool *val)
{
	struct page_directory_t *curr_page_dir;
	struct page_table_t *ptable;
	uint32_t state, ret;

	if (!val)
		panic("arg 'val' appears to be NULL!");

	save_flags(&state);
	cli();
	*val = true;
	curr_page_dir = curr_pdir();
	ptable = get_ptable(get_pde_index(virt_addr) << PAGE_SHIFT);
	if (!curr_page_dir->ptables[get_pde_index(virt_addr)].present)
		*val = false;
	if (!ptable->pages[get_pte_index(virt_addr)].present)
		*val = false;
	ret = (ptable->pages[get_pte_index(virt_addr)].frame << PAGE_SHIFT)
	      + (virt_addr & 0xfff);
	load_flags(state);
	return ret;
}

int
mmap(uint32_t virt_addr, uint32_t phys_addr)
{
	struct page_directory_t *curr_page_dir;
	struct page_table_t *ptable;
	uint32_t pt_addr, state;
	int ret = -ENOMEM;

	save_flags(&state);
	cli();
	curr_page_dir = curr_pdir();
	ptable = get_ptable(get_pde_index(virt_addr) << PAGE_SHIFT);
	if (!curr_page_dir->ptables[get_pde_index(virt_addr)].present) {
		pt_addr = get_free_frame();
		if (!pt_addr) goto err;
		SET_PDE(curr_page_dir->ptables[get_pde_index(virt_addr)],
			1,
			1,
			0,
			pt_addr >> PAGE_SHIFT);
	}
	if (ptable->pages[get_pte_index(virt_addr)].present)
		panic("duplicate mapping: 0x%08lx -> 0x%08lx", virt_addr, phys_addr);
	SET_PTE(ptable->pages[get_pte_index(virt_addr)],
		1,
		1,
		0,
		phys_addr >> PAGE_SHIFT);
	flush_tlb();
	load_flags(state);
	return 0;
err:
	load_flags(state);
	return ret;
}

int
unmap(uint32_t virt_addr)
{
	int ret = -EFAULT;
	struct page_directory_t *curr_page_dir;
	struct page_table_t *ptable;
	uint32_t state;
	uint32_t i;

	save_flags(&state);
	cli();
	curr_page_dir = curr_pdir();
	if (!curr_page_dir->ptables[get_pde_index(virt_addr)].present)
		goto err;
	ptable = get_ptable(get_pde_index(virt_addr) << PAGE_SHIFT);
	if (!ptable->pages[get_pte_index(virt_addr)].present)
		goto err;
	ptable->pages[get_pte_index(virt_addr)].present = 0;
	for (i = 0; i < 1024 && !ptable->pages[i].present; ++i)
		;
	if (i == 1024) {
		curr_page_dir->ptables[get_pde_index(virt_addr)].present = 0;
		pfree((void *)(curr_page_dir->ptables[get_pde_index(virt_addr)].pt_addr
			       << PAGE_SHIFT),
		      PAGE_SIZE);
	}
	flush_tlb();
	load_flags(state);
	return 0;
err:
	load_flags(state);
	return ret;
}

int
mmap_range(uint32_t virt_addr_start, uint32_t virt_addr_end)
{
	uint32_t state, free_page;
	uint32_t s;
	int ret;

	save_flags(&state);
	cli();
	s = virt_addr_start;
	virt_addr_start &= ~(PAGE_SIZE - 1);
	virt_addr_end = roundup_pagesize(virt_addr_end);
	for (; virt_addr_start < virt_addr_end; virt_addr_start += PAGE_SIZE) {
		free_page = get_free_frame();
		if (free_page) {
			if ((ret = mmap(virt_addr_start, free_page)) < 0) {
				load_flags(state);
				return ret;
			}
			continue;
		}
		if (s < virt_addr_start) {
			ret = unmap_range(s, virt_addr_start);
			if (ret < 0)
				panic("can't unmap range!");
		}
		load_flags(state);
		return -ENOMEM;
	}
	load_flags(state);
	return 0;
}

int
unmap_range(uint32_t virt_addr_start, uint32_t virt_addr_end)
{
	uint32_t state;
	int ret;

	save_flags(&state);
	cli();
	virt_addr_start &= ~(PAGE_SIZE - 1);
	virt_addr_end = roundup_pagesize(virt_addr_end);
	for (; virt_addr_start < virt_addr_end; virt_addr_start += PAGE_SIZE) {
		if ((ret = unmap(virt_addr_start)) < 0) {
			load_flags(state);
			return ret;
		}
	}
	load_flags(state);
	return 0;
}

void
dump_mappings(void)
{
	uint32_t i, j, nr = 0, state;
	uint32_t addr, paddr;
	struct page_directory_t *pdir;
	struct page_table_t *ptable;
	bool valid_map;

	save_flags(&state);
	cli();
	serial_dump("Mmap dump for [%s:%d]\n", curr_proc->name, curr_proc->pid);
	printf("Mmap dump for [%s:%d]\n", curr_proc->name, curr_proc->pid);
	pdir = curr_pdir();
	for (i = 1; i < 1023; ++i) {
		if (!pdir->ptables[i].present)
			continue;
		ptable = get_ptable(i << PAGE_SHIFT);
		for (j = 0; j < 1024; ++j) {
			if (!ptable->pages[j].present)
				continue;
			addr = (i << 22) | (j << 12);
			paddr = virt_to_phys(addr, &valid_map);
			if (!valid_map)
				continue;
			serial_dump("[%lu] 0x%08lx -> 0x%08lx\n", nr++, addr, paddr);
			printf("[%lu] 0x%08lx -> 0x%08lx\n", nr++, addr, paddr);
		}
	}
	load_flags(state);
}

void
remove_proc_mappings(void)
{
	uint32_t state, i, j;
	uint32_t addr, paddr, tmp;
	struct page_directory_t *pdir;
	struct page_table_t *ptable;
	bool valid_map;
	struct list_head *iter, *tmpreg;
	struct mmap_region *reg;

	save_flags(&state);
	cli();
	list_for_each_safe(iter, tmpreg, &curr_proc->l_regions) {
		reg = list_entry(iter, struct mmap_region, l_region);
		if (unmap_range(reg->base_addr, reg->base_addr + reg->size) < 0)
			panic("failed to unmap pages!");
		list_del(iter);
		kfree(reg);
	}
	pdir = curr_pdir();
	for (i = 2; i < 1023; ++i) {
		if (pdir->ptables[i].present) {
			if (*(uint32_t *)&curr_proc->parent->page_dir->ptables[i]
					!= *(uint32_t *)&pdir->ptables[i]) {
				ptable = get_ptable(i << PAGE_SHIFT);
				for (j = 0; j < 1024; ++j)
					if (ptable->pages[j].present)
						pfree((void *)(ptable->pages[j].frame << PAGE_SHIFT),
						      PAGE_SIZE);
				paddr = pdir->ptables[i].pt_addr << PAGE_SHIFT;
				pfree((void *)paddr, PAGE_SIZE);
			}
		}
	}
	paddr = read_cr3();
	for (i = 0; i < PAGE_HEAP_SIZE >> PAGE_SHIFT; i++) {
		if (!is_page_free(i)) {
			addr = PAGE_HEAP + (i << PAGE_SHIFT);
			tmp = virt_to_phys(addr, &valid_map);
			if (valid_map && paddr == tmp) {
				free_page_pa((void *)addr,  &paddr, FREE_PHYS);
				break;
			}
		}
	}
	load_flags(state);
}

/* a bitmap based physical memory allocator */
uint32_t
get_num_free_frames(void)
{
	uint32_t i, nfree = 0;

	for (i = 0; i < total_frames; ++i)
		if (is_frame_free(i))
			++nfree;
	return nfree;
}

void *
palloc(size_t size)
{
	uint32_t i, j, frames_alloc, state;

	assert(framemap);
	if (!size)
		return ERR_PTR(-EINVAL);

	size = roundup_pagesize(size);
	frames_alloc = size >> PAGE_SHIFT;
	if (frames_alloc > total_frames - 1)
		return ERR_PTR(-ENOMEM);

	save_flags(&state);
	cli();
	for (i = 0; i < total_frames; ++i) {
		if (is_frame_free(i)) {
			for (j = 0; j < frames_alloc; ++j)
				if (!is_frame_free(i + j))
					break;
			if (j == frames_alloc) {
				for (j = 0; j < frames_alloc; ++j)
					framemap[get_frame_block(i + j)] |=
						(1 << get_frame_offset(i + j));
				load_flags(state);
				return (void *)(mm_phys_addr + (i << PAGE_SHIFT));
			}
		}
	}
	load_flags(state);
	return ERR_PTR(-ENOMEM);
}

void
pfree(void *addr, size_t size)
{
	uint32_t framestofree, base_addr, i, state;

	if (!addr || !size)
		return;

	size = roundup_pagesize(size);
	framestofree = size >> PAGE_SHIFT;
	base_addr = (uint32_t)addr;
	base_addr -= mm_phys_addr;
	base_addr >>= PAGE_SHIFT;

	save_flags(&state);
	cli();
	for (i = 0; i < framestofree; ++i)
		framemap[get_frame_block(base_addr + i)] &=
			~(1 << get_frame_offset(base_addr + i));
	load_flags(state);
}

int
init_page_pa_allocator(void)
{
	size_t len = PAGE_HEAP_SIZE >> PAGE_SHIFT;

	pagemap_pa = kmalloc(len);
	if (IS_ERR(pagemap_pa))
		return PTR_ERR(pagemap_pa);
	memset(pagemap_pa, 0, len);
	return 0;
}

void *
alloc_page_pa(uint32_t *frame, enum page_pa_fl flags)
{
	uint32_t i, addr, state;
	int ret;

	if (!frame)
		return ERR_PTR(-EINVAL);

	save_flags(&state);
	cli();
	for (i = 0; i < PAGE_HEAP_SIZE >> PAGE_SHIFT; ++i) {
		if (is_page_free(i)) {
			addr = (flags == ALLOC_PHYS) ? get_free_frame() : *frame;
			if (addr) {
				if ((ret = mmap(PAGE_HEAP
						+ (i << PAGE_SHIFT), addr)) < 0) {
					load_flags(state);
					return ERR_PTR(ret);
				}
				pagemap_pa[get_page_block(i)] |= (1 << get_page_offset(i));
				*frame = addr;
				if (flags == ALLOC_PHYS)
					memset((void *)(PAGE_HEAP + (i << PAGE_SHIFT)), 0,
					       PAGE_SIZE);
				load_flags(state);
				return (void *)(PAGE_HEAP + (i << PAGE_SHIFT));
			}
			break;
		}
	}
	load_flags(state);
	return ERR_PTR((flags == ALLOC_PHYS) ? -ENOMEM : -EINVAL);
}

int
free_page_pa(void *page, uint32_t *frame, enum page_pa_fl flags)
{
	uint32_t base_addr, state;
	int ret;

	if (!page || !frame)
		return -EINVAL;

	save_flags(&state);
	cli();
	base_addr = (uint32_t)page;
	if ((ret = unmap(base_addr)) < 0) {
		load_flags(state);
		return ret;
	}
	base_addr -= PAGE_HEAP;
	base_addr >>= PAGE_SHIFT;
	pagemap_pa[get_page_block(base_addr)] &=
		~(1 << get_page_offset(base_addr));
	if (flags == FREE_PHYS)
		pfree((void *)*frame, PAGE_SIZE);
	load_flags(state);
	return 0;
}

