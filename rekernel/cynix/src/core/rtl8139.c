/*
 *  core/rtl8139.c
 *
 *  Copyright (C) 2010 stateless
 */

#include <rtl8139.h>
#include <pci.h>
#include <x86.h>
#include <idt.h>
#include <heap.h>
#include <rtc.h>
#include <string.h>
#include <errno.h>

#define INTMASK (RTL_INT_PCIERR | RTL_INT_RX_ERR \
		 | RTL_INT_RX_OK | RTL_INT_TX_ERR \
		 | RTL_INT_TX_OK | RTL_INT_RXBUF_OVERFLOW)

enum {
	RTL_VENDOR_ID = 0x10ec,
	RTL_DEVICE_ID = 0x8139,
	RTL_RESET_TIMEOUT = 20 /* 200 ms */
};

/* NOTE: register naming comes from NetBSD-DC sources. */
enum rtl_registers {
	RTL_IDR0               = 0x00,            /* Mac address */
	RTL_MAR0               = 0x08,            /* Multicast filter */
	RTL_TXSTATUS0          = 0x10,            /* Transmit status (4 32bit regs) */
	RTL_TXSTATUS1          = 0x14,            /* Transmit status (4 32bit regs) */
	RTL_TXSTATUS2          = 0x18,            /* Transmit status (4 32bit regs) */
	RTL_TXSTATUS3          = 0x1c,            /* Transmit status (4 32bit regs) */
	RTL_TXADDR0            = 0x20,            /* Tx descriptors (also 4 32bit) */
	RTL_TXADDR1            = 0x24,            /* Tx descriptors (also 4 32bit) */
	RTL_TXADDR2            = 0x28,            /* Tx descriptors (also 4 32bit) */
	RTL_TXADDR3            = 0x2c,            /* Tx descriptors (also 4 32bit) */
	RTL_RXBUF              = 0x30,            /* Receive buffer start address */
	RTL_RXEARLYCNT         = 0x34,            /* Early Rx byte count */
	RTL_RXEARLYSTATUS      = 0x36,            /* Early Rx status */
	RTL_CHIPCMD            = 0x37,            /* Command register */
	RTL_RXBUFTAIL          = 0x38,            /* Current address of packet read (queue tail) */
	RTL_RXBUFHEAD          = 0x3A,            /* Current buffer address (queue head) */
	RTL_INTRMASK           = 0x3C,            /* Interrupt mask */
	RTL_INTRSTATUS         = 0x3E,            /* Interrupt status */
	RTL_TXCONFIG           = 0x40,            /* Tx config */
	RTL_RXCONFIG           = 0x44,            /* Rx config */
	RTL_TIMER              = 0x48,            /* A general purpose counter */
	RTL_RXMISSED           = 0x4C,            /* 24 bits valid, write clears */
	RTL_CFG9346            = 0x50,            /* 93C46 command register */
	RTL_CONFIG0            = 0x51,            /* Configuration reg 0 */
	RTL_CONFIG1            = 0x52,            /* Configuration reg 1 */
	RTL_TIMERINT           = 0x54,            /* Timer interrupt register (32 bits) */
	RTL_MEDIASTATUS        = 0x58,            /* Media status register */
	RTL_CONFIG3            = 0x59,            /* Config register 3 */
	RTL_CONFIG4            = 0x5A,            /* Config register 4 */
	RTL_MULTIINTR          = 0x5C,            /* Multiple interrupt select */
	RTL_MII_TSAD           = 0x60,            /* Transmit status of all descriptors (16 bits) */
	RTL_MII_BMCR           = 0x62,            /* Basic Mode Control Register (16 bits) */
	RTL_MII_BMSR           = 0x64,            /* Basic Mode Status Register (16 bits) */
	RTL_AS_ADVERT          = 0x66,            /* Auto-negotiation advertisement reg (16 bits) */
	RTL_AS_LPAR            = 0x68,            /* Auto-negotiation link partner reg (16 bits) */
	RTL_AS_EXPANSION       = 0x6A             /* Auto-negotiation expansion reg (16 bits) */
};

enum rtl_command_bits {
	RTL_CMD_RESET          = 0x10,
	RTL_CMD_RX_ENABLE      = 0x08,
	RTL_CMD_TX_ENABLE      = 0x04,
	RTL_CMD_RX_BUF_EMPTY   = 0x01
};

enum rtl_int_status {
	RTL_INT_PCIERR              = 0x8000,          /* PCI Bus error */
	RTL_INT_TIMEOUT             = 0x4000,          /* Set when TCTR reaches TimerInt value */
	RTL_INT_RXFIFO_OVERFLOW     = 0x0040,          /* Rx FIFO overflow */
	RTL_INT_RXFIFO_UNDERRUN     = 0x0020,          /* Packet underrun / link change */
	RTL_INT_RXBUF_OVERFLOW      = 0x0010,          /* Rx BUFFER overflow */
	RTL_INT_TX_ERR              = 0x0008,
	RTL_INT_TX_OK               = 0x0004,
	RTL_INT_RX_ERR              = 0x0002,
	RTL_INT_RX_OK               = 0x0001
};

enum rtl_tx_status {
	RTL_TX_CARRIER_LOST         = 0x80000000,      /* Carrier sense lost */
	RTL_TX_ABORTED              = 0x40000000,      /* Transmission aborted */
	RTL_TX_OUT_OF_WINDOW        = 0x20000000,      /* Out of window collision */
	RTL_TX_STATUS_OK            = 0x00008000,      /* Status ok: a good packet was transmitted */
	RTL_TX_UNDERRUN             = 0x00004000,      /* Transmit FIFO underrun */
	RTL_TX_HOST_OWNS            = 0x00002000,      /* Set to 1 when DMA operation is completed */
	RTL_TX_SIZE_MASK            = 0x00001fff       /* Descriptor size mask */
};

enum rtl_rx_status {
	RTL_RX_MULTICAST            = 0x00008000,      /* Multicast packet */
	RTL_RX_PAM                  = 0x00004000,      /* Physical address matched */
	RTL_RX_BROADCAST            = 0x00002000,      /* Broadcast address matched */
	RTL_RX_BAD_SYMBOL           = 0x00000020,      /* Invalid symbol in 100TX packet */
	RTL_RX_RUNT                 = 0x00000010,      /* Packet size is <64 bytes */
	RTL_RX_TOO_LONG             = 0x00000008,      /* Packet size is >4K bytes */
	RTL_RX_CRC_ERR              = 0x00000004,      /* CRC error */
	RTL_RX_FRAME_ALIGN          = 0x00000002,      /* Frame alignment error */
	RTL_RX_STATUS_OK            = 0x00000001       /* Status ok: a good packet was received */
};

struct rtl8139_t {
	char rx_buffer[8192 + 16];
	uint8_t mac_addr[6];
	spinlock_t iolock;
	uint32_t io_base;
} *rtl = NULL;

void
rtl8139_int(__attribute__ ((unused)) struct trapframe_t *regs)
{
	panic("yoohoo!");
}

static void
rtl_write8(uint16_t offset, uint8_t val)
{
	acquire(&rtl->iolock);
	outb(rtl->io_base + offset, val);
	release(&rtl->iolock);
}

static void
rtl_write32(uint16_t offset, uint32_t val)
{
	acquire(&rtl->iolock);
	outl(rtl->io_base + offset, val);
	release(&rtl->iolock);
}

static void
rtl_write16(uint16_t offset, uint16_t val)
{
	acquire(&rtl->iolock);
	outw(rtl->io_base + offset, val);
	release(&rtl->iolock);
}

static uint8_t
rtl_read8(uint16_t offset)
{
	uint8_t r;

	acquire(&rtl->iolock);
	r = inb(rtl->io_base + offset);
	release(&rtl->iolock);
	return r;
}

static uint32_t
rtl_read32(uint16_t offset)
{
	uint32_t r;

	acquire(&rtl->iolock);
	r = inl(rtl->io_base + offset);
	release(&rtl->iolock);
	return r;
}

int
rtl8139_init(void)
{
	struct pci_dev_t *dev;
	uint32_t i, timeout = 0;

	/* enumerate the PCI bus looking for an rtl8139 */
	dev = get_pci_dev(RTL_VENDOR_ID, RTL_DEVICE_ID);
	if (!dev)
		return -ENODEV;

	rtl = kmalloc(sizeof(*rtl));
	if (IS_ERR(rtl))
		return PTR_ERR(rtl);

	memset(rtl, 0, sizeof(*rtl));
	initspinlock(&rtl->iolock);

	/* grab the io port base for all subsequence i/o operations */
	rtl->io_base = dev->cspace->type0.bar0 & 0xfffffffc;

	/* reset the device */
	rtl_write8(RTL_CHIPCMD, RTL_CMD_RESET);
	do {
		if (timeout++ == RTL_RESET_TIMEOUT)
			goto err;
		msleep(1); /* sleep for ~10ms */
	} while (rtl_read8(RTL_CHIPCMD) & RTL_CMD_RESET);

	register_isr_handler(0x20 + dev->cspace->type0.irq, rtl8139_int);

	/* grab the mac-address */
	for (i = 0; i < 6; ++i)
		rtl->mac_addr[i] = rtl_read8(i);

	/* put the device in config mode to enable writing to the config registers */
	rtl_write8(RTL_CFG9346, 0xc0);
	/* reset config 1 */
	rtl_write8(RTL_CONFIG1, 0);
	/* go back to normal mode */
	rtl_write8(RTL_CFG9346, 0);

	/* set the receive buffer address */
	rtl_write32(RTL_RXBUF, (uint32_t)rtl->rx_buffer);
	/* TODO: setup the tx descriptors */

#if 0
	/* accept
	 *        physical address packets,
	 *        physical match packets,
	 *        multicast packets,
	 *        and broadcast packets
	 * enable wrap mode
	 */
	rtl_write32(RTL_RXCONFIG, 0xf | (1 << 7));
	/* make sure IRQs will fire */
	rtl_write32(RTL_INTRMASK, RTL_INT_TX_OK | RTL_INT_RX_OK);
#else
	rtl_write32(RTL_RXMISSED, 0);
	rtl_write32(RTL_RXCONFIG, rtl_read32(RTL_RXCONFIG) | 0x0000000f);
	rtl_write32(RTL_MAR0, 0);
	rtl_write32(RTL_MAR0 + 4, 0);
	rtl_write16(RTL_MULTIINTR, 0);
	rtl_write16(RTL_INTRMASK, INTMASK);
#endif

	/* enable the receiver and the transmitter */
	rtl_write32(RTL_CHIPCMD, RTL_CMD_RX_ENABLE | RTL_CMD_TX_ENABLE);
	/* just to be on the safe side */
	rtl_write8(RTL_CFG9346, 0);
	return 0;
err:
	kfree(rtl);
	return -EBUSY;
}

const uint8_t *
get_mac_addr(void)
{
	if (!rtl)
		return NULL;
	return rtl->mac_addr;
}

