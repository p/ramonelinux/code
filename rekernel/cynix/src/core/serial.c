/*
 *  core/serial.c
 *
 *  Copyright (C) 2009 stateless
 */

#include <serial.h>
#include <x86.h>
#include <stdarg.h>
#include <stdio.h>
#include <sys/types.h>
#include <errno.h>

static int serial_has_init = 0;

void
serial_init(void)
{
	uint32_t state;

	save_flags(&state);
	cli();
	outb(COM_PORT + 1, 0x0);
	outb(COM_PORT + 3, 1 << 7);
	outb(COM_PORT + 0, 0x3);
	outb(COM_PORT + 1, 0x0);
	outb(COM_PORT + 3, 0 << 7);
	outb(COM_PORT + 3, 0x3);
	outb(COM_PORT + 2, 0xc7);
	outb(COM_PORT + 4, 0x0b);
	serial_has_init = 1;
	load_flags(state);
}

int
serial_dump(const char *s, ...)
{
	char buf[512], *ptr;
	va_list ap;
	int c;
	uint32_t state;

	if (!serial_has_init)
		return -EIO;
	save_flags(&state);
	cli();
	va_start(ap, s);
	c = vsprintf(buf, s, ap);
	va_end(ap);
	ptr = buf;
	while (*ptr)
		if (inb(COM_PORT + 5) & 0x20)
			outb(COM_PORT, *ptr++);
	load_flags(state);
	return c;
}

int
serial_putchar(int c)
{
	uint32_t state;

	if (!serial_has_init)
		return -EIO;
	save_flags(&state);
	cli();
	if (inb(COM_PORT + 5) & 0x20)
		outb(COM_PORT, (uint8_t)c);
	load_flags(state);
	return 0;
}

int
serial_avail(void)
{
	return serial_has_init;
}

