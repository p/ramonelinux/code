/*
 *  core/pci.c
 *
 *  Copyright (C) 2010 stateless
 */

#include <pci.h>
#include <x86.h>
#include <string.h>
#include <heap.h>
#include <errno.h>
#include <serial.h>

static int has_pci_init = 0;
static struct list_head rootbus;
static spinlock_t pci_lock;

uint32_t
pci_read32(struct pci_config_addr_t *addr)
{
	uint32_t r;

	acquire(&pci_lock);
	if (addr->reg_num & 0x3)
		panic("offset is not 0-padded!");
	outl(CONFIG_ADDRESS, *(uint32_t *)addr);
	r = inl(CONFIG_DATA);
	release(&pci_lock);
	return r;
}

uint32_t
pci_init(void)
{
	uint32_t ret, tmp;

	initspinlock(&pci_lock);
	outl(CONFIG_ADDRESS, 0);
	outl(0xcfa, 0);
	if (inl(CONFIG_ADDRESS) == 0 && inl(0xcfa) == 0) {
		info("PCI type-2 is not supported!\n");
		return 1;
	}
	tmp = inl(CONFIG_ADDRESS);
	outl(CONFIG_ADDRESS, 0x80000000);
	ret = inl(CONFIG_ADDRESS);
	outl(CONFIG_ADDRESS, tmp);
	if (ret == 0x80000000)
		has_pci_init = 1;
	return ret != 0x80000000;
}

static void
dump_dev(struct pci_dev_t *dev)
{
	struct pci_config_addr_t addr;
	union pci_config_space_t *cspace;
	uint32_t reg = 0x10, r;

	printf("Bus = %d, Device = %d\n", dev->busn, dev->devn);
	cspace = dev->cspace;
	if (cspace->type0.header_type == 0x1)
		printf("\tPCI-to-PCI bridge\n");
	printf("\tvendor_id = 0x%04lx, device_id = 0x%04lx\n",
	       cspace->type0.vendor_id, cspace->type0.device_id);
	printf("\tstatus = 0x%04lx, command = 0x%04lx\n",
	       cspace->type0.status, cspace->type0.command);
	printf("\tclass = 0x%02lx, subclass = 0x%02lx\n",
	       cspace->type0.class, cspace->type0.subclass);
	printf("\tIRQ = %u\n", cspace->type0.irq);

	serial_dump("Bus = %d, Device = %d\n", dev->busn, dev->devn);
	cspace = dev->cspace;
	if (cspace->type0.header_type == 0x1)
		serial_dump("\tPCI-to-PCI bridge\n");
	serial_dump("\tvendor_id = 0x%04lx, device_id = 0x%04lx\n",
		    cspace->type0.vendor_id, cspace->type0.device_id);
	serial_dump("\tstatus = 0x%04lx, command = 0x%04lx\n",
		    cspace->type0.status, cspace->type0.command);
	serial_dump("\tclass = 0x%02lx, subclass = 0x%02lx\n",
		    cspace->type0.class, cspace->type0.subclass);
	serial_dump("\tIRQ = %u\n", cspace->type0.irq);

	addr.enable_bit = 1;
	addr.bus_num = dev->busn;
	addr.dev_num = dev->devn;
	if (cspace->type0.header_type) {
		if (cspace->type0.header_type & 0x80)
			printf("\tmultiple functions are available\n");
		addr.reg_num = 0x18;
		r = pci_read32(&addr);
		printf("\tprimary bus = %u, secondary bus = %u\n",
		       r & 0xff, (r >> 8) & 0xff);
	}
	for (; reg <= (cspace->type0.header_type == 0x1 ? 0x14 : 0x24);
			reg += 4) {
		addr.reg_num = reg;
		r = pci_read32(&addr);
		if (r != 0 && r != 0xffffffff)
			printf("\tbar%d = 0x%08lx\n",
			       (reg - 0x10) / 4, r);
	}
}

void
lspci(void)
{
	struct list_head *iter_bus, *iter_dev;
	struct pci_dev_t *dev;
	struct pci_bus_t *bus;

	if (!has_pci_init) {
		info("there is no PCI bus!\n");
		return;
	}

	list_for_each(iter_bus, &rootbus) {
		bus = list_entry(iter_bus, struct pci_bus_t, pci_bus_list);
		list_for_each(iter_dev, &bus->pci_dev_list) {
			dev = list_entry(iter_dev, struct pci_dev_t, list);
			dump_dev(dev);
		}
	}
}

static int
load_config_space(struct pci_dev_t *dev, struct pci_config_addr_t addr)
{
	uint32_t r, reg, *p;
	union pci_config_space_t *cspace;

	cspace = kmalloc(sizeof(*cspace));
	if (IS_ERR(cspace))
		return PTR_ERR(cspace);
	memset(cspace, 0, sizeof(cspace));
	p = (uint32_t *)cspace;
	addr.enable_bit = 1;
	for (reg = 0; reg <= 0x3c; reg += 4) {
		addr.reg_num = reg;
		r = pci_read32(&addr);
		if (r != 0)
			*p = r;
		++p;
	}
	dev->cspace = cspace;
	return 0;
}

static void
cleanup(void)
{
	struct list_head *iter_bus, *iter_dev, *tmp1, *tmp2;
	struct pci_dev_t *dev;
	struct pci_bus_t *bus;

	list_for_each_safe(iter_bus, tmp1, &rootbus) {
		bus = list_entry(iter_bus, struct pci_bus_t, pci_bus_list);
		list_for_each_safe(iter_dev, tmp2, &bus->pci_dev_list) {
			dev = list_entry(iter_dev, struct pci_dev_t, list);
			list_del(iter_dev);
			kfree(dev->cspace);
			kfree(dev);
		}
		list_del(iter_bus);
		kfree(bus);
	}
}

static struct pci_bus_t *
create_bus(void) {
	struct pci_bus_t *bus;

	bus = kmalloc(sizeof(*bus));
	if (IS_ERR(bus)) {
		cleanup();
		return bus;
	}
	INIT_LIST_HEAD(&bus->pci_dev_list);
	return bus;
}

static int
attach_dev(struct pci_bus_t *bus, struct pci_config_addr_t addr,
	   int busn, int devn)
{
	int ret;
	struct pci_dev_t *dev;

	dev = kmalloc(sizeof(*dev));
	if (IS_ERR(dev)) {
		cleanup();
		return PTR_ERR(dev);
	}
	memset(dev, 0, sizeof(*dev));
	dev->cspace = NULL;
	dev->bus = bus;
	dev->busn = busn;
	dev->devn = devn;
	ret = load_config_space(dev, addr);
	if (ret < 0) {
		assert(IS_ERR(dev->cspace));
		kfree(dev);
		cleanup();
		return -1;
	}
	list_add_tail(&dev->list, &bus->pci_dev_list);
	return 0;
}

int
pci_enumerate(void)
{
	struct pci_bus_t *busp;
	struct pci_config_addr_t addr;
	int dev, bus, ret, f;
	uint32_t r;

	INIT_LIST_HEAD(&rootbus);
	memset(&addr, 0, sizeof(addr));
	addr.enable_bit = 1;
	for (bus = 0; bus < 256; ++bus) {
		addr.bus_num = bus;
		for (dev = 0, f = 0; dev < 32; ++dev) {
			addr.dev_num = dev;
			if ((r = pci_read32(&addr)) == PCI_NO_DEV)
				continue;
			if (!f) {
				busp = create_bus();
				if (IS_ERR(busp))
					return PTR_ERR(busp);
				list_add(&busp->pci_bus_list, &rootbus);
				f = 1;
			}
			ret = attach_dev(busp, addr, bus, dev);
			if (ret < 0) {
				return ret;
			}
		}
	}
	return 0;
}

struct pci_dev_t *
get_pci_dev(uint16_t vendor_id, uint16_t device_id) {
	struct list_head *iter_bus, *iter_dev;
	struct pci_dev_t *dev;
	struct pci_bus_t *bus;

	if (!has_pci_init) {
		info("there is no PCI bus!\n");
		return NULL;
	}

	list_for_each(iter_bus, &rootbus) {
		bus = list_entry(iter_bus, struct pci_bus_t, pci_bus_list);
		list_for_each(iter_dev, &bus->pci_dev_list) {
			dev = list_entry(iter_dev, struct pci_dev_t, list);
			if (!dev->cspace)
				continue;
			if (dev->cspace->type0.vendor_id == vendor_id
					&& dev->cspace->type0.device_id == device_id)
				return dev;
		}
	}
	return NULL;
}

