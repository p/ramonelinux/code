/*
 *  core/tss.c
 *
 *  Copyright (C) 2009 stateless
 */

#include <tss.h>
#include <string.h>
#include <common.h>
#include <heap.h>
#include <errno.h>
#include <syscall.h>
#include <tty.h>

#define NR_HASH_ENT 64

extern struct page_directory_t *new_kernel_pdir;

struct task_t *curr_proc = NULL;

static struct list_head htsleep[NR_HASH_ENT];
static struct list_head runqueue;
static struct task_t *idle = NULL;
static int tss_init = 0;
static pid_t currpid = 0;

static uint32_t hash_chan(void *chan);

static inline pid_t
get_new_pid(void)
{
	int pid;
	uint32_t state;

	save_flags(&state);
	cli();
	/* I hope this never wraps */
	pid = currpid++;
	load_flags(state);
	return pid;
}

void
ps(void)
{
	uint32_t state, i;
	struct list_head *iter;
	struct task_t *task;

	printf("%-12s%-12s%-12s%-12s%-12s\n",
	       "PID",
	       "TYPE",
	       "STATE",
	       "PROC",
	       "PARENT");
	save_flags(&state);
	cli();
	list_for_each(iter, &runqueue) {
		task = list_entry(iter, struct task_t, q_task);
		printf("%-12d%-12s%-12s%-12s%-12s\n",
		       task->pid,
		       (task->flags & KERNEL_PROCESS) ? "KTHREAD" : "USER PROC",
		       (task->state == TASK_ZOMBIE) ? "ZOMBIE" :
		       (task->state == TASK_SLEEPING) ? "SLEEPING" :
		       (task->state == TASK_RUNNABLE) ? "RUNNABLE" : "RUNNING",
		       task->name,
		       (!task->parent) ? "(null)" : task->parent->name);
	}
	for (i = 0; i < NR_HASH_ENT; ++i) {
		list_for_each(iter, &htsleep[i]) {
			task = list_entry(iter, struct task_t, q_task);
			assert(task->state == TASK_SLEEPING);
			printf("%-12d%-12s%-12s%-12s%-12s\n",
			       task->pid,
			       (task->flags & KERNEL_PROCESS) ? "KTRHEAD" : "USER PROC",
			       "SLEEPING",
			       task->name,
			       (!task->parent) ? "(null)" : task->parent->name);
		}
	}
	load_flags(state);
}

void
idle_fn(void)
{
	tss_init = 1;
	curr_proc->state = TASK_RUNNING;
	sti();
	while (1) {
	}
}

void
freeze_tasks(void)
{
	struct list_head *iter;
	struct task_t *task;

	list_for_each(iter, &runqueue) {
		task = list_entry(iter, struct task_t, q_task);
		if (!strcmp(task->name, "kdb")
				|| !strcmp(task->name, "idle")) {
			task->state = TASK_RUNNABLE;
			continue;
		}
		task->old_state = task->state;
		task->state = TASK_SLEEPING;
	}
}

void
unfreeze_tasks(void)
{
	struct list_head *iter;
	struct task_t *task;

	list_for_each(iter, &runqueue) {
		task = list_entry(iter, struct task_t, q_task);
		if (strcmp(task->name, "kdb")
				&& strcmp(task->name, "idle"))
			task->state = task->old_state;
	}
}

void
schedule(void)
{
	struct list_head *iter, *q;
	struct task_t *task;

	if (!tss_init)
		return;

	list_for_each_safe(iter, q, &runqueue) {
		task = list_entry(iter, struct task_t, q_task);
		if (task->state == TASK_RUNNABLE) {
			task->state = TASK_RUNNING;
			curr_proc = task;
			switch_page_dir(curr_proc->page_dir);
			break;
		} else if (task->state == TASK_RUNNING)
			task->state = TASK_RUNNABLE;
		list_del(iter);
		add_task(task);
	}
}

int
init_tss(void)
{
	int i;

	cli();
	INIT_LIST_HEAD(&runqueue);
	for (i = 0; i < NR_HASH_ENT; ++i)
		INIT_LIST_HEAD(&htsleep[i]);
	idle = create_kthread("idle", idle_fn);
	if (IS_ERR(idle))
		return PTR_ERR(idle);
	curr_proc = idle;
	return 0;
}

void
kick_tss(void)
{
	PUT_ESP(curr_proc->esp);
	jmp(((struct cswitch_frame_t *)curr_proc->esp)->eip);
}

struct task_t *
create_kthread(const char *name, void *routine) {
	struct task_t *task;
	int ret = -ENOMEM, i;
	size_t len;

	task = kmalloc(sizeof(*task));
	if (IS_ERR(task)) {
		ret = PTR_ERR(task);
		goto err;
	}
	memset(task, 0, sizeof(*task));
	len = strlen(name) + 1;
	assert(len);
	task->name = kmalloc(len);
	if (IS_ERR(task->name)) {
		ret = PTR_ERR(task->name);
		goto err1;
	}
	strncpy(task->name, name, len);
	task->name[len - 1] = '\0';
	INIT_LIST_HEAD(&task->l_regions);
	task->parent = curr_proc;
	task->pid = get_new_pid();
	task->state = task->old_state = TASK_RUNNABLE;
	task->flags = KERNEL_PROCESS;
	task->cdir = get_root_inode();
	if (!task->cdir) {
		ret = -EIO;
		goto err2;
	}
	for (i = 0; i < NR_MAX_OPEN_FILES; ++i)
		task->fdtable[i] = NULL;
	task->stack = kmalloc(TASK_STACK_SIZE);
	if (IS_ERR(task->stack)) {
		ret = PTR_ERR(task->stack);
		goto err2;
	}
	memset(task->stack, 0x0, TASK_STACK_SIZE);
	task->page_dir = clone_page_dir();
	if (IS_ERR(task->page_dir)) {
		ret = PTR_ERR(task->page_dir);
		goto err3;
	}
	task->cf = (struct cswitch_frame_t *)((uint8_t *)task->stack
					      + TASK_STACK_SIZE
					      - sizeof(struct cswitch_frame_t));
	task->cf->eflags = 0x202;
	task->cf->cs = 0x8;
	task->cf->eip = (uint32_t)routine;
	task->cf->ds = 0x10;
	task->esp = (uint32_t)task->cf;
	attach_tty(task);
	add_task(task);
	return task;
err3:
	kfree(task->stack);
err2:
	kfree(task->name);
err1:
	kfree(task);
err:
	return ERR_PTR(ret);
}

void
add_task(struct task_t *task)
{
	uint32_t state;

	save_flags(&state);
	cli();
	list_add_tail(&task->q_task, &runqueue);
	load_flags(state);
}

void
remove_task(struct task_t *task)
{
	uint32_t state;

	save_flags(&state);
	cli();
	list_del(&task->q_task);
	kfree(task->name);
	kfree(task->stack);
	kfree(task);
	load_flags(state);
}

int
fork(void)
{
	int ret = -ENOMEM, i;
	uint32_t offset;
	struct task_t *task, *parent;
	size_t len;

	parent = curr_proc;
	task = kmalloc(sizeof(*task));
	if (IS_ERR(task)) {
		ret = PTR_ERR(task);
		goto err;
	}
	memset(task, 0, sizeof(*task));
	len = strlen(parent->name) + 1;
	assert(len);
	task->name = kmalloc(len);
	if (IS_ERR(task->name)) {
		ret = PTR_ERR(task->name);
		goto err1;
	}
	strncpy(task->name, parent->name, len);
	task->name[len - 1] = '\0';
	INIT_LIST_HEAD(&task->l_regions);
	task->parent = parent;
	task->pid = get_new_pid();
	task->state = task->old_state = TASK_RUNNABLE;
	task->flags = parent->flags;
	task->uid = parent->uid;
	task->gid = parent->gid;
	task->fuid = parent->fuid;
	task->fgid = parent->fgid;
	task->cdir = parent->cdir;
	for (i = 0; i < NR_MAX_OPEN_FILES; ++i) {
		task->fdtable[i] = parent->fdtable[i];
		if (task->fdtable[i]->f_state == FILE_ALLOC)
			++task->fdtable[i]->f_refcount;
	}
	for (i = 0; i < NR_MAX_OPEN_FILES; ++i)
		task->pipebufs[i] = parent->pipebufs[i];
	task->stack = kmalloc(TASK_STACK_SIZE);
	if (IS_ERR(task->stack)) {
		ret = PTR_ERR(task->stack);
		goto err2;
	}
	memset(task->stack, 0, TASK_STACK_SIZE);
	task->page_dir = clone_page_dir();
	if (IS_ERR(task->page_dir)) {
		ret = PTR_ERR(task->page_dir);
		goto err3;
	}
	memcpy(task->stack, parent->stack, TASK_STACK_SIZE);
	/*
	 * perhaps by patching all ebps in the parent stack, the child will be able to
	 * return properly even if SYS_FORK is not inlined
	 */
	offset = (uint32_t)((uint8_t *)parent->stack
			    + TASK_STACK_SIZE
			    - parent->esp);
	task->cf = (struct cswitch_frame_t *)((uint8_t *)task->stack
					      + TASK_STACK_SIZE
					      - offset
					      - sizeof(struct cswitch_frame_t));
	*task->cf = *parent->cf;
	offset = (uint32_t)((uint8_t *)parent->stack
			    + TASK_STACK_SIZE
			    - parent->cf->ebp);
	task->cf->ebp = (uint32_t)((uint8_t *)task->stack
				   + TASK_STACK_SIZE
				   - offset);
	task->cf->eax = 0;
	task->esp = (uint32_t)task->cf;
	/* TODO: create userspace stuff */
	attach_tty(task);
	add_task(task);
	return task->pid;
err3:
	kfree(task->stack);
err2:
	kfree(task->name);
err1:
	kfree(task);
err:
	return ret;
}

/* TODO: test this one */
pid_t
waitpid(pid_t pid, int *status, int options)
{
	struct list_head *iter, *q;
	struct task_t *task;
	pid_t retpid;
	int haschld, pidexists;

	assert(options == WNOHANG);
	if (options != WNOHANG)
		return -EINVAL;

	haschld = 0;
	pidexists = (pid == -1) ? 1 : 0;
	for (;;) {
		list_for_each_safe(iter, q, &runqueue) {
			task = list_entry(iter, struct task_t, q_task);
			if (pid != -1 && task->pid == pid)
				pidexists = 1;
			if (task->parent != curr_proc)
				continue;
			haschld = 1;
			if (task->state != TASK_ZOMBIE)
				continue;
			if (status) *status = (int)task->status;
			retpid = task->pid;
			remove_task(task);
			goto out;
		}
		if (!pidexists)
			return -ECHILD;
		if (options == WNOHANG) {
			if (haschld)
				return 0;
			return -ECHILD;
		}
	}
out:
	return retpid;
}

static uint32_t
hash_chan(void *chan)
{
	uint32_t *c = chan, hash = 0, i;

	for (i = 0; i < 32; ++i)
		hash = (*c & (1lu << i)) + (hash << 6) + (hash << 16) - hash;
	return hash;
}

/* can be called as follows:
 * syscall -> suspend_task (from a syscall)
 * suspend_task (normally)
 * NOTE: suspend_task will fail if it is called
 * from nested syscalls (syscall depth > 1)
 */
void
suspend_task(void *channel)
{
	uint32_t i;

	curr_proc->state = TASK_SLEEPING;
	list_del(&curr_proc->q_task);
	i = hash_chan(channel) % NR_HASH_ENT;
	list_add_tail(&curr_proc->q_task, &htsleep[i]);
	schedule();
}

int
resume_task(void *channel)
{
	uint32_t i;
	struct list_head *iter, *q;
	struct task_t *task;

	i = hash_chan(channel) % NR_HASH_ENT;
	list_for_each_safe(iter, q, &htsleep[i]) {
		task = list_entry(iter, struct task_t, q_task);
		assert(task->state == TASK_SLEEPING);
		task->state = TASK_RUNNABLE;
		list_del(&task->q_task);
		list_add(&task->q_task, &runqueue);
	}
	return 0;
}

uid_t
getuid(void)
{
	return curr_proc->uid;
}

int
setuid(__attribute__ ((unused)) uid_t uid)
{
	/* TODO: perform additional checks */
	curr_proc->uid = uid; /* w00t w00t */
	return 0;
}

gid_t
getgid(void)
{
	return curr_proc->gid;
}

int
setgid(__attribute__ ((unused)) gid_t gid)
{
	/* TODO: perform additional checks */
	curr_proc->gid = gid;
	return 0;
}

