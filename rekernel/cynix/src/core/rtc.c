/*
 *  core/rtc.c
 *
 *  Copyright (C) 2009 stateless
 */

#include <rtc.h>
#include <common.h>
#include <x86.h>
#include <idt.h>
#include <tss.h>
#include <tty.h>

volatile uint32_t systimer = 0;

static uint32_t curr_freq = 100;

void
rtc_callback(__attribute__ ((unused)) struct trapframe_t *regs)
{
	++systimer;
	if (systimer % 500)
		blink();
	schedule();
}

void
init_rtc(uint32_t freq)
{
	register_isr_handler(32, rtc_callback);

	curr_freq = freq;
	uint32_t divisor = 1193182 / freq;
	/* channel: 0, access mode: lo/hi byte, operating mode: square wave, binary mode */
	outb(0x43, 0x36);
	outb(0x40, (divisor & 0xff));
	outb(0x40, ((divisor >> 8) & 0xff));
}

void
sleep(uint32_t seconds)
{
	while (seconds--) {
		uint32_t now = systimer;
		while (systimer < now + curr_freq)
			;
	}
}

void
msleep(uint32_t msec)
{
	uint32_t s = systimer;
	while (systimer < s + msec)
		;
	return;
}

