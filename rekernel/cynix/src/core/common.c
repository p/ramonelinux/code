/*
 *  core/common.c
 *
 *  Copyright (C) 2009 stateless
 */

#include <common.h>
#include <tty.h>
#include <ctype.h>
#include <elf.h>
#include <serial.h>
#include <x86.h>
#include <tss.h>

extern uint8_t __code[0];
extern uint8_t __end[0];
extern unsigned long int initial_esp;

void
dump_regs(void)
{
	uint32_t eax, ebx, ecx, edx, esp, ebp;
	uint32_t edi, esi;

	asm volatile ("movl %%eax, %0" : "=r"(eax));
	asm volatile ("movl %%ebx, %0" : "=r"(ebx));
	asm volatile ("movl %%ecx, %0" : "=r"(ecx));
	asm volatile ("movl %%edx, %0" : "=r"(edx));
	asm volatile ("movl %%esp, %0" : "=r"(esp));
	asm volatile ("movl %%ebp, %0" : "=r"(ebp));
	asm volatile ("movl %%esi, %0" : "=r"(esi));
	asm volatile ("movl %%edi, %0" : "=r"(edi));

	printf("eax = 0x%08lx\nebx = 0x%08lx\necx = 0x%08lx\n",
	       eax, ebx, ecx);
	printf("edx = 0x%08lx\nesp = 0x%08lx\nebp = 0x%08lx\n",
	       edx, esp, ebp);
	printf("esi = 0x%08lx\nedi = 0x%08lx\n",
	       esi, edi);
	serial_dump("eax = 0x%08lx\nebx = 0x%08lx\necx = 0x%08lx\n",
		    eax, ebx, ecx);
	serial_dump("edx = 0x%08lx\nesp = 0x%08lx\nebp = 0x%08lx\n",
		    edx, esp, ebp);
	serial_dump("esi = 0x%08lx\nedi = 0x%08lx\n",
		    esi, edi);
}

void
panic_internal(const char *file, const char *fn, uint32_t line, const char *fmt, ...)
{
	va_list ap;
	char buf[512], line_s[32];

	asm volatile ("cli");
	printf("~~~ kernel panic ~~~\n");
	stacktrace(8);
	itoa(line, line_s, 10);
	printf("%s:%s:%s ", file, fn, line_s);
	va_start(ap, fmt);
	vsprintf(buf, fmt, ap);
	va_end(ap);
	printf("%s\n", buf);
	update_cursor(cursor_y, cursor_x);
	serial_dump("~~~ kernel panic ~~~\n");
	serial_dump("%s:%s:%s ", file, fn, line_s);
	serial_dump("%s\n", buf);
	dump_regs();
	asm volatile ("hlt");
}

void
hexdump(const void *ptr, size_t len)
{
	const char *addr = (const char *)ptr;
	size_t i;
	int j;

	for (i = 0; i < len; i += 16) {
		serial_dump("0x%08lx: ", (uint32_t)addr);
		serial_dump("0x%08lx 0x%08lx 0x%08lx 0x%08lx |",
			    *(uint32_t *)addr,
			    *(uint32_t *)(addr + 4),
			    *(uint32_t *)(addr + 8),
			    *(uint32_t *)(addr + 12)
			   );
		printf("0x%08lx: ", (uint32_t)addr);
		printf("0x%08lx 0x%08lx 0x%08lx 0x%08lx |",
		       *(uint32_t *)addr,
		       *(uint32_t *)(addr + 4),
		       *(uint32_t *)(addr + 8),
		       *(uint32_t *)(addr + 12)
		      );

		for (j = 0; j < 16; ++j) {
			serial_dump("%c", (isalpha(addr[j])) ? addr[j] : '.');
			printf("%c", (isalpha(addr[j])) ? addr[j] : '.');
		}
		serial_dump("|\n");
		printf("|\n");
		addr += 16;
	}
}

void
stacktrace(int depth)
{
	uint32_t *addr, ebp, stack_top;
	const char *p;

	stack_top = (!curr_proc) ? initial_esp :
		    (uint32_t)curr_proc->stack + TASK_STACK_SIZE - sizeof(struct cswitch_frame_t);
	GET_EBP(ebp);
	addr = (uint32_t *)(ebp + 4);
	if (curr_proc)
		printf("Stack trace for thread [%d:%s]\n", curr_proc->pid, curr_proc->name);
	printf("\t\t%-12s  %-12s\n", "Address", "Function");
	while (depth--) {
		p = find_symbol(*addr);
		if (!p)
			++depth;
		else
			printf("\t\t0x%-12.8lx%-12s\n", *addr, p);
		for (;;) {
			if ((uint32_t)addr + 4 >= stack_top)
				return;
			addr += 4;
			/* this is just a heuristic */
			if (*addr > (uint32_t)__code && *addr < (uint32_t)__end)
				break;
		}
	}
}

