/*
 *  core/idt.c
 *
 *  Copyright (C) 2009 stateless
 */

#include <idt.h>
#include <common.h>
#include <x86.h>
#include <string.h>
#include <syscall.h>
#include <tss.h>
#include <errno.h>
#include <tty.h>
#include <pic.h>

#define INIT_IDT_INTR_GATE(index, offset, sel, gsize, pl, sp) \
	({ \
		idt_intr_gates[index].offset_lo = (uint16_t)offset; \
		idt_intr_gates[index].selector  = sel; \
		idt_intr_gates[index].all0      = 0; \
		idt_intr_gates[index].gate_size = gsize; \
		idt_intr_gates[index].privilege_level = pl; \
		idt_intr_gates[index].segment_present = sp; \
		idt_intr_gates[index].offset_hi = (uint16_t)(offset >> 16) & 0xffff; \
	})

extern void idt_flush(uint32_t);
extern void syscall_handler(void);

static const char *interrupt_names[] = {
	"Division by zero exception",
	"Debug exception",
	"Non maskable interrupt",
	"Breakpoint exception",
	"Into detected overflow",
	"Out of bounds exception",
	"Invalid opcode exception",
	"No coprocessor exception",
	"Double fault",
	"Coprocessor segment overrun",
	"Bad TSS",
	"Segment not present",
	"Stack fault",
	"General protection fault",
	"Page fault",
	"Unknown interrupt exception",
	"Coprocessor fault",
	"Alignment check exception",
	"Machine check exception"
};

static isr_handler isr_handlers[256];
static struct idt_intr_gate idt_intr_gates[256];
static struct idt_selector idt_intr_sel;

static void
spurious_irq7(__attribute__ ((unused)) struct trapframe_t *regs)
{
}

void
initialize_idt(void)
{
	idt_intr_sel.limit = sizeof(struct idt_intr_gate) * 256 - 1;
	idt_intr_sel.base = (uint32_t)idt_intr_gates;

	memset(idt_intr_gates, 0, sizeof(struct idt_intr_gate) * 256);

	INIT_IDT_INTR_GATE(0, (uint32_t)isr0, 0x08, 0xe, 0x0, 0x1);
	INIT_IDT_INTR_GATE(1, (uint32_t)isr1, 0x08, 0xe, 0x0, 0x1);
	INIT_IDT_INTR_GATE(2, (uint32_t)isr2, 0x08, 0xe, 0x0, 0x1);
	INIT_IDT_INTR_GATE(3, (uint32_t)isr3, 0x08, 0xe, 0x0, 0x1);
	INIT_IDT_INTR_GATE(4, (uint32_t)isr4, 0x08, 0xe, 0x0, 0x1);
	INIT_IDT_INTR_GATE(5, (uint32_t)isr5, 0x08, 0xe, 0x0, 0x1);
	INIT_IDT_INTR_GATE(6, (uint32_t)isr6, 0x08, 0xe, 0x0, 0x1);
	INIT_IDT_INTR_GATE(7, (uint32_t)isr7, 0x08, 0xe, 0x0, 0x1);
	INIT_IDT_INTR_GATE(8, (uint32_t)isr8, 0x08, 0xe, 0x0, 0x1);
	INIT_IDT_INTR_GATE(9, (uint32_t)isr9, 0x08, 0xe, 0x0, 0x1);
	INIT_IDT_INTR_GATE(10, (uint32_t)isr10, 0x08, 0xe, 0x0, 0x1);
	INIT_IDT_INTR_GATE(11, (uint32_t)isr11, 0x08, 0xe, 0x0, 0x1);
	INIT_IDT_INTR_GATE(12, (uint32_t)isr12, 0x08, 0xe, 0x0, 0x1);
	INIT_IDT_INTR_GATE(13, (uint32_t)isr13, 0x08, 0xe, 0x0, 0x1);
	INIT_IDT_INTR_GATE(14, (uint32_t)isr14, 0x08, 0xe, 0x0, 0x1);
	INIT_IDT_INTR_GATE(15, (uint32_t)isr15, 0x08, 0xe, 0x0, 0x1);
	INIT_IDT_INTR_GATE(16, (uint32_t)isr16, 0x08, 0xe, 0x0, 0x1);
	INIT_IDT_INTR_GATE(17, (uint32_t)isr17, 0x08, 0xe, 0x0, 0x1);
	INIT_IDT_INTR_GATE(18, (uint32_t)isr18, 0x08, 0xe, 0x0, 0x1);
	INIT_IDT_INTR_GATE(19, (uint32_t)isr19, 0x08, 0xe, 0x0, 0x1);
	INIT_IDT_INTR_GATE(20, (uint32_t)isr20, 0x08, 0xe, 0x0, 0x1);
	INIT_IDT_INTR_GATE(21, (uint32_t)isr21, 0x08, 0xe, 0x0, 0x1);
	INIT_IDT_INTR_GATE(22, (uint32_t)isr22, 0x08, 0xe, 0x0, 0x1);
	INIT_IDT_INTR_GATE(23, (uint32_t)isr23, 0x08, 0xe, 0x0, 0x1);
	INIT_IDT_INTR_GATE(24, (uint32_t)isr24, 0x08, 0xe, 0x0, 0x1);
	INIT_IDT_INTR_GATE(25, (uint32_t)isr25, 0x08, 0xe, 0x0, 0x1);
	INIT_IDT_INTR_GATE(26, (uint32_t)isr26, 0x08, 0xe, 0x0, 0x1);
	INIT_IDT_INTR_GATE(27, (uint32_t)isr27, 0x08, 0xe, 0x0, 0x1);
	INIT_IDT_INTR_GATE(28, (uint32_t)isr28, 0x08, 0xe, 0x0, 0x1);
	INIT_IDT_INTR_GATE(29, (uint32_t)isr29, 0x08, 0xe, 0x0, 0x1);
	INIT_IDT_INTR_GATE(30, (uint32_t)isr30, 0x08, 0xe, 0x0, 0x1);
	INIT_IDT_INTR_GATE(31, (uint32_t)isr31, 0x08, 0xe, 0x0, 0x1);
	INIT_IDT_INTR_GATE(32, (uint32_t)isr32, 0x08, 0xe, 0x0, 0x1);
	INIT_IDT_INTR_GATE(33, (uint32_t)isr33, 0x08, 0xe, 0x0, 0x1);
	INIT_IDT_INTR_GATE(34, (uint32_t)isr34, 0x08, 0xe, 0x0, 0x1);
	INIT_IDT_INTR_GATE(35, (uint32_t)isr35, 0x08, 0xe, 0x0, 0x1);
	INIT_IDT_INTR_GATE(36, (uint32_t)isr36, 0x08, 0xe, 0x0, 0x1);
	INIT_IDT_INTR_GATE(37, (uint32_t)isr37, 0x08, 0xe, 0x0, 0x1);
	INIT_IDT_INTR_GATE(38, (uint32_t)isr38, 0x08, 0xe, 0x0, 0x1);
	INIT_IDT_INTR_GATE(39, (uint32_t)isr39, 0x08, 0xe, 0x0, 0x1);
	INIT_IDT_INTR_GATE(40, (uint32_t)isr40, 0x08, 0xe, 0x0, 0x1);
	INIT_IDT_INTR_GATE(41, (uint32_t)isr41, 0x08, 0xe, 0x0, 0x1);
	INIT_IDT_INTR_GATE(42, (uint32_t)isr42, 0x08, 0xe, 0x0, 0x1);
	INIT_IDT_INTR_GATE(43, (uint32_t)isr43, 0x08, 0xe, 0x0, 0x1);
	INIT_IDT_INTR_GATE(44, (uint32_t)isr44, 0x08, 0xe, 0x0, 0x1);
	INIT_IDT_INTR_GATE(45, (uint32_t)isr45, 0x08, 0xe, 0x0, 0x1);
	INIT_IDT_INTR_GATE(46, (uint32_t)isr46, 0x08, 0xe, 0x0, 0x1);
	INIT_IDT_INTR_GATE(47, (uint32_t)isr47, 0x08, 0xe, 0x0, 0x1);
	INIT_IDT_INTR_GATE(0x80, (uint32_t)isr128, 0x08, 0xe, 0x0, 0x1);

	register_isr_handler(0x80, syscall_dispatcher);
	register_isr_handler(39, spurious_irq7);
	idt_flush((uint32_t)&idt_intr_sel);
}

int
register_isr_handler(uint8_t index, isr_handler handler)
{
	if (!handler)
		return -EINVAL;
	isr_handlers[index] = handler;
	return 0;
}

uint32_t
handle_interrupt(struct trapframe_t regs)
{
	if (isr_handlers[regs.int_no]) {
		if (curr_proc) {
			curr_proc->esp = regs.unroll_esp;
			if (regs.int_no == 0x80 && regs.eax != __NR_suspend_task)
				curr_proc->old_esp = curr_proc->esp;
			curr_proc->cf = (struct cswitch_frame_t *)((uint32_t *) & regs + 1);
		}
		isr_handler handler = isr_handlers[regs.int_no];
		handler(&regs);
	} else {
		clear_scr();
		ccurr_col(BLACK, RED);
		DEBUG_CODE(printf("Unhandled interrupt[%lu]: %s\n", regs.int_no,
				  (regs.int_no < (sizeof(interrupt_names) / sizeof(interrupt_names[0])))
				  ? interrupt_names[regs.int_no] : "Unknown");)
		DUMP_STACK_REGS(regs);
		panic("halted");
	}
	pic_eoi(regs.int_no);
	if (!curr_proc)
		return regs.unroll_esp;
	return curr_proc->esp;
}

