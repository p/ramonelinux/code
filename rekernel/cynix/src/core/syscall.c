/*
 *  core/syscall.c
 *
 *  Copyright (C) 2009 stateless
 */

#include <syscall.h>
#include <string.h>
#include <x86.h>
#include <common.h>
#include <tss.h>
#include <serial.h>
#include <rtc.h>
#include <mm.h>
#include <tty.h>
#include <errno.h>
#include <pipe.h>

extern int f_kdb;

static void __sys_write(struct trapframe_t *regs);
static void __sys_read(struct trapframe_t *regs);
static void __sys_exit(struct trapframe_t *regs);
static void __sys_fork(struct trapframe_t *regs);
static void __sys_waitpid(struct trapframe_t *regs);
static void __sys_reschedule(struct trapframe_t *regs);
static void __sys_open(struct trapframe_t *regs);
static void __sys_close(struct trapframe_t *regs);
static void __sys_creat(struct trapframe_t *regs);
static void __sys_execve(struct trapframe_t *regs);
static void __sys_isatty(struct trapframe_t *regs);
static void __sys_getpid(struct trapframe_t *regs);
static void __sys_getppid(struct trapframe_t *regs);
static void __sys_readdir(struct trapframe_t *regs);
static void __sys_dup(struct trapframe_t *regs);
static void __sys_dup2(struct trapframe_t *regs);
static void __sys_stat(struct trapframe_t *regs);
static void __sys_fstat(struct trapframe_t *regs);
static void __sys_seeder(struct trapframe_t *regs);
static void __sys_sbrk(struct trapframe_t *regs);
static void __sys_getuid(struct trapframe_t *regs);
static void __sys_setuid(struct trapframe_t *regs);
static void __sys_getgid(struct trapframe_t *regs);
static void __sys_setgid(struct trapframe_t *regs);
static void __sys_suspend_task(struct trapframe_t *regs);
static void __sys_resume_task(struct trapframe_t *regs);
static void __sys_pipe(struct trapframe_t *regs);

static void *syscall_table[SYSCALL_NR] = {
	[__NR_exit] = &__sys_exit,
	[__NR_fork] = &__sys_fork,
	[__NR_read] = &__sys_read,
	[__NR_write] = &__sys_write,
	[__NR_waitpid] = &__sys_waitpid,
	[__NR_reschedule] = &__sys_reschedule,
	[__NR_open] = &__sys_open,
	[__NR_close] = &__sys_close,
	[__NR_creat] = &__sys_creat,
	[__NR_execve] = &__sys_execve,
	[__NR_isatty] = &__sys_isatty,
	[__NR_getpid] = &__sys_getpid,
	[__NR_getppid] = &__sys_getppid,
	[__NR_readdir] = &__sys_readdir,
	[__NR_dup] = &__sys_dup,
	[__NR_dup2] = &__sys_dup2,
	[__NR_stat] = &__sys_stat,
	[__NR_fstat] = &__sys_fstat,
	[__NR_seeder] = &__sys_seeder,
	[__NR_sbrk] = &__sys_sbrk,
	[__NR_getuid] = &__sys_getuid,
	[__NR_setuid] = &__sys_setuid,
	[__NR_getgid] = &__sys_getgid,
	[__NR_setgid] = &__sys_setgid,
	[__NR_suspend_task] = &__sys_suspend_task,
	[__NR_resume_task] = &__sys_resume_task,
	[__NR_pipe] = &__sys_pipe
};

#ifdef BSYSCALL_TRACE
static const char *syscall_names[SYSCALL_NR] = {
	[__NR_exit] = "sys_exit",
	[__NR_fork] = "sys_fork",
	[__NR_read] = "sys_read",
	[__NR_write] = "sys_write",
	[__NR_waitpid] = "sys_waitpid",
	[__NR_reschedule] = "sys_reschedule",
	[__NR_open] = "sys_open",
	[__NR_close] = "sys_close",
	[__NR_creat] = "sys_creat",
	[__NR_execve] = "sys_execve",
	[__NR_isatty] = "sys_isatty",
	[__NR_getpid] = "sys_getpid",
	[__NR_getppid] = "sys_getppid",
	[__NR_readdir] = "sys_readdir",
	[__NR_dup] = "sys_dup",
	[__NR_dup2] = "sys_dup2",
	[__NR_stat] = "sys_stat",
	[__NR_fstat] = "sys_fstat",
	[__NR_seeder] = "sys_seeder",
	[__NR_sbrk] = "sys_sbrk",
	[__NR_getuid] = "sys_getuid",
	[__NR_setuid] = "sys_setuid",
	[__NR_getgid] = "sys_getgid",
	[__NR_setgid] = "sys_setgid",
	[__NR_suspend_task] = "sys_suspend_task",
	[__NR_resume_task] = "sys_resume_task",
	[__NR_pipe] = "sys_pipe"
};
#endif

static void
__sys_write(struct trapframe_t *regs)
{
	regs->eax = (uint32_t)write((int)regs->ebx, (const void *)regs->ecx, (size_t)regs->edx);
}

static void
__sys_read(struct trapframe_t *regs)
{
	regs->eax = (uint32_t)read((int)regs->ebx, (void *)regs->ecx, (size_t)regs->edx);
}

static void
__sys_exit(struct trapframe_t *regs)
{
	if (!strcmp(curr_proc->name, "kdb")) {
		unfreeze_tasks();
		f_kdb = 0;
	}
	curr_proc->state = TASK_ZOMBIE;
	curr_proc->status = (int)regs->ebx;
	attach_tty(curr_proc->parent);
	tty_resume();
	closefds();
	remove_proc_mappings();
	schedule();
}

static void
__sys_fork(struct trapframe_t *regs)
{
	regs->eax = (uint32_t)fork();
}

static void
__sys_waitpid(struct trapframe_t *regs)
{
	regs->eax = (uint32_t)waitpid((pid_t)regs->ebx, (int *)regs->ecx, (int)regs->edx);
}

static void
__sys_reschedule(__attribute__ ((unused)) struct trapframe_t *regs)
{
	schedule();
}

static void
__sys_open(struct trapframe_t *regs)
{
	regs->eax = (uint32_t)open((const char *)regs->ebx, (int)regs->ecx, (mode_t)regs->edx);
}

static void
__sys_close(struct trapframe_t *regs)
{
	regs->eax = (uint32_t)close((int)regs->ebx);
}

static void
__sys_creat(struct trapframe_t *regs)
{
	regs->eax = (int)creat((const char *)regs->ebx, (mode_t)regs->ecx);
}

static void
__sys_execve(__attribute__ ((unused)) struct trapframe_t *regs)
{
	regs->eax = execve((const char *)regs->ebx, 0, 0); /* FIXME */
}

static void
__sys_isatty(struct trapframe_t *regs)
{
	regs->eax = 1;
}

static void
__sys_getpid(struct trapframe_t *regs)
{
	regs->eax = (uint32_t)curr_proc->pid;
}

static void
__sys_getppid(struct trapframe_t *regs)
{
	regs->eax = (!curr_proc->parent) ? 0 : (uint32_t)curr_proc->parent->pid;
}

static void
__sys_readdir(struct trapframe_t *regs)
{
	/* I will probably need to change the prototype of readdir to not depend on DIR */
	regs->eax = (uint32_t)readdir((DIR *)regs->ebx);
}

static void
__sys_dup(struct trapframe_t *regs)
{
	regs->eax = (uint32_t)dup((int)regs->ebx);
}

static void
__sys_dup2(struct trapframe_t *regs)
{
	regs->eax = (uint32_t)dup2((int)regs->ebx, (int)regs->ecx);
}

static void
__sys_stat(struct trapframe_t *regs)
{
	regs->eax = (uint32_t)stat((const char *)regs->ebx, (struct stat *)regs->ecx);
}

static void
__sys_fstat(struct trapframe_t *regs)
{
	regs->eax = (uint32_t)fstat((int)regs->ebx, (struct stat *)regs->ecx);
}

static void
__sys_seeder(struct trapframe_t *regs)
{
	regs->eax = systimer ^ (uint32_t)curr_proc;
}

static void
__sys_sbrk(struct trapframe_t *regs)
{
	regs->eax = (uint32_t)sbrk((intptr_t)regs->ebx);
}

static void
__sys_getuid(struct trapframe_t *regs)
{
	regs->eax = (uint32_t)getuid();
}

static void
__sys_setuid(struct trapframe_t *regs)
{
	regs->eax = (uint32_t)setuid((uid_t)regs->ebx);
}

static void
__sys_getgid(struct trapframe_t *regs)
{
	regs->eax = (uint32_t)getgid();
}

static void
__sys_setgid(struct trapframe_t *regs)
{
	regs->eax = (uint32_t)setgid((uid_t)regs->ebx);
}

static void
__sys_suspend_task(struct trapframe_t *regs)
{
	suspend_task((void *)regs->ebx);
}

static void
__sys_resume_task(struct trapframe_t *regs)
{
	regs->eax = (uint32_t)resume_task((void *)regs->ebx);
}

static void
__sys_pipe(struct trapframe_t *regs)
{
	regs->eax = (uint32_t)pipe((int *)regs->ebx);
}

void
syscall_dispatcher(struct trapframe_t *regs)
{
	if (regs->eax > 0 && regs->eax < SYSCALL_NR && syscall_table[regs->eax]) {
		if (curr_proc) {
			TRACE_SYSCALL(serial_dump("tracing[%s with pid %d]: %s [eax = %08lx, ebx = %08lx, ecx = %08lx, edx = %08lx]\n",
						  curr_proc->name, curr_proc->pid, syscall_names[regs->eax],
						  regs->eax, regs->ebx, regs->ecx, regs->edx);)
		}
		void (*fp)(struct trapframe_t * regs) = (void (*)(struct trapframe_t *))syscall_table[regs->eax];
		fp(regs);
	} else {
		regs->eax = -ENOSYS;
	}
}

