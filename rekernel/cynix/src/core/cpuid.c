/*
 *  core/cpuid.c
 *
 *  Copyright (C) 2010 stateless
 */

#include <x86.h>
#include <stdint.h>

#define cpuid(code, eax, ebx, ecx, edx) \
	({ \
		uint32_t __code = code; \
		asm volatile ("cpuid" : "=a"(eax), "=b"(ebx), "=c"(ecx), "=d"(edx) : "0"(__code)); \
	})

static const char *intel_brands[] = {
	"Brand not supported",
	"Intel(R) Celeron(R) processor",
	"Intel(R) Pentium(R) III processor",
	"Intel(R) Pentium(R) III Xeon(R) processor",
	"Intel(R) Pentium(R) III processor",
	"Reserved",
	"Mobile Intel(R) Pentium(R) III processor-M",
	"Mobile Intel(R) Celeron(R) processor",
	"Intel(R) Pentium(R) 4 processor",
	"Intel(R) Pentium(R) 4 processor",
	"Intel(R) Celeron(R) processor",
	"Intel(R) Xeon(R) Processor",
	"Intel(R) Xeon(R) processor MP",
	"Reserved",
	"Mobile Intel(R) Pentium(R) 4 processor-M",
	"Mobile Intel(R) Pentium(R) Celeron(R) processor",
	"Reserved",
	"Mobile Genuine Intel(R) processor",
	"Intel(R) Celeron(R) M processor",
	"Mobile Intel(R) Celeron(R) processor",
	"Intel(R) Celeron(R) processor",
	"Mobile Geniune Intel(R) processor",
	"Intel(R) Pentium(R) M processor",
	"Mobile Intel(R) Celeron(R) processor"
};

static void
print_vendor(uint32_t str[4])
{
	printf("vendor_id: ");
	printf("%c%c%c%c", (str[1] & 0xff), (str[1] >> 8) & 0xff,
	       (str[1] >> 16) & 0xff, (str[1] >> 24) & 0xff);
	printf("%c%c%c%c", (str[3] & 0xff), (str[3] >> 8) & 0xff,
	       (str[3] >> 16) & 0xff, (str[3] >> 24) & 0xff);
	printf("%c%c%c%c", (str[2] & 0xff), (str[2] >> 8) & 0xff,
	       (str[2] >> 16) & 0xff, (str[2] >> 24) & 0xff);
	putchar('\n');
}

void
do_cpuid(void)
{
	uint32_t output[4], family, brand, a, b, d;
	uint32_t unused;

	cpuid(0, output[0], output[1], output[2], output[3]);
	printf("processor: 0\n");
	print_vendor(output);
	if (output[1] != 0x756e6547) { /* not an Intel cpu */
		info("can't get any more info out of this cpu!\n");
		return;
	}

	cpuid(1, a, b, unused, d);
	family = (a >> 8) & 0xf;
	printf("cpu_family: %s\n",
	       (family == 4) ? "i486" :
	       (family == 5) ? "Pentium" :
	       (family == 6) ? "Pentium Pro" : "Unknown");
	brand = (b & 0xff);
	if (brand < sizeof(intel_brands) / sizeof(intel_brands[0]))
		printf("model_name: %s\n", intel_brands[brand]);
	else
		printf("model_name: Unknown\n");
}

