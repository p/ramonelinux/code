/*
 *  core/ext2.c
 *
 *  Copyright (C) 2009 stateless
 */

#include <ext2.h>
#include <common.h>
#include <errno.h>
#include <string.h>
#include <heap.h>
#include <x86.h>
#include <mm.h>
#include <syscall.h>
#include <elf.h>
#include <tty.h>
#include <unistd.h>
#include <pipe.h>

#define EXT2_SETERRNO_AND_RET(err, ret) SETERRNO_AND_RET(err, ret)

spinlock_t flist_lock;
struct list_head flist;

static struct ext2_superblock_t *sblock = NULL;
static struct ext2_inode_t *root_inode = NULL;
static char *fs_base_addr = NULL;

static void *readblock(uint32_t block_no);
static struct ext2_inode_t *geti(ino_t ino);
static struct ext2_gdt_t *readgdt(uint32_t gdt);
static int read_inode_blocks(struct ext2_inode_t *inode, off_t offs, char *buffer, size_t nbytes);
static int check_inode_status(ino_t inode);
static struct ext2_dir_t *resolve_path_internal(struct ext2_inode_t *inode, const char *path);
static struct ext2_dir_t *resolve_path(const char *path);
static struct ext2_dir_t *find_dentry(struct ext2_inode_t *inode, const char *name, uint32_t is_root);
static int filealloc(ino_t inode_nr);
static blksize_t get_block_size(void);

static blksize_t
get_block_size(void)
{
	assert(sblock);
	return 1024 << sblock->block_size;
}

static inline void *
readblock(uint32_t block_no)
{
	assert(sblock);
	return fs_base_addr + get_block_size() * block_no;
}

static inline struct ext2_gdt_t *
readgdt(uint32_t gdt) {
	char *block;

	block = readblock(2);
	return (struct ext2_gdt_t *)(block + (gdt * sizeof(struct ext2_gdt_t)));
}

static inline struct ext2_inode_t *
geti(ino_t ino) {
	struct ext2_gdt_t *gdt;

	gdt = readgdt((ino - 1) / sblock->nr_inodes_per_group);
	return (struct ext2_inode_t *)((char *)readblock(gdt->addr_inode_table) + (ino - 1) * sblock->inode_size);
}

/* should be called with flist_lock held */
__attribute__ ((unused)) static void
dump_flist(void)
{
	struct list_head *iter;
	struct file_t *f;

	list_for_each(iter, &flist) {
		f = list_entry(iter, struct file_t, f_listopen);
		printf("inode = 0x%08lx, off = %lu, state = %s, ref_count = %d\n",
		       (void *)f->f_inode,
		       f->f_off,
		       (f->f_state == FILE_ALLOC) ? "FILE_ALLOC" : "FILE_NONE",
		       f->f_refcount
		      );
	}
}

int
ext2_mount(char *addr)
{
	int i;

	if (!addr) return -EINVAL;
	fs_base_addr = addr;
	sblock = (struct ext2_superblock_t *)(fs_base_addr + 1024);
	if (sblock->magic != 0xef53) {
		info("invalid ext2 volume!\n");
		sblock = NULL;
		return -EILSEQ;
	}
	root_inode = geti(2);
	if (!(root_inode->file_mode & DIRECTORY_T)) {
		info("can't locate the root directory of the filesystem!\n");
		return -ENOTDIR;
	}
	for (i = 1; i <= 10; ++i)
		if (check_inode_status(i) < 0) {
			info("inode validation failed!\n");
			return -EIO;
		}
	INIT_LIST_HEAD(&flist);
	initspinlock(&flist_lock);
	return 0;
}

struct ext2_inode_t *
get_root_inode(void) {
	assert(root_inode);
	return root_inode;
}

static int
read_inode_blocks(struct ext2_inode_t *inode, off_t offs /* in blocks */, char *buffer, size_t nbytes)
{
	int ret = -EINVAL, off = 0;
	uint32_t *block, i;
	blkcnt_t nr_blocks;
	blksize_t blksize;
	char *tmp;

	if (!inode || !buffer)
		return ret;
	if (!nbytes)
		return 0;

	/* TODO: make sure this code is bug-proof */
	blksize = get_block_size();
	nr_blocks = inode->size_low32 / blksize;
	if (nr_blocks >= 12 + (blksize / 4)
			|| (nbytes / blksize) + offs >= 12 + (blksize / 4)) {
		info("can't handle double indirect blocks yet!\n");
		return -EIO;
	}
	if (offs >= 12) {
		offs -= 12;
		goto read_indirect;
	}
	block = (uint32_t *)inode->direct_blocks;
	block += offs;
	for (i = offs / blksize; nbytes && i <= nr_blocks; ++i, ++block) {
		if (!*block)
			continue;
		tmp = readblock(*block);
		memcpy((char *)buffer + off, tmp, blksize);
		nbytes -= blksize;
		off += blksize;
		if (off / blksize >= 12 && nbytes)
			goto read_indirect;
	}
	if (!nbytes) return off;
read_indirect:
	block = (uint32_t *)readblock(inode->single_indir_block);
	block += offs;
	for (i = offs / blksize; nbytes && i <= nr_blocks; ++i, ++block) {
		if (!*block)
			continue;
		tmp = readblock(*block);
		memcpy((char *)buffer + off, tmp, blksize);
		nbytes -= blksize;
		off += blksize;
	}
	return off;
}

static struct ext2_dir_t *
find_dentry(struct ext2_inode_t *inode, const char *name, uint32_t is_root) {
	char *buffer;
	struct ext2_dir_t *dir, *retdir;
	int ret;
	blkcnt_t nr_blocks;
	blksize_t blksize;

	if (!name)
		return ERR_PTR(-EINVAL);

	blksize = get_block_size();
	buffer = kmalloc(blksize);
	if (IS_ERR(buffer))
		return (void *)buffer;
	nr_blocks = inode->size_low32 / blksize;
	for (uint32_t j = 0; j <= nr_blocks; ++j) {
		if ((ret = read_inode_blocks(inode, j, buffer, blksize)) < 0)
			kerror(-ret);
		if (!ret) continue;
		dir = (struct ext2_dir_t *)buffer;
		if (is_root) goto found;
		for (; dir->rec_length;
				dir = (struct ext2_dir_t *)((char *)dir + dir->rec_length)) {
			if (strlen(name) == dir->name_length) {
				if (!strncmp(name, dir->filename, dir->name_length)) {
found:
					if (is_root)
						assert(!strcmp(name, "/"));
					retdir = kmalloc(sizeof(*retdir));
					if (IS_ERR(retdir)) {
						kfree(buffer);
						return retdir;
					}
					memcpy(retdir, dir, sizeof(*retdir));
					kfree(buffer);
					return retdir;
				}
			}
		}
	}
	kfree(buffer);
	return ERR_PTR(-ENOENT);
}

static int
filealloc(ino_t inode_nr)
{
	struct ext2_inode_t *inode;
	struct file_t *f;
	int i;

	inode = geti(inode_nr);
	acquire(&flist_lock);
	for (i = 3; i < NR_MAX_OPEN_FILES; ++i) {
		if (curr_proc->fdtable[i]
				&& curr_proc->fdtable[i]->f_state != FILE_NONE)
			continue;
		if (!curr_proc->fdtable[i]) {
			f = kmalloc(sizeof(struct file_t));
			if (IS_ERR(f)) {
				release(&flist_lock);
				return PTR_ERR(f);
			}
			f->f_state = FILE_NONE;
			list_add_tail(&f->f_listopen, &flist);
			curr_proc->fdtable[i] = f;
		}
		f = curr_proc->fdtable[i];
		assert(f->f_state == FILE_NONE);
		f->f_refcount = 1;
		f->f_off = 0;
		f->f_state = FILE_ALLOC;
		f->f_type = FILE_REG;
		f->f_inode = inode;
		f->f_inode_nr = inode_nr;
		release(&flist_lock);
		return i;
	}
	release(&flist_lock);
	return -EMFILE;
}

int
fileclose(int fd)
{
	struct file_t *f;
	int j;

	if (fd < 0 || fd >= NR_MAX_OPEN_FILES)
		return -EINVAL;

	acquire(&flist_lock);
	f = curr_proc->fdtable[fd];
	if (!f || f->f_state != FILE_ALLOC) {
		release(&flist_lock);
		return -EBADF;
	}
	if (!--f->f_refcount) {
		curr_proc->fdtable[fd]->f_state = FILE_NONE;
		if (f->f_type == FILE_PIPE) {
			for (j = 3; j < NR_MAX_OPEN_FILES; ++j) {
				if (fd == j || curr_proc->pipebufs[j] != curr_proc->pipebufs[fd])
					continue;
				if (curr_proc->fdtable[j]->f_state == FILE_ALLOC)
					break;
				/* no one is referencing this buffer */
				kfree(curr_proc->pipebufs[fd]);
				curr_proc->pipebufs[fd] = curr_proc->pipebufs[j] = NULL;
				break;
			}
		}
	}
	release(&flist_lock);
	return 0;
}

/* TODO: free pipebufs */
void
closefds(void)
{
	struct file_t *f;
	int i;

	acquire(&flist_lock);
	for (i = 3; i < NR_MAX_OPEN_FILES; ++i) {
		if (!curr_proc->fdtable[i])
			continue;
		f = curr_proc->fdtable[i];
		if (f->f_state == FILE_NONE || f->f_state == FILE_ALLOC) {
			if (f->f_state == FILE_NONE) {
				assert(!f->f_refcount);
				list_del(&f->f_listopen);
				/* invalidate dup(2)ed close fds */
				bzero(f, sizeof(*f));
				kfree(f);
			} else {
				if (!--f->f_refcount) {
					list_del(&f->f_listopen);
					bzero(f, sizeof(*f));
					kfree(f);
				}
			}
		}
	}
	release(&flist_lock);
}

static struct ext2_dir_t *
resolve_path_internal(struct ext2_inode_t *inode, const char *path) {
	struct ext2_dir_t *dirent;
	char *buf, *bufptr;
	const char *ptr;
	uint32_t ino;
	uint32_t is_root_flag = 1;
	int ret;

	buf = kmalloc(EXT2_NAME_LEN + 1);
	if (IS_ERR(buf)) {
		ret = PTR_ERR(buf);
		return ERR_PTR(ret);
	}
	memset(buf, 0, EXT2_NAME_LEN + 1);
	ptr = path;
	*buf = *ptr;
	if (*ptr == '/') ++ptr;
	if (*ptr) {
		bufptr = buf;
		while (*ptr && *ptr != '/') {
			if (bufptr - buf >= EXT2_NAME_LEN) {
				kfree(buf);
				return ERR_PTR(-ENAMETOOLONG);
			}
			*bufptr++ = *ptr++;
		}
		if (*ptr == '/') ++ptr;
		is_root_flag = 0;
	}
	if (IS_ERR(dirent = find_dentry(inode, buf, is_root_flag))) {
		ret = PTR_ERR(dirent);
		goto err;
	}
	kfree(buf);
	if (!*ptr)
		return dirent;
	ino = dirent->inode;
	kfree(dirent);
	return resolve_path_internal(geti(ino), ptr);
err:
	kfree(buf);
	return ERR_PTR(ret);
}

static struct ext2_dir_t *
resolve_path(const char *path) {
	if (!path)
		return NULL;
	if (*path == '/')
		return resolve_path_internal(root_inode, path);
	return resolve_path_internal(curr_proc->cdir, path);
}

static int
check_inode_status(ino_t inode)
{
	struct ext2_gdt_t *gdt;
	char *inode_bitmap;
	blksize_t i;

	gdt = readgdt((inode - 1) / sblock->nr_inodes_per_group);
	inode_bitmap = readblock(gdt->addr_block_bitmap);
	for (i = 0; i < get_block_size(); ++i)
		if (inode_bitmap[i / 8] & (1 << (1 % 8)))
			return 0;
	return -ENOENT;
}

int
dup(int oldfd)
{
	int i;
	struct file_t *f, *tmp;

	if (oldfd < 0 || oldfd >= NR_MAX_OPEN_FILES)
		return -EINVAL;
	acquire(&flist_lock);
	f = curr_proc->fdtable[oldfd];
	if (!f || f->f_state != FILE_ALLOC) {
		release(&flist_lock);
		return -EBADF;
	}
	for (i = 3; i < NR_MAX_OPEN_FILES; ++i) {
		tmp = curr_proc->fdtable[i];
		if (!tmp || tmp->f_state == FILE_NONE) {
			if (tmp->f_state == FILE_NONE) {
				assert(!tmp->f_refcount);
				list_del(&tmp->f_listopen);
				kfree(tmp);
				tmp = NULL;
			}
			curr_proc->fdtable[i] = f;
			++f->f_refcount;
			release(&flist_lock);
			return i;
		}
	}
	release(&flist_lock);
	return -EMFILE;
}

int
dup2(int oldfd, int newfd)
{
	struct file_t *f, *tmp;

	if (oldfd < 0 || oldfd >= NR_MAX_OPEN_FILES)
		return -EINVAL;
	acquire(&flist_lock);
	f = curr_proc->fdtable[oldfd];
	if (!f || f->f_state != FILE_ALLOC
			|| newfd < 0 || newfd >= NR_MAX_OPEN_FILES) {
		release(&flist_lock);
		return -EBADF;
	}
	if (oldfd == newfd) {
		release(&flist_lock);
		return newfd;
	}
	tmp = curr_proc->fdtable[newfd];
	if (tmp && tmp->f_state == FILE_ALLOC)
		if (!--tmp->f_refcount)
			tmp->f_state = FILE_NONE;
	if (tmp)
		assert(!tmp->f_refcount);
	if (!tmp || !tmp->f_refcount) {
		curr_proc->fdtable[newfd] = f;
		++f->f_refcount;
		release(&flist_lock);
		return newfd;
	}
	release(&flist_lock);
	return -EMFILE;
}

int
open(const char *pathname,
     __attribute__ ((unused)) int flags,
     __attribute__ ((unused)) mode_t mode)
{
	struct ext2_dir_t *dirent;
	int fd;

	if (!pathname)
		return -EINVAL;
	dirent = resolve_path(pathname);
	if (IS_ERR(dirent))
		return PTR_ERR(dirent);
	fd = filealloc(dirent->inode);
	return fd;
}

int
creat(__attribute__ ((unused)) const char *pathname,
      __attribute__ ((unused)) mode_t mode)
{
	return -ENOSYS;
}

int
close(int fd)
{
	return fileclose(fd);
}

DIR *
opendir(const char *name)
{
	int fd;
	DIR *newdir = NULL;

	fd = sys_open(name, 0, 0);
	if (fd < 0)
		EXT2_SETERRNO_AND_RET(-fd, NULL);
	if (!(curr_proc->fdtable[fd]->f_inode->file_mode & DIRECTORY_T)) {
		errno = ENOTDIR;
		goto err;
	}
	newdir = kmalloc(sizeof(*newdir));
	if (IS_ERR(newdir)) {
		errno = -PTR_ERR(newdir);
		goto err;
	}
	newdir->off = 0;
	newdir->fd = fd;
	return newdir;
err:
	sys_close(fd);
	EXT2_SETERRNO_AND_RET(errno, NULL);
}

struct dirent_t *
readdir(DIR *dirp) {
	struct file_t *file;
	static struct dirent_t dirent;
	struct ext2_dir_t *dir;
	struct ext2_inode_t *inode;
	char *buffer;
	blksize_t blksize;
	int ret;

	if (!dirp)
		return ERR_PTR(-EINVAL);
	file = curr_proc->fdtable[dirp->fd];
	if (!file || !(file->f_inode->file_mode & DIRECTORY_T))
		return ERR_PTR(-EINVAL);
	inode = geti(file->f_inode_nr);
	blksize = get_block_size();
	buffer = kmalloc(blksize);
	if (IS_ERR(buffer))
		return (void *)buffer;
	/* TODO: make it possible to have directory entries spanning many blocks */
	ret = read_inode_blocks(inode, 0, buffer, blksize);
	if (ret <= 0) {
		kfree(buffer);
		return ERR_PTR(ret);
	}
	dir = (struct ext2_dir_t *)(buffer + dirp->off);
	if (dirp->off >= blksize) {
		kfree(buffer);
		return NULL;
	}
	dirp->off += dir->rec_length;
	dirent.d_inode = dir->inode;
	dirent.d_off = (uint32_t)((char *)dir - buffer);
	dirent.d_namelen = dir->name_length;
	memset(dirent.d_name, 0, EXT2_NAME_LEN + 1);
	memcpy(dirent.d_name, dir->filename, dir->name_length);
	kfree(buffer);
	return &dirent;
}

int
closedir(DIR *dirp)
{
	int ret;

	if (!dirp)
		return -EINVAL;
	if ((ret = sys_close(dirp->fd)) < 0)
		return ret;
	kfree(dirp);
	return 0;
}

ssize_t
read(int fd, void *buf, size_t count)
{
	struct file_t *f;
	int ret;
	char *newbuf;
	size_t newcnt;
	blksize_t blksize;
	off_t soff;
	ssize_t r;


	if (fd < 0 || fd >= NR_MAX_OPEN_FILES || !buf)
		return -EINVAL;

	if (fd == STDIN_FILENO)
		return tty_read(buf, count);

	acquire(&flist_lock);
	f = curr_proc->fdtable[fd];
	if (!f || f->f_state != FILE_ALLOC) {
		release(&flist_lock);
		return -EBADF;
	}
	if (f->f_type == FILE_PIPE) {
		r = piperead(fd, buf, count);
		release(&flist_lock);
		return r;
	}
	assert(f->f_inode);
	if (f->f_inode->file_mode & DIRECTORY_T) {
		release(&flist_lock);
		return -EISDIR;
	}
	blksize = get_block_size();
	if (f->f_inode->size_low32 - f->f_off < count)
		count = f->f_inode->size_low32 - f->f_off;
	if (!count) goto out;
	newcnt = count;
	if (newcnt % blksize) newcnt = (newcnt + blksize) & ~(blksize - 1);
	newbuf = kmalloc(newcnt);
	if (IS_ERR(newbuf)) {
		release(&flist_lock);
		return PTR_ERR(newbuf);
	}
	soff = (f->f_off % blksize) ? (f->f_off & ~((off_t)blksize - 1)) : (off_t)f->f_off;
	soff /= blksize;
	if ((ret = read_inode_blocks(f->f_inode, soff, newbuf, newcnt)) <= 0) {
		kfree(newbuf);
		release(&flist_lock);
		return ret;
	}
	soff = f->f_off % newcnt;
	if (newcnt - soff < count) count = newcnt - soff;
	memcpy(buf, newbuf + soff, count);
	f->f_off += count;
	kfree(newbuf);
out:
	release(&flist_lock);
	return count;
}

ssize_t
write(int fd, const void *buf, size_t count)
{
	const char *ptr;
	size_t len = 0;
	struct file_t *f;
	ssize_t r;

	if (fd < 0 || fd >= NR_MAX_OPEN_FILES || !buf)
		return -EINVAL;

	/* ignore STDIN, STDOUT and STDERR for now */
	if (fd > 2) {
		acquire(&flist_lock);
		f = curr_proc->fdtable[fd];
		if (!f || f->f_state != FILE_ALLOC) {
			release(&flist_lock);
			return -EBADF;
		}
		if (f->f_type == FILE_PIPE) {
			r = pipewrite(fd, buf, count);
			release(&flist_lock);
			return r;
		}
		release(&flist_lock);
	}
	ptr = buf;
	while (count--) {
		if (putchar(*ptr++) < 0)
			break;
		++len;
	}
	return len;
}

int
stat(const char *path, struct stat *buf)
{
	struct ext2_dir_t *dir;
	struct ext2_inode_t *inode;

	if (!path || !buf)
		return -EINVAL;

	dir = resolve_path(path);
	if (IS_ERR(dir))
		return PTR_ERR(dir);
	inode = geti(dir->inode);
	buf->st_dev = 0;
	buf->st_ino = dir->inode;
	buf->st_mode = inode->file_mode;
	buf->st_nlink = inode->link_count;
	buf->st_uid = inode->uid_low16 | (inode->uid_upper16 << 16);
	buf->st_gid = inode->gid_low16 | (inode->gid_upper16 << 16);
	buf->st_rdev = 0;
	buf->st_size = inode->size_low32;
	buf->st_blksize = get_block_size();
	buf->st_blocks = (inode->size_low32 / buf->st_blksize) * (buf->st_blksize / 512);
	buf->st_atime = inode->atime;
	buf->st_mtime = inode->mtime;
	buf->st_ctime = inode->ctime;
	return 0;
}

int
fstat(int fd, struct stat *buf)
{
	struct file_t *f;
	struct ext2_inode_t *inode;

	if (fd < 0 || fd >= NR_MAX_OPEN_FILES)
		return -EBADF;
	if (!buf)
		return -EINVAL;
	acquire(&flist_lock);
	f = curr_proc->fdtable[fd];
	if (!f || f->f_state != FILE_ALLOC) {
		release(&flist_lock);
		return -EBADF;
	}
	inode = geti(f->f_inode_nr);
	buf->st_dev = 0;
	buf->st_ino = f->f_inode_nr;
	buf->st_mode = inode->file_mode;
	buf->st_nlink = inode->link_count;
	buf->st_uid = inode->uid_low16 | (inode->uid_upper16 << 16);
	buf->st_gid = inode->gid_low16 | (inode->gid_upper16 << 16);
	buf->st_rdev = 0;
	buf->st_size = inode->size_low32;
	buf->st_blksize = get_block_size();
	buf->st_blocks = (inode->size_low32 / buf->st_blksize) * (buf->st_blksize / 512);
	buf->st_atime = inode->atime;
	buf->st_mtime = inode->mtime;
	buf->st_ctime = inode->ctime;
	release(&flist_lock);
	return 0;
}

int
execve(const char *filename,
       __attribute__ ((unused)) char *const argv[],
       __attribute__ ((unused)) char *const envp[])
{
	struct stat buf;
	int fd, ret = 0, i;
	char *elfimage;
	ssize_t bytesleft;
	ssize_t bytesread = 0;
	Elf32_Ehdr *elfheader;
	Elf32_Phdr *elfprog;
	uint32_t sz;
	struct mmap_region *reg;
	struct list_head *iter, *tmpreg;
	size_t len;
	blksize_t blksize;

	if ((fd = open(filename, O_RDONLY, 0)) < 0)
		return fd;
	if ((ret = fstat(fd, &buf)) < 0)
		goto err;
	if (IS_ERR(elfimage = kmalloc(buf.st_size))) {
		ret = PTR_ERR(elfimage);
		goto err;
	}

	bytesleft = buf.st_size;
	blksize = get_block_size();
	do {
		ret = read(fd, elfimage + bytesread, bytesleft);
		if (!ret) break;
		if (ret < 0) goto err1;
		bytesread += ret;
		bytesleft -= ret;
	} while (bytesleft > 0);
	assert(!bytesleft && bytesread == buf.st_size);
	elfheader = (Elf32_Ehdr *)elfimage;

	if (elfheader->e_ident[EI_MAG0] != 0x7f
			|| elfheader->e_ident[EI_MAG1] != 'E'
			|| elfheader->e_ident[EI_MAG2] != 'L'
			|| elfheader->e_ident[EI_MAG3] != 'F'
			|| elfheader->e_type != ET_EXEC
			|| elfheader->e_machine != EM_386
			|| !elfheader->e_entry
			|| elfheader->e_version != EV_CURRENT
			|| elfheader->e_ident[EI_VERSION] != EV_CURRENT
			|| !elfheader->e_phnum
			|| elfheader->e_ident[EI_CLASS] != ELFCLASS32
			|| elfheader->e_ident[EI_DATA] != ELFDATA2LSB) {
		ret = -ENOEXEC;
		goto err1;
	}

	elfprog = (Elf32_Phdr *)(elfimage + elfheader->e_phoff);
	for (i = 0; i < elfheader->e_phnum; ++i, ++elfprog) {
		if (elfprog->p_type != 0x1) continue;
		if (elfprog->p_vaddr + elfprog->p_memsz < elfprog->p_vaddr)
			goto err1;
		sz = elfprog->p_filesz;
		reg = kmalloc(sizeof(*reg));
		if (IS_ERR(reg)) {
			ret = PTR_ERR(reg);
			goto err1;
		}
		if ((ret = mmap_range(elfprog->p_vaddr, elfprog->p_vaddr + sz)) < 0) {
			for (--elfprog; i > 0; --i) {
				ret = unmap_range(elfprog->p_vaddr, elfprog->p_vaddr + sz);
				if (ret < 0)
					panic("can't unmap mapped pages!");
			}
			kfree(reg);
			goto err2;
		}
		bzero((void *)elfprog->p_vaddr, 0);
		memcpy((void *)elfprog->p_vaddr,
		       (const void *)(elfimage + elfprog->p_offset), sz);
		reg->base_addr = elfprog->p_vaddr; reg->size = sz;
		list_add_tail(&reg->l_region, &curr_proc->l_regions);
	}

	kfree(curr_proc->name);
	len = strlen(filename) + 1;
	assert(len);
	curr_proc->name = kmalloc(len);
	if (IS_ERR(curr_proc->name)) {
		ret = PTR_ERR(curr_proc->name);
		goto err2;
	}
	strncpy(curr_proc->name, filename, len);
	curr_proc->name[len - 1] = '\0';
	curr_proc->cf->eip = elfheader->e_entry;
	goto err1;
err2:
	list_for_each_safe(iter, tmpreg, &curr_proc->l_regions) {
		reg = list_entry(iter, struct mmap_region, l_region);
		if (unmap_range(reg->base_addr, reg->base_addr + reg->size) < 0)
			panic("failed to unmap pages!");
		list_del(iter);
		kfree(reg);
	}
err1:
	kfree(elfimage);
err:
	close(fd);
	return ret;
}

