/*
 *  core/tty.c
 *
 *  Copyright (C) 2009 stateless
 */

#include <tty.h>
#include <x86.h>
#include <keyboard.h>
#include <ctype.h>
#include <tss.h>
#include <string.h>
#include <syscall.h>

unsigned short int cursor_x = 0;
unsigned short int cursor_y = 0;
volatile unsigned char *framebuffer = (unsigned char *)FRAMEBUFFER_ADDR;
enum vga_colours curr_col = 0;

static void *tty_sleep_chan = (void *) & tty_sleep_chan;
static void *tty_suspend_chan = (void *) & tty_suspend_chan;
static struct task_t *ctrl_tty_task = NULL;

void
ccurr_col(enum vga_colours bg, enum vga_colours fg)
{
	uint32_t state;

	save_flags(&state);
	cli();
	curr_col = 0;
	curr_col |= fg;
	curr_col = curr_col | (bg << 4);
	load_flags(state);
}

void
clear_scr(void)
{
	uint32_t state;
	int y, x;

	save_flags(&state);
	cli();
	for (y = 0; y < BUF_HEIGHT; ++y) {
		for (x = 0; x < BUF_WIDTH; ++x) {
			framebuffer[((y * BUF_WIDTH + x) << 1)] = 0;
			framebuffer[((y * BUF_WIDTH + x) << 1) + 1] = curr_col;
		}
	}
	gotoxy(0, 0);
	load_flags(state);
}

void
gotoxy(unsigned short newy, unsigned short newx)
{
	uint32_t state;

	save_flags(&state);
	cli();
	cursor_x = newx;
	cursor_y = newy;
	load_flags(state);
}

void
erase_line(int y)
{
	int x;
	uint32_t state;

	save_flags(&state);
	cli();
	for (x = 0; x < BUF_WIDTH; ++x) {
		framebuffer[((y * BUF_WIDTH + x) << 1)] = 0;
		framebuffer[((y * BUF_WIDTH + x) << 1) + 1] = curr_col;
	}
	load_flags(state);
}

void
scroll(void)
{
	int y, x;
	volatile unsigned char *dst, *src;
	uint32_t state;

	save_flags(&state);
	cli();
	if (cursor_y >= BUF_HEIGHT) {
		for (y = 0; y < BUF_HEIGHT - 1; ++y) {
			erase_line(y);
			dst = framebuffer + ((y * BUF_WIDTH) << 1);
			src = framebuffer + (((y + 1) * BUF_WIDTH) << 1);
			for (x = 0; x < BUF_WIDTH * 2; ++x)
				*(dst + x) = *(src + x);
		}
		erase_line(BUF_HEIGHT - 1);
		gotoxy(BUF_HEIGHT - 1, 0);
	}
	load_flags(state);
}

void
blink(void)
{
	uint32_t state;
	uint8_t temp;

	save_flags(&state);
	cli();
	outb(0x3d4, 0xa);
	temp = inb(0x3d5);
	temp ^= 0x20;
	outb(0x3d5, temp);
	load_flags(state);
}

int
tty_putchar(int c)
{
	uint32_t state;
	volatile unsigned char *ptr;

	save_flags(&state);
	cli();
	if (c == '\b' && cursor_x) {
		gotoxy(cursor_y, cursor_x - 1);
	} else if (c == '\n') {
		gotoxy(cursor_y + 1, 0);
	} else if (c == '\r') {
		gotoxy(cursor_y, 0);
	} else if (c == '\t') {
		gotoxy(cursor_y, cursor_x + 4);
	} else {
		ptr = framebuffer + ((cursor_y * BUF_WIDTH + cursor_x) << 1);
		*ptr = c & 0xff;
		*(ptr + 1) = curr_col & 0xff;
		gotoxy(cursor_y, cursor_x + 1);
	}
	if (cursor_x >= BUF_WIDTH)
		gotoxy(cursor_y + 1, 0);
	scroll();
	load_flags(state);
	return c;
}

void
update_cursor(uint16_t row, uint16_t col)
{
	uint32_t state;
	uint16_t position = (row * 80) + col;

	save_flags(&state);
	cli();
	outb(0x3d4, 0xf);
	outb(0x3d5, (position & 0xff));
	outb(0x3d4, 0xe);
	outb(0x3d5, ((position >> 8) & 0xff));
	load_flags(state);
}

void *
tty_get_sleep_chan(void)
{
	return tty_sleep_chan;
}

void
attach_tty(struct task_t *ctask)
{
	if (strcmp(ctask->name, "idle"))
		ctrl_tty_task = ctask;
}

int
is_tty_attached(void)
{
	return !ctrl_tty_task ? 0 : 1;
}

int
is_ctrl_task(struct task_t *ctask)
{
	return ctrl_tty_task == ctask;
}

void
tty_suspend(void)
{
	sys_suspend_task(tty_suspend_chan);
}

void
tty_resume(void)
{
	sys_resume_task(tty_suspend_chan);
}

struct task_t *
tty_get_ctrl_task(void) {
	return ctrl_tty_task;
}

ssize_t
tty_read(void *buf, size_t count)
{
	int c;
	size_t i = 0;
	char *p;
	volatile unsigned char *ptr;

	p = buf;
	while (i < count) {
		do {
			if (!is_ctrl_task(curr_proc))
				tty_suspend();
again:
			sys_suspend_task(tty_sleep_chan);
			c = kbd_getchar();
			if (c != -1 && (isprint(c) || c == 127)) {
				if (c == 127) {
					if (p == buf)
						goto again;
					gotoxy(cursor_y, cursor_x - 1);
					update_cursor(cursor_y, cursor_x);
					*--p = '\0';
					c = ' ';
					ptr = framebuffer +
					      ((cursor_y * BUF_WIDTH + cursor_x) << 1);
					*ptr = c & 0xff;
					*(ptr + 1) = curr_col & 0xff;
					goto again;
				}
				if (*p != '\n')
					update_cursor(cursor_y, cursor_x + 1);
				tty_putchar(c);
				*p = c;
				if (*p == '\n') {
					/* HACK */
					curr_proc->esp = curr_proc->old_esp;
					return i + 1;
				}
				++p;
				break;
			}
		} while (1);
		++i;
	}
	/* HACK */
	curr_proc->esp = curr_proc->old_esp;
	return count;
}

int
raw_tty_getchar(void)
{
	int c;

	do {
		c = kbd_getchar();
		if (c != -1)
			return c;
	} while (1);
}

