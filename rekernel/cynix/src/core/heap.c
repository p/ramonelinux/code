/*
 *  core/heap.c
 *
 *  Copyright (C) 2009 stateless
 */

#include <heap.h>
#include <mm.h>
#include <common.h>
#include <x86.h>
#include <errno.h>
#include <list.h>
#include <tty.h>

/* for debugging purposes */
#define DUMP_CHUNK(chunk) \
	({ \
		printf("\tbase_addr = 0x%lx, end_addr = 0x%lx, size = 0x%x\n", \
		       chunk->base_addr, \
		       chunk->end_addr, chunk->size); \
		serial_dump("\tbase_addr = 0x%lx, end_addr = 0x%lx, size = 0x%x\n", \
			    chunk->base_addr, \
			    chunk->end_addr, chunk->size); \
	})

struct chunk_t {
	uint32_t base_addr;
	uint32_t end_addr;
	size_t size;
	struct list_head list;
};

static int is_init = 0;
static struct list_head freelist;
static struct list_head allolist;

void
walk_heap_lists(void)
{
	uint32_t state;
	struct list_head *iter;
	struct chunk_t *tmp;

	save_flags(&state);
	cli();
	printf("Summary of free chunks\n");
	serial_dump("Summary of free chunks\n");
	list_for_each(iter, &freelist) {
		tmp = list_entry(iter, struct chunk_t, list);
		DUMP_CHUNK(tmp);
	}
	printf("Summary of allocated chunks\n");
	serial_dump("Summary of allocated chunks\n");
	list_for_each(iter, &allolist) {
		tmp = list_entry(iter, struct chunk_t, list);
		DUMP_CHUNK(tmp);
	}
	load_flags(state);
}

void
kfree(void *ptr)
{
	struct chunk_t *tmp;
	struct list_head *iter, *q;
	uint32_t state;

	if (!ptr) return; /* free-ing NULL pointers is good */
	if (!is_init)
		panic("allocator is not initialized!");
	save_flags(&state);
	cli();
	list_for_each_safe(iter, q, &allolist) {
		tmp = list_entry(iter, struct chunk_t, list);
		if (tmp->base_addr == (uint32_t)ptr) {
			list_del(&tmp->list);
			list_add_tail(&tmp->list, &freelist);
			goto out;
		}
	}
	panic("trying to free non-existent chunk at 0x%08lx!", ptr);
out:
	load_flags(state);
}

void *
kmalloc_ext(size_t size, enum heap_cntl flag)
{
	struct list_head *iter, *q;
	struct chunk_t *tmp, *new;
	uint32_t addr, state;
	int ret;

	if (!is_init)
		panic("allocator is not initialized!");

	if (flag != DEFAULT)
		return ERR_PTR(-ENOTSUP);

	if (!size)
		return ERR_PTR(-EINVAL);
	save_flags(&state);
	cli();
	size += sizeof(struct chunk_t);
	do {
		list_for_each_safe(iter, q, &freelist) {
			tmp = list_entry(iter, struct chunk_t, list);
			if (size > tmp->size)
				continue;
			new = (struct chunk_t *)(tmp->end_addr - size);
			new->base_addr = tmp->end_addr - size + sizeof(*tmp);
			new->size = size - sizeof(*tmp);
			new->end_addr = tmp->end_addr;
			tmp->size -= size;
			tmp->end_addr -= size;
			list_add_tail(&new->list, &allolist);
			load_flags(state);
			return (void *)new->base_addr;
		}
		addr = (uint32_t)sbrk(size);
		if (IS_ERR((void *)addr)) {
			ret = PTR_ERR((void *)addr);
			break;
		}
		tmp = (struct chunk_t *)addr;
		tmp->base_addr = addr + sizeof(*tmp);
		tmp->size = size;
		tmp->size = roundup_pagesize(tmp->size);
		tmp->size -= sizeof(*tmp);
		tmp->end_addr = tmp->base_addr + tmp->size;
		list_add_tail(&tmp->list, &freelist);
	} while (1);
	load_flags(state);
	return ERR_PTR(ret);
}

void *
kmalloc(size_t size)
{
	return kmalloc_ext(size, DEFAULT);
}

void
init_heap(void)
{
	uint32_t state;

	save_flags(&state);
	cli();
	INIT_LIST_HEAD(&freelist);
	INIT_LIST_HEAD(&allolist);
	is_init = 1;
	load_flags(state);
}

