/*
 *  core/elf.c
 *
 *  Copyright (C) 2010 stateless
 */

#include <elf.h>
#include <multiboot.h>
#include <common.h>
#include <string.h>
#include <serial.h>

extern multiboot_info_t *info_boot;

Elf32_Shdr *symtab = NULL, *strtab = NULL;

int
prepare_elf(void)
{
	elf_section_header_table_t *header;
	Elf32_Shdr *sh, *tmp;
	unsigned long i;

	header = &info_boot->u.elf_sec;
	sh = (Elf32_Shdr *)(header->addr + header->shndx * header->size);
	for (i = 0; i < header->num; ++i) {
		tmp = (Elf32_Shdr *)(header->addr + i * header->size);
		if (tmp->sh_type == SHT_SYMTAB)
			symtab = tmp;
		else if (tmp->sh_type == SHT_STRTAB)
			if (!strcmp((const char *)sh->sh_addr + tmp->sh_name, ".strtab"))
				strtab = tmp;
	}
	return (!symtab || !strtab) ? -1 : 0;
}

const char *
find_symbol(uint32_t addr)
{
	Elf32_Sym *sym;
	size_t i;

	if (!symtab || !strtab)
		return NULL;

	sym = (Elf32_Sym *)symtab->sh_addr;
	for (i = 0; i < symtab->sh_size / sizeof(*sym); ++i) {
		if (addr >= sym->st_value && addr < sym->st_value + sym->st_size)
			return (const char *)(strtab->sh_addr + sym->st_name);
		++sym;
	}

	return NULL;
}

