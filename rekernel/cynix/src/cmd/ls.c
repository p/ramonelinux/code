#include <dirent.h>
#include <sys/stat.h>
#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int
main(void)
{
	char buf[256];
	DIR *dirp;
	struct dirent *dirent;
	struct stat stbuf;
	int ret;

	dirp = opendir("/");
	if (!dirp)
		return -errno;
	dirent = readdir(dirp);
	printf("%-12s%-12s%-12s%-12s\n", "FILENAME", "TYPE", "UID", "GID");
	while (dirent) {
		if (!strcmp(dirent->d_name, ".")
				|| !strcmp(dirent->d_name, ".."))
			goto out;
		bzero(buf, 256);
		buf[0] = '/';
		strncpy(buf + 1, dirent->d_name, 255);
		buf[255] = '\0';
		ret = stat(buf, &stbuf);
		if (ret < 0) {
			fprintf(stderr, "%s\n", strerror(errno));
			break;
		}
		printf("%-12s%-12s%-12d%-12d\n",
		       dirent->d_name,
		       S_ISREG(stbuf.st_mode) ? "FILE" : "DIR",
		       stbuf.st_uid,
		       stbuf.st_gid);
out:
		dirent = readdir(dirp);
	}
	ret = closedir(dirp);
	if (ret < 0)
		return -errno;
	return 0;
}

