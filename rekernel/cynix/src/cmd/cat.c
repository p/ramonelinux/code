#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

int main(int argc, char *argv[])
{
	int fd;
	int n;
	char buf[BUFSIZ];

	fd = open(argv[1], O_RDONLY);

	while ((n = read(fd, buf, BUFSIZ)) > 0) {
		printf("%s\n", buf);
	}
}
