/* This file is released under the GPL v2 public license */

#include <string.h>

void *
memcpy(void *dest, const void *src, size_t n)
{
	char *tmp = dest;
	const char *s = src;

	while (n--)
		*tmp++ = *s++;
	return dest;
}

void *
memmove(void *dest, const void *src, size_t count)
{
	char *tmp;
	const char *s;

	if (dest <= src) {
		tmp = dest;
		s = src;
		while (count--)
			*tmp++ = *s++;
	} else {
		tmp = dest;
		tmp += count;
		s = src;
		s += count;
		while (count--)
			*--tmp = *--s;
	}
	return dest;
}

void *
memset(void *s, int c, size_t n)
{
	char *ptr = s;
	while (n--)
		*ptr++ = c;
	return s;
}

int
memcmp(const void *cs, const void *ct, size_t count)
{
	const unsigned char *su1, *su2;
	int res = 0;

	for (su1 = cs, su2 = ct; 0 < count; ++su1, ++su2, count--)
		if ((res = *su1 - *su2) != 0)
			break;
	return res;
}

size_t
strlen(const char *s)
{
	size_t len = 0;

	while (*s++) ++len;
	return len;
}

size_t
strnlen(const char *s, size_t count)
{
	const char *sc;

	for (sc = s; count-- && *sc != '\0'; ++sc)
		;
	return sc - s;
}

int
strcmp(const char *cs, const char *ct)
{
	signed char res;

	while (1) {
		if ((res = *cs - *ct++) != 0 || !*cs++)
			break;
	}
	return res;
}

char *
strcpy(char *dest, const char *src)
{
	char *tmp = dest;

	while ((*dest++ = *src++) != '\0')
		/* nothing */;
	return tmp;
}

char *
strncpy(char *dest, const char *src, size_t count)
{
	char *tmp = dest;

	while (count) {
		if ((*tmp = *src) != 0)
			src++;
		tmp++;
		count--;
	}
	return dest;
}

int
strncmp(const char *cs, const char *ct, size_t count)
{
	signed char res = 0;

	while (count) {
		if ((res = *cs - *ct++) != 0 || !*cs++)
			break;
		count--;
	}
	return res;
}

void
bzero(void *s, size_t n)
{
	unsigned char *ptr = s;

	while (n--)
		*ptr++ = 0;
}

