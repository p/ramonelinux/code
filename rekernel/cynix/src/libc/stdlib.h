#ifndef __STDLIB_H__
#define __STDLIB_H__

extern void itoa(long int num, char *buffer, int base /* should be 'd', 'u' or 'x' */);
extern unsigned long strtoul(const char *nptr, char **endptr, int base);

#endif

