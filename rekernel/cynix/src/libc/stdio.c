/*
 *  libc/stdio.c
 *
 *  Copyright (C) 2009 stateless
 */

#include <tty.h>
#include <stdio.h>
#include <string.h>
#include <x86.h>

int
putchar(int c)
{
	uint32_t state;

	save_flags(&state);
	cli();
	c = tty_putchar(c);
	load_flags(state);
	return c;
}

int
printf(const char *fmt, ...)
{
	char buffer[512], *ptr;
	va_list ap;
	int c;
	uint32_t state;

	save_flags(&state);
	cli();
	va_start(ap, fmt);
	vsprintf(buffer, fmt, ap);
	va_end(ap);
	ptr = buffer;
	while (*ptr) {
		putchar(*ptr++);
		++c;
	}
	load_flags(state);
	return c;
}

int
puts(const char *s)
{
	uint32_t state;

	save_flags(&state);
	cli();
	const char *ptr = s;
	while (*ptr)
		putchar(*ptr++);
	putchar('\n');
	load_flags(state);
	return (int)(ptr - s);
}

