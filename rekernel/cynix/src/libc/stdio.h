#ifndef __STDIO_H__
#define __STDIO_H__

#include <stdarg.h>

#define NULL ((void *)0)
#define BUFSIZ 8192

extern int putchar(int c);
extern int printf(const char *fmt, ...);
extern int puts(const char *s);
extern int vsprintf(char *buf, const char *fmt, va_list args);

#endif

