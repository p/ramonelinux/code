#ifndef __STRING_H__
#define __STRING_H__

#include <sys/types.h>

extern void *memcpy(void *dest, const void *src, size_t n);
extern void *memmove(void *dest, const void *src, size_t count);
extern void *memset(void *s, int c, size_t n);
extern int memcmp(const void *cs, const void *ct, size_t count);
extern size_t strlen(const char *s);
extern size_t strnlen(const char *s, size_t count);
extern int strcmp(const char *cs, const char *ct);
extern char *strcpy(char *dest, const char *src);
extern char *strncpy(char *dest, const char *src, size_t count);
extern int strncmp(const char *cs, const char *ct, size_t count);
extern void bzero(void *s, size_t n);

#endif

