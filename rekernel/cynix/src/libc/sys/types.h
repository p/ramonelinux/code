#ifndef __TYPES_H__
#define __TYPES_H__

typedef int ssize_t;
typedef unsigned int size_t;
typedef int pid_t;
typedef long int time_t;
typedef unsigned short uid_t;
typedef unsigned short gid_t;
typedef unsigned short dev_t;
typedef unsigned short ino_t;
typedef unsigned long int mode_t;
typedef long int off_t;
typedef short int nlink_t;
typedef unsigned long int blksize_t;
typedef unsigned long int blkcnt_t;

#endif

