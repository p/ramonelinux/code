#ifndef __IDT_H__
#define __IDT_H__

#include <stdint.h>

#define DUMP_STACK_REGS(regs) \
	({ \
		DEBUG_CODE(printf("unroll_esp = 0x%lx\n", regs.unroll_esp);) \
		DEBUG_CODE(printf("ds = 0x%lx\n", regs.ds);) \
		DEBUG_CODE(printf("edi = 0x%lx, esi = 0x%lx, ebp = 0x%lx, esp = 0x%lx, ebx = 0x%lx, edx = 0x%lx, ecx = 0x%lx, eax = 0x%lx\n", \
				  regs.edi, regs.esi, regs.ebp, regs.esp, regs.ebx, regs.edx, regs.ecx, regs.eax);) \
		DEBUG_CODE(printf("int_no = 0x%lx, err_code = 0x%lx\n", regs.int_no, regs.err_code);) \
		DEBUG_CODE(printf("eip = 0x%lx, cs = 0x%lx, eflags = 0x%lx\n", \
				  regs.eip, regs.cs, regs.eflags);) \
	})

struct trapframe_t {
	uint32_t unroll_esp;
	uint32_t ds;                                      /* Data segment selector */
	uint32_t edi, esi, ebp, esp, ebx, edx, ecx, eax;  /* Pushed by pusha. */
	uint32_t int_no, err_code;                        /* Interrupt number and error code (if applicable) */
	uint32_t eip, cs, eflags;                         /* Pushed by the processor automatically. I might need to add useresp and ss.*/
} __attribute__ ((packed));

struct idt_intr_gate {
	uint16_t offset_lo        : 16;
	uint16_t selector         : 16;
	uint8_t  all0             : 8;
	uint8_t  gate_size        : 5; /* 0b0D110 */
	uint8_t  privilege_level  : 2;
	uint8_t  segment_present  : 1;
	uint16_t offset_hi        : 16;
} __attribute__((packed));

struct idt_selector {
	uint16_t limit;
	uint32_t base;
} __attribute__ ((packed));

typedef void (*isr_handler)(struct trapframe_t *regs);

extern void isr0(void);
extern void isr1(void);
extern void isr2(void);
extern void isr3(void);
extern void isr4(void);
extern void isr5(void);
extern void isr6(void);
extern void isr7(void);
extern void isr8(void);
extern void isr9(void);
extern void isr10(void);
extern void isr11(void);
extern void isr12(void);
extern void isr13(void);
extern void isr14(void);
extern void isr15(void);
extern void isr16(void);
extern void isr17(void);
extern void isr18(void);
extern void isr19(void);
extern void isr20(void);
extern void isr21(void);
extern void isr22(void);
extern void isr23(void);
extern void isr24(void);
extern void isr25(void);
extern void isr26(void);
extern void isr27(void);
extern void isr28(void);
extern void isr29(void);
extern void isr30(void);
extern void isr31(void);
extern void isr32(void);
extern void isr33(void);
extern void isr34(void);
extern void isr35(void);
extern void isr36(void);
extern void isr37(void);
extern void isr38(void);
extern void isr39(void);
extern void isr40(void);
extern void isr41(void);
extern void isr42(void);
extern void isr43(void);
extern void isr44(void);
extern void isr45(void);
extern void isr46(void);
extern void isr47(void);
extern void isr128(void);
extern void initialize_idt(void);
extern uint32_t handle_interrupt(struct trapframe_t regs);
extern int register_isr_handler(uint8_t index, isr_handler handler);

#endif

