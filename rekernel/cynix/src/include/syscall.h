#ifndef __SYSCALL_H__
#define __SYSCALL_H__

#include <idt.h>
#include <common.h>

#define sys_exit(status) \
	({ \
		int __num = __NR_exit, err_code = status; \
		asm volatile ("int $0x80" :: "a"(__num), "b"(err_code)); \
	})

#define sys_fork() \
	({ \
		int __num = __NR_fork; \
		pid_t __ret; \
		asm volatile ("int $0x80" : "=a"(__ret) : "0"(__num)); \
		__ret; \
	})

#define sys_waitpid(pid, status, options) \
	({ \
		int __num = __NR_waitpid, *__status = status, __options = options; \
		pid_t __pid = pid, __ret; \
		asm volatile ("int $0x80" : "=a"(__ret) : "0"(__num), "b"(__pid), "c"(__status), "d"(__options)); \
		__ret; \
	})

#define sys_read(fd, buf, count) \
	({ \
		int __num = __NR_read, __fd = fd; \
		ssize_t __ret; \
		void *__buf = buf; \
		size_t __count = count; \
		asm volatile ("int $0x80" : "=a"(__ret) : "0"(__num), "b"(__fd), "c"(__buf), "d"(__count)); \
		__ret; \
	})

#define sys_write(fd, buf, count) \
	({ \
		int __num = __NR_write, __fd = fd; \
		ssize_t __ret; \
		void *__buf = buf; \
		size_t __count = count; \
		asm volatile ("int $0x80" : "=a"(__ret) : "0"(__num), "b"(__fd), "c"(__buf), "d"(__count)); \
		__ret; \
	})

#define sys_reschedule() \
	({ \
		int __num = __NR_reschedule; \
		asm volatile ("int $0x80" :: "a"(__num)); \
	})

#define sys_open(pathname, flags, mode) \
	({ \
		int __num = __NR_open, __flags = flags, __ret; \
		const char *__pathname = pathname; \
		mode_t __mode = mode; \
		asm volatile ("int $0x80" : "=a"(__ret) : "0"(__num), "b"(__pathname), "c"(__flags), "d"(__mode)); \
		__ret; \
	})

#define sys_creat(pathname, mode) \
	({ \
		int __num = __NR_creat, __ret; \
		mode_t __mode = mode; \
		const char *__pathname = pathname; \
		asm volatile ("int $0x80" : "=a"(__ret) : "0"(__num), "b"(__pathname), "c"(__mode)); \
		__ret; \
	})

#define sys_close(fd) \
	({ \
		int __num = __NR_close, __ret; \
		int __fd = fd; \
		asm volatile ("int $0x80" : "=a"(__ret) : "0"(__num), "b"(__fd)); \
		__ret; \
	})

#define sys_isatty(fd) \
	({ \
		int __num = __NR_isatty, __ret, __fd = fd; \
		asm volatile ("int $0x80" : "=a"(__ret) : "0"(__num), "b"(__fd)); \
		__ret; \
	})

#define sys_getpid() \
	({ \
		int __num = __NR_getpid; \
		pid_t __ret; \
		asm volatile ("int $0x80" : "=a"(__ret) : "0"(__num)); \
		__ret; \
	})

#define sys_getppid() \
	({ \
		int __num = __NR_getppid; \
		pid_t __ret; \
		asm volatile ("int $0x80" : "=a"(__ret) : "0"(__num)); \
		__ret; \
	})

#define sys_readdir(dir) \
	({ \
		int __num = __NR_readdir; \
		DIR *__dir = dir; \
		struct dirent_t *__ret; \
		asm volatile ("int $0x80" : "=a"(__ret) : "0"(__num), "b"(__dir)); \
		__ret; \
	})

#define sys_dup(oldfd) \
	({ \
		int __num = __NR_dup, __oldfd = oldfd, __ret; \
		asm volatile ("int $0x80" : "=a"(__ret) : "0"(__num), "b"(__oldfd)); \
		__ret; \
	})

#define sys_dup2(oldfd, newfd) \
	({ \
		int __num = __NR_dup, __oldfd = oldfd, __newfd = newfd, __ret; \
		asm volatile ("int $0x80" : "=a"(__ret) : "0"(__num), "b"(__oldfd), "c"(__newfd)); \
		__ret; \
	})

#define sys_stat(path, stbuf) \
	({ \
		int __num = __NR_stat, __ret; \
		const char *__path = path; \
		struct stat *__buf = stbuf; \
		asm volatile ("int $0x80" : "=a"(__ret) : "0"(__num), "b"(__path), "c"(__buf)); \
		__ret; \
	})

#define sys_fstat(fd, stbuf) \
	({ \
		int __num = __NR_fstat, __ret, __fd = fd; \
		struct stat *__buf = stbuf; \
		asm volatile ("int $0x80" : "=a"(__ret) : "0"(__num), "b"(__fd), "c"(__buf)); \
		__ret; \
	})

#define sys_execve(filename, argv, envp) \
	({ \
		int __num = __NR_execve, __ret; \
		const char *__filename = filename; \
		char **const __argv = argv; \
		char **const __envp = envp; \
		asm volatile ("int $0x80" : "=a"(__ret) : "0"(__num), "b"(__filename), "c"(__argv), "d"(__envp)); \
		__ret; \
	})

#define sys_seeder() \
	({ \
		int __num = __NR_seeder, __ret; \
		asm volatile ("int $0x80" : "=a"(__ret) : "0"(__num)); \
		__ret; \
	})

#define sys_sbrk(incr) \
	({ \
		int __num = __NR_sbrk, __ret; \
		intptr_t __incr = incr; \
		asm volatile ("int $0x80" : "=a"(__ret) : "0"(__num), "b"(__incr)); \
		__ret; \
	})

#define sys_getuid() \
	({ \
		int __num = __NR_getuid; \
		uid_t __ret; \
		asm volatile ("int $0x80" : "=a"(__ret) : "0"(__num)); \
		__ret; \
	})

#define sys_setuid(uid) \
	({ \
		int __num = __NR_setuid, __ret; \
		uid_t __uid = uid; \
		asm volatile ("int $0x80" : "=a"(__ret) : "0"(__num), "b"(__uid)); \
		__ret; \
	})

#define sys_getgid() \
	({ \
		int __num = __NR_getgid; \
		uid_t __ret; \
		asm volatile ("int $0x80" : "=a"(__ret) : "0"(__num)); \
		__ret; \
	})

#define sys_setgid(gid) \
	({ \
		int __num = __NR_setgid, __ret; \
		uid_t __gid = gid; \
		asm volatile ("int $0x80" : "=a"(__ret) : "0"(__num), "b"(__gid)); \
		__ret; \
	})

#define sys_suspend_task(channel) \
	({ \
		int __num = __NR_suspend_task, __ret; \
		void * __channel = channel; \
		asm volatile ("int $0x80" : "=a"(__ret) : "0"(__num), "b"(__channel)); \
		__ret; \
	})

#define sys_resume_task(channel) \
	({ \
		int __num = __NR_resume_task; \
		void * __channel = channel; \
		asm volatile ("int $0x80" :: "a"(__num), "b"(__channel)); \
	})

#define sys_pipe(pipefds) \
	({ \
		int __num = __NR_pipe, __ret; \
		int *__pipefds = pipefds; \
		asm volatile ("int $0x80" : "=a"(__ret) : "0"(__num), "b"(__pipefds)); \
		__ret; \
	})

#define SYSCALL_NR 30
#define __NR_exit 1
#define __NR_fork 2
#define __NR_read 3
#define __NR_write 4
#define __NR_open 5
#define __NR_close 6
#define __NR_waitpid 7
#define __NR_creat 8
#define __NR_link 9
#define __NR_unlink 10
#define __NR_execve 11
#define __NR_reschedule 12
#define __NR_isatty 13
#define __NR_getpid 14
#define __NR_getppid 15
#define __NR_readdir 16
#define __NR_dup 17
#define __NR_dup2 18
#define __NR_stat 19
#define __NR_fstat 20
#define __NR_seeder 21
#define __NR_sbrk 22
#define __NR_getuid 23
#define __NR_setuid 24
#define __NR_getgid 25
#define __NR_setgid 26
#define __NR_suspend_task 27
#define __NR_resume_task 28
#define __NR_pipe 29

extern void syscall_dispatcher(struct trapframe_t *regs);

#endif

