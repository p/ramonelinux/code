#ifndef __PIPE_H__
#define __PIPE_H__

#include <sys/types.h>

enum {
	PIPE_SIZ = 8192
};

extern int pipe(int pipefd[2]);
extern ssize_t piperead(int fd, void *buf, size_t count);
extern ssize_t pipewrite(int fd, const void *buf, size_t count);

#endif

