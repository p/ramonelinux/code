#ifndef __X86_H__
#define __X86_H__

#include <stdint.h>
#include <common.h>

/* useful macros */
#define GET_ESP(esp) asm volatile ("movl %%esp, %0" : "=r"(esp));
#define PUT_ESP(esp) asm volatile ("movl %0, %%esp" :: "r"(esp));
#define GET_EBP(ebp) asm volatile ("movl %%ebp, %0" : "=r"(ebp));
#define PUT_EBP(ebp) asm volatile ("movl %0, %%ebp" :: "r"(ebp));

extern void save_flags(uint32_t *state);
extern void load_flags(uint32_t state);
extern uint32_t fetch_eip(void);
extern void do_cpuid(void);

typedef int spinlock_t;

static inline void
cli(void)
{
	asm volatile ("cli");
}

static inline void
sti(void)
{
	asm volatile ("sti");
}

static inline bool
ints_on(void)
{
	uint32_t state;

	save_flags(&state);
	return state & (1 << 9);
}

static inline void
hlt(void)
{
	asm volatile ("hlt");
}

static inline void
jmp(uint32_t addr)
{
	asm volatile ("call *%0" :: "a"(addr));
}

static inline void
outb(uint16_t port, uint8_t val)
{
	asm volatile ("outb %0, %1" :: "a"(val), "Nd"(port));
}

static inline void
outw(uint16_t port, uint16_t val)
{
	asm volatile ("outw %0, %1" :: "a"(val), "Nd"(port));
}

static inline void
outl(uint16_t port, uint32_t val)
{
	asm volatile ("outl %0, %1" :: "a"(val), "Nd"(port));
}

static inline uint8_t
inb(uint16_t port)
{
	uint8_t ret;

	asm volatile ("inb %1, %0" : "=a"(ret) : "Nd"(port));
	return ret;
}

static inline uint16_t
inw(uint16_t port)
{
	uint16_t ret;

	asm volatile ("inw %1, %0" : "=a"(ret) : "Nd"(port));
	return ret;
}

static inline uint32_t
inl(uint16_t port)
{
	uint32_t ret;

	asm volatile ("inl %1, %0" : "=a"(ret) : "Nd"(port));
	return ret;
}

static inline uint32_t
read_cr3(void)
{
	uint32_t cr3;

	asm volatile ("movl %%cr3, %0" : "=r"(cr3));
	return cr3;
}

static inline void
initspinlock(spinlock_t *spinlock)
{
	assert(spinlock != NULL);
	__sync_lock_release(spinlock);
}

static inline void
acquire(spinlock_t *spinlock)
{
	assert(spinlock != NULL);
	while (__sync_lock_test_and_set(spinlock, 1) == 1)
		;
}

static inline int
try_acquire(spinlock_t *spinlock)
{
	assert(spinlock != NULL);
	return __sync_lock_test_and_set(spinlock, 1);
}

static inline void
release(spinlock_t *spinlock)
{
	assert(spinlock != NULL);
	__sync_lock_release(spinlock);
}

#endif

