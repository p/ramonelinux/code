#ifndef __MM_H__
#define __MM_H__

#include <stdint.h>
#include <stdlib.h>
#include <sys/types.h>
#include <common.h>

enum { PAGE_SIZE = 0x1000, PAGE_SHIFT = 12, PAGE_HEAP = 0xf0000000, PAGE_HEAP_SIZE = 0x4000000 };
enum page_pa_fl { DONT_ALLOC_PHYS, ALLOC_PHYS, DONT_FREE_PHYS, FREE_PHYS };

struct pte_t {
	uint8_t present      : 1;
	uint8_t rw           : 1;
	uint8_t privilege    : 1; /* user/supervisor */
	uint8_t pwt          : 1; /* if the bit is set write-through is enabled, otherwise write-back */
	uint8_t pcd          : 1; /* if the bit is set the page will not be cached */
	uint8_t accessed     : 1;
	uint8_t dirty        : 1;
	uint8_t pat          : 1; /* must be zero if pat is not supported */
	uint8_t global       : 1;
	uint8_t ignore       : 3;
	uint32_t frame       : 20;
} __attribute__ ((packed));

struct page_table_t {
	struct pte_t pages[1024];
} __attribute__ ((packed));

struct pde_t {
	uint8_t present      : 1;
	uint8_t rw           : 1;
	uint8_t privilege    : 1; /* user/supervisor */
	uint8_t pwt          : 1; /* if the bit is set write-through is enabled, otherwise write-back */
	uint8_t pcd          : 1; /* if the bit is set the page will not be cached */
	uint8_t accessed     : 1;
	uint8_t dirty        : 1; /* ignored */
	uint8_t page_size    : 1; /* if the bit is set, the pages are 4mb in size (pse must be enabled), otherwise 4kb */
	uint8_t ignore       : 4;
	uint32_t pt_addr     : 20;
} __attribute__ ((packed));

struct page_directory_t {
	struct pde_t ptables[1024];
} __attribute__ ((packed));

extern void switch_page_dir(struct page_directory_t *pagedir);
extern int init_mm(void);
extern int init_vm(void);
extern void *palloc(size_t size);
extern void pfree(void *addr, size_t size);
extern uint32_t virt_to_phys(uint32_t virt_addr, bool *val);
extern int mmap(uint32_t virt_addr, uint32_t phys_addr);
extern int mmap_range(uint32_t virt_addr_start, uint32_t virt_addr_end);
extern int unmap_range(uint32_t virt_addr_start, uint32_t virt_addr_end);
extern int unmap(uint32_t virt_addr);
extern void *sbrk(intptr_t incr);
extern void flush_tlb(void);
extern struct page_directory_t *clone_page_dir(void);
extern void set_curr_page_dir(struct page_directory_t *page_dir);
extern int init_page_pa_allocator(void);
extern void *alloc_page_pa(uint32_t *frame, enum page_pa_fl flags);
extern int free_page_pa(void *page, uint32_t *frame, enum page_pa_fl flags);
extern void remove_proc_mappings(void);
extern uint32_t get_num_free_frames(void);
extern void dump_mappings(void);

static inline uint32_t
roundup_pagesize(uint32_t n)
{
	if (n % PAGE_SIZE)
		n = (n + PAGE_SIZE) & ~(PAGE_SIZE - 1);
	return n;
}

#endif

