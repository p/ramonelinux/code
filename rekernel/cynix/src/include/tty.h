#ifndef __TTY_H__
#define __TTY_H__

#include <stdint.h>
#include <sys/types.h>
#include <tss.h>

enum vga_defs {
	FRAMEBUFFER_ADDR = 0xb8000,
	BUF_HEIGHT = 25,
	BUF_WIDTH = 80
};

enum vga_colours {
	BLACK,
	BLUE,
	GREEN,
	CYAN,
	RED,
	MAGENTA,
	BROWN,
	LIGHT_GREY,
	DARK_GREY,
	LIGHT_BLUE,
	LIGHT_GREEN,
	LIGHT_CYAN,
	LIGHT_RED,
	LIGHT_MAGENTA,
	LIGHT_BROWN,
	WHITE
};

extern unsigned short cursor_x;
extern unsigned short cursor_y;
extern volatile unsigned char *framebuffer;
extern enum vga_colours curr_col;

extern void ccurr_col(enum vga_colours bg, enum vga_colours fg);
extern void clear_scr(void);
extern void gotoxy(unsigned short newy, unsigned short newx);
extern void scroll(void);
extern void erase_line(int y);
extern void blink(void);
extern void update_cursor(uint16_t row, uint16_t col);
extern int tty_putchar(int c);
extern ssize_t tty_read(void *buf, size_t count);
extern int raw_tty_read(void);
extern void *tty_get_sleep_chan(void);
extern void attach_tty(struct task_t *ctask);
extern void tty_suspend(void);
extern void tty_resume(void);
extern int is_ctrl_task(struct task_t *ctask);
extern struct task_t *tty_get_ctrl_task(void);
extern int is_tty_attached(void);

#endif

