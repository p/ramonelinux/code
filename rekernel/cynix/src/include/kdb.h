#ifndef __KDB_H__
#define __KDB_H__

#include <idt.h>

extern void set_trapframe(struct trapframe_t *tframe);
extern void kdb_enter(void);

#endif

