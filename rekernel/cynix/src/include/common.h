#ifndef __COMMON_H__
#define __COMMON_H__

#include <stdio.h>
#include <stdint.h>
#include <sys/types.h>

#if __STDC_VERSION__ < 199901L
#if __GNUC__ >= 2
#define __func__ __FUNCTION__
#else
#define __func__ "<unknown>"
#endif
#endif

#define BINCLUDE_DEBUG_CODE
#if defined(BINCLUDE_DEBUG_CODE)
#define DEBUG_CODE(statement) statement
#else
#define DEBUG_CODE(statement)
#endif

#define BSYSCALL_TRACE
#if 1
#undef BSYSCALL_TRACE
#endif
#if defined(BSYSCALL_TRACE)
#define TRACE_SYSCALL(statement) statement
#else
#define TRACE_SYSCALL(statement)
#endif

#define MIN_ERRNO 1
#define MAX_ERRNO 44
#define SETERRNO_AND_RET(err, ret) ({ errno = (err); return (ret); })
#define stringify(s) #s
#define info(format, ...) printf(format, ##__VA_ARGS__)
#define likely(x) __builtin_expect(!!(x), 1)
#define unlikely(x) __builtin_expect(!!(x), 0)
#define assert(cond) ({ if (unlikely(!(cond))) panic("Assertion '%s' failed!", #cond); })
#define len(array) (sizeof(array) / sizeof(array[0]))
#define offsetof(TYPE, MEMBER) ((size_t)__builtin_offsetof(TYPE, MEMBER))
#define container_of(ptr, type, member) ({ const typeof( ((type *)0)->member ) *__mptr = (ptr); (type *)( (char *)__mptr - offsetof(type,member) ); })
#define panic(format, ...) panic_internal(__FILE__, __func__, __LINE__, format, ##__VA_ARGS__)

typedef enum { false, true } bool;
typedef uint32_t addr_t;

extern void panic_internal(const char *file, const char *fn, uint32_t line, const char *fmt, ...);
extern void hexdump(const void *ptr, size_t len);
extern void stacktrace(int depth);

static inline int
IS_ERR(const void *ptr)
{
	return (int)ptr >= -MAX_ERRNO && (int)ptr <= -MIN_ERRNO;
}

static inline void *
ERR_PTR(int error)
{
	return (void *)error;
}

static inline int
PTR_ERR(const void *ptr)
{
	return (int)ptr;
}

#endif

