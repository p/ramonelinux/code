#ifndef __HEAP_H__
#define __HEAP_H__

#include <sys/types.h>

enum { KERNEL_HEAP = 0xd0000000, KERNEL_HEAP_END = 0xd8000000 };
enum heap_cntl { DEFAULT };

extern void *kmalloc(size_t size);
extern void *kmalloc_ext(size_t size, enum heap_cntl flag);
extern void kfree(void *ptr);
extern void walk_heap_lists(void);
extern void init_heap(void);

#endif

