#ifndef __SERIAL_H__
#define __SERIAL_H__

#define COM_PORT 0x3f8

extern void serial_init(void);
extern int serial_dump(const char *s, ...);
extern int serial_avail(void);
extern int serial_putchar(int c);

#endif

