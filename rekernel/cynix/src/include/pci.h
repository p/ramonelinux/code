#ifndef __PCI_H__
#define __PCI_H__

#include <stdint.h>
#include <list.h>

enum {
	CONFIG_ADDRESS = 0xcf8,
	CONFIG_DATA = 0xcfc,
	PCI_NO_DEV = 0xffffffff
};

struct pci_config_addr_t {
	uint8_t reg_num : 8;
	uint8_t func_num : 3;
	uint8_t dev_num : 5;
	uint8_t bus_num : 8;
	uint8_t reserved : 7;
	uint8_t enable_bit : 1;
} __attribute__ ((packed));

union pci_config_space_t {
	struct {
		uint16_t vendor_id;
		uint16_t device_id;
		uint16_t command;
		uint16_t status;
		uint8_t rev_id;
		uint8_t prog_if;
		uint8_t subclass;
		uint8_t class;
		uint8_t cachelsz;
		uint8_t latency_timer;
		uint8_t header_type;
		uint8_t bist;
		uint32_t bar0, bar1, bar2, bar3, bar4, bar5;
		uint32_t cardbus_cis_reg;
		uint16_t sub_vendor_id;
		uint16_t sub_id;
		uint32_t exp_rom_addr;
		uint16_t caps;
		uint16_t reserved0;
		uint32_t reserved1;
		uint8_t irq;
		uint8_t int_pin;
		uint8_t min_grant;
		uint8_t max_latency;
	} __attribute__((packed)) type0;
};

struct pci_dev_t;

struct pci_bus_t {
	struct list_head pci_dev_list;
	struct list_head pci_bus_list;
};

struct pci_dev_t {
	union pci_config_space_t *cspace;
	struct pci_bus_t *bus;
	struct list_head list;
	int busn;
	int devn;
};

extern uint32_t pci_read32(struct pci_config_addr_t *addr);
extern uint32_t pci_init(void);
extern int pci_enumerate(void);
extern void lspci(void);
extern struct pci_dev_t * get_pci_dev(uint16_t vendor_id, uint16_t device_id);

#endif

