#ifndef __EXT2_H__
#define __EXT2_H__

#include <stdint.h>
#include <sys/types.h>
#include <list.h>

#define O_RDONLY 0
#define NR_MAX_OPEN_FILES 32
#define EXT2_NAME_LEN 255

#define S_IFMT 0170000 /* type of file */
#define S_IFREG 0100000 /* regular */
#define S_IFDIR 0040000 /* directory */
#define S_IFBLK 0060000 /* block special */
#define S_IFCHR 0020000 /* character special */
#define S_IFIFO 0010000 /* fifo */

#define S_ISUID 0004000 /* set user id on execution */
#define S_ISGID 0002000 /* set group id on execution */

#define S_IRWXU 0000700 /* read,write,execute perm: owner */
#define S_IRUSR 0000400 /* read permission: owner */
#define S_IWUSR 0000200 /* write permission: owner */
#define S_IXUSR 0000100 /* execute/search permission: owner */
#define S_IRWXG 0000070 /* read,write,execute perm: group */
#define S_IRGRP 0000040 /* read permission: group */
#define S_IWGRP 0000020 /* write permission: group */
#define S_IXGRP 0000010 /* execute/search permission: group */
#define S_IRWXO 0000007 /* read,write,execute perm: other */
#define S_IROTH 0000004 /* read permission: other */
#define S_IWOTH 0000002 /* write permission: other */
#define S_IXOTH 0000001 /* execute/search permission: other */

#define S_ISFIFO(m) (((m)&(S_IFMT)) == (S_IFIFO))
#define S_ISDIR(m) (((m)&(S_IFMT)) == (S_IFDIR))
#define S_ISCHR(m) (((m)&(S_IFMT)) == (S_IFCHR))
#define S_ISBLK(m) (((m)&(S_IFMT)) == (S_IFBLK))
#define S_ISREG(m) (((m)&(S_IFMT)) == (S_IFREG))

enum file_state {
	FILE_ALLOC,
	FILE_NONE
};

enum file_type {
	FILE_REG,
	FILE_PIPE
};

enum fs_state {
	EXT2_CLEAN = 1 << 0,
	EXT2_FAULTY = 1 << 1,
	EXT2_ORPHAN_REC = 1 << 2
};

enum fs_err_handling {
	EXT2_CONT,
	EXT2_REMNT_RO,
	EXT2_PANIC
};

enum fs_creator {
	EXT2_LINUX,
	EXT2_HURD,
	EXT2_MASIX,
	EXT2_FREEBSD,
	EXT2_LITES
};

enum fs_feat_compat {
	EXT2_PREALLOCATE_DIR_BLOCKS = 1 << 0,
	EXT2_AFS_EXIST = 1 << 1,
	EXT2_HAS_JOURNAL = 1 << 2,
	EXT2_INODES_EXTENDED = 1 << 3,
	EXT2_CAN_RESIZE = 1 << 4,
	EXT2_USE_HASH = 1 << 5
};

enum fs_feat_incompat {
	EXT2_COMPRESSION = 1 << 0,
	EXT2_DIR_HAS_TYPE = 1 << 1,
	EXT2_NEEDS_RECOVERY = 1 << 2,
	EXT2_USES_JOURNAL_DEVICE = 1 << 3
};

enum fs_feat_ro_compat {
	EXT2_SPARSE_SUPER_AND_GDT = 1 << 0,
	EXT2_LARGE_FILES = 1 << 1,
	EXT2_DIR_B_TREES = 1 << 2
};

enum fs_file_mode_flags {
	/* bits [0, 8] */
	OX = 01,
	OW = 02,
	OR = 03,
	GX = 010,
	GW = 020,
	GR = 040,
	UX = 0100,
	UW = 0200,
	UR = 0400,
	/* bits [9, 11] */
	STICKY = 0x200,
	SGID = 0x400,
	SUID = 0x800,
	/* bits [12, 15] */
	FIFO_T = 0x1000,
	CHDEV_T = 0x2000,
	DIRECTORY_T = 0x4000,
	BLOCK_T = 0x6000,
	REGULAR_T = 0x8000,
	SYM_T = 0xa000,
	UNIX_SOCKET_T = 0xc000
};

enum fs_inode_flags {
	EXT2_SEC_DEL = 1 << 0,
	EXT2_KEEP_COPY_ON_DEL = 1 << 1,
	EXT2_FILE_COMPRESSION = 1 << 2,
	EXT2_SYNC = 1 << 3,
	EXT2_IMMUTABLE = 1 << 4,
	EXT2_APPEND_ONLY = 1 << 5,
	EXT2_DUMP_EXCLUDE = 1 << 6,
	EXT2_IGNORE_ATIME = 1 << 7,
	EXT2_HASH_DIR = 1 << 8,
	EXT2_DATA_JOURNALED_EXT3 = 1 << 9
};

enum fs_file_t {
	EXT2_FT_UNKNOWN,
	EXT2_FT_REG_FILE,
	EXT2_FT_DIR,
	EXT2_FT_CHRDEV,
	EXT2_FT_BLKDEV,
	EXT2_FT_FIFO,
	EXT2_FT_SOCK,
	EXT2_FT_SYMLINK,
	EXT2_FT_MAX
};

struct ext2_superblock_t {
	uint32_t nr_inodes;
	uint32_t nr_blocks;
	uint32_t nr_reserved_inodes;
	uint32_t nr_free_blocks;
	uint32_t nr_free_inodes;
	uint32_t first_data_block;
	uint32_t block_size; /* saved as the number of places to shift 1024 to the left */
	uint32_t fragment_size; /* same as above */
	uint32_t nr_blocks_per_group;
	uint32_t nr_fragments_per_group;
	uint32_t nr_inodes_per_group;
	uint32_t last_mtime;
	uint32_t last_wtime;
	uint16_t mnt_count;
	uint16_t max_mnt_count;
	uint16_t magic;
	uint16_t state;
	uint16_t err_handling_method;
	uint16_t minor_version;
	uint32_t lastcheck;
	uint32_t check_interval;
	uint32_t creator_os;
	uint32_t major_version;
	uint16_t uid_for_reserved_blocks;
	uint16_t gid_for_reserved_blocks;
	uint32_t first_ino;
	uint16_t inode_size;
	uint16_t super_block_group;
	uint32_t feat_compat;
	uint32_t feat_incompat;
	uint32_t feat_ro_compat;
	char fs_id[16];
	char volume_name[16];
	char path_last_mount[64];
	uint32_t algo_usage_bitmap;
	uint8_t nr_blocks_preallocate_file;
	uint8_t nr_blocks_preallocate_dir;
	uint16_t unused1;
	char journal_id[16];
	uint32_t journal_inode;
	uint32_t journal_dev;
	uint32_t head_orphan_inode_list;
	uint32_t hash_seed[4];
	uint8_t default_hash_version;
	uint8_t unused2;
	uint16_t unused3;
	uint32_t mount_options;
	uint32_t first_metablock;
	uint32_t reserved[190];
} __attribute__ ((packed));

struct ext2_gdt_t {
	uint32_t addr_block_bitmap;
	uint32_t addr_inode_bitmap;
	uint32_t addr_inode_table;
	uint16_t nr_free_blocks;
	uint16_t nr_free_inodes;
	uint16_t nr_dir_entries;
	uint16_t unused;
	uint32_t reserved[3];
} __attribute__ ((packed));

struct ext2_inode_t {
	uint16_t file_mode;
	uint16_t uid_low16;
	uint32_t size_low32;
	uint32_t atime;
	uint32_t ctime;
	uint32_t mtime;
	uint32_t dtime;
	uint16_t gid_low16;
	uint16_t link_count;
	uint32_t sector_count;
	uint32_t flags;
	uint32_t unused1;
	char direct_blocks[48];
	uint32_t single_indir_block;
	uint32_t double_indir_block;
	uint32_t triple_indir_block;
	uint32_t gen_number;
	uint32_t ext_attribute_block;
	uint32_t size_upper32;
	uint32_t addr_fragment;
	uint8_t fragment_index;
	uint8_t fragment_size;
	uint16_t unused2;
	uint16_t uid_upper16;
	uint16_t gid_upper16;
	uint32_t reserved;
} __attribute__ ((packed));

struct ext2_dir_t {
	uint32_t inode;
	uint16_t rec_length;
	uint8_t name_length;
	uint8_t file_t;
	char filename[EXT2_NAME_LEN];
} __attribute__ ((packed));

struct file_t {
	struct ext2_inode_t *f_inode;
	ino_t f_inode_nr;
	off_t f_off;
	enum file_state f_state;
	enum file_type f_type;
	int f_refcount;
	struct list_head f_listopen;
};

typedef struct DIR {
	int fd;
	uint32_t off;
} DIR;

struct dirent_t {
	ino_t d_inode;
	off_t d_off;
	uint16_t d_namelen;
	char d_name[EXT2_NAME_LEN + 1];
};

struct stat {
	dev_t     st_dev; /* ID of device containing file */
	ino_t     st_ino; /* inode number */
	mode_t    st_mode; /* protection */
	nlink_t   st_nlink; /* number of hard links */
	uid_t     st_uid; /* user ID of owner */
	gid_t     st_gid; /* group ID of owner */
	dev_t     st_rdev; /* device ID (if special file) */
	off_t     st_size; /* total size, in bytes */
	blksize_t st_blksize; /* blocksize for file system I/O */
	blkcnt_t  st_blocks; /* number of 512B blocks allocated */
	time_t    st_atime; /* time of last access */
	time_t    st_mtime; /* time of last modification */
	time_t    st_ctime; /* time of last status change */
};

extern struct ext2_inode_t *get_root_inode(void);
extern int ext2_mount(char *addr);
extern void ext2_dump(void);
extern int open(const char *pathname, int flags, mode_t mode);
extern int creat(const char *pathname, mode_t mode);
extern int close(int fd);
extern ssize_t write(int fd, const void *buf, size_t count);
extern ssize_t read(int fd, void *buf, size_t count);
extern void closefds(void);
extern DIR *opendir(const char *name);
extern int closedir(DIR *dirp);
extern struct dirent_t *readdir(DIR *dirp);
extern int dup(int oldfd);
extern int dup2(int oldfd, int newfd);
extern int stat(const char *path, struct stat *buf);
extern int fstat(int fd, struct stat *buf);
extern int execve(const char *filename, char *const argv[], char *const envp[]);
extern int fileclose(int fd);

#endif

