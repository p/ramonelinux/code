#ifndef __GDT_H__
#define __GDT_H__

#include <stdint.h>

struct gdt_descriptor_t {
	uint16_t limit_lo         : 16;
	uint16_t base_lo          : 16;
	uint8_t  base_middle      : 8;
	uint8_t  type             : 4;
	uint8_t  s                : 1;
	uint8_t  privilege_level  : 2;
	uint8_t  segment_present  : 1;
	uint8_t  limit_hi         : 4;
	uint8_t  available        : 1;
	uint8_t  l                : 1;
	uint8_t  db               : 1;
	uint8_t  granularity      : 1;
	uint8_t  base_hi          : 8;
} __attribute__((packed));

struct gdt_selector_t {
	uint16_t limit;
	uint32_t base;
} __attribute__((packed));

extern void initialize_gdt(void);

#endif

