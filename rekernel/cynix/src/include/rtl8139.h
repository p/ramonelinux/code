#ifndef __RTL8139_H__
#define __RTL8139_H__

#include <stdint.h>

extern int rtl8139_init(void);
extern const uint8_t *get_mac_addr(void);

#endif

