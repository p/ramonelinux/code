#ifndef __PIC_H__
#define __PIC_H__

#include <stdint.h>

extern void remap_pic(void);
extern void enable_irq(uint32_t irq);
extern void disable_irq(uint32_t irq);
extern void pic_eoi(uint32_t int_no);

#endif

