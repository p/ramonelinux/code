#ifndef __KEYBOARD_H__
#define __KEYBOARD_H__

#include <stdint.h>

extern void initialize_keyboard(void);
extern int kbd_getchar(void);

#endif

