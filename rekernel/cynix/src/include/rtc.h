#ifndef __RTC_H__
#define __RTC_H__

#include <stdint.h>

extern volatile uint32_t systimer;

extern void init_rtc(uint32_t freq);
extern void sleep(uint32_t seconds);
extern void msleep(uint32_t msec);

#endif

