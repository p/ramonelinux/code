/* POSIX error codes for system calls
 *
 * E2BIG       : argument list too long
 * EACCES      : permission denied
 * EAGAIN      : resource temporarily unavailable
 * EBADF       : bad file descriptor
 * EBADMSG     : bad message
 * EBUSY       : device or resource busy
 * ECANCELED   : operation canceled
 * ECHILD      : no child processes
 * EDEADLK     : resource deadlock avoided
 * EDOM        : mathematics argument out of domain of function
 * EEXIST      : file exists
 * EFAULT      : bad address
 * EFBIG       : file too large
 * EILSEQ      : illegal byte sequence
 * EINPROGRESS : operation in progress
 * EINTR       : interrupted function call
 * EINVAL      : invalid argument
 * EIO         : input/output error
 * EISDIR      : is a directory
 * EMFILE      : too many open files
 * EMLINK      : too many links
 * EMSGSIZE    : message too long
 * ENAMETOOLONG: filename too long
 * ENFILE      : too many open files in system
 * ENODEV      : no such device
 * ENOENT      : no such file or directory
 * ENOEXEC     : exec format error
 * ENOLCK      : no locks available
 * ENOMEM      : not enough space
 * ENOSPC      : no space left on device
 * ENOSYS      : function not implemented
 * ENOTDIR     : not a directory
 * ENOTEMPTY   : directory not empty
 * ENOTSUP     : operation not supported
 * ENOTTY      : inappropriate I/O control
 * ENXIO       : no such device or address
 * EPERM       : operation not permitted
 * EPIPE       : broken pipe
 * ERANGE      : result too large
 * EROFS       : read-only file system
 * ESPIPE      : invalid seek
 * ESRCH       : no such process
 * ETIMEDOUT   : connection timed out
 * EXDEV       : improper link
 */

#ifndef __ERRNO_H__
#define __ERRNO_H__

#include <common.h>

#define E2BIG         1
#define EACCES        2
#define EAGAIN        3
#define EBADF         4
#define EBADMSG       5
#define EBUSY         6
#define ECANCELED     7
#define ECHILD        8
#define EDEADLK       9
#define EDOM         10
#define EEXIST       11
#define EFAULT       12
#define EFBIG        13
#define EILSEQ       14
#define EINPROGRESS  15
#define EINTR        16
#define EINVAL       17
#define EIO          18
#define EISDIR       19
#define EMFILE       20
#define EMLINK       21
#define EMSGSIZE     22
#define ENAMETOOLONG 23
#define ENFILE       24
#define ENODEV       25
#define ENOENT       26
#define ENOEXEC      27
#define ENOLCK       28
#define ENOMEM       29
#define ENOSPC       30
#define ENOSYS       31
#define ENOTDIR      32
#define ENOTEMPTY    33
#define ENOTSUP      34
#define ENOTTY       35
#define ENXIO        36
#define EPERM        37
#define EPIPE        38
#define ERANGE       39
#define EROFS        40
#define ESPIPE       41
#define ESRCH        42
#define ETIMEDOUT    43
#define EXDEV        44

extern int errno;
extern const char *sys_errlist [];

static inline
void kerror(int err)
{
	if (err < 1 || err > 44)
		panic("Out of range!");
	panic(sys_errlist[err - 1]);
}

#endif

