#ifndef __TSS_H__
#define __TSS_H__

#include <stdint.h>
#include <idt.h>
#include <mm.h>
#include <list.h>
#include <ext2.h>
#include <x86.h>
#include <sys/types.h>
#include <serial.h>

#define DUMP_CSWITCH_FRAME(frame) \
	({ \
		DEBUG_CODE(serial_dump("ds = 0x%lx\n", frame->ds);) \
		DEBUG_CODE(serial_dump("edi = 0x%lx, esi = 0x%lx, ebp = 0x%lx, esp = 0x%lx, ebx = 0x%lx, edx = 0x%lx, ecx = 0x%lx, eax = 0x%lx\n", \
				       frame->edi, frame->esi, frame->ebp, frame->esp, frame->ebx, frame->edx, frame->ecx, frame->eax);) \
		DEBUG_CODE(serial_dump("int_no = 0x%lx, err_code = 0x%lx\n", frame->int_no, frame->err_code);) \
		DEBUG_CODE(serial_dump("eip = 0x%lx, cs = 0x%lx, eflags = 0x%lx\n", \
				       frame->eip, frame->cs, frame->eflags);) \
	})

#define TASK_STACK_SIZE (2048 * sizeof(uint32_t))
#define WNOHANG 1

enum task_state_t {
	TASK_ZOMBIE,
	TASK_SLEEPING,
	TASK_RUNNABLE,
	TASK_RUNNING
};

enum task_fl {
	KERNEL_PROCESS = 1,
	USER_PROCESS
};

struct tss_t {
	uint32_t link;
	uint32_t esp0;
	uint32_t ss0;
	uint32_t esp1;
	uint32_t ss1;
	uint32_t esp2;
	uint32_t ss2;
	uint32_t cr3;
	uint32_t eip;
	uint32_t eflags;
	uint32_t eax;
	uint32_t ecx;
	uint32_t edx;
	uint32_t ebx;
	uint32_t esp;
	uint32_t ebp;
	uint32_t esi;
	uint32_t edi;
	uint32_t es;
	uint32_t cs;
	uint32_t ss;
	uint32_t ds;
	uint32_t fs;
	uint32_t gs;
	uint32_t ldt;
	uint16_t trace_bitmap;
	uint16_t iomap;
} __attribute__ ((packed));

struct cswitch_frame_t {
	uint32_t ds; /* Data segment selector */
	uint32_t edi, esi, ebp, esp, ebx, edx, ecx, eax; /* Pushed by pusha. */
	uint32_t int_no, err_code; /* Interrupt number and error code (if applicable) */
	uint32_t eip, cs, eflags; /* Pushed by the processor automatically. I might need to add useresp and ss.*/
} __attribute__ ((packed));

struct tss_descriptor_t {
	uint16_t limit_lo         : 16;
	uint16_t base_lo          : 16;
	uint8_t  base_middle      : 8;
	uint8_t  type             : 4; /* 1001b inactive, 1011b busy */
	uint8_t  s                : 1; /* zero */
	uint8_t  privilege_level  : 2;
	uint8_t  segment_present  : 1;
	uint8_t  limit_hi         : 4;
	uint8_t  available        : 1;
	uint8_t  l                : 1; /* zero */
	uint8_t  db               : 1; /* zero */
	uint8_t  granularity      : 1; /* must be zero */
	uint8_t  base_hi          : 8;
} __attribute__ ((packed));

struct mmap_region {
	uint32_t base_addr;
	uint32_t size;
	struct list_head l_region;
};

struct task_t {
	uint32_t esp, old_esp;
	enum task_state_t state, old_state;
	struct page_directory_t *page_dir;
	uint32_t *stack;
	char *name;
	enum task_fl flags;
	struct list_head q_task;
	pid_t pid;
	int status;
	struct task_t *parent;
	struct cswitch_frame_t *cf;
	struct ext2_inode_t *cdir;
	struct file_t *fdtable[NR_MAX_OPEN_FILES];
	char *pipebufs[NR_MAX_OPEN_FILES];
	uid_t uid;
	gid_t gid;
	uid_t fuid;
	gid_t fgid;
	struct list_head l_regions;
};

extern void ps(void);
extern void kick_tss(void);
extern void remove_task(struct task_t *task);
extern void add_task(struct task_t *task);
extern struct task_t *create_kthread(const char *name, void *routine);
extern struct task_t *current;
extern int init_tss(void);
extern void schedule(void);
extern int fork(void);
extern pid_t waitpid(pid_t pid, int *status, int options);
extern void suspend_task(void *channel);
extern int resume_task(void *channel);
extern void freeze_tasks(void);
extern void unfreeze_tasks(void);
extern uid_t getuid(void);
extern int setuid(uid_t uid);
extern gid_t getgid(void);
extern int setgid(gid_t gid);

extern struct task_t *curr_proc;

#endif

