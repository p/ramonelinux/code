/*
 *  core/init.c
 *
 *  Copyright (C) 2009 stateless
 */

#include <multiboot.h>
#include <common.h>

extern int main(void);
extern int puts(const char *s);

unsigned long int addr_info = 0, initial_esp = 0;

void
init(unsigned long int magic, unsigned long int addr, unsigned long int initial_esp_arg)
{
	if (magic != MULTIBOOT_BOOTLOADER_MAGIC)
		panic("0x%lx invalid magic number", magic);
	addr_info = addr;
	initial_esp = initial_esp_arg;
	main();
}

