#include <types.h>

#define PAGE_OFFSET 0xC0000000

#define PAGE_PRESENT    0x1
#define PAGE_WRITE      0x2
#define PAGE_USER       0x4

#define PAGE_SIZE       4096

#define PGD_INDEX(x) (((x) >> 22) & 0x3FF)
#define PTE_INDEX(x) (((x) >> 12) & 0x3FF)

typedef uint32_t pgd_t;
typedef uint32_t pte_t;

#define PGD_SIZE (PAGE_SIZE / sizeof(pte_t))
#define PTE_SIZE (PAGE_SIZE / sizeof(uint32_t))

#define PTE_COUNT 128

void vmm_init();
