#include <gdt.h>
#include <idt.h>
#include <vmm.h>

void start_kernel();

__attribute__((section(".init.data"))) 
pgd_t *pgd_tmp  = (pgd_t *)0x1000;
__attribute__((section(".init.data"))) 
pgd_t *pte_low  = (pgd_t *)0x2000;
__attribute__((section(".init.data"))) 
pgd_t *pte_high = (pgd_t *)0x3000;

__attribute__((section(".init.text"))) 
void pag_init()
{
    pgd_tmp[0] = (uint32_t)pte_low | PAGE_PRESENT | PAGE_WRITE;
    pgd_tmp[PGD_INDEX(PAGE_OFFSET)] = (uint32_t)pte_high | PAGE_PRESENT | PAGE_WRITE;

    int i;
    for (i = 0; i < 1024; i++)
        pte_low[i] = (i << 12) | PAGE_PRESENT | PAGE_WRITE;
    for (i = 0; i < 1024; i++)
        pte_high[i] = (i << 12) | PAGE_PRESENT | PAGE_WRITE;

    asm volatile("movl %0, %%cr3" : : "r" (pgd_tmp));

    uint32_t cr0;
    asm volatile("movl %%cr0, %0" : "=r" (cr0));
    cr0 |= 0x80000000;
    asm volatile("movl %0, %%cr0" : : "r" (cr0));
}

__attribute__((section(".init.text"))) 
void setup_kernel()
{
    pag_init();
    idt_init();
    gdt_init();
    start_kernel();
}
