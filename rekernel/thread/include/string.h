#include <types.h>

static inline void memset(void *dest, uint8_t val, uint32_t len)
{
	uint8_t *dst = (uint8_t *)dest;

	for ( ; len != 0; len--)
		*dst++ = val;
}

static inline void bzero(void *dest, uint32_t len)
{
	memset(dest, 0, len);
}
