#include <task.h>
#include <printk.h>
#include <string.h>
#include <pmm.h>
#include <kmalloc.h>
#include <sched.h>

pid_t now_pid = 0;

int32_t kernel_thread(int (*fn)(void *), void *arg)
{
    struct task_struct *new_task = (struct task_struct *)kmalloc(STACK_SIZE);
    if (new_task == NULL) {
        printk("kern_thread: kmalloc error\n");
        while (1);
    }

    bzero(new_task, sizeof(struct task_struct));

    new_task->state = TASK_RUNNABLE;
    new_task->stack = current;
    new_task->pid = now_pid++;
    new_task->mm = NULL;

    uint32_t *stack_top = (uint32_t *)((uint32_t)new_task + STACK_SIZE);

    *(--stack_top) = (uint32_t)arg;
    *(--stack_top) = (uint32_t)kthread_exit;
    *(--stack_top) = (uint32_t)fn;

    new_task->context.esp = (uint32_t)new_task + STACK_SIZE - sizeof(uint32_t) * 3;

    new_task->context.eflags = 0x200;
    new_task->next = running_proc_head;

    struct task_struct *tail = running_proc_head;
    if (tail == NULL) {
        printk("Must init sched!\n");
        while (1);
    }

    while (tail->next != running_proc_head)
        tail = tail->next;
    tail->next = new_task;

    return new_task->pid;
}

void kthread_exit()
{
    register uint32_t val asm("eax");
    printk("Thread exited with value %d\n", val);

    while (1);
}

static int flag = 0;

int thread(void *arg)
{
    while (1) {
        if (flag == 1) {
            printk("B");
            flag = 0;
        }
    }

    return 0;
}

void thread_test()
{
    kernel_thread(thread, NULL);
    enable_intr();

    while (1) {
        if (flag == 0) {
            printk("A");
            flag = 1;
        }
    }
}
