#include <idt.h>
#include <printk.h>
#include <string.h>
#include <port.h>
#include <traps.h>

idt_ptr_t idt_ptr;

interrupt_handler_t interrupt_handlers[IDT_LENGTH];

void pic_init()
{
    outb(0x11, 0x20);
    outb(0x11, 0xA0);

    outb(0x20, 0x21);

    outb(0x28, 0xA1);

    outb(0x04, 0x21);

    outb(0x02, 0xA1);

    outb(0x01, 0x21);
    outb(0x01, 0xA1);

    outb(0x0, 0x21);
    outb(0x0, 0xA1);
}

void idt_init()
{
    pic_init();

    bzero((uint8_t *)&interrupt_handlers, sizeof(interrupt_handler_t) * IDT_LENGTH);
    bzero((uint8_t *)&idt_entries, sizeof(idt_entry_t) * IDT_LENGTH);

    idt_ptr.limit = sizeof(idt_entry_t) * IDT_LENGTH - 1;
    idt_ptr.base = (uint32_t)&idt_entries;

    idt_flush((uint32_t)&idt_ptr);
}

void isr_handler(pt_regs_t *regs)
{
    if (interrupt_handlers[regs->int_no])
        interrupt_handlers[regs->int_no](regs);
    else
        printk("Unhandled interrupt: %d\n", regs->int_no);
}

void register_interrupt_handler(uint8_t num, interrupt_handler_t handler)
{
    interrupt_handlers[num] = handler;
}

void irq_handler(pt_regs_t *regs)
{
    if (regs->int_no >= 40)
        outb(0x20, 0xA0);
    outb(0x20, 0x20);

    if (interrupt_handlers[regs->int_no])
        interrupt_handlers[regs->int_no](regs);
}

inline void enable_intr()
{
    asm volatile("sti");
}

inline void disable_intr()
{
    asm volatile("cli" ::: "memory");
}
