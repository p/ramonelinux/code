#include <multiboot.h>
#include <printk.h>
#include <gdt.h>
#include <idt.h>
#include <timer.h>
#include <pmm.h>
#include <vmm.h>
#include <kmalloc.h>

char kern_stack[STACK_SIZE] __attribute__((aligned(16)));
uint32_t kern_stack_top;

__attribute__((section(".init.data"))) 
pgd_t *pgd_tmp  = (pgd_t *)0x1000;
__attribute__((section(".init.data"))) 
pgd_t *pte_low  = (pgd_t *)0x2000;
__attribute__((section(".init.data"))) 
pgd_t *pte_high = (pgd_t *)0x3000;

multiboot_info_t *glb_mboot_ptr;

static void kernel_init()
{
    gdt_init();
    idt_init();

    /* Clear the screen. */
    cls();
    printk("hello, kernel\n");

    timer_init(200);

    //asm volatile("int $0x0");
    //asm volatile("int $0x4");

    //multiboot_info();
    printk("kernel in memory start: 0x%x\n", kern_start);
    printk("kernel in memory end: 0x%x\n", kern_end);
    printk("kernel in memory used: %d KB\n", (kern_end - kern_start + 1023) / 1024);

    pmm_init();
    vmm_init();
    kmalloc_init();

    enable_intr();

    page_alloc_test();
    kmalloc_test();

    while(1) {
        asm volatile("hlt");
    }
}

__attribute__((section(".init.text"))) 
void start_kernel()
{
    pgd_tmp[0] = (uint32_t)pte_low | PAGE_PRESENT | PAGE_WRITE;
    pgd_tmp[PGD_INDEX(PAGE_OFFSET)] = (uint32_t)pte_high | PAGE_PRESENT | PAGE_WRITE;

    int i;
    for (i = 0; i < 1024; i++)
        pte_low[i] = (i << 12) | PAGE_PRESENT | PAGE_WRITE;
    for (i = 0; i < 1024; i++)
        pte_high[i] = (i << 12) | PAGE_PRESENT | PAGE_WRITE;

    asm volatile("movl %0, %%cr3" : : "r" (pgd_tmp));

    uint32_t cr0;
    asm volatile("movl %%cr0, %0" : "=r" (cr0));
    cr0 |= 0x80000000;
    asm volatile("movl %0, %%cr0" : : "r" (cr0));

    kern_stack_top = ((uint32_t)kern_stack + STACK_SIZE);
    asm volatile("movl %0, %%esp\n\t"
                  "xorl %%ebp, %%ebp" : : "r" (kern_stack_top));
    glb_mboot_ptr = mboot_ptr_tmp + PAGE_OFFSET;

    kernel_init();
}
