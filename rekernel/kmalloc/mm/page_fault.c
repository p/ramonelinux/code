#include <idt.h>
#include <printk.h>

void page_fault(pt_regs_t *regs)
{
    uint32_t cr2;
    asm volatile("movl %%cr2, %0" : "=r" (cr2));

    printk("Page fault at EIP: 0x%x, virtual faulting address 0x%x.\n", regs->eip, cr2);
    printk("Error code: %x\n", regs->err_code);

    if (!(regs->err_code & 0x1))
        printk("Because the page wasn't persent.\n");
    
    if (regs->err_code & 0x2)
        printk("Write error.\n");
    else
        printk("Read error.\n");

    if (regs->err_code & 0x4)
        printk("In user mode.\n");
    else
        printk("In kernel mode.\n");

    if (regs->err_code & 0x8)
        printk("Reserved bits being overwritten.\n");

    if (regs->err_code & 0x10)
        printk("The fault occurred during an instruction fetch.\n");

    while(1);
}
