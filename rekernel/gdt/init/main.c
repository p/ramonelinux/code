#include <multiboot.h>
#include <printk.h>
#include <gdt.h>

void start_kernel(unsigned long magic, unsigned long addr)
{
    gdt_init();

    /* Clear the screen. */
    cls();

    multiboot_info(magic, addr);
}
