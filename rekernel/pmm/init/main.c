#include <multiboot.h>
#include <printk.h>
#include <gdt.h>
#include <idt.h>
#include <timer.h>
#include <pmm.h>

void start_kernel(unsigned long magic, unsigned long addr)
{
    gdt_init();
    idt_init();

    /* Clear the screen. */
    cls();

    timer_init(200);

    enable_intr();

    //asm volatile("int $0x0");
    //asm volatile("int $0x4");

    //multiboot_info(magic, addr);
    printk("kernel in memory start: 0x%x\n", kern_start);
    printk("kernel in memory end: 0x%x\n", kern_end);
    printk("kernel in memory used: %d KB\n", (kern_end - kern_start + 1023) / 1024);

    pmm_init(addr);

    uint32_t alloc_addr = NULL;
    alloc_addr = pmm_alloc_page();
    printk("Alloc Physical Addr: 0x%x\n", alloc_addr);
    alloc_addr = pmm_alloc_page();
    printk("Alloc Physical Addr: 0x%x\n", alloc_addr);
    alloc_addr = pmm_alloc_page();
    printk("Alloc Physical Addr: 0x%x\n", alloc_addr);
    alloc_addr = pmm_alloc_page();
    printk("Alloc Physical Addr: 0x%x\n", alloc_addr);
    pmm_free_page(alloc_addr);
    pmm_free_page(alloc_addr);
    pmm_free_page(alloc_addr);
    pmm_free_page(alloc_addr);
}
