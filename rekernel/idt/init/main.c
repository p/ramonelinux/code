#include <multiboot.h>
#include <printk.h>
#include <gdt.h>
#include <idt.h>
#include <timer.h>

void start_kernel(unsigned long magic, unsigned long addr)
{
    gdt_init();
    idt_init();

    /* Clear the screen. */
    cls();

    timer_init(200);

    enable_intr();

    //asm volatile("int $0x0");
    //asm volatile("int $0x4");

    //multiboot_info(magic, addr);
}
