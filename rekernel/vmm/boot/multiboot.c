#include <multiboot.h>
#include <printk.h>

/* Check if the bit BIT in FLAGS is set. */
#define CHECK_FLAG(flags,bit)   ((flags) & (1 << (bit)))

/* Check if MAGIC is valid and print the Multiboot information structure
   pointed by ADDR. */
void multiboot_info()
{
    multiboot_info_t *mbi = glb_mboot_ptr;

    /* Print out the flags. */
    printk("flags = 0x%x\n", (unsigned)mbi->flags);

    /* Are mem_* valid? */
    if (CHECK_FLAG (mbi->flags, 0))
        printk("(0)mem_lower = %uKB, mem_upper = %uKB(%uMB)\n",
                (unsigned) mbi->mem_lower, (unsigned) mbi->mem_upper, (unsigned)(mbi->mem_upper / 1024));

    /* Is boot_device valid? */
    if (CHECK_FLAG (mbi->flags, 1))
        printk("(1)boot_device = 0x%x\n", (unsigned) mbi->boot_device);

    /* Is the command line passed? */
    if (CHECK_FLAG (mbi->flags, 2))
        printk("(2)cmdline = %s\n", (char *) mbi->cmdline);

    /* Are mods_* valid? */
    if (CHECK_FLAG (mbi->flags, 3)) {
        multiboot_module_t *mod;
        int i;

        printk("(3)mods_count = %d, mods_addr = 0x%x\n",
                (int) mbi->mods_count, (int) mbi->mods_addr);
        for (i = 0, mod = (multiboot_module_t *) mbi->mods_addr;
                i < mbi->mods_count;
                i++, mod++) {
            printk(" mod_start = 0x%x, mod_end = 0x%x, cmdline = %s\n",
                    (unsigned) mod->mod_start,
                    (unsigned) mod->mod_end,
                    (char *) mod->cmdline);
            printk(" mod%d: %s", i, (char *)mod->mod_start);
        }
    }

    /* Bits 4 and 5 are mutually exclusive! */
    if (CHECK_FLAG (mbi->flags, 4) && CHECK_FLAG (mbi->flags, 5)) {
        printk("Both bits 4 and 5 are set.\n");
        return;
    }

    /* Is the symbol table of a.out valid? */
    if (CHECK_FLAG (mbi->flags, 4)) {
        multiboot_aout_symbol_table_t *multiboot_aout_sym = &(mbi->u.aout_sym);

        printk("(4)multiboot_aout_symbol_table: tabsize = 0x%0x, "
                "strsize = 0x%x, addr = 0x%x\n",
                (unsigned) multiboot_aout_sym->tabsize,
                (unsigned) multiboot_aout_sym->strsize,
                (unsigned) multiboot_aout_sym->addr);
    }

    /* Is the section header table of ELF valid? */
    if (CHECK_FLAG (mbi->flags, 5)) {
        multiboot_elf_section_header_table_t *multiboot_elf_sec = &(mbi->u.elf_sec);

        printk("(5)multiboot_elf_sec: num = %u, size = 0x%x,"
                " addr = 0x%x, shndx = 0x%x\n",
                (unsigned) multiboot_elf_sec->num, (unsigned) multiboot_elf_sec->size,
                (unsigned) multiboot_elf_sec->addr, (unsigned) multiboot_elf_sec->shndx);
    }

    /* Are mmap_* valid? */
    if (CHECK_FLAG (mbi->flags, 6)) { 
        multiboot_memory_map_t *mmap;

        printk("(6)mmap_addr = 0x%x, mmap_length = 0x%x\n",
                (unsigned) mbi->mmap_addr, (unsigned) mbi->mmap_length);
        for (mmap = (multiboot_memory_map_t *) mbi->mmap_addr;
                (unsigned long) mmap < mbi->mmap_addr + mbi->mmap_length;
                mmap = (multiboot_memory_map_t *) ((unsigned long) mmap
                    + mmap->size + sizeof (mmap->size)))
            printk(" size = 0x%x, base_addr = 0x%x%x,"
                   " length = 0x%x%x, type = 0x%x\n",
                    (unsigned) mmap->size,
                    (unsigned)(mmap->addr >> 32), 
                    (unsigned)(mmap->addr & 0xffffffff),
                    (unsigned)(mmap->len >> 32), 
                    (unsigned)(mmap->len & 0xffffffff),
                    (unsigned) mmap->type);
    }
}
