#include <pmm.h>
#include <multiboot.h>
#include <printk.h>

static uint32_t pmm_stack[PAGE_MAX_SIZE + 1];
static uint32_t pmm_stack_top;

uint32_t phy_page_count;

void pmm_init()
{
    multiboot_info_t *mbi = glb_mboot_ptr;
    
    multiboot_memory_map_t *mmap;
    for (mmap = (multiboot_memory_map_t *) mbi->mmap_addr;
            (uint32_t)mmap < mbi->mmap_addr + mbi->mmap_length;
            mmap = (multiboot_memory_map_t *)((uint32_t)mmap + mmap->size + sizeof(mmap->size))) {
        uint32_t base_addr_low = (uint32_t)(mmap->addr & 0xffffffff);
        uint32_t length_low = (uint32_t)(mmap->len & 0xffffffff);

        if ((mmap->type == MULTIBOOT_MEMORY_AVAILABLE) && (base_addr_low == 0x100000)) {
            uint32_t page_addr = base_addr_low + (uint32_t)(kern_end - kern_start);
            uint32_t length = base_addr_low + length_low;

            while ((page_addr < length) && (page_addr < PMM_MAX_SIZE)) {
                pmm_free_page(page_addr);
                page_addr += PMM_PAGE_SIZE;
                phy_page_count++;
            }
        }
    }
}

uint32_t pmm_alloc_page()
{
    if (pmm_stack_top == 0)
        printk("out of memory\n");

    uint32_t page = pmm_stack[pmm_stack_top--];
    return page;
}

void pmm_free_page(uint32_t p)
{
    if (pmm_stack_top == PAGE_MAX_SIZE)
        printk("out of pmm_stack stack\n");

    pmm_stack[++pmm_stack_top] = p;
}
