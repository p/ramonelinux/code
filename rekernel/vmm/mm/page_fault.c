#include <idt.h>
#include <printk.h>

void page_fault(pt_regs_t *regs)
{
    printk("Page fault at EIP: 0x%x\n", regs->eip);
    while(1);
}
