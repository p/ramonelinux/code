#include <types.h>

#define STACK_SIZE 8192

// (512M)
#define PMM_MAX_SIZE 0x20000000

// 4096
#define PMM_PAGE_SIZE 0x1000

#define PAGE_MAX_SIZE (PMM_MAX_SIZE / PMM_PAGE_SIZE)

extern uint8_t kern_start[];
extern uint8_t kern_end[];

void pmm_init();

uint32_t pmm_alloc_page();
void pmm_free_page(uint32_t p);
