startup_32
    |
startup_32_smp
    |
i386_start_kernel()
    |-> i386_default_early_setup()
     -> start_kernel()

arch/x86/kernel/vmlinux.lds.S
 36 ENTRY(phys_startup_32)

 86         phys_startup_32 = startup_32 - LOAD_OFFSET;

 18 #define LOAD_OFFSET __PAGE_OFFSET

arch/x86/include/asm/page_32_types.h
  6 /*
  7  * This handles the memory map.
  8  *
  9  * A __PAGE_OFFSET of 0xC0000000 means that the kernel has
 10  * a virtual address space of one gigabyte, which limits the
 11  * amount of physical memory you can use to about 950MB.
 12  *
 13  * If you want more physical memory than this then see the CONFIG_HIGHMEM4G
 14  * and CONFIG_HIGHMEM64G options in the kernel configuration.
 15  */
 16 #define __PAGE_OFFSET       _AC(CONFIG_PAGE_OFFSET, UL)

arch/x86/kernel/head_32.S
439 is486:
440     movl $0x50022,%ecx  # set AM, WP, NE and MP
441     movl %cr0,%eax
442     andl $0x80000011,%eax   # Save PG,PE,ET
443     orl %ecx,%eax
444     movl %eax,%cr0
445 
446     lgdt early_gdt_descr
447     lidt idt_descr
448     ljmp $(__KERNEL_CS),$1f
449 1:  movl $(__KERNEL_DS),%eax    # reload all the segment registers
450     movl %eax,%ss           # after changing gdt.
451 
452     movl $(__USER_DS),%eax      # DS/ES contains default USER segment
453     movl %eax,%ds
454     movl %eax,%es
455 
456     movl $(__KERNEL_PERCPU), %eax
457     movl %eax,%fs           # set this cpu's percpu
458 
459     movl $(__KERNEL_STACK_CANARY),%eax
460     movl %eax,%gs
461 
462     xorl %eax,%eax          # Clear LDT
463     lldt %ax
464 
465     pushl $0        # fake return address for unwinder
466     jmp *(initial_code)

648 ENTRY(initial_code)
649     .long i386_start_kernel

arch/x86/kernel/head32.c
 23 static void __init i386_default_early_setup(void)
 24 {
 25     /* Initialize 32bit specific setup functions */
 26     x86_init.resources.reserve_resources = i386_reserve_resources;
 27     x86_init.mpparse.setup_ioapic_ids = setup_ioapic_ids_from_mpc;
 28 
 29     reserve_ebda_region();
 30 }
 31 
 32 asmlinkage __visible void __init i386_start_kernel(void)
 33 {
 34     sanitize_boot_params(&boot_params);
 35 
 36     /* Call the subarch specific early setup function */
 37     switch (boot_params.hdr.hardware_subarch) {
 38     case X86_SUBARCH_INTEL_MID:
 39         x86_intel_mid_early_setup();
 40         break;
 41     case X86_SUBARCH_CE4100:
 42         x86_ce4100_early_setup();
 43         break;
 44     default:
 45         i386_default_early_setup();
 46         break;
 47     }
 48 
 49     start_kernel();
 50 }

arch/x86/kernel/head.c
  8 /*
  9  * The BIOS places the EBDA/XBDA at the top of conventional
 10  * memory, and usually decreases the reported amount of
 11  * conventional memory (int 0x12) too. This also contains a
 12  * workaround for Dell systems that neglect to reserve EBDA.
 13  * The same workaround also avoids a problem with the AMD768MPX
 14  * chipset: reserve a page before VGA to prevent PCI prefetch
 15  * into it (errata #56). Usually the page is reserved anyways,
 16  * unless you have no PS/2 mouse plugged in.
 17  *
 18  * This functions is deliberately very conservative.  Losing
 19  * memory in the bottom megabyte is rarely a problem, as long
 20  * as we have enough memory to install the trampoline.  Using
 21  * memory that is in use by the BIOS or by some DMA device
 22  * the BIOS didn't shut down *is* a big problem.
 23  */
 24 
 25 #define BIOS_LOWMEM_KILOBYTES   0x413
 26 #define LOWMEM_CAP      0x9f000U    /* Absolute maximum */
 27 #define INSANE_CUTOFF       0x20000U    /* Less than this = insane */
 28 
 29 void __init reserve_ebda_region(void)
 30 {
 31     unsigned int lowmem, ebda_addr;
 32 
 33     /*
 34      * To determine the position of the EBDA and the
 35      * end of conventional memory, we need to look at
 36      * the BIOS data area. In a paravirtual environment
 37      * that area is absent. We'll just have to assume
 38      * that the paravirt case can handle memory setup
 39      * correctly, without our help.
 40      */
 41     if (paravirt_enabled())
 42         return;
 43 
 44     /* end of low (conventional) memory */
 45     lowmem = *(unsigned short *)__va(BIOS_LOWMEM_KILOBYTES);
 46     lowmem <<= 10;
 47 
 48     /* start of EBDA area */
 49     ebda_addr = get_bios_ebda();
 50 
 51     /*
 52      * Note: some old Dells seem to need 4k EBDA without
 53      * reporting so, so just consider the memory above 0x9f000
 54      * to be off limits (bugzilla 2990).
 55      */
 56 
 57     /* If the EBDA address is below 128K, assume it is bogus */
 58     if (ebda_addr < INSANE_CUTOFF)
 59         ebda_addr = LOWMEM_CAP;
 60 
 61     /* If lowmem is less than 128K, assume it is bogus */
 62     if (lowmem < INSANE_CUTOFF)
 63         lowmem = LOWMEM_CAP;
 64 
 65     /* Use the lower of the lowmem and EBDA markers as the cutoff */
 66     lowmem = min(lowmem, ebda_addr);
 67     lowmem = min(lowmem, LOWMEM_CAP); /* Absolute cap */
 68 
 69     /* reserve all memory between lowmem and the 1MB mark */
 70     memblock_reserve(lowmem, 0x100000 - lowmem);
 71 }
