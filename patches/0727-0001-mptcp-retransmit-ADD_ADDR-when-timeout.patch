From 10263bb42eac2f1c27c77e5e61552d067c63dc52 Mon Sep 17 00:00:00 2001
Message-Id: <10263bb42eac2f1c27c77e5e61552d067c63dc52.1600171893.git.geliangtang@gmail.com>
From: Geliang Tang <geliangtang@gmail.com>
To: mptcp@lists.01.org
Date: Tue, 15 Sep 2020 17:08:08 +0800
Subject: [MPTCP][PATCH v2 mptcp-next] mptcp: retransmit ADD_ADDR when timeout

This patch implemented the retransmition of ADD_ADDR when no ADD_ADDR echo
is received. It added a delayed_work with the announced address. When timeout
occurs, ADD_ADDR will be retransmitted.

Signed-off-by: Geliang Tang <geliangtang@gmail.com>
---
v2:
 - Use delayed_work instead of timer.
 - This patch depends on my another patch named 'Squash-to: "mptcp: remove
   addr and subflow in PM netlink"'.
---
 net/mptcp/options.c    |  1 +
 net/mptcp/pm_netlink.c | 68 +++++++++++++++++++++++++++++++++++++-----
 net/mptcp/protocol.h   |  1 +
 3 files changed, 63 insertions(+), 7 deletions(-)

diff --git a/net/mptcp/options.c b/net/mptcp/options.c
index 171039cbe9c4..283bd891b2b0 100644
--- a/net/mptcp/options.c
+++ b/net/mptcp/options.c
@@ -893,6 +893,7 @@ void mptcp_incoming_options(struct sock *sk, struct sk_buff *skb,
 			mptcp_pm_add_addr_received(msk, &addr);
 			MPTCP_INC_STATS(sock_net(sk), MPTCP_MIB_ADDADDR);
 		} else {
+			mptcp_pm_del_add_work(msk, &addr);
 			MPTCP_INC_STATS(sock_net(sk), MPTCP_MIB_ECHOADD);
 		}
 		mp_opt.add_addr = 0;
diff --git a/net/mptcp/pm_netlink.c b/net/mptcp/pm_netlink.c
index 8780d618a05a..55766367bb6e 100644
--- a/net/mptcp/pm_netlink.c
+++ b/net/mptcp/pm_netlink.c
@@ -28,6 +28,14 @@ struct mptcp_pm_addr_entry {
 	struct rcu_head		rcu;
 };
 
+struct mptcp_pm_add_entry {
+	struct list_head	list;
+	struct mptcp_addr_info	addr;
+	struct delayed_work	add_work;
+	struct mptcp_sock	*sock;
+	u8			retrans_times;
+};
+
 struct pm_nl_pernet {
 	/* protects pernet updates */
 	spinlock_t		lock;
@@ -41,6 +49,7 @@ struct pm_nl_pernet {
 };
 
 #define MPTCP_PM_ADDR_MAX	8
+#define ADD_ADDR_RETRANS_MAX	3
 
 static bool addresses_equal(const struct mptcp_addr_info *a,
 			    struct mptcp_addr_info *b, bool use_port)
@@ -178,10 +187,47 @@ static void check_work_pending(struct mptcp_sock *msk)
 		WRITE_ONCE(msk->pm.work_pending, false);
 }
 
+static void mptcp_pm_add_expire(struct work_struct *work)
+{
+	struct mptcp_pm_add_entry *entry =
+		container_of(work, struct mptcp_pm_add_entry, add_work.work);
+	struct mptcp_sock *msk = entry->sock;
+
+	pr_debug("msk=%p\n", msk);
+
+	if (!msk)
+		return;
+
+	spin_lock_bh(&msk->pm.lock);
+
+	if (entry->addr.id > 0) {
+		pr_debug("retransmit ADD_ADDR id=%d\n", entry->addr.id);
+		mptcp_pm_announce_addr(msk, &entry->addr, false);
+		entry->retrans_times++;
+	}
+
+	if (entry->retrans_times < ADD_ADDR_RETRANS_MAX)
+		schedule_delayed_work(&entry->add_work, TCP_RTO_MAX);
+
+	spin_unlock_bh(&msk->pm.lock);
+}
+
+void mptcp_pm_del_add_work(struct mptcp_sock *msk, struct mptcp_addr_info *addr)
+{
+	struct mptcp_pm_add_entry *entry;
+
+	spin_lock_bh(&msk->pm.lock);
+	list_for_each_entry(entry, &msk->pm.anno_list, list) {
+		if (addresses_equal(&entry->addr, addr, false))
+			cancel_delayed_work(&entry->add_work);
+	}
+	spin_unlock_bh(&msk->pm.lock);
+}
+
 static bool lookup_anno_list_by_saddr(struct mptcp_sock *msk,
 				      struct mptcp_addr_info *addr)
 {
-	struct mptcp_pm_addr_entry *entry;
+	struct mptcp_pm_add_entry *entry;
 
 	list_for_each_entry(entry, &msk->pm.anno_list, list) {
 		if (addresses_equal(&entry->addr, addr, false))
@@ -194,28 +240,36 @@ static bool lookup_anno_list_by_saddr(struct mptcp_sock *msk,
 static bool mptcp_pm_alloc_anno_list(struct mptcp_sock *msk,
 				     struct mptcp_pm_addr_entry *entry)
 {
-	struct mptcp_pm_addr_entry *clone = NULL;
+	struct mptcp_pm_add_entry *add_entry = NULL;
 
 	if (lookup_anno_list_by_saddr(msk, &entry->addr))
 		return false;
 
-	clone = kmemdup(entry, sizeof(*entry), GFP_ATOMIC);
-	if (!clone)
+	add_entry = kmalloc(sizeof(*add_entry), GFP_ATOMIC);
+	if (!add_entry)
 		return false;
 
-	list_add(&clone->list, &msk->pm.anno_list);
+	list_add(&add_entry->list, &msk->pm.anno_list);
+
+	add_entry->addr = entry->addr;
+	add_entry->sock = msk;
+	add_entry->retrans_times = 0;
+
+	INIT_DELAYED_WORK(&add_entry->add_work, mptcp_pm_add_expire);
+	schedule_delayed_work(&add_entry->add_work, TCP_RTO_MAX);
 
 	return true;
 }
 
 void mptcp_pm_free_anno_list(struct mptcp_sock *msk)
 {
-	struct mptcp_pm_addr_entry *entry, *tmp;
+	struct mptcp_pm_add_entry *entry, *tmp;
 
 	pr_debug("msk=%p\n", msk);
 
 	spin_lock_bh(&msk->pm.lock);
 	list_for_each_entry_safe(entry, tmp, &msk->pm.anno_list, list) {
+		cancel_delayed_work(&entry->add_work);
 		list_del(&entry->list);
 		kfree(entry);
 	}
@@ -654,7 +708,7 @@ __lookup_addr_by_id(struct pm_nl_pernet *pernet, unsigned int id)
 static bool remove_anno_list_by_saddr(struct mptcp_sock *msk,
 				      struct mptcp_addr_info *addr)
 {
-	struct mptcp_pm_addr_entry *entry, *tmp;
+	struct mptcp_pm_add_entry *entry, *tmp;
 
 	list_for_each_entry_safe(entry, tmp, &msk->pm.anno_list, list) {
 		if (addresses_equal(&entry->addr, addr, false)) {
diff --git a/net/mptcp/protocol.h b/net/mptcp/protocol.h
index db1e5de2fee7..16db6ea10a00 100644
--- a/net/mptcp/protocol.h
+++ b/net/mptcp/protocol.h
@@ -444,6 +444,7 @@ void mptcp_pm_add_addr_received(struct mptcp_sock *msk,
 				const struct mptcp_addr_info *addr);
 void mptcp_pm_rm_addr_received(struct mptcp_sock *msk, u8 rm_id);
 void mptcp_pm_free_anno_list(struct mptcp_sock *msk);
+void mptcp_pm_del_add_work(struct mptcp_sock *msk, struct mptcp_addr_info *addr);
 
 int mptcp_pm_announce_addr(struct mptcp_sock *msk,
 			   const struct mptcp_addr_info *addr,
-- 
2.17.1

