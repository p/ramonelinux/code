From 590bc9cdd343eedd5c2789917920ceb6f3894593 Mon Sep 17 00:00:00 2001
Message-Id: <590bc9cdd343eedd5c2789917920ceb6f3894593.1444830957.git.geliangtang@163.com>
From: Geliang Tang <geliangtang@163.com>
Date: Wed, 14 Oct 2015 21:55:29 +0800
Subject: [PATCH] pstore: add pstore_unregister()

Add pstore_unregister().

Signed-off-by: Geliang Tang <geliangtang@163.com>
---
 fs/pstore/ftrace.c     | 20 +++++++++++----
 fs/pstore/internal.h   | 12 ++++++---
 fs/pstore/platform.c   | 68 ++++++++++++++++++++++++++++++++++++++++++++------
 fs/pstore/pmsg.c       | 59 +++++++++++++++++++++++++++++--------------
 fs/pstore/ram.c        |  1 +
 include/linux/pstore.h |  3 +++
 6 files changed, 127 insertions(+), 36 deletions(-)

diff --git a/fs/pstore/ftrace.c b/fs/pstore/ftrace.c
index 76a4eeb..2ccd7e2 100644
--- a/fs/pstore/ftrace.c
+++ b/fs/pstore/ftrace.c
@@ -104,18 +104,19 @@ static const struct file_operations pstore_knob_fops = {
 	.write	= pstore_ftrace_knob_write,
 };
 
-void pstore_register_ftrace(void)
+static struct dentry *dir;
+
+int pstore_register_ftrace(void)
 {
-	struct dentry *dir;
 	struct dentry *file;
 
 	if (!psinfo->write_buf)
-		return;
+		goto err;
 
 	dir = debugfs_create_dir("pstore", NULL);
 	if (!dir) {
 		pr_err("%s: unable to create pstore directory\n", __func__);
-		return;
+		goto err;
 	}
 
 	file = debugfs_create_file("record_ftrace", 0600, dir, NULL,
@@ -125,7 +126,16 @@ void pstore_register_ftrace(void)
 		goto err_file;
 	}
 
-	return;
+	return 0;
+
 err_file:
 	debugfs_remove(dir);
+err:
+	return -ENOMEM;
+}
+
+void pstore_unregister_ftrace(void)
+{
+	unregister_ftrace_function(&pstore_ftrace_ops);
+	debugfs_remove_recursive(dir);
 }
diff --git a/fs/pstore/internal.h b/fs/pstore/internal.h
index c36ba2c..4e9bc29 100644
--- a/fs/pstore/internal.h
+++ b/fs/pstore/internal.h
@@ -40,15 +40,19 @@ pstore_ftrace_decode_cpu(struct pstore_ftrace_record *rec)
 }
 
 #ifdef CONFIG_PSTORE_FTRACE
-extern void pstore_register_ftrace(void);
+extern int pstore_register_ftrace(void);
+extern void pstore_unregister_ftrace(void);
 #else
-static inline void pstore_register_ftrace(void) {}
+static inline int pstore_register_ftrace(void) { return 0; }
+static inline void pstore_unregister_ftrace(void) {}
 #endif
 
 #ifdef CONFIG_PSTORE_PMSG
-extern void pstore_register_pmsg(void);
+extern int pstore_register_pmsg(void);
+extern void pstore_unregister_pmsg(void);
 #else
-static inline void pstore_register_pmsg(void) {}
+static inline int pstore_register_pmsg(void) { return 0; }
+static inline void pstore_unregister_pmsg(void) {}
 #endif
 
 extern struct pstore_info *psinfo;
diff --git a/fs/pstore/platform.c b/fs/pstore/platform.c
index 791743d..16b803a 100644
--- a/fs/pstore/platform.c
+++ b/fs/pstore/platform.c
@@ -237,6 +237,12 @@ static void allocate_buf_for_compression(void)
 
 }
 
+static void free_buf_for_compression(void)
+{
+	kfree(stream.workspace);
+	kfree(big_oops_buf);
+}
+
 /*
  * Called when compression fails, since the printk buffer
  * would be fetched for compression calling it again when
@@ -353,6 +359,19 @@ static struct kmsg_dumper pstore_dumper = {
 	.dump = pstore_dump,
 };
 
+/*
+ * Register with kmsg_dump to save last part of console log on panic.
+ */
+static int pstore_register_kmsg(void)
+{
+	return kmsg_dump_register(&pstore_dumper);
+}
+
+static void pstore_unregister_kmsg(void)
+{
+	kmsg_dump_unregister(&pstore_dumper);
+}
+
 #ifdef CONFIG_PSTORE_CONSOLE
 static void pstore_console_write(struct console *con, const char *s, unsigned c)
 {
@@ -386,12 +405,19 @@ static struct console pstore_console = {
 	.index	= -1,
 };
 
-static void pstore_register_console(void)
+static int pstore_register_console(void)
 {
 	register_console(&pstore_console);
+	return 0;
+}
+
+static void pstore_unregister_console(void)
+{
+	unregister_console(&pstore_console);
 }
 #else
-static void pstore_register_console(void) {}
+static int pstore_register_console(void) { return 0; }
+static void pstore_unregister_console(void) {}
 #endif
 
 static int pstore_write_compat(enum pstore_type_id type,
@@ -410,12 +436,11 @@ static int pstore_write_compat(enum pstore_type_id type,
  * read function right away to populate the file system. If not
  * then the pstore mount code will call us later to fill out
  * the file system.
- *
- * Register with kmsg_dump to save last part of console log on panic.
  */
 int pstore_register(struct pstore_info *psi)
 {
 	struct module *owner = psi->owner;
+	int ret;
 
 	if (backend && strcmp(backend, psi->name))
 		return -EPERM;
@@ -442,12 +467,20 @@ int pstore_register(struct pstore_info *psi)
 	if (pstore_is_mounted())
 		pstore_get_records(0);
 
-	kmsg_dump_register(&pstore_dumper);
+	ret = pstore_register_kmsg();
+	if (ret)
+		goto out;
 
 	if ((psi->flags & PSTORE_FLAGS_FRAGILE) == 0) {
-		pstore_register_console();
-		pstore_register_ftrace();
-		pstore_register_pmsg();
+		ret = pstore_register_console();
+		if (ret)
+			goto register_console_fail;
+		ret = pstore_register_ftrace();
+		if (ret)
+			goto register_ftrace_fail;
+		ret = pstore_register_pmsg();
+		if (ret)
+			goto register_pmsg_fail;
 	}
 
 	if (pstore_update_ms >= 0) {
@@ -465,9 +498,28 @@ int pstore_register(struct pstore_info *psi)
 	pr_info("Registered %s as persistent store backend\n", psi->name);
 
 	return 0;
+
+register_pmsg_fail:
+	pstore_unregister_ftrace();
+register_ftrace_fail:
+	pstore_unregister_console();
+register_console_fail:
+	pstore_unregister_kmsg();
+out:
+	return ret;
 }
 EXPORT_SYMBOL_GPL(pstore_register);
 
+void pstore_unregister(void)
+{
+	pstore_unregister_pmsg();
+	pstore_unregister_ftrace();
+	pstore_unregister_console();
+	pstore_unregister_kmsg();
+	free_buf_for_compression();
+}
+EXPORT_SYMBOL_GPL(pstore_unregister);
+
 /*
  * Read all the records from the persistent store. Create
  * files in our filesystem.  Don't warn about -EEXIST errors
diff --git a/fs/pstore/pmsg.c b/fs/pstore/pmsg.c
index feb5dd2..306ce52 100644
--- a/fs/pstore/pmsg.c
+++ b/fs/pstore/pmsg.c
@@ -15,10 +15,18 @@
 #include <linux/device.h>
 #include <linux/fs.h>
 #include <linux/uaccess.h>
+#include <linux/slab.h>
 #include <linux/vmalloc.h>
 #include "internal.h"
 
-static DEFINE_MUTEX(pmsg_lock);
+struct pstore_pmsg {
+	int major;
+	struct class *class;
+	struct device *device;
+	struct mutex lock;
+};
+static struct pstore_pmsg *pmsg;
+
 #define PMSG_MAX_BOUNCE_BUFFER_SIZE (2*PAGE_SIZE)
 
 static ssize_t write_pmsg(struct file *file, const char __user *buf,
@@ -37,8 +45,10 @@ static ssize_t write_pmsg(struct file *file, const char __user *buf,
 	if (buffer_size > PMSG_MAX_BOUNCE_BUFFER_SIZE)
 		buffer_size = PMSG_MAX_BOUNCE_BUFFER_SIZE;
 	buffer = vmalloc(buffer_size);
+	if (!buffer)
+		return -ENOMEM;
 
-	mutex_lock(&pmsg_lock);
+	mutex_lock(&pmsg->lock);
 	for (i = 0; i < count; ) {
 		size_t c = min(count - i, buffer_size);
 		u64 id;
@@ -46,7 +56,7 @@ static ssize_t write_pmsg(struct file *file, const char __user *buf,
 
 		ret = __copy_from_user(buffer, buf + i, c);
 		if (unlikely(ret != 0)) {
-			mutex_unlock(&pmsg_lock);
+			mutex_unlock(&pmsg->lock);
 			vfree(buffer);
 			return -EFAULT;
 		}
@@ -56,7 +66,7 @@ static ssize_t write_pmsg(struct file *file, const char __user *buf,
 		i += c;
 	}
 
-	mutex_unlock(&pmsg_lock);
+	mutex_unlock(&pmsg->lock);
 	vfree(buffer);
 	return count;
 }
@@ -67,8 +77,6 @@ static const struct file_operations pmsg_fops = {
 	.write		= write_pmsg,
 };
 
-static struct class *pmsg_class;
-static int pmsg_major;
 #define PMSG_NAME "pmsg"
 #undef pr_fmt
 #define pr_fmt(fmt) PMSG_NAME ": " fmt
@@ -80,35 +88,48 @@ static char *pmsg_devnode(struct device *dev, umode_t *mode)
 	return NULL;
 }
 
-void pstore_register_pmsg(void)
+int pstore_register_pmsg(void)
 {
-	struct device *pmsg_device;
+	pmsg = kzalloc(sizeof(*pmsg), GFP_KERNEL);
+	if (!pmsg)
+		return -ENOMEM;
+
+	mutex_init(&pmsg->lock);
 
-	pmsg_major = register_chrdev(0, PMSG_NAME, &pmsg_fops);
-	if (pmsg_major < 0) {
+	pmsg->major = register_chrdev(0, PMSG_NAME, &pmsg_fops);
+	if (pmsg->major < 0) {
 		pr_err("register_chrdev failed\n");
 		goto err;
 	}
 
-	pmsg_class = class_create(THIS_MODULE, PMSG_NAME);
-	if (IS_ERR(pmsg_class)) {
+	pmsg->class = class_create(THIS_MODULE, PMSG_NAME);
+	if (IS_ERR(pmsg->class)) {
 		pr_err("device class file already in use\n");
 		goto err_class;
 	}
-	pmsg_class->devnode = pmsg_devnode;
+	pmsg->class->devnode = pmsg_devnode;
 
-	pmsg_device = device_create(pmsg_class, NULL, MKDEV(pmsg_major, 0),
+	pmsg->device = device_create(pmsg->class, NULL, MKDEV(pmsg->major, 0),
 					NULL, "%s%d", PMSG_NAME, 0);
-	if (IS_ERR(pmsg_device)) {
+	if (IS_ERR(pmsg->device)) {
 		pr_err("failed to create device\n");
 		goto err_device;
 	}
-	return;
+	return 0;
 
 err_device:
-	class_destroy(pmsg_class);
+	class_destroy(pmsg->class);
 err_class:
-	unregister_chrdev(pmsg_major, PMSG_NAME);
+	unregister_chrdev(pmsg->major, PMSG_NAME);
 err:
-	return;
+	kfree(pmsg);
+	return -1;
+}
+
+void pstore_unregister_pmsg(void)
+{
+	device_destroy(pmsg->class, MKDEV(pmsg->major, 0));
+	class_destroy(pmsg->class);
+	unregister_chrdev(pmsg->major, PMSG_NAME);
+	kfree(pmsg);
 }
diff --git a/fs/pstore/ram.c b/fs/pstore/ram.c
index 6c26c4d..7b93c08 100644
--- a/fs/pstore/ram.c
+++ b/fs/pstore/ram.c
@@ -593,6 +593,7 @@ static int __exit ramoops_remove(struct platform_device *pdev)
 	/* TODO(kees): When pstore supports unregistering, call it here. */
 	kfree(cxt->pstore.buf);
 	cxt->pstore.bufsize = 0;
+	pstore_unregister();
 
 	return 0;
 #endif
diff --git a/include/linux/pstore.h b/include/linux/pstore.h
index 8e7a25b..2146056 100644
--- a/include/linux/pstore.h
+++ b/include/linux/pstore.h
@@ -77,6 +77,7 @@ struct pstore_info {
 
 #ifdef CONFIG_PSTORE
 extern int pstore_register(struct pstore_info *);
+extern void pstore_unregister(void);
 extern bool pstore_cannot_block_path(enum kmsg_dump_reason reason);
 #else
 static inline int
@@ -84,6 +85,8 @@ pstore_register(struct pstore_info *psi)
 {
 	return -ENODEV;
 }
+static inline void
+pstore_unregister(void) {}
 static inline bool
 pstore_cannot_block_path(enum kmsg_dump_reason reason)
 {
-- 
2.5.0

