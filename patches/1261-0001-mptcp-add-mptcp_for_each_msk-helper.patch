From ba5b4db66f47311d65703f5ea25430a5957106a9 Mon Sep 17 00:00:00 2001
Message-Id: <ba5b4db66f47311d65703f5ea25430a5957106a9.1617703144.git.geliangtang@gmail.com>
In-Reply-To: <cover.1617703144.git.geliangtang@gmail.com>
References: <cover.1617703144.git.geliangtang@gmail.com>
From: Geliang Tang <geliangtang@gmail.com>
To: mptcp@lists.linux.dev
Date: Wed, 31 Mar 2021 12:04:51 +0800
Subject: [MPTCP][PATCH mptcp-next 1/2] mptcp: add mptcp_for_each_msk helper

This patch added a new macro helper mptcp_for_each_msk to traverse each
existing msk socket in the net namespace.

Signed-off-by: Geliang Tang <geliangtang@gmail.com>
---
 net/mptcp/pm_netlink.c | 39 +++++++++++++++++----------------------
 1 file changed, 17 insertions(+), 22 deletions(-)

diff --git a/net/mptcp/pm_netlink.c b/net/mptcp/pm_netlink.c
index 6ba040897738..b9044defd89b 100644
--- a/net/mptcp/pm_netlink.c
+++ b/net/mptcp/pm_netlink.c
@@ -58,6 +58,10 @@ struct pm_nl_pernet {
 #define MPTCP_PM_ADDR_MAX	8
 #define ADD_ADDR_RETRANS_MAX	3
 
+#define mptcp_for_each_msk(__net, __msk)				\
+	long __slot = 0, __num = 0;					\
+	while ((__msk = mptcp_token_iter_next(__net, &(__slot), &(__num))) != NULL)
+
 static bool addresses_equal(const struct mptcp_addr_info *a,
 			    struct mptcp_addr_info *b, bool use_port)
 {
@@ -985,9 +989,8 @@ static struct pm_nl_pernet *genl_info_pm_nl(struct genl_info *info)
 static int mptcp_nl_add_subflow_or_signal_addr(struct net *net)
 {
 	struct mptcp_sock *msk;
-	long s_slot = 0, s_num = 0;
 
-	while ((msk = mptcp_token_iter_next(net, &s_slot, &s_num)) != NULL) {
+	mptcp_for_each_msk(net, msk) {
 		struct sock *sk = (struct sock *)msk;
 
 		if (!READ_ONCE(msk->fully_established))
@@ -1096,14 +1099,9 @@ static int mptcp_nl_remove_subflow_and_signal_addr(struct net *net,
 						   struct mptcp_addr_info *addr)
 {
 	struct mptcp_sock *msk;
-	long s_slot = 0, s_num = 0;
-	struct mptcp_rm_list list = { .nr = 0 };
-
-	pr_debug("remove_id=%d", addr->id);
 
-	list.ids[list.nr++] = addr->id;
-
-	while ((msk = mptcp_token_iter_next(net, &s_slot, &s_num)) != NULL) {
+	mptcp_for_each_msk(net, msk) {
+		struct mptcp_rm_list list = { .nr = 0 };
 		struct sock *sk = (struct sock *)msk;
 		bool remove_subflow;
 
@@ -1112,6 +1110,8 @@ static int mptcp_nl_remove_subflow_and_signal_addr(struct net *net,
 			goto next;
 		}
 
+		list.ids[list.nr++] = addr->id;
+
 		lock_sock(sk);
 		remove_subflow = lookup_subflow_by_saddr(&msk->conn_list, addr);
 		mptcp_pm_remove_anno_addr(msk, addr, remove_subflow);
@@ -1162,13 +1162,10 @@ static void mptcp_pm_free_addr_entry(struct mptcp_pm_addr_entry *entry)
 static int mptcp_nl_remove_id_zero_address(struct net *net,
 					   struct mptcp_addr_info *addr)
 {
-	struct mptcp_rm_list list = { .nr = 0 };
-	long s_slot = 0, s_num = 0;
 	struct mptcp_sock *msk;
 
-	list.ids[list.nr++] = 0;
-
-	while ((msk = mptcp_token_iter_next(net, &s_slot, &s_num)) != NULL) {
+	mptcp_for_each_msk(net, msk) {
+		struct mptcp_rm_list list = { .nr = 0 };
 		struct sock *sk = (struct sock *)msk;
 		struct mptcp_addr_info msk_local;
 
@@ -1179,6 +1176,8 @@ static int mptcp_nl_remove_id_zero_address(struct net *net,
 		if (!addresses_equal(&msk_local, addr, addr->port))
 			goto next;
 
+		list.ids[list.nr++] = 0;
+
 		lock_sock(sk);
 		spin_lock_bh(&msk->pm.lock);
 		mptcp_pm_remove_addr(msk, &list);
@@ -1271,13 +1270,9 @@ static void mptcp_pm_remove_addrs_and_subflows(struct mptcp_sock *msk,
 static void mptcp_nl_remove_addrs_list(struct net *net,
 				       struct list_head *rm_list)
 {
-	long s_slot = 0, s_num = 0;
 	struct mptcp_sock *msk;
 
-	if (list_empty(rm_list))
-		return;
-
-	while ((msk = mptcp_token_iter_next(net, &s_slot, &s_num)) != NULL) {
+	mptcp_for_each_msk(net, msk) {
 		struct sock *sk = (struct sock *)msk;
 
 		lock_sock(sk);
@@ -1320,7 +1315,8 @@ static int mptcp_nl_cmd_flush_addrs(struct sk_buff *skb, struct genl_info *info)
 	pernet->next_id = 1;
 	bitmap_zero(pernet->id_bitmap, MAX_ADDR_ID + 1);
 	spin_unlock_bh(&pernet->lock);
-	mptcp_nl_remove_addrs_list(sock_net(skb->sk), &free_list);
+	if (!list_empty(&free_list))
+		mptcp_nl_remove_addrs_list(sock_net(skb->sk), &free_list);
 	__flush_addrs(&free_list);
 	return 0;
 }
@@ -1535,11 +1531,10 @@ static int mptcp_nl_addr_backup(struct net *net,
 				struct mptcp_addr_info *addr,
 				u8 bkup)
 {
-	long s_slot = 0, s_num = 0;
 	struct mptcp_sock *msk;
 	int ret = -EINVAL;
 
-	while ((msk = mptcp_token_iter_next(net, &s_slot, &s_num)) != NULL) {
+	mptcp_for_each_msk(net, msk) {
 		struct sock *sk = (struct sock *)msk;
 
 		if (list_empty(&msk->conn_list))
-- 
2.30.2

