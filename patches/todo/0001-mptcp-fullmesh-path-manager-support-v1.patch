From 8daa965176250096a11d016628cbd72431e5ea89 Mon Sep 17 00:00:00 2001
From: Geliang Tang <geliangtang@gmail.com>
Date: Thu, 24 Jun 2021 16:47:55 +0800
Subject: [PATCH] mptcp: fullmesh path manager support v1

Implement the in-kernel fullmesh path manager like on the mptcp.org
kernel.

Signed-off-by: Geliang Tang <geliangtang@gmail.com>
---
 net/mptcp/Makefile                            |   2 +-
 net/mptcp/pm.c                                |   2 +
 net/mptcp/pm_fullmesh.c                       | 283 ++++++++++++++++++
 net/mptcp/pm_netlink.c                        |  10 +-
 net/mptcp/protocol.c                          |   7 +-
 net/mptcp/protocol.h                          |   3 +
 net/mptcp/subflow.c                           |   2 +-
 .../testing/selftests/net/mptcp/mptcp_join.sh |  11 +
 8 files changed, 311 insertions(+), 9 deletions(-)
 create mode 100644 net/mptcp/pm_fullmesh.c

diff --git a/net/mptcp/Makefile b/net/mptcp/Makefile
index 0a0608b6b4b4..f295c12f38b4 100644
--- a/net/mptcp/Makefile
+++ b/net/mptcp/Makefile
@@ -3,7 +3,7 @@ obj-$(CONFIG_MPTCP) += mptcp.o
 ccflags-y += -DDEBUG
 
 mptcp-y := protocol.o subflow.o options.o token.o crypto.o ctrl.o pm.o diag.o \
-	   mib.o pm_netlink.o sockopt.o
+	   mib.o pm_netlink.o pm_fullmesh.o sockopt.o
 
 obj-$(CONFIG_SYN_COOKIES) += syncookies.o
 obj-$(CONFIG_INET_MPTCP_DIAG) += mptcp_diag.o
diff --git a/net/mptcp/pm.c b/net/mptcp/pm.c
index 639271e09604..9fe2c082e433 100644
--- a/net/mptcp/pm.c
+++ b/net/mptcp/pm.c
@@ -327,9 +327,11 @@ void mptcp_pm_data_init(struct mptcp_sock *msk)
 	INIT_LIST_HEAD(&msk->pm.anno_list);
 
 	mptcp_pm_nl_data_init(msk);
+	mptcp_pm_fm_data_init(msk);
 }
 
 void __init mptcp_pm_init(void)
 {
 	mptcp_pm_nl_init();
+	mptcp_pm_fm_init();
 }
diff --git a/net/mptcp/pm_fullmesh.c b/net/mptcp/pm_fullmesh.c
new file mode 100644
index 000000000000..ca196ffde6ad
--- /dev/null
+++ b/net/mptcp/pm_fullmesh.c
@@ -0,0 +1,283 @@
+// SPDX-License-Identifier: GPL-2.0
+
+#define pr_fmt(fmt) "MPTCP: " fmt
+
+#include <linux/kernel.h>
+#include <linux/notifier.h>
+#include <linux/inetdevice.h>
+#include <linux/proc_fs.h>
+#include <net/addrconf.h>
+#include <net/netns/generic.h>
+
+#include "protocol.h"
+
+static int pm_fm_pernet_id;
+
+struct mptcp_pm_addr_entry {
+	struct list_head	list;
+	struct mptcp_addr_info	addr;
+};
+
+struct pm_fm_pernet {
+	spinlock_t		lock;
+	struct list_head	local_addr_list;
+	unsigned int 		next_id;
+};
+
+void check_work_pending(struct mptcp_sock *msk);
+bool lookup_subflow_by_saddr(const struct list_head *list,
+			     struct mptcp_addr_info *saddr);
+void remote_address(const struct sock_common *skc,
+		    struct mptcp_addr_info *addr);
+
+static struct mptcp_pm_addr_entry *
+fm_select_local_address(const struct pm_fm_pernet *pernet,
+		        struct mptcp_sock *msk)
+{
+	struct mptcp_pm_addr_entry *entry, *ret = NULL;
+	struct sock *sk = (struct sock *)msk;
+
+	pr_debug("%s", __func__);
+
+	msk_owned_by_me(msk);
+
+	rcu_read_lock();
+	__mptcp_flush_join_list(msk);
+	list_for_each_entry_rcu(entry, &pernet->local_addr_list, list) {
+		if (entry->addr.family != sk->sk_family)
+				continue;
+
+		/* avoid any address already in use by subflows and
+		 * pending join
+		 */
+		if (!lookup_subflow_by_saddr(&msk->conn_list, &entry->addr)) {
+			ret = entry;
+			break;
+		}
+	}
+	rcu_read_unlock();
+	return ret;
+}
+
+static void mptcp_fm_create_subflow_or_signal_addr(struct mptcp_sock *msk)
+{
+	struct sock *sk = (struct sock *)msk;
+	struct mptcp_pm_addr_entry *local;
+	struct pm_fm_pernet *pernet;
+
+	pernet = net_generic(sock_net(sk), pm_fm_pernet_id);
+
+	pr_debug("%s", __func__);
+
+	local = fm_select_local_address(pernet, msk);
+	if (local) {
+		struct mptcp_addr_info remote = { 0 };
+
+		pr_debug("%s local=%pI4", __func__, &local->addr.addr);
+		msk->pm.local_addr_used++;
+		msk->pm.subflows++;
+		//check_work_pending(msk);
+		remote_address((struct sock_common *)sk, &remote);
+		spin_unlock_bh(&msk->pm.lock);
+		__mptcp_subflow_connect(sk, &local->addr, &remote, 0, 0);
+		spin_lock_bh(&msk->pm.lock);
+		return;
+	}
+
+	pr_debug("%s local is null", __func__);
+
+	/* lookup failed, avoid fourther attempts later */
+	//msk->pm.local_addr_used = 8;
+	//check_work_pending(msk);
+}
+
+static void mptcp_pm_fm_fully_established(struct mptcp_sock *msk)
+{
+	mptcp_fm_create_subflow_or_signal_addr(msk);
+}
+
+static void mptcp_pm_fm_subflow_established(struct mptcp_sock *msk)
+{
+	mptcp_fm_create_subflow_or_signal_addr(msk);
+}
+
+void mptcp_pm_fm_work(struct mptcp_sock *msk)
+{
+	struct mptcp_pm_data *pm = &msk->pm;
+
+	msk_owned_by_me(msk);
+
+	spin_lock_bh(&msk->pm.lock);
+
+	pr_debug("%s msk=%p status=%x", __func__, msk, pm->status);
+	if (pm->status & BIT(MPTCP_PM_ESTABLISHED)) {
+		pm->status &= ~BIT(MPTCP_PM_ESTABLISHED);
+		mptcp_pm_fm_fully_established(msk);
+	}
+	if (pm->status & BIT(MPTCP_PM_SUBFLOW_ESTABLISHED)) {
+		pm->status &= ~BIT(MPTCP_PM_SUBFLOW_ESTABLISHED);
+		mptcp_pm_fm_subflow_established(msk);
+	}
+
+	spin_unlock_bh(&msk->pm.lock);
+}
+
+static int mptcp_pm_fm_append_new_local_addr(struct pm_fm_pernet *pernet,
+					     struct mptcp_pm_addr_entry *entry)
+{
+	struct mptcp_pm_addr_entry *cur;
+
+	spin_lock_bh(&pernet->lock);
+
+	list_for_each_entry(cur, &pernet->local_addr_list, list) {
+		//
+	}
+
+	list_add_tail_rcu(&entry->list, &pernet->local_addr_list);
+	spin_unlock_bh(&pernet->lock);
+
+	return 0;
+}
+
+static void addr_event_handler(const struct in_ifaddr *ifa, unsigned long event,
+			       struct net *net)
+{
+	struct pm_fm_pernet *pernet = net_generic(net, pm_fm_pernet_id);
+	const struct net_device *netdev = ifa->ifa_dev->dev;
+	struct mptcp_pm_addr_entry *entry;
+
+	if (ifa->ifa_scope > RT_SCOPE_LINK ||
+	    ipv4_is_loopback(ifa->ifa_local))
+		return;
+
+	entry = kmalloc(sizeof(*entry), GFP_KERNEL);
+	if (!entry) {
+		pr_err("can't allocate addr");
+		return;
+	}
+
+	entry->addr.family = AF_INET;
+	entry->addr.id = pernet->next_id++;
+	entry->addr.port = 0;
+	entry->addr.addr.s_addr = ifa->ifa_local;
+
+	if (event == NETDEV_UP)
+		mptcp_pm_fm_append_new_local_addr(pernet, entry);
+	else if (event == NETDEV_DOWN)
+		;
+	else if (event == NETDEV_CHANGE)
+		;
+}
+
+static int mptcp_pm_addr_event(struct notifier_block *this,
+			       unsigned long event, void *ptr)
+{
+	const struct in_ifaddr *ifa = (struct in_ifaddr *)ptr;
+	struct net *net = dev_net(ifa->ifa_dev->dev);
+
+	if (!(event == NETDEV_UP || event == NETDEV_DOWN || event == NETDEV_CHANGE))
+		return NOTIFY_DONE;
+
+	addr_event_handler(ifa, event, net);
+
+	return NOTIFY_DONE;
+}
+
+static struct notifier_block mptcp_pm_addr_notifier = {
+	.notifier_call = mptcp_pm_addr_event,
+};
+
+static int mptcp_pm_addr6_event(struct notifier_block *this,
+				unsigned long event, void *ptr)
+{
+	const struct inet6_ifaddr *ifa6 = (struct inet6_ifaddr *)ptr;
+	struct net *net = dev_net(ifa6->idev->dev);
+
+	if (!(event == NETDEV_UP || event == NETDEV_DOWN || event == NETDEV_CHANGE))
+		return NOTIFY_DONE;
+
+	return NOTIFY_DONE;
+}
+
+static struct notifier_block mptcp_pm_addr6_notifier = {
+	.notifier_call = mptcp_pm_addr6_event,
+};
+
+/* Output /proc/net/mptcp_fullmesh */
+static int mptcp_fm_seq_show(struct seq_file *seq, void *v)
+{
+	const struct net *net = seq->private;
+	struct mptcp_pm_addr_entry *cur;
+	struct pm_fm_pernet *pernet;
+	int i;
+
+	pernet = net_generic(net, pm_fm_pernet_id);
+
+	seq_printf(seq, "Index, Address-ID, Backup, IP-address, if-idx\n");
+
+	spin_lock_bh(&pernet->lock);
+
+	list_for_each_entry(cur, &pernet->local_addr_list, list) {
+		seq_printf(seq, "%u, %u, %pI4\n", i++, cur->addr.id, &cur->addr.addr);
+	}
+
+	spin_unlock_bh(&pernet->lock);
+
+	return 0;
+}
+
+static int __net_init pm_fm_init_net(struct net *net)
+{
+	struct pm_fm_pernet *pernet = net_generic(net, pm_fm_pernet_id);
+
+	if (!proc_create_net_single("mptcp_fullmesh", S_IRUGO, net->proc_net,
+				    mptcp_fm_seq_show, NULL))
+		return -1;
+
+	spin_lock_init(&pernet->lock);
+	INIT_LIST_HEAD_RCU(&pernet->local_addr_list);
+	pernet->next_id = 1;
+
+	return 0;
+}
+
+static void __net_exit pm_fm_exit_net(struct list_head *net_list)
+{
+	struct net *net;
+
+	list_for_each_entry(net, net_list, exit_list) {
+		struct pm_fm_pernet *pernet = net_generic(net, pm_fm_pernet_id);
+	}
+}
+
+static struct pernet_operations mptcp_pm_fm_pernet_ops = {
+	.init = pm_fm_init_net,
+	.exit_batch = pm_fm_exit_net,
+	.id = &pm_fm_pernet_id,
+	.size = sizeof(struct pm_fm_pernet),
+};
+
+void __init mptcp_pm_fm_init(void)
+{
+	int ret;
+
+	if (register_pernet_subsys(&mptcp_pm_fm_pernet_ops) < 0)
+		panic("Failed to register MPTCP PM fullmesh pernet subsystem.\n");
+
+	ret = register_inetaddr_notifier(&mptcp_pm_addr_notifier);
+	if (ret)
+		return;
+
+	ret = register_inet6addr_notifier(&mptcp_pm_addr6_notifier);
+	if (ret)
+		return;
+}
+
+void mptcp_pm_fm_data_init(struct mptcp_sock *msk)
+{
+	struct mptcp_pm_data *pm = &msk->pm;
+
+	WRITE_ONCE(pm->work_pending, true);
+	WRITE_ONCE(pm->accept_addr, true);
+	WRITE_ONCE(pm->accept_subflow, 8);
+}
diff --git a/net/mptcp/pm_netlink.c b/net/mptcp/pm_netlink.c
index d2591ebf01d9..e440c0c6b19e 100644
--- a/net/mptcp/pm_netlink.c
+++ b/net/mptcp/pm_netlink.c
@@ -109,8 +109,8 @@ static void local_address(const struct sock_common *skc,
 #endif
 }
 
-static void remote_address(const struct sock_common *skc,
-			   struct mptcp_addr_info *addr)
+void remote_address(const struct sock_common *skc,
+		    struct mptcp_addr_info *addr)
 {
 	addr->family = skc->skc_family;
 	addr->port = skc->skc_dport;
@@ -122,8 +122,8 @@ static void remote_address(const struct sock_common *skc,
 #endif
 }
 
-static bool lookup_subflow_by_saddr(const struct list_head *list,
-				    struct mptcp_addr_info *saddr)
+bool lookup_subflow_by_saddr(const struct list_head *list,
+			     struct mptcp_addr_info *saddr)
 {
 	struct mptcp_subflow_context *subflow;
 	struct mptcp_addr_info cur;
@@ -255,7 +255,7 @@ unsigned int mptcp_pm_get_local_addr_max(struct mptcp_sock *msk)
 }
 EXPORT_SYMBOL_GPL(mptcp_pm_get_local_addr_max);
 
-static void check_work_pending(struct mptcp_sock *msk)
+void check_work_pending(struct mptcp_sock *msk)
 {
 	if (msk->pm.add_addr_signaled == mptcp_pm_get_add_addr_signal_max(msk) &&
 	    (msk->pm.local_addr_used == mptcp_pm_get_local_addr_max(msk) ||
diff --git a/net/mptcp/protocol.c b/net/mptcp/protocol.c
index 199a36fe4f69..4fae692f0ec2 100644
--- a/net/mptcp/protocol.c
+++ b/net/mptcp/protocol.c
@@ -2335,8 +2335,11 @@ static void mptcp_worker(struct work_struct *work)
 
 	mptcp_check_fastclose(msk);
 
-	if (msk->pm.status)
-		mptcp_pm_nl_work(msk);
+	pr_debug("%s msk->pm.status=%u", __func__, msk->pm.status);
+	if (msk->pm.status) {
+		//mptcp_pm_nl_work(msk);
+		mptcp_pm_fm_work(msk);
+	}
 
 	if (test_and_clear_bit(MPTCP_WORK_EOF, &msk->flags))
 		mptcp_check_for_eof(msk);
diff --git a/net/mptcp/protocol.h b/net/mptcp/protocol.h
index 426ed80fe72f..9f0c63e3a9a0 100644
--- a/net/mptcp/protocol.h
+++ b/net/mptcp/protocol.h
@@ -784,6 +784,9 @@ unsigned int mptcp_pm_get_add_addr_signal_max(struct mptcp_sock *msk);
 unsigned int mptcp_pm_get_add_addr_accept_max(struct mptcp_sock *msk);
 unsigned int mptcp_pm_get_subflows_max(struct mptcp_sock *msk);
 unsigned int mptcp_pm_get_local_addr_max(struct mptcp_sock *msk);
+void __init mptcp_pm_fm_init(void);
+void mptcp_pm_fm_work(struct mptcp_sock *msk);
+void mptcp_pm_fm_data_init(struct mptcp_sock *msk);
 
 void mptcp_sockopt_sync(struct mptcp_sock *msk, struct sock *ssk);
 void mptcp_sockopt_sync_all(struct mptcp_sock *msk);
diff --git a/net/mptcp/subflow.c b/net/mptcp/subflow.c
index 966f777d35ce..927b7fb71263 100644
--- a/net/mptcp/subflow.c
+++ b/net/mptcp/subflow.c
@@ -1404,7 +1404,7 @@ int __mptcp_subflow_connect(struct sock *sk, const struct mptcp_addr_info *loc,
 		goto failed;
 
 	mptcp_crypto_key_sha(subflow->remote_key, &remote_token, NULL);
-	pr_debug("msk=%p remote_token=%u local_id=%d remote_id=%d", msk,
+	pr_debug("%s msk=%p remote_token=%u local_id=%d remote_id=%d", __func__, msk,
 		 remote_token, local_id, remote_id);
 	subflow->remote_token = remote_token;
 	subflow->local_id = local_id;
diff --git a/tools/testing/selftests/net/mptcp/mptcp_join.sh b/tools/testing/selftests/net/mptcp/mptcp_join.sh
index f02f4de2f3a0..2465f6e796c8 100755
--- a/tools/testing/selftests/net/mptcp/mptcp_join.sh
+++ b/tools/testing/selftests/net/mptcp/mptcp_join.sh
@@ -803,6 +803,17 @@ chk_prio_nr()
 
 subflows_tests()
 {
+	# subflow
+	reset
+	ip netns exec $ns1 ./pm_nl_ctl limits 8 8
+	ip netns exec $ns2 ./pm_nl_ctl limits 8 8
+	run_tests $ns1 $ns2 10.0.1.1 0 0 0 slow
+	chk_join_nr "single subflow" 3 3 3
+	ip netns exec $ns1 cat /proc/net/mptcp_fullmesh
+	ip netns exec $ns2 cat /proc/net/mptcp_fullmesh
+
+	exit
+
 	reset
 	run_tests $ns1 $ns2 10.0.1.1
 	chk_join_nr "no JOIN" "0" "0" "0"
-- 
2.31.1

