From 9e6ae5b00b3efa74b0f5126beb1c091c85e17f09 Mon Sep 17 00:00:00 2001
From: Geliang Tang <geliangtang@gmail.com>
Date: Tue, 14 Jun 2016 11:34:13 +0800
Subject: [PATCH] zram: update zram to use zpool

Change zram to use the zpool api instead of directly using zsmalloc.

Signed-off-by: Geliang Tang <geliangtang@gmail.com>
---
 drivers/block/zram/Kconfig    |  1 +
 drivers/block/zram/zram_drv.c | 57 +++++++++++++++++++++++--------------------
 drivers/block/zram/zram_drv.h |  4 +--
 include/linux/zpool.h         |  8 +++---
 mm/z3fold.c                   |  6 ++---
 mm/zbud.c                     |  6 ++---
 mm/zpool.c                    |  4 +--
 mm/zsmalloc.c                 | 12 ++++++---
 8 files changed, 54 insertions(+), 44 deletions(-)

diff --git a/drivers/block/zram/Kconfig b/drivers/block/zram/Kconfig
index b8ecba6..7de2b02 100644
--- a/drivers/block/zram/Kconfig
+++ b/drivers/block/zram/Kconfig
@@ -2,6 +2,7 @@ config ZRAM
 	tristate "Compressed RAM block device support"
 	depends on BLOCK && SYSFS && ZSMALLOC && CRYPTO
 	select CRYPTO_LZO
+	select ZPOOL
 	default n
 	help
 	  Creates virtual block devices called /dev/zramX (X = 0, 1, ...).
diff --git a/drivers/block/zram/zram_drv.c b/drivers/block/zram/zram_drv.c
index 7454cf1..6ccfec2 100644
--- a/drivers/block/zram/zram_drv.c
+++ b/drivers/block/zram/zram_drv.c
@@ -39,6 +39,8 @@ static DEFINE_MUTEX(zram_index_mutex);
 
 static int zram_major;
 static const char *default_compressor = "lzo";
+static char *default_zpool_type = "zsmalloc";
+static unsigned long pages_compacted;
 
 /* Module params (documentation at end) */
 static unsigned int num_devices = 1;
@@ -228,11 +230,11 @@ static ssize_t mem_used_total_show(struct device *dev,
 	down_read(&zram->init_lock);
 	if (init_done(zram)) {
 		struct zram_meta *meta = zram->meta;
-		val = zs_get_total_pages(meta->mem_pool);
+		val = zpool_get_total_size(meta->mem_pool);
 	}
 	up_read(&zram->init_lock);
 
-	return scnprintf(buf, PAGE_SIZE, "%llu\n", val << PAGE_SHIFT);
+	return scnprintf(buf, PAGE_SIZE, "%llu\n", val);
 }
 
 static ssize_t mem_limit_show(struct device *dev,
@@ -297,7 +299,7 @@ static ssize_t mem_used_max_store(struct device *dev,
 	if (init_done(zram)) {
 		struct zram_meta *meta = zram->meta;
 		atomic_long_set(&zram->stats.max_used_pages,
-				zs_get_total_pages(meta->mem_pool));
+			zpool_get_total_size(meta->mem_pool) >> PAGE_SHIFT);
 	}
 	up_read(&zram->init_lock);
 
@@ -379,7 +381,7 @@ static ssize_t compact_store(struct device *dev,
 	}
 
 	meta = zram->meta;
-	zs_compact(meta->mem_pool);
+	zpool_shrink(meta->mem_pool, 0, &pages_compacted);
 	up_read(&zram->init_lock);
 
 	return len;
@@ -407,18 +409,13 @@ static ssize_t mm_stat_show(struct device *dev,
 		struct device_attribute *attr, char *buf)
 {
 	struct zram *zram = dev_to_zram(dev);
-	struct zs_pool_stats pool_stats;
 	u64 orig_size, mem_used = 0;
 	long max_used;
 	ssize_t ret;
 
-	memset(&pool_stats, 0x00, sizeof(struct zs_pool_stats));
-
 	down_read(&zram->init_lock);
-	if (init_done(zram)) {
-		mem_used = zs_get_total_pages(zram->meta->mem_pool);
-		zs_pool_stats(zram->meta->mem_pool, &pool_stats);
-	}
+	if (init_done(zram))
+		mem_used = zpool_get_total_size(zram->meta->mem_pool);
 
 	orig_size = atomic64_read(&zram->stats.pages_stored);
 	max_used = atomic_long_read(&zram->stats.max_used_pages);
@@ -427,11 +424,11 @@ static ssize_t mm_stat_show(struct device *dev,
 			"%8llu %8llu %8llu %8lu %8ld %8llu %8lu\n",
 			orig_size << PAGE_SHIFT,
 			(u64)atomic64_read(&zram->stats.compr_data_size),
-			mem_used << PAGE_SHIFT,
+			mem_used,
 			zram->limit_pages << PAGE_SHIFT,
 			max_used << PAGE_SHIFT,
 			(u64)atomic64_read(&zram->stats.zero_pages),
-			pool_stats.pages_compacted);
+			pages_compacted);
 	up_read(&zram->init_lock);
 
 	return ret;
@@ -490,10 +487,10 @@ static void zram_meta_free(struct zram_meta *meta, u64 disksize)
 		if (!handle)
 			continue;
 
-		zs_free(meta->mem_pool, handle);
+		zpool_free(meta->mem_pool, handle);
 	}
 
-	zs_destroy_pool(meta->mem_pool);
+	zpool_destroy_pool(meta->mem_pool);
 	vfree(meta->table);
 	kfree(meta);
 }
@@ -513,7 +510,13 @@ static struct zram_meta *zram_meta_alloc(char *pool_name, u64 disksize)
 		goto out_error;
 	}
 
-	meta->mem_pool = zs_create_pool(pool_name);
+	if (!zpool_has_pool(default_zpool_type)) {
+		pr_err("zpool %s not available\n", default_zpool_type);
+		goto out_error;
+	}
+
+	meta->mem_pool = zpool_create_pool(default_zpool_type,
+					   pool_name, 0, NULL);
 	if (!meta->mem_pool) {
 		pr_err("Error creating memory pool\n");
 		goto out_error;
@@ -549,7 +552,7 @@ static void zram_free_page(struct zram *zram, size_t index)
 		return;
 	}
 
-	zs_free(meta->mem_pool, handle);
+	zpool_free(meta->mem_pool, handle);
 
 	atomic64_sub(zram_get_obj_size(meta, index),
 			&zram->stats.compr_data_size);
@@ -577,7 +580,7 @@ static int zram_decompress_page(struct zram *zram, char *mem, u32 index)
 		return 0;
 	}
 
-	cmem = zs_map_object(meta->mem_pool, handle, ZS_MM_RO);
+	cmem = zpool_map_handle(meta->mem_pool, handle, ZPOOL_MM_RO);
 	if (size == PAGE_SIZE) {
 		copy_page(mem, cmem);
 	} else {
@@ -586,7 +589,7 @@ static int zram_decompress_page(struct zram *zram, char *mem, u32 index)
 		ret = zcomp_decompress(zstrm, cmem, size, mem);
 		zcomp_stream_put(zram->comp);
 	}
-	zs_unmap_object(meta->mem_pool, handle);
+	zpool_unmap_handle(meta->mem_pool, handle);
 	bit_spin_unlock(ZRAM_ACCESS, &meta->table[index].value);
 
 	/* Should NEVER happen. Return bio error if it does. */
@@ -735,20 +738,20 @@ compress_again:
 	 * from the slow path and handle has already been allocated.
 	 */
 	if (!handle)
-		handle = zs_malloc(meta->mem_pool, clen,
+		ret = zpool_malloc(meta->mem_pool, clen,
 				__GFP_KSWAPD_RECLAIM |
 				__GFP_NOWARN |
 				__GFP_HIGHMEM |
-				__GFP_MOVABLE);
+				__GFP_MOVABLE, &handle);
 	if (!handle) {
 		zcomp_stream_put(zram->comp);
 		zstrm = NULL;
 
 		atomic64_inc(&zram->stats.writestall);
 
-		handle = zs_malloc(meta->mem_pool, clen,
+		ret = zpool_malloc(meta->mem_pool, clen,
 				GFP_NOIO | __GFP_HIGHMEM |
-				__GFP_MOVABLE);
+				__GFP_MOVABLE, &handle);
 		if (handle)
 			goto compress_again;
 
@@ -758,16 +761,16 @@ compress_again:
 		goto out;
 	}
 
-	alloced_pages = zs_get_total_pages(meta->mem_pool);
+	alloced_pages = zpool_get_total_size(meta->mem_pool) >> PAGE_SHIFT;
 	update_used_max(zram, alloced_pages);
 
 	if (zram->limit_pages && alloced_pages > zram->limit_pages) {
-		zs_free(meta->mem_pool, handle);
+		zpool_free(meta->mem_pool, handle);
 		ret = -ENOMEM;
 		goto out;
 	}
 
-	cmem = zs_map_object(meta->mem_pool, handle, ZS_MM_WO);
+	cmem = zpool_map_handle(meta->mem_pool, handle, ZPOOL_MM_WO);
 
 	if ((clen == PAGE_SIZE) && !is_partial_io(bvec)) {
 		src = kmap_atomic(page);
@@ -779,7 +782,7 @@ compress_again:
 
 	zcomp_stream_put(zram->comp);
 	zstrm = NULL;
-	zs_unmap_object(meta->mem_pool, handle);
+	zpool_unmap_handle(meta->mem_pool, handle);
 
 	/*
 	 * Free memory associated with this sector
diff --git a/drivers/block/zram/zram_drv.h b/drivers/block/zram/zram_drv.h
index 74fcf10..de3e013 100644
--- a/drivers/block/zram/zram_drv.h
+++ b/drivers/block/zram/zram_drv.h
@@ -16,7 +16,7 @@
 #define _ZRAM_DRV_H_
 
 #include <linux/rwsem.h>
-#include <linux/zsmalloc.h>
+#include <linux/zpool.h>
 #include <linux/crypto.h>
 
 #include "zcomp.h"
@@ -91,7 +91,7 @@ struct zram_stats {
 
 struct zram_meta {
 	struct zram_table_entry *table;
-	struct zs_pool *mem_pool;
+	struct zpool *mem_pool;
 };
 
 struct zram {
diff --git a/include/linux/zpool.h b/include/linux/zpool.h
index 2e97b77..8a9b343 100644
--- a/include/linux/zpool.h
+++ b/include/linux/zpool.h
@@ -50,8 +50,8 @@ int zpool_malloc(struct zpool *pool, size_t size, gfp_t gfp,
 
 void zpool_free(struct zpool *pool, unsigned long handle);
 
-int zpool_shrink(struct zpool *pool, unsigned int pages,
-			unsigned int *reclaimed);
+int zpool_shrink(struct zpool *pool, unsigned long pages,
+			unsigned long *reclaimed);
 
 void *zpool_map_handle(struct zpool *pool, unsigned long handle,
 			enum zpool_mapmode mm);
@@ -93,8 +93,8 @@ struct zpool_driver {
 				unsigned long *handle);
 	void (*free)(void *pool, unsigned long handle);
 
-	int (*shrink)(void *pool, unsigned int pages,
-				unsigned int *reclaimed);
+	int (*shrink)(void *pool, unsigned long pages,
+				unsigned long *reclaimed);
 
 	void *(*map)(void *pool, unsigned long handle,
 				enum zpool_mapmode mm);
diff --git a/mm/z3fold.c b/mm/z3fold.c
index 8f9e89c..cc7d9ce 100644
--- a/mm/z3fold.c
+++ b/mm/z3fold.c
@@ -725,10 +725,10 @@ static void z3fold_zpool_free(void *pool, unsigned long handle)
 	z3fold_free(pool, handle);
 }
 
-static int z3fold_zpool_shrink(void *pool, unsigned int pages,
-			unsigned int *reclaimed)
+static int z3fold_zpool_shrink(void *pool, unsigned long pages,
+			unsigned long *reclaimed)
 {
-	unsigned int total = 0;
+	unsigned long total = 0;
 	int ret = -EINVAL;
 
 	while (total < pages) {
diff --git a/mm/zbud.c b/mm/zbud.c
index b42322e..fde96dd 100644
--- a/mm/zbud.c
+++ b/mm/zbud.c
@@ -166,10 +166,10 @@ static void zbud_zpool_free(void *pool, unsigned long handle)
 	zbud_free(pool, handle);
 }
 
-static int zbud_zpool_shrink(void *pool, unsigned int pages,
-			unsigned int *reclaimed)
+static int zbud_zpool_shrink(void *pool, unsigned long pages,
+			unsigned long *reclaimed)
 {
-	unsigned int total = 0;
+	unsigned long total = 0;
 	int ret = -EINVAL;
 
 	while (total < pages) {
diff --git a/mm/zpool.c b/mm/zpool.c
index fd3ff71..1d6832c 100644
--- a/mm/zpool.c
+++ b/mm/zpool.c
@@ -293,8 +293,8 @@ void zpool_free(struct zpool *zpool, unsigned long handle)
  *
  * Returns: 0 on success, negative value on error/failure.
  */
-int zpool_shrink(struct zpool *zpool, unsigned int pages,
-			unsigned int *reclaimed)
+int zpool_shrink(struct zpool *zpool, unsigned long pages,
+			unsigned long *reclaimed)
 {
 	return zpool->driver->shrink(zpool->pool, pages, reclaimed);
 }
diff --git a/mm/zsmalloc.c b/mm/zsmalloc.c
index 6a58edc..5ff0f7c 100644
--- a/mm/zsmalloc.c
+++ b/mm/zsmalloc.c
@@ -418,10 +418,16 @@ static void zs_zpool_free(void *pool, unsigned long handle)
 	zs_free(pool, handle);
 }
 
-static int zs_zpool_shrink(void *pool, unsigned int pages,
-			unsigned int *reclaimed)
+static int zs_zpool_shrink(void *pool, unsigned long pages,
+			unsigned long *reclaimed)
 {
-	return -EINVAL;
+	unsigned long pages_compacted;
+
+	pages_compacted = zs_compact(pool);
+	if (reclaimed)
+		*reclaimed = pages_compacted;
+
+	return 0;
 }
 
 static void *zs_zpool_map(void *pool, unsigned long handle,
-- 
1.9.1

