From f210561e8ebf93ffdb4e8e76b0a9727dd770b4b9 Mon Sep 17 00:00:00 2001
From: Geliang Tang <geliangtang@gmail.com>
Date: Wed, 28 Aug 2019 17:00:30 +0800
Subject: [PATCH] erofs-utils: add lzo and zstd compressors

Signed-off-by: Geliang Tang <geliangtang@gmail.com>
---
 configure.ac          |  2 +-
 include/erofs_fs.h    |  2 ++
 lib/Makefile.am       |  2 ++
 lib/compress.c        |  4 +++
 lib/compressor.c      |  2 ++
 lib/compressor.h      |  2 ++
 lib/compressor_lz4.c  |  5 +++-
 lib/compressor_lzo.c  | 60 ++++++++++++++++++++++++++++++++++++++
 lib/compressor_zstd.c | 68 +++++++++++++++++++++++++++++++++++++++++++
 9 files changed, 145 insertions(+), 2 deletions(-)
 create mode 100644 lib/compressor_lzo.c
 create mode 100644 lib/compressor_zstd.c

diff --git a/configure.ac b/configure.ac
index fcdf30a..bc01dfa 100644
--- a/configure.ac
+++ b/configure.ac
@@ -121,7 +121,7 @@ AC_CHECK_DECL(lseek64,[AC_DEFINE(HAVE_LSEEK64_PROTOTYPE, 1,
 AC_CHECK_FUNCS([fallocate gettimeofday memset realpath strdup strerror strrchr strtoull])
 
 # Configure lz4
-test -z $LZ4_LIBS && LZ4_LIBS='-llz4'
+test -z $LZ4_LIBS && LZ4_LIBS='-llz4 -llzo2 -lzstd'
 
 if test "x$enable_lz4" = "xyes"; then
   test -z "${with_lz4_incdir}" || LZ4_CFLAGS="-I$with_lz4_incdir $LZ4_CFLAGS"
diff --git a/include/erofs_fs.h b/include/erofs_fs.h
index 601b477..fb56c69 100644
--- a/include/erofs_fs.h
+++ b/include/erofs_fs.h
@@ -185,6 +185,8 @@ struct erofs_xattr_entry {
 /* available compression algorithm types */
 enum {
 	Z_EROFS_COMPRESSION_LZ4,
+	Z_EROFS_COMPRESSION_LZO,
+	Z_EROFS_COMPRESSION_ZSTD,
 	Z_EROFS_COMPRESSION_MAX
 };
 
diff --git a/lib/Makefile.am b/lib/Makefile.am
index dea82f7..61d0293 100644
--- a/lib/Makefile.am
+++ b/lib/Makefile.am
@@ -10,5 +10,7 @@ liberofs_la_SOURCES += compressor_lz4.c
 if ENABLE_LZ4HC
 liberofs_la_SOURCES += compressor_lz4hc.c
 endif
+liberofs_la_SOURCES += compressor_lzo.c
+liberofs_la_SOURCES += compressor_zstd.c
 endif
 
diff --git a/lib/compress.c b/lib/compress.c
index a977c87..d141c0f 100644
--- a/lib/compress.c
+++ b/lib/compress.c
@@ -493,6 +493,10 @@ static int erofs_get_compress_algorithm_id(const char *name)
 {
 	if (!strcmp(name, "lz4") || !strcmp(name, "lz4hc"))
 		return Z_EROFS_COMPRESSION_LZ4;
+	if (!strcmp(name, "lzo"))
+		return Z_EROFS_COMPRESSION_LZO;
+	if (!strcmp(name, "zstd"))
+		return Z_EROFS_COMPRESSION_ZSTD;
 	return -ENOTSUP;
 }
 
diff --git a/lib/compressor.c b/lib/compressor.c
index 8cc2f43..6127885 100644
--- a/lib/compressor.c
+++ b/lib/compressor.c
@@ -46,6 +46,8 @@ int erofs_compressor_init(struct erofs_compress *c,
 #endif
 		&erofs_compressor_lz4,
 #endif
+		&erofs_compressor_lzo,
+		&erofs_compressor_zstd,
 	};
 
 	int ret, i;
diff --git a/lib/compressor.h b/lib/compressor.h
index 6429b2a..c2dd5ae 100644
--- a/lib/compressor.h
+++ b/lib/compressor.h
@@ -42,6 +42,8 @@ struct erofs_compress {
 /* list of compression algorithms */
 extern struct erofs_compressor erofs_compressor_lz4;
 extern struct erofs_compressor erofs_compressor_lz4hc;
+extern struct erofs_compressor erofs_compressor_lzo;
+extern struct erofs_compressor erofs_compressor_zstd;
 
 int erofs_compress_destsize(struct erofs_compress *c, int compression_level,
 			    void *src, unsigned int *srcsize,
diff --git a/lib/compressor_lz4.c b/lib/compressor_lz4.c
index 0d33223..17e46b5 100644
--- a/lib/compressor_lz4.c
+++ b/lib/compressor_lz4.c
@@ -16,8 +16,11 @@ static int lz4_compress_destsize(struct erofs_compress *c,
 				 void *dst, unsigned int dstsize)
 {
 	int srcSize = (int)*srcsize;
-	int rc = LZ4_compress_destSize(src, dst, &srcSize, (int)dstsize);
+	int rc;
 
+	fprintf(stderr, "%s srcSize=%d, dstsize=%d\n", __func__, srcSize, dstsize);
+
+	rc = LZ4_compress_destSize(src, dst, &srcSize, (int)dstsize);
 	if (!rc)
 		return -EFAULT;
 	*srcsize = srcSize;
diff --git a/lib/compressor_lzo.c b/lib/compressor_lzo.c
new file mode 100644
index 0000000..de36d13
--- /dev/null
+++ b/lib/compressor_lzo.c
@@ -0,0 +1,60 @@
+// SPDX-License-Identifier: GPL-2.0+
+/*
+ * erofs-utils/lib/compressor-lzo.c
+ *
+ * Copyright (C) 2018-2019 HUAWEI, Inc.
+ *             http://www.huawei.com/
+ * Created by Geliang Tang <geliangtang@gmail.com>
+ */
+#include <lzo/lzo1x.h>
+#include "erofs/internal.h"
+#include "compressor.h"
+
+#define lzo1x_worst_compress(x) ((x) + ((x) / 16) + 64 + 3 + 2)
+
+static int lzo_compress_destsize(struct erofs_compress *c,
+				 int compression_level,
+				 void *src, unsigned int *srcsize,
+				 void *dst, unsigned int dstsize)
+{
+	lzo_align_t wrkmem[LZO1X_1_MEM_COMPRESS];
+	lzo_uint srcSize = *srcsize;
+	lzo_uint dstSize = lzo1x_worst_compress(dstsize);
+	int rc;
+	
+	fprintf(stderr, "%s srcSize=%ld, dstSize=%ld\n", __func__, srcSize, dstSize);
+
+	if (srcSize > dstSize)
+		srcSize = dstSize;
+
+	rc = lzo1x_1_compress(src, srcSize, dst, &dstSize, wrkmem);
+	if (rc != LZO_E_OK)
+		return -EFAULT;
+
+	*srcsize = srcSize;
+	return dstSize;
+}
+
+static int compressor_lzo_exit(struct erofs_compress *c)
+{
+	return 0;
+}
+
+static int compressor_lzo_init(struct erofs_compress *c,
+				 char *alg_name)
+{
+	if (alg_name && strcmp(alg_name, "lzo"))
+		return -EINVAL;
+	c->alg = &erofs_compressor_lzo;
+	lzo_init();
+	return 0;
+}
+
+struct erofs_compressor erofs_compressor_lzo = {
+	.default_level = 0,
+	.best_level = 0,
+	.init = compressor_lzo_init,
+	.exit = compressor_lzo_exit,
+	.compress_destsize = lzo_compress_destsize,
+};
+
diff --git a/lib/compressor_zstd.c b/lib/compressor_zstd.c
new file mode 100644
index 0000000..fac97f9
--- /dev/null
+++ b/lib/compressor_zstd.c
@@ -0,0 +1,68 @@
+// SPDX-License-Identifier: GPL-2.0+
+/*
+ * erofs-utils/lib/compressor-zstd.c
+ *
+ * Copyright (C) 2018-2019 HUAWEI, Inc.
+ *             http://www.huawei.com/
+ * Created by Geliang Tang <geliangtang@gmail.com>
+ */
+#include <zstd.h>
+#include "erofs/internal.h"
+#include "compressor.h"
+
+#include <stdio.h>
+#include <stdlib.h>
+
+static int zstd_compress_destsize(struct erofs_compress *c,
+				 int compression_level,
+				 void *src, unsigned int *srcsize,
+				 void *dst, unsigned int dstsize)
+{
+	unsigned srcSize = *srcsize;
+	unsigned dstSize = ZSTD_compressBound(dstsize);
+	size_t cSize;
+	ZSTD_CCtx *cctx = ZSTD_createCCtx();
+
+	if(!cctx) {
+		return -1;
+	}
+
+	fprintf(stderr, "%s1 srcSize=%d, dstsize=%d, dstSize=%d\n", __func__, srcSize, dstsize, dstSize);
+	if (srcSize > dstSize)
+		srcSize = dstSize;
+	fprintf(stderr, "%s2 srcSize=%d, dstsize=%d, dstSize=%d\n", __func__, srcSize, dstsize, dstSize);
+
+	cSize = ZSTD_compressCCtx(cctx, dst, dstSize, src, srcSize, 1);
+	if (ZSTD_isError(cSize)) {
+		fprintf(stderr, "ZSTD_compress error\n");
+		return -EFAULT;
+	}
+
+	fprintf(stderr, "%s3 cSize=%ld\n", __func__, cSize);
+	*srcsize = srcSize;
+
+	return cSize;
+}
+
+static int compressor_zstd_exit(struct erofs_compress *c)
+{
+	return 0;
+}
+
+static int compressor_zstd_init(struct erofs_compress *c,
+				 char *alg_name)
+{
+	if (alg_name && strcmp(alg_name, "zstd"))
+		return -EINVAL;
+	c->alg = &erofs_compressor_zstd;
+	return 0;
+}
+
+struct erofs_compressor erofs_compressor_zstd = {
+	.default_level = 0,
+	.best_level = 0,
+	.init = compressor_zstd_init,
+	.exit = compressor_zstd_exit,
+	.compress_destsize = zstd_compress_destsize,
+};
+
-- 
2.17.1

