From b6e183d57845dea737348ddcbdc70feac43776d9 Mon Sep 17 00:00:00 2001
From: Geliang Tang <geliangtang@gmail.com>
Date: Thu, 29 Oct 2020 19:57:30 +0800
Subject: [PATCH 1/3] mptcp: add MP_PRIO support v2

Add handling for sending and receiving MP_PRIO suboption.

Signed-off-by: Geliang Tang <geliangtang@gmail.com>
---
 include/net/mptcp.h    |  1 +
 net/mptcp/options.c    | 53 +++++++++++++++++++++++++++++++++-
 net/mptcp/pm.c         | 64 ++++++++++++++++++++++++++++++++++++++++++
 net/mptcp/pm_netlink.c |  2 ++
 net/mptcp/protocol.c   |  1 +
 net/mptcp/protocol.h   | 19 ++++++++++++-
 6 files changed, 138 insertions(+), 2 deletions(-)

diff --git a/include/net/mptcp.h b/include/net/mptcp.h
index b6cf07143a8a..ba3239a1c233 100644
--- a/include/net/mptcp.h
+++ b/include/net/mptcp.h
@@ -50,6 +50,7 @@ struct mptcp_out_options {
 	u8 rm_id;
 	u8 join_id;
 	u8 backup;
+	u8 prio;
 	u32 nonce;
 	u64 thmac;
 	u32 token;
diff --git a/net/mptcp/options.c b/net/mptcp/options.c
index 248e3930c0cb..40e117ea4c96 100644
--- a/net/mptcp/options.c
+++ b/net/mptcp/options.c
@@ -282,6 +282,15 @@ static void mptcp_parse_option(const struct sk_buff *skb,
 		pr_debug("RM_ADDR: id=%d", mp_opt->rm_id);
 		break;
 
+	case MPTCPOPT_MP_PRIO:
+		if (opsize != TCPOLEN_MPTCP_PRIO)
+			break;
+
+		mp_opt->prio = 1;
+		mp_opt->prio_backup = (*ptr++) & MPTCP_PRIO_FLAG;
+		pr_debug("MP_PRIO: prio=%d", mp_opt->prio_backup);
+		break;
+
 	default:
 		break;
 	}
@@ -302,6 +311,8 @@ void mptcp_get_options(const struct sk_buff *skb,
 	mp_opt->port = 0;
 	mp_opt->rm_addr = 0;
 	mp_opt->dss = 0;
+	mp_opt->prio = 0;
+	mp_opt->prio_backup = 0;
 
 	length = (th->doff * 4) - sizeof(struct tcphdr);
 	ptr = (const unsigned char *)(th + 1);
@@ -660,6 +671,31 @@ static bool mptcp_established_options_rm_addr(struct sock *sk,
 	return true;
 }
 
+static bool mptcp_established_options_prio(struct sock *sk,
+					   unsigned int *size,
+					   unsigned int remaining,
+					   struct mptcp_out_options *opts)
+{
+	struct mptcp_subflow_context *subflow = mptcp_subflow_ctx(sk);
+	struct mptcp_sock *msk = mptcp_sk(subflow->conn);
+	u8 prio;
+
+	if (!mptcp_pm_should_prio_signal(msk) ||
+	    !(mptcp_pm_prio_signal(msk, remaining, &prio)))
+		return false;
+
+	if (remaining < TCPOLEN_MPTCP_PRIO)
+		return false;
+
+	*size = TCPOLEN_MPTCP_PRIO;
+	opts->suboptions |= OPTION_MPTCP_PRIO;
+	opts->prio = prio;
+
+	pr_debug("prio=%d", opts->prio);
+
+	return true;
+}
+
 bool mptcp_established_options(struct sock *sk, struct sk_buff *skb,
 			       unsigned int *size, unsigned int remaining,
 			       struct mptcp_out_options *opts)
@@ -692,7 +728,11 @@ bool mptcp_established_options(struct sock *sk, struct sk_buff *skb,
 
 	*size += opt_size;
 	remaining -= opt_size;
-	if (mptcp_established_options_add_addr(sk, skb, &opt_size, remaining, opts)) {
+	if (mptcp_established_options_prio(sk, &opt_size, remaining, opts)) {
+		*size += opt_size;
+		remaining -= opt_size;
+		ret = true;
+	} else if (mptcp_established_options_add_addr(sk, skb, &opt_size, remaining, opts)) {
 		*size += opt_size;
 		remaining -= opt_size;
 		ret = true;
@@ -958,6 +998,11 @@ void mptcp_incoming_options(struct sock *sk, struct sk_buff *skb)
 		mp_opt.rm_addr = 0;
 	}
 
+	if (mp_opt.prio) {
+		subflow->backup = mp_opt.prio_backup;
+		mp_opt.prio = 0;
+	}
+
 	if (!mp_opt.dss)
 		return;
 
@@ -1110,6 +1155,12 @@ void mptcp_write_options(__be32 *ptr, const struct tcp_sock *tp,
 				      0, opts->rm_id);
 	}
 
+	if (OPTION_MPTCP_PRIO & opts->suboptions) {
+		*ptr++ = mptcp_option(MPTCPOPT_MP_PRIO,
+				      TCPOLEN_MPTCP_PRIO,
+				      opts->prio, TCPOPT_NOP);
+	}
+
 	if (OPTION_MPTCP_MPJ_SYN & opts->suboptions) {
 		*ptr++ = mptcp_option(MPTCPOPT_MP_JOIN,
 				      TCPOLEN_MPTCP_MPJ_SYN,
diff --git a/net/mptcp/pm.c b/net/mptcp/pm.c
index 75c5040e8d5d..399c223184bb 100644
--- a/net/mptcp/pm.c
+++ b/net/mptcp/pm.c
@@ -49,6 +49,45 @@ int mptcp_pm_remove_subflow(struct mptcp_sock *msk, u8 local_id)
 	return 0;
 }
 
+static void mptcp_pm_prio_send_ack(struct mptcp_sock *msk, u8 remote_id)
+{
+	struct mptcp_subflow_context *subflow, *tmp;
+
+	if (!remote_id)
+		return;
+
+	if (list_empty(&msk->conn_list))
+		return;
+
+	list_for_each_entry_safe(subflow, tmp, &msk->conn_list, node) {
+		struct sock *ssk = mptcp_subflow_tcp_sock(subflow);
+
+		if (remote_id != subflow->remote_id)
+			continue;
+
+		spin_unlock_bh(&msk->pm.lock);
+		pr_debug("send ack for prio");
+		lock_sock(ssk);
+		tcp_send_ack(ssk);
+		release_sock(ssk);
+		spin_lock_bh(&msk->pm.lock);
+
+		break;
+	}
+}
+
+int mptcp_pm_prio(struct mptcp_sock *msk, u8 remote_id, u8 prio)
+{
+	pr_debug("msk=%p, prio=%d", msk, prio);
+
+	msk->pm.prio = prio;
+	WRITE_ONCE(msk->pm.prio_signal, true);
+
+	mptcp_pm_prio_send_ack(msk, remote_id);
+
+	return 0;
+}
+
 /* path manager event handlers */
 
 void mptcp_pm_new_connection(struct mptcp_sock *msk, int server_side)
@@ -235,6 +274,29 @@ bool mptcp_pm_rm_addr_signal(struct mptcp_sock *msk, unsigned int remaining,
 	return ret;
 }
 
+bool mptcp_pm_prio_signal(struct mptcp_sock *msk, unsigned int remaining,
+			  u8 *prio)
+{
+	int ret = false;
+
+	spin_lock_bh(&msk->pm.lock);
+
+	/* double check after the lock is acquired */
+	if (!mptcp_pm_should_prio_signal(msk))
+		goto out_unlock;
+
+	if (remaining < TCPOLEN_MPTCP_PRIO)
+		goto out_unlock;
+
+	*prio = msk->pm.prio;
+	WRITE_ONCE(msk->pm.prio_signal, false);
+	ret = true;
+
+out_unlock:
+	spin_unlock_bh(&msk->pm.lock);
+	return ret;
+}
+
 int mptcp_pm_get_local_id(struct mptcp_sock *msk, struct sock_common *skc)
 {
 	return mptcp_pm_nl_get_local_id(msk, skc);
@@ -247,9 +309,11 @@ void mptcp_pm_data_init(struct mptcp_sock *msk)
 	msk->pm.local_addr_used = 0;
 	msk->pm.subflows = 0;
 	msk->pm.rm_id = 0;
+	msk->pm.prio = 0;
 	WRITE_ONCE(msk->pm.work_pending, false);
 	WRITE_ONCE(msk->pm.add_addr_signal, 0);
 	WRITE_ONCE(msk->pm.rm_addr_signal, false);
+	WRITE_ONCE(msk->pm.prio_signal, false);
 	WRITE_ONCE(msk->pm.accept_addr, false);
 	WRITE_ONCE(msk->pm.accept_subflow, false);
 	msk->pm.status = 0;
diff --git a/net/mptcp/pm_netlink.c b/net/mptcp/pm_netlink.c
index 6180a8b39a3f..a04c2287d149 100644
--- a/net/mptcp/pm_netlink.c
+++ b/net/mptcp/pm_netlink.c
@@ -450,6 +450,7 @@ void mptcp_pm_nl_rm_addr_received(struct mptcp_sock *msk)
 		if (msk->pm.rm_id != subflow->remote_id)
 			continue;
 
+		pr_debug("%s subflow->backup=%d", __func__, subflow->backup);
 		spin_unlock_bh(&msk->pm.lock);
 		mptcp_subflow_shutdown(sk, ssk, how);
 		__mptcp_close_ssk(sk, ssk, subflow);
@@ -786,6 +787,7 @@ static bool mptcp_pm_remove_anno_addr(struct mptcp_sock *msk,
 	ret = remove_anno_list_by_saddr(msk, addr);
 	if (ret || force) {
 		spin_lock_bh(&msk->pm.lock);
+		mptcp_pm_prio(msk, addr->id, 1);
 		mptcp_pm_remove_addr(msk, addr->id);
 		spin_unlock_bh(&msk->pm.lock);
 	}
diff --git a/net/mptcp/protocol.c b/net/mptcp/protocol.c
index a6bd06c724d5..dcfac9c2e3f3 100644
--- a/net/mptcp/protocol.c
+++ b/net/mptcp/protocol.c
@@ -1147,6 +1147,7 @@ static struct sock *mptcp_subflow_get_send(struct mptcp_sock *msk,
 		send_info[i].ratio = -1;
 	}
 	mptcp_for_each_subflow(msk, subflow) {
+		pr_debug("%s subflow->backup=%d", __func__, subflow->backup);
 		ssk =  mptcp_subflow_tcp_sock(subflow);
 		if (!mptcp_subflow_active(subflow))
 			continue;
diff --git a/net/mptcp/protocol.h b/net/mptcp/protocol.h
index d29c6a4749eb..bf4897bf9639 100644
--- a/net/mptcp/protocol.h
+++ b/net/mptcp/protocol.h
@@ -23,6 +23,7 @@
 #define OPTION_MPTCP_ADD_ADDR	BIT(6)
 #define OPTION_MPTCP_ADD_ADDR6	BIT(7)
 #define OPTION_MPTCP_RM_ADDR	BIT(8)
+#define OPTION_MPTCP_PRIO	BIT(9)
 
 /* MPTCP option subtypes */
 #define MPTCPOPT_MP_CAPABLE	0
@@ -58,6 +59,7 @@
 #define TCPOLEN_MPTCP_ADD_ADDR6_BASE_PORT	22
 #define TCPOLEN_MPTCP_PORT_LEN		2
 #define TCPOLEN_MPTCP_RM_ADDR_BASE	4
+#define TCPOLEN_MPTCP_PRIO		4
 
 /* MPTCP MP_JOIN flags */
 #define MPTCPOPT_BACKUP		BIT(0)
@@ -84,6 +86,9 @@
 #define MPTCP_ADDR_IPVERSION_4	4
 #define MPTCP_ADDR_IPVERSION_6	6
 
+/* MPTCP MP_PRIO flags */
+#define MPTCP_PRIO_FLAG		BIT(0)
+
 /* MPTCP socket flags */
 #define MPTCP_DATA_READY	0
 #define MPTCP_NOSPACE		1
@@ -114,7 +119,8 @@ struct mptcp_options_received {
 		rm_addr : 1,
 		family : 4,
 		echo : 1,
-		backup : 1;
+		backup : 1,
+		prio : 1;
 	u32	token;
 	u32	nonce;
 	u64	thmac;
@@ -137,6 +143,7 @@ struct mptcp_options_received {
 	};
 	u64	ahmac;
 	u16	port;
+	u8	prio_backup;
 };
 
 static inline __be32 mptcp_option(u8 subopt, u8 len, u8 nib, u8 field)
@@ -182,6 +189,7 @@ struct mptcp_pm_data {
 
 	u8		add_addr_signal;
 	bool		rm_addr_signal;
+	bool		prio_signal;
 	bool		server_side;
 	bool		work_pending;
 	bool		accept_addr;
@@ -196,6 +204,7 @@ struct mptcp_pm_data {
 	u8		subflows_max;
 	u8		status;
 	u8		rm_id;
+	u8		prio;
 };
 
 struct mptcp_data_frag {
@@ -523,6 +532,9 @@ int mptcp_pm_announce_addr(struct mptcp_sock *msk,
 			   bool echo);
 int mptcp_pm_remove_addr(struct mptcp_sock *msk, u8 local_id);
 int mptcp_pm_remove_subflow(struct mptcp_sock *msk, u8 local_id);
+bool mptcp_pm_prio_signal(struct mptcp_sock *msk, unsigned int remaining,
+			  u8 *prio);
+int mptcp_pm_prio(struct mptcp_sock *msk, u8 remote_id, u8 prio);
 
 static inline bool mptcp_pm_should_add_signal(struct mptcp_sock *msk)
 {
@@ -544,6 +556,11 @@ static inline bool mptcp_pm_should_rm_signal(struct mptcp_sock *msk)
 	return READ_ONCE(msk->pm.rm_addr_signal);
 }
 
+static inline bool mptcp_pm_should_prio_signal(struct mptcp_sock *msk)
+{
+	return READ_ONCE(msk->pm.prio_signal);
+}
+
 static inline unsigned int mptcp_add_addr_len(int family, bool echo)
 {
 	if (family == AF_INET)
-- 
2.26.2

