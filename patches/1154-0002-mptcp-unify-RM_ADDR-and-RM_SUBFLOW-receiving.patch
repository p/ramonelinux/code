From 06d4bf11abca96d8b060d8ffec7923c54ece2654 Mon Sep 17 00:00:00 2001
Message-Id: <06d4bf11abca96d8b060d8ffec7923c54ece2654.1614757087.git.geliangtang@gmail.com>
In-Reply-To: <751c42fd164f37ff6f7af4f009285c7c7a920470.1614757087.git.geliangtang@gmail.com>
References: <cover.1614757087.git.geliangtang@gmail.com>
	<751c42fd164f37ff6f7af4f009285c7c7a920470.1614757087.git.geliangtang@gmail.com>
From: Geliang Tang <geliangtang@gmail.com>
To: mptcp@lists.01.org
Date: Fri, 19 Feb 2021 16:07:13 +0800
Subject: [MPTCP][PATCH v4 mptcp-next 2/4] mptcp: unify RM_ADDR and RM_SUBFLOW
 receiving

There are some duplicate code in mptcp_pm_nl_rm_addr_received and
mptcp_pm_nl_rm_subflow_received. This patch unifies them into a new
function named mptcp_pm_nl_rm_addr_or_subflow. In it, use the input
parameter type to identify it's now removing an address or a subflow.

Signed-off-by: Geliang Tang <geliangtang@gmail.com>
---
 net/mptcp/pm_netlink.c | 88 +++++++++++++++++++-----------------------
 1 file changed, 40 insertions(+), 48 deletions(-)

diff --git a/net/mptcp/pm_netlink.c b/net/mptcp/pm_netlink.c
index 1cad39edde31..419b643bd6fe 100644
--- a/net/mptcp/pm_netlink.c
+++ b/net/mptcp/pm_netlink.c
@@ -594,45 +594,75 @@ int mptcp_pm_nl_mp_prio_send_ack(struct mptcp_sock *msk,
 	return -EINVAL;
 }
 
-static void mptcp_pm_nl_rm_addr_received(struct mptcp_sock *msk)
+enum rm_type {
+	RM_ADDR,
+	RM_SUBFLOW,
+};
+
+static void mptcp_pm_nl_rm_addr_or_subflow(struct mptcp_sock *msk,
+					   struct mptcp_rm_list rm_list,
+					   enum rm_type type)
 {
 	struct mptcp_subflow_context *subflow, *tmp;
 	struct sock *sk = (struct sock *)msk;
 	u8 i;
 
-	pr_debug("address rm_list_nr %d", msk->pm.rm_list_rx.nr);
+	pr_debug("%s rm_list_nr %d", type == RM_ADDR ? "address" : "subflow", rm_list.nr);
 
 	msk_owned_by_me(msk);
 
-	if (!msk->pm.rm_list_rx.nr)
+	if (!rm_list.nr)
 		return;
 
 	if (list_empty(&msk->conn_list))
 		return;
 
-	for (i = 0; i < msk->pm.rm_list_rx.nr; i++) {
+	for (i = 0; i < rm_list.nr; i++) {
 		list_for_each_entry_safe(subflow, tmp, &msk->conn_list, node) {
 			struct sock *ssk = mptcp_subflow_tcp_sock(subflow);
 			int how = RCV_SHUTDOWN | SEND_SHUTDOWN;
+			u8 id = subflow->local_id;
+
+			if (type == RM_ADDR)
+				id = subflow->remote_id;
 
-			if (msk->pm.rm_list_rx.ids[i] != subflow->remote_id)
+			if (rm_list.ids[i] != id)
 				continue;
 
-			pr_debug(" -> address rm_list_ids[%d]=%u", i, msk->pm.rm_list_rx.ids[i]);
+			pr_debug(" -> %s rm_list_ids[%d]=%u local_id=%u remote_id=%u",
+				 type == RM_ADDR ? "address" : "subflow", i, rm_list.ids[i],
+				 subflow->local_id, subflow->remote_id);
 			spin_unlock_bh(&msk->pm.lock);
 			mptcp_subflow_shutdown(sk, ssk, how);
 			mptcp_close_ssk(sk, ssk, subflow);
 			spin_lock_bh(&msk->pm.lock);
 
-			msk->pm.add_addr_accepted--;
-			msk->pm.subflows--;
-			WRITE_ONCE(msk->pm.accept_addr, true);
+			if (type == RM_ADDR) {
+				msk->pm.add_addr_accepted--;
+				msk->pm.subflows--;
+				WRITE_ONCE(msk->pm.accept_addr, true);
 
-			__MPTCP_INC_STATS(sock_net(sk), MPTCP_MIB_RMADDR);
+				__MPTCP_INC_STATS(sock_net(sk), MPTCP_MIB_RMADDR);
+			} else if (type == RM_SUBFLOW) {
+				msk->pm.local_addr_used--;
+				msk->pm.subflows--;
+
+				__MPTCP_INC_STATS(sock_net(sk), MPTCP_MIB_RMSUBFLOW);
+			}
 		}
 	}
 }
 
+static void mptcp_pm_nl_rm_addr_received(struct mptcp_sock *msk)
+{
+	mptcp_pm_nl_rm_addr_or_subflow(msk, msk->pm.rm_list_rx, RM_ADDR);
+}
+
+void mptcp_pm_nl_rm_subflow_received(struct mptcp_sock *msk, struct mptcp_rm_list rm_list)
+{
+	mptcp_pm_nl_rm_addr_or_subflow(msk, rm_list, RM_SUBFLOW);
+}
+
 void mptcp_pm_nl_work(struct mptcp_sock *msk)
 {
 	struct mptcp_pm_data *pm = &msk->pm;
@@ -666,44 +696,6 @@ void mptcp_pm_nl_work(struct mptcp_sock *msk)
 	spin_unlock_bh(&msk->pm.lock);
 }
 
-void mptcp_pm_nl_rm_subflow_received(struct mptcp_sock *msk, struct mptcp_rm_list rm_list)
-{
-	struct mptcp_subflow_context *subflow, *tmp;
-	struct sock *sk = (struct sock *)msk;
-	u8 i;
-
-	pr_debug("subflow rm_list_nr %d", rm_list.nr);
-
-	msk_owned_by_me(msk);
-
-	if (!rm_list.nr)
-		return;
-
-	if (list_empty(&msk->conn_list))
-		return;
-
-	for (i = 0; i < rm_list.nr; i++) {
-		list_for_each_entry_safe(subflow, tmp, &msk->conn_list, node) {
-			struct sock *ssk = mptcp_subflow_tcp_sock(subflow);
-			int how = RCV_SHUTDOWN | SEND_SHUTDOWN;
-
-			if (rm_list.ids[i] != subflow->local_id)
-				continue;
-
-			pr_debug(" -> subflow rm_list_ids[%d]=%u", i, rm_list.ids[i]);
-			spin_unlock_bh(&msk->pm.lock);
-			mptcp_subflow_shutdown(sk, ssk, how);
-			mptcp_close_ssk(sk, ssk, subflow);
-			spin_lock_bh(&msk->pm.lock);
-
-			msk->pm.local_addr_used--;
-			msk->pm.subflows--;
-
-			__MPTCP_INC_STATS(sock_net(sk), MPTCP_MIB_RMSUBFLOW);
-		}
-	}
-}
-
 static bool address_use_port(struct mptcp_pm_addr_entry *entry)
 {
 	return (entry->addr.flags &
-- 
2.29.2

