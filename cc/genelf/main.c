#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#define ALLOC_SIZE 99999

#define ELF_BASE      0x08048000
#define PHDR_OFFSET   0x30

#define INTERP_OFFSET 0x90
#define INTERP_SIZE   0x13

#define DYNAMIC_OFFSET (INTERP_OFFSET + INTERP_SIZE + 1)
#define DYNAMIC_SIZE   (11*8)

#define ELFSTART_SIZE  (DYNAMIC_OFFSET + DYNAMIC_SIZE)

/* size of startup code */
#define STARTUP_SIZE   17

/* size of library names at the start of the .dynstr section */
#define DYNSTR_BASE      22

void *data, *glo;

void put32(int addr, int word)
{
	*(char *)addr++ = word;
	*(char *)addr++ = word >> 8;
	*(char *)addr++ = word >> 16; 
	*(char *)addr++ = word >> 24; 
}

static void gle32(int word)
{
	put32((int)glo, word);
	glo = glo + 4;
}

int main()
{
	int glo_saved;
	char file[32] = "test";

	glo_saved = (int)glo;

	data = malloc(ALLOC_SIZE);
	glo = data + ELFSTART_SIZE;

	gle32(0x464c457f);

	int fd = creat(file, 0775);
	int len = write(fd, data, 10);
	close(fd);
}
