#include <stdio.h>

#define NUM 5

int global;

function(arg)
{
    int i;
    for (i = 0; i < arg; i++)
        printf("hello world!\n");
}

main()
{
    global = NUM;
    function(global);
    return 0;
}
