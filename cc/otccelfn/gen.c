#include <stdio.h>
#include <string.h>
#include "tcc.h"
#include "tok.h"
#include "elf.h"

#define LOCAL   0x200
int rsym, loc;
    /* rsym: return symbol */
    /* loc : local variable index */
static void expr(void);
void decl(int l);

extern int tok, tokc, tokl;
extern int ch;

static void skip(int c)
{
    if (tok != c) {
        printf("'%c' expected", c);
    }
    next();
}

/* from 0 to 4 bytes */
/* o: output */
/* ind: index */
/* o函数将值n放到ind处, 并后移ind */
/* 相当于把n值放到prog里面 */
void o(int n)
{
    /* cannot use unsigned, so we must do a hack */
    while (n && n != -1) {
        *(char *)ind++ = n;
        n = n >> 8;
    }
}

/* output a symbol and patch all references to it */
void gsym_addr(int t, int b)
{
    int n;
    while (t) {
        n = get32(t); /* next value */
        /* patch absolute reference (always mov/lea before) */
        if (*(char *)(t - 1) == 0x05) {
            /* XXX: incorrect if data < 0 */
            if (b >= (int)data && b < (int)glo)
                put32(t, b + data_offset);
            else
                put32(t, b - (int)prog + text + data_offset);
        } else {
            put32(t, b - t - 4);
        }
        t = n;
    }
}

static void gsym(int t)
{
    gsym_addr(t, (int)ind);
}

/* psym is used to put an instruction with a data field which is a
   reference to a symbol. It is in fact the same as oad ! */
#define psym oad

/* instruction + address */
/* oad: output with address */
/* 将t值赋到ind指向的地址, 并后移ind指针 */
/* 相当于把t值放到prog里面 */
/* 输入: 值t */
/* 输出: ind */
int oad(int n, int t)
{
    o(n);
    put32(ind, t);
    t = (int)ind;
    ind = ind + 4;
    return t;
}

/* load immediate value */
void li(int t)
{
    oad(0xb8, t); /* mov $xx, %eax */
    /* B8+ rd MOV r32,imm32 Move imm32 to r32 */
}

/* generate a jump to a label */
static int gjmp(int t)
{
    return psym(0xe9, t); /* jmpl xxx */
    /* E9 cw JMP rel16 Jump near, relative, displacement relative to next instruction */
    /* E9 cd JMP rel32 Jump near, relative, displacement relative to next instruction */
}

/* l = 0: je, l == 1: jne */
/* generate a test. set 'inv' to invert test. Stack entry is popped */
static int gtst(int l, int t)
{
    o(0x0fc085); /* test %eax, %eax, je/jne xxx */
    return psym(0x84 + l, t);
}

/* generate a cmp */
static void gcmp(int t)
{
    o(0xc139); /* cmp %eax,%ecx */
    li(0);
    o(0x0f); /* setxx %al */
    o(t + 0x90);
    o(0xc0);
}

/* generate a mov */
static void gmov(int l, int t)
{
    int n;
    o(l + 0x83);
    n = *(int *)t;
    if (n && n < LOCAL)
        oad(0x85, n);
    else {
        t = t + 4;
        *(int *)t = psym(0x05, *(int *)t);
    }
}

/* l is one if '=' parsing wanted (quick hack) */
/* unary: 一元的；单项的 */
static int unary(int l)
{
    int n, t, a, c;

    n = 1; /* type of expression 0 = forward, 1 = value, other =
              lvalue */
    if (tok == '\"') {
        li((int)(glo + data_offset));
        while (ch != '\"') {
            getq();
            *(char *)glo++ = ch;
            inp();
        }
        *(char *)glo = 0;
        glo = (char *)((int)glo + 4 & -4); /* align heap */
        inp();
        next();
    } else {
        c = tokl;
        a = tokc;
        t = tok;
        next();
        if (t == TOK_NUM) {
            li(a);
        } else if (c == 2) {
            /* -, +, !, ~ */
            unary(0);
            oad(0xb9, 0); /* movl $0, %ecx */
            if (t == '!')
                gcmp(a);
            else
                o(a);
        } else if (t == '(') {
            expr();
            skip(')');
        } else if (t == '*') {
            /* parse cast */
            skip('(');
            t = tok; /* get type */
            next(); /* skip int/char/void */
            next(); /* skip '*' or '(' */
            if (tok == '*') {
                /* function type */
                skip('*');
                skip(')');
                skip('(');
                skip(')');
                t = 0;
            }
            skip(')');
            unary(0);
            if (tok == '=') {
                next();
                o(0x50); /* push %eax */
                expr();
                o(0x59); /* pop %ecx */
                o(0x0188 + (t == TOK_INT)); /* movl %eax/%al, (%ecx) */
            } else if (t) {
                if (t == TOK_INT)
                    o(0x8b); /* mov (%eax), %eax */
                else 
                    o(0xbe0f); /* movsbl (%eax), %eax */
                ind++; /* add zero in code */
            }
        } else if (t == '&') {
            gmov(10, tok); /* leal EA, %eax */
            next();
        } else {
            n = 0;
            if (tok == '=' & l) {
                /* assignment */
                next();
                expr();
                gmov(6, t); /* mov %eax, EA */
            } else if (tok != '(') {
                /* variable */
                gmov(8, t); /* mov EA, %eax */
                if (tokl == 11) {
                    gmov(0, t);
                    o(tokc);
                    next();
                }
            }
        }
    }

    /* function call */
    if (tok == '(') {
        if (n)
            o(0x50); /* push %eax */

        /* push args and invert order */
        a = oad(0xec81, 0); /* sub $xxx, %esp */
        next();
        l = 0;
        while(tok != ')') {
            expr();
            oad(0x248489, l); /* movl %eax, xxx(%esp) */
            if (tok == ',')
                next();
            l = l + 4;
        }
        put32(a, l);
        next();
        if (n) {
            oad(0x2494ff, l); /* call *xxx(%esp) */
            l = l + 4;
        } else {
            /* forward reference */
            t = t + 4;
            *(int *)t = psym(0xe8, *(int *)t);
        }
        if (l)
            oad(0xc481, l); /* add $xxx, %esp */
    }
}

static void sum(int l)
{
    int t, n, a;

    if (l-- == 1)
        unary(1);
    else {
        sum(l);
        a = 0;
        while (l == tokl) {
            n = tok;
            t = tokc;
            next();

            if (l > 8) {
                a = gtst(t, a); /* && and || output code generation */
                sum(l);
            } else {
                o(0x50); /* push %eax */
                sum(l);
                o(0x59); /* pop %ecx */
                
                if (l == 4 | l == 5) {
                    gcmp(t);
                } else {
                    o(t);
                    if (n == '%')
                        o(0x92); /* xchg %edx, %eax */
                }
            }
        }
        /* && and || output code generation */
        if (a && l > 8) {
            a = gtst(t, a);
            li(t ^ 1);
            gjmp(5); /* jmp $ + 5 */
            gsym(a);
            li(t);
        }
    }
}

static void expr(void)
{
    sum(11);
}

static int test_expr(void)
{
    expr();
    return gtst(0, 0);
}

/* 处理代码块 */
static void block(int l)
{
    int a, n, t;

    /* 条件语句 */
    if (tok == TOK_IF) {
        next();
        skip('(');
        a = test_expr();
        skip(')');
        block(l);
        if (tok == TOK_ELSE) {
            next();
            n = gjmp(0); /* jmp */
            gsym(a);
            block(l);
            gsym(n); /* patch else jmp */
        } else {
            gsym(a); /* patch if test */
        }
    } else if (tok == TOK_WHILE | tok == TOK_FOR) { /* 循环语句 */
        t = tok;
        next();
        skip('(');
        if (t == TOK_WHILE) {
            n = (int)ind;
            a = test_expr();
        } else {
            if (tok != ';')
                expr();
            skip(';');
            n = (int)ind;
            a = 0;
            if (tok != ';')
                a = test_expr();
            skip(';');
            if (tok != ')') {
                t = gjmp(0);
                expr();
                gjmp(n - (int)ind - 5);
                gsym(t);
                n = t + 4;
            }
        }
        skip(')');
        block((int)&a);
        gjmp(n - (int)ind - 5); /* jmp */
        gsym(a);
    } else if (tok == '{') {
        next();
        /* declarations */
        decl(1);
        while(tok != '}')
            block(l);
        next();
    } else {
        if (tok == TOK_RETURN) {
            next();
            if (tok != ';')
                expr();
            rsym = gjmp(rsym); /* jmp */
        } else if (tok == TOK_BREAK) {
            next();
            *(int *)l = gjmp(*(int *)l);
        } else if (tok != ';')
            expr();
        skip(';');
    }
}

/* 'l' is true if local declarations */
void decl(int l)
{
    int a;

    /* == 7, | 10, != 7, & 8, ! 2 */
    /*    (tok == TOK_INT) | ((tok != -1) & !l) */
    /* 不能处理非整型 */
    /* l为0全局变量 */
    /* l为1局部变量 */
    while (tok == TOK_INT | tok != -1 & !l) {
        if (tok == TOK_INT) {
            next();
            while (tok != ';') {
                if (l) {
                    loc = loc + 4;
                    *(int *)tok = -loc;
                } else {
                    *(int *)tok = (int)glo;
                    glo = glo + 4;
                }
                next();
                if (tok == ',') 
                    next();
            }
            skip(';');
        } else { /* 处理函数 */
            /* put function address */
            *(int *)tok = (int)ind;
            next();
            skip('(');
            a = 8;
            while (tok != ')') { /* 函数参数 */
                /* read param name and compute offset */
                *(int *)tok = a;
                a = a + 4;
                next();
                if (tok == ',')
                    next();
            }
            next(); /* skip ')' */
            rsym = loc = 0;
            o(0xe58955); /* push   %ebp, mov %esp, %ebp */
            a = oad(0xec81, 0); /* sub $xxx, %esp */
            block(0);    /* 函数体 */
            gsym(rsym);
            o(0xc3c9); /* leave, ret */
            put32(a, loc); /* save local variables */
        }
    }
}
