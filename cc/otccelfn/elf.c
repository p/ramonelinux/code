#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include "tcc.h"
#include "elf.h"
#include "tok.h"

void *text;
extern int tok;

/* put a 32 bit little endian word 'w' at unaligned address 'addr' */
/* 将值赋到地址上 */
void put32(int addr, int word)
{
    *(char *)addr++ = word;
    *(char *)addr++ = word >> 8;
    *(char *)addr++ = word >> 16;
    *(char *)addr++ = word >> 24;
}

/* get a 32 bit little endian word at unaligned address 'addr' */
/* 取参数地址处的值 */
int get32(int addr)
{
    return (*(char *)addr & 0xff) |
        (*(char *)(addr + 1) & 0xff) << 8 |
        (*(char *)(addr + 2) & 0xff) << 16 |
        (*(char *)(addr + 3) & 0xff) << 24;
}

/* 将word值赋到glo指向的地址, 并后移glo指针 */
/* 相当于把word值放到data里面 */
/* 输入: 值word */
/* 输出: glo */
static void gle32(int word)
{
    put32((int)glo, word);
    glo = glo + 4;
}

/* used to generate a program header at offset 't' of size 's' */
/* gphdr: generate program header */
static void gphdr1(int n, int t)
{
    gle32(n);
    n = n + ELF_BASE;
    gle32(n);
    gle32(n);
    gle32(t);
    gle32(t);
}

/* elf 重定位 */
static void elf_reloc(int l)
{
    int a, n, p, b, c;
    void *t;

    p = 0;
    t = sym_stk;
    while (1) {
        /* extract symbol name */
        t++;
        a = (int)t;
        while (*(char *)t != TAG_TOK && t < dstk)
            t++;
        if (t == dstk)
            break;
        /* now see if it is forward defined */
        tok = (int)vars + (a - (int)sym_stk) * 8 + TOK_IDENT - 8;
        b = *(int *)tok;
        n = *(int *)(tok + 4);
        if (n && b != 1) {
            if (!b) {
                if (!l) {
                    /* symbol string */
                    memcpy(glo, (char *)a, (int)t - a);
                    glo += (int)t - a + 1; /* add a zero */
                } else if (l == 1) {
                    /* symbol table */
                    gle32(p + DYNSTR_BASE);
                    gle32(0);
                    gle32(0);
                    gle32(0x10); /* STB_GLOBAL, STT_NOTYPE */
                    p += (int)t - a + 1; /* add a zero */
                } else {
                    p++;
                    /* generate relocation patches */
                    while (n) {
                        a = get32(n);
                        /* c = 0: R_386_32, c = 1: R_386_PC32 */
                        c = *(char *)(n - 1) != 0x05;
                        put32(n, -c * 4);
                        gle32(n - (int)prog + (int)text + data_offset);
                        gle32(p * 256 + c + 1);
                        n = a;
                    }
                }
            } else if (!l) {
                /* generate standard relocation */
                gsym_addr(n, b);
            }
        }
    }
}

void elf_out(char *file)
{
    int glo_saved, dynstr, dynstr_size, dynsym, hash, rel, n, t, text_size;

    /*****************************/
    /* add text segment (but copy it later to handle relocations) */
    text = glo;
    text_size = ind - prog;

    /* 现在填充prog最前端 */
    /* add the startup code */
    ind = prog;
    o(0x505458); /* pop %eax, push %esp, push %eax */
    t = *(int *)(vars + TOK_MAIN);
    oad(0xe8, t - (int)ind - 5);
    o(0xc389);  /* movl %eax, %ebx */
    /* 打造自己的反汇编引擎Intel指令编码学习报告 */
    /* http://www.pediy.com/kssd/pediy10/75557.html */
    /* IA-32 Intel® Architecture Software Developer’s Manual 
     * Volume 2: Instruction Set Reference */
    /* Reg/Opcode部分为eax，编码为(000)，R/M部分为ebx是一个寄存器，因此Mod = 11，R/M部分为ebx的编码（011）。由于并没有偏移地址部分，这条指令就该为：*/
    /* mov  Mod  Reg/Opcode  R/M */
    /* 89   (11  000         011) => 89 C3*/
    li(1);      /* mov $1, %eax */
    o(0x80cd);  /* int $0x80 */
    glo = glo + text_size;

    /*****************************/
    /* add symbol strings */
    dynstr = (int)glo;
    /* libc name for dynamic table */
    glo++;
    glo = strcpy(glo, "libc.so.6") + 10;
    glo = strcpy(glo, "libdl.so.2") + 11;
    
    /* export all forward referenced functions */
    elf_reloc(0);
    dynstr_size = (int)glo - dynstr;

    /*****************************/
    /* add symbol table */
    glo = (char *)(((int)glo + 3) & -4);
    dynsym = (int)glo;
    gle32(0);
    gle32(0);
    gle32(0);
    gle32(0);
    elf_reloc(1);

    /*****************************/
    /* add symbol hash table */
    hash = (int)glo;
    n = ((int)glo - dynsym) / 16;
    gle32(1); /* one bucket (simpler!) */
    gle32(n);
    gle32(1);
    gle32(0); /* dummy first symbol */
    t = 2;
    while (t < n)
        gle32(t++);
    gle32(0);
    
    /*****************************/
    /* relocation table */
    rel = (int)glo;
    elf_reloc(2);

    /* 将prog接到data后面 */
    /* copy code AFTER relocation is done */
    memcpy(text, prog, text_size);

    /* 现在填充data最前端, STARTUP_SIZE预留的区域 */
    glo_saved = (int)glo;
    glo = data;

    /* elf header */
    gle32(0x464c457f); /* Magic */ /* <---- 小端, 从右向左看 */
    gle32(0x00010101); /* pad[0]; hversion; byteorder; addr_width */
                       /* 01: Header version 1; 01: little-endian; 01: 32bits */
    gle32(0);          /* pad[4]; pad[3]; pad[2]; pad[1] */
    gle32(0);          /* pad[8]; pad[7]; pad[6]; pad[5] */
    gle32(0x00030002); /* archtype; filetype */
                       /* 0003: x86; 0002: executable */
    gle32(1);          /* fversion */
                       /* 1: File version 1 */
    gle32((int)text + data_offset); /* address of _start */
                       /* entry: Entry point if executable */
    gle32(PHDR_OFFSET); /* offset of phdr */
                       /* phdrpos: Program header position */
    gle32(0);          /* shdrpos: Section header position */
    gle32(0);          /* flags: Architecture relative */
    gle32(0x00200034); /* phdrent; hdrsize */
                       /* 0020: Size of program headers; 0034: ELF header size */
    gle32(3); /* phdr entry count */
                       /* shdrent; phdrcnt */

    /* program headers */
    gle32(3); /* PT_INTERP */
    gphdr1(INTERP_OFFSET, INTERP_SIZE);
    gle32(4); /* PF_R */
    gle32(1); /* align */
    
    gle32(1); /* PT_LOAD */
    gphdr1(0, glo_saved - (int)data);
    gle32(7); /* PF_R | PF_X | PF_W */
    gle32(0x1000); /* align */
    
    gle32(2); /* PT_DYNAMIC */
    gphdr1(DYNAMIC_OFFSET, DYNAMIC_SIZE);
    gle32(6); /* PF_R | PF_W */
    gle32(0x4); /* align */

    /* now the interpreter name */
    glo = strcpy(glo, "/lib/ld-linux.so.2") + 0x14;

    /* now the dynamic section */
    gle32(1); /* DT_NEEDED */
    gle32(1); /* libc name */
    gle32(1); /* DT_NEEDED */
    gle32(11); /* libdl name */
    gle32(4); /* DT_HASH */
    gle32(hash + data_offset);
    gle32(6); /* DT_SYMTAB */
    gle32(dynsym + data_offset);
    gle32(5); /* DT_STRTAB */
    gle32(dynstr + data_offset);
    gle32(10); /* DT_STRSZ */
    gle32(dynstr_size);
    gle32(11); /* DT_SYMENT */
    gle32(16);
    gle32(17); /* DT_REL */
    gle32(rel + data_offset);
    gle32(18); /* DT_RELSZ */
    gle32(glo_saved - rel);
    gle32(19); /* DT_RELENT */
    gle32(8);
    gle32(0);  /* DT_NULL */
    gle32(0);

    int fd = creat(file, 0775);
    printf("data: %s\n size: %d\n", (char *)data, glo_saved - (int)data);
    int len = write(fd, data, glo_saved - (int)data);
    close(fd);
}
