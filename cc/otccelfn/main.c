#include <stdio.h>
#include <string.h>
#include "tcc.h"
#include "elf.h"
#include "tok.h"

#define ALLOC_SIZE 99999

FILE *file;
int ch;

// All symbols are stored in this symbol stacks.
char *sym_stk; /* sym_stk: symbol stack */
               /* sym_stk 表示符号表 */
void *dstk;    /* dstk: symbol stack pointer */
void *vars; /* vars: value of variables */
            /* 相对地址 */
void *prog, *ind;
            /* prog: output code */
            /* ind : output code ptr */
void *data, *glo;
            /* data: base of data segment */
            /* glo : global variable ptr */
int data_offset;

/* read next char from current input file and handle end of input buffer */
/* 读取下一个字符到 ch */
/* 输入: 文件流 file */
/* 输出: 当前字符 ch */
extern int dptr, dch;
inline void inp(void) // input
{
    if(dptr) { // handle end of input buffer
        ch = *(char *)dptr++;
        if (ch == TAG_MACRO) {
            dptr = 0;
            ch = dch;
        }
    } else // read next char from current input file
        ch = fgetc(file);
        /* printf("ch=%c 0x%x\n", ch, ch); */
}

int main(int argc, char *argv[])
{
    char outfile[1024];

    if (argc == 3) {
        strcpy(outfile, argv[2]);
    } else if (argc == 2) {
        strcpy(outfile, "a.out");
    } else {
        printf("usage: otccelf file.c outfile\n");
        return 0;
    }

/*
     __________________TOK_STR_SIZE__________________
    |                                                |
    | int if else while break return for define main |----------------------------
    |                                                |
   sym_stk                                          dstk ----->
    TOK_STR_SIZE 48个字节, sym_stk初始化为48个字符, 新解析出来的符号添加到符号表的后面。
*/
    sym_stk = malloc(ALLOC_SIZE);
    /* 填充符号表 */
    strcpy(sym_stk, " int if else while break return for define main ");
    dstk = sym_stk + TOK_STR_SIZE;
    
/*
     ___________________________________________________
    |
   vars
*/
    vars = malloc(ALLOC_SIZE);

/*
     ______________________data_offset__________________________________________________
    |                                                                               |
            _INTERP_SIZE_ _DYNAMIC_SIZE_ _STARTUP_SIZE_
     ______|_____________|______________|______________|________________________________
    |      |             |              |                                           |
   data INTERP_OFFSET DYNAMIC_OFFSET ELFSTART_SIZE                              ELF_BASE
                                        |
                                       glo
*/
    data = malloc(ALLOC_SIZE);
    glo = data + ELFSTART_SIZE;
    data_offset = ELF_BASE - (int)data;

/*
                                         _STARTUP_SIZE_
                                        |______________|___________
                                        |              |
                                       prog           ind
*/
    prog = malloc(ALLOC_SIZE);
    ind = prog + STARTUP_SIZE;

    file = fopen(argv[1], "r");

    inp();
    next();
    decl(0);

    fclose(file);

    elf_out(outfile);
    
    return 0;
}
