#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "tcc.h"
#include "tok.h"

//tok contains the current token (see TOK_xxx) constants.
//Identifiers and keywords are also keywords.
//tokc contains additional infos about the token (for example a constant value if number or string token). 
int tok, tokc, tokl;
extern int ch;

static void pdef(int t)
{
    *(char *)dstk++ = t;
}

static int isid(void)
{
    return isalnum(ch) | ch == '_';
}

/* read a character constant */
void getq(void)
{
    if (ch == '\\') {
        inp();
        if (ch == 'n')
            ch = '\n';
    }
}

int dptr, dch; /* dptr, dch: macro state */
static int last_id;
//next() reads the next token in the current file.
/* return next token with macro substitution */
/* 词法分析 */
/* 输入: ch */
/* 输出: tok */
void next(void)
{
    int l, a;

    //printf("tok=%s %x\n", last_id, tok);

    /*  isspace - checks  for  white-space  characters.
     *  In  the "C" and "POSIX" locales, these are: space, form-feed ('\f'), newline ('\n'), carriage return ('\r'), horizontal tab ('\t'), and vertical tab ('\v'). */
    while (isspace(ch) | ch == '#') { /* 第一部分 *//* 处理宏 define */
        if (ch == '#') {
            inp();
            next();
            if (tok == TOK_DEFINE) {
                next();
                pdef(TAG_TOK); /* fill last ident tag */
                *(int *)tok = SYM_DEFINE;
                *(int *)(tok + 4) = (int)dstk; /* define stack */
            }
            /* well we always save the values ! */
            while (ch != '\n') {
                pdef(ch);
                inp();
            }
            pdef(ch);
            pdef(TAG_MACRO);
        }
        inp();
    } /* 第一部分结束 */
    tokl = 0;
    tok = ch;
    /* encode identifiers & numbers */
    /* 第二部分 */
    if (isid()) {
        pdef(TAG_TOK);
        last_id = (int)dstk;
        while (isid()) {
            pdef(ch);
            inp();
        }
        if (isdigit(tok)) {
            tokc = strtol((char *)last_id, 0, 0); /* string to long */
            tok = TOK_NUM;
        } else {
            *(char *)dstk = TAG_TOK; /* no need to mark end of string (we
                                        suppose data is initied to zero */
            /* 将新的符号添加到符号表中 */
            tok = strstr(sym_stk, (char *)(last_id - 1)) - sym_stk;
            *(char *)dstk = 0;   /* mark real end of ident for dlsym() */
            tok = tok * 8 + TOK_IDENT;
            if (tok > TOK_DEFINE) {
                tok = (int)vars + tok;
                /* printf("tok=%s %x\n", last_id, tok); */
                /* eg: tok=include 959a930 */
                /* define handling */
                if (*(int *)tok == SYM_DEFINE) {
                    dptr = *(int *)(tok + 4);
                    dch = ch;
                    inp();
                    next();
                }
            }
        }
    } else { /* 第二部分结束 *//* 第三部分 */
        inp();
        if (tok == '\'') {
            tok = TOK_NUM;
            getq();
            tokc = ch;
            inp();
            inp();
        } else if (tok == '/' & ch == '*') {
            inp();
            while (ch) {
                while (ch != '*')
                    inp();
                inp();
                if (ch == '/')
                    ch = 0;
            }
            inp();
            next();
        } else {
            char *t = "++#m--%am*@R<^1c/@%[_[H3c%@%[_[H3c+@.B#d-@%:_^BKd<<Z/03e>>`/03e<=0f>=/f<@.f>@1f==&g!=\'g&&k||#l&@.BCh^@.BSi|@.B+j~@/%Yd!@&d*@b";
            while (l = *t++) {
                a = *t++;
                tokc = 0;
                while ((tokl = *t++ - 'b') < 0)
                    tokc = tokc * 64 + tokl + 64;
                if (l == tok & (a == ch | a == '@')) {
                    if (a == ch) {
                        inp();
                        tok = TOK_DUMMY; /* dummy token for double tokens */
                    }
                    break;
                }
            }
        }
    } /* 第三部分结束 */
}
