Name:       policycoreutils
Version:    2.4
Release:    1%{?dist}
Summary:    policycoreutils

Group:      Development/Libraries
License:    BSD and LGPLv2+
Url:        https://github.com/SELinuxProject/selinux/wiki
Source:     https://github.com/SELinuxProject/selinux/wiki/Releases/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  libsepol setools libcap-ng
AutoReq:        no

%description
This is the upstream repository for the Security Enhanced Linux (SELinux) userland libraries and tools.
The software provided by this project complements the SELinux features integrated into the Linux kernel and is used by Linux distributions.

%prep
%setup -q -n %{name}-%{version}

%build
sed -i s/-Werror// setfiles/Makefile &&
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

rm -rf %{buildroot}/usr/sbin/load_policy
ln -s /sbin/load_policy %{buildroot}/usr/sbin/load_policy

%files
%defattr(-,root,root,-)
/etc/dbus-1/system.d/org.selinux.conf
/etc/pam.d/newrole
/etc/pam.d/run_init
/etc/rc.d/init.d/restorecond
/etc/selinux/restorecond.conf
/etc/selinux/restorecond_user.conf
/etc/sestatus.conf
/etc/sysconfig/sandbox
/etc/xdg/autostart/restorecond.desktop
/sbin/fixfiles
/sbin/load_policy
/sbin/restorecon
/sbin/setfiles
%{_bindir}/audit2allow
%{_bindir}/audit2why
%{_bindir}/chcat
%{_bindir}/newrole
%{_bindir}/sandbox
%{_bindir}/secon
%{_bindir}/semodule_deps
%{_bindir}/semodule_expand
%{_bindir}/semodule_link
%{_bindir}/semodule_package
%{_bindir}/semodule_unpackage
%{_bindir}/sepolgen
%{_bindir}/sepolgen-ifgen
%{_bindir}/sepolgen-ifgen-attr-helper
%{_bindir}/sepolicy
%{_bindir}/system-config-selinux
%{_libdir}/python2.7/site-packages/*
%{_libdir}/systemd/system/restorecond.service
%{_libdir}exec/selinux/hll/pp
%{_sbindir}/genhomedircon
%{_sbindir}/load_policy
%{_sbindir}/open_init_pty
%{_sbindir}/restorecond
%{_sbindir}/run_init
%{_sbindir}/semanage
%{_sbindir}/semodule
%{_sbindir}/sestatus
%{_sbindir}/setsebool
%{_sbindir}/seunshare
%{_datadir}/bash-completion/completions/semanage
%{_datadir}/bash-completion/completions/sepolicy
%{_datadir}/bash-completion/completions/setsebool
%{_datadir}/dbus-1/services/org.selinux.Restorecond.service
%{_datadir}/dbus-1/system-services/org.selinux.service
%{_datadir}/icons/hicolor/*/apps/*.png
%{_datadir}/locale/*/LC_MESSAGES/policycoreutils.mo
%{_datadir}/pixmaps/sepolicy.png
%{_datadir}/pixmaps/system-config-selinux.png
%{_datadir}/polkit-1/actions/org.selinux.config.policy
%{_datadir}/polkit-1/actions/org.selinux.policy
%{_datadir}/sandbox/sandboxX.sh
%{_datadir}/sandbox/start
%{_datadir}/system-config-selinux/*.py
%{_datadir}/system-config-selinux/*.glade
%{_datadir}/system-config-selinux/*.desktop
%{_datadir}/system-config-selinux/*.png
%{_mandir}/man*/*.gz

%changelog
* Mon Mar 30 2015 tanggeliang <tanggeliang@gmail.com>
- create
