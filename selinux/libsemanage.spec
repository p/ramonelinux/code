Name:       libsemanage
Version:    2.4
Release:    1%{?dist}
Summary:    libsemanage

Group:      Development/Libraries
License:    BSD and LGPLv2+
Url:        https://github.com/SELinuxProject/selinux/wiki
Source:     https://github.com/SELinuxProject/selinux/wiki/Releases/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  libsepol ustr audit

%description
This is the upstream repository for the Security Enhanced Linux (SELinux) userland libraries and tools.
The software provided by this project complements the SELinux features integrated into the Linux kernel and is used by Linux distributions.

%prep
%setup -q -n %{name}-%{version}

%build
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
/etc/selinux/semanage.conf
%{_includedir}/semanage/*.h
%{_libdir}/libsemanage.a
%{_libdir}/libsemanage.so*
%{_libdir}/pkgconfig/libsemanage.pc
/usr/libexec/selinux/semanage_migrate_store
%{_mandir}/man*/*.gz

%changelog
* Mon Mar 30 2015 tanggeliang <tanggeliang@gmail.com>
- create
