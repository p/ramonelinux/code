Name:       libselinux
Version:    2.4
Release:    1%{?dist}
Summary:    libselinux

Group:      Development/Libraries
License:    BSD and LGPLv2+
Url:        https://github.com/SELinuxProject/selinux/wiki
Source:     https://github.com/SELinuxProject/selinux/wiki/Releases/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  libsepol

%description
This is the upstream repository for the Security Enhanced Linux (SELinux) userland libraries and tools.
The software provided by this project complements the SELinux features integrated into the Linux kernel and is used by Linux distributions.

%package        python
Summary:        python
%description    python

%prep
%setup -q -n %{name}-%{version}

%build
make %{?_smp_mflags}
make %{?_smp_mflags} pywrap

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install
make DESTDIR=%{buildroot} LIBDIR=%{buildroot}/%{_libdir} install-pywrap

%files
%defattr(-,root,root,-)
/lib/libselinux.so.1
%{_includedir}/selinux/*.h
%{_libdir}/libselinux.a
%{_libdir}/libselinux.so
%{_libdir}/pkgconfig/libselinux.pc
%{_sbindir}/avcstat
%{_sbindir}/compute_av
%{_sbindir}/compute_create
%{_sbindir}/compute_member
%{_sbindir}/compute_relabel
%{_sbindir}/compute_user
%{_sbindir}/getconlist
%{_sbindir}/getdefaultcon
%{_sbindir}/getenforce
%{_sbindir}/getfilecon
%{_sbindir}/getpidcon
%{_sbindir}/getsebool
%{_sbindir}/getseuser
%{_sbindir}/matchpathcon
%{_sbindir}/policyvers
%{_sbindir}/sefcontext_compile
%{_sbindir}/selinux_check_securetty_context
%{_sbindir}/selinuxenabled
%{_sbindir}/selinuxexeccon
%{_sbindir}/setenforce
%{_sbindir}/setfilecon
%{_sbindir}/togglesebool
%{_mandir}/man*/*.*.gz

%files python
%defattr(-,root,root,-)
%{_libdir}/python2.7/site-packages/selinux/__init__.py
%{_libdir}/python2.7/site-packages/selinux/_selinux.so
%{_libdir}/python2.7/site-packages/selinux/audit2why.so

%changelog
* Mon Mar 30 2015 tanggeliang <tanggeliang@gmail.com>
- create
