Name:       setools
Version:    3.3.8
Release:    1%{?dist}
Summary:    SETools v3 - Policy Analysis Tools for SELinux

Group:      Development/Libraries
License:    BSD and LGPLv2+
Url:        https://github.com/TresysTechnology/setools3/wiki
Source:     https://github.com/TresysTechnology/setools3/wiki/Download/%{name}3-f1e5b208d507171968ca4d2eeefd7980f1004a3c.zip

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  libsepol automake autoconf m4
BuildRequires:	tk libglade
AutoReq:        no

%description
SETools is an open source project designed to facilitate SELinux policy analysis.

%prep
%setup -q -n %{name}3-f1e5b208d507171968ca4d2eeefd7980f1004a3c

%build
sed -i s:AC_PROG_SWIG\(2.0.0\):AC_PROG_SWIG\(3.0.0\): configure.ac

autoreconf -fi
./configure --prefix=/usr \
	    --disable-bwidget-check \
	    --disable-selinux-check \
	    --enable-swig-tcl	    \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/apol
%{_bindir}/findcon
%{_bindir}/indexcon
%{_bindir}/replcon
%{_bindir}/seaudit-report
%{_bindir}/sechecker
%{_bindir}/sediff
%{_bindir}/sediffx
%{_bindir}/seinfo
%{_bindir}/sesearch
%{_includedir}/apol/*.h
%{_includedir}/poldiff/*.h
%{_includedir}/qpol/*.h
%{_includedir}/seaudit/*.h
%{_includedir}/sefs/*.h*
%{_libdir}/libapol.*
%{_libdir}/libpoldiff.*
%{_libdir}/libqpol.*
%{_libdir}/libseaudit.*
%{_libdir}/libsefs.*
%{_libdir}/pkgconfig/libapol.pc
%{_libdir}/pkgconfig/libpoldiff.pc
%{_libdir}/pkgconfig/libqpol.pc
%{_libdir}/pkgconfig/libseaudit.pc
%{_libdir}/pkgconfig/libsefs.pc
%{_libdir}/setools/apol/libtapol.so.4.4
%{_libdir}/setools/apol/pkgIndex.tcl
%{_libdir}/setools/apol_tcl/libapol_tcl.so.4.4
%{_libdir}/setools/apol_tcl/pkgIndex.tcl
%{_libdir}/setools/poldiff/libtpoldiff.so.1.3.3
%{_libdir}/setools/poldiff/pkgIndex.tcl
%{_libdir}/setools/qpol/libtqpol.so.1.7
%{_libdir}/setools/qpol/pkgIndex.tcl
%{_libdir}/setools/seaudit/libtseaudit.so.4.5
%{_libdir}/setools/seaudit/pkgIndex.tcl
%{_libdir}/setools/sefs/libtsefs.so.4.0.4
%{_libdir}/setools/sefs/pkgIndex.tcl
%{_sbindir}/seaudit
%{_mandir}/man*/*.gz
%{_datadir}/setools-3.3/*

%changelog
* Mon Mar 30 2015 tanggeliang <tanggeliang@gmail.com>
- create
