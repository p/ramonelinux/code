Name:       refpolicy
Version:    2.20141203
Release:    1%{?dist}
Summary:    refpolicy

Group:      Development/Libraries
License:    BSD and LGPLv2+
Url:        https://github.com/TresysTechnology/refpolicy/wiki
Source:     https://github.com/TresysTechnology/refpolicy/wiki/DownloadRelease/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  checkpolicy policycoreutils libsepol libsemanage

%description
The SELinux Reference Policy project (refpolicy) is a complete SELinux policy that can be used as the system policy for a variety of systems and used as the basis for creating other policies.
Reference Policy was originally based on the NSA example policy, but aims to accomplish many additional goals.

%prep
%setup -q -n %{name}

%build
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
/etc/selinux/refpolicy/contexts/*_type
/etc/selinux/refpolicy/contexts/*_types
/etc/selinux/refpolicy/contexts/*_context
/etc/selinux/refpolicy/contexts/*_contexts
/etc/selinux/refpolicy/contexts/files/file_contexts.subs_dist
/etc/selinux/refpolicy/contexts/files/media
/etc/selinux/refpolicy/contexts/users/*_u
/etc/selinux/refpolicy/contexts/users/root
%{_datadir}/selinux/refpolicy/*.pp

%changelog
* Tue Mar 31 2015 tanggeliang <tanggeliang@gmail.com>
- create
