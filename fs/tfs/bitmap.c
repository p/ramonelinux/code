#include "tfs.h"

static DEFINE_SPINLOCK(bitmap_lock);

struct tfs_inode *tfs_raw_inode(struct super_block *sb, ino_t ino, struct buffer_head **bh)
{
	struct tfs_sb_info *sbi = tfs_sb(sb);
	struct tfs_inode *p;
	int block;

	if (!ino || ino > sbi->s_ninodes) {
		printk("Bad inode number on dev %s: %ld is out of range\n",
				sb->s_id, (long)ino);
		return NULL;
	}
	ino--;

	block = 4 + ino / TFS_INODES_PER_BLOCK;
	pr_info("%s ino=%lu, block=%d, inoTFS_INODES_PER_BLOCK=%lu\n", __func__, ino, block, (ino % TFS_INODES_PER_BLOCK));
	*bh = sb_bread(sb, block);
	if (!*bh) {
		pr_err("Unable to read inode block\n");
		return NULL;
	}

	p = (void *)(*bh)->b_data;

	return p + ino % TFS_INODES_PER_BLOCK;
}

int tfs_new_block(struct inode *inode)
{
	struct tfs_sb_info *sbi = tfs_sb(inode->i_sb);
	int bits_per_zone = 8 * BLOCK_SIZE;
	struct buffer_head *bh = sbi->s_zmap;
	int i;

	spin_lock(&bitmap_lock);
	i = tfs_find_first_zero_bit(bh->b_data, bits_per_zone);
	if (i < bits_per_zone) {
		tfs_set_bit(i, bh->b_data);
		spin_unlock(&bitmap_lock);
		mark_buffer_dirty(bh);
		i += bits_per_zone + sbi->s_firstdatazone - 1;
		if (i < sbi->s_firstdatazone || i >= 1) {
			spin_unlock(&bitmap_lock);
			return -1;
		}
		return i;
	}
	spin_unlock(&bitmap_lock);

	return 0;
}

struct inode *tfs_new_inode(const struct inode *dir, umode_t mode, int *error)
{
        struct super_block *sb = dir->i_sb;
        struct tfs_sb_info *sbi = tfs_sb(sb);
        struct inode *inode = new_inode(sb);
        struct buffer_head *bh;
        int bits_per_zone = 8 * BLOCK_SIZE;
        unsigned long i;

	if (!inode) {
		*error = -ENOMEM;
		return NULL;
	}
	i = bits_per_zone;
	bh = NULL;
	*error = -ENOSPC;
	spin_lock(&bitmap_lock);
	bh = sbi->s_imap;
	i = tfs_find_first_zero_bit(bh->b_data, bits_per_zone);
	if (!bh || i >= bits_per_zone) {
		spin_unlock(&bitmap_lock);
		iput(inode);
		return NULL;
	}
	if (tfs_test_and_set_bit(i, bh->b_data)) {    /* shouldn't happen */
		spin_unlock(&bitmap_lock);
		printk("minix_new_inode: bit already set\n");
		iput(inode);
		return NULL;
	}
	spin_unlock(&bitmap_lock);
	mark_buffer_dirty(bh);
	if (!i || i > sbi->s_ninodes) {
		iput(inode);
		return NULL;
	}
	inode_init_owner(inode, dir, mode);
	inode->i_ino = i;
	//pr_info("%s ino=%lu\n", __func__, i);
	inode->i_mtime = inode->i_atime = inode->i_ctime = current_time(inode);
	inode->i_blocks = 0;
	memset(&tfs_i(inode)->data, 0, sizeof(tfs_i(inode)->data));
	insert_inode_hash(inode);
	mark_inode_dirty(inode);

	*error = 0;
	return inode;
}
