#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <stdint.h>
#include <linux/fs.h>
#include "tfs_fs.h"

int main(int argc, char *argv[])
{
	int fd;
	ssize_t len;
	off_t off;
	struct tfs_super_block tsb;
	struct tfs_inode ti;
	struct tfs_dir_entry de;
	int fs_dirsize = 16;
	char *inode_map;
	char *zone_map;

	if (argc != 2)
		return -1;

	fd = open(argv[1], O_RDWR);
	if (fd == -1)
		perror("open");

	// block 0: skip
	off = lseek(fd, BLOCK_SIZE, SEEK_CUR);
	if (off == -1)
		perror("lseek");

	printf("block_size=%d\n", BLOCK_SIZE);

	// block 1: super block
	tsb.s_magic = TFS_MAGIC;
	tsb.s_ninodes = 0;
	tsb.s_mnt_count = 0;

	len = write(fd, &tsb, sizeof(struct tfs_super_block));
	if (len == -1)
		perror("write");

	printf("Write super block OK!\n");

	// block 2: inode bitmap
	inode_map = malloc(BLOCK_SIZE);
	//memset(inode_map, 0xff, BLOCK_SIZE);

	off = lseek(fd, BLOCK_SIZE * 2, SEEK_SET);
	if (off == -1)
		perror("lseek");
	len = write(fd, inode_map, BLOCK_SIZE);
	if (len == -1)
		perror("write");
	free(inode_map);

	// block 3: block bitmap
	zone_map = malloc(BLOCK_SIZE);
	//memset(zone_map, 0xff, BLOCK_SIZE);
	off = lseek(fd, BLOCK_SIZE * 3, SEEK_SET);
	if (off == -1)
		perror("lseek");
	len = write(fd, zone_map, BLOCK_SIZE);
	if (len == -1)
		perror("write");
	free(zone_map);

	// block 4: inode table
	off = lseek(fd, BLOCK_SIZE * 4, SEEK_SET);
	if (off == -1)
		perror("lseek");

	ti.i_mode = S_IFDIR + 0755;
	ti.i_uid = 0;
	ti.i_size = 2 * fs_dirsize;
	ti.i_time = 0;
	ti.i_gid = 0;
	ti.i_nlinks = 2;
	ti.i_zone[0] = 5;
	len = write(fd, &ti, sizeof(struct tfs_inode));
	if (len == -1)
		perror("write");

	printf("Write root inode OK!\n");

	// block 5: root dir
	off = lseek(fd, BLOCK_SIZE * 5, SEEK_SET);
	if (off == -1)
		perror("lseek");

	char root_block[BLOCK_SIZE];
	char *tmp = root_block;

	memset(root_block, 0, BLOCK_SIZE);

	*(uint16_t *) tmp = 1;
	strcpy(tmp + 2, ".");
	tmp += fs_dirsize;
	*(uint16_t *) tmp = 1;
	strcpy(tmp + 2, "..");
	tmp += fs_dirsize;
	*(uint16_t *) tmp = 2;
	strcpy(tmp + 2, ".badblocks");

	len = write(fd, root_block, BLOCK_SIZE);
	if (len == -1)
		perror("write");

	close(fd);

	return 0;
}
