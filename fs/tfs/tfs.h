#ifndef __TFS_H__
#define __TFS_H__

#include <linux/fs.h>
#include <linux/buffer_head.h>
#include "tfs_fs.h"

/*
 * tfs super-block data in memory
 */
struct tfs_sb_info {
	struct tfs_super_block *s_tsb;
	struct buffer_head *s_imap;
	struct buffer_head *s_zmap;
	struct buffer_head *s_sbh;
	unsigned long s_ninodes;
	int s_dirsize;
	int s_namelen;
	int s_firstdatazone;
};

struct tfs_inode_info {
	__u16 data[9];
	struct inode vfs_inode;
};

extern const struct inode_operations tfs_file_inode_operations;
extern const struct inode_operations tfs_dir_inode_operations;
extern const struct file_operations tfs_file_operations;
extern const struct file_operations tfs_dir_operations;

static inline struct tfs_sb_info *tfs_sb(struct super_block *sb)
{
	return sb->s_fs_info;
}

static inline struct tfs_inode_info *tfs_i(struct inode *inode)
{
	return container_of(inode, struct tfs_inode_info, vfs_inode);
}

#define tfs_test_and_set_bit	__test_and_set_bit_le
#define tfs_set_bit		__set_bit_le
#define tfs_find_first_zero_bit	find_first_zero_bit_le

extern struct inode *tfs_iget(struct super_block *sb, unsigned long ino);
extern ino_t tfs_inode_by_name(struct dentry *dentry);
extern struct tfs_inode *tfs_raw_inode(struct super_block *sb, ino_t ino, struct buffer_head **bh);
extern int tfs_get_block(struct inode * inode, sector_t block,
			 struct buffer_head *bh, int create);
extern struct inode *tfs_new_inode(const struct inode *dir, umode_t mode, int *error);
extern void tfs_set_inode(struct inode *inode, dev_t dev);
extern int tfs_make_empty(struct inode *inode, struct inode *dir);
extern int tfs_prepare_chunk(struct page *page, loff_t pos, unsigned len);
extern int tfs_add_link(struct dentry *dentry, struct inode *inode);

#endif /* __TFS_H__ */
