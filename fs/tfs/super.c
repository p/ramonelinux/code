#include <linux/init.h>
#include <linux/module.h>
#include <linux/fs.h>
#include <linux/pagemap.h>
#include <linux/magic.h>
#include <linux/buffer_head.h>
#include <linux/writeback.h>
#include "tfs.h"

static struct inode *tfs_alloc_inode(struct super_block *sb)
{
	struct tfs_inode_info *ti;

	ti = kzalloc(sizeof(struct tfs_inode_info), GFP_KERNEL);
	if (!ti)
		return NULL;
	inode_init_once(&ti->vfs_inode);
	return &ti->vfs_inode;
}

static void tfs_destroy_inode(struct inode *inode)
{
	kfree(tfs_i(inode));
}

static struct buffer_head *tfs_update_inode(struct inode *inode)
{
	struct buffer_head *bh;
	struct tfs_inode *raw_inode;
	struct tfs_inode_info *tfs_inode = tfs_i(inode);
	int i;

	raw_inode = tfs_raw_inode(inode->i_sb, inode->i_ino, &bh);
	if (!raw_inode)
		return NULL;
	raw_inode->i_mode = inode->i_mode;
	//raw_inode->i_uid = fs_high2lowuid(i_uid_read(inode));
	//raw_inode->i_gid = fs_high2lowgid(i_gid_read(inode));
	raw_inode->i_nlinks = inode->i_nlink;
	raw_inode->i_size = inode->i_size;
	raw_inode->i_time = inode->i_mtime.tv_sec;
	if (S_ISCHR(inode->i_mode) || S_ISBLK(inode->i_mode))
		raw_inode->i_zone[0] = old_encode_dev(inode->i_rdev);
	else for (i = 0; i < 9; i++) {
		pr_info("%s inod->i_ino=%ld, tfs_inode->data[%d]=%d\n", __func__, inode->i_ino, i, tfs_inode->data[i]);
		raw_inode->i_zone[i] = tfs_inode->data[i];
	}
	mark_buffer_dirty(bh);
	return bh;
}

static int tfs_write_inode(struct inode *inode, struct writeback_control *wbc)
{
	int err = 0;
	struct buffer_head *bh;

	bh = tfs_update_inode(inode);
	if (!bh)
		return -EIO;
	if (wbc->sync_mode == WB_SYNC_ALL && buffer_dirty(bh)) {
		sync_dirty_buffer(bh);
		if (buffer_req(bh) && !buffer_uptodate(bh)) {
			printk("IO error syncing minix inode [%s:%08lx]\n",
					inode->i_sb->s_id, inode->i_ino);
			err = -EIO;
		}
	}
	brelse (bh);
	return err;
}

static void tfs_put_super(struct super_block *sb)
{
	struct tfs_sb_info *sbi = tfs_sb(sb);

	if (!sb_rdonly(sb)) {
		mark_buffer_dirty(sbi->s_sbh);
	}

	brelse(sbi->s_imap);
	brelse(sbi->s_zmap);
	brelse(sbi->s_sbh);
	sb->s_fs_info = NULL;
	kfree(sbi);
}

static const struct super_operations tfs_sops = {
	.alloc_inode	= tfs_alloc_inode,
	.destroy_inode	= tfs_destroy_inode,
	.write_inode	= tfs_write_inode,
	.put_super	= tfs_put_super,
	.statfs		= simple_statfs,
};

int tfs_fill_super(struct super_block *sb, void *data, int silent)
{
	struct buffer_head *bh;
	struct tfs_super_block *tsb;
	struct tfs_sb_info *sbi;
	struct inode *root_inode;
	int ret = 0;

	sbi = kzalloc(sizeof(struct tfs_sb_info), GFP_KERNEL);
	if (!sbi)
		return -ENOMEM;
	sb->s_fs_info = sbi;

	if (!sb_set_blocksize(sb, BLOCK_SIZE)) {
		ret = -EINVAL;
		goto out_fail1;
	}

	pr_debug("block size=%d\n", BLOCK_SIZE);

	bh = sb_bread(sb, 1);
	if (!bh) {
		ret = -EINVAL;
		goto out_fail1;
	}

	tsb = (struct tfs_super_block *)bh->b_data;
	sbi->s_tsb = tsb;
	sbi->s_sbh = bh;
	sbi->s_ninodes = tsb->s_ninodes;
	sbi->s_ninodes = 100;

	pr_info("tsb->s_ninodes: %d\n", tsb->s_ninodes);
	pr_info("tsb->s_mnt_count: %d\n", tsb->s_mnt_count);
	pr_debug("tsb->s_magic: 0x%x\n", tsb->s_magic);

	sb->s_magic = tsb->s_magic;
	if (sb->s_magic != TFS_MAGIC) {
		pr_err("tfs: bad superblock\n");
		ret = -EINVAL;
		goto out_fail2;
	}

	sbi->s_imap = sb_bread(sb, 2);
	sbi->s_zmap = sb_bread(sb, 3);

	tfs_set_bit(0, sbi->s_imap->b_data);
	tfs_set_bit(0, sbi->s_zmap->b_data);
	//pr_info("%s b_data=%c\n", __func__, sbi->s_imap->b_data[0]);

	sbi->s_dirsize = 16;
	sbi->s_namelen = 14;
	sbi->s_firstdatazone = 5;
	sb->s_blocksize = BLOCK_SIZE;

	tsb->s_mnt_count += 1;

	sb->s_op = &tfs_sops;
	pr_debug("allocate root inode\n");
	root_inode = tfs_iget(sb, TFS_ROOT_INO);
	if (IS_ERR(root_inode)) {
		ret = PTR_ERR(root_inode);
		goto out_fail3;
	}

	pr_debug("make root directory\n");
	sb->s_root = d_make_root(root_inode);
	if (!sb->s_root) {
		ret = -ENOMEM;
		goto out_fail3;
	}

	pr_debug("mark buffer dirty\n");
	if (!sb_rdonly(sb)) {
		mark_buffer_dirty(bh);
	}

	return 0;

out_fail3:
	brelse(sbi->s_imap);
	brelse(sbi->s_zmap);
out_fail2:
	brelse(bh);
out_fail1:
	kfree(sbi);
	return ret;
}

struct dentry *tfs_mount(struct file_system_type *fs_type,
			 int flags, const char *dev_name, void *data)
{
	return mount_bdev(fs_type, flags, dev_name, data, tfs_fill_super);
}

static struct file_system_type tfs_fs_type = {
	.owner		= THIS_MODULE,
	.name		= "tfs",
	.mount		= tfs_mount,
	.kill_sb	= kill_block_super,
	.fs_flags	= FS_REQUIRES_DEV,
};

static int __init tfs_fs_init(void)
{
	return register_filesystem(&tfs_fs_type);
}

static void __exit tfs_fs_exit(void)
{
	unregister_filesystem(&tfs_fs_type);
}

module_init(tfs_fs_init);
module_exit(tfs_fs_exit);
MODULE_LICENSE("GPL");
