#include <linux/fs.h>
#include "tfs.h"

static int dir_commit_chunk(struct page *page, loff_t pos, unsigned len)
{
	struct address_space *mapping = page->mapping;
	struct inode *dir = mapping->host;
	int err = 0;
	block_write_end(NULL, mapping, pos, len, len, page, NULL);

	if (pos + len > dir->i_size) {
		i_size_write(dir, pos + len);
		mark_inode_dirty(dir);
	}
	if (IS_DIRSYNC(dir))
		err = write_one_page(page);
	else
		unlock_page(page);
	return err;
}

static struct page *dir_get_page(struct inode *dir, unsigned long n)
{
	struct address_space *mapping = dir->i_mapping;
	struct page *page = read_mapping_page(mapping, n, NULL);

	if (!IS_ERR(page))
		kmap(page);
	return page;
}

static inline void dir_put_page(struct page *page)
{
	kunmap(page);
	put_page(page);
}

/*
 * Return the offset into page `page_nr' of the last valid
 * byte in that page, plus one.
 */
static unsigned tfs_last_byte(struct inode *inode, unsigned long page_nr)
{
	unsigned last_byte = PAGE_SIZE;

	if (page_nr == (inode->i_size >> PAGE_SHIFT))
		last_byte = inode->i_size & (PAGE_SIZE - 1);
	return last_byte;
}

static inline void *tfs_next_entry(void *de, struct tfs_sb_info *sbi)
{
	return (void*)((char*)de + sbi->s_dirsize);
}

static inline int namecompare(int len, int maxlen, const char *name, const char *buffer)
{
	if (len < maxlen && buffer[len])
		return 0;
	return !memcmp(name, buffer, len);
}

struct tfs_dir_entry *tfs_find_entry(struct dentry *dentry, struct page **res_page)
{
	const char *name = dentry->d_name.name;
	int namelen = dentry->d_name.len;
	struct inode *dir = d_inode(dentry->d_parent);
	struct super_block *sb = dir->i_sb;
	struct tfs_sb_info *sbi = tfs_sb(sb);
	unsigned long n;
	unsigned long npages = dir_pages(dir);
	struct page *page = NULL;
	char *p;

	char *namx;
	__u32 ino;
	*res_page = NULL;

	for (n = 0; n < npages; n++) {
		char *kaddr, *limit;

		page = dir_get_page(dir, n);
		if (IS_ERR(page))
			continue;

		kaddr = (char*)page_address(page);
		limit = kaddr + tfs_last_byte(dir, n) - sbi->s_dirsize;
		for (p = kaddr; p <= limit; p = tfs_next_entry(p, sbi)) {
			struct tfs_dir_entry *de = (struct tfs_dir_entry *)p;
			namx = de->name;
			ino = de->ino;
			if (!ino)
				continue;
			if (namecompare(namelen, sbi->s_namelen, name, namx))
				goto found;
		}
		dir_put_page(page);
	}
	return NULL;

found:
	*res_page = page;
	return (struct tfs_dir_entry *)p;
}

ino_t tfs_inode_by_name(struct dentry *dentry)
{
	struct page *page;
	struct tfs_dir_entry *de = tfs_find_entry(dentry, &page);
	ino_t res = 0;

	if (de) {
		res = de->ino;
		dir_put_page(page);
	}
	return res;
}

static int tfs_readdir(struct file *file, struct dir_context *ctx)
{
	struct inode *inode = file_inode(file);
	struct super_block *sb = inode->i_sb;
	struct tfs_sb_info *sbi = tfs_sb(sb);
	unsigned chunk_size = sbi->s_dirsize;
	unsigned long npages = dir_pages(inode);
	unsigned long pos = ctx->pos;
	unsigned offset;
	unsigned long n;

	pr_info("%s npages=%ld, ino=%ld, i_size=%lld\n",
		__func__, npages, inode->i_ino, inode->i_size);
	ctx->pos = pos = ALIGN(pos, chunk_size);
	if (pos >= inode->i_size)
		return 0;

	offset = pos & ~PAGE_MASK;
	n = pos >> PAGE_SHIFT;

	for ( ; n < npages; n++, offset = 0) {
		char *p, *kaddr, *limit;
		struct page *page = dir_get_page(inode, n);

		if (IS_ERR(page))
			continue;
		kaddr = (char *)page_address(page);
		p = kaddr + offset;
		limit = kaddr + tfs_last_byte(inode, n) - chunk_size;
		for ( ; p <= limit; p = tfs_next_entry(p, sbi)) {
			const char *name;
			__u32 ino;
			struct tfs_dir_entry *de = (struct tfs_dir_entry *)p;

			name = de->name;
			pr_info("%s name=%s\n", __func__, name);
			ino = de->ino;
			if (ino) {
				unsigned l = strnlen(name, sbi->s_namelen);
				if (!dir_emit(ctx, name, l, ino, DT_UNKNOWN)) {
					dir_put_page(page);
					return 0;
				}
			}
			ctx->pos += chunk_size;
		}
		dir_put_page(page);
	}
	return 0;
}

const struct file_operations tfs_dir_operations = {
	.llseek         = generic_file_llseek,
	.read           = generic_read_dir,
	.iterate_shared = tfs_readdir,
	.fsync          = generic_file_fsync,
};

int tfs_add_link(struct dentry *dentry, struct inode *inode)
{
	struct inode *dir = d_inode(dentry->d_parent);
	const char * name = dentry->d_name.name;
	int namelen = dentry->d_name.len;
	struct super_block * sb = dir->i_sb;
	struct tfs_sb_info * sbi = tfs_sb(sb);
	struct page *page = NULL;
	unsigned long npages = dir_pages(dir);
	unsigned long n;
	char *kaddr, *p;
	struct tfs_dir_entry *de;
	loff_t pos;
	int err;
	char *namx = NULL;
	__u32 inumber;

	/*
	 * We take care of directory expansion in the same loop
	 * This code plays outside i_size, so it locks the page
	 * to protect that region.
	 */
	for (n = 0; n <= npages; n++) {
		char *limit, *dir_end;

		page = dir_get_page(dir, n);
		err = PTR_ERR(page);
		if (IS_ERR(page))
			goto out;
		lock_page(page);
		kaddr = (char*)page_address(page);
		dir_end = kaddr + tfs_last_byte(dir, n);
		limit = kaddr + PAGE_SIZE - sbi->s_dirsize;
		for (p = kaddr; p <= limit; p = tfs_next_entry(p, sbi)) {
			de = (struct tfs_dir_entry *)p;
			namx = de->name;
			inumber = de->ino;
			if (p == dir_end) {
				/* We hit i_size */
				de->ino = 0;
				goto got_it;
			}
			if (!inumber)
				goto got_it;
			err = -EEXIST;
			if (namecompare(namelen, sbi->s_namelen, name, namx))
				goto out_unlock;
		}
		unlock_page(page);
		dir_put_page(page);
	}
	BUG();
	return -EINVAL;

got_it:
	pos = page_offset(page) + p - (char *)page_address(page);
	err = tfs_prepare_chunk(page, pos, sbi->s_dirsize);
	if (err)
		goto out_unlock;
	memcpy(namx, name, namelen);
	memset(namx + namelen, 0, sbi->s_dirsize - namelen - 2);
	de->ino = inode->i_ino;
	err = dir_commit_chunk(page, pos, sbi->s_dirsize);
	dir->i_mtime = dir->i_ctime = current_time(dir);
	mark_inode_dirty(dir);
out_put:
	dir_put_page(page);
out:
	return err;
out_unlock:
	unlock_page(page);
	goto out_put;
}

int tfs_make_empty(struct inode *inode, struct inode *dir)
{
	struct page *page = grab_cache_page(inode->i_mapping, 0);
	struct tfs_sb_info *sbi = tfs_sb(inode->i_sb);
	struct tfs_dir_entry *de;
	char *kaddr;
	int err;

	if (!page)
		return -ENOMEM;
	err = tfs_prepare_chunk(page, 0, 2 * sbi->s_dirsize);
	if (err) {
		unlock_page(page);
		goto fail;
	}

	kaddr = kmap_atomic(page);
	memset(kaddr, 0, PAGE_SIZE);

	de = (struct tfs_dir_entry *)kaddr;

	de->ino = inode->i_ino;
	strcpy(de->name, ".");
	de = tfs_next_entry(de, sbi);
	de->ino = dir->i_ino;
	strcpy(de->name, "..");
	kunmap_atomic(kaddr);

	err = dir_commit_chunk(page, 0, 2 * sbi->s_dirsize);
fail:
	put_page(page);
	return err;
}
