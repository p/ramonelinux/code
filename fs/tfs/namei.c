#include <linux/fs.h>
#include "tfs.h"

static int tfs_mknod(struct inode *dir, struct dentry *dentry,
		umode_t mode, dev_t dev)
{
	int error = -ENOSPC;

	return error;
}

static int tfs_mkdir(struct inode *dir, struct dentry *dentry,
		umode_t mode)
{
	struct inode *inode;
	int err;

	inode_inc_link_count(dir);

	inode = tfs_new_inode(dir, S_IFDIR | mode, &err);
	if (!inode)
		goto out_dir;

	tfs_set_inode(inode, 0);

	inode_inc_link_count(inode);

	err = tfs_make_empty(inode, dir);
	if (err)
		goto out_fail;

	err = tfs_add_link(dentry, inode);
	if (err)
		goto out_fail;

	d_instantiate(dentry, inode);
out:
	return err;

out_fail:
	inode_dec_link_count(inode);
	inode_dec_link_count(inode);
	iput(inode);
out_dir:
	inode_dec_link_count(dir);
	goto out;
}

static int tfs_create(struct inode *dir, struct dentry *dentry,
		umode_t mode, bool excl)
{
	return tfs_mknod(dir, dentry, mode | S_IFREG, 0);
}

static struct dentry *tfs_lookup(struct inode *dir, struct dentry *dentry, unsigned int flags)
{
	struct inode *inode = NULL;
	ino_t ino;

	if (dentry->d_name.len > tfs_sb(dir->i_sb)->s_namelen)
		return ERR_PTR(-ENAMETOOLONG);

	ino = tfs_inode_by_name(dentry);
	//pr_info("%s ino=%ld\n", __func__, ino);
	if (ino) {
		inode = tfs_iget(dir->i_sb, ino);
		if (IS_ERR(inode))
			return ERR_CAST(inode);
	}
	d_add(dentry, inode);
	return NULL;
}

const struct inode_operations tfs_dir_inode_operations = {
	.create		= tfs_create,
	.lookup		= tfs_lookup,
	.link		= simple_link,
	.unlink		= simple_unlink,
	.mkdir		= tfs_mkdir,
	.rmdir		= simple_rmdir,
	.mknod		= tfs_mknod,
	.rename		= simple_rename,
};
