#include <linux/fs.h>
#include "tfs.h"

static int tfs_readpage(struct file *file, struct page *page)
{
	pr_info("%s\n", __func__);
	return block_read_full_page(page, tfs_get_block);
}

static int tfs_writepage(struct page *page, struct writeback_control *wbc)
{
	return block_write_full_page(page, tfs_get_block, wbc);
}

void tfs_truncate(struct inode *inode)
{

}

static void tfs_write_failed(struct address_space *mapping, loff_t to)
{
	struct inode *inode = mapping->host;

	if (to > inode->i_size) {
		truncate_pagecache(inode, inode->i_size);
		tfs_truncate(inode);
	}
}

static int tfs_write_begin(struct file *file, struct address_space *mapping,
		loff_t pos, unsigned len, unsigned flags,
		struct page **pagep, void **fsdata)
{
	int ret;

	ret = block_write_begin(mapping, pos, len, flags, pagep,
			tfs_get_block);
	if (unlikely(ret))
		tfs_write_failed(mapping, pos + len);

	return ret;
}

static sector_t tfs_bmap(struct address_space *mapping, sector_t block)
{
	return generic_block_bmap(mapping, block, tfs_get_block);
}

const struct address_space_operations tfs_aops = {
	.readpage	= tfs_readpage,
	.writepage	= tfs_writepage,
	.write_begin	= tfs_write_begin,
	.write_end	= generic_write_end,
	.bmap		= tfs_bmap,
};

int tfs_prepare_chunk(struct page *page, loff_t pos, unsigned len)
{
	return __block_write_begin(page, pos, len, tfs_get_block);
}

void tfs_set_inode(struct inode *inode, dev_t dev)
{
	if (S_ISREG(inode->i_mode)) {
		inode->i_op = &tfs_file_inode_operations;
		inode->i_fop = &tfs_file_operations;
		inode->i_mapping->a_ops = &tfs_aops;
	} else if (S_ISDIR(inode->i_mode)) {
		inode->i_op = &tfs_dir_inode_operations;
		inode->i_fop = &tfs_dir_operations;
		inode->i_mapping->a_ops = &tfs_aops;
	} else {
		init_special_inode(inode, inode->i_mode, dev);
	}
}

static struct inode *tfs_iget_raw(struct inode *inode)
{
	struct buffer_head *bh;
	struct tfs_inode *raw_inode;
	struct tfs_inode_info *tfs_inode = tfs_i(inode);
	int i;

	raw_inode = tfs_raw_inode(inode->i_sb, inode->i_ino, &bh);
	if (!raw_inode) {
		iget_failed(inode);
		return ERR_PTR(-EIO);
	}

	pr_info("%s ino=%ld, ram_inode->i_zone=%d\n", __func__, inode->i_ino, raw_inode->i_zone[0]);

	inode->i_mode = raw_inode->i_mode;
	//i_uid_write(inode, raw_inode->i_uid);
	//i_gid_write(inode, raw_inode->i_gid);
	set_nlink(inode, raw_inode->i_nlinks);
	inode->i_size = raw_inode->i_size;
	inode->i_mtime.tv_sec = inode->i_atime.tv_sec = inode->i_ctime.tv_sec = raw_inode->i_time;
	inode->i_mtime.tv_nsec = 0;
	inode->i_atime.tv_nsec = 0;
	inode->i_ctime.tv_nsec = 0;
	inode->i_blocks = 0;

	for (i = 0; i < 9; i++)
		tfs_inode->data[i] = raw_inode->i_zone[i];

	tfs_set_inode(inode, old_decode_dev(raw_inode->i_zone[0]));
	brelse(bh);
	unlock_new_inode(inode);
	pr_info("%s inode->i_ino=%ld tfs_inode->data[0]=%d\n", __func__, inode->i_ino, tfs_inode->data[0]);
	return inode;
}

struct inode *tfs_iget(struct super_block *sb, unsigned long ino)
{
	struct inode *inode;

	inode = iget_locked(sb, ino);
	if (!inode)
		return ERR_PTR(-ENOMEM);

	if (!(inode->i_state & I_NEW)) {
		pr_info("An old inode\n");
		return inode;
	}

	pr_info("A new inode\n");
	return tfs_iget_raw(inode);
}
