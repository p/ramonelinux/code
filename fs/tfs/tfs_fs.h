#ifndef __TFS_FS_H__
#define __TFS_FS_H__

#include <linux/types.h>

#define TFS_MAGIC 0xCDEF
#define TFS_ROOT_INO 1

#define TFS_INODES_PER_BLOCK ((BLOCK_SIZE) / (sizeof(struct tfs_inode)))

/*
 * tfs super-block on disk
 */
struct tfs_super_block {
	__u16 s_magic;
	__u16 s_ninodes;
	__u16 s_mnt_count;
};

/*
 * tfs inode layout on disk
 */
struct tfs_inode {
	__u16 i_mode;
	__u16 i_uid;
	__u32 i_size;
	__u32 i_time;
	__u8  i_gid;
	__u8  i_nlinks;
	__u16 i_zone[9];
};

/*
 * dentry on disk
 */
struct tfs_dir_entry {
	__u16 ino;
	char name[0];
};
#endif /* __TFS_FS_H__ */
