#include "tfs.h"

static inline u16 *i_data(struct inode *inode)
{
	return (u16 *)tfs_i(inode)->data;
}

int tfs_get_block(struct inode *inode, sector_t block,
		  struct buffer_head *bh, int create)
{
	pr_info("%s ino=%ld, i_data=%u\n", __func__, inode->i_ino, i_data(inode)[0]);
	map_bh(bh, inode->i_sb, i_data(inode)[0]);

	return 0;
}
