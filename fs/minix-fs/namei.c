/*
 *  linux/fs/minix/namei.c
 *
 *  Copyright (C) 1991, 1992  Linus Torvalds
 */

#include "minix.h"

static int add_nondir(struct dentry *dentry, struct inode *inode)
{
	int err = minix_add_link(dentry, inode);

	if (!err) {
		d_instantiate(dentry, inode);
		return 0;
	}
	inode_dec_link_count(inode);
	iput(inode);
	return err;
}

//7 在目录dir中创建一个设备文件dentry。rdev为设备号。
static int minix_mknod(struct inode *dir, struct dentry *dentry,
		umode_t mode, dev_t rdev)
{
	int error;
	struct inode *inode;

	if (!old_valid_dev(rdev))
		return -EINVAL;

	inode = minix_new_inode(dir, mode, &error);
	if (inode) {
		minix_set_inode(inode, rdev);
		mark_inode_dirty(inode);
		error = add_nondir(dentry, inode);
	}
	return error;
}

//1 该方法只对代表目录的inode有意义。用于在目录dir下创建常规文件dentry。
static int minix_create(struct inode *dir, struct dentry *dentry, umode_t mode,
		bool excl)
{
	return minix_mknod(dir, dentry, mode, 0);
}

//2 在目录dir中寻找当前节点dentry的目录项。
static struct dentry *minix_lookup(struct inode *dir, struct dentry *dentry,
		unsigned int flags)
{
	struct inode *inode = NULL;
	ino_t ino;

	if (dentry->d_name.len > minix_sb(dir->i_sb)->s_namelen)
		return ERR_PTR(-ENAMETOOLONG);

	ino = minix_inode_by_name(dentry);
	if (ino) {
		inode = minix_iget(dir->i_sb, ino);
		if (IS_ERR(inode))
			return ERR_CAST(inode);
	}
	d_add(dentry, inode);
	return NULL;
}

//3 在目录dir下创建硬链接，链接目标对应old_dentry，硬链接对应dentry。
static int minix_link(struct dentry *old_dentry, struct inode *dir,
	struct dentry *dentry)
{
	struct inode *inode = old_dentry->d_inode;

	inode->i_ctime = current_time(inode);
	inode_inc_link_count(inode);
	ihold(inode);
	return add_nondir(dentry, inode);
}

//4 从目录dir中删除硬链接dentry。
static int minix_unlink(struct inode *dir, struct dentry *dentry)
{
	int err = -ENOENT;
	struct inode *inode = dentry->d_inode;
	struct page *page;
	struct minix_dir_entry *de;

	de = minix_find_entry(dentry, &page);	// 通过dentry找到de和page
	if (!de)
		goto end_unlink;
	err = minix_delete_entry(de, page);	// 在page中删除de
	if (err)
		goto end_unlink;

	inode->i_ctime = dir->i_ctime;
	inode_dec_link_count(inode);
end_unlink:
	return err;
}

//5 在目录dir下创建子目录dentry。
static int minix_mkdir(struct inode *dir, struct dentry *dentry, umode_t mode)
{
	struct inode *inode;
	int err;

	inode_inc_link_count(dir);

	inode = minix_new_inode(dir, S_IFDIR | mode, &err);	// 新建一个inode
	if (!inode)
		goto out_dir;
	minix_set_inode(inode, 0);				// 绑定inode的操作为目录操作
	inode_inc_link_count(inode);				// 增加inode使用计数
	err = minix_make_empty(inode, dir);			// 清空inode的子目录
	if (err)
		goto out_fail;

	err = minix_add_link(dentry, inode);			// 将子目录的目录项关联到inode上
	if (err)
		goto out_fail;
	d_instantiate(dentry, inode);
out:
	return err;

out_fail:
	inode_dec_link_count(inode);
	inode_dec_link_count(inode);
	iput(inode);
out_dir:
	inode_dec_link_count(dir);
	goto out;
}

//6 在目录dir中删除子目录dentry。
static int minix_rmdir(struct inode *dir, struct dentry *dentry)
{
	struct inode *inode = dentry->d_inode;
	int err = -ENOTEMPTY;

	if (minix_empty_dir(inode)) {	// 检查子目录是否为空
		err = minix_unlink(dir, dentry);	// 从父目录中删除子目录链接
		if (!err) {
			inode_dec_link_count(dir);	// 父目录减少一个链接
			inode_dec_link_count(inode);	// 子目录减少一个链接
		}
	}
	return err;
}

/*
 * directories can handle most operations...
 */
const struct inode_operations minix_dir_inode_operations = {
	.create		= minix_create,
	.lookup		= minix_lookup,
	.link		= minix_link,
	.unlink		= minix_unlink,
	.mkdir		= minix_mkdir,
	.rmdir		= minix_rmdir,
	.mknod		= minix_mknod,
};
