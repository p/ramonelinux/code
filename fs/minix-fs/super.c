/*
 *  linux/fs/minix/inode.c
 *  Copyright (C) 1991, 1992  Linus Torvalds
 */
#include <linux/module.h>
#include <linux/buffer_head.h>
#include <linux/slab.h>
#include <linux/init.h>
#include "minix.h"

static int minix_fill_super(struct super_block *s, void *data, int silent)
{
	struct buffer_head *bh;
	struct buffer_head **map;
	struct minix_super_block *ms;
	unsigned long block;
	struct inode *root_inode;
	struct minix_sb_info *sbi;
	int ret = -EINVAL;

	sbi = kzalloc(sizeof(struct minix_sb_info), GFP_KERNEL);
	if (!sbi)
		return -ENOMEM;
	s->s_fs_info = sbi;

	BUILD_BUG_ON(32 != sizeof(struct minix_inode));

	if (!sb_set_blocksize(s, BLOCK_SIZE))
		goto out_bad_hblock;

	if (!(bh = sb_bread(s, 1)))
		goto out_bad_sb;

	ms = (struct minix_super_block *)bh->b_data;
	sbi->s_ms = ms;
	sbi->s_sbh = bh;
	sbi->s_mount_state = ms->s_state;
	sbi->s_ninodes = ms->s_ninodes;
	sbi->s_nzones = ms->s_nzones;
	sbi->s_imap_blocks = ms->s_imap_blocks;
	sbi->s_zmap_blocks = ms->s_zmap_blocks;
	sbi->s_firstdatazone = ms->s_firstdatazone;
	sbi->s_log_zone_size = ms->s_log_zone_size;
	sbi->s_max_size = ms->s_max_size;
	s->s_magic = ms->s_magic;
	if (s->s_magic == MINIX_SUPER_MAGIC2) {
		sbi->s_version = MINIX_V1;
		sbi->s_dirsize = 32;	// __u16 2 + 30
		sbi->s_namelen = 30;
		s->s_max_links = MINIX_LINK_MAX;
	} else
		goto out_no_fs;

	/*
	 * Allocate the buffer map to keep the superblock small.
	 */
	if (sbi->s_imap_blocks == 0 || sbi->s_zmap_blocks == 0)
		goto out_illegal_sb;
	map = kzalloc(2 * sizeof(bh), GFP_KERNEL);
	if (!map)
		goto out_no_map;
	sbi->s_imap = &map[0];
	sbi->s_zmap = &map[1];

	if (!(sbi->s_imap[0] = sb_bread(s, 2)))
		goto out_no_bitmap;
	if (!(sbi->s_zmap[0] = sb_bread(s, 3)))
		goto out_no_bitmap;

	minix_set_bit(0, sbi->s_imap[0]->b_data);
	minix_set_bit(0, sbi->s_zmap[0]->b_data);

	/* Apparently minix can create filesystems that allocate more blocks for
	 * the bitmaps than needed.  We simply ignore that, but verify it didn't
	 * create one with not enough blocks and bail out if so.
	 */
	block = minix_blocks_needed(sbi->s_ninodes, s->s_blocksize);
	if (sbi->s_imap_blocks < block) {
		pr_err("MINIX-fs: file system does not have enough imap blocks allocated.  Refusing to mount\n");
		goto out_no_bitmap;
	}

	block = minix_blocks_needed(
			(sbi->s_nzones - (sbi->s_firstdatazone + 1)),
			s->s_blocksize);
	if (sbi->s_zmap_blocks < block) {
		pr_err("MINIX-fs: file system does not have enough zmap blocks allocated.  Refusing to mount.\n");
		goto out_no_bitmap;
	}

	/* set up enough so that it can read an inode */
	s->s_op = &minix_sops;
	root_inode = minix_iget(s, MINIX_ROOT_INO);
	if (IS_ERR(root_inode)) {
		ret = PTR_ERR(root_inode);
		goto out_no_root;
	}

	ret = -ENOMEM;
	s->s_root = d_make_root(root_inode);
	if (!s->s_root)
		goto out_no_root;

	if (!(s->s_flags & MS_RDONLY)) {
		ms->s_state &= ~MINIX_VALID_FS;
		mark_buffer_dirty(bh);
	}
	if (!(sbi->s_mount_state & MINIX_VALID_FS))
		pr_info("MINIX-fs: mounting unchecked file system, running fsck is recommended\n");
	else if (sbi->s_mount_state & MINIX_ERROR_FS)
		pr_info("MINIX-fs: mounting file system with errors, running fsck is recommended\n");

	return 0;

out_no_root:
	if (!silent)
		pr_err("MINIX-fs: get root inode failed\n");
	goto out_freemap;

out_no_bitmap:
	pr_err("MINIX-fs: bad superblock or unable to read bitmaps\n");
out_freemap:
	brelse(sbi->s_imap[0]);
	brelse(sbi->s_zmap[0]);
	kfree(sbi->s_imap);
	goto out_release;

out_no_map:
	ret = -ENOMEM;
	if (!silent)
		pr_err("MINIX-fs: can't allocate map\n");
	goto out_release;

out_illegal_sb:
	if (!silent)
		pr_err("MINIX-fs: bad superblock\n");
	goto out_release;

out_no_fs:
	if (!silent)
		pr_err("VFS: Can't find a Minix filesystem V1 on device %s.\n",
			s->s_id);
out_release:
	brelse(bh);
	goto out;

out_bad_hblock:
	pr_err("MINIX-fs: blocksize too small for device\n");
	goto out;

out_bad_sb:
	pr_err("MINIX-fs: unable to read superblock\n");
out:
	s->s_fs_info = NULL;
	kfree(sbi);
	return ret;
}

static struct dentry *minix_mount(struct file_system_type *fs_type,
	int flags, const char *dev_name, void *data)
{
	return mount_bdev(fs_type, flags, dev_name, data, minix_fill_super);
}

static struct file_system_type minix_fs_type = {
	.owner		= THIS_MODULE,
	.name		= "minix",
	.mount		= minix_mount,
	.kill_sb	= kill_block_super,
	.fs_flags	= FS_REQUIRES_DEV,
};
MODULE_ALIAS_FS("minix");

static int __init init_minix_fs(void)
{
	return register_filesystem(&minix_fs_type);
}

static void __exit exit_minix_fs(void)
{
	unregister_filesystem(&minix_fs_type);
}

module_init(init_minix_fs)
module_exit(exit_minix_fs)
MODULE_LICENSE("GPL");
