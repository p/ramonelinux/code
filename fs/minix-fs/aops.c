#include <linux/buffer_head.h>
#include "minix.h"

//aops1
static int minix_readpage(struct file *file, struct page *page)
{
	return block_read_full_page(page, minix_get_block);
}

//aops2
static int minix_writepage(struct page *page, struct writeback_control *wbc)
{
	return block_write_full_page(page, minix_get_block, wbc);
}

static void minix_write_failed(struct address_space *mapping, loff_t to)
{
	struct inode *inode = mapping->host;

	if (to > inode->i_size) {
		truncate_pagecache(inode, inode->i_size);
		minix_truncate(inode);
	}
}

//aops3
static int minix_write_begin(struct file *file, struct address_space *mapping,
			loff_t pos, unsigned len, unsigned flags,
			struct page **pagep, void **fsdata)
{
	int ret;

	ret = block_write_begin(mapping, pos, len, flags, pagep,
				minix_get_block);
	if (unlikely(ret))
		minix_write_failed(mapping, pos + len);

	return ret;
}

//aops4
static sector_t minix_bmap(struct address_space *mapping, sector_t block)
{
	return generic_block_bmap(mapping, block, minix_get_block);
}

const struct address_space_operations minix_aops = {
	.readpage	= minix_readpage,
	.writepage	= minix_writepage,
	.write_begin	= minix_write_begin,
	.write_end	= generic_write_end,
	.bmap		= minix_bmap
};
