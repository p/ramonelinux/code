#ifndef FS_MINIX_H
#define FS_MINIX_H

#include <linux/fs.h>
#include <linux/pagemap.h>
#include "minix_fs.h"

#define MINIX_V1		0x0001		/* original minix fs */

/*
 * minix fs inode data in memory
 */
struct minix_inode_info {
	union {
		__u16 i1_data[16];
	} u;
	struct inode vfs_inode;
};

/*
 * minix super-block data in memory
 */
struct minix_sb_info {
	unsigned long s_ninodes;
	unsigned long s_nzones;
	unsigned long s_imap_blocks;
	unsigned long s_zmap_blocks;
	unsigned long s_firstdatazone;
	unsigned long s_log_zone_size;
	unsigned long s_max_size;
	int s_dirsize;
	int s_namelen;
	struct buffer_head **s_imap;
	struct buffer_head **s_zmap;
	struct buffer_head *s_sbh;
	struct minix_super_block *s_ms;
	unsigned short s_mount_state;
	unsigned short s_version;
};

extern struct inode *minix_iget(struct super_block *, unsigned long);
extern struct minix_inode *minix_raw_inode(struct super_block *, ino_t,
		struct buffer_head **);
extern struct inode *minix_new_inode(const struct inode *, umode_t, int *);
extern void minix_free_inode(struct inode *inode);
extern int minix_new_block(struct inode *inode);
extern void minix_free_block(struct inode *inode, unsigned long block);

extern void minix_truncate(struct inode *);
extern void minix_set_inode(struct inode *, dev_t);
extern int minix_get_block(struct inode *, sector_t, struct buffer_head *, int);

extern struct minix_dir_entry *minix_find_entry(struct dentry *,
		struct page **);
extern int minix_add_link(struct dentry *, struct inode *);
extern int minix_delete_entry(struct minix_dir_entry *, struct page *);
extern int minix_make_empty(struct inode *, struct inode *);
extern int minix_empty_dir(struct inode *);
extern ino_t minix_inode_by_name(struct dentry *);

extern const struct inode_operations minix_dir_inode_operations;
extern const struct file_operations minix_dir_operations;
extern const struct address_space_operations minix_aops;
extern const struct super_operations minix_sops;
extern const struct file_operations minix_file_operations;

static inline struct minix_sb_info *minix_sb(struct super_block *sb)
{
	return sb->s_fs_info;
}

static inline struct minix_inode_info *minix_i(struct inode *inode)
{
	return list_entry(inode, struct minix_inode_info, vfs_inode);
}

static inline unsigned minix_blocks_needed(unsigned bits, unsigned blocksize)
{
	return DIV_ROUND_UP(bits, blocksize * 8);
}

/*
 * little-endian bitmaps
 */
#define minix_test_and_set_bit		__test_and_set_bit_le
#define minix_set_bit			__set_bit_le
#define minix_test_and_clear_bit	__test_and_clear_bit_le
#define minix_test_bit			test_bit_le
#define minix_find_first_zero_bit	find_first_zero_bit_le

#endif /* FS_MINIX_H */
