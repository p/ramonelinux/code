extern const struct inode_operations ramfs_file_inode_operations;
extern const struct inode_operations ramfs_dir_inode_operations;
extern const struct file_operations ramfs_file_operations;
extern struct inode *ramfs_get_inode(struct super_block *sb,
		const struct inode *dir, umode_t mode, dev_t dev);
