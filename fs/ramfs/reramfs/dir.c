#include <linux/fs.h>
#include "internal.h"

/*
 * File creation. Allocate an inode, and we're done..
 */
/* SMP-safe */
static int ramfs_mknod(struct inode *dir, struct dentry *dentry,
		umode_t mode, dev_t dev)
{
	struct inode *inode = ramfs_get_inode(dir->i_sb, dir, mode, dev);
	int error = -ENOSPC;

	if (inode) {
		d_instantiate(dentry, inode);
		dget(dentry);	/* Extra count - pin the dentry in core */
		error = 0;
		dir->i_mtime = dir->i_ctime = current_time(dir);
	}
	return error;
}

static int ramfs_mkdir(struct inode *dir, struct dentry *dentry,
		umode_t mode)
{
	return ramfs_mknod(dir, dentry, mode | S_IFDIR, 0);
}

static int ramfs_create(struct inode *dir, struct dentry *dentry,
		umode_t mode, bool excl)
{
	return ramfs_mknod(dir, dentry, mode | S_IFREG, 0);
}

const struct inode_operations ramfs_dir_inode_operations = {
	.create		= ramfs_create,
	.lookup		= simple_lookup,
	.link		= simple_link,
	.unlink		= simple_unlink,
	.mkdir		= ramfs_mkdir,
	.rmdir		= simple_rmdir,
	.mknod		= ramfs_mknod,
	.rename		= simple_rename,
};
