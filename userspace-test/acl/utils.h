#include <pwd.h>
#include <grp.h>

char * userNameFromId(uid_t uid);
char * groupNameFromId(gid_t gid);
uid_t userIdFromName(const char *name);
gid_t groupIdFromName(const char *name);
void errExit(char *info);

typedef enum { FALSE, TRUE } Boolean;
