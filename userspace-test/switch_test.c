#include <stdio.h>

enum TEST_TYPE {
	TEST1,
	TEST2,
	TEST3,
	TEST4,
	TEST5,
	TEST_NUMS
};

int main()
{
	int type = TEST4;

	switch (type) {
	case TEST1:
		printf("TEST1\n");
		break;
	case TEST2:
		printf("TEST2\n");
		break;
	case TEST3 ... TEST5:
		printf("TEST3-5\n");
		break;
	default:
		printf("Unknown type!\n");
		break;
	}

	return 0;
}
