#include <stdio.h>

int main()
{
	printf("sizeof(int): %d\n", sizeof(int));
	printf("__alignof__(int): %d\n", __alignof__(int));

	printf("sizeof(double): %d\n", sizeof(double));
	printf("__alignof__(double): %d\n", __alignof__(double));

	return 0;
}
