#include <stdio.h>

unsigned long sp(void) { asm("movl %esp, %eax"); }

void func(int arg1, int arg2)
{
    int i;
    unsigned long esp = sp();
    printf("     esp: %lx\n", esp);

    int *ebp = (int *)esp;

    for (i = 0; i < 58; i++) {
        ebp += 1;
        printf("%lx: %8.8lx.\n", ebp, *ebp);
    }

    int argc = *ebp;

    for (i = 0; i < argc; i++) {
        ebp += 1;
        printf("argv[%d] at %lx: %s\n", i, ebp, *ebp);
    }   

#if 0
    ebp += 1;
    i = 0;
    while (*(++ebp)) {
        printf("envp[%d] at %lx: %s\n", i, ebp, *ebp);
        i++;
    }
#endif
}

int main()
{
    func(0x1357, 0x2468);
    return 0;
}
