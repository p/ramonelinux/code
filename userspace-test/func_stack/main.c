#include <stdio.h>

unsigned long sp(void) { asm("movl %esp, %eax"); }

void func(int a, int b)
{
    unsigned long esp = sp();
    printf("esp: %lx\n", esp);
    printf("a at %lx: %lx\n", &a, a);
    printf("b at %lx: %lx\n", &b, b);
}

int main(int argc, char *argv[], char *envp[])
{
    int i;
    
    unsigned long esp = sp();
    printf("esp: %lx\n", esp);

    printf("     %lx: %d\n", &argc, argc);
    func(0x1234, 0x5678);

    return 0;
}
