#include <stdio.h>

unsigned long sp(void) { asm("movl %esp, %eax"); }

void func(int arg1, int arg2)
{
    int i;
    unsigned long esp = sp();
    printf("     esp: %lx\n", esp);
    int *ebp = (int *)esp;
    for (i = 0; i < 15; i++) {
        ebp += 1;
        printf("%lx: %8.8lx.\n", ebp, *ebp);
    }
}

int main()
{
    func(0x1357, 0x2468);
    return 0;
}
