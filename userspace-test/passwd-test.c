/* 三位密码，只用字母和数字表示，写代码打印所有的组合。*/
#include <stdio.h>

#define MAX_CHARSET 100

int main()
{
    int i, j, k, index = 0;
    char set[MAX_CHARSET];

    for (i = '0'; i <= '9'; i++)
        set[index++] = i;
    for (i = 'A'; i <= 'Z'; i++)
        set[index++] = i;
    for (i = 'a'; i <= 'z'; i++)
        set[index++] = i;

    for (i = 0; i < index; i++)
        printf("%c", set[i]);
    printf("\n");

    for (i = 0; i < index; i++)
        for (j = 0; j < index; j++)
            for (k = 0; k < index; k++)
                printf("%c%c%c\n", set[i], set[j], set[k]);

    return 0;
}
