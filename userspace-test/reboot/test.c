#include <stdio.h>
#include <unistd.h>
#include <linux/reboot.h>
#include <sys/reboot.h>

int main()
{
	int ret;

	ret = reboot(LINUX_REBOOT_CMD_RESTART);
	if (ret)
		perror("reboot");

	return 0;
}
