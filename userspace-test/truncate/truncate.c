#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>

int main(int argc, char *argv[])
{
	int ret;
	char *path;
	off_t length;

	if (argc != 3) {
		printf("Usage: truncate <path> <length>.\n");
		return -1;
	}
	
	path = argv[1];
	length = atoi(argv[2]);

	ret = truncate(path, length);

	return ret;
}
