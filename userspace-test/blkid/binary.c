#include <stdio.h>
#include <blkid/blkid.h>

int main()
{
	blkid_probe pr;
	blkid_partlist ls;
	int nparts, i;
	const char *devname = "/dev/sda";

	pr = blkid_new_probe_from_filename(devname);
	if (!pr)
		printf("%s: faild to open device", devname);

	ls = blkid_probe_get_partitions(pr);
	nparts = blkid_partlist_numof_partitions(ls);

	for (i = 0; i < nparts; i++) {
		blkid_partition par = blkid_partlist_get_partition(ls, i);
		printf("#%d: %lu %lu  0x%x\n",
			blkid_partition_get_partno(par),
			blkid_partition_get_start(par),
			blkid_partition_get_size(par),
			blkid_partition_get_type(par));
	}

	blkid_free_probe(pr);

	// don't forget to check return codes in your code!

	return 0;
}
