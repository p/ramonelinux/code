#include <stdio.h>
#include <stdlib.h>
#include <err.h>
#include <blkid/blkid.h>

int main(int argc, char *argv[])
{
	blkid_probe pr;
	const char *uuid;
	const char *type;

	if (argc != 2) {
		fprintf(stderr, "Usage: %s devname\n", argv[0]);
		exit(1);
	}

	pr = blkid_new_probe_from_filename(argv[1]);
	if (!pr) {
		err(2, "Failed to open %s", argv[1]);
	}

	blkid_do_probe(pr);
	blkid_probe_lookup_value(pr, "UUID", &uuid, NULL);
	blkid_probe_lookup_value(pr, "TYPE", &type, NULL);

	printf("UUID=%s, TYPE=%s\n", uuid, type);

	blkid_free_probe(pr);

	return 0;
}
