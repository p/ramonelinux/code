#include <stdio.h>
#include <blkid/blkid.h>

#define TRUE 1

int main()
{
	blkid_probe pr;
	const char *devname = "/dev/sda";
	const char *pttype;

	pr = blkid_new_probe_from_filename(devname);
	if (!pr)
		printf("%s: faild to open device", devname);

	blkid_probe_enable_partitions(pr, TRUE);
	blkid_do_fullprobe(pr);

	blkid_probe_lookup_value(pr, "PTTYPE", &pttype, NULL);
	printf("%s partition type detected\n", pttype);

	blkid_free_probe(pr);

	// don't forget to check return codes in your code!

	return 0;
}
