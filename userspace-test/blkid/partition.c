#include <stdio.h>
#include <blkid/blkid.h>

int main(int argc, char *argv[])
{
	blkid_probe pr;
	blkid_partlist ls;
	int nparts, i;

	if (argc != 2) {
		printf("Usage: %s device\n", argv[0]);
		return -1;
	}

	pr = blkid_new_probe_from_filename(argv[1]);
	if (!pr) {
		perror("faild to open device");
		return -1;
	}

	ls = blkid_probe_get_partitions(pr);
	nparts = blkid_partlist_numof_partitions(ls);

	for (i = 0; i < nparts; i++) {
		blkid_partition par = blkid_partlist_get_partition(ls, i);
		printf("#%d: %lu %lu  0x%x %s %s %s\n",
			blkid_partition_get_partno(par),
			blkid_partition_get_start(par),
			blkid_partition_get_size(par),
			blkid_partition_get_type(par),
			blkid_partition_get_name(par),
			blkid_partition_get_type_string(par),
			blkid_partition_get_uuid(par));
	}

	blkid_free_probe(pr);

	return 0;
}
