#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <unistd.h>
#include <asm/types.h>
#include <sys/socket.h>
#include <linux/netlink.h>
#include <linux/rtnetlink.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <linux/sock_diag.h>
#include <linux/inet_diag.h>
#include <arpa/inet.h>
#include <pwd.h>

static const char *tcp_states_map[] = {
	[TCP_ESTABLISHED]	= "ESTABLISHED",
	[TCP_SYN_SENT]		= "SYN-SENT",
	[TCP_SYN_RECV]		= "SYN-RECV",
	[TCP_FIN_WAIT1]		= "FIN-WAIT-1",
	[TCP_FIN_WAIT2]		= "FIN-WAIT-2",
	[TCP_TIME_WAIT]		= "TIME-WAIT",
	[TCP_CLOSE]		= "CLOSE",
	[TCP_CLOSE_WAIT]	= "CLOSE-WAIT",
	[TCP_LAST_ACK]		= "LAST-ACK",
	[TCP_LISTEN]		= "LISTEN",
	[TCP_CLOSING]		= "CLOSING"
};

#define TCPF_ALL	0xFFF
#define TCPF_CONN	TCPF_ALL & ~((1 << TCP_SYN_RECV)	\
				   | (1 << TCP_TIME_WAIT)	\
				   | (1 << TCP_LISTEN)		\
				   | (1 << TCP_CLOSE))

void parse_diag_msg(struct inet_diag_msg *diag_msg, int rtalen)
{
	struct passwd *uid_info = getpwuid(diag_msg->idiag_uid);
	char local_addr_buf[INET6_ADDRSTRLEN];
	char remote_addr_buf[INET6_ADDRSTRLEN];

	memset(local_addr_buf, 0, sizeof(local_addr_buf));
	memset(remote_addr_buf, 0, sizeof(remote_addr_buf));

	if (diag_msg->idiag_family == AF_INET) {
		inet_ntop(AF_INET, (struct in_addr *)&(diag_msg->id.idiag_src),
			  local_addr_buf, INET_ADDRSTRLEN);
		inet_ntop(AF_INET, (struct in_addr *)&(diag_msg->id.idiag_dst),
			  remote_addr_buf, INET_ADDRSTRLEN);
	} else {
		fprintf(stderr, "Unknown family\n");
		return;
	}

	if (local_addr_buf[0] == 0 || remote_addr_buf[0] == 0) {
		fprintf(stderr, "Could not get required connection information\n");
		return;
	} else {
		fprintf(stdout, "User: %s (UID: %u) Src: %s:%d Dst: %s:%d\n",
			uid_info == NULL ? "Not found" : uid_info->pw_name,
			diag_msg->idiag_uid,
			local_addr_buf, ntohs(diag_msg->id.idiag_sport),
			remote_addr_buf, ntohs(diag_msg->id.idiag_dport));
	}

	if (rtalen > 0) {
		struct rtattr *attr = (struct rtattr *)(diag_msg + 1);

		while (RTA_OK(attr, rtalen)) {
			if (attr->rta_type == INET_DIAG_INFO) {
				struct tcp_info *tcpi = (struct tcp_info *)RTA_DATA(attr);

				fprintf(stdout, "\tstate: %s rtt: %gms/%gms rcv_rtt: %gms snd_cwnd: %u/%u\n",
					tcp_states_map[tcpi->tcpi_state],
					(double)tcpi->tcpi_rtt / 1000,
					(double)tcpi->tcpi_rttvar / 1000,
					(double)tcpi->tcpi_rcv_rtt / 1000,
					tcpi->tcpi_unacked,
					tcpi->tcpi_snd_cwnd);
			}
			attr = RTA_NEXT(attr, rtalen);
		}
	}
}

int main(int argc, char *argv[])
{
	int nl_sock = 0;

	if ((nl_sock = socket(AF_NETLINK, SOCK_DGRAM, NETLINK_INET_DIAG)) == -1) {
		perror("socket: ");
		return EXIT_FAILURE;
	}

	struct sockaddr_nl sa;
	memset(&sa, 0, sizeof(sa));
	sa.nl_family = AF_NETLINK;

	struct inet_diag_req_v2 conn_req;
	memset(&conn_req, 0, sizeof(conn_req));
	conn_req.sdiag_family = AF_INET;
	conn_req.sdiag_protocol = IPPROTO_TCP;
	conn_req.idiag_states = TCPF_CONN;
	conn_req.idiag_ext |= (1 << (INET_DIAG_INFO - 1));

	struct nlmsghdr nlh;
	memset(&nlh, 0, sizeof(nlh));
	nlh.nlmsg_len = NLMSG_LENGTH(sizeof(conn_req));
	nlh.nlmsg_flags = NLM_F_DUMP | NLM_F_REQUEST;
	nlh.nlmsg_type = SOCK_DIAG_BY_FAMILY;

	struct iovec iov[4];
	iov[0].iov_base = (void *)&nlh;
	iov[0].iov_len = sizeof(nlh);
	iov[1].iov_base = (void *)&conn_req;
	iov[1].iov_len = sizeof(conn_req);

	struct msghdr msg;
	memset(&msg, 0, sizeof(msg));
	msg.msg_name = (void *)&sa;
	msg.msg_namelen = sizeof(sa);
	msg.msg_iov = iov;
	msg.msg_iovlen = 2;

	if (sendmsg(nl_sock, &msg, 0) < 0) {
		perror("sendmsg: ");
		return EXIT_FAILURE;
	}

	while (1) {
		struct nlmsghdr *recv_nlh;
		uint8_t recv_buf[8192];
		int numbytes = 0;

		numbytes = recv(nl_sock, recv_buf, sizeof(recv_buf), 0);
		recv_nlh = (struct nlmsghdr *)recv_buf;

		while (NLMSG_OK(recv_nlh, numbytes)) {
			struct inet_diag_msg *diag_msg;
			int rtalen = 0;

			if (recv_nlh->nlmsg_type == NLMSG_DONE) {
				close(nl_sock);
				return EXIT_SUCCESS;
			}

			if (recv_nlh->nlmsg_type == NLMSG_ERROR) {
				fprintf(stderr, "Error in netlink message\n");
				close(nl_sock);
				return EXIT_FAILURE;
			}

			diag_msg = (struct inet_diag_msg *)NLMSG_DATA(recv_nlh);
			rtalen = recv_nlh->nlmsg_len - NLMSG_LENGTH(sizeof(*diag_msg));
			parse_diag_msg(diag_msg, rtalen);

			recv_nlh = NLMSG_NEXT(recv_nlh, numbytes);
		}
	}
}
