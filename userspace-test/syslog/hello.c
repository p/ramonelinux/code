/* 
 * SYSLOG(3)
 * closelog, openlog, syslog, vsyslog - send messages to the system logger
 */

#include <syslog.h>

int main()
{
	syslog(LOG_INFO, "hello, world\n");
}
