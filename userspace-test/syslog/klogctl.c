/*
 * SYSLOG(2)
 * syslog, klogctl - read and/or clear kernel message ring buffer; set console_loglevel
 */

#include <sys/klog.h>
#include <stdio.h>
#include <stdlib.h>

/* Close the log.  Currently a NOP. */
#define SYSLOG_ACTION_CLOSE          0
/* Open the log. Currently a NOP. */
#define SYSLOG_ACTION_OPEN           1
/* Read from the log. */
#define SYSLOG_ACTION_READ           2
/* Read all messages remaining in the ring buffer. (allowed for non-root) */
#define SYSLOG_ACTION_READ_ALL       3
/* Read and clear all messages remaining in the ring buffer */
#define SYSLOG_ACTION_READ_CLEAR     4
/* Clear ring buffer. */
#define SYSLOG_ACTION_CLEAR          5
/* Disable printk's to console */
#define SYSLOG_ACTION_CONSOLE_OFF    6
/* Enable printk's to console */
#define SYSLOG_ACTION_CONSOLE_ON     7
/* Set level of messages printed to console */
#define SYSLOG_ACTION_CONSOLE_LEVEL  8
/* Return number of unread characters in the log buffer */
#define SYSLOG_ACTION_SIZE_UNREAD    9
/* Return size of the log buffer */
#define SYSLOG_ACTION_SIZE_BUFFER   10

int main()
{
	int size = klogctl(SYSLOG_ACTION_SIZE_BUFFER, NULL, 0);
	char *buf = malloc(size);

	klogctl(SYSLOG_ACTION_READ_ALL, buf, size);
	printf("%s", buf);

	free(buf);

	return 0;
}
