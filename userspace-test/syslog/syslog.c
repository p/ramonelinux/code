/* 
 * SYSLOG(3)
 * closelog, openlog, syslog, vsyslog - send messages to the system logger
 */
#include <syslog.h>

int main()
{
	openlog("syslog test", LOG_PID | LOG_CONS, LOG_USER);
	syslog(LOG_INFO, "informative message, pid=%d", getpid());
	closelog();
}
