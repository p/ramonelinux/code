#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <limits.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <pcap.h>

static void countme(u_char *user, const struct pcap_pkthdr *h, const u_char *sp)
{
	int *counterp = (int *)user;

	(*counterp)++;
}

/*
 * Copy arg vector into a new buffer, concatenating arguments with spaces.
 */
static char *copy_argv(register char **argv)
{
	register char **p;
	register size_t len = 0;
	char *buf;
	char *src, *dst;

	p = argv;
	if (*p == 0)
		return 0;

	while (*p)
		len += strlen(*p++) + 1;

	buf = (char *)malloc(len);
	if (buf == NULL)
		printf("copy_argv: malloc");

	p = argv;
	dst = buf;
	while ((src = *p++) != NULL) {
		while ((*dst++ = *src++) != '\0')
			;
		dst[-1] = ' ';
	}
	dst[-1] = '\0';

	return buf;
}

int main(int argc, char **argv)
{
	register char *cp, *cmdbuf, *device;
	char *p;
	int timeout = 1000;
	int nonblock = 0;
	pcap_if_t *devlist;
	bpf_u_int32 localnet, netmask;
	struct bpf_program fcode;
	char ebuf[PCAP_ERRBUF_SIZE];
	int status;
	int packet_count;
	static pcap_t *pd;

	if (pcap_findalldevs(&devlist, ebuf) == -1)
		printf("%s", ebuf);
	if (devlist == NULL)
		printf("no interfaces available for capture");

	device = strdup(devlist->name);
	pcap_freealldevs(devlist);

	*ebuf = '\0';
	pd = pcap_create(device, ebuf);
	if (pd == NULL)
		printf("%s", ebuf);

	status = pcap_set_snaplen(pd, 65535);
	if (status != 0)
		printf("%s: pcap_set_snaplen failed: %s", device, pcap_statustostr(status));
	status = pcap_set_timeout(pd, timeout);
	if (status != 0)
		printf("%s: pcap_set_timeout failed: %s", device, pcap_statustostr(status));
	status = pcap_activate(pd);
	if (status < 0) {
		/*
		 * pcap_activate() failed.
		 */
		printf("%s: %s\n(%s)", device, pcap_statustostr(status), pcap_geterr(pd));
	} else if (status > 0) {
		/*
		 * pcap_activate() succeeded, but it's printf us of a problem it had.
		 */
		printf("%s: %s\n(%s)", device, pcap_statustostr(status), pcap_geterr(pd));
	}
	if (pcap_lookupnet(device, &localnet, &netmask, ebuf) < 0) {
		localnet = 0;
		netmask = 0;
		printf("%s", ebuf);
	}
	cmdbuf = copy_argv(&argv[optind]);

	if (pcap_compile(pd, &fcode, cmdbuf, 1, netmask) < 0)
		printf("%s", pcap_geterr(pd));

	if (pcap_setfilter(pd, &fcode) < 0)
		printf("%s", pcap_geterr(pd));
	if (pcap_setnonblock(pd, nonblock, ebuf) == -1)
		printf("pcap_setnonblock failed: %s", ebuf);
	printf("Listening on %s\n", device);
	for (;;) {
		packet_count = 0;
		status = pcap_dispatch(pd, -1, countme, (u_char *)&packet_count);
		if (status < 0)
			break;
		if (status != 0) {
			printf("%d packets seen, %d packets counted after pcap_dispatch returns\n", status, packet_count);
		}
	}
	if (status == -2) {
		/*
		 * We got interrupted, so perhaps we didn't
		 * manage to finish a line we were printing.
		 * Print an extra newline, just in case.
		 */
		putchar('\n');
	}
	(void)fflush(stdout);
	if (status == -1) {
		/*
		 * Error.  Report it.
		 */
		(void)fprintf(stderr, "pcap_loop: %s\n", pcap_geterr(pd));
	}
	pcap_close(pd);
	pcap_freecode(&fcode);
	free(cmdbuf);
	exit(status == -1 ? 1 : 0);
}
