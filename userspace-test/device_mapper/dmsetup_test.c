#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <linux/dm-ioctl.h>
#include <linux/kdev_t.h>

#define DEVMAPPER_BUFFER_SIZE 4096

void ioctl_init(struct dm_ioctl *io, size_t datasize,
	       const char *name, unsigned flags)
{
	memset(io, 0, datasize);

	io->data_size = datasize;
	io->data_start = sizeof(struct dm_ioctl);
	io->version[0] = DM_VERSION_MAJOR;
	io->version[1] = DM_VERSION_MINOR;
	io->version[2] = DM_VERSION_PATCHLEVEL;
	io->flags = flags;

	if (name) {
		strncpy(io->name, name, sizeof(io->name));
	}
}

void version(int fd)
{
	int rc;
	char *buffer = (char *)malloc(DEVMAPPER_BUFFER_SIZE);
	struct dm_ioctl *ctl = (struct dm_ioctl *)buffer;

	ioctl_init(ctl, DEVMAPPER_BUFFER_SIZE, NULL, 0);
	rc = ioctl(fd, DM_VERSION, ctl);
	if (rc) {
		perror("ioctl error");
		free(buffer);
		close(fd);
		exit(EXIT_FAILURE);
	}

	printf("version: %d %d %d\n", ctl->version[0], ctl->version[1], ctl->version[2]);
	free(buffer);
}

void device_stat(int fd, struct dm_name_list *n)
{
	char *buffer = (char *)malloc(DEVMAPPER_BUFFER_SIZE);
	struct dm_ioctl *io = (struct dm_ioctl *)buffer;
	char tmp[512];

	ioctl_init(io, DEVMAPPER_BUFFER_SIZE, n->name, 0);
	if (ioctl(fd, DM_DEV_STATUS, io)) {
		perror("DM_DEV_STATUS ioctl failed");
		io = NULL;
	}

	if (!io) {
		sprintf(tmp, "%s %llu:%llu (no status available)", n->name, MAJOR(n->dev), MINOR(n->dev));
	} else {
		sprintf(tmp, "%s %llu:%llu %d %d 0x%.8x %llu:%llu", n->name, MAJOR(n->dev), MINOR(n->dev),
				io->target_count, io->open_count, io->flags, MAJOR(io->dev), MINOR(io->dev));
	}
	printf("%s\n", tmp);
	free(buffer);
}

void list_devices(int fd)
{
	int rc;
	char *buffer = (char *)malloc(DEVMAPPER_BUFFER_SIZE);
	struct dm_ioctl *ctl = (struct dm_ioctl *)buffer;
	struct dm_name_list *n;
	unsigned nxt = 0;

	ioctl_init(ctl, DEVMAPPER_BUFFER_SIZE, NULL, 0);
	rc = ioctl(fd, DM_LIST_DEVICES, ctl);
	if (rc) {
		perror("ioctl error");
		free(buffer);
		close(fd);
		exit(EXIT_FAILURE);
	}

	n = (struct dm_name_list *)(((char *)buffer) + ctl->data_start);
	if (!n->dev) {
		printf("dm_name_list error\n");
		free(buffer);
		close(fd);
		exit(EXIT_FAILURE);
	}

	do {
		n = (struct dm_name_list *)(((char *) n) + nxt);
		device_stat(fd, n);
		nxt = n->next;
	} while (nxt);

	free(buffer);
}

void create(int fd, char *name)
{
	int rc;
	char *buffer = (char *)malloc(DEVMAPPER_BUFFER_SIZE);
	struct dm_ioctl *ctl = (struct dm_ioctl *)buffer;

	ioctl_init(ctl, DEVMAPPER_BUFFER_SIZE, name, 0);
	rc = ioctl(fd, DM_DEV_CREATE, ctl);
	if (rc) {
		perror("ioctl error");
		free(buffer);
		close(fd);
		exit(EXIT_FAILURE);
	}

	free(buffer);
}

void load_table(int fd, char *name, char *table)
{
	struct dm_target_spec *ts;
	char *buffer = (char *)malloc(DEVMAPPER_BUFFER_SIZE);
	struct dm_ioctl *io = (struct dm_ioctl *)buffer;
	int sector_start, length, start;
	char target_type[32], target[32];

	sscanf(table, "%d %d %s %s %d", &sector_start, &length, target_type, target, &start);

	ts = (struct dm_target_spec *)&buffer[sizeof(struct dm_ioctl)];
	ioctl_init(io, DEVMAPPER_BUFFER_SIZE, name, DM_STATUS_TABLE_FLAG);
	io->target_count = 1;
	ts->status = 0;
	ts->sector_start = sector_start;
	ts->length = length;
	strncpy(ts->target_type, target_type, sizeof(ts->target_type));
	char *params = buffer + sizeof(struct dm_ioctl) + sizeof(struct dm_target_spec);
	snprintf(params, DEVMAPPER_BUFFER_SIZE - (sizeof(struct dm_ioctl) + sizeof(struct dm_target_spec)),
		 "%s %d", target, start);
	params += strlen(params) + 1;
	ts->next = params - buffer;

	if (ioctl(fd, DM_TABLE_LOAD, io)) {
		perror("ioctl DM_TABLE_LOAD error");
		free(buffer);
		close(fd);
		exit(EXIT_FAILURE);
	}

	free(buffer);
}

void resume(int fd, char *name)
{
	char *buffer = (char *)malloc(DEVMAPPER_BUFFER_SIZE);
	struct dm_ioctl *io = (struct dm_ioctl *) buffer;

	ioctl_init(io, DEVMAPPER_BUFFER_SIZE, name, 0); 
	if (ioctl(fd, DM_DEV_SUSPEND, io)) {
		perror("ioctl DM_DEV_SUSPEND error");
		free(buffer);
		close(fd);
		exit(EXIT_FAILURE);
	}

	free(buffer);
}

void destroy(int fd, char *name)
{
	int rc;
	char *buffer = (char *)malloc(DEVMAPPER_BUFFER_SIZE);
	struct dm_ioctl *ctl = (struct dm_ioctl *)buffer;

	ioctl_init(ctl, DEVMAPPER_BUFFER_SIZE, name, 0);
	rc = ioctl(fd, DM_DEV_REMOVE, ctl);
	if (rc) {
		perror("ioctl DM_DEV_REMOVE error");
		free(buffer);
		close(fd);
		exit(EXIT_FAILURE);
	}

	free(buffer);
}

int main(int argc, char *argv[])
{
	int fd;
	int rc;
	char ctl_node[32];
	char *name = "dmtest";
	char *table = "0 10240 linear /dev/sdb7 0";
	
	if (argc != 2) {
		return -1;
	}

	sprintf(ctl_node, "/dev/%s/%s", DM_DIR, DM_CONTROL_NODE);
	fd = open(ctl_node, O_RDWR | O_CLOEXEC);
	if (fd < 0) {
		perror("open error");
		return EXIT_FAILURE;
	}

	if (!strcmp(argv[1], "version"))
		version(fd);
	else if (!strcmp(argv[1], "ls"))
		list_devices(fd);
	else if (!strcmp(argv[1], "create")) {
		create(fd, name);
		load_table(fd, name, table);
		resume(fd, name);
	} else if (!strcmp(argv[1], "destroy"))
		destroy(fd, name);
	else
		printf("argv error\n");

	close(fd);

	return 0;
}
