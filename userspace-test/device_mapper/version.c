#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <linux/dm-ioctl.h>

int main()
{
	int fd;
	int rc;
	char ctl_node[32];
	char *buffer = (char *)malloc(4096);
	
	sprintf(ctl_node, "/dev/%s/%s", DM_DIR, DM_CONTROL_NODE);
	fd = open(ctl_node, O_RDWR | O_CLOEXEC);
	if (fd < 0) {
		printf("%s\n", ctl_node);
		perror("open error");
		return EXIT_FAILURE;
	}

	struct dm_ioctl *ctl = (struct dm_ioctl *)buffer;
	memset(ctl, 0, 4096);
	ctl->data_size = 4096;
	ctl->data_start = sizeof(struct dm_ioctl);
	ctl->version[0] = DM_VERSION_MAJOR;
	ctl->version[1] = DM_VERSION_MINOR;
	ctl->version[2] = DM_VERSION_PATCHLEVEL;
	rc = ioctl(fd, DM_VERSION, ctl);
	if (rc) {
		perror("ioctl error");
		printf("rc = %d\n", rc);
		return EXIT_FAILURE;
	}

	printf("version: %d %d %d\n",
	       ctl->version[0], ctl->version[1], ctl->version[2]);

	free(buffer);
	close(fd);

	return 0;
}
