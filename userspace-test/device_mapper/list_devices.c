#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <linux/dm-ioctl.h>
#include <linux/kdev_t.h>

#define DEVMAPPER_BUFFER_SIZE 4096

void ioctl_init(struct dm_ioctl *io, size_t datasize,
	        const char *name, unsigned flags) {
	memset(io, 0, datasize);
	io->data_size = datasize;
	io->data_start = sizeof(struct dm_ioctl);
	io->version[0] = DM_VERSION_MAJOR;
	io->version[1] = DM_VERSION_MINOR;
	io->version[2] = DM_VERSION_PATCHLEVEL;
	io->flags = flags;
	if (name) {
		strncpy(io->name, name, sizeof(io->name));
	}
}

int main()
{
	int fd;
	int rc;
	char ctl_node[32];
	char *buffer = (char *)malloc(DEVMAPPER_BUFFER_SIZE);
	
	sprintf(ctl_node, "/dev/%s/%s", DM_DIR, DM_CONTROL_NODE);
	fd = open(ctl_node, O_RDWR | O_CLOEXEC);
	if (fd < 0) {
		perror("open error");
		return EXIT_FAILURE;
	}

	struct dm_ioctl *ctl = (struct dm_ioctl *)buffer;
	ioctl_init(ctl, DEVMAPPER_BUFFER_SIZE, NULL, 0);
	rc = ioctl(fd, DM_VERSION, ctl);
	if (rc) {
		perror("ioctl error");
		return EXIT_FAILURE;
	}

	printf("version: %d %d %d\n", ctl->version[0], ctl->version[1], ctl->version[2]);

	ioctl_init(ctl, DEVMAPPER_BUFFER_SIZE, NULL, 0);
	rc = ioctl(fd, DM_LIST_DEVICES, ctl);
	if (rc) {
		perror("ioctl error");
		return EXIT_FAILURE;
	}

	struct dm_name_list *n = (struct dm_name_list *)(((char *)buffer) + ctl->data_start);
	if (!n->dev) {
		printf("dm_name_list error\n");
		return EXIT_FAILURE;
	}

	char *st_buffer = (char *)malloc(DEVMAPPER_BUFFER_SIZE);
	unsigned next = 0;
	do {
		n = (struct dm_name_list *)(((char *) n) + next);

		memset(st_buffer, 0, DEVMAPPER_BUFFER_SIZE);
		struct dm_ioctl *io = (struct dm_ioctl *)st_buffer;
		ioctl_init(io, DEVMAPPER_BUFFER_SIZE, n->name, 0);
		if (ioctl(fd, DM_DEV_STATUS, io)) {
			perror("DM_DEV_STATUS ioctl failed");
			io = NULL;
		}

		char tmp[512];
		if (!io) {
			sprintf(tmp, "%s %llu:%llu (no status available)",
				n->name, MAJOR(n->dev), MINOR(n->dev));
		} else {
			sprintf(tmp, "%s %llu:%llu %d %d 0x%.8x %llu:%llu",
				n->name, MAJOR(n->dev), MINOR(n->dev),
				io->target_count, io->open_count, io->flags,
				MAJOR(io->dev), MINOR(io->dev));
		}
		printf("%s\n", tmp);
		next = n->next;
	} while (next);

	free(st_buffer);
	free(buffer);

	close(fd);

	return 0;
}
