#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main()
{
	char *str, *token, *subtoken;
	char *saveptr;
	int i;
	char string[100] = "192.168.1.100";
	char delim[100] = ".";

	for (i = 1, str = string; ; i++, str = NULL) {
		token = strtok_r(str, delim, &saveptr);
		if (token == NULL)
			break;
		printf("%d: %s\n", i, token);
	}

	exit(EXIT_SUCCESS);
}
