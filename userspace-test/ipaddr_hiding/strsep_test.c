#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static void ip_tokens(char *string, char *delim, char *tokens[])
{
	char *token;
	int i = 0;

	while (token = strsep(&string, delim))
		tokens[i++] = token;
}

int main()
{
	int i;
	char *src = "10.239.41.129";
	char delim[2] = ".";
	char *tokens[4];
	char str[100];

	strcpy(str, src);

	ip_tokens(str, delim, tokens);

	for (i = 0; i < 4; i++)
		printf("%d: %s\n", i, tokens[i]);

	printf("%s -> %s.*.*.%s\n", src, tokens[0], tokens[3]);

	return 0;
}

