/*
 * From: https://github.com/TheCodeArtist/elf-parser
 */
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <elf.h>
#include <sys/types.h>
#include <sys/stat.h>

int main(int argc, char *argv[])
{
	int fd;
	ssize_t len;
	off_t off;
	int i;
	Elf32_Ehdr elf_header;		/* The ELF file header */

	if (argc != 2) {
		printf("Usage: elf-parser <ELF-file>\n");
		return -1;
	}

	fd = open(argv[1], O_RDONLY | O_SYNC);
	if (fd < 0) {
		perror("open");
		return -1;
	}

	len = read(fd, &elf_header, sizeof(Elf32_Ehdr));
	if (len != sizeof(Elf32_Ehdr)) {
		perror("read");
		return -1;
	}

	/* <1> e_ident: Magic number and other info */
	/* [0-3] ELF magic bytes are 0x7f,'E','L','F' */
	if (strncmp((const char *)elf_header.e_ident, ELFMAG, SELFMAG)) {
		printf("Magic error: mismatch!\n");
		return -1;
	}

	/* [4] File class byte index */
	switch(elf_header.e_ident[EI_CLASS]) {
	case ELFCLASS32:
		printf("32-bit objects\n");
		break;
	case ELFCLASS64:
		printf("64-bit objects\n");
		break;
	default:
		printf("Invalid class\n");
		break;
	}

	/* [5] Data encoding byte index */
	switch(elf_header.e_ident[EI_DATA]) {
	case ELFDATA2LSB:
		printf("2's complement, little endian\n");
		break;
	case ELFDATA2MSB:
		printf("2's complement, big endian\n");
		break;
	default:
		printf("Invalid Format\n");
		break;
	}

	/* [6] File version byte index, Value must be EV_CURRENT */
	if (elf_header.e_ident[EI_VERSION] != EV_CURRENT) {
		printf("File version error\n");
		return -1;
	}

	/* [7] OS ABI identification */
	switch(elf_header.e_ident[EI_OSABI]) {
	case ELFOSABI_SYSV:
		printf("UNIX System V ABI\n");
		break;
	case ELFOSABI_HPUX:
		printf("HP-UX\n");
		break;
	case ELFOSABI_NETBSD:
		printf("NetBSD\n");
		break;
	case ELFOSABI_LINUX:
		printf("Object uses GNU ELF extensions\n");
		break;
	case ELFOSABI_SOLARIS:
		printf("Sun Solaris\n");
		break;
	case ELFOSABI_AIX:
		printf("IBM AIX\n");
		break;
	case ELFOSABI_IRIX:
		printf("SGI Irix\n");
		break;
	case ELFOSABI_FREEBSD:
		printf("FreeBSD\n");
		break;
	case ELFOSABI_TRU64:
		printf("Compaq TRU64 UNIX\n");
		break;
	case ELFOSABI_MODESTO:
		printf("Novell Modesto\n");
		break;
	case ELFOSABI_OPENBSD:
		printf("OpenBSD\n");
		break;
	case ELFOSABI_ARM_AEABI:
		printf("ARM EABI\n");
		break;
	case ELFOSABI_ARM:
		printf("ARM\n");
		break;
	case ELFOSABI_STANDALONE:
		printf("Standalone (embedded) application\n");
		break;
	default:
		printf("Unknown OS ABI\n");
		break;
	}

	/* [8] ABI version */
	printf("ABI version: %d\n", elf_header.e_ident[EI_ABIVERSION]);

	/* [9-15] Byte index of padding bytes */
	/* EI_NIDENT = 16 */

	/* <2> e_type: Object file type */
	switch(elf_header.e_type) {
	case ET_NONE:
		printf("No file type\n");
		break;
	case ET_REL:
		printf("Relocatable file\n");
		break;
	case ET_EXEC:
		printf("Executable file\n");
		break;
	/*TODO*/
	}

	/* <3> e_machine: Architecture */
	switch(elf_header.e_machine) {
	case EM_X86_64:
		printf("AMD x86-64 architecture\n");
		break;
	case EM_NONE:
	default:
		printf("No machine\n");
		break;
	}

	/* <4> e_version: Object file version */
	printf("Object file version: 0x%x\n", elf_header.e_version);

	/* <5> e_entry: Entry point virtual address */
	printf("Entry point virtual address: 0x%x\n", elf_header.e_entry);

	/* <6> e_phoff: Program header table file offset */
	/* <7> e_shoff: Section header table file offset*/
	/* <8> e_flags: Processor-specific flags */
	/* <9> e_ehsize: ELF header size in bytes */
	printf("ELF header size: %d\n", elf_header.e_ehsize);

	/* <10> e_phentsize: Program header table entry size */
	printf("Program header table entry size: %d\n", elf_header.e_phentsize);

	/* <11> e_phnum: Program header table entry count */
	/* <12> e_shentsize: Section header table entry size */
	printf("Section header table entry size: %d\n", elf_header.e_shentsize);

	/* <13> e_shnum: Section header table entry count */
	printf("Section header table entry count: %d\n", elf_header.e_shnum);
	/* <14> e_shstrndx: Section header string table index */


	Elf32_Shdr *section_headers;	/* Section header */
	section_headers = malloc(elf_header.e_shentsize * elf_header.e_shnum);
	if (!section_headers) {
		printf("Failed to allocate memory\n");
		return -1;
	}

	off = lseek(fd, (off_t)elf_header.e_shoff, SEEK_SET);
	if (off != (off_t)elf_header.e_shoff) {
		printf("lseek error\n");
		return -1;
	}

	for (i = 0; i < elf_header.e_shnum; i++) {
		len = read(fd, &section_headers[i], elf_header.e_shentsize);
		if (len != elf_header.e_shentsize) {
			printf("read error\n");
			return -1;
		}
	}


	/* Section header string table index */
	printf("Section header string table index = %d\n", elf_header.e_shstrndx);
	Elf32_Shdr sh_strtable = section_headers[elf_header.e_shstrndx];
	char* sh_str = malloc(sh_strtable.sh_size);
	if(!sh_str) {
		printf("Failed to allocate memory\n");
		return -1;
	}

	off = lseek(fd, (off_t)sh_strtable.sh_offset, SEEK_SET);
	if (off != (off_t)sh_strtable.sh_offset) {
		printf("lseek error\n");
		return -1;
	}

	len = read(fd, (void *)sh_str, sh_strtable.sh_size);
	if (len != sh_strtable.sh_size) {
		printf("read error\n");
		return -1;
	}

	for (i = 0; i < elf_header.e_shnum; i++) {
		printf("%2d\t", i);
		/* Section name (string tbl index) */
		printf("%20s\t", sh_str + section_headers[i].sh_name);
		/* Section type */
		printf("0x%08x\t", section_headers[i].sh_type);
		/* Section flags */
		printf("0x%02x\t", section_headers[i].sh_flags);
		/* Section virtual addr at execution */
		printf("0x%08x\t", section_headers[i].sh_addr);
		/* Section file offset */
		printf("0x%08x\t", section_headers[i].sh_offset);
		/* Section size in bytes */
		printf("%4d\t", section_headers[i].sh_size);
		/* Link to another section */
		printf("%2d\t", section_headers[i].sh_link);
		/* Additional section information */
		printf("%2d\t", section_headers[i].sh_info);
		/* Section alignment */
		printf("0x%02x\t", section_headers[i].sh_addralign);
		/* Entry size if section holds table */
		printf("%2d\n", section_headers[i].sh_entsize);
	}

	for (i = 0; i < elf_header.e_shnum; i++) {
		if (section_headers[i].sh_type == SHT_SYMTAB ||
		    section_headers[i].sh_type == SHT_DYNSYM) {
			printf("[Section %d]\n", i);
			// TODO
		}
	}

	return 0;
}
