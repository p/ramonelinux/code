#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>

int main()
{
	char *tmp;
	char *s = "hello";
	int d = 10;

	asprintf(&tmp, "%s %d", s, d);
	
	printf("%s\n", tmp);
	free(tmp);
}

// gcc test.c -D_GNU_SOURCE=1
