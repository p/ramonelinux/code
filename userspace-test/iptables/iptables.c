#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <linux/in.h>
#include <linux/netfilter.h>
#include <linux/netfilter_ipv4/ip_tables.h>

static int print_match(const struct xt_entry_match *m,
		       const struct ipt_ip *ip)
{
	printf("%s ", m->u.user.name);

	/* Don't stop iterating. */
	return 0;
}

static int dump_entry(struct ipt_entry *e, int *num)
{
	struct xt_entry_target *t;

	IPT_MATCH_ITERATE(e, print_match, &e->ip);

	t = ipt_get_target(e);
	printf("Target name: `%s' [%u]\n", t->u.user.name, t->u.target_size);
	if (strcmp(t->u.user.name, XT_STANDARD_TARGET) == 0) {
		const unsigned char *data = t->data;
		int pos = *(const int *)data;
		if (pos < 0)
			printf("verdict=%s\n",
			       pos == -NF_ACCEPT-1 ? "NF_ACCEPT"
			       : pos == -NF_DROP-1 ? "NF_DROP"
			       : pos == -NF_QUEUE-1 ? "NF_QUEUE"
			       : pos == XT_RETURN ? "RETURN"
			       : "UNKNOWN");
		else
			printf("verdict=%u\n", pos);
	} else if (strcmp(t->u.user.name, XT_ERROR_TARGET) == 0)
		printf("error=`%s'\n", t->data);

	printf("\n");
	return 0;
}

int main()
{
	int sockfd;
	const char *tablename = "filter";
	struct ipt_getinfo info;
	struct ipt_get_entries *entries;
	unsigned int num = 0;
	socklen_t s;
	unsigned int len;

	sockfd = socket(AF_INET, SOCK_RAW, IPPROTO_RAW);
	if (sockfd < 0) {
		perror("socket");
		exit(-1);
	}

	s = sizeof(info);

	strcpy(info.name, tablename);
	if (getsockopt(sockfd, IPPROTO_IP, IPT_SO_GET_INFO, &info, &s) < 0) {
		perror("getsockopt info");
		close(sockfd);
		exit(EXIT_FAILURE);
	}

	printf("valid_hooks=0x%08x, num_entries=%u, size=%u\n",
		info.valid_hooks, info.num_entries, info.size);

	len = sizeof(struct ipt_get_entries) + info.size;

	entries = malloc(len);
	
	strcpy(entries->name, tablename);
	entries->size = info.size;

	if (getsockopt(sockfd, IPPROTO_IP, IPT_SO_GET_ENTRIES, entries, &len) < 0) {
		perror("getsockopt entries");
		close(sockfd);
		free(entries);
		exit(EXIT_FAILURE);
	}

	IPT_ENTRY_ITERATE(entries->entrytable, entries->size, dump_entry, &num);

	free(entries);

#if 0
	struct ipt_replace *repl;
	unsigned int new_size = 0;

	repl = malloc(sizeof(struct ipt_replace) + new_size);
	memset(repl, 0, sizeof(*repl) + new_size);
	repl->size = new_size;
	strcpy(repl->name, info.name);
	repl->num_entries = 0;

	if (setsockopt(sockfd, IPPROTO_IP, IPT_SO_SET_REPLACE, repl, sizeof(*repl) + repl->size) < 0) {
		perror("setsockopt");
		exit(EXIT_FAILURE);
	}
#endif

	close(sockfd);
	return 0;
}
