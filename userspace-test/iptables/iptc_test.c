#include <stdio.h>
#include <libiptc/libiptc.h>

static int print_match(const struct xt_entry_match *e,
		       const struct ipt_ip *ip)
{
	const char *name = e->u.user.name;

	printf("name: %s", name);
	printf("\n");
}

void print_rule(const struct ipt_entry *e, struct xtc_handle *h, const char *chain)
{
	const char *target_name;

	if (e->target_offset)
		IPT_MATCH_ITERATE(e, print_match, &e->ip);

	target_name = iptc_get_target(e, h);
	printf("target_name: %s", target_name);
}

void print_header(const char *chain, struct xtc_handle *handle)
{
	struct xt_counters counters;
	const char *pol = iptc_get_policy(chain, &counters, handle);
	printf("Chain %s", chain);
	if (pol)
		printf(" (policy %s)\n", pol);
	printf("target\tprot opt source\t\tdestination\n");
}

int main()
{
	const char *tablename = "filter";
	struct xtc_handle *handle;
	const char *this = NULL;

	handle = iptc_init(tablename);
	// dump_entries(handle);

	for (this = iptc_first_chain(handle);
	     this;
	     this = iptc_next_chain(handle)) {
		const struct ipt_entry *e;

		print_header(this, handle);

		e = iptc_first_rule(this, handle);
		while (e) {
			print_rule(e, handle, this);
			e = iptc_next_rule(e, handle);
		}
		printf("\n");
	}

#if 0
	int rulenum = 1; // First rule.
	iptc_delete_num_entry("OUTPUT", rulenum - 1, handle);
	iptc_commit(handle);
#endif

	//e = malloc(sizeof(struct ipt_entry) + );
	//iptc_append_entry("OUTPUT", e1, handle);
	
	iptc_free(handle);

	return 0;
}
