#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "sw842.h"

int main()
{
	int ret;
	const unsigned char *src = "This is a test module for sw842 compress! This is a test module for sw842 compress!";
	size_t slen = strlen(src);
	unsigned int tlen = 1024, dlen = 1024;
	unsigned char *wrkmem = malloc(SW842_MEM_COMPRESS);
	unsigned char *tmp = malloc(1024);
	unsigned char *dst = malloc(1024);

	if (!wrkmem || !tmp || !dst)
		return -1;
	
	ret = sw842_compress(src, slen, tmp, &tlen, wrkmem);
	if (ret) {
		printf("sw842_compress error %d!\n", ret);
		return -1;
	}

	ret = sw842_decompress(tmp, tlen, dst, &dlen);
	if (ret) {
		printf("sw842_decompress error %d!\n", ret);
		return -1;
	}

	printf("dst: %s, slen: %d, tlen: %d, dlen: %d\n", dst, (int)slen, (int)tlen, (int)dlen);

	free(dst);
	free(tmp);
	free(wrkmem);

	return 0;
}
