/* SPDX-License-Identifier: GPL-2.0 */
#ifndef __SW842_H__
#define __SW842_H__

#include <linux/types.h>

#define SW842_MEM_COMPRESS	(0xf000)

int sw842_compress(const __u8 *src, unsigned int srclen,
		   __u8 *dst, unsigned int *destlen, void *wmem);

int sw842_decompress(const __u8 *src, unsigned int srclen,
		     __u8 *dst, unsigned int *destlen);

#endif
