#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <libaudit.h>

char buf[1024];
int audit_send(int fd, int type, const void *data, unsigned int size);

static void print_rule(struct audit_rule_data *r)
{
	int i;
	size_t boffset = 0;

	printf("action=%s\n", audit_action_to_name(r->action));
	printf("flags=%s\n", audit_flag_to_name(r->flags));
	printf("field_count=%d\n", r->field_count);

	for (i = 0; i < r->field_count; i++) {
		const char *name;
		int op = r->fieldflags[i] & AUDIT_OPERATORS;
		int field = r->fields[i] & ~AUDIT_OPERATORS;
		name = audit_field_to_name(field);
		if (name) {
			if (field == AUDIT_MSGTYPE) {
				//
			} else if (field == AUDIT_DIR) {
				printf("-w %.*s", r->values[i], &r->buf[boffset]);
				boffset += r->values[i];
			} else if (field == AUDIT_PERM) {
				char perms[5];
				int val = r->values[i];
				perms[0] = 0;
				if (val & AUDIT_PERM_READ)
					strcat(perms, "r");
				if (val & AUDIT_PERM_WRITE)
					strcat(perms, "w");
				if (val & AUDIT_PERM_EXEC)
					strcat(perms, "x");
				if (val & AUDIT_PERM_ATTR)
					strcat(perms, "a");
				printf(" -p %s", perms);
			}
		}
	}
	printf("\n");
}

static int audit_print_reply(struct audit_reply *rep, int fd)
{
	struct audit_rule_data *r = (struct audit_rule_data *)buf;
	switch (rep->type) {
		case NLMSG_NOOP:
			printf("NLMSG_NOOP\n");
			return 1;
		case NLMSG_DONE:
			printf("NLMSG_DONE\n");
			break;
		case NLMSG_ERROR:
			printf("NLMSG_ERROR\n");
			break;
		case AUDIT_GET:
			printf("AUDIT_GET\n");
			break;
		case AUDIT_GET_FEATURE:
			printf("AUDIT_GET_FEATURE");
			break;
		case AUDIT_LIST_RULES:
			printf("AUDIT_LIST_RULES, rep->ruledata->buflen=%d\n",
			       rep->ruledata->buflen);
			memcpy(buf, rep->ruledata,
			       sizeof(struct audit_rule_data) + rep->ruledata->buflen);
			print_rule(r);
			return 1;
		default:
			printf("Unknown: type=%d, len=%d\n",
			       rep->type, rep->nlh->nlmsg_len);
			break;
	}
	return 0;
}

static void get_reply(int fd)
{
        int i, retval;
        int timeout = 40; /* loop has delay of .1 - so this is 4 seconds */
        struct audit_reply rep;
        fd_set read_mask;
        FD_ZERO(&read_mask);
        FD_SET(fd, &read_mask);

        for (i = 0; i < timeout; i++) {
                struct timeval t;

                t.tv_sec  = 0;
                t.tv_usec = 100000; /* .1 second */
                do {
                        retval = select(fd+1, &read_mask, NULL, NULL, &t);
                } while (retval < 0 && errno == EINTR); 
                // We'll try to read just in case
                retval = audit_get_reply(fd, &rep, GET_REPLY_NONBLOCKING, 0);
                if (retval > 0) {
                        if (rep.type == NLMSG_ERROR && rep.error->error == 0) {
                                i = 0;    /* reset timeout */
                                continue; /* This was an ack */
                        }

                        if ((retval = audit_print_reply(&rep, fd)) == 0)
                                break;
                        else
                                i = 0; /* If getting more, reset timeout */
                }
        }
}

int main(int argc, char *argv[])
{
	int fd;
	int rc;

	if ((fd = audit_open()) < 0) {
		printf("Cannot open netlink audit socket\n");
		return -1;
	}

	rc = audit_send(fd, AUDIT_LIST_RULES, NULL, 0);
	if (rc < 0 && rc != -EINVAL) {
		printf("Error sending rule list data request (%s)\n", strerror(-rc));
		return -1;
	}

	get_reply(fd);
	audit_close(fd);

	return 0;
}
