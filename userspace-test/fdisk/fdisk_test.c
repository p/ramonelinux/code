#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <libfdisk/libfdisk.h>

int main(int argc, char *argv[])
{
	struct fdisk_context *cxt = NULL;
	struct fdisk_table *tb = NULL;
	struct fdisk_partition *pa = NULL;
	struct fdisk_iter *itr = NULL;

	if (argc != 2)
		return EXIT_FAILURE;

	cxt = fdisk_new_context();
	if (!cxt)
		printf("failed to allocate libfdisk context\n");

	if (fdisk_assign_device(cxt, argv[1], 1) != 0) {
		printf("cannot open %s\n", argv[1]);
		return EXIT_FAILURE;
	}


	uint64_t bytes = fdisk_get_nsectors(cxt) * fdisk_get_sector_size(cxt);
	printf("Disk: %s, %ju bytes, %ju sectors\n",
		fdisk_get_devname(cxt), bytes, (uintmax_t)fdisk_get_nsectors(cxt));

	if (fdisk_get_partitions(cxt, &tb) || fdisk_table_get_nents(tb) <= 0)
		return EXIT_FAILURE;

	itr = fdisk_new_iter(FDISK_ITER_FORWARD);
	if (!itr)
		return EXIT_FAILURE;

	printf("name\tpartno\tsize\n");
	while (fdisk_table_next_partition(tb, itr, &pa) == 0) {
		const char *name = fdisk_partition_get_name(pa);
		size_t partno = fdisk_partition_get_partno(pa);
		uint64_t size = fdisk_partition_get_size(pa);
		printf("%s\t%d\t%ld\n", name, partno, size);
	}

	return EXIT_SUCCESS;
}
