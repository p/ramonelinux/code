#include <unistd.h>

int main()
{
	const char *path = "/bin/sh";
	char *const argv[] = { "sh", "-c", "for d in `ls`; do echo $d; done", NULL };
	execv(path, argv);
}
