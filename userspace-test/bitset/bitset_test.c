#include <stdio.h>

void w_bitset(__uint64_t *remove_addrs)
{
	int i;

	__uint8_t *p = (__uint8_t *)remove_addrs;

	for (i = 0; i < 4; i++) {
		*p++ = (100 + i);
	}
}

void r_bitset(__uint64_t *remove_addrs)
{
	int i;
	__uint8_t *p = (__uint8_t *)remove_addrs;

	for (i = 0; i < 8; i++, p++) {
		if (*p)
			printf("id[%d]=%d\n", i, *p);
	}
}
int main()
{
	__uint64_t remove_addrs;

	w_bitset(&remove_addrs);

	r_bitset(&remove_addrs);
}
