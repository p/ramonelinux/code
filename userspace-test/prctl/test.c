#include <stdio.h>
#include <sys/prctl.h>

int main()
{
	int ret;

	ret = prctl(PR_GET_DUMPABLE, 0, 0, 0, 0);
	printf("%d\n", ret);

	return 0;
}
