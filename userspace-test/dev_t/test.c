#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <linux/kdev_t.h>

dev_t get_device(char *path)
{
	struct stat st;

	if (stat(path, &st)) {
		return 0;
	}

	return st.st_dev;
}

int main()
{
	dev_t dev1 = MKDEV(8, 1);
	printf("dev1=%ld (%ld, %ld)\n", dev1, MAJOR(dev1), MINOR(dev1));

	dev_t dev2 = get_device("/dev/sda1");
	printf("dev2=%lu (%ld, %ld)\n", dev2, MAJOR(dev2), MINOR(dev2));

	return 0;
}
