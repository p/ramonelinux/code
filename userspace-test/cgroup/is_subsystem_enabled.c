#include <stdio.h>
#include <string.h>
#include <sys/param.h> /* MAX()/MIN() */

/**
 * The function tests if the given controller is enabled in kernel
 * @param name the name of the controller to be checked
 * @param exist set to 1 if the controller exists
 */
void is_subsystem_enabled(const char *name, int *exist)
{
        int hierarchy, num_cgroups, enabled;
        FILE *fd;
        char subsys_name[FILENAME_MAX];
	int len;

        fd = fopen("/proc/cgroups", "r");
        if (!fd)
                return;

        while (!feof(fd)) {
                fscanf(fd, "%s\t%d\t%d\t%d", subsys_name,
                                         &hierarchy, &num_cgroups, &enabled);
		//printf("%s %d %d %d\n", subsys_name, hierarchy, num_cgroups, enabled);
		len = MAX(strlen(name), strlen(subsys_name));
                if (strncmp(name, subsys_name, len) == 0) {
			//printf("%s %s matched.\n", name, subsys_name);
                        *exist = 1;
			break;
		}
        }

        fclose(fd);
}

int main(int argc, char *argv[])
{
	int exist = 0;

	if (argc != 2)
		return -1;

	is_subsystem_enabled(argv[1], &exist);
	printf("exist: %d\n", exist);

	return 0;
}
