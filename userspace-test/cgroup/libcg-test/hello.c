#include <stdio.h>
#include <stdlib.h>
#include <libcgroup.h>

int main(int argc, char *argv[])
{
	int ret;
	struct cgroup *cg;
	struct cgroup_controller *cpuset;
	pid_t pid;
	
	if (argc != 3)
		return;

	pid = atoi(argv[1]);
       
	ret = cgroup_init();
	if (ret) {
		perror("cgroup_init");
		return EXIT_FAILURE;
	}

	cg = cgroup_new_cgroup("hello");
	if (!cg) {
		perror("cgroup_new_cgroup");
		return EXIT_FAILURE;
	}

	cpuset = cgroup_add_controller(cg, "cpuset");
	if (!cpuset) {
		perror("cgroup_add_controller");
		return EXIT_FAILURE;
	}

	if (cgroup_add_value_string(cpuset, "cpuset.cpus", argv[2])) {
		perror("cgroup_add_value_string cpus");
		return EXIT_FAILURE;
	}
	if (cgroup_add_value_string(cpuset, "cpuset.mems", "0")) {
		perror("cgroup_add_value_string mems");
		return EXIT_FAILURE;
	}

	ret = cgroup_create_cgroup(cg, 0);
	if (ret) {
		perror("cgroup_create_cgroup");
		return EXIT_FAILURE;
	}

	ret = cgroup_attach_task_pid(cg, pid);
	if (ret) {
		perror("cgroup_attach_task_pid");
		return EXIT_FAILURE;
	}

	getchar();

	cgroup_delete_cgroup(cg, 0);

	return 0;
}
