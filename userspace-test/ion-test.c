#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <string.h>
#include "/home/ram/linux-3.18/drivers/staging/android/uapi/ion.h"

#define ION_HEAP(bit) (1 << (bit))
#define ION_BUFFER_LENGTH 4096

int main()
{
	struct ion_allocation_data alloc;
	struct ion_fd_data ifd_data;
	int rc;
	unsigned char *v_addr;

	int ion_fd = open("/dev/ion", O_RDONLY | O_SYNC);
	if (ion_fd < 0) {
		perror("open error");
		return -1;
	}
	alloc.len = ION_BUFFER_LENGTH;
	alloc.align = 4096;
	alloc.heap_id_mask = ION_HEAP(ION_HEAP_TYPE_SYSTEM);
	alloc.flags = 0;
	rc = ioctl(ion_fd, ION_IOC_ALLOC, &alloc);
	if (rc) {
		perror("ioctl alloc error");
		return -1;
	}
	if (alloc.handle != 0) {
		ifd_data.handle = alloc.handle;
	} else {
		printf("handle error\n");
		return -1;
	}

	rc = ioctl(ion_fd, ION_IOC_MAP, &ifd_data);
	if (rc) {
		perror("map error");
		return -1;
	}

	/* Make the ion mmap call */
	v_addr = (unsigned char *)mmap(NULL, alloc.len,
			PROT_READ | PROT_WRITE,
			MAP_SHARED, ifd_data.fd, 0);
	if (v_addr == MAP_FAILED) {
		perror("mmap error");
		return -1;
	}

	int i;
	for (i = 0; i < ION_BUFFER_LENGTH; i++) {
		*(v_addr + i) = (char)(i % 255);
	}

	char *tmp = malloc(ION_BUFFER_LENGTH);
	memcpy(tmp, v_addr, ION_BUFFER_LENGTH);
	printf("tmp: (%s)\n", tmp);

	getchar();

	close(ion_fd);
	
	return 0;
}
