/*
 * lsattr.c		- List file attributes on an ext2 file system
 *
 * Copyright (C) 1993, 1994  Remy Card <card@masi.ibp.fr>
 *                           Laboratoire MASI, Institut Blaise Pascal
 *                           Universite Pierre et Marie Curie (Paris VI)
 */
#include <sys/types.h>
#include <dirent.h>
#include <errno.h>
#include <fcntl.h>
#include <getopt.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/param.h>
#include <sys/stat.h>

#include "et/com_err.h"
#include "e2p/e2p.h"

static const char *program_name = "lsattr";

static int all;
static int dirs_opt;
static unsigned pf_options;
static int recursive;
static int verbose;
static int generation_opt;
static int project_opt;

static void usage(void)
{
	fprintf(stderr, "Usage: %s [-RVadlpv] [files...]\n", program_name);
	exit(1);
}

static int list_attributes(const char *name)
{
	unsigned long flags;
	unsigned long generation;
	unsigned long project;

	if (fgetflags(name, &flags) == -1) {
		com_err(program_name, errno, "While reading flags on %s",
			name);
		return -1;
	}
	if (project_opt) {
		if (fgetproject(name, &project) == -1) {
			com_err(program_name, errno,
				"While reading project on %s",
				name);
			return -1;
		}
		printf("%5lu ", project);
	}
	if (generation_opt) {
		if (fgetversion(name, &generation) == -1) {
			com_err(program_name, errno,
				"While reading version on %s",
				name);
			return -1;
		}
		printf("%-10lu ", generation);
	}
	if (pf_options & PFOPT_LONG) {
		printf("%-28s ", name);
		print_flags(stdout, flags, pf_options);
		fputc('\n', stdout);
	} else {
		print_flags(stdout, flags, pf_options);
		printf(" %s\n", name);
	}
	return 0;
}

static int lsattr_dir_proc(const char *dir_name, struct dirent *de,
			   void *unused)
{
	struct stat st;
	char *path;
	int dir_len = strlen(dir_name);

	path = malloc(dir_len + strlen(de->d_name) + 2);

	if (dir_len && dir_name[dir_len-1] == '/')
		sprintf(path, "%s%s", dir_name, de->d_name);
	else
		sprintf(path, "%s/%s", dir_name, de->d_name);
	if (lstat(path, &st) == -1)
		perror(path);
	else {
		if (de->d_name[0] != '.' || all) {
			list_attributes(path);
			if (S_ISDIR(st.st_mode) && recursive &&
			    strcmp(de->d_name, ".") &&
			    strcmp(de->d_name, "..")) {
				printf("\n%s:\n", path);
				iterate_on_dir(path, lsattr_dir_proc, NULL);
				printf("\n");
			}
		}
	}
	free(path);
	return 0;
}

static int lsattr_args(const char * name)
{
	struct stat st;
	int retval = 0;

	if (lstat(name, &st) == -1) {
		com_err(program_name, errno, "while trying to stat %s",
			name);
		retval = -1;
	} else {
		if (S_ISDIR(st.st_mode) && !dirs_opt)
			retval = iterate_on_dir(name, lsattr_dir_proc, NULL);
		else
			retval = list_attributes(name);
	}
	return retval;
}

int main(int argc, char *argv[])
{
	int c;
	int i;
	int err, retval = 0;

	if (argc && *argv)
		program_name = *argv;
	while ((c = getopt(argc, argv, "RVadlvp")) != EOF)
		switch (c)
		{
			case 'R':
				recursive = 1;
				break;
			case 'V':
				verbose = 1;
				break;
			case 'a':
				all = 1;
				break;
			case 'd':
				dirs_opt = 1;
				break;
			case 'l':
				pf_options = PFOPT_LONG;
				break;
			case 'v':
				generation_opt = 1;
				break;
			case 'p':
				project_opt = 1;
				break;
			default:
				usage();
		}

	if (optind > argc - 1) {
		if (lsattr_args(".") == -1)
			retval = 1;
	} else {
		for (i = optind; i < argc; i++) {
			err = lsattr_args(argv[i]);
			if (err)
				retval = 1;
		}
	}
	exit(retval);
}
