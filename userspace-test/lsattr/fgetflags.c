#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/ioctl.h>

#define FS_IOC_GETFLAGS	_IOR('f', 1, long)

int fgetflags(const char *name, unsigned long *flags)
{
	struct stat buf;
	int fd, r, f;

	if (!lstat(name, &buf) &&
	    !S_ISREG(buf.st_mode) && !S_ISDIR(buf.st_mode)) {
		return -1;
	}
	fd = open(name, O_RDONLY | O_NONBLOCK);
	if (fd == -1)
		return -1;
	r = ioctl(fd, FS_IOC_GETFLAGS, &f);
	if (r == -1)
		return -1;
	*flags = f;
	close(fd);
	return r;
}

int main(int argc, char *argv[])
{
	unsigned long f;
	int r;

	r = fgetflags(argv[1], &f);
	if (r == -1)
		perror("fgetflags");
	else
		printf("f=%ld\n", f);

	return 0;
}
