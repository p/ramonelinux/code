#include <stdio.h>
#include <linux/seccomp.h>
#include <linux/filter.h>
#include <linux/audit.h>
#include <linux/signal.h>
#include <sys/ptrace.h>
#include <sys/prctl.h>

int main()
{
	int ret;

	ret = prctl(PR_GET_SECCOMP, 0, 0, 0, 0);
	printf("%d\n", ret);

	unsigned int operation;
	unsigned int flags;
	void *args;
	ret = seccomp(operation, flags, args);

	return 0;
}
