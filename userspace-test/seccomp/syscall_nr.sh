#!/bin/bash

syscall_nr() {
	cat ~/linux-next/arch/x86/entry/syscalls/syscall_64.tbl | \
	awk '$2 != "x32" && $3 == "'$1'" { print $1 }'
}

syscall_nr execve
syscall_nr seccomp
