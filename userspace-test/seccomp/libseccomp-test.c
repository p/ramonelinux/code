#include <stdlib.h>
#include <seccomp.h>

int main(int argc, char *argv[])
{
     int rc = -1;
     scmp_filter_ctx ctx;

     ctx = seccomp_init(SCMP_ACT_KILL);
     if (ctx == NULL)
          goto out;

     /* ... */

     rc = seccomp_reset(ctx, SCMP_ACT_KILL);
     if (rc < 0)
          goto out;

     /* ... */

out:
     seccomp_release(ctx);
     return -rc;
}
