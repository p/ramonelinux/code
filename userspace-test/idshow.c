#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main(int argc, char *argv[])
{
	uid_t ruid, euid, suid, fsuid;
	gid_t rgid, egid, sgid, fsgid;

	if (getresuid(&ruid, &euid, &suid) == -1)
		perror("getresuid: ");
	printf("UID real=%ld, eff=%ld, saved=%ld\n", ruid, euid, suid);
	if (getresgid(&rgid, &egid, &sgid) == -1)
		perror("getresuid: ");
	printf("GID real=%ld, eff=%ld, saved=%ld\n", rgid, egid, sgid);

	ruid = 1001;
	euid = 1002;
	if (setreuid(ruid, euid) == -1)
		perror("setreuid: ");

	if (getresuid(&ruid, &euid, &suid) == -1)
		perror("getresuid: ");
	printf("UID real=%ld, eff=%ld, saved=%ld\n", ruid, euid, suid);

	exit(EXIT_SUCCESS);
}
