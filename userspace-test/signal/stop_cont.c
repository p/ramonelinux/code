#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <signal.h>

int main(int argc, char *argv[])
{
	pid_t pid;
	int on;
	int ret;

	if (argc != 3) {
		printf("Usage: %s pid on\n", argv[0]);
		return -1;
	}

	pid = atoi(argv[1]);
	on = atoi(argv[2]);
	if (on == 1) {
		ret = kill(pid, SIGCONT);
		if (ret)
			perror("kill sigcont");
	} else if (on == 0) {
		ret = kill(pid, SIGSTOP);
		if (ret)
			perror("kill sigstop");
	}

	return 0;
}
