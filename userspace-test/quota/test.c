#include <sys/quota.h>
#include <stdio.h>
#include <stdlib.h>

int main()
{
	char *dev = "/dev/mapper/ubuntu--vg-root";
	int uid = 1000;
	int type;
	struct dqblk dq;
	int ret = quotactl(QCMD(Q_GETQUOTA, USRQUOTA), dev, uid, (void *)&dq);
	if (ret) {
		perror("quotactl");
		return EXIT_FAILURE;
	}

	printf("uid: %d, hard: %ld, soft: %ld, curspace: %ld\n",
		uid, dq.dqb_bhardlimit, dq.dqb_bsoftlimit, dq.dqb_curspace);

	return 0;
}
