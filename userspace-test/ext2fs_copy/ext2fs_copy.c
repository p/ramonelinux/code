#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <ext2fs/ext2fs.h>

static errcode_t init_fs(const char* fsname, ext2_filsys *ret_fs)
{
	int flags = 0;
	int superblock = 0;
	unsigned int block_size = 0;
	io_manager manager = unix_io_manager;

	flags |= EXT2_FLAG_RW;

	return ext2fs_open(fsname, flags, superblock, block_size, manager, ret_fs);
}

static errcode_t read_block64(ext2_filsys fs, blk64_t *blocknr, e2_blkcnt_t blockcnt,
			      blk_t ref_blk, int ref_offset, void *buf)
{
	errcode_t retval;

	if(*blocknr > UINT32_MAX)
		return EDOM;
	retval = io_channel_read_blk(fs->io, *blocknr, 1,
			(char *)buf + EXT2_BLOCK_SIZE(fs->super) * blockcnt);

	return retval;
}

static int ino_to_block(ext2_filsys fs, ext2_ino_t ino, char *filename)
{
	struct ext2_inode *inode;
	errcode_t errcode;

	inode = (struct ext2_inode *)malloc(EXT2_INODE_SIZE(fs->super));
	errcode = ext2fs_read_inode(fs, ino, inode);
	if (errcode) {
		return -1;
	}

	char *buf = (char *)malloc(EXT2_BLOCK_SIZE(fs->super));
	size_t buf_size;
	FILE *stream = fopen(filename, "wb");
	int i;
	blk64_t blknum;
	__u32 rest = inode->i_size;
	// 4096(logic block size) / 512(sector size) = 8
	for (i = 0; i < inode->i_blocks / 8; i++) {
		errcode = ext2fs_bmap2(fs, ino, inode, NULL, 0, i, NULL, &blknum);
		if (errcode) {
			return -1;
		}
		printf("%d blknum: %llu\n", i, blknum);
		errcode = read_block64(fs, &blknum, 0, 0, 0, buf);
		if (errcode) {
			return -1;
		}

		if (rest > EXT2_BLOCK_SIZE(fs->super))
			buf_size = EXT2_BLOCK_SIZE(fs->super);
		else
			buf_size = rest;

		fwrite(buf, buf_size, 1, stream);
		rest -= buf_size;
	}

	fclose(stream);
	free(buf);

	return 0;
}

struct match_struct {
	ext2_ino_t ret_ino;
	char *curr_name;
	ext2_filsys fs;
};

static int match_name(struct ext2_dir_entry *dirent, int off, int blksize,
		      char *buf, void *priv)
{
	char *curr_name = strdup(((struct match_struct *) priv)->curr_name);
	const struct ext2_dir_entry_2 *curr_ent = (const struct ext2_dir_entry_2 *)dirent;

	if (!strncmp(curr_name, curr_ent->name, strlen(curr_name))) {
		((struct match_struct *) priv)->ret_ino = dirent->inode;
	}
	return 0;
}

static ext2_ino_t name_to_ino(ext2_filsys fs, ext2_ino_t ino, const char *filename)
{
	struct match_struct tmp;
	tmp.ret_ino = 0;
	tmp.curr_name = strdup(filename);
	tmp.fs = fs;
	struct match_struct *priv = &tmp;

	ext2fs_dir_iterate(fs, ino, 0, NULL, match_name, priv);
	return priv->ret_ino;
}

int main(int argc, char* argv[])
{
	ext2_filsys fs;
	errcode_t errcode;
	ext2_ino_t ino = EXT2_ROOT_INO;
	char filename[FILENAME_MAX];

	if (argc != 3) {
		printf("Usage: %s filesystem_device file_path\n", argv[0]);
		return -1;
	}

	errcode = init_fs(argv[1], &fs);
	if (errcode) {
		perror("Failed open filesystem");
		return -1;
	}

	char *p = strtok(argv[2], "/");
	do {
		ino = name_to_ino(fs, ino, p);
		bzero(filename, sizeof(filename));
		strcpy(filename, p);
		printf("ino: %u, filename: %s\n", ino, filename);
	} while (p = strtok(NULL, "/"));

	ino_to_block(fs, ino, filename);

	ext2fs_close(fs);

	return 0;
}
