#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <limits.h>
#include <errno.h>
#include <ctype.h>
#include <dirent.h>
#include <inttypes.h>
#include <pthread.h>
#include <unistd.h>
#include <stdbool.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mount.h>
#include <sys/uio.h>
#include <sys/errno.h>
#include <sys/inotify.h>
#include <sys/param.h>
#include <sys/resource.h>
#include <sys/stat.h>
#include <sys/statfs.h>
#include <sys/time.h>
#include <linux/fuse.h>
#include <linux/types.h>
#include <linux/fs.h>

#define FUSE_TRACE 0

#if FUSE_TRACE
#define TRACE(x...) printf(x)
#else
#define TRACE(x...) do {} while (0)
#endif

#define ERROR(x...) printf(x)

/* Maximum number of bytes to write in one request. */
#define MAX_WRITE (256 * 1024)

/* Maximum number of bytes to read in one request. */
#define MAX_READ (128 * 1024)

#define PAGESIZE 4096

/* Pseudo-error constant used to indicate that no fuse status is needed
 * or that a reply has already been written. */
#define NO_STATUS 1

#define FUSE_UNKNOWN_INO 0xffffffff

/* Largest possible request.
 * The request size is bounded by the maximum size of a FUSE_WRITE request because it has
 * the largest possible data payload. */
#define MAX_REQUEST_SIZE (sizeof(struct fuse_in_header) + sizeof(struct fuse_write_in) + MAX_WRITE)

struct handle {
	int fd;
};

struct dirhandle {
	DIR *d;
};

struct node {
	__u32 refcount;
	__u64 nid;
	__u64 gen;
	/*
	 * The inode number for this FUSE node. Note that this isn't stable across
	 * multiple invocations of the FUSE daemon.
	 */
	__u32 ino;

	/* State derived based on current position in hierarchy. */
	uid_t uid;

	struct node *next;          /* per-dir sibling list */
	struct node *child;         /* first contained file by this dir */
	struct node *parent;        /* containing directory */

	size_t namelen;
	char *name;
	/* If non-null, this is the real name of the file in the underlying storage.
	 * This may differ from the field "name" only by case.
	 * strlen(actual_name) will always equal strlen(name), so it is safe to use
	 * namelen for both fields.
	 */
	char *actual_name;

	/* If non-null, an exact underlying path that should be grafted into this
	 * position. Used to support things like OBB. */
	char* graft_path;
	size_t graft_pathlen;

	bool deleted;
};

/* Global data for all FUSE mounts */
struct fuse_global {
	pthread_mutex_t lock;

	uid_t uid;
	gid_t gid;
	bool multi_user;

	char source_path[PATH_MAX];

	__u64 next_generation;
	struct node root;

	/* Used to allocate unique inode numbers for fuse nodes. We use
	 * a simple counter based scheme where inode numbers from deleted
	 * nodes aren't reused. Note that inode allocations are not stable
	 * across multiple invocation of the sdcard daemon, but that shouldn't
	 * be a huge problem in practice.
	 *
	 * Note that we restrict inodes to 32 bit unsigned integers to prevent
	 * truncation on 32 bit processes when unsigned long long stat.st_ino is
	 * assigned to an unsigned long ino_t type in an LP32 process.
	 *
	 * Also note that fuse_attr and fuse_dirent inode values are 64 bits wide
	 * on both LP32 and LP64, but the fuse kernel code doesn't squash 64 bit
	 * inode numbers into 32 bit values on 64 bit kernels (see fuse_squash_ino
	 * in fs/fuse/inode.c).
	 *
	 * Accesses must be guarded by |lock|.
	 */
	__u32 inode_ctr;

	struct fuse* fuse_default;
};

struct fuse {
	struct fuse_global* global;

	char dest_path[PATH_MAX];
	int fd;
	gid_t gid;
	mode_t mask;
};

struct fuse_handler {
	struct fuse* fuse;
	int token;

	/* To save memory, we never use the contents of the request buffer and the read
	 * buffer at the same time.  This allows us to share the underlying storage. */
	union {
		__u8 request_buffer[MAX_REQUEST_SIZE];
		__u8 read_buffer[MAX_READ + PAGESIZE];
	};
};

static inline void *id_to_ptr(__u64 nid)
{
	return (void *) (uintptr_t) nid;
}

static inline __u64 ptr_to_id(void *ptr)
{
	return (__u64) (uintptr_t) ptr;
}

static void acquire_node_locked(struct node* node)
{
	node->refcount++;
	TRACE("ACQUIRE %p (%s) rc=%d\n", node, node->name, node->refcount);
}

static void remove_node_from_parent_locked(struct node* node);

static void release_node_locked(struct node* node)
{
	TRACE("RELEASE %p (%s) rc=%d\n", node, node->name, node->refcount);
	if (node->refcount > 0) {
		node->refcount--;
		if (!node->refcount) {
			TRACE("DESTROY %p (%s)\n", node, node->name);
			remove_node_from_parent_locked(node);

			/* TODO: remove debugging - poison memory */
			memset(node->name, 0xef, node->namelen);
			free(node->name);
			free(node->actual_name);
			memset(node, 0xfc, sizeof(*node));
			free(node);
		}
	} else {
		ERROR("Zero refcnt %p\n", node);
	}
}

static void add_node_to_parent_locked(struct node *node, struct node *parent) {
	node->parent = parent;
	node->next = parent->child;
	parent->child = node;
	acquire_node_locked(parent);
}

static void remove_node_from_parent_locked(struct node* node)
{
	if (node->parent) {
		if (node->parent->child == node) {
			node->parent->child = node->parent->child->next;
		} else {
			struct node *node2;
			node2 = node->parent->child;
			while (node2->next != node)
				node2 = node2->next;
			node2->next = node->next;
		}
		release_node_locked(node->parent);
		node->parent = NULL;
		node->next = NULL;
	}
}

/* Finds the absolute path of a file within a given directory.
 * Performs a case-insensitive search for the file and sets the buffer to the path
 * of the first matching file.  If 'search' is zero or if no match is found, sets
 * the buffer to the path that the file would have, assuming the name were case-sensitive.
 *
 * Populates 'buf' with the path and returns the actual name (within 'buf') on success,
 * or returns NULL if the path is too long for the provided buffer.
 */
static char* find_file_within(const char* path, const char* name,
		char* buf, size_t bufsize, int search)
{
	size_t pathlen = strlen(path);
	size_t namelen = strlen(name);
	size_t childlen = pathlen + namelen + 1;
	char* actual;

	if (bufsize <= childlen) {
		return NULL;
	}

	memcpy(buf, path, pathlen);
	buf[pathlen] = '/';
	actual = buf + pathlen + 1;
	memcpy(actual, name, namelen + 1);

	if (search && access(buf, F_OK)) {
		struct dirent* entry;
		DIR* dir = opendir(path);
		if (!dir) {
			ERROR("opendir %s failed: %s\n", path, strerror(errno));
			return actual;
		}
		while ((entry = readdir(dir))) {
			if (!strcasecmp(entry->d_name, name)) {
				/* we have a match - replace the name, don't need to copy the null again */
				memcpy(actual, entry->d_name, namelen);
				break;
			}
		}
		closedir(dir);
	}
	return actual;
}

static void attr_from_stat(struct fuse* fuse, struct fuse_attr *attr,
		const struct stat *s, const struct node* node) {
	attr->ino = node->ino;
	attr->size = s->st_size;
	attr->blocks = s->st_blocks;
	attr->atime = s->st_atim.tv_sec;
	attr->mtime = s->st_mtim.tv_sec;
	attr->ctime = s->st_ctim.tv_sec;
	attr->atimensec = s->st_atim.tv_nsec;
	attr->mtimensec = s->st_mtim.tv_nsec;
	attr->ctimensec = s->st_ctim.tv_nsec;
	attr->mode = s->st_mode;
	attr->nlink = s->st_nlink;

	attr->uid = node->uid;

	int visible_mode = 0775 & ~fuse->mask;
	int owner_mode = s->st_mode & 0700;
	int filtered_mode = visible_mode & (owner_mode | (owner_mode >> 3) | (owner_mode >> 6));
	attr->mode = (attr->mode & S_IFMT) | filtered_mode;
}

struct node *create_node_locked(struct fuse* fuse,
		struct node *parent, const char *name, const char* actual_name)
{
	struct node *node;
	size_t namelen = strlen(name);

	// Detect overflows in the inode counter. "4 billion nodes should be enough
	// for everybody".
	if (fuse->global->inode_ctr == 0) {
		ERROR("No more inode numbers available\n");
		return NULL;
	}

	node = calloc(1, sizeof(struct node));
	if (!node) {
		return NULL;
	}
	node->name = malloc(namelen + 1);
	if (!node->name) {
		free(node);
		return NULL;
	}
	memcpy(node->name, name, namelen + 1);
	if (strcmp(name, actual_name)) {
		node->actual_name = malloc(namelen + 1);
		if (!node->actual_name) {
			free(node->name);
			free(node);
			return NULL;
		}
		memcpy(node->actual_name, actual_name, namelen + 1);
	}
	node->namelen = namelen;
	node->nid = ptr_to_id(node);
	node->ino = fuse->global->inode_ctr++;
	node->gen = fuse->global->next_generation++;

	node->deleted = false;

	acquire_node_locked(node);
	add_node_to_parent_locked(node, parent);
	return node;
}

static struct node *lookup_node_by_id_locked(struct fuse *fuse, __u64 nid)
{
	if (nid == FUSE_ROOT_ID) {
		return &fuse->global->root;
	} else {
		return id_to_ptr(nid);
	}
}

/* Gets the absolute path to a node into the provided buffer.
 *
 * Populates 'buf' with the path and returns the length of the path on success,
 * or returns -1 if the path is too long for the provided buffer.
 */
static ssize_t get_node_path_locked(struct node* node, char* buf, size_t bufsize) {
	const char* name;
	size_t namelen;
	if (node->graft_path) {
		name = node->graft_path;
		namelen = node->graft_pathlen;
	} else if (node->actual_name) {
		name = node->actual_name;
		namelen = node->namelen;
	} else {
		name = node->name;
		namelen = node->namelen;
	}

	if (bufsize < namelen + 1) {
		return -1;
	}

	ssize_t pathlen = 0;
	if (node->parent && node->graft_path == NULL) {
		pathlen = get_node_path_locked(node->parent, buf, bufsize - namelen - 2);
		if (pathlen < 0) {
			return -1;
		}
		buf[pathlen++] = '/';
	}

	memcpy(buf + pathlen, name, namelen + 1); /* include trailing \0 */
	return pathlen + namelen;
}

static struct node* lookup_node_and_path_by_id_locked(struct fuse* fuse, __u64 nid,
		char* buf, size_t bufsize)
{
	struct node* node = lookup_node_by_id_locked(fuse, nid);
	if (node && get_node_path_locked(node, buf, bufsize) < 0) {
		node = NULL;
	}
	return node;
}

static struct node *lookup_child_by_name_locked(struct node *node, const char *name)
{
	for (node = node->child; node; node = node->next) {
		/* use exact string comparison, nodes that differ by case
		 * must be considered distinct even if they refer to the same
		 * underlying file as otherwise operations such as "mv x x"
		 * will not work because the source and target nodes are the same. */
		if (!strcmp(name, node->name) && !node->deleted) {
			return node;
		}
	}
	return 0;
}

static struct node* acquire_or_create_child_locked(
		struct fuse* fuse, struct node* parent,
		const char* name, const char* actual_name)
{
	struct node* child = lookup_child_by_name_locked(parent, name);
	if (child) {
		acquire_node_locked(child);
	} else {
		child = create_node_locked(fuse, parent, name, actual_name);
	}
	return child;
}

static void fuse_status(struct fuse *fuse, __u64 unique, int err)
{
	struct fuse_out_header hdr;
	hdr.len = sizeof(hdr);
	hdr.error = err;
	hdr.unique = unique;
	write(fuse->fd, &hdr, sizeof(hdr));
}

static void fuse_reply(struct fuse *fuse, __u64 unique, void *data, int len)
{
	struct fuse_out_header hdr;
	struct iovec vec[2];
	int res;

	hdr.len = len + sizeof(hdr);
	hdr.error = 0;
	hdr.unique = unique;

	vec[0].iov_base = &hdr;
	vec[0].iov_len = sizeof(hdr);
	vec[1].iov_base = data;
	vec[1].iov_len = len;

	res = writev(fuse->fd, vec, 2);
	if (res < 0) {
		ERROR("*** REPLY FAILED *** %d\n", errno);
	}
}

static int fuse_reply_entry(struct fuse* fuse, __u64 unique,
		struct node* parent, const char* name, const char* actual_name,
		const char* path)
{
	struct node* node;
	struct fuse_entry_out out;
	struct stat s;

	if (lstat(path, &s) < 0) {
		return -errno;
	}

	pthread_mutex_lock(&fuse->global->lock);
	node = acquire_or_create_child_locked(fuse, parent, name, actual_name);
	if (!node) {
		pthread_mutex_unlock(&fuse->global->lock);
		return -ENOMEM;
	}
	memset(&out, 0, sizeof(out));
	attr_from_stat(fuse, &out.attr, &s, node);
	out.attr_valid = 10;
	out.entry_valid = 10;
	out.nodeid = node->nid;
	out.generation = node->gen;
	pthread_mutex_unlock(&fuse->global->lock);
	fuse_reply(fuse, unique, &out, sizeof(out));
	return NO_STATUS;
}

static int fuse_reply_attr(struct fuse* fuse, __u64 unique, const struct node* node,
		const char* path)
{
	struct fuse_attr_out out;
	struct stat s;

	if (lstat(path, &s) < 0) {
		return -errno;
	}
	memset(&out, 0, sizeof(out));
	attr_from_stat(fuse, &out.attr, &s, node);
	out.attr_valid = 10;
	fuse_reply(fuse, unique, &out, sizeof(out));
	return NO_STATUS;
}

static int handle_lookup(struct fuse* fuse, struct fuse_handler* handler,
		const struct fuse_in_header *hdr, const char* name)
{
	struct node* parent_node;
	char parent_path[PATH_MAX];
	char child_path[PATH_MAX];
	const char* actual_name;

	pthread_mutex_lock(&fuse->global->lock);
	parent_node = lookup_node_and_path_by_id_locked(fuse, hdr->nodeid,
			parent_path, sizeof(parent_path));
	TRACE("[%d] LOOKUP %s @ %"PRIx64" (%s)\n", handler->token, name, hdr->nodeid,
			parent_node ? parent_node->name : "?");
	pthread_mutex_unlock(&fuse->global->lock);

	if (!parent_node || !(actual_name = find_file_within(parent_path, name,
					child_path, sizeof(child_path), 1))) {
		return -ENOENT;
	}

	return fuse_reply_entry(fuse, hdr->unique, parent_node, name, actual_name, child_path);
}

static int handle_getattr(struct fuse* fuse, struct fuse_handler* handler,
		const struct fuse_in_header *hdr, const struct fuse_getattr_in *req)
{
	struct node* node;
	char path[PATH_MAX];

	pthread_mutex_lock(&fuse->global->lock);
	node = lookup_node_and_path_by_id_locked(fuse, hdr->nodeid, path, sizeof(path));
	TRACE("[%d] GETATTR flags=%x fh=%lx @ %lx (%s)\n", handler->token,
			req->getattr_flags, req->fh, hdr->nodeid, node ? node->name : "?");
	pthread_mutex_unlock(&fuse->global->lock);

	if (!node) {
		return -ENOENT;
	}

	return fuse_reply_attr(fuse, hdr->unique, node, path);
}

static int handle_readlink(struct fuse* fuse, struct fuse_handler* handler,
		const struct fuse_in_header* hdr, const struct fuse_read_in* req)
{
	struct node* node;
	char path[PATH_MAX];
	int res;
	__u32 size = req->size;
	__u8 *buffer = (__u8 *) ((uintptr_t)(handler->read_buffer + PAGESIZE) & ~((uintptr_t)PAGESIZE-1));

	pthread_mutex_lock(&fuse->global->lock);
	node = lookup_node_and_path_by_id_locked(fuse, hdr->nodeid, path, sizeof(path));
	pthread_mutex_unlock(&fuse->global->lock);

	if (!node)
		return -errno;

	if (size < 0)
		return -errno;
	else if (size == 0)
		return 0;

	res = readlink(path, (char *)buffer, size);
	if (res < 0) {
		ERROR("Fail readlink res=%d path=%s size=%d\n", res, path, size);
		return -errno;
	}
	buffer[res] = '\0';

	fuse_reply(fuse, hdr->unique, buffer, res);
	return NO_STATUS;
}

/* TODO */
static int handle_symlink(struct fuse* fuse, struct fuse_handler* handler,
		const struct fuse_in_header* hdr, const char* from, const char* to)
{
	int res;
	struct node* parent_node;
	struct node* child_node;
	char parent_path[PATH_MAX];
	char child_path[PATH_MAX];

	pthread_mutex_lock(&fuse->global->lock);
	parent_node = lookup_node_and_path_by_id_locked(fuse, hdr->nodeid, parent_path, sizeof(parent_path));
	pthread_mutex_unlock(&fuse->global->lock);

	if (!parent_node || !find_file_within(parent_path, to,
				child_path, sizeof(child_path), 1)) {
		return -ENOENT;
	}

	pthread_mutex_lock(&fuse->global->lock);
	child_node = lookup_child_by_name_locked(parent_node, from);
	if (child_node) {
		ERROR("child_node->name: %s\n", child_node->name);
	}
	pthread_mutex_unlock(&fuse->global->lock);

	ERROR("[%d] SYMLINK %s->%s parent_path:%s child_path: %s\n", handler->token, from, to, parent_path, child_path);
	res = symlink(from, to);
	if (res < 0) {
		res = -errno;
		perror("symlink");
		goto io_error;
	}

io_error:
	return res;
}

static int handle_opendir(struct fuse* fuse, struct fuse_handler* handler,
		const struct fuse_in_header* hdr, const struct fuse_open_in* req)
{
	struct node* node;
	char path[PATH_MAX];
	struct fuse_open_out out;
	struct dirhandle *h;

	pthread_mutex_lock(&fuse->global->lock);
	node = lookup_node_and_path_by_id_locked(fuse, hdr->nodeid, path, sizeof(path));
	TRACE("[%d] OPENDIR @ %"PRIx64" (%s)\n", handler->token,
			hdr->nodeid, node ? node->name : "?");
	pthread_mutex_unlock(&fuse->global->lock);

	if (!node) {
		return -ENOENT;
	}
	h = malloc(sizeof(*h));
	if (!h) {
		return -ENOMEM;
	}
	TRACE("[%d] OPENDIR %s\n", handler->token, path);
	h->d = opendir(path);
	if (!h->d) {
		free(h);
		return -errno;
	}
	out.fh = ptr_to_id(h);
	out.open_flags = 0;
	out.padding = 0;

	fuse_reply(fuse, hdr->unique, &out, sizeof(out));
	return NO_STATUS;
}

static int handle_readdir(struct fuse* fuse, struct fuse_handler* handler,
		const struct fuse_in_header* hdr, const struct fuse_read_in* req)
{
	char buffer[8192];
	struct fuse_dirent *fde = (struct fuse_dirent*) buffer;
	struct dirent *de;
	struct dirhandle *h = id_to_ptr(req->fh);

	TRACE("[%d] READDIR %p\n", handler->token, h);
	if (req->offset == 0) {
		/* rewinddir() might have been called above us, so rewind here too */
		TRACE("[%d] calling rewinddir()\n", handler->token);
		rewinddir(h->d);
	}
	de = readdir(h->d);
	if (!de) {
		return 0;
	}
	fde->ino = FUSE_UNKNOWN_INO;
	/* increment the offset so we can detect when rewinddir() seeks back to the beginning */
	fde->off = req->offset + 1;
	fde->type = de->d_type;
	fde->namelen = strlen(de->d_name);
	memcpy(fde->name, de->d_name, fde->namelen + 1);
	fuse_reply(fuse, hdr->unique, fde,
		FUSE_DIRENT_ALIGN(sizeof(struct fuse_dirent) + fde->namelen));
	return NO_STATUS;
}

static int handle_releasedir(struct fuse* fuse, struct fuse_handler* handler,
		const struct fuse_in_header* hdr, const struct fuse_release_in* req)
{
	struct dirhandle *h = id_to_ptr(req->fh);

	TRACE("[%d] RELEASEDIR %p\n", handler->token, h);
	closedir(h->d);
	free(h);
	return 0;
}

static int handle_init(struct fuse* fuse, struct fuse_handler* handler,
		const struct fuse_in_header* hdr, const struct fuse_init_in* req) 
{
	struct fuse_init_out out; 
	size_t fuse_struct_size;

	TRACE("[%d] INIT ver=%d.%d maxread=%d flags=%x\n",
	       handler->token, req->major, req->minor, req->max_readahead, req->flags);

	/* Kernel 2.6.16 is the first stable kernel with struct fuse_init_out
	 * defined (fuse version 7.6). The structure is the same from 7.6 through
	 * 7.22. Beginning with 7.23, the structure increased in size and added
	 * new parameters.
	 */
	if (req->major != FUSE_KERNEL_VERSION || req->minor < 6) { 
		ERROR("Fuse kernel version mismatch: Kernel version %d.%d, Expected at least %d.6",
		      req->major, req->minor, FUSE_KERNEL_VERSION);
		return -1;
	}

	/* We limit ourselves to 15 because we don't handle BATCH_FORGET yet */
	out.minor = MIN(req->minor, 15); 
	fuse_struct_size = sizeof(out);
#if defined(FUSE_COMPAT_22_INIT_OUT_SIZE)
	/* FUSE_KERNEL_VERSION >= 23. */

	/* If the kernel only works on minor revs older than or equal to 22,
	 * then use the older structure size since this code only uses the 7.22
	 * version of the structure. */
	if (req->minor <= 22) {
		fuse_struct_size = FUSE_COMPAT_22_INIT_OUT_SIZE;
	}
#endif
	out.major = FUSE_KERNEL_VERSION;
	out.max_readahead = req->max_readahead;
	out.flags = FUSE_ATOMIC_O_TRUNC | FUSE_BIG_WRITES;

	out.max_background = 32;
	out.congestion_threshold = 32;
	out.max_write = MAX_WRITE;
	fuse_reply(fuse, hdr->unique, &out, fuse_struct_size);
	return NO_STATUS;
}

static int handle_fuse_request(struct fuse *fuse, struct fuse_handler* handler,
		const struct fuse_in_header *hdr, const void *data, size_t data_len)
{
	switch (hdr->opcode) {
	case FUSE_LOOKUP: { /* bytez[] -> entry_out */
		const char* name = data;
		return handle_lookup(fuse, handler, hdr, name);
	}

	case FUSE_GETATTR: { /* getattr_in -> attr_out */
		const struct fuse_getattr_in *req = data;
		return handle_getattr(fuse, handler, hdr, req);
	}

	case FUSE_READLINK: { /* read_in -> byte[] */
		const struct fuse_read_in *req = data;
		return handle_readlink(fuse, handler, hdr, req);
	}

	case FUSE_SYMLINK: { /* to, from ->  */
		const char *to = data;
		const char *from = to + strlen(to) + 1;
		return handle_symlink(fuse, handler, hdr, from, to);
	}

	case FUSE_OPENDIR: { /* open_in -> open_out */
		const struct fuse_open_in *req = data;
		return handle_opendir(fuse, handler, hdr, req);
	}    

	case FUSE_READDIR: {
		const struct fuse_read_in *req = data;
		return handle_readdir(fuse, handler, hdr, req);
	}    

	case FUSE_RELEASEDIR: { /* release_in -> */
		const struct fuse_release_in *req = data;
		return handle_releasedir(fuse, handler, hdr, req);
	}
	
	case FUSE_INIT: { /* init_in -> init_out */
		const struct fuse_init_in *req = data;
		return handle_init(fuse, handler, hdr, req);
	}

	default: {
		ERROR("[%d] NOTIMPL op=%d uniq=%lx nid=%lx\n",
			handler->token, hdr->opcode, hdr->unique, hdr->nodeid);
		return -ENOSYS;
	}
	}
}

static void handle_fuse_requests(struct fuse_handler* handler)
{
	struct fuse* fuse = handler->fuse;
	for (;;) {
		ssize_t len = read(fuse->fd, handler->request_buffer, sizeof(handler->request_buffer));
		if (len < 0) {
			if (errno == ENODEV) {
				ERROR("[%d] someone stole our marbles!\n", handler->token);
				exit(2);
			}
			ERROR("[%d] handle_fuse_requests: errno=%d\n", handler->token, errno);
			continue;
		}

		if ((size_t)len < sizeof(struct fuse_in_header)) {
			ERROR("[%d] request too short: len=%zu\n", handler->token, (size_t)len);
			continue;
		}

		const struct fuse_in_header *hdr = (void*)handler->request_buffer;
		if (hdr->len != (size_t)len) {
			ERROR("[%d] malformed header: len=%zu, hdr->len=%u\n",
					handler->token, (size_t)len, hdr->len);
			continue;
		}

		const void *data = handler->request_buffer + sizeof(struct fuse_in_header);
		size_t data_len = len - sizeof(struct fuse_in_header);
		__u64 unique = hdr->unique;
		int res = handle_fuse_request(fuse, handler, hdr, data, data_len);
		/* We do not access the request again after this point because the underlying
		 * buffer storage may have been reused while processing the request. */

		if (res != NO_STATUS) {
			if (res) {
				ERROR("[%d] ERROR %d\n", handler->token, res);
			}
			fuse_status(fuse, unique, res);
		}
	}
}

static void* start_handler(void* data)
{
	struct fuse_handler* handler = data;
	handle_fuse_requests(handler);
	return NULL;
}

static int fuse_setup(struct fuse *fuse)
{
	mode_t mask = MS_NOSUID | MS_NODEV | MS_NOEXEC | MS_NOATIME;
	const char *dest_path = "mnt";
	char opts[256];
	const char *devname = "/dev/fuse";
	struct stat stbuf;
	int res;

	res = stat(dest_path, &stbuf);
	if (res == -1) {
		perror("Failed to access mountpoint");
		return -1;
	}

	fuse->fd = open(devname, O_RDWR);
	if (fuse->fd == -1) {
		perror("Failed to open fuse device");
		return -1;
	}

	snprintf(opts, sizeof(opts),
		 "fd=%i,rootmode=%o,user_id=%i,group_id=%i",
		 fuse->fd, stbuf.st_mode & S_IFMT, getuid(), getgid());

	if (mount(devname, dest_path, "fuse", mask, opts) != 0) {
		perror("Failed to mount fuse filesystem");
		return -1;
	}

	fuse->gid = getgid();
	fuse->mask = mask;

	return 0;
}

int main(int argc, char *argv[])
{
	struct fuse_global global;
	struct fuse fuse_default;
	struct fuse_handler handler_default;
	pthread_t thread_default;
	const char *source_path = "/";

	printf("pagesize: %d\n", getpagesize());
	memset(&global, 0, sizeof(global));
	pthread_mutex_init(&global.lock, NULL);
	global.inode_ctr = 1;

	memset(&global.root, 0, sizeof(global.root));
	global.root.nid = FUSE_ROOT_ID; /* 1 */
	global.root.refcount = 2;
	global.root.namelen = strlen(source_path);
	global.root.name = strdup(source_path);
	global.root.uid = 0; /* AID_ROOT */

	strcpy(global.source_path, source_path);

	memset(&fuse_default, 0, sizeof(fuse_default));
	fuse_default.global = &global;
	handler_default.fuse = &fuse_default;
	handler_default.token = 0;
	fuse_setup(&fuse_default);
	if (pthread_create(&thread_default, NULL, start_handler, &handler_default)) {
		perror("Failed to pthread_create");
		return -1;
	}
	while (1) {
		;
	}
}
