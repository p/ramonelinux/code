#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mount.h>
#include <sys/param.h> /* MIN() */
#include <sys/uio.h>
#include <linux/fuse.h>
#include <linux/types.h>

/* Pseudo-error constant used to indicate that no fuse status is needed
 * or that a reply has already been written. */
#define NO_STATUS 1

/* Maximum number of bytes to write in one request. */
#define MAX_WRITE (256 * 1024)

/* Maximum number of bytes to read in one request. */
#define MAX_READ (128 * 1024)

/* Largest possible request.
 * The request size is bounded by the maximum size of a FUSE_WRITE request because it has
 * the largest possible data payload. */
#define MAX_REQUEST_SIZE (sizeof(struct fuse_in_header) + sizeof(struct fuse_write_in) + MAX_WRITE)
__u8 request_buffer[MAX_REQUEST_SIZE];

static const char *dest_path = "mnt";
static int fd;

static int fuse_setup()
{
	const char *devname = "/dev/fuse";
	mode_t mask = MS_NOSUID | MS_NODEV | MS_NOEXEC | MS_NOATIME;
	char opts[256];
	struct stat stbuf;
	int res;

	res = stat(dest_path, &stbuf);
	if (res == -1) {
		perror("Failed to access mountpoint");
		return -1;
	}

	fd = open(devname, O_RDWR);
	if (fd == -1) {
		perror("Failed to open fuse device");
		return -1;
	}

	snprintf(opts, sizeof(opts),
		 "fd=%i,rootmode=%o,user_id=%i,group_id=%i",
		 fd, stbuf.st_mode & S_IFMT, getuid(), getgid());

	printf("opts: %s\n", opts);

	if (mount(devname, dest_path, "fuse", mask, opts) != 0) {
		perror("Failed to mount fuse filesystem");
		return -1;
	}

	return 0;
}

static void fuse_reply(__u64 unique, void *data, int len)
{
	struct fuse_out_header hdr;
	struct iovec vec[2];
	int res;

	hdr.len = len + sizeof(hdr);
	hdr.error = 0;
	hdr.unique = unique;

	vec[0].iov_base = &hdr;
	vec[0].iov_len = sizeof(hdr);
	vec[1].iov_base = data;
	vec[1].iov_len = len;

	res = writev(fd, vec, 2);
	if (res < 0) {
		printf("*** REPLY FAILED *** %d\n", errno);
	}
}

static int handle_init(const struct fuse_in_header* hdr, const struct fuse_init_in* req)
{
	struct fuse_init_out out;
	size_t fuse_struct_size;

	out.minor = MIN(req->minor, 15);
	fuse_struct_size = sizeof(out);
	out.major = FUSE_KERNEL_VERSION;
	out.max_readahead = req->max_readahead;
	out.flags = FUSE_ATOMIC_O_TRUNC | FUSE_BIG_WRITES;

	out.max_background = 32;
	out.congestion_threshold = 32;
	out.max_write = MAX_WRITE;
	fuse_reply(hdr->unique, &out, fuse_struct_size);
	return NO_STATUS;
}

static int handle_fuse_request(const struct fuse_in_header *hdr, const void *data, size_t data_len)
{
	switch (hdr->opcode) {
	
	case FUSE_INIT: /* init_in -> init_out */
		const struct fuse_init_in *req = data;
		return handle_init(hdr, req);

	default:
		printf("NOTIMPL op=%d uniq=%lx nid=%lx\n",
			hdr->opcode, hdr->unique, hdr->nodeid);
		return -ENOSYS;
	}
}

static void handle_fuse_requests()
{
	for (;;) {
		ssize_t len = read(fd, request_buffer, sizeof(request_buffer));
		if (len < 0) {
			if (errno == ENODEV) {
				perror("someone stole our marbles!\n");
				exit(2);
			}
			printf("handle_fuse_requests: errno=%d\n", errno);
			continue;
		}

		if ((size_t)len < sizeof(struct fuse_in_header)) {
			printf("request too short: len=%zu\n", (size_t)len);
			continue;
		}

		const struct fuse_in_header *hdr = (void*)request_buffer;
		if (hdr->len != (size_t)len) {
			printf("malformed header: len=%zu, hdr->len=%u\n",
				(size_t)len, hdr->len);
			continue;
		}
		const void *data = request_buffer + sizeof(struct fuse_in_header);
		size_t data_len = len - sizeof(struct fuse_in_header);
		int res = handle_fuse_request(hdr, data, data_len);
		if (res != NO_STATUS) {
			if (res) {
				printf("ERROR %d\n", res);
			}
		}
	}
}

int main(int argc, char *argv[])
{
	fuse_setup();
	handle_fuse_requests();

	umount2(dest_path, MNT_DETACH);
	return 0;
}
