#define FUSE_USE_VERSION 26

#include <fuse.h>

static struct fuse_operations hello_oper = {
};

int main(int argc, char *argv[])
{
	return fuse_main(argc, argv, &hello_oper, NULL);
}
