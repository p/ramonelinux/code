#define FUSE_USE_VERSION 26

#include <fuse.h>
#include <dirent.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdarg.h>
#include <sys/stat.h>
#include <lzo/lzo1x.h>
#include <pthread.h>

lzo_align_t wrkmem[LZO1X_1_MEM_COMPRESS];
FILE *logfile;

void log_init()
{
	logfile = fopen("zipfs.log", "w");
	setvbuf(logfile, NULL, _IOLBF, 0);
}

void log_msg(const char *format, ...)
{
	va_list ap;
	va_start(ap, format);

	vfprintf(logfile, format, ap);
}

static void fullpath(char fpath[PATH_MAX], const char *path)
{
	char *rootdir = "/mnt/fuse";

	strcpy(fpath, rootdir);
	strncat(fpath, path, PATH_MAX);
}

static int hello_getattr(const char *path, struct stat *stbuf)
{
	int res;
	char fpath[PATH_MAX];

	fullpath(fpath, path);
	res = lstat(fpath, stbuf);
	if (res == -1)
		return -errno;

	return 0;
}

static int hello_readdir(const char *path, void *buf, fuse_fill_dir_t filler,
		         off_t offset, struct fuse_file_info *fi)
{
	DIR *dp;
	struct dirent *de;
	char fpath[PATH_MAX];

	fullpath(fpath, path);
	dp = opendir(fpath);
	if (!dp)
		return -errno;

	while (de = readdir(dp)) {
		struct stat st;

		memset(&st, 0, sizeof(st));
		st.st_ino = de->d_ino;
		st.st_mode = de->d_type << 12;
		if (filler(buf, de->d_name, &st, 0))
			break;
	}

	closedir(dp);
	return 0;
}

static int hello_readlink(const char *path, char *buf, size_t size)
{
	int res;
	char fpath[PATH_MAX];

	fullpath(fpath, path);
	res = readlink(fpath, buf, size - 1);
	if (res == -1)
		return -errno;

	buf[res] = '\0';
	return 0;
}

static int hello_mknod(const char *path, mode_t mode, dev_t rdev)
{
	int res;
	char fpath[PATH_MAX];

	fullpath(fpath, path);
	if (S_ISREG(mode)) {
		res = open(fpath, O_CREAT | O_EXCL | O_WRONLY, mode);
		if (res > 0)
			res = close(res);
	} else if (S_ISFIFO(mode)) {
		res = mkfifo(fpath, mode);
	} else {
		res = mknod(fpath, mode, rdev);
	}

	if (res == -1)
		return -errno;

	return 0;
}

static int hello_mkdir(const char *path, mode_t mode)
{
	int res;
	char fpath[PATH_MAX];

	fullpath(fpath, path);
	res = mkdir(fpath, mode);
	if (res == -1)
		return -errno;

	return 0;
}

static int hello_symlink(const char *from, const char *to)
{
	int res;

	res = symlink(from, to);
	if (res == -1)
		return -errno;

	return 0;
}

static int hello_unlink(const char *path)
{
	int res;
	char fpath[PATH_MAX];

	fullpath(fpath, path);
	res = unlink(fpath);
	if (res == -1)
		return -errno;

	return 0;
}

static int hello_rmdir(const char *path)
{
	int res;
	char fpath[PATH_MAX];

	fullpath(fpath, path);
	res = rmdir(fpath);
	if (res == -1)
		return -errno;

	return 0;
}

static int hello_rename(const char *from, const char *to)
{
	int res;

	res = rename(from, to);
	if (res == -1)
		return -errno;

	return 0;
}

static int hello_link(const char *from, const char *to)
{
	int res;

	res = link(from, to);
	if (res == -1)
		return -errno;

	return 0;
}

static int hello_chmod(const char *path, mode_t mode)
{
	int res;
	char fpath[PATH_MAX];

	fullpath(fpath, path);
	res = chmod(fpath, mode);
	if (res == -1)
		return -errno;

	return 0;
}

static int hello_chown(const char *path, uid_t uid, gid_t gid)
{
	int res;
	char fpath[PATH_MAX];

	fullpath(fpath, path);
	res = lchown(fpath, uid, gid);
	if (res == -1)
		return -errno;

	return 0;
}

static int hello_truncate(const char *path, off_t size)
{
	int res;
	char fpath[PATH_MAX];

	fullpath(fpath, path);
	res = truncate(fpath, size);
	if (res == -1)
		return -errno;

	return 0;
}

static int hello_utime(const char *path, struct utimbuf *times)
{
	int res;
	char fpath[PATH_MAX];

	fullpath(fpath, path);
	res = utime(fpath, times);
	if (res == -1)
		return -errno;

	return 0;
}

static int hello_open(const char *path, struct fuse_file_info *fi)
{
	int res;
	char fpath[PATH_MAX];

	fullpath(fpath, path);
	res = open(fpath, fi->flags);
	if (res == -1)
		return -errno;

	close(res);
	return 0;
}

#define ZIPFS_SIG (0x43474244) /* DBGC */

struct compressd_buffer {
	uint32_t    sig;
	//uint32_t    raw_len;
	uint32_t    zip_len;
};

pthread_mutex_t f_lock;

static size_t r_offset;

static int hello_read(const char *path, char *buf, size_t size, off_t offset,
		      struct fuse_file_info *fi)
{
	int fd;
	int res, err;
	char dst[4096];
	//char tmp[4096];
	char *tmp;
	lzo_uint raw_len;
	int all_len = 0;
	int all_raw_len = 0;
	char fpath[PATH_MAX];
	struct compressd_buffer *header;
	int i;

	pthread_mutex_lock(&f_lock);

	fullpath(fpath, path);

	log_msg("read: fpath=%s\n", fpath);
	fd = open(fpath, O_RDONLY);
	if (fd == -1) {
		pthread_mutex_unlock(&f_lock);
		return -errno;
	}

	log_msg("size=%ld, offset=%ld\n", size, offset);

	if (offset > 0) {
		pthread_mutex_unlock(&f_lock);
		return size;
	}

	header = malloc(sizeof(*header));
	int size_in = size;
	tmp = malloc(size_in);

	while (all_len < size_in) {
		log_msg("pread1 i=%d, offset=%ld, r_offset=%ld, all_len=%ld\n", i, offset, r_offset, all_len);
		res = pread(fd, header, sizeof(*header), r_offset);
		if (res == -1) {
			log_msg("read buf error\n");
			pthread_mutex_unlock(&f_lock);
			return -errno;
		}
		all_len += sizeof(*header);
		r_offset += sizeof(*header);

		if (header->sig != ZIPFS_SIG) {
			log_msg("magic error\n");
			pthread_mutex_unlock(&f_lock);
			return -errno;
		}

		log_msg("i=%d, header->zip_len: %d\n", i, header->zip_len);

		bzero(dst, sizeof(dst));

		log_msg("pread2 i=%d, offset=%ld, r_offset=%ld, all_len=%ld\n", i, offset, r_offset, all_len);
		res = pread(fd, dst, header->zip_len, r_offset);
		if (res == -1) {
			log_msg("pread error\n");
			pthread_mutex_unlock(&f_lock);
			return -errno;
		}
		log_msg("i=%d, res=%ld\n", i, res);

		all_len += header->zip_len;
		r_offset += header->zip_len;
		//all_len += 4096;

		err = lzo1x_decompress(dst, res, tmp + all_len, &raw_len, wrkmem);
		if (err != LZO_E_OK) {
			log_msg("lzo1x_decompress error\n");
			pthread_mutex_unlock(&f_lock);
			return -errno;
		}

		//size_in -= (header->zip_len + sizeof(*header));
		all_raw_len += raw_len;
		i++;
	}

	//log_msg("raw_len=%ld\n", raw_len);
	//bzero(buf, size);
	//memcpy(buf, tmp, all_raw_len);
	//log_msg("tmp: %s\n", tmp);
	close(fd);
	free(header);
	free(tmp);

	pthread_mutex_unlock(&f_lock);
	//return size;
	return res + sizeof(*header);
}

static size_t w_offset;

static int hello_write(const char *path, const char *buf, size_t size,
		       off_t offset, struct fuse_file_info *fi)
{
	int fd;
	int res, err;
	char tmp[4096];
	lzo_uint zipped_len;
	char fpath[PATH_MAX];
	struct compressd_buffer *header;

	//if (offset > 0)
	//	return size;

	fullpath(fpath, path);

	log_msg("write: fpath=%s\n", fpath);
	fd = open(fpath, O_WRONLY);
	if (fd == -1)
		return -errno;

	log_msg("size=%ld offset=%ld\n", size, offset);

	header = malloc(sizeof(*header));
	header->sig = ZIPFS_SIG;
	//header->raw_len = size;

	bzero(tmp, sizeof(tmp));
	err = lzo1x_1_compress(buf, size, tmp, &zipped_len, wrkmem);
	if (err != LZO_E_OK) {
		log_msg("lzo1x_1_compress error\n");
		return -errno;
	}

	header->zip_len = zipped_len;

	log_msg("pwrite1 sizeof(*header)=%ld, w_offset=%ld\n", sizeof(*header), w_offset);
	res = pwrite(fd, header, sizeof(*header), w_offset);
	if (res == -1) {
		log_msg("pwrite error\n");
		return -errno;
	}
	w_offset += sizeof(*header);

	log_msg("pwrite2 zipped_len=%ld, w_offset=%ld\n", zipped_len, w_offset);
	res = pwrite(fd, tmp, header->zip_len, w_offset);
	if (res == -1) {
		log_msg("pwrite error\n");
		return -errno;
	}
	w_offset += header->zip_len;

	log_msg("res=%ld\n", res);

	close(fd);
	free(header);

	//return res + sizeof(*header);
	return size;
}

static int hello_statfs(const char *path, struct statvfs *stbuf)
{
	int res;
	char fpath[PATH_MAX];

	fullpath(fpath, path);

	res = statvfs(fpath, stbuf);
	if (res == -1)
		return -errno;

	return 0;
}

static int hello_release(const char *path, struct fuse_file_info *fi)
{
	return 0;
}

static struct fuse_operations hello_oper = {
	.getattr	= hello_getattr,
	.readdir	= hello_readdir,
	.readlink	= hello_readlink,
	.mknod		= hello_mknod,
	.mkdir		= hello_mkdir,
	.symlink	= hello_symlink,
	.unlink		= hello_unlink,
	.rmdir		= hello_rmdir,
	.rename		= hello_rename,
	.link		= hello_link,
	.chmod		= hello_chmod,
	.chown		= hello_chown,
	.truncate	= hello_truncate,
	.utime		= hello_utime,
	.open		= hello_open,
	.read		= hello_read,
	.write		= hello_write,
	.statfs		= hello_statfs,
	.release	= hello_release,
};

int main(int argc, char *argv[])
{
	log_init();
	lzo_init();
	pthread_mutex_init(&f_lock, NULL);

	return fuse_main(argc, argv, &hello_oper, NULL);
}
