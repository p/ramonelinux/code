#define FUSE_USE_VERSION 26

#include <fuse.h>

static int hello_open(const char *path, struct fuse_file_info *fi)
{
	return 0;
}

static struct fuse_operations hello_oper = {
	.open	= hello_open,
};

int main(int argc, char *argv[])
{
	return fuse_main(argc, argv, &hello_oper, NULL);
}
