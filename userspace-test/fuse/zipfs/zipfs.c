#define FUSE_USE_VERSION 26

#include <fuse.h>
#include <dirent.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdarg.h>
#include <sys/stat.h>
#include <lzo/lzo1x.h>

lzo_align_t wrkmem[LZO1X_1_MEM_COMPRESS];
FILE *logfile;

void log_init()
{
	logfile = fopen("zipfs.log", "w");
	setvbuf(logfile, NULL, _IOLBF, 0);
}

void log_msg(const char *format, ...)
{
	va_list ap;
	va_start(ap, format);

	vfprintf(logfile, format, ap);
}

static void fullpath(char fpath[PATH_MAX], const char *path)
{
	char *rootdir = "/mnt/fuse";

	strcpy(fpath, rootdir);
	strncat(fpath, path, PATH_MAX);
}

static int hello_getattr(const char *path, struct stat *stbuf)
{
	int res;
	char fpath[PATH_MAX];

	fullpath(fpath, path);
	res = lstat(fpath, stbuf);
	if (res == -1)
		return -errno;

	return 0;
}

static int hello_readdir(const char *path, void *buf, fuse_fill_dir_t filler,
		         off_t offset, struct fuse_file_info *fi)
{
	DIR *dp;
	struct dirent *de;
	char fpath[PATH_MAX];

	fullpath(fpath, path);
	dp = opendir(fpath);
	if (!dp)
		return -errno;

	while (de = readdir(dp)) {
		struct stat st;

		memset(&st, 0, sizeof(st));
		st.st_ino = de->d_ino;
		st.st_mode = de->d_type << 12;
		if (filler(buf, de->d_name, &st, 0))
			break;
	}

	closedir(dp);
	return 0;
}

static int hello_readlink(const char *path, char *buf, size_t size)
{
	int res;
	char fpath[PATH_MAX];

	fullpath(fpath, path);
	res = readlink(fpath, buf, size - 1);
	if (res == -1)
		return -errno;

	buf[res] = '\0';
	return 0;
}

static int hello_mknod(const char *path, mode_t mode, dev_t rdev)
{
	int res;
	char fpath[PATH_MAX];

	fullpath(fpath, path);
	if (S_ISREG(mode)) {
		res = open(fpath, O_CREAT | O_EXCL | O_WRONLY, mode);
		if (res > 0)
			res = close(res);
	} else if (S_ISFIFO(mode)) {
		res = mkfifo(fpath, mode);
	} else {
		res = mknod(fpath, mode, rdev);
	}

	if (res == -1)
		return -errno;

	return 0;
}

static int hello_mkdir(const char *path, mode_t mode)
{
	int res;
	char fpath[PATH_MAX];

	fullpath(fpath, path);
	res = mkdir(fpath, mode);
	if (res == -1)
		return -errno;

	return 0;
}

static int hello_symlink(const char *from, const char *to)
{
	int res;

	res = symlink(from, to);
	if (res == -1)
		return -errno;

	return 0;
}

static int hello_unlink(const char *path)
{
	int res;
	char fpath[PATH_MAX];

	fullpath(fpath, path);
	res = unlink(fpath);
	if (res == -1)
		return -errno;

	return 0;
}

static int hello_rmdir(const char *path)
{
	int res;
	char fpath[PATH_MAX];

	fullpath(fpath, path);
	res = rmdir(fpath);
	if (res == -1)
		return -errno;

	return 0;
}

static int hello_rename(const char *from, const char *to)
{
	int res;

	res = rename(from, to);
	if (res == -1)
		return -errno;

	return 0;
}

static int hello_link(const char *from, const char *to)
{
	int res;

	res = link(from, to);
	if (res == -1)
		return -errno;

	return 0;
}

static int hello_chmod(const char *path, mode_t mode)
{
	int res;
	char fpath[PATH_MAX];

	fullpath(fpath, path);
	res = chmod(fpath, mode);
	if (res == -1)
		return -errno;

	return 0;
}

static int hello_chown(const char *path, uid_t uid, gid_t gid)
{
	int res;
	char fpath[PATH_MAX];

	fullpath(fpath, path);
	res = lchown(fpath, uid, gid);
	if (res == -1)
		return -errno;

	return 0;
}

static int hello_truncate(const char *path, off_t size)
{
	int res;
	char fpath[PATH_MAX];

	fullpath(fpath, path);
	res = truncate(fpath, size);
	if (res == -1)
		return -errno;

	return 0;
}

static int hello_utime(const char *path, struct utimbuf *times)
{
	int res;
	char fpath[PATH_MAX];

	fullpath(fpath, path);
	res = utime(fpath, times);
	if (res == -1)
		return -errno;

	return 0;
}

static int hello_open(const char *path, struct fuse_file_info *fi)
{
	int res;
	char fpath[PATH_MAX];

	fullpath(fpath, path);
	res = open(fpath, fi->flags);
	if (res == -1)
		return -errno;

	close(res);
	return 0;
}

static int hello_read(const char *path, char *buf, size_t size, off_t offset,
		      struct fuse_file_info *fi)
{
	int fd;
	int res, err;
	char dst[4096] = { 0 };
	lzo_uint unzipped_len;
	char fpath[PATH_MAX];

	fullpath(fpath, path);

	log_msg("read: fpath=%s\n", fpath);
	//char *dst = malloc(size);
	fd = open(fpath, O_RDONLY);
	if (fd == -1)
		return -errno;

	log_msg("size=%ld, offset=%ld\n", size, offset);

	res = pread(fd, dst, size, offset);
	//res = pread(fd, buf, size, offset);
	if (res == -1) {
		log_msg("pread error\n");
		res = -errno;
	}
	log_msg("res=%ld\n", res);
#if 1
	err = lzo1x_decompress(dst, res, buf, &unzipped_len, wrkmem);
	if (err != LZO_E_OK) {
		log_msg("lzo1x_decompress error\n");
		return -errno;
	}

	log_msg("unzipped_len=%ld\n", unzipped_len);
#endif
	close(fd);
	return res;
}

static int hello_write(const char *path, const char *buf, size_t size,
		       off_t offset, struct fuse_file_info *fi)
{
	int fd;
	int res, err;
	char tmp[4096] = { 0 };
	lzo_uint zipped_len;
	char fpath[PATH_MAX];

	fullpath(fpath, path);

	log_msg("write: fpath=%s\n", fpath);
	fd = open(fpath, O_WRONLY);
	if (fd == -1)
		return -errno;

	log_msg("size=%ld offset=%ld\n", size, offset);
#if 1
	err = lzo1x_1_compress(buf, size, tmp, &zipped_len, wrkmem);
	if (err != LZO_E_OK) {
		log_msg("lzo1x_1_compress error\n");
		return -errno;
	}

	log_msg("zipped_len=%ld\n", zipped_len);
	res = pwrite(fd, tmp, zipped_len, offset);
#endif
	//res = pwrite(fd, buf, size, offset);
	if (res == -1) {
		log_msg("pwrite error\n");
		return -errno;
	}

	log_msg("res=%ld\n", res);

	close(fd);
	return res;
}

static int hello_statfs(const char *path, struct statvfs *stbuf)
{
	int res;
	char fpath[PATH_MAX];

	fullpath(fpath, path);

	res = statvfs(fpath, stbuf);
	if (res == -1)
		return -errno;

	return 0;
}

static int hello_release(const char *path, struct fuse_file_info *fi)
{
	return 0;
}

static struct fuse_operations hello_oper = {
	.getattr	= hello_getattr,
	.readdir	= hello_readdir,
	.readlink	= hello_readlink,
	.mknod		= hello_mknod,
	.mkdir		= hello_mkdir,
	.symlink	= hello_symlink,
	.unlink		= hello_unlink,
	.rmdir		= hello_rmdir,
	.rename		= hello_rename,
	.link		= hello_link,
	.chmod		= hello_chmod,
	.chown		= hello_chown,
	.truncate	= hello_truncate,
	.utime		= hello_utime,
	.open		= hello_open,
	.read		= hello_read,
	.write		= hello_write,
	.statfs		= hello_statfs,
	.release	= hello_release,
};

int main(int argc, char *argv[])
{
	log_init();
	lzo_init();

	return fuse_main(argc, argv, &hello_oper, NULL);
}
