#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/kvm.h>

int main()
{
	int fd = open("/dev/kvm", O_RDONLY);

	int version = ioctl(fd, KVM_GET_API_VERSION, 0);
	printf("version: %d\n", version);

	int vmfd = ioctl(fd, KVM_CREATE_VM, 0);
	printf("vmfd: %d, fd: %d\n", vmfd, fd);

	int coalesced_mmio = ioctl(fd, KVM_CHECK_EXTENSION, KVM_CAP_COALESCED_MMIO);
	printf("coalesced_mmio: %d\n", coalesced_mmio);

	int join_memory_regions_works = ioctl(fd, KVM_CHECK_EXTENSION, KVM_CAP_JOIN_MEMORY_REGIONS_WORKS);
	printf("join_memory_regions_works: %d\n", join_memory_regions_works);

	int vcpu_events = ioctl(fd, KVM_CHECK_EXTENSION, KVM_CAP_VCPU_EVENTS);
	printf("vcpu_events: %d\n", vcpu_events);

	int robust_singlestep = ioctl(fd, KVM_CHECK_EXTENSION, KVM_CAP_X86_ROBUST_SINGLESTEP);
	printf("robust_singlestep: %d\n", robust_singlestep);

	int debugregs = ioctl(fd, KVM_CHECK_EXTENSION, KVM_CAP_DEBUGREGS);
	printf("debugregs: %d\n", debugregs);

	int xsave = ioctl(fd, KVM_CHECK_EXTENSION, KVM_CAP_XSAVE);
	printf("xsave: %d\n", xsave);

	int xcrs = ioctl(fd, KVM_CHECK_EXTENSION, KVM_CAP_XCRS);
	printf("xcrs: %d\n", xcrs);

	int pit_state2 = ioctl(fd, KVM_CHECK_EXTENSION, KVM_CAP_PIT_STATE2);
	printf("pit_state2: %d\n", pit_state2);

	int direct_msi = ioctl(fd, KVM_CHECK_EXTENSION, KVM_CAP_SIGNAL_MSI);
	printf("direct_msi: %d\n", direct_msi);

	int pci_2_3 = ioctl(fd, KVM_CHECK_EXTENSION, KVM_CAP_PCI_2_3);
	printf("pci_2_3: %d\n", pci_2_3);

	int irq_inject_status = ioctl(fd, KVM_CHECK_EXTENSION, KVM_CAP_IRQ_INJECT_STATUS);
	printf("irq_inject_status: %d\n", irq_inject_status);

	int readonly_mem = ioctl(fd, KVM_CHECK_EXTENSION, KVM_CAP_READONLY_MEM);
	printf("readonly_mem: %d\n", readonly_mem);

	int ioeventfd = ioctl(fd, KVM_CHECK_EXTENSION, KVM_CAP_IOEVENTFD);
	printf("ioeventfd: %d\n", ioeventfd);

	return 0;
}
