#include <stdio.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <linux/kvm.h>

int main()
{
	int fd = open("/dev/kvm", O_RDONLY);

	int version = ioctl(fd, KVM_GET_API_VERSION, 0);
	printf("version: %d\n", version);

	int vmfd = ioctl(fd, KVM_CREATE_VM, 0);

	long mmap_size = ioctl(fd, KVM_GET_VCPU_MMAP_SIZE, 0);
	printf("mmap_size: %ld\n", mmap_size);

	int kvm_fd = ioctl(vmfd, KVM_CREATE_VCPU, 0);
	if (kvm_fd < 0) {
		printf("kvm_create_vcpu failed\n");
		return -1;
	}

	struct kvm_run *run = mmap(NULL, mmap_size, PROT_READ | PROT_WRITE, MAP_SHARED, kvm_fd, 0);
	if (run == MAP_FAILED) {
		printf("mmaping vcpu state failed\n");
		return -1;
	}

	int ret = 0;

	do {
		int r = ioctl(kvm_fd, KVM_RUN, 0);
		if (r < 0) {
			printf("kvm run failed\n");
			return -1;
		}

		switch (run->exit_reason) {
		case KVM_EXIT_IO:
			ret = 0;
			break;
		case KVM_EXIT_INTERNAL_ERROR:
			//printf("KVM_EXIT_INTERNAL_ERROR\n");
			break;
		default:
			break;
		}
	} while (ret == 0);

	return ret;
}
