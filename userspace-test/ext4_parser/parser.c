#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <linux/types.h>
#include "ext4.h"

int block_nr_super_block = 0;
int block_nr_group_desc = 1;
int block_size = 4096;
int inode_size;
int inodes_per_group;

int getblock(int fd, ssize_t block_nr, int blocks, char *block_buf)
{
	lseek(fd, block_size * block_nr, SEEK_SET);
	bzero(block_buf, block_size);
	ssize_t len = read(fd, block_buf, block_size * blocks);
	if (len < 0)
		perror("read");

	return 0;
}

int name_get_ino(__u8 *block_buf, char *name)
{
	int i;
	int len = 0;
	struct ext4_dir_entry_2 *dentry;

	for (i = 0; i < 128; i++) {
		dentry = (struct ext4_dir_entry_2 *)(block_buf + len);
		if (!strncmp(dentry->name, name, dentry->name_len)) {
			printf("matched: %s %d\n", dentry->name, dentry->inode);
			return dentry->inode;
		}
		len += dentry->rec_len;
	}

	return -1;
}

int ino_get_ext(int device_fd, int ino, int *start, int *len)
{
	struct ext4_group_desc *group_table, *group;
	struct ext4_extent_header *eh;
	struct ext4_extent *ext;
	struct ext4_inode *inode;
	__u8 block_buf[4096 * 512];

	int bg = (ino - 1) / inodes_per_group;
	int index = (ino - 1) % inodes_per_group;
	int offset = index * inode_size;

	getblock(device_fd, block_nr_group_desc, 512, block_buf);
	group_table = (struct ext4_group_desc *)block_buf;
	group = group_table + bg;

	getblock(device_fd, group->bg_inode_table_lo, 512, block_buf);
	inode = (struct ext4_inode *)(block_buf + offset);
	
	eh = (struct ext4_extent_header *)inode->i_block;
	if (eh->eh_magic != EXT4_EXT_MAGIC) {
		printf("eh_magic not matched!\n");
		return -1;
	}

	ext = (struct ext4_extent *)(&inode->i_block[3]);

	*start = ext->ee_start_lo;
	*len = ext->ee_len;

	return 0;
}

int main(int argc, char *argv[])
{
	__u8 block_buf[4096 * 512];
	int device_fd;
	struct ext4_super_block *sb;
	int ino;
	int ee_start;
	int ee_len;

	if (argc != 3) {
		printf("Usage: %s block_device file_name\n", argv[0]);
		return -1;
	}

	device_fd = open(argv[1], O_RDONLY);
	if (device_fd < 0)
		perror("open");

	getblock(device_fd, block_nr_super_block, 1, block_buf);
	sb = (struct ext4_super_block *)(block_buf + 1024);
	if (sb->s_magic != EXT4_FS_MAGIC) {
		printf("s_magic not matched!\n");
		return -1;
	}
	inode_size = sb->s_inode_size;
	inodes_per_group = sb->s_inodes_per_group;

	ino = 2;
	if (ino_get_ext(device_fd, ino, &ee_start, &ee_len))
		return -1;
	getblock(device_fd, ee_start, ee_len, block_buf);

	char *p = strtok(argv[2], "/");
	do {
		ino = name_get_ino(block_buf, p);
		if (ino <= 0) {
			printf("%s not found!\n", p);
			return -1;
		}
		printf("%s ino: %d\n", p, ino);

		if (ino_get_ext(device_fd, ino, &ee_start, &ee_len) < 0)
			return -1;
		getblock(device_fd, ee_start, ee_len, block_buf);
	} while (p = strtok(NULL, "/"));

	printf("%s\n", block_buf);

	close(device_fd);
	return 0;
}
