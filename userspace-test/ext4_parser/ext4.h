#define EXT4_FS_MAGIC 0xef53
#define EXT4_N_BLOCKS 15

struct ext4_super_block {
/*00*/  __le32  s_inodes_count;         /* Inodes count */
	__le32  s_blocks_count_lo;      /* Blocks count */
	__le32  s_r_blocks_count_lo;    /* Reserved blocks count */
	__le32  s_free_blocks_count_lo; /* Free blocks count */
/*10*/  __le32  s_free_inodes_count;    /* Free inodes count */
	__le32  s_first_data_block;     /* First Data Block */
	__le32  s_log_block_size;       /* Block size */
	__le32  s_log_cluster_size;     /* Allocation cluster size */
/*20*/  __le32  s_blocks_per_group;     /* # Blocks per group */
	__le32  s_clusters_per_group;   /* # Clusters per group */
	__le32  s_inodes_per_group;     /* # Inodes per group */
	__le32  s_mtime;                /* Mount time */
/*30*/  __le32  s_wtime;                /* Write time */
	__le16  s_mnt_count;            /* Mount count */
	__le16  s_max_mnt_count;        /* Maximal mount count */
	__le16  s_magic;                /* Magic signature */
	__le16  s_state;                /* File system state */
	__le16  s_errors;               /* Behaviour when detecting errors */
	__le16  s_minor_rev_level;      /* minor revision level */
/*40*/  __le32  s_lastcheck;            /* time of last check */
	__le32  s_checkinterval;        /* max. time between checks */
	__le32  s_creator_os;           /* OS */
	__le32  s_rev_level;            /* Revision level */
/*50*/  __le16  s_def_resuid;           /* Default uid for reserved blocks */
	__le16  s_def_resgid;           /* Default gid for reserved blocks */
	/*
	 * These fields are for EXT4_DYNAMIC_REV superblocks only.
	 *
	 * Note: the difference between the compatible feature set and
	 * the incompatible feature set is that if there is a bit set
	 * in the incompatible feature set that the kernel doesn't
	 * know about, it should refuse to mount the filesystem.
	 *
	 * e2fsck's requirements are more strict; if it doesn't know
	 * about a feature in either the compatible or incompatible
	 * feature set, it must abort and not try to meddle with
	 * things it doesn't understand...
	 */
	__le32  s_first_ino;            /* First non-reserved inode */
	__le16  s_inode_size;           /* size of inode structure */
	__le16  s_block_group_nr;       /* block group # of this superblock */
	__le32  s_feature_compat;       /* compatible feature set */
/*60*/  __le32  s_feature_incompat;     /* incompatible feature set */
	__le32  s_feature_ro_compat;    /* readonly-compatible feature set */
/*68*/  __u8   s_uuid[16];             /* 128-bit uuid for volume */
/*78*/  char   s_volume_name[16];      /* volume name */
/*88*/  char   s_last_mounted[64];     /* directory where last mounted */
/*C8*/  __le32  s_algorithm_usage_bitmap; /* For compression */
};

struct ext4_group_desc
{
	__le32  bg_block_bitmap_lo;     /* Blocks bitmap block */
	__le32  bg_inode_bitmap_lo;     /* Inodes bitmap block */
	__le32  bg_inode_table_lo;      /* Inodes table block */
	__le16  bg_free_blocks_count_lo;/* Free blocks count */
	__le16  bg_free_inodes_count_lo;/* Free inodes count */
	__le16  bg_used_dirs_count_lo;  /* Directories count */
	__le16  bg_flags;               /* EXT4_BG_flags (INODE_UNINIT, etc) */
	__le32  bg_exclude_bitmap_lo;   /* Exclude bitmap for snapshots */
	__le16  bg_block_bitmap_csum_lo;/* crc32c(s_uuid+grp_num+bbitmap) LE */
	__le16  bg_inode_bitmap_csum_lo;/* crc32c(s_uuid+grp_num+ibitmap) LE */
	__le16  bg_itable_unused_lo;    /* Unused inodes count */
	__le16  bg_checksum;            /* crc16(sb_uuid+group+desc) */
};

struct ext4_inode {
	__le16  i_mode;         /* File mode */
	__le16  i_uid;          /* Low 16 bits of Owner Uid */
	__le32  i_size_lo;      /* Size in bytes */
	__le32  i_atime;        /* Access time */
	__le32  i_ctime;        /* Inode Change time */
	__le32  i_mtime;        /* Modification time */
	__le32  i_dtime;        /* Deletion Time */
	__le16  i_gid;          /* Low 16 bits of Group Id */
	__le16  i_links_count;  /* Links count */
	__le32  i_blocks_lo;    /* Blocks count */
	__le32  i_flags;        /* File flags */
	union {
		struct {
			__le32  l_i_version;
		} linux1;
		struct {
			__le32  h_i_translator;
		} hurd1;
		struct {
			__le32  m_i_reserved1;
		} masix1;
	} osd1;                         /* OS dependent 1 */
	__le32  i_block[EXT4_N_BLOCKS];/* Pointers to blocks */
	__le32  i_generation;   /* File version (for NFS) */
	__le32  i_file_acl_lo;  /* File ACL */
	__le32  i_size_high;
	__le32  i_obso_faddr;   /* Obsoleted fragment address */
	union {
		struct {
			__le16  l_i_blocks_high; /* were l_i_reserved1 */
			__le16  l_i_file_acl_high;
			__le16  l_i_uid_high;   /* these 2 fields */
			__le16  l_i_gid_high;   /* were reserved2[0] */
			__le16  l_i_checksum_lo;/* crc32c(uuid+inum+inode) LE */
			__le16  l_i_reserved;
		} linux2;
		struct {
			__le16  h_i_reserved1;  /* Obsoleted fragment number/size which are removed in ext4 */
			__le16   h_i_mode_high;
			__le16   h_i_uid_high;
			__le16   h_i_gid_high;
			__le32   h_i_author;
		} hurd2;
		struct {
			__le16  h_i_reserved1;  /* Obsoleted fragment number/size which are removed in ext4 */
			__le16  m_i_file_acl_high;
			__le32   m_i_reserved2[2];
		} masix2;
	} osd2;                         /* OS dependent 2 */
	__le16  i_extra_isize;
	__le16  i_checksum_hi;  /* crc32c(uuid+inum+inode) BE */
	__le32  i_ctime_extra;  /* extra Change time      (nsec << 2 | epoch) */
	__le32  i_mtime_extra;  /* extra Modification time(nsec << 2 | epoch) */
	__le32  i_atime_extra;  /* extra Access time      (nsec << 2 | epoch) */
	__le32  i_crtime;       /* File Creation time */
	__le32  i_crtime_extra; /* extra FileCreationtime (nsec << 2 | epoch) */
	__le32  i_version_hi;   /* high 32 bits for 64-bit version */
	__le32  i_projid;       /* Project ID */
};

struct ext4_extent {
	__le32  ee_block;       /* first logical block extent covers */
	__le16  ee_len;         /* number of blocks covered by extent */
	__le16  ee_start_hi;    /* high 16 bits of physical block */
	__le32  ee_start_lo;    /* low 32 bits of physical block */
};

struct ext4_extent_idx {
	__le32  ei_block;       /* index covers logical blocks from 'block' */
	__le32  ei_leaf_lo;     /* pointer to the physical block of the next *
				 * level. leaf or next index could be there */
	__le16  ei_leaf_hi;     /* high 16 bits of physical block */
	__u16   ei_unused;
};

struct ext4_extent_header {
	__le16  eh_magic;       /* probably will support different formats */
	__le16  eh_entries;     /* number of valid entries */
	__le16  eh_max;         /* capacity of store in entries */
	__le16  eh_depth;       /* has tree real underlying blocks? */
	__le32  eh_generation;  /* generation of the tree */
};

#define EXT4_EXT_MAGIC 0xf30a
#define EXT4_NAME_LEN 255

struct ext4_dir_entry_2 {
	__le32  inode;                  /* Inode number */
	__le16  rec_len;                /* Directory entry length */
	__u8    name_len;               /* Name length */
	__u8    file_type;
	char    name[EXT4_NAME_LEN];    /* File name */
};
