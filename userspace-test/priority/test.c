#include <sys/time.h>
#include <sys/resource.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>

int main()
{
	int pid = getpid();
	int prio = 10;
	int ret;

	ret = getpriority(PRIO_PROCESS, pid);
	printf("%d\n", ret);
	ret = setpriority(PRIO_PROCESS, pid, prio);
	printf("%d\n", ret);
	return 0;
}
