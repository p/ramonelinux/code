#include <stdio.h>

unsigned long sp(void) { asm("movl %esp, %eax"); }

int main()
{
    unsigned long esp = sp();
    printf("esp: %lx\n", esp);
    esp += 0xc8;

    int *ebp = (int *)esp;
    int argc = *ebp;
    int i;

    for (i = 0; i < argc; i++) {
        ebp += 1;
        printf("argv[%d] at %lx: %s\n", i, ebp, *ebp);
    }

    ebp += 1;
    i = 0;
    while (*(++ebp)) {
        printf("envp[%d] at %lx: %s\n", i, ebp, *ebp);
        i++;
    }
}
