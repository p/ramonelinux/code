#include <stdio.h>

int main(int argc, char *argv[], char *envp[])
{
    int i;
    
    for (i = 0; i < argc; i++)
        printf("argv[%d] at %lx: %s\n", i, &argv[i], argv[i]);
    for (i = 0; envp[i] != NULL; i++)
        printf("envp[%d] at %lx: %s\n", i, &envp[i], envp[i]);

    return 0;
}
