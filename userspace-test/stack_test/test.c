#include <stdio.h>

void main(unsigned long esp)
{
    int *ebp = (int *)esp;
    int argc = *ebp;
    int i;

    for (i = 0; i < argc; i++) {
        ebp++;
        printf("argv[%d] at %lx: %s\n", i, ebp, *ebp);
    }

    ebp += 1;
    i = 0;
    while (*(++ebp)) {
        printf("envp[%d] at %lx: %s\n", i, ebp, *ebp);
        i++;
    }
}
