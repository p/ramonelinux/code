#include <stdio.h>

#define MPTCP_PM_ADDR_FLAG_SIGNAL                       (1 << 0)
#define MPTCP_PM_ADDR_FLAG_SUBFLOW                      (1 << 1)
#define MPTCP_PM_ADDR_FLAG_BACKUP                       (1 << 2) 
#define MPTCP_PM_ADDR_FLAG_FULLMESH                     (1 << 3)

int main()
{
	__uint8_t changed;
	__uint8_t mask = MPTCP_PM_ADDR_FLAG_BACKUP |
			 MPTCP_PM_ADDR_FLAG_FULLMESH;
	__uint8_t addr_flags = 0;
	__uint8_t entry_flags = 0;

	addr_flags |= MPTCP_PM_ADDR_FLAG_BACKUP;
	entry_flags |= MPTCP_PM_ADDR_FLAG_SUBFLOW;

	changed = (addr_flags ^ entry_flags) & mask;
	entry_flags = (entry_flags & ~mask) | (addr_flags & mask);

	printf("entry_flags=%x mask=%x changed=%x\n", entry_flags, mask, changed);

	if (changed & MPTCP_PM_ADDR_FLAG_BACKUP) {
		printf("do backup %x\n", changed & mask);
		printf("do backup %x\n", changed & ~mask);
		changed |= mask;
		printf("do backup %x\n", changed);
	}
	if (changed & MPTCP_PM_ADDR_FLAG_FULLMESH)
		printf("do fullmesh\n");

	return 0;
}
