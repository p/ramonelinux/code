#include <stdio.h>
#include <string.h>

#define id "9876543210"

int main()
{
	char tmp[13] = "1234";
	printf("sizeof=%d strlen=%d\nsizeof=%d strlen=%d\n",
			sizeof(id), strlen(id), sizeof(tmp), strlen(tmp));
}
