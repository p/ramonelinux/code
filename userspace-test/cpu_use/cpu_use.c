#include<stdio.h>  
#include<stdlib.h>  
#include<string.h>  

#define __DEBUG__ 1
#define CK_TIME 1

int main(int argc, char *argv[])
{
	FILE *fp;
	char buf[128];
	char cpu[5];
	long int user, nice, system, idle, iowait, irq, softirq;

	long int sum1, sum2, idle1, idle2;
	float usage;

	while(1) {
		fp = fopen("/proc/stat", "r");
		if(!fp) {
			perror("fopen");
			exit(EXIT_FAILURE);
		}

		fgets(buf, sizeof(buf), fp);
#if __DEBUG__
		printf("      cpuN, user, nice, system, idle, iowait, irq, softirq, steal, guest, guest_nice\nbuf = %s", buf);
#endif
		sscanf(buf, "%s%ld%ld%ld%ld%ld%ld%ld", cpu, &user, &nice, &system, &idle, &iowait, &irq, &softirq);
		sum1 = user + nice + system + idle + iowait + irq + softirq;
		idle1 = idle;
#if __DEBUG__
		printf("sum1 = %ld, idle1 = %ld\n", sum1, idle1);
#endif
		rewind(fp);

		/*第二次取数据*/  
		sleep(CK_TIME);
		memset(buf, 0, sizeof(buf));
		cpu[0] = '\0';
		user = nice = system = idle = iowait = irq = softirq = 0;
		fgets(buf, sizeof(buf), fp);
#if __DEBUG__
		printf("buf = %s", buf);
#endif
		sscanf(buf, "%s%ld%ld%ld%ld%ld%ld%ld", cpu, &user, &nice, &system, &idle, &iowait, &irq, &softirq);
		sum2 = user + nice + system + idle + iowait + irq + softirq;
		idle2 = idle;
#if __DEBUG__
		printf("sum2 = %ld, idle2 = %ld\n", sum2, idle2);
#endif

		usage = (float)(sum2 - sum1 - (idle2 - idle1)) / (sum2 - sum1) * 100;

		printf("sum = %ld, ", sum2 - sum1);
		printf("ilde = %ld, ", idle2 - idle1);
		printf("cpu usage = %.2f%%\r", usage);

#if __DEBUG__
		printf("\n=======================\n");
#else
		fflush(stdout);
#endif

		fclose(fp);
	}

	return EXIT_SUCCESS;
}
