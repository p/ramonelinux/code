#include <stdio.h>
#include <string.h>
#include <lzo/lzo1x.h>

/*
 * LZO_EXTERN(int)
 * lzo1x_1_compress        ( const lzo_bytep src, lzo_uint  src_len,
 *                                 lzo_bytep dst, lzo_uintp dst_len,
 *                                 lzo_voidp wrkmem );
 */
int main()
{
	const lzo_bytep src = "This is a test for lzo compress! This is a test for lzo compress! This is a test for lzo compress!";
	lzo_uint slen = strlen(src);
	char tmp[1024];
	lzo_uint tlen;
	char dst[1024] = { 0 };
	lzo_uint dlen;
	lzo_align_t wrkmem[LZO1X_1_MEM_COMPRESS];
	int ret;

	lzo_init();
	ret = lzo1x_1_compress(src, slen, tmp, &tlen, wrkmem);
	if (ret != LZO_E_OK)
		perror("lzo1x_1_compress error: ");
	ret = lzo1x_decompress(tmp, tlen, dst, &dlen, wrkmem);
	if (ret != LZO_E_OK)
		perror("lzo1x_decompress_safe error: ");
	printf("dst: %s\nslen: %d, tlen: %d, dlen: %d\n", dst, (int)slen, (int)tlen, (int)dlen);

	return 0;
}
