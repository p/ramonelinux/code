#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/dm-ioctl.h>

int main(int argc, char *argv[])
{
	int fd;
	int ret;
	struct dm_ioctl dm;

	fd = open("/dev/md0", O_RDONLY);
	if (fd == -1) {
		perror("open");
		exit(EXIT_FAILURE);
	}

	ret = ioctl(fd, DM_VERSION_CMD, &dm);
	if (ret == -1) {
		perror("ioctl");
		exit(EXIT_FAILURE);
	}

	close(fd);

	exit(EXIT_SUCCESS);
}
