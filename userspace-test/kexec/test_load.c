#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <fcntl.h>
#include <stddef.h>
#include <limits.h>
#include <linux/kexec.h>
#include <linux/reboot.h>
#include <sys/syscall.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/reboot.h>
#include <linux/fb.h>
#include <linux/elf.h>
#include <byteswap.h>
#include "purgatory.c"

unsigned long long mem_min = 0;
unsigned long long mem_max = ULONG_MAX;

#define KEXEC_MAX_SEGMENTS 16

#define _ALIGN_UP_MASK(addr, mask)   (((addr) + (mask)) & ~(mask))
#define _ALIGN_DOWN_MASK(addr, mask) ((addr) & ~(mask))

/* align addr on a size boundary - adjust address up/down if needed */
#define _ALIGN_UP(addr, size)        \
	        _ALIGN_UP_MASK(addr, (typeof(addr))(size) - 1)
#define _ALIGN_DOWN(addr, size)      \
	        _ALIGN_DOWN_MASK(addr, (typeof(addr))(size) - 1)

/* align addr on a size boundary - adjust address up if needed */
#define _ALIGN(addr, size)     _ALIGN_UP(addr, size)

#define DEFAULT_INITRD_ADDR_MAX 0x37FFFFFF
#define INITRD_BASE 0x1000000 /* 16MB */

/* command line parameter may be appended by purgatory */
#define PURGATORY_CMDLINE_SIZE 64

#define EDD_MBR_SIG_MAX 16

#ifndef E820MAX
#define E820MAX 128             /* number of entries in E820MAP */
#endif

struct e820entry {
	uint64_t addr;  /* start of memory segment */
	uint64_t size;  /* size of memory segment */
	uint32_t type;          /* type of memory segment */
#define E820_RAM        1
#define E820_RESERVED   2
#define E820_ACPI       3 /* usable as RAM once ACPI tables have been read */
#define E820_NVS        4
#define E820_PMEM       7
#define E820_PRAM       12
} __attribute__((packed));

#define EDDMAXNR        6       /* number of edd_info structs starting at EDDBUF  */

#define EDD_DEVICE_PARAM_SIZE 74

struct edd_info {
	uint8_t  device;
	uint8_t  version;
	uint16_t interface_support;
	uint16_t legacy_max_cylinder;
	uint8_t  legacy_max_head;
	uint8_t  legacy_sectors_per_track;
	uint8_t  edd_device_params[EDD_DEVICE_PARAM_SIZE];
} __attribute__ ((packed));

struct apm_bios_info {
	uint16_t version;       /* 0x40 */
	uint16_t cseg;          /* 0x42 */
	uint32_t offset;        /* 0x44 */
	uint16_t cseg_16;       /* 0x48 */
	uint16_t dseg;          /* 0x4a */
	uint16_t flags;         /* 0x4c */
	uint16_t cseg_len;      /* 0x4e */
	uint16_t cseg_16_len;   /* 0x50 */
	uint16_t dseg_len;      /* 0x52 */
	uint8_t  reserved[44];  /* 0x54 */
};

/* FIXME expand on drive_info_)struct... */
struct drive_info_struct {
	uint8_t dummy[32];
};
struct sys_desc_table {
	uint16_t length;
	uint8_t  table[30];
};

struct x86_linux_param_header {
	uint8_t  orig_x;                        /* 0x00 */
	uint8_t  orig_y;                        /* 0x01 */
	uint16_t ext_mem_k;                     /* 0x02 -- EXT_MEM_K sits here */
	uint16_t orig_video_page;               /* 0x04 */
	uint8_t  orig_video_mode;               /* 0x06 */
	uint8_t  orig_video_cols;               /* 0x07 */
	uint16_t unused2;                       /* 0x08 */
	uint16_t orig_video_ega_bx;             /* 0x0a */
	uint16_t unused3;                       /* 0x0c */
	uint8_t  orig_video_lines;              /* 0x0e */
	uint8_t  orig_video_isVGA;              /* 0x0f */
	uint16_t orig_video_points;             /* 0x10 */

	/* VESA graphic mode -- linear frame buffer */
	uint16_t lfb_width;                     /* 0x12 */
	uint16_t lfb_height;                    /* 0x14 */
	uint16_t lfb_depth;                     /* 0x16 */
	uint32_t lfb_base;                      /* 0x18 */
	uint32_t lfb_size;                      /* 0x1c */
	uint16_t cl_magic;                      /* 0x20 */
#define CL_MAGIC_VALUE 0xA33F
	uint16_t cl_offset;                     /* 0x22 */
	uint16_t lfb_linelength;                /* 0x24 */
	uint8_t  red_size;                      /* 0x26 */
	uint8_t  red_pos;                       /* 0x27 */
	uint8_t  green_size;                    /* 0x28 */
	uint8_t  green_pos;                     /* 0x29 */
	uint8_t  blue_size;                     /* 0x2a */
	uint8_t  blue_pos;                      /* 0x2b */
	uint8_t  rsvd_size;                     /* 0x2c */
	uint8_t  rsvd_pos;                      /* 0x2d */
	uint16_t vesapm_seg;                    /* 0x2e */
	uint16_t vesapm_off;                    /* 0x30 */
	uint16_t pages;                         /* 0x32 */
	uint8_t  reserved4[12];                 /* 0x34 -- 0x3f reserved for future expansion */

	struct apm_bios_info apm_bios_info;     /* 0x40 */
	struct drive_info_struct drive_info;    /* 0x80 */
	struct sys_desc_table sys_desc_table;   /* 0xa0 */
	uint32_t ext_ramdisk_image;             /* 0xc0 */
	uint32_t ext_ramdisk_size;              /* 0xc4 */
	uint32_t ext_cmd_line_ptr;              /* 0xc8 */
	uint8_t reserved4_1[0x1c0 - 0xcc];      /* 0xe4 */
	uint8_t efi_info[32];                   /* 0x1c0 */
	uint32_t alt_mem_k;                     /* 0x1e0 */
	uint8_t  reserved5[4];                  /* 0x1e4 */
	uint8_t  e820_map_nr;                   /* 0x1e8 */
	uint8_t  eddbuf_entries;                /* 0x1e9 */
	uint8_t  edd_mbr_sig_buf_entries;       /* 0x1ea */
	uint8_t  reserved6[6];                  /* 0x1eb */
	uint8_t  setup_sects;                   /* 0x1f1 */
	uint16_t mount_root_rdonly;             /* 0x1f2 */
	uint16_t syssize;                       /* 0x1f4 */
	uint16_t swapdev;                       /* 0x1f6 */
	uint16_t ramdisk_flags;                 /* 0x1f8 */
#define RAMDISK_IMAGE_START_MASK        0x07FF
#define RAMDISK_PROMPT_FLAG             0x8000
#define RAMDISK_LOAD_FLAG               0x4000
	uint16_t vid_mode;                      /* 0x1fa */
	uint16_t root_dev;                      /* 0x1fc */
	uint8_t  reserved9[1];                  /* 0x1fe */
	uint8_t  aux_device_info;               /* 0x1ff */
	/* 2.00+ */
	uint8_t  reserved10[2];                 /* 0x200 */
	uint8_t  header_magic[4];               /* 0x202 */
	uint16_t protocol_version;              /* 0x206 */
	uint16_t rmode_switch_ip;               /* 0x208 */
	uint16_t rmode_switch_cs;               /* 0x20a */
	uint8_t  reserved11[4];                 /* 0x208 */
	uint8_t  loader_type;                   /* 0x210 */
#define LOADER_TYPE_LOADLIN         1
#define LOADER_TYPE_BOOTSECT_LOADER 2
#define LOADER_TYPE_SYSLINUX        3
#define LOADER_TYPE_ETHERBOOT       4
#define LOADER_TYPE_KEXEC           0x0D
#define LOADER_TYPE_UNKNOWN         0xFF
	uint8_t  loader_flags;                  /* 0x211 */
	uint8_t  reserved12[2];                 /* 0x212 */
	uint32_t kernel_start;                  /* 0x214 */
	uint32_t initrd_start;                  /* 0x218 */
	uint32_t initrd_size;                   /* 0x21c */
	uint8_t  reserved13[4];                 /* 0x220 */
	/* 2.01+ */
	uint16_t heap_end_ptr;                  /* 0x224 */
	uint8_t  reserved14[2];                 /* 0x226 */
	/* 2.02+ */
	uint32_t cmd_line_ptr;                  /* 0x228 */
	/* 2.03+ */
	uint32_t initrd_addr_max;               /* 0x22c */
#if TENATIVE
	/* 2.04+ */
	uint16_t entry32_off;                   /* 0x230 */
	uint16_t internal_cmdline_off;          /* 0x232 */
	uint32_t low_base;                      /* 0x234 */
	uint32_t low_memsz;                     /* 0x238 */
	uint32_t low_filesz;                    /* 0x23c */
	uint32_t real_base;                     /* 0x240 */
	uint32_t real_memsz;                    /* 0x244 */
	uint32_t real_filesz;                   /* 0x248 */
	uint32_t high_base;                     /* 0x24C */
	uint32_t high_memsz;                    /* 0x250 */
	uint32_t high_filesz;                   /* 0x254 */
	uint8_t  reserved15[0x2d0 - 0x258];     /* 0x258 */
#else
	/* 2.04+ */
	uint32_t kernel_alignment;              /* 0x230 */
	uint8_t  relocatable_kernel;            /* 0x234 */
	uint8_t  min_alignment;                 /* 0x235 */
	uint16_t xloadflags;                    /* 0x236 */
	uint32_t cmdline_size;                  /* 0x238 */
	uint32_t hardware_subarch;              /* 0x23C */
	uint64_t hardware_subarch_data;         /* 0x240 */
	uint32_t payload_offset;                /* 0x248 */
	uint32_t payload_length;                /* 0x24C */
	uint64_t setup_data;                    /* 0x250 */
	uint64_t pref_address;                  /* 0x258 */
	uint32_t init_size;                     /* 0x260 */
	uint32_t handover_offset;               /* 0x264 */
	uint8_t  reserved16[0x290 - 0x268];     /* 0x268 */
	uint32_t edd_mbr_sig_buffer[EDD_MBR_SIG_MAX];   /* 0x290 */
#endif
	struct  e820entry e820_map[E820MAX];    /* 0x2d0 */
	uint8_t _pad8[48];                      /* 0xcd0 */
	struct  edd_info eddbuf[EDDMAXNR];      /* 0xd00 */
	/* 0xeec */
#define COMMAND_LINE_SIZE 2048
};

struct x86_linux_header {
	uint8_t  reserved1[0xc0];               /* 0x000 */
	uint32_t ext_ramdisk_image;             /* 0x0c0 */
	uint32_t ext_ramdisk_size;              /* 0x0c4 */
	uint32_t ext_cmd_line_ptr;              /* 0x0c8 */
	uint8_t  reserved1_1[0x1f1-0xcc];       /* 0x0cc */
	uint8_t  setup_sects;                   /* 0x1f1 */
	uint16_t root_flags;                    /* 0x1f2 */
	uint32_t syssize;                       /* 0x1f4 */
	uint16_t ram_size;                      /* 0x1f8 */
	uint16_t vid_mode;                      /* 0x1fa */
	uint16_t root_dev;                      /* 0x1fc */
	uint16_t boot_sector_magic;             /* 0x1fe */
	/* 2.00+ */
	uint16_t jump;                          /* 0x200 */
	uint8_t  header_magic[4];               /* 0x202 */
	uint16_t protocol_version;              /* 0x206 */
	uint32_t realmode_swtch;                /* 0x208 */
	uint16_t start_sys;                     /* 0x20c */
	uint16_t kver_addr;                     /* 0x20e */
	uint8_t  type_of_loader;                /* 0x210 */
	uint8_t  loadflags;                     /* 0x211 */
	uint16_t setup_move_size;               /* 0x212 */
	uint32_t code32_start;                  /* 0x214 */
	uint32_t ramdisk_image;                 /* 0x218 */
	uint32_t ramdisk_size;                  /* 0x21c */
	uint32_t bootsect_kludge;               /* 0x220 */
	/* 2.01+ */
	uint16_t heap_end_ptr;                  /* 0x224 */
	uint8_t  ext_loader_ver;                /* 0x226 */
	uint8_t  ext_loader_type;               /* 0x227 */
	/* 2.02+ */
	uint32_t cmd_line_ptr;                  /* 0x228 */
	/* 2.03+ */
	uint32_t initrd_addr_max;               /* 0x22c */

	uint32_t kernel_alignment;              /* 0x230 */
	uint8_t  relocatable_kernel;            /* 0x234 */
	uint8_t  min_alignment;                 /* 0x235 */
	uint16_t xloadflags;                    /* 0x236 */
	uint32_t cmdline_size;                  /* 0x238 */
	uint32_t hardware_subarch;              /* 0x23C */
	uint64_t hardware_subarch_data;         /* 0x240 */
	uint32_t payload_offset;                /* 0x248 */
	uint32_t payload_size;                  /* 0x24C */
	uint64_t setup_data;                    /* 0x250 */
	uint64_t pref_address;                  /* 0x258 */
	uint32_t init_size;                     /* 0x260 */
	uint32_t handover_offset;               /* 0x264 */
} __attribute__((packed));

struct entry64_regs {
	uint64_t rax;
	uint64_t rbx;
	uint64_t rcx;
	uint64_t rdx;
	uint64_t rsi;
	uint64_t rdi;
	uint64_t rsp;
	uint64_t rbp;   
	uint64_t r8;    
	uint64_t r9;    
	uint64_t r10;
	uint64_t r11;
	uint64_t r12;
	uint64_t r13;
	uint64_t r14;
	uint64_t r15;
	uint64_t rip; 
};

struct mem_ehdr {
	unsigned ei_class;
	unsigned ei_data;
	unsigned e_type;
	unsigned e_machine;
	unsigned e_version;
	unsigned e_flags;
	unsigned e_phnum;
	unsigned e_shnum;
	unsigned e_shstrndx;
	unsigned long long e_entry;
	unsigned long long e_phoff;
	unsigned long long e_shoff;
	unsigned e_notenum;
	struct mem_phdr *e_phdr;
	struct mem_shdr *e_shdr;
	struct mem_note *e_note;
	unsigned long rel_addr, rel_size;
};

struct memory_range {
	unsigned long long start, end;
	unsigned type;
#define RANGE_RAM       0
#define RANGE_RESERVED  1
#define RANGE_ACPI      2
#define RANGE_ACPI_NVS  3
#define RANGE_UNCACHED  4
#define RANGE_PMEM              6
#define RANGE_PRAM              11
};

struct memory_ranges {
	unsigned int size;
	unsigned int max_size;
	struct memory_range *ranges;
};

struct kexec_info {
	struct kexec_segment *segment;
	int nr_segments;
	struct memory_range *memory_range;
	int memory_ranges;
	struct memory_range *crash_range;
	int nr_crash_ranges; 
	void *entry;
	struct mem_ehdr rhdr;
	unsigned long backup_start;
	unsigned long kexec_flags;
	unsigned long backup_src_start;
	unsigned long backup_src_size;
	/* Set to 1 if we are using kexec file syscall */
	unsigned long file_mode :1;

	/* Filled by kernel image processing code */
	int initrd_fd;
	char *command_line;
	int command_line_len;
};

int bzImage_support_efi_boot;

enum coretype {
	CORE_TYPE_UNDEF = 0,
	CORE_TYPE_ELF32 = 1,
	CORE_TYPE_ELF64 = 2
};

struct arch_options_t {
	uint8_t         reset_vga;
	uint16_t        serial_base;
	uint32_t        serial_baud;
	uint8_t         console_vga;
	uint8_t         console_serial;
	enum coretype   core_header_type;
	uint8_t         pass_memmap_cmdline;
	uint8_t         noefi;
};


struct arch_options_t arch_options = {
	.reset_vga   = 0,
	.serial_base = 0x3f8,
	.serial_baud = 0,
	.console_vga = 0, 
	.console_serial = 0,
	.core_header_type = CORE_TYPE_ELF64,
	.pass_memmap_cmdline = 0,
	.noefi = 0,
};

int sort_segments(struct kexec_info *info)
{
	int i, j;
	void *end;

	/* Do a stupid insertion sort... */
	for (i = 0; i < info->nr_segments; i++) {
		int tidx;
		struct kexec_segment temp;
		tidx = i;
		for (j = i +1; j < info->nr_segments; j++) {
			if (info->segment[j].mem < info->segment[tidx].mem) {
				tidx = j;
			}
		}
		if (tidx != i) {
			temp = info->segment[tidx];
			info->segment[tidx] = info->segment[i];
			info->segment[i] = temp;
		}
	}
	/* Now see if any of the segments overlap */
	end = 0;
	for (i = 0; i < info->nr_segments; i++) {
		if (end > info->segment[i].mem) {
			fprintf(stderr, "Overlapping memory segments at %p\n",
					end);
			return -1;
		}
		end = ((char *)info->segment[i].mem) + info->segment[i].memsz;
	}
	return 0;
}

unsigned long locate_hole(struct kexec_info *info,
		unsigned long hole_size, unsigned long hole_align, 
		unsigned long hole_min, unsigned long hole_max, 
		int hole_end)
{
	int i, j;
	struct memory_range *mem_range;
	int max_mem_ranges, mem_ranges;
	unsigned long hole_base;

	if (hole_end == 0) {
		printf("Invalid hole end argument of 0 specified to locate_hole");
		exit(-1);
	}

	/* Set an initial invalid value for the hole base */
	hole_base = ULONG_MAX;

	/* Align everything to at least a page size boundary */
	if (hole_align < (unsigned long)getpagesize()) {
		hole_align = getpagesize();
	}

	/* Compute the free memory ranges */
	max_mem_ranges = info->memory_ranges + info->nr_segments;
	mem_range = malloc(max_mem_ranges *sizeof(struct memory_range));
	mem_ranges = 0;

	/* Perform a merge on the 2 sorted lists of memory ranges  */
	for (j = 0, i = 0; i < info->memory_ranges; i++) {
		unsigned long long sstart, send;
		unsigned long long mstart, mend;
		mstart = info->memory_range[i].start;
		mend = info->memory_range[i].end;
		if (info->memory_range[i].type != RANGE_RAM)
			continue;
		while ((j < info->nr_segments) &&
				(((unsigned long)info->segment[j].mem) <= mend)) {
			sstart = (unsigned long)info->segment[j].mem;
			send = sstart + info->segment[j].memsz -1;
			if (mstart < sstart) {
				mem_range[mem_ranges].start = mstart;
				mem_range[mem_ranges].end = sstart -1;
				mem_range[mem_ranges].type = RANGE_RAM;
				mem_ranges++;
			}
			mstart = send +1;
			j++;
		}
		if (mstart < mend) {
			mem_range[mem_ranges].start = mstart;
			mem_range[mem_ranges].end = mend;
			mem_range[mem_ranges].type = RANGE_RAM;
			mem_ranges++;
		}
	}
	/* Now find the end of the last memory_range I can use */
	for (i = 0; i < mem_ranges; i++) {
		unsigned long long start, end, size;
		start = mem_range[i].start;
		end   = mem_range[i].end;
		/* First filter the range start and end values
		 * through the lens of mem_min, mem_max and hole_align.
		 */
		if (start < mem_min) {
			start = mem_min;
		}
		if (start < hole_min) {
			start = hole_min;
		}
		start = _ALIGN(start, hole_align);
		if (end > mem_max) {
			end = mem_max;
		}
		if (end > hole_max) {
			end = hole_max;
		}
		/* Is this still a valid memory range? */
		if ((start >= end) || (start >= mem_max) || (end <= mem_min)) {
			continue;
		}
		/* Is there enough space left so we can use it? */
		size = end - start;
		if (!hole_size || size >= hole_size - 1) {
			if (hole_end > 0) {
				hole_base = start;
				break;
			} else {
				hole_base = _ALIGN_DOWN(end - hole_size + 1,
						hole_align);
			}
		}
	}
	free(mem_range);
	if (hole_base == ULONG_MAX) {
		fprintf(stderr, "Could not find a free area of memory of "
				"0x%lx bytes...\n", hole_size);
		return ULONG_MAX;
	}
	if (hole_size && (hole_base + hole_size - 1)  > hole_max) {
		fprintf(stderr, "Could not find a free area of memory below: "
				"0x%lx...\n", hole_max);
		return ULONG_MAX;
	}
	return hole_base;
}

void add_segment_phys_virt(struct kexec_info *info,
		const void *buf, size_t bufsz,
		unsigned long base, size_t memsz, int phys)
{
	unsigned long last;
	size_t size;
	int pagesize;

	if (bufsz > memsz) {
		bufsz = memsz;
	}
	/* Forget empty segments */
	if (memsz == 0) {
		return;
	}

	/* Round memsz up to a multiple of pagesize */
	pagesize = getpagesize();
	memsz = _ALIGN(memsz, pagesize);

	/* Verify base is pagesize aligned.
	 * 	 * Finding a way to cope with this problem
	 * 	 	 * is important but for now error so at least
	 * 	 	 	 * we are not surprised by the code doing the wrong
	 * 	 	 	 	 * thing.
	 * 	 	 	 	 	 */
	if (base & (pagesize -1)) {
		printf("Base address: 0x%lx is not page aligned\n", base);
		exit(-1);
	}

	if (phys)
		base = virt_to_phys(base);

	last = base + memsz -1;
	if (!valid_memory_range(info, base, last)) {
		printf("Invalid memory segment %p - %p\n",
				(void *)base, (void *)last);
		exit(-1);
	}

	size = (info->nr_segments + 1) * sizeof(info->segment[0]);
	info->segment = realloc(info->segment, size);
	info->segment[info->nr_segments].buf   = buf;
	info->segment[info->nr_segments].bufsz = bufsz;
	info->segment[info->nr_segments].mem   = (void *)base;
	info->segment[info->nr_segments].memsz = memsz;
	info->nr_segments++;
	if (info->nr_segments > KEXEC_MAX_SEGMENTS) {
		fprintf(stderr, "Warning: kernel segment limit reached. "
				"This will likely fail\n");
	}
}

unsigned long add_buffer_phys_virt(struct kexec_info *info,
		const void *buf, unsigned long bufsz, unsigned long memsz,
		unsigned long buf_align, unsigned long buf_min, unsigned long buf_max,
		int buf_end, int phys)
{
	unsigned long base;
	int result;
	int pagesize;

	result = sort_segments(info);
	if (result < 0) {
		printf("sort_segments failed\n");
		exit(-1);
	}

	/* Round memsz up to a multiple of pagesize */
	pagesize = getpagesize();
	memsz = _ALIGN(memsz, pagesize);

	base = locate_hole(info, memsz, buf_align, buf_min, buf_max, buf_end);
	if (base == ULONG_MAX) {
		printf("locate_hole failed\n");
		exit(-1);
	}

	add_segment_phys_virt(info, buf, bufsz, base, memsz, phys);
	return base;
}

unsigned long add_buffer_virt(struct kexec_info *info, const void *buf,
		unsigned long bufsz, unsigned long memsz,
		unsigned long buf_align, unsigned long buf_min,
		unsigned long buf_max, int buf_end)
{
	return add_buffer_phys_virt(info, buf, bufsz, memsz, buf_align,
			buf_min, buf_max, buf_end, 0);
}

unsigned long add_buffer(struct kexec_info *info,
		const void *buf,
		unsigned long bufsz,
		unsigned long memsz,
		unsigned long buf_align,
		unsigned long buf_min,
		unsigned long buf_max,
		int buf_end)
{
	return add_buffer_virt(info, buf, bufsz, memsz, buf_align,
			buf_min, buf_max, buf_end);
}

static inline int is_kexec_load_implemented(void)
{
	if (__NR_kexec_load != 0xffffffff)
		return 1;
	return 0;
}

static inline long kexec_load(void *entry, unsigned long nr_segments,
                        struct kexec_segment *segments, unsigned long flags)
{
        return (long)syscall(__NR_kexec_load, entry, nr_segments, segments, flags);
}

int setup_linux_vesafb(struct x86_linux_param_header *real_mode)
{
	struct fb_fix_screeninfo fix;
	struct fb_var_screeninfo var;
	int fd;

	fd = open("/dev/fb0", O_RDONLY);
	if (-1 == fd)
		return -1;

	if (-1 == ioctl(fd, FBIOGET_FSCREENINFO, &fix))
		goto out;
	if (-1 == ioctl(fd, FBIOGET_VSCREENINFO, &var))
		goto out;
	if (0 == strcmp(fix.id, "VESA VGA")) {
		/* VIDEO_TYPE_VLFB */
		real_mode->orig_video_isVGA = 0x23;
	} else if (0 == strcmp(fix.id, "EFI VGA")) {
		/* VIDEO_TYPE_EFI */
		real_mode->orig_video_isVGA = 0x70;
	} else {
		/* cannot handle and other types */
		goto out;
	}
	close(fd);

	real_mode->lfb_width      = var.xres;
	real_mode->lfb_height     = var.yres;
	real_mode->lfb_depth      = var.bits_per_pixel;
	real_mode->lfb_base       = fix.smem_start;
	real_mode->lfb_linelength = fix.line_length;
	real_mode->vesapm_seg     = 0;

	/* FIXME: better get size from the file returned by proc_iomem() */
	real_mode->lfb_size       = (fix.smem_len + 65535) / 65536;
	real_mode->pages          = (fix.smem_len + 4095) / 4096;

	if (var.bits_per_pixel > 8) {
		real_mode->red_pos    = var.red.offset;
		real_mode->red_size   = var.red.length;
		real_mode->green_pos  = var.green.offset;
		real_mode->green_size = var.green.length;
		real_mode->blue_pos   = var.blue.offset;
		real_mode->blue_size  = var.blue.length;
		real_mode->rsvd_pos   = var.transp.offset;
		real_mode->rsvd_size  = var.transp.length;
	}
	return 0;

out:
	close(fd);
	return -1;
}

void setup_linux_system_parameters(struct kexec_info *info,
		struct x86_linux_param_header *real_mode)
{
	/* get subarch from running kernel */
	setup_subarch(real_mode);
	if (bzImage_support_efi_boot && !arch_options.noefi)
		setup_efi_info(info, real_mode);

	/* Default screen size */
	real_mode->orig_x = 0;
	real_mode->orig_y = 0;
	real_mode->orig_video_page = 0;
	real_mode->orig_video_mode = 0;
	real_mode->orig_video_cols = 80; 
	real_mode->orig_video_lines = 25; 
	real_mode->orig_video_ega_bx = 0;
	real_mode->orig_video_isVGA = 1;
	real_mode->orig_video_points = 16; 
	setup_linux_vesafb(real_mode);

	/* Fill in the memsize later */
	real_mode->ext_mem_k = 0;
	real_mode->alt_mem_k = 0;
	real_mode->e820_map_nr = 0;

	/* Default APM info */
	memset(&real_mode->apm_bios_info, 0, sizeof(real_mode->apm_bios_info));
	/* Default drive info */
	memset(&real_mode->drive_info, 0, sizeof(real_mode->drive_info));
	/* Default sysdesc table */
	real_mode->sys_desc_table.length = 0;

	/* default yes: this can be overridden on the command line */
	real_mode->mount_root_rdonly = 0xFFFF;

	/* default /dev/hda
	 * this can be overrident on the command line if necessary.
	 */
	real_mode->root_dev = (0x3 <<8)| 0;

	/* another safe default */
	real_mode->aux_device_info = 0;

	setup_e820(info, real_mode);

	/* fill the EDD information */
	setup_edd_info(real_mode);
}

void setup_linux_bootloader_parameters_high(
		struct kexec_info *info, struct x86_linux_param_header *real_mode,
		unsigned long real_mode_base, unsigned long cmdline_offset,
		const char *cmdline, off_t cmdline_len,
		const char *initrd_buf, off_t initrd_size, int initrd_high)
{
	char *cmdline_ptr;
	unsigned long initrd_base, initrd_addr_max;

	/* Say I'm a boot loader */
	real_mode->loader_type = LOADER_TYPE_KEXEC << 4;

	/* No loader flags */
	real_mode->loader_flags = 0;

	/* Find the maximum initial ramdisk address */
	if (initrd_high)
		initrd_addr_max = ULONG_MAX;
	else {
		initrd_addr_max = DEFAULT_INITRD_ADDR_MAX;
		if (real_mode->protocol_version >= 0x0203) {
			initrd_addr_max = real_mode->initrd_addr_max;
			printf("initrd_addr_max is 0x%lx\n",
					initrd_addr_max);
		}
	}

	/* Load the initrd if we have one */
	if (initrd_buf) {
		initrd_base = add_buffer(info,
				initrd_buf, initrd_size, initrd_size,
				4096, INITRD_BASE, initrd_addr_max, -1);
		printf("Loaded initrd at 0x%lx size 0x%lx\n", initrd_base,
				initrd_size);
	} else {
		initrd_base = 0;
		initrd_size = 0;
	}

	/* Ramdisk address and size */
	real_mode->initrd_start = initrd_base & 0xffffffffUL;
	real_mode->initrd_size  = initrd_size & 0xffffffffUL;

	if (real_mode->protocol_version >= 0x020c &&
			(initrd_base & 0xffffffffUL) != initrd_base)
		real_mode->ext_ramdisk_image = initrd_base >> 32;

	if (real_mode->protocol_version >= 0x020c &&
			(initrd_size & 0xffffffffUL) != initrd_size)
		real_mode->ext_ramdisk_size = initrd_size >> 32;

	/* The location of the command line */
	/* if (real_mode_base == 0x90000) { */
	real_mode->cl_magic = CL_MAGIC_VALUE;
	real_mode->cl_offset = cmdline_offset;
	/* setup_move_size */
	/* } */
	if (real_mode->protocol_version >= 0x0202) {
		unsigned long cmd_line_ptr = real_mode_base + cmdline_offset;

		real_mode->cmd_line_ptr = cmd_line_ptr & 0xffffffffUL;
		if ((real_mode->protocol_version >= 0x020c) &&
				((cmd_line_ptr & 0xffffffffUL) != cmd_line_ptr))
			real_mode->ext_cmd_line_ptr = cmd_line_ptr >> 32;
	}

	/* Fill in the command line */
	if (cmdline_len > COMMAND_LINE_SIZE) {
		cmdline_len = COMMAND_LINE_SIZE;
	}
	cmdline_ptr = ((char *)real_mode) + cmdline_offset;
	memcpy(cmdline_ptr, cmdline, cmdline_len);
	cmdline_ptr[cmdline_len - 1] = '\0';
}

struct mem_shdr {
	unsigned sh_name;
	unsigned sh_type;
	unsigned long long sh_flags;
	unsigned long long sh_addr;
	unsigned long long sh_offset;
	unsigned long long sh_size;
	unsigned sh_link;
	unsigned sh_info;
	unsigned long long sh_addralign;
	unsigned long long sh_entsize;
	const unsigned char *sh_data;
};

struct mem_sym {
	unsigned long st_name;   /* Symbol name (string tbl index) */
	unsigned char st_info;   /* No defined meaning, 0 */
	unsigned char st_other;  /* Symbol type and binding */
	unsigned st_shndx;  /* Section index */
	unsigned long long st_value;  /* Symbol value */ 
	unsigned long long st_size;   /* Symbol size */
};

struct  mem_rela {
	unsigned long long r_offset;
	unsigned r_sym;
	unsigned r_type;
	unsigned long long r_addend;
};

#define STN_UNDEF       0               /* End of a chain.  */

#define cpu_to_le16(val) (val)
#define cpu_to_le32(val) (val)
#define cpu_to_le64(val) (val)
#define cpu_to_be16(val) bswap_16(val)
#define cpu_to_be32(val) bswap_32(val)
#define cpu_to_be64(val) bswap_64(val)
#define le16_to_cpu(val) (val)
#define le32_to_cpu(val) (val)
#define le64_to_cpu(val) (val)
#define be16_to_cpu(val) bswap_16(val)
#define be32_to_cpu(val) bswap_32(val)
#define be64_to_cpu(val) bswap_64(val)

uint16_t elf16_to_cpu(const struct mem_ehdr *ehdr, uint16_t value)
{
	if (ehdr->ei_data == ELFDATA2LSB) {
		value = le16_to_cpu(value);
	}
	else if (ehdr->ei_data == ELFDATA2MSB) {
		value = be16_to_cpu(value);
	}
	return value;
}

uint32_t elf32_to_cpu(const struct mem_ehdr *ehdr, uint32_t value)
{
	if (ehdr->ei_data == ELFDATA2LSB) {
		value = le32_to_cpu(value);
	}
	else if (ehdr->ei_data == ELFDATA2MSB) {
		value = be32_to_cpu(value);
	}
	return value;
}

uint64_t elf64_to_cpu(const struct mem_ehdr *ehdr, uint64_t value)
{
	if (ehdr->ei_data == ELFDATA2LSB) {
		value = le64_to_cpu(value);
	}
	else if (ehdr->ei_data == ELFDATA2MSB) {
		value = be64_to_cpu(value);
	}
	return value;
}

static struct mem_sym elf_sym(struct mem_ehdr *ehdr, const unsigned char *ptr)
{
	struct mem_sym sym = { 0, 0, 0, 0, 0, 0 };
	if (ehdr->ei_class == ELFCLASS32) {
		Elf32_Sym lsym;
		memcpy(&lsym, ptr, sizeof(lsym));
		sym.st_name  = elf32_to_cpu(ehdr, lsym.st_name);
		sym.st_value = elf32_to_cpu(ehdr, lsym.st_value);
		sym.st_size  = elf32_to_cpu(ehdr, lsym.st_size);
		sym.st_info  = lsym.st_info;
		sym.st_other = lsym.st_other;
		sym.st_shndx = elf16_to_cpu(ehdr, lsym.st_shndx);
	}
	else if (ehdr->ei_class == ELFCLASS64) {
		Elf64_Sym lsym;
		memcpy(&lsym, ptr, sizeof(lsym));
		sym.st_name  = elf32_to_cpu(ehdr, lsym.st_name);
		sym.st_value = elf64_to_cpu(ehdr, lsym.st_value);
		sym.st_size  = elf64_to_cpu(ehdr, lsym.st_size);
		sym.st_info  = lsym.st_info;
		sym.st_other = lsym.st_other;
		sym.st_shndx = elf16_to_cpu(ehdr, lsym.st_shndx);
	}
	else {
		printf("Bad elf class");
		exit(-1);
	}
	return sym;
}

static struct mem_rela elf_rel(struct mem_ehdr *ehdr, const unsigned char *ptr)
{
	struct mem_rela rela = { 0, 0, 0, 0 };
	if (ehdr->ei_class == ELFCLASS32) {
		Elf32_Rel lrel;
		memcpy(&lrel, ptr, sizeof(lrel));
		rela.r_offset = elf32_to_cpu(ehdr, lrel.r_offset);
		rela.r_sym    = ELF32_R_SYM(elf32_to_cpu(ehdr, lrel.r_info));
		rela.r_type   = ELF32_R_TYPE(elf32_to_cpu(ehdr, lrel.r_info));
		rela.r_addend = 0;
	}
	else if (ehdr->ei_class == ELFCLASS64) {
		Elf64_Rel lrel;
		memcpy(&lrel, ptr, sizeof(lrel));
		rela.r_offset = elf64_to_cpu(ehdr, lrel.r_offset);
		rela.r_sym    = ELF64_R_SYM(elf64_to_cpu(ehdr, lrel.r_info));
		rela.r_type   = ELF64_R_TYPE(elf64_to_cpu(ehdr, lrel.r_info));
		rela.r_addend = 0;
	}
	else {
		printf("Bad elf class");
		exit(-1);
	}
	return rela;
}

static struct mem_rela elf_rela(struct mem_ehdr *ehdr, const unsigned char *ptr)
{
	struct mem_rela rela = { 0, 0, 0, 0 };
	if (ehdr->ei_class == ELFCLASS32) {
		Elf32_Rela lrela;
		memcpy(&lrela, ptr, sizeof(lrela));
		rela.r_offset = elf32_to_cpu(ehdr, lrela.r_offset);
		rela.r_sym    = ELF32_R_SYM(elf32_to_cpu(ehdr, lrela.r_info));
		rela.r_type   = ELF32_R_TYPE(elf32_to_cpu(ehdr, lrela.r_info));
		rela.r_addend = elf32_to_cpu(ehdr, lrela.r_addend);
	}
	else if (ehdr->ei_class == ELFCLASS64) {
		Elf64_Rela lrela;
		memcpy(&lrela, ptr, sizeof(lrela));
		rela.r_offset = elf64_to_cpu(ehdr, lrela.r_offset);
		rela.r_sym    = ELF64_R_SYM(elf64_to_cpu(ehdr, lrela.r_info));
		rela.r_type   = ELF64_R_TYPE(elf64_to_cpu(ehdr, lrela.r_info));
		rela.r_addend = elf64_to_cpu(ehdr, lrela.r_addend);
	}
	else {
		printf("Bad elf class");
		exit(-1);
	}
	return rela;
}

static size_t elf_sym_size(struct mem_ehdr *ehdr)
{
	size_t sym_size = 0;
	if (ehdr->ei_class == ELFCLASS32) {
		sym_size = sizeof(Elf32_Sym);
	}
	else if (ehdr->ei_class == ELFCLASS64) {
		sym_size = sizeof(Elf64_Sym);
	}
	else {
		printf("Bad elf class");
		exit(-1);
	}
	return sym_size;
}


static size_t elf_rel_size(struct mem_ehdr *ehdr)
{
	size_t rel_size = 0;
	if (ehdr->ei_class == ELFCLASS32) {
		rel_size = sizeof(Elf32_Rel);
	}
	else if (ehdr->ei_class == ELFCLASS64) {
		rel_size = sizeof(Elf64_Rel);
	}
	else {
		printf("Bad elf class");
		exit(-1);
	}
	return rel_size;
}

static size_t elf_rela_size(struct mem_ehdr *ehdr)
{
	size_t rel_size = 0;
	if (ehdr->ei_class == ELFCLASS32) {
		rel_size = sizeof(Elf32_Rela);
	}
	else if (ehdr->ei_class == ELFCLASS64) {
		rel_size = sizeof(Elf64_Rela);
	}
	else {
		printf("Bad elf class");
		exit(-1);
	}
	return rel_size;
}

#define ELF32_MAX 0xffffffff
#define ELF64_MAX 0xffffffffffffffff
#if ELF64_MAX > ULONG_MAX
#undef ELF64_MAX
#define ELF64_MAX ULONG_MAX
#endif

unsigned long elf_max_addr(const struct mem_ehdr *ehdr)
{
	unsigned long max_addr = 0;
	if (ehdr->ei_class == ELFCLASS32) {
		max_addr = ELF32_MAX;
	}
	else if (ehdr->ei_class == ELFCLASS64) {
		max_addr = ELF64_MAX;
	}
	return max_addr;
}

int elf_rel_load(struct mem_ehdr *ehdr, struct kexec_info *info,
		unsigned long min, unsigned long max, int end)
{
	struct mem_shdr *shdr, *shdr_end, *entry_shdr;
	unsigned long entry;
	int result;
	unsigned char *buf;
	unsigned long buf_align, bufsz, bss_align, bsssz, bss_pad;
	unsigned long buf_addr, data_addr, bss_addr;

	if (max > elf_max_addr(ehdr)) {
		max = elf_max_addr(ehdr);
	}
	if (!ehdr->e_shdr) {
		fprintf(stderr, "No section header?\n");
		result = -1;
		goto out;
	}
	shdr_end = &ehdr->e_shdr[ehdr->e_shnum];

	/* Find which section entry is in */
	entry_shdr = NULL;
	entry = ehdr->e_entry;
	for(shdr = ehdr->e_shdr; shdr != shdr_end; shdr++) {
		if (!(shdr->sh_flags & SHF_ALLOC)) {
			continue;
		}
		if (!(shdr->sh_flags & SHF_EXECINSTR)) {
			continue;
		}
		/* Make entry section relative */
		if ((shdr->sh_addr <= ehdr->e_entry) &&
				((shdr->sh_addr + shdr->sh_size) > ehdr->e_entry)) {
			entry_shdr = shdr;
			entry -= shdr->sh_addr;
			break;
		}
	}

	/* Find the memory footprint of the relocatable object */
	buf_align = 1;
	bss_align = 1;
	bufsz = 0;
	bsssz = 0;
	for(shdr = ehdr->e_shdr; shdr != shdr_end; shdr++) {
		if (!(shdr->sh_flags & SHF_ALLOC)) {
			continue;
		}
		if (shdr->sh_type != SHT_NOBITS) {
			unsigned long align;
			align = shdr->sh_addralign;
			/* See if I need more alignment */
			if (buf_align < align) {
				buf_align = align;
			}
			/* Now align bufsz */
			bufsz = _ALIGN(bufsz, align);
			/* And now add our buffer */
			bufsz += shdr->sh_size;
		}
		else {
			unsigned long align;
			align = shdr->sh_addralign;
			/* See if I need more alignment */
			if (bss_align < align) {
				bss_align = align;
			}
			/* Now align bsssz */
			bsssz = _ALIGN(bsssz, align);
			/* And now add our buffer */
			bsssz += shdr->sh_size;
		}
	}
	if (buf_align < bss_align) {
		buf_align = bss_align;
	}
	bss_pad = 0;
	if (bufsz & (bss_align - 1)) {
		bss_pad = bss_align - (bufsz & (bss_align - 1));
	}

	/* Allocate where we will put the relocated object */
	buf = malloc(bufsz);
	buf_addr = add_buffer(info, buf, bufsz, bufsz + bss_pad + bsssz,
			buf_align, min, max, end);
	ehdr->rel_addr = buf_addr;
	ehdr->rel_size = bufsz + bss_pad + bsssz;

	/* Walk through and find an address for each SHF_ALLOC section */
	data_addr = buf_addr;
	bss_addr  = buf_addr + bufsz + bss_pad;
	for(shdr = ehdr->e_shdr; shdr != shdr_end; shdr++) {
		unsigned long align;
		if (!(shdr->sh_flags & SHF_ALLOC)) {
			continue;
		}
		align = shdr->sh_addralign;
		if (shdr->sh_type != SHT_NOBITS) {
			unsigned long off;
			/* Adjust the address */
			data_addr = _ALIGN(data_addr, align);

			/* Update the section */
			off = data_addr - buf_addr;
			memcpy(buf + off, shdr->sh_data, shdr->sh_size);
			shdr->sh_addr = data_addr;
			shdr->sh_data = buf + off;

			/* Advance to the next address */
			data_addr += shdr->sh_size;
		} else {
			/* Adjust the address */
			bss_addr = _ALIGN(bss_addr, align);

			/* Update the section */
			shdr->sh_addr = bss_addr;

			/* Advance to the next address */
			bss_addr += shdr->sh_size;
		}
	}
	/* Compute the relocated value for entry, and load it */
	if (entry_shdr) {
		entry += entry_shdr->sh_addr;
		ehdr->e_entry = entry;
	}
	info->entry = (void *)entry;

	/* Now that the load address is known apply relocations */
	for(shdr = ehdr->e_shdr; shdr != shdr_end; shdr++) {
		struct mem_shdr *section, *symtab;
		const unsigned char *strtab;
		size_t rel_size;
		const unsigned char *ptr, *rel_end;
		if ((shdr->sh_type != SHT_RELA) && (shdr->sh_type != SHT_REL)) {
			continue;
		}
		if ((shdr->sh_info > ehdr->e_shnum) ||
				(shdr->sh_link > ehdr->e_shnum))
		{
			printf("Invalid section number\n");
			exit(-1);
		}
		section = &ehdr->e_shdr[shdr->sh_info];
		symtab = &ehdr->e_shdr[shdr->sh_link];

		if (!(section->sh_flags & SHF_ALLOC)) {
			continue;
		}

		if (symtab->sh_link > ehdr->e_shnum) {
			/* Invalid section number? */
			continue;
		}
		strtab = ehdr->e_shdr[symtab->sh_link].sh_data;

		rel_size = 0;
		if (shdr->sh_type == SHT_REL) {
			rel_size = elf_rel_size(ehdr);
		}
		else if (shdr->sh_type == SHT_RELA) {
			rel_size = elf_rela_size(ehdr);
		}
		else {
			printf("Cannot find elf rel size\n");
			exit(-1);
		}
		rel_end = shdr->sh_data + shdr->sh_size;
		for(ptr = shdr->sh_data; ptr < rel_end; ptr += rel_size) {
			struct mem_rela rel = {0};
			struct mem_sym sym;
			const void *location;
			const unsigned char *name;
			unsigned long address, value, sec_base;
			if (shdr->sh_type == SHT_REL) {
				rel = elf_rel(ehdr, ptr);
			}
			else if (shdr->sh_type == SHT_RELA) {
				rel = elf_rela(ehdr, ptr);
			}
			/* the location to change */
			location = section->sh_data + rel.r_offset;

			/* The final address of that location */
			address = section->sh_addr + rel.r_offset;

			/* The relevant symbol */
			sym = elf_sym(ehdr, symtab->sh_data + (rel.r_sym * elf_sym_size(ehdr)));

			if (sym.st_name) {
				name = strtab + sym.st_name;
			}
			else {
				name = ehdr->e_shdr[ehdr->e_shstrndx].sh_data;
				name += ehdr->e_shdr[sym.st_shndx].sh_name;
			}

			printf("sym: %10s info: %02x other: %02x shndx: %x value: %llx size: %llx\n",
					name,
					sym.st_info,
					sym.st_other,
					sym.st_shndx,
					sym.st_value,
					sym.st_size);

			if (sym.st_shndx == STN_UNDEF) {
				/*
				 * NOTE: ppc64 elf .ro shows up a  UNDEF section.
				 * From Elf 1.2 Spec:
				 * Relocation Entries: If the index is STN_UNDEF,
				 * the undefined symbol index, the relocation uses 0
				 * as the "symbol value".
				 * TOC symbols appear as undefined but should be
				 * resolved as well. Their type is STT_NOTYPE so allow
				 * such symbols to be processed.
				 */
				if (ELF32_ST_TYPE(sym.st_info) != STT_NOTYPE) {
					printf("Undefined symbol: %s\n", name);
					exit(-1);
				}
			}
			sec_base = 0;
			if (sym.st_shndx == SHN_COMMON) {
				printf("symbol: '%s' in common section\n",
						name);
				exit(-1);
			}
			else if (sym.st_shndx == SHN_ABS) {
				sec_base = 0;
			}
			else if (sym.st_shndx > ehdr->e_shnum) {
				printf("Invalid section: %d for symbol %s\n",
						sym.st_shndx, name);
				exit(-1);
			}
			else {
				sec_base = ehdr->e_shdr[sym.st_shndx].sh_addr;
			}
			value = sym.st_value;
			value += sec_base;
			value += rel.r_addend;

			printf("sym: %s value: %lx addr: %lx\n",
					name, value, address);

			machine_apply_elf_rel(ehdr, &sym, rel.r_type,
					(void *)location, address, value);
		}
	}
	result = 0;
out:
	return result;
}

void elf_rel_build_load(struct kexec_info *info, struct mem_ehdr *ehdr,
		const char *buf, off_t len, unsigned long min, unsigned long max,
		int end, uint32_t flags)
{
	int result;

	/* Parse the Elf file */
	result = build_elf_rel_info(buf, len, ehdr, flags);
	if (result < 0) {
		printf("ELF rel parse failed\n");
		exit(-1);
	}
	/* Load the Elf data */
	result = elf_rel_load(ehdr, info, min, max, end);
	if (result < 0) {
		printf("ELF rel load failed\n");
		exit(-1);
	}
}

int elf_rel_find_symbol(struct mem_ehdr *ehdr,
		const char *name, struct mem_sym *ret_sym)
{
	struct mem_shdr *shdr, *shdr_end;

	if (!ehdr->e_shdr) {
		/* "No section header? */
		return  -1;
	}
	/* Walk through the sections and find the symbol table */
	shdr_end = &ehdr->e_shdr[ehdr->e_shnum];
	for (shdr = ehdr->e_shdr; shdr != shdr_end; shdr++) {
		const char *strtab;
		size_t sym_size;
		const unsigned char *ptr, *sym_end;
		if (shdr->sh_type != SHT_SYMTAB) {
			continue;
		}
		if (shdr->sh_link > ehdr->e_shnum) {
			/* Invalid strtab section number? */
			continue;
		}
		strtab = (char *)ehdr->e_shdr[shdr->sh_link].sh_data;
		/* Walk through the symbol table and find the symbol */
		sym_size = elf_sym_size(ehdr);
		sym_end = shdr->sh_data + shdr->sh_size;
		for(ptr = shdr->sh_data; ptr < sym_end; ptr += sym_size) {
			struct mem_sym sym;
			sym = elf_sym(ehdr, ptr);
			if (ELF32_ST_BIND(sym.st_info) != STB_GLOBAL) {
				continue;
			}
			if (strcmp(strtab + sym.st_name, name) != 0) {
				continue;
			}
			if ((sym.st_shndx == STN_UNDEF) ||
					(sym.st_shndx > ehdr->e_shnum))
			{
				printf("Symbol: %s has Bad section index %d\n",
						name, sym.st_shndx);
				exit(-1);
			}
			*ret_sym = sym;
			return 0;
		}
	}
	/* I did not find it :( */
	return -1;

}

unsigned long elf_rel_get_addr(struct mem_ehdr *ehdr, const char *name)
{
	struct mem_shdr *shdr;
	struct mem_sym sym;
	int result;
	result = elf_rel_find_symbol(ehdr, name, &sym);
	if (result < 0) {
		printf("Symbol: %s not found cannot retrive it's address\n",
				name);
		exit(-1);
	}
	shdr = &ehdr->e_shdr[sym.st_shndx];
	return shdr->sh_addr + sym.st_value;
}

void elf_rel_set_symbol(struct mem_ehdr *ehdr,
		const char *name, const void *buf, size_t size)
{
	unsigned char *sym_buf;
	struct mem_shdr *shdr;
	struct mem_sym sym;
	int result;

	result = elf_rel_find_symbol(ehdr, name, &sym);
	if (result < 0) {
		printf("Symbol: %s not found cannot set\n",
				name);
		exit(-1);
	}
	if (sym.st_size != size) {
		printf("Symbol: %s has size: %lld not %zd\n",
				name, sym.st_size, size);
		exit(-1);
	}
	shdr = &ehdr->e_shdr[sym.st_shndx];
	if (shdr->sh_type == SHT_NOBITS) {
		printf("Symbol: %s is in a bss section cannot set\n", name);
		exit(-1);
	}
	sym_buf = (unsigned char *)(shdr->sh_data + sym.st_value);
	memcpy(sym_buf, buf, size);
}

void elf_rel_get_symbol(struct mem_ehdr *ehdr,
		const char *name, void *buf, size_t size)
{
	const unsigned char *sym_buf;
	struct mem_shdr *shdr;
	struct mem_sym sym;
	int result;

	result = elf_rel_find_symbol(ehdr, name, &sym);
	if (result < 0) {
		printf("Symbol: %s not found cannot get\n", name);
		exit(-1);
	}
	if (sym.st_size != size) {
		printf("Symbol: %s has size: %lld not %zd\n",
				name, sym.st_size, size);
		exit(-1);
	}
	shdr = &ehdr->e_shdr[sym.st_shndx];
	if (shdr->sh_type == SHT_NOBITS) {
		printf("Symbol: %s is in a bss section cannot set\n", name);
		exit(-1);
	}
	sym_buf = shdr->sh_data + sym.st_value;
	memcpy(buf, sym_buf,size);
}

static int do_bzImage64_load(struct kexec_info *info,
		const char *kernel, off_t kernel_len,
		const char *command_line, off_t command_line_len,
		const char *initrd, off_t initrd_len)
{
	struct x86_linux_header setup_header;
	struct x86_linux_param_header *real_mode;
	int setup_sects;
	size_t size;
	int kern16_size;
	unsigned long setup_base, setup_size, setup_header_size;
	struct entry64_regs regs64;
	char *modified_cmdline;
	unsigned long cmdline_end;
	unsigned long align, addr, k_size;
	unsigned kern16_size_needed;

	/*
	 * Find out about the file I am about to load.
	 */
	if ((uintmax_t)kernel_len < (uintmax_t)(2 * 512))
		return -1; 

	memcpy(&setup_header, kernel, sizeof(setup_header));
	setup_sects = setup_header.setup_sects;
	if (setup_sects == 0)
		setup_sects = 4;
	kern16_size = (setup_sects + 1) * 512;
	if (kernel_len < kern16_size) {
		fprintf(stderr, "BzImage truncated?\n");
		return -1; 
	}

	if ((uintmax_t)command_line_len > (uintmax_t)setup_header.cmdline_size) {
		printf("Kernel command line too long for kernel!\n");
		return -1;
	}

	/* Need to append some command line parameters internally in case of
	 * taking crash dumps.
	 */
	if (info->kexec_flags & (KEXEC_ON_CRASH | KEXEC_PRESERVE_CONTEXT)) {
		modified_cmdline = malloc(COMMAND_LINE_SIZE);
		memset((void *)modified_cmdline, 0, COMMAND_LINE_SIZE);
		if (command_line) {
			strncpy(modified_cmdline, command_line,
					COMMAND_LINE_SIZE);
			modified_cmdline[COMMAND_LINE_SIZE - 1] = '\0';
		}

		/* If panic kernel is being loaded, additional segments need
		 * to be created. load_crashdump_segments will take care of
		 * loading the segments as high in memory as possible, hence
		 * in turn as away as possible from kernel to avoid being
		 * stomped by the kernel.
		 */
		if (load_crashdump_segments(info, modified_cmdline, -1, 0) < 0)
			return -1;

		/* Use new command line buffer */
		command_line = modified_cmdline;
		command_line_len = strlen(command_line) + 1;
	}

	/* x86_64 purgatory could be anywhere */
	elf_rel_build_load(info, &info->rhdr, purgatory, purgatory_size,
			0x3000, -1, -1, 0);
	printf("Loaded purgatory at addr 0x%lx\n", info->rhdr.rel_addr);
	/* The argument/parameter segment */
	kern16_size_needed = kern16_size;
	if (kern16_size_needed < 4096)
		kern16_size_needed = 4096;
	setup_size = kern16_size_needed + command_line_len +
		PURGATORY_CMDLINE_SIZE;
	real_mode = malloc(setup_size);
	memset(real_mode, 0, setup_size);

	/* only copy setup_header */
	setup_header_size = kernel[0x201] + 0x202 - 0x1f1;
	if (setup_header_size > 0x7f)
		setup_header_size = 0x7f;
	memcpy((unsigned char *)real_mode + 0x1f1, kernel + 0x1f1,
			setup_header_size);

	/* No real mode code will be executing. setup segment can be loaded
	 * anywhere as we will be just reading command line.
	 */
	setup_base = add_buffer(info, real_mode, setup_size, setup_size,
			16, 0x3000, -1, -1);

	printf("Loaded real_mode_data and command line at 0x%lx\n",
			setup_base);

	/* The main kernel segment */
	k_size = kernel_len - kern16_size;
	/* need to use run-time size for buffer searching */
	printf("kernel init_size 0x%x\n", real_mode->init_size);
	size = _ALIGN(real_mode->init_size, 4096);
	align = real_mode->kernel_alignment;
	addr = add_buffer(info, kernel + kern16_size, k_size,
			size, align, 0x100000, -1, -1);
	if (addr == ULONG_MAX) {
		perror("can not load bzImage64");
		exit(-1);
	}
	printf("Loaded 64bit kernel at 0x%lx\n", addr);

	/* Tell the kernel what is going on */
	setup_linux_bootloader_parameters_high(info, real_mode, setup_base,
			kern16_size_needed, command_line, command_line_len,
			initrd, initrd_len, 1); /* put initrd high too */

	elf_rel_get_symbol(&info->rhdr, "entry64_regs", &regs64,
			sizeof(regs64));
	regs64.rbx = 0;           /* Bootstrap processor */
	regs64.rsi = setup_base;  /* Pointer to the parameters */
	regs64.rip = addr + 0x200; /* the entry point for startup_64 */
	regs64.rsp = elf_rel_get_addr(&info->rhdr, "stack_end"); /* Stack, unused */
	elf_rel_set_symbol(&info->rhdr, "entry64_regs", &regs64,
			sizeof(regs64));

	cmdline_end = setup_base + kern16_size_needed + command_line_len - 1;
	elf_rel_set_symbol(&info->rhdr, "cmdline_end", &cmdline_end,
			sizeof(unsigned long));

	/* Fill in the information BIOS calls would normally provide. */
	setup_linux_system_parameters(info, real_mode);

	return 0;
}

int main(int argc, char *argv[])
{
	char *kernel;
	char *initrd;
	char *cmdline;
	int kernel_fd;
	int initrd_fd;
	int cmdline_len;
	int ret;

	unsigned long entry;
	unsigned long nr_segments;
	struct kexec_segment *segments;
	unsigned long flags;

	if (argc != 4) {
		printf("Usage: %s kernel initrd cmdline\n", argv[0]);
		return -1;
	}

	kernel = argv[1];
	initrd = argv[2];
	cmdline = argv[3];

	if (!is_kexec_load_implemented()) {
		printf("syscall kexec_file_load not available.\n");
		return -1;
	}

	kernel_fd = open(kernel, O_RDONLY);
	if (kernel_fd == -1) {
		fprintf(stderr, "Failed to open file %s: %s\n", kernel, strerror(errno));
		return -1;
	}

	initrd_fd = open(initrd, O_RDONLY);
	if (initrd_fd == -1) {
		fprintf(stderr, "Failed to open file %s: %s\n", initrd, strerror(errno));
		return -1;
	}

	cmdline_len = strlen(cmdline) + 1;

	printf("kernel_fd: %d\n", kernel_fd);
	printf("initrd_fd: %d\n", initrd_fd);
	printf("cmdline: %s\n", cmdline);
	printf("cmdline_len: %d\n", cmdline_len);
	printf("flags: %lx\n", flags);

	ret = kexec_load(&entry, nr_segments, segments, flags);
	if (ret != 0) {
		fprintf(stderr, "kexec_file_load failed: %s\n", strerror(errno));
		return -1;
	}

	ret = reboot(LINUX_REBOOT_CMD_KEXEC);
	if (ret) {
		perror("reboot");
		return -1;
	}

	return 0;
}
