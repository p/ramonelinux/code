#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <fcntl.h>
#include <linux/kexec.h>
#include <linux/reboot.h>
#include <sys/syscall.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/reboot.h>

static inline int is_kexec_file_load_implemented(void)
{
	if (__NR_kexec_file_load != 0xffffffff)
		return 1;
	return 0;
}

static inline long kexec_file_load(int kernel_fd, int initrd_fd,
		unsigned long cmdline_len, const char *cmdline_ptr,
		unsigned long flags)
{
	return (long)syscall(__NR_kexec_file_load, kernel_fd, initrd_fd,
			     cmdline_len, cmdline_ptr, flags);
}

int main(int argc, char *argv[])
{
	char *kernel;
	char *initrd;
	char *cmdline;
	int kernel_fd;
	int initrd_fd;
	int cmdline_len;
	unsigned long flags = 0;
	int ret;

	if (argc != 4) {
		printf("Usage: %s kernel initrd cmdline\n", argv[0]);
		return -1;
	}

	kernel = argv[1];
	initrd = argv[2];
	cmdline = argv[3];

	if (!is_kexec_file_load_implemented()) {
		printf("syscall kexec_file_load not available.\n");
		return -1;
	}

	kernel_fd = open(kernel, O_RDONLY);
	if (kernel_fd == -1) {
		fprintf(stderr, "Failed to open file %s: %s\n", kernel, strerror(errno));
		return -1;
	}

	initrd_fd = open(initrd, O_RDONLY);
	if (initrd_fd == -1) {
		fprintf(stderr, "Failed to open file %s: %s\n", initrd, strerror(errno));
		return -1;
	}

	cmdline_len = strlen(cmdline) + 1;

	printf("kernel_fd: %d\n", kernel_fd);
	printf("initrd_fd: %d\n", initrd_fd);
	printf("cmdline: %s\n", cmdline);
	printf("cmdline_len: %d\n", cmdline_len);
	printf("flags: %lx\n", flags);

	ret = kexec_file_load(kernel_fd, initrd_fd, cmdline_len, cmdline, flags);
	if (ret != 0) {
		fprintf(stderr, "kexec_file_load failed: %s\n", strerror(errno));
		return -1;
	}

	ret = reboot(LINUX_REBOOT_CMD_KEXEC);
	if (ret) {
		perror("reboot");
		return -1;
	}

	return 0;
}
