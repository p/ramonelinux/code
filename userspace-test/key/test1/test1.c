/* https://gist.github.com/daurnimator/dfdbaef3c255bdc11531 */

#include <stddef.h> /* NULL */
#include <errno.h> /* errno, ENOKEY */
#include <sys/types.h> /* needed for keyutils.h */
#include <keyutils.h> /* request_key, add_key, KEY_SPEC_THREAD_KEYRING */

int hasforked()
{
	key_serial_t key;
	key = request_key("user", "random", NULL, KEY_SPEC_THREAD_KEYRING);
	if (key != -1)
		return 0;
	else if (errno == ENOKEY) {
		key = add_key("user", "random", "" , 1, KEY_SPEC_THREAD_KEYRING);
	}
	if (key == -1) return -1;
	return 1;
}

#include <stdio.h> /* printf */
#include <unistd.h> /* getpid, fork */

int main()
{
	printf("%d %d\n", getpid(), hasforked());
	fork();
	printf("%d %d\n", getpid(), hasforked());
	fork();
	printf("%d %d\n", getpid(), hasforked());
	fork();
	printf("%d %d\n", getpid(), hasforked());
}
