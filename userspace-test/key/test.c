#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <keyutils.h>

/*
#include <keyutils.h>

key_serial_t add_key(const char *type, const char *description,
		     const void *payload, size_t plen,
		     key_serial_t keyring);

key_serial_t request_key(const char *type, const char *description,
			 const char *callout_info,
			 key_serial_t keyring);

long keyctl(int cmd, ...);
*/

struct encryption_key {
	uint32_t mode;
	char raw[32];
	uint32_t size;
};

static void init_keyring()
{
	key_serial_t keyring = add_key("keyring", "test", 0, 0,
				       KEY_SPEC_SESSION_KEYRING);
	if (keyring == -1) {
		perror("add_key");
		exit(-1);
	}
	printf("Keyring created with id %d in process %d\n", keyring, getpid());
}

static key_serial_t search_keying()
{
	return keyctl(KEYCTL_SEARCH, KEY_SPEC_SESSION_KEYRING,
		      "keyring", "test", 0);
}

int main()
{
	init_keyring();

	key_serial_t keyring1 = search_keying();
	printf("keyring1: %d\n", keyring1);
	struct encryption_key enkey;
	enkey.mode = 1;
	enkey.size = 32;
	key_serial_t key_id1 = add_key("logon", "enkey:1234567890",
			(void*)&enkey, sizeof(enkey), keyring1);
	printf("key_id1: %d\n", key_id1);

	key_serial_t keyring2 = request_key("keyring", "test", NULL, KEY_SPEC_SESSION_KEYRING);

	printf("keyring2: %d\n", keyring2);

	key_serial_t key_id2 = request_key("logon", "enkey:1234567890", NULL, keyring2);

	printf("key_id2: %d\n", key_id2);


	getchar();

	return 0;
}
