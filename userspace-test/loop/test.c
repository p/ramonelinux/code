#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <linux/loop.h>

int loop_get_status(char *device)
{
	int lfd;
	struct loop_info64 loop;

	lfd = open(device, O_RDWR);
	if (lfd < 0) {
		printf("open %s error.\n", device);
		return -1;
	}

	if (ioctl(lfd, LOOP_GET_STATUS64, &loop)) {
		perror("ioctl LOOP_GET_STATUS64 error");
		return -1;
	}

	printf("lo_offset: %llu\n", loop.lo_offset);
	printf("lo_sizelimit: %llu\n", loop.lo_sizelimit);
	printf("lo_file_name: %s\n", loop.lo_file_name);

	close(lfd);
}

int loop_set_status(char *device)
{
	struct loop_info64 loop;
	int lfd;
	char file[LO_NAME_SIZE] = "/home/tgl/btrfs.img";

	lfd = open(device, O_RDWR);
	if (lfd < 0) {
		printf("open %s error.\n", device);
		return -1;
	}

	loop.lo_offset = 0;
	loop.lo_sizelimit = 100;
	strncpy(loop.lo_file_name, file, LO_NAME_SIZE);

	if (ioctl(lfd, LOOP_SET_STATUS64, &loop)) {
		perror("ioctl LOOP_SET_STATUS64 error");
		return -1;
	}

	close(lfd);
}

int loop_add_device(char *file)
{
	int ffd;
	int cfd;
	int lfd;
	struct loop_info64 loop;
	int i;
	char device[32];

	printf("in loop_add\n");
	cfd = open("/dev/loop-control", O_RDWR);
	if (cfd < 0) {
		perror("open loop-control error");
		return -1;
	}

	i = ioctl(cfd, LOOP_CTL_GET_FREE);
	if (i < 0) {
		perror("ioctl error");
	}

	sprintf(device, "/dev/loop%d", i);
	printf("device: %s\n", device);

	lfd = open(device, O_RDWR);
	if (lfd < 0) {
		printf("open %s error.\n", device);
		return -1;
	}

	ffd = open(file, O_RDWR);
	if (ffd < 0) {
		printf("open %s error.\n", file);
		return -1;
	}

	if (ioctl(lfd, LOOP_SET_FD, ffd)) {
		perror("ioctl LOOP_SET_FD error");
		return -1;
	}

	close(ffd);
	close(lfd);
	close(cfd);
}

int loop_remove_device(int i)
{
	int lfd;
	char device[32];

	sprintf(device, "/dev/loop%d", i);

	lfd = open(device, O_RDWR);
	if (lfd < 0) {
		printf("open %s error.\n", device);
		return -1;
	}

	if (ioctl(lfd, LOOP_CLR_FD, i)) {
		perror("ioctl LOOP_CLR_FD error");
		return -1;
	}

	close(lfd);
	return 0;
}

int main(int argc, char *argv[])
{
	char file[LO_NAME_SIZE] = "/home/tgl/btrfs.img";

	if (argc != 2) {
		printf("args error\n");
	}
	if (!strcmp(argv[1], "add")) {
		loop_add_device(file);
	} else if (!strcmp(argv[1], "remove")) {
		loop_remove_device(0);
	} else if (!strcmp(argv[1], "set")) {
		loop_set_status("/dev/loop0");
	} else if (!strcmp(argv[1], "get")) {
		loop_get_status("/dev/loop0");
	}

#if 0
	if (ioctl(cfd, LOOP_CTL_ADD, i)) {
		perror("ioctl LOOP_CTL_ADD error");
	}

	if (ioctl(cfd, LOOP_CTL_REMOVE, i)) {
		perror("ioctl LOOP_CTL_REMOVE error");
	}
#endif

	return 0;
}
