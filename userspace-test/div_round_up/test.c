#include <stdio.h>
#include <linux/kernel.h>

int main()
{
	unsigned blocks = __KERNEL_DIV_ROUND_UP(41, 8);
	printf("blocks = %u\n", blocks);

	return 0;
}
/*
 * #define __KERNEL_DIV_ROUND_UP(n, d) (((n) + (d) - 1) / (d))
 *
 * (n - 1) / d + 1 向上取整
 */
