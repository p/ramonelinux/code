Name:       kmod
Version:    18
Release:    1%{?dist}
Summary:    Kmod

Group:      System Environment/Base
License:    GPLv2+
Url:        http://packages.profusion.mobi/kmod
Source:     http://packages.profusion.mobi/kmod/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build

%description
The Kmod package contains libraries and utilities for loading kernel modules.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr              \
            --bindir=/bin              \
            --sysconfdir=/etc          \
            --with-rootlibdir=/%{_lib} \
            --with-xz                  \
            --with-zlib                \
            --libdir=/%{_lib}   
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make pkgconfigdir=%{_libdir}/pkgconfig install DESTDIR=%{buildroot}
make -C man install DESTDIR=%{buildroot}

mkdir -p %{buildroot}/sbin
for target in depmod insmod modinfo modprobe rmmod; do
  ln -sv ../bin/kmod %{buildroot}/sbin/$target
done
ln -sv kmod %{buildroot}/bin/lsmod

%files
%defattr(-,root,root,-)
/bin/kmod
/bin/lsmod
/sbin/depmod
/sbin/insmod
/sbin/modinfo
/sbin/modprobe
/sbin/rmmod
/%{_lib}/libkmod.*
%{_includedir}/libkmod.h
%{_libdir}/pkgconfig/libkmod.pc
%{_datadir}/bash-completion/completions/kmod
%{_mandir}/man5/*.5.gz
%{_mandir}/man8/*.8.gz

%clean
rm -rf %{buildroot}

%changelog
* Sat Jul 26 2014 tanggeliang <tanggeliang@gmail.com>
- update from 16 to 18
* Fri Feb 28 2014 tanggeliang <tanggeliang@gmail.com>
- update from 15 to 16
* Thu Oct 3 2013 tanggeliang <tanggeliang@gmail.com>
- update from 14 to 15
* Thu Jul 25 2013 tanggeliang <tanggeliang@gmail.com>
- update from 12 to 14
* Sun Mar 17 2013 tanggeliang <tanggeliang@gmail.com>
- update from 9 to 12
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
