Name:       psmisc
Version:    22.21
Release:    1%{?dist}
Summary:    Psmisc

Group:      System Environment/Base
License:    GPLv2+
Url:        http://prdownloads.sourceforge.net/psmisc
Source:     http://prdownloads.sourceforge.net/psmisc/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build

%description
The Psmisc package contains programs for displaying information about running processes.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr &&
make %{?_smp_mflags}

%check

%install
make DESTDIR=%{buildroot} install

mkdir %{buildroot}/bin
mv -v %{buildroot}/usr/bin/fuser %{buildroot}/bin
mv -v %{buildroot}/usr/bin/killall %{buildroot}/bin

%files
%defattr(-,root,root,-)
/bin/fuser
/bin/killall
%{_bindir}/peekfd
%{_bindir}/prtstat
%{_bindir}/pstree
%{_bindir}/pstree.x11
%{_datadir}/locale/*/LC_MESSAGES/psmisc.mo

%files doc
%defattr(-,root,root,-)
%{_mandir}/man1/*.1.gz

%clean
rm -rf %{buildroot}

%changelog
* Sun Mar 16 2014 tanggeliang <tanggeliang@gmail.com>
- updare from 22.20 to 22.21
* Sun Oct 14 2012 tanggeliang <tanggeliang@gmail.com>
- update from 22.19 to 22.20
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
