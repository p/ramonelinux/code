Name:       gdbm
Version:    1.11
Release:    1%{?dist}
Summary:    GDBM

Group:      System Environment/Base
License:    GPLv2+
Url:        http://www.gnu.org
Source:     http://ftp.gnu.org/gnu/gdbm/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build

%description
The GDBM package contains the GNU Database Manager.
This is a disk file format database which stores key/data-pairs in single files.
The actual data of any record being stored is indexed by a unique key, which can be retrieved in less time than if it was stored in a text file.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --enable-libgdbm-compat \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
make prefix=%{buildroot}/usr install \
     libdir=%{buildroot}/%{_libdir}

rm -f %{buildroot}/%{_infodir}/dir

%files
%defattr(-,root,root,-)
%{_bindir}/gdbm_dump
%{_bindir}/gdbm_load
%{_bindir}/gdbmtool
%{_includedir}/*dbm.h
%{_libdir}/libgdbm.*a
%{_libdir}/libgdbm.so*
%{_libdir}/libgdbm_compat.*a
%{_libdir}/libgdbm_compat.so*
%{_datadir}/locale/*/LC_MESSAGES/gdbm.mo

%files doc
%defattr(-,root,root,-)
%{_infodir}/gdbm.info.gz
%{_mandir}/man1/gdbm_dump.1.gz
%{_mandir}/man1/gdbm_load.1.gz
%{_mandir}/man1/gdbmtool.1.gz
%{_mandir}/man3/gdbm.3.gz

%clean
rm -rf %{buildroot}

%changelog
* Sun Jan 5 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.10 to 1.11
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
