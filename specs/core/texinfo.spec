Name:       texinfo
Version:    5.2
Release:    1%{?dist}
Summary:    Texinfo

Group:      System Environment/Base
License:    GPLv2+
Url:        http://www.gnu.org
Source:     http://ftp.gnu.org/gnu/texinfo/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
Requires:       gzip
# Requires: perl(Locale::gettext_xs)
AutoReq:        no

%description
The Texinfo package contains programs for reading, writing, and converting info pages.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install
make DESTDIR=%{buildroot} TEXMF=%{_datadir}/texmf install-tex

%files
%defattr(-,root,root,-)
%{_bindir}/info
%{_bindir}/infokey
%{_bindir}/install-info
%{_bindir}/makeinfo
%{_bindir}/pdftexi2dvi
%{_bindir}/pod2texi
%{_bindir}/texi2any
%{_bindir}/texi2dvi
%{_bindir}/texi2pdf
%{_bindir}/texindex
%{_infodir}/dir
%{_infodir}/*info*.gz
%{_datadir}/locale/*/LC_MESSAGES/texinfo.mo
%{_datadir}/locale/*/LC_MESSAGES/texinfo_document.mo
%{_datadir}/texinfo/DebugTexinfo/*.pm
%{_datadir}/texinfo/Pod-Simple-Texinfo/Pod/Simple/Texinfo.pm
%{_datadir}/texinfo/Texinfo/*.pm
%{_datadir}/texinfo/Texinfo/Convert/*.pm
%{_datadir}/texinfo/htmlxref.cnf
%{_datadir}/texinfo/init/*.pm
%{_datadir}/texinfo/lib/Text-Unidecode/lib/Text/Unidecode.pm
%{_datadir}/texinfo/lib/Text-Unidecode/lib/Text/Unidecode/*.pm
%{_datadir}/texinfo/lib/Unicode-EastAsianWidth/lib/Unicode/EastAsianWidth.pm
%{_datadir}/texinfo/lib/libintl-perl/lib/Locale/*.pm
%{_datadir}/texinfo/lib/libintl-perl/lib/Locale/Recode/*.pm
%{_datadir}/texinfo/lib/libintl-perl/lib/Locale/RecodeData/*.pm
%{_datadir}/texinfo/texinfo.*
%{_datadir}/texmf/tex/generic/epsf/epsf.tex
%{_datadir}/texmf/tex/texinfo/t*xi*.tex

%files doc
%defattr(-,root,root,-)
%{_mandir}/man*/*.gz

%clean
rm -rf %{buildroot}

%changelog
* Sun Oct 20 2013 tanggeliang <tanggeliang@gmail.com>
- update from 5.1 to 5.2
* Sun Jun 9 2013 tanggeliang <tanggeliang@gmail.com>
- update from 5.0 to 5.1
* Sun Mar 17 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.13 to 5.0
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
