Name:       groff
Version:    1.22.3
Release:    1%{?dist}
Summary:    Groff

Group:      System Environment/Base
License:    GPLv2+
Url:        http://www.gnu.org
Source:     http://ftp.gnu.org/gnu/groff/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
AutoReq:        no

%description
The Groff package contains programs for processing and formatting text.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
PAGE=letter ./configure --prefix=/usr \
                        --libdir=%{_libdir} &&
make -j1

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

rm -f %{buildroot}/%{_infodir}/dir

%files
%defattr(-,root,root,-)
%{_bindir}/addftinfo
%{_bindir}/afmtodit
%{_bindir}/chem
%{_bindir}/eqn
%{_bindir}/eqn2graph
%{_bindir}/gdiffmk
%{_bindir}/glilypond
%{_bindir}/gperl
%{_bindir}/gpinyin
%{_bindir}/grap2graph
%{_bindir}/grn
%{_bindir}/grodvi
%{_bindir}/groff
%{_bindir}/groffer
%{_bindir}/grog
%{_bindir}/grolbp
%{_bindir}/grolj4
%{_bindir}/gropdf
%{_bindir}/grops
%{_bindir}/grotty
%{_bindir}/hpftodit
%{_bindir}/indxbib
%{_bindir}/lkbib
%{_bindir}/lookbib
%{_bindir}/mmroff
%{_bindir}/neqn
%{_bindir}/nroff
%{_bindir}/pdfmom
%{_bindir}/pdfroff
%{_bindir}/pfbtops
%{_bindir}/pic
%{_bindir}/pic2graph
%{_bindir}/post-grohtml
%{_bindir}/pre-grohtml
%{_bindir}/preconv
%{_bindir}/refer
%{_bindir}/roff2dvi
%{_bindir}/roff2html
%{_bindir}/roff2pdf
%{_bindir}/roff2ps
%{_bindir}/roff2text
%{_bindir}/roff2x
%{_bindir}/soelim
%{_bindir}/tbl
%{_bindir}/tfmtodit
%{_bindir}/troff
%{_libdir}/groff/glilypond/*.pl
%{_libdir}/groff/gpinyin/subs.pl
%{_libdir}/groff/groff_opts_*.txt
%{_libdir}/groff/groffer/*
%{_libdir}/groff/grog/subs.pl
%{_datadir}/groff/%{version}/*
%{_datadir}/groff/current
%{_datadir}/groff/site-tmac/*.local

%files doc
%defattr(-,root,root,-)
%{_docdir}/groff-%{version}/*
%{_infodir}/groff.info*.gz
%{_mandir}/man*/*.gz

%clean
rm -rf %{buildroot}

%changelog
* Wed Nov 19 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.22.2 to 1.22.3
* Sun Mar 17 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.21 to 1.22.2
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
