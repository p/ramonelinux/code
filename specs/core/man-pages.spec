Name:       man-pages
Version:    3.75
Release:    1%{?dist}
Summary:    Man-pages

Group:      System Environment/Base
License:    GPLv2+
Url:        http://www.kernel.org/pub/linux/docs/manpages
Source:     http://www.kernel.org/pub/linux/docs/manpages/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildArch:      noarch
Requires:       man-db

%description
The Man-pages package contains over 1,900 man pages.

%prep
%setup -q -n %{name}-%{version}

%build

%check

%install
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_mandir}/man1/*.1.gz
%{_mandir}/man2/*.2.gz
%{_mandir}/man3/*.3.gz
%{_mandir}/man4/*.4.gz
%{_mandir}/man5/*.5.gz
%{_mandir}/man6/*.6.gz
%{_mandir}/man7/*.7.gz
%{_mandir}/man8/*.8.gz

%clean
rm -rf %{buildroot}

%changelog
* Wed Nov 19 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.63 to 3.75
* Sun Mar 30 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.62 to 3.63
* Sun Mar 16 2014 tanggeliang <tanggeliang@gmail.com>
- updare from 3.59 to 3.62
* Fri Feb 28 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.55 to 3.59
* Tue Dec 17 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.54 to 3.55
* Sun Oct 20 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.53 to 3.54
* Sat Aug 24 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.47 to 3.53
* Sun Mar 17 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.43 to 3.47
* Tue Nov 6 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.42 to 3.43
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
