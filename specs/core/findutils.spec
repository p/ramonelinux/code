Name:       findutils
Version:    4.4.2
Release:    8%{?dist}
Summary:    Findutils

Group:      System Environment/Base
License:    GPLv2+
Url:        http://ftp.gnu.org/gnu/findutils
Source:     http://ftp.gnu.org/gnu/findutils/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  gettext

%description
The Findutils package contains programs to find files.
These programs are provided to recursively search through a directory tree and to create, maintain, and search a database (often faster than the recursive find, but unreliable if the database has not been recently updated).

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr                     \
            --libexecdir=%{_libdir}/findutils \
            --localstatedir=/var/lib/locate   \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

pushd %{buildroot}
mkdir bin
mv -v usr/bin/find bin
sed -i 's/find:=${BINDIR}/find:=\/bin/' usr/bin/updatedb
rm -f .%{_infodir}/dir
popd

%files
%defattr(-,root,root,-)
/bin/find
%{_bindir}/locate
%{_bindir}/oldfind
%{_bindir}/updatedb
%{_bindir}/xargs
%{_libdir}/findutils/bigram
%{_libdir}/findutils/code
%{_libdir}/findutils/frcode
%{_datadir}/locale/*/LC_MESSAGES/findutils.mo
%dir %{_var}/lib/

%files doc
%defattr(-,root,root,-)
%{_infodir}/find*.info.gz
%{_mandir}/man1/*.1.gz
%{_mandir}/man5/*.5.gz

%clean
rm -rf %{buildroot}

%changelog
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
