Name:       procps-ng
Version:    3.3.9
Release:    1%{?dist}
Summary:    Procps

Group:      System Environment/Base
License:    GPLv2+
Url:        http://procps-ng.sourceforge.net
Source:     http://procps-ng.sourceforge.net/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build

%description
The Procps package contains programs for monitoring processes.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr                            \
            --exec-prefix=                           \
            --libdir=%{_libdir}                      \
            --docdir=%{_docdir}/procps-ng-%{version} \
            --disable-static                         \
            --disable-kill
make %{?_smp_mflags}

%check

%install
make DESTDIR=%{buildroot} install

mkdir -p %{buildroot}/%{_lib}
mv -v %{buildroot}/%{_libdir}/libprocps.so.* %{buildroot}/%{_lib}
ln -sfv ../../%{_lib}/$(readlink %{buildroot}/%{_libdir}/libprocps.so) %{buildroot}/%{_libdir}/libprocps.so

%files
%defattr(-,root,root,-)
/bin/ps
/%{_lib}/libprocps.so.3*
/sbin/sysctl
%{_bindir}/free
%{_bindir}/pgrep
%{_bindir}/pidof
%{_bindir}/pkill
%{_bindir}/pmap
%{_bindir}/pwdx
%{_bindir}/slabtop
%{_bindir}/tload
%{_bindir}/top
%{_bindir}/uptime
%{_bindir}/vmstat
%{_bindir}/w
%{_bindir}/watch
%{_includedir}/proc/*.h
%{_libdir}/libprocps.la
%{_libdir}/libprocps.so
%{_libdir}/pkgconfig/libprocps.pc

%files doc
%defattr(-,root,root,-)
%{_mandir}/man*/*.gz
%{_docdir}/procps-ng-%{version}/*

%clean
rm -rf %{buildroot}

%changelog
* Sun Dec 15 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.3.8 to 3.3.9
* Thu Jul 25 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.3.6 to 3.3.8
* Sun Mar 17 2013 tanggeliang <tanggeliang@gmail.com>
- update from Procps to Procps-ng
* Sat Aug 25 2012 tanggeliang <tanggeliang@gmail.com>
- add install="install -D" fix "cannot change ownership".
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
