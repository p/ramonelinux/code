Name:       gmp
Version:    6.0.0
Release:    1%{?dist}
Summary:    GMP

Group:      System Environment/Base
License:    GPLv2+
Url:        http://www.gnu.org
Source:     http://ftp.gnu.org/gnu/gmp/%{name}-%{version}a.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  m4

%description
The GMP package contains math libraries. These have useful functions for arbitrary precision arithmetic.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --enable-cxx \
    --libdir=%{_libdir}
make %{?_smp_mflags}

%check

%install
make DESTDIR=%{buildroot} install

mkdir -pv %{buildroot}/usr/share/doc/gmp-%{version}
cp    -v doc/{isa_abi_headache,configuration} doc/*.html \
         %{buildroot}/usr/share/doc/gmp-%{version}

rm -f %{buildroot}/%{_infodir}/dir

%files
%defattr(-,root,root,-)
%{_includedir}/gmp.h
%{_includedir}/gmpxx.h
%{_libdir}/libgmp.*a
%{_libdir}/libgmp.so*
%{_libdir}/libgmpxx.*a
%{_libdir}/libgmpxx.so*

%files doc
%defattr(-,root,root,-)
%{_docdir}/gmp-%{version}/*
%{_infodir}/gmp.info*.gz

%clean
rm -rf %{buildroot}

%changelog
* Sat Jul 26 2014 tanggeliang <tanggeliang@gmail.com>
- update from 5.1.3 to 6.0.0
* Sun Oct 20 2013 tanggeliang <tanggeliang@gmail.com>
- update from 5.1.2 to 5.1.3
* Thu May 30 2013 tanggeliang <tanggeliang@gmail.com>
- update from 5.1.1 to 5.1.2
* Sun Mar 17 2013 tanggeliang <tanggeliang@gmail.com>
- update from 5.0.5 to 5.1.1 
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
