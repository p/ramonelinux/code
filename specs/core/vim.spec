Name:       vim
Version:    7.4
Release:    1%{?dist}
Summary:    Vim

Group:      System Environment/Base
License:    GPLv2+
Url:        http://www.vim.org
Source:     ftp://ftp.vim.org/pub/vim/unix/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build

%description
The Vim package contains a powerful text editor.

%package        vi
Summary:        vi
%description    vi

%package        gvim
Summary:        gvim
BuildRequires:  libx11 libice libxdmcp libsm libxt libxpm gtk2 python tcl ruby gpm
Requires:	vim
%description    gvim

%package        tools
Summary:        tools
AutoReq:        no
%description    tools

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n vim74

%build
echo '#define SYS_VIMRC_FILE "/etc/vimrc"' >> src/feature.h

./configure --prefix=/usr --enable-multibyte \
            --with-features=huge \
            --enable-gtk2-check --enable-gui=gtk2 &&
make %{?_smp_mflags}
cp src/vim src/gvim
make clean

./configure --prefix=/usr --enable-multibyte \
            --with-features=huge \
            --with-x=no --enable-gui=no &&
make %{?_smp_mflags}
cp src/vim src/enhanced-vim
make clean

./configure --prefix=/usr --enable-multibyte \
            --with-features=small \
            --with-x=no --enable-gui=no \
            --enable-perlinterp --enable-pythoninterp --enable-tclinterp --enable-rubyinterp &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

install -m755 src/vim %{buildroot}/%{_bindir}/vi
install -m755 src/enhanced-vim %{buildroot}/%{_bindir}/vim
install -m755 src/gvim %{buildroot}/%{_bindir}/gvim
install -m755 src/gvimtutor %{buildroot}/%{_bindir}/gvimtutor

ln -sf vi %{buildroot}/%{_bindir}/rvi
ln -sf vi %{buildroot}/%{_bindir}/rview
ln -sf vi %{buildroot}/%{_bindir}/view
ln -sf vi %{buildroot}/%{_bindir}/ex
ln -sf vim %{buildroot}/%{_bindir}/rvim
ln -sf vim %{buildroot}/%{_bindir}/vimdiff
ln -sf gvim %{buildroot}/%{_bindir}/gview
ln -sf gvim %{buildroot}/%{_bindir}/gex
ln -sf gvim %{buildroot}/%{_bindir}/evim
ln -sf gvim %{buildroot}/%{_bindir}/gvimdiff
ln -sf gvim %{buildroot}/%{_bindir}/vimx

for L in  %{buildroot}/usr/share/man/{,*/}man1/vim.1; do
    ln -sv vim.1 $(dirname $L)/vi.1
done

mkdir -pv %{buildroot}/usr/share/doc
ln -sv ../vim/vim74/doc %{buildroot}/usr/share/doc/vim-7.4

mkdir %{buildroot}/etc
cat > %{buildroot}/etc/vimrc << "EOF"
" Begin /etc/vimrc

set nocompatible
set backspace=2
set ruler
syntax on
if (&term == "iterm") || (&term == "putty")
  set background=dark
endif

" End /etc/vimrc
EOF

%files
%defattr(-,root,root,-)
%{_sysconfdir}/vimrc
%{_bindir}/vim
%{_bindir}/rvim
%{_bindir}/vimdiff
%{_bindir}/vimtutor
%{_bindir}/xxd
%{_datadir}/vim/vim74/*.vim
%{_datadir}/vim/vim74/autoload/*
%{_datadir}/vim/vim74/colors/*
%{_datadir}/vim/vim74/compiler/*
%{_datadir}/vim/vim74/ftplugin/*
%{_datadir}/vim/vim74/indent/*
%{_datadir}/vim/vim74/keymap/*
%{_datadir}/vim/vim74/lang/*
%{_datadir}/vim/vim74/macros/*
%{_datadir}/vim/vim74/plugin/*
%{_datadir}/vim/vim74/print/*
%{_datadir}/vim/vim74/spell/*
%{_datadir}/vim/vim74/syntax/*
%{_datadir}/vim/vim74/tutor/*

%files vi
%defattr(-,root,root,-)
%{_bindir}/ex
%{_bindir}/vi
%{_bindir}/view
%{_bindir}/rvi
%{_bindir}/rview

%files gvim
%defattr(-,root,root,-)
%{_bindir}/gvimtutor
%{_bindir}/gvim
%{_bindir}/gvimdiff
%{_bindir}/gview
%{_bindir}/gex
%{_bindir}/vimx
%{_bindir}/evim

%files tools
%defattr(-,root,root,-)
%{_datadir}/vim/vim74/tools/*

%files doc
%defattr(-,root,root,-)
%{_datadir}/vim/vim74/doc/*
%{_docdir}/vim-%{version}
%{_mandir}/*/man1/*.1.gz
%{_mandir}/man1/*.1.gz

%clean
rm -rf %{buildroot}

%changelog
* Mon Aug 19 2013 tanggeliang <tanggeliang@gmail.com>
- update from 7.3 to 7.4
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
