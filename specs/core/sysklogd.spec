Name:       sysklogd
Version:    1.5
Release:    7%{?dist}
Summary:    Sysklogd

Group:      System Environment/Base
License:    GPLv2+
Url:        http://www.infodrom.org/projects/sysklogd/download
Source:     http://www.infodrom.org/projects/sysklogd/download/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build

%description
The Sysklogd package contains programs for logging system messages, such as those given by the kernel when unusual things happen.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
make %{?_smp_mflags}

%check

%install
mkdir -pv %{buildroot}/usr/share/man/man{5,8}
mkdir %{buildroot}/sbin

make prefix=%{buildroot} BINDIR=%{buildroot}/sbin install MAN_USER=`id -nu` MAN_GROUP=`id -ng`

chmod 755 %{buildroot}/sbin/syslogd
chmod 755 %{buildroot}/sbin/klogd

mkdir %{buildroot}/etc
cat > %{buildroot}/etc/syslog.conf << "EOF"
# Begin /etc/syslog.conf

auth,authpriv.* -/var/log/auth.log
*.*;auth,authpriv.none -/var/log/sys.log
daemon.* -/var/log/daemon.log
kern.* -/var/log/kern.log
mail.* -/var/log/mail.log
user.* -/var/log/user.log
*.emerg *

# End /etc/syslog.conf
EOF

%files
%defattr(-,root,root,-)
%{_sysconfdir}/syslog.conf
/sbin/klogd
/sbin/syslogd

%files doc
%defattr(-,root,root,-)
%{_mandir}/man*/*.gz

%clean
rm -rf %{buildroot}

%changelog
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
