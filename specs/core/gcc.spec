%global DATE 20150918
%ifarch %{ix86}
%global gcc_target_platform i686-pc-linux-gnu
%else %ifarch x86_64
%global gcc_target_platform x86_64-unknown-linux-gnu
%endif

Name:       gcc
Version:    5.2.0
Release:    1%{?dist}
Summary:    GCC, GNU compiler collection

Group:      Development/Tools
License:    GPLv2+
Url:        http://www.gnu.org
Source:     http://ftp.gnu.org/gnu/gcc/gcc-%{version}/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  gmp mpc mpfr dejagnu
Requires:       gcc-libgcc gcc-libstdc++ gcc-libgomp gcc-libssp
Requires:       binutils glibc-devel

%description
The GCC package contains the GNU compiler collection, which includes the C and C++ compilers.

%package        libgcc
Summary:        GCC version 4.9 shared support library

%description    libgcc
This package contains GCC shared support library which is needed e.g. for exception handling support.

%package        libstdc++
Summary:        GNU Standard C++ Library

%description    libstdc++
The libstdc++ package contains a rewritten standard compliant GCC Standard C++ Library.

%package        libgomp
Summary:        GCC OpenMP v3.0 shared support library

%description    libgomp
This package contains GCC shared support library which is needed for OpenMP v3.0 support.

%package        libssp
Summary:        libssp
%description    libssp

%package        libcilkrts
Summary:        libcilkrts
%description    libcilkrts

%package        libcc1
Summary:        libcc1
%description    libcc1

%package        gfortran
Summary:        Fortran support for GCC
%description    gfortran

%package        gnat
Summary:        Ada 95 support for GCC
BuildRequires:  gcc-gnat
%description    gnat

%package        go
Summary:        Go support for GCC
%description    go

%package        objc
Summary:        Objective-C support for GCC
%description    objc

%package        objc++
Summary:        Objective-C++ support for GCC
%description    objc++

%package        doc
Summary:        Documentation
BuildArch:      noarch
Requires(post): texinfo
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
mkdir ../gcc-build &&
cd    ../gcc-build &&

../gcc-%{version}/configure     \
    --prefix=/usr               \
    --libdir=%{_libdir}         \
    --libexecdir=%{_libdir}     \
    --disable-multilib          \
    --with-system-zlib          \
    --enable-languages=c,c++,fortran,ada,go,objc,obj-c++ &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
cd    ../gcc-build &&
make DESTDIR=%{buildroot} install

mkdir %{buildroot}/%{_lib}
ln -sv ../usr/bin/cpp %{buildroot}/%{_lib}
ln -sv gcc %{buildroot}/usr/bin/cc

mkdir -pv %{buildroot}/usr/share/gdb/auto-load/%{_libdir}
mv -v %{buildroot}/%{_libdir}/*gdb.py %{buildroot}/%{_datadir}/gdb/auto-load/%{_libdir}

cp %{buildroot}/%{_libdir}/libgcc_s.so.1 %{buildroot}/%{_lib}/libgcc_s-%{version}-%{DATE}.so.1
chmod 755 %{buildroot}/%{_lib}/libgcc_s-%{version}-%{DATE}.so.1
ln -sf libgcc_s-%{version}-%{DATE}.so.1 %{buildroot}/%{_lib}/libgcc_s.so.1

rm -f %{buildroot}/%{_infodir}/dir

%files
%defattr(-,root,root,-)
/%{_lib}/cpp
%{_bindir}/c++
%{_bindir}/cc
%{_bindir}/cpp
%{_bindir}/g++
%{_bindir}/gcc
%{_bindir}/gcc-ar
%{_bindir}/gcc-nm
%{_bindir}/gcc-ranlib
%{_bindir}/gcov
%{_bindir}/gcov-tool
%{_bindir}/%{gcc_target_platform}-c++
%{_bindir}/%{gcc_target_platform}-g++
%{_bindir}/%{gcc_target_platform}-gcc
%{_bindir}/%{gcc_target_platform}-gcc-%{version}
%{_bindir}/%{gcc_target_platform}-gcc-ar
%{_bindir}/%{gcc_target_platform}-gcc-nm
%{_bindir}/%{gcc_target_platform}-gcc-ranlib
%{_includedir}/c++/%{version}/*
%{_libdir}/gcc/%{gcc_target_platform}/%{version}/*.o
%{_libdir}/gcc/%{gcc_target_platform}/%{version}/include-fixed/*
%{_libdir}/gcc/%{gcc_target_platform}/%{version}/include/*.h
%{_libdir}/gcc/%{gcc_target_platform}/%{version}/include/sanitizer/*.h
%{_libdir}/gcc/%{gcc_target_platform}/%{version}/install-tools/fixinc_list
%{_libdir}/gcc/%{gcc_target_platform}/%{version}/install-tools/gsyslimits.h
%{_libdir}/gcc/%{gcc_target_platform}/%{version}/install-tools/include/*
%{_libdir}/gcc/%{gcc_target_platform}/%{version}/install-tools/macro_list
%{_libdir}/gcc/%{gcc_target_platform}/%{version}/install-tools/mkheaders.conf
%{_libdir}/gcc/%{gcc_target_platform}/%{version}/libgcc*.a
%{_libdir}/gcc/%{gcc_target_platform}/%{version}/libgcov.a
%{_libdir}/gcc/%{gcc_target_platform}/%{version}/plugin/gtype.state
%{_libdir}/gcc/%{gcc_target_platform}/%{version}/plugin/include/*
%{_libdir}/gcc/%{gcc_target_platform}/%{version}/cc1
%{_libdir}/gcc/%{gcc_target_platform}/%{version}/cc1plus
%{_libdir}/gcc/%{gcc_target_platform}/%{version}/collect2
%{_libdir}/gcc/%{gcc_target_platform}/%{version}/install-tools/fixincl
%{_libdir}/gcc/%{gcc_target_platform}/%{version}/install-tools/fixinc.sh
%{_libdir}/gcc/%{gcc_target_platform}/%{version}/install-tools/mkheaders
%{_libdir}/gcc/%{gcc_target_platform}/%{version}/install-tools/mkinstalldirs
%{_libdir}/gcc/%{gcc_target_platform}/%{version}/liblto_plugin.la
%{_libdir}/gcc/%{gcc_target_platform}/%{version}/liblto_plugin.so*
%{_libdir}/gcc/%{gcc_target_platform}/%{version}/lto-wrapper
%{_libdir}/gcc/%{gcc_target_platform}/%{version}/lto1
%{_libdir}/gcc/%{gcc_target_platform}/%{version}/plugin/gengtype
%{_libdir}/libasan.*a
%{_libdir}/libasan.so*
%{_libdir}/libasan_preinit.o
%{_libdir}/libatomic.*a
%{_libdir}/libatomic.so*
%{_libdir}/libitm.*a
%{_libdir}/libitm.so*
%{_libdir}/libitm.spec
%{_libdir}/libquadmath.*a
%{_libdir}/libquadmath.so*
%{_libdir}/libsupc++.*a
%{_libdir}/libsanitizer.spec
%{_libdir}/libubsan.*
%{_libdir}/libvtv.*
%ifarch x86_64
%{_libdir}/liblsan.*a
%{_libdir}/liblsan.so*
%{_libdir}/libtsan.*a
%{_libdir}/libtsan.so*
%endif
%{_datadir}/locale/*/LC_MESSAGES/cpplib.mo
%{_datadir}/locale/*/LC_MESSAGES/gcc.mo

%files libgcc
%defattr(-,root,root,-)
/%{_lib}/libgcc_s*.so.1
%{_libdir}/libgcc_s.so*

%files libstdc++
%defattr(-,root,root,-)
%{_libdir}/libstdc++.*a
%{_libdir}/libstdc++.so*
%{_datadir}/gdb/auto-load/%{_libdir}/libstdc++.so.*-gdb.py
%{_datadir}/gcc-%{version}/python/libstdcxx/__init__.py
%{_datadir}/gcc-%{version}/python/libstdcxx/v6/*.py

%files libgomp
%defattr(-,root,root,-)
%{_libdir}/libgomp.*a
%{_libdir}/libgomp.so*
%{_libdir}/libgomp.spec
%{_libdir}/libgomp-plugin-host_nonshm.la
%{_libdir}/libgomp-plugin-host_nonshm.so*

%files libssp
%defattr(-,root,root,-)
%{_libdir}/libssp.*a
%{_libdir}/libssp.so*
%{_libdir}/libssp_nonshared.*a
%{_libdir}/gcc/%{gcc_target_platform}/%{version}/include/ssp/*.h

%files libcilkrts
%defattr(-,root,root,-)
%{_libdir}/libcilkrts.*
%{_libdir}/gcc/%{gcc_target_platform}/%{version}/include/cilk/*.h

%files libcc1
%defattr(-,root,root,-)
%{_libdir}/libcc1.la
%{_libdir}/libcc1.so*
%{_libdir}/gcc/%{gcc_target_platform}/%{version}/plugin/libcc1plugin.la
%{_libdir}/gcc/%{gcc_target_platform}/%{version}/plugin/libcc1plugin.so*

%files gfortran
%defattr(-,root,root,-)
%{_bindir}/gfortran
%{_bindir}/%{gcc_target_platform}-gfortran
%{_libdir}/gcc/%{gcc_target_platform}/%{version}/finclude/*
%{_libdir}/gcc/%{gcc_target_platform}/%{version}/libgfortranbegin.*a
%{_libdir}/gcc/%{gcc_target_platform}/%{version}/libcaf_single.*a
%{_libdir}/gcc/%{gcc_target_platform}/%{version}/f951
%{_libdir}/libgfortran.*a
%{_libdir}/libgfortran.so*
%{_libdir}/libgfortran.spec

%files gnat
%defattr(-,root,root,-)
%{_bindir}/gnat
%{_bindir}/gnatbind
%{_bindir}/gnatchop
%{_bindir}/gnatclean
%{_bindir}/gnatfind
%{_bindir}/gnatkr
%{_bindir}/gnatlink
%{_bindir}/gnatls
%{_bindir}/gnatmake
%{_bindir}/gnatname
%{_bindir}/gnatprep
%{_bindir}/gnatxref
%{_libdir}/gcc/%{gcc_target_platform}/%{version}/adainclude/*
%{_libdir}/gcc/%{gcc_target_platform}/%{version}/adalib/*
%{_libdir}/gcc/%{gcc_target_platform}/%{version}/gnat1

%files go
%defattr(-,root,root,-)
%{_bindir}/gccgo
%{_bindir}/go
%{_bindir}/gofmt
%{_bindir}/%{gcc_target_platform}-gccgo
%{_libdir}/gcc/%{gcc_target_platform}/%{version}/cgo
%{_libdir}/gcc/%{gcc_target_platform}/%{version}/go1
%{_libdir}/go/%{version}/%{gcc_target_platform}/*
%{_libdir}/libgo.*a
%{_libdir}/libgo.so*
%{_libdir}/libgobegin.a
%{_libdir}/libgolibbegin.a
%{_libdir}/libnetgo.a
%{_infodir}/gccgo.info.gz
%{_mandir}/man1/gccgo.1.gz

%files objc
%defattr(-,root,root,-)
%{_libdir}/gcc/%{gcc_target_platform}/%{version}/include/objc/*.h
%{_libdir}/gcc/%{gcc_target_platform}/%{version}/cc1obj
%{_libdir}/libobjc.*a
%{_libdir}/libobjc.so*

%files objc++
%defattr(-,root,root,-)
%{_libdir}/gcc/%{gcc_target_platform}/%{version}/cc1objplus

%files doc
%defattr(-,root,root,-)
%{_infodir}/*.info.gz
%{_mandir}/man*/*.gz

%post doc
if [ -f %{_infodir}/gcc.info.gz ]; then 
  install-info \
    --info-dir=%{_infodir} %{_infodir}/gcc.info.gz || : 
fi

%preun doc
if [ $1 = 0 -a -f %{_infodir}/gcc.info.gz ]; then 
  install-info --delete \
    --info-dir=%{_infodir} %{_infodir}/gcc.info.gz || : 
fi

%clean
rm -rf %{buildroot}
rm -rf ../gcc-build

%changelog
* Thu Sep 17 2015 tanggeliang <tanggeliang@gmail.com>
- update from 4.9.2 to 5.2.0
- 'make bootstrap' to 'make'
- remove java package.
* Tue Nov 18 2014 tanggeliang <tanggeliang@gmail.com>
- update from 4.9.1 to 4.9.2
* Sun Jul 20 2014 tanggeliang <tanggeliang@gmail.com>
- update from 4.8.2 to 4.9.1
* Sun Dec 1 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.8.1 to 4.8.2
* Fri Jul 5 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.8.0 to 4.8.1
- no ffi in 4.8.1
* Sat Mar 30 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.7.2 to 4.8.0
* Tue Oct 9 2012 tanggeliang <tanggeliang@gmail.com>
- update from 4.7.1 to 4.7.2
- "disable-bootstrap" to "enable-bootstrap"
- "make" to "make bootstrap"
- "c,c++" to "c,c++,ada,fortran,java,objc,obj-c++".
* Thu Oct 4 2012 tanggeliang <tanggeliang@gmail.com>
- add arch x86-64.
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
