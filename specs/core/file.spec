Name:       file
Version:    5.22
Release:    1%{?dist}
Summary:    File

Group:      System Environment/Base
License:    GPLv2+
Url:        ftp://ftp.astron.com/pub/file
Source:     ftp://ftp.astron.com/pub/file/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build

%description
The File package contains a utility for determining the type of a given file or files. 

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/file
%{_includedir}/magic.h
%{_libdir}/libmagic.*a
%{_libdir}/libmagic.so*
%{_datadir}/misc/magic.mgc

%files doc
%defattr(-,root,root,-)
%{_mandir}/man*/*.gz

%clean
rm -rf %{buildroot}

%changelog
* Sat Mar 28 2015 tanggeliang <tanggeliang@gmail.com>
- update from 5.20 to 5.22
* Wed Nov 19 2014 tanggeliang <tanggeliang@gmail.com>
- update from 5.19 to 5.20
* Sat Jul 19 2014 tanggeliang <tanggeliang@gmail.com>
- update from 5.17 to 5.19
* Fri Feb 28 2014 tanggeliang <tanggeliang@gmail.com>
- update from 5.16 to 5.17
* Tue Dec 17 2013 tanggeliang <tanggeliang@gmail.com>
- update from 5.15 to 5.16
* Sun Oct 20 2013 tanggeliang <tanggeliang@gmail.com>
- update from 5.14 to 5.15
* Wed May 29 2013 tanggeliang <tanggeliang@gmail.com>
- update from 5.13 to 5.14
* Sun Mar 17 2013 tanggeliang <tanggeliang@gmail.com>
- update from 5.11 to 5.13
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
