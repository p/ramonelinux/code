Name:       sed
Version:    4.2.2
Release:    2%{?dist}
Summary:    Sed

Group:      System Environment/Base
License:    GPLv2+
Url:        http://www.gnu.org
Source:     http://ftp.gnu.org/gnu/sed/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build

%description
The Sed package contains a stream editor.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --bindir=/bin --htmldir=/usr/share/doc/sed-%{version} &&
make %{?_smp_mflags}
make html %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

make -C doc install-html DESTDIR=%{buildroot}

rm -f %{buildroot}/%{_infodir}/dir

%files
%defattr(-,root,root,-)
/bin/sed
%{_datadir}/locale/*/LC_MESSAGES/sed.mo

%files doc
%defattr(-,root,root,-)
%{_docdir}/sed-%{version}/sed.html
%{_infodir}/sed.info.gz
%{_mandir}/man1/sed.1.gz

%clean
rm -rf %{buildroot}

%changelog
* Sun Mar 17 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.2.1 to 4.2.2
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
