Name:       bzip
Version:    1.0.6
Release:    9%{?dist}
Summary:    Bzip2

Group:      System Environment/Base
License:    GPLv2+
Url:        http://www.bzip.org
Source:     http://www.bzip.org/1.0.6/%{name}2-%{version}.tar.gz
Patch:      bzip2-1.0.6-install_docs-1.patch

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build

%description
The Bzip2 package contains programs for compressing and decompressing files.
Compressing text files with bzip2 yields a much better compression percentage than with the traditional gzip.

%package        static
Summary:        static
%description    static

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}2-%{version}
%patch -p1

%build
sed -i 's@\(ln -s -f \)$(PREFIX)/bin/@\1@' Makefile
make -f Makefile-libbz2_so
make clean
make %{?_smp_mflags}

%check

%install
make PREFIX=%{buildroot}/usr install

mkdir -p %{buildroot}/{bin,%{_lib},%{_libdir}}
cp -v bzip2-shared %{buildroot}/bin/bzip2
cp -av libbz2.so* %{buildroot}/%{_lib}
ln -sv ../../%{_lib}/libbz2.so.1.0 %{buildroot}/%{_libdir}/libbz2.so
rm -v %{buildroot}/usr/bin/{bunzip2,bzcat,bzip2}
ln -sv bzip2 %{buildroot}/bin/bunzip2
ln -sv bzip2 %{buildroot}/bin/bzcat

%files
%defattr(-,root,root,-)
/bin/bunzip2
/bin/bzcat
/bin/bzip2
/%{_lib}/libbz2.so.1.0*
%{_bindir}/bzcmp
%{_bindir}/bzdiff
%{_bindir}/bzegrep
%{_bindir}/bzfgrep
%{_bindir}/bzgrep
%{_bindir}/bzip2recover
%{_bindir}/bzless
%{_bindir}/bzmore
%{_includedir}/bzlib.h
%{_libdir}/libbz2.*

%files static
%defattr(-,root,root,-)
/usr/lib/libbz2.a

%files doc
%defattr(-,root,root,-)
%{_docdir}/bzip2-1.0.6/*
/usr/man/man1/*.1.gz

%clean
rm -rf %{buildroot}

%changelog
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
