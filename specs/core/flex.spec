Name:       flex
Version:    2.5.39
Release:    2%{?dist}
Summary:    Flex

Group:      Development/Tools
License:    GPLv2+
Url:        http://flex.sourceforge.net
Source:     http://prdownloads.sourceforge.net/flex/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  m4

%description
The Flex package contains a utility for generating programs that recognize patterns in text.

%package        libfl
Summary:        libfl
%description    libfl

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
sed -i -e '/test-bison/d' tests/Makefile.in

./configure --prefix=/usr --docdir=/usr/share/doc/flex-%{version} \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

ln -s flex %{buildroot}/usr/bin/lex

rm -f %{buildroot}/%{_infodir}/dir

%files
%defattr(-,root,root,-)
%{_bindir}/flex
%{_bindir}/flex++
%{_bindir}/lex
%{_includedir}/FlexLexer.h
%{_libdir}/libfl.a
%{_libdir}/libfl_pic.a
%{_datadir}/locale/*/LC_MESSAGES/flex.mo

%files libfl
%defattr(-,root,root,-)
%{_libdir}/libfl.la
%{_libdir}/libfl.so*
%{_libdir}/libfl_pic.la
%{_libdir}/libfl_pic.so*

%files doc
%defattr(-,root,root,-)
%{_docdir}/flex-%{version}/*
%{_infodir}/flex.info*.gz
%{_mandir}/man1/flex.1.gz

%clean
rm -rf %{buildroot}

%changelog
* Tue Mar 31 2015 tanggeliang <tanggeliang@gmail.com>
- fix "/usr/lib/libfl.so: undefined reference to yylex" in "glusterfs" and "checkpolicy".
- Split into two packages: flex and libfl.
* Sat Jul 19 2014 tanggeliang <tanggeliang@gmail.com>
- update from 2.5.38 to 2.5.39
* Fri Feb 28 2014 tanggeliang <tanggeliang@gmail.com>
- update from 2.5.37 to 2.5.38
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
