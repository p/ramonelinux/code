Name:       make
Version:    4.1
Release:    1%{?dist}
Summary:    Make

Group:      Development/Tools
License:    GPLv2+
Url:        http://www.gnu.org
Source:     http://ftp.gnu.org/gnu/make/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build

%description
The Make package contains a program for compiling packages.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

rm -f %{buildroot}/%{_infodir}/dir

%files
%defattr(-,root,root,-)
%{_bindir}/make
%{_includedir}/gnumake.h
%{_datadir}/locale/*/LC_MESSAGES/make.mo

%files doc
%defattr(-,root,root,-)
%{_infodir}/make.info*.gz
%{_mandir}/man1/make.1.gz

%clean
rm -rf %{buildroot}

%changelog
* Fri Mar 27 2015 tanggeliang <tanggeliang@gmail.com>
- update from 4.0 to 4.1
* Sun Oct 20 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.82 to 4.0
* Sun Sep 30 2012 tanggeliang <tanggeliang@gmail.com>
- add make-3.82-upstream_fixes-3.patch fix WebKitGTK+ build fail.
- remove make-3.82-bugfixes.patch, conflicts with make-3.82-upstream_fixes-3.patch.
* Tue Aug 28 2012 tanggeliang <tanggeliang@gmail.com>
- add make-3.82-bugfixes.patch fix isomd5sum build error: No rule to make target `libimplantisomd5.a.
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
