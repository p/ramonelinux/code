Name:       gettext
Version:    0.19.4
Release:    1%{?dist}
Summary:    Gettext

Group:      System Environment/Base
License:    GPLv2+
Url:        http://www.gnu.org
Source:     http://ftp.gnu.org/gnu/gettext/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build

%description
The Gettext package contains utilities for internationalization and localization.
These allow programs to be compiled with NLS (Native Language Support), enabling them to output messages in the user's native language.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr    \
            --disable-static \
	    --docdir=%{_docdir}/gettext-%{version} \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

rm -f %{buildroot}/%{_infodir}/dir

%files
%defattr(-,root,root,-)
%{_bindir}/autopoint
%{_bindir}/envsubst
%{_bindir}/gettext
%{_bindir}/gettext.sh
%{_bindir}/gettextize
%{_bindir}/msgattrib
%{_bindir}/msgcat
%{_bindir}/msgcmp
%{_bindir}/msgcomm
%{_bindir}/msgconv
%{_bindir}/msgen
%{_bindir}/msgexec
%{_bindir}/msgfilter
%{_bindir}/msgfmt
%{_bindir}/msggrep
%{_bindir}/msginit
%{_bindir}/msgmerge
%{_bindir}/msgunfmt
%{_bindir}/msguniq
%{_bindir}/ngettext
%{_bindir}/recode-sr-latin
%{_bindir}/xgettext
%{_includedir}/autosprintf.h
%{_includedir}/gettext-po.h
%{_libdir}/gettext/*
%{_libdir}/libasprintf.*
%{_libdir}/libgettextlib*
%{_libdir}/libgettextpo.*
%{_libdir}/libgettextsrc*
%{_libdir}/preloadable_libintl.so
%{_datadir}/aclocal/*.m4
%{_datadir}/gettext/*
%{_datadir}/locale/*/LC_MESSAGES/gettext-runtime.mo
%{_datadir}/locale/*/LC_MESSAGES/gettext-tools.mo

%files doc
%defattr(-,root,root,-)
%{_docdir}/gettext-%{version}/*
%{_infodir}/autosprintf.info.gz
%{_infodir}/gettext.info.gz
%{_mandir}/man*/*.gz

%clean
rm -rf %{buildroot}

%changelog
* Sat Mar 28 2015 tanggeliang <tanggeliang@gmail.com>
- update from 0.19.2 to 0.19.4
* Fri Sep 19 2014 tanggeliang <tanggeliang@gmail.com>
- update from 0.19.1 to 0.19.2
* Sat Jul 19 2014 tanggeliang <tanggeliang@gmail.com>
- update from 0.18.3.2 to 0.19.1
* Fri Feb 28 2014 tanggeliang <tanggeliang@gmail.com>
- update from 0.18.3.1 to 0.18.3.2
* Thu Oct 3 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.18.3 to 0.18.3.1
* Wed Jul 31 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.18.2.1 to 0.18.3
* Wed May 29 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.18.2 to 0.18.2.1
* Sun Mar 17 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.18.1.1 to 0.18.2
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
