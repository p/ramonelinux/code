Name:       xz
Version:    5.2.1
Release:    1%{?dist}
Summary:    Xz

Group:      System Environment/Base
License:    GPLv2+
Url:        http://www.zlib.net
Source:     http://www.zlib.net/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build

%description
The Xz package contains programs for compressing and decompressing files.
It provides capabilities for the lzma and the newer xz compression formats.
Compressing text files with xz yields a better compression percentage than with the traditional gzip or bzip2 commands.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
	    --disable-static \
	    --docdir=%{_docdir}/xz-%{version} \
	    --libdir=/%{_lib} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make pkgconfigdir=%{_libdir}/pkgconfig install DESTDIR=%{buildroot}

%files
%defattr(-,root,root,-)
%{_bindir}/lzcat
%{_bindir}/lzcmp
%{_bindir}/lzdiff
%{_bindir}/lzegrep
%{_bindir}/lzfgrep
%{_bindir}/lzgrep
%{_bindir}/lzless
%{_bindir}/lzma
%{_bindir}/lzmadec
%{_bindir}/lzmainfo
%{_bindir}/lzmore
%{_bindir}/unlzma
%{_bindir}/unxz
%{_bindir}/xz
%{_bindir}/xzcat
%{_bindir}/xzcmp
%{_bindir}/xzdec
%{_bindir}/xzdiff
%{_bindir}/xzegrep
%{_bindir}/xzfgrep
%{_bindir}/xzgrep
%{_bindir}/xzless
%{_bindir}/xzmore
%{_includedir}/lzma.h
%{_includedir}/lzma/*.h
/%{_lib}/liblzma.*a
/%{_lib}/liblzma.so*
%{_libdir}/pkgconfig/liblzma.pc
%{_datadir}/locale/*/LC_MESSAGES/xz.mo

%files doc
%defattr(-,root,root,-)
%{_docdir}/xz-%{version}/*
%{_mandir}/man1/*.1.gz

%clean
rm -rf %{buildroot}

%changelog
* Sat Mar 28 2015 tanggeliang <tanggeliang@gmail.com>
- update from 5.0.5 to 5.2.1
* Thu Jul 25 2013 tanggeliang <tanggeliang@gmail.com>
- update from 5.0.4 to 5.0.5
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
