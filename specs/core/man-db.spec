Name:       man-db
Version:    2.7.1
Release:    1%{?dist}
Summary:    Man-DB

Group:      System Environment/Base
License:    GPLv2+
Url:        http://www.gnu.org
Source:     http://download.savannah.gnu.org/releases/man-db/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  libpipeline groff gdbm

%description
The Man-DB package contains programs for finding and viewing man pages.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr                         \
            --docdir=%{_docdir}/man-db-%{version} \
            --sysconfdir=/etc                     \
            --disable-setuid                      \
            --with-browser=/usr/bin/lynx          \
            --with-vgrind=/usr/bin/vgrind         \
            --with-grap=/usr/bin/grap             \
            --libexecdir=%{_libdir}               \
            --libdir=%{_libdir}                   \
            --with-systemdtmpfilesdir=%{_libdir}/tmpfiles.d &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_sysconfdir}/man_db.conf
%{_bindir}/apropos
%{_bindir}/catman
%{_bindir}/lexgrog
%{_bindir}/man
%{_bindir}/mandb
%{_bindir}/manpath
%{_bindir}/whatis
%{_libdir}/tmpfiles.d/man-db.conf
%{_libdir}/man-db/globbing
%{_libdir}/man-db/libman.la
%{_libdir}/man-db/libman*.so
%{_libdir}/man-db/libmandb.la
%{_libdir}/man-db/manconv
%{_libdir}/man-db/zsoelim
%{_sbindir}/accessdb
%{_datadir}/locale/*/LC_MESSAGES/man-db*.mo

%files doc
%defattr(-,root,root,-)
%{_docdir}/man-db-%{version}/man-db-manual.*
%{_mandir}/*/man*/*.gz
%{_mandir}/man*/*.gz

%clean
rm -rf %{buildroot}

%changelog
* Wed Nov 19 2014 tanggeliang <tanggeliang@gmail.com>
- update from 2.6.6 to 2.7.1
* Fri Feb 28 2014 tanggeliang <tanggeliang@gmail.com>
- update from 2.6.5 to 2.6.6
* Sat Aug 24 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.6.3 to 2.6.5
* Sun Oct 14 2012 tanggeliang <tanggeliang@gmail.com>
- update from 2.6.2 to 2.6.3
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
