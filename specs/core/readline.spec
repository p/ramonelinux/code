Name:       readline
Version:    6.3
Release:    1%{?dist}
Summary:    Readline

Group:      System Environment/core
License:    GPLv2+
Url:        http://www.gnu.org
Source:     http://ftp.gnu.org/gnu/readline/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build

%description
The Readline package is a set of libraries that offers command-line editing and history capabilities.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
sed -i '/MV.*old/d' Makefile.in
sed -i '/{OLDSUFF}/c:' support/shlib-install

./configure --prefix=/usr --docdir=/usr/share/doc/readline-%{version} \
            --libdir=%{_libdir} &&
make SHLIB_LIBS=-lncurses %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

mkdir -p %{buildroot}/%{_lib}
mv -v %{buildroot}/%{_libdir}/lib{readline,history}.so.* %{buildroot}/%{_lib}

ln -sfv ../../%{_lib}/$(readlink %{buildroot}/%{_libdir}/libreadline.so) %{buildroot}/%{_libdir}/libreadline.so
ln -sfv ../../%{_lib}/$(readlink %{buildroot}/%{_libdir}/libhistory.so ) %{buildroot}/%{_libdir}/libhistory.so

install -v -m644 doc/*.{ps,pdf,html,dvi} %{buildroot}/usr/share/doc/readline-%{version}

rm -f %{buildroot}/%{_infodir}/dir

%files
%defattr(-,root,root,-)
/%{_lib}/libhistory.so.6*
/%{_lib}/libreadline.so.6*
%{_includedir}/readline/*.h
%{_libdir}/libhistory.*
%{_libdir}/libreadline.*
%{_datadir}/readline/*

%files doc
%defattr(-,root,root,-)
%{_docdir}/readline-%{version}/*
%{_infodir}/history.info.gz
%{_infodir}/readline.info.gz
%{_infodir}/rluserman.info.gz
%{_mandir}/man3/history.3.gz
%{_mandir}/man3/readline.3.gz

%clean
rm -rf %{buildroot}

%changelog
* Sat Mar 15 2014 tanggeliang <tanggeliang@gmail.com>
- update from 6.2 to 6.3
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
