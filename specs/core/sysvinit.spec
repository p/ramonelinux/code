Name:       sysvinit
Version:    2.88
Release:    10%{?dist}
Summary:    Sysvinit

Group:      System Environment/Base
License:    GPLv2+
Url:        http://ftp.twaren.net/Unix/NonGNU/sysvinit
Source:     http://ftp.twaren.net/Unix/NonGNU/sysvinit/%{name}-%{version}dsf.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build

%description
The Sysvinit package contains programs for controlling the startup, running, and shutdown of the system.

%package        utils
Summary:        utils
%description    utils

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}dsf

%build
sed -i 's@Sending processes@& configured via /etc/inittab@g' src/init.c
sed -i -e '/utmpdump/d' \
       -e '/mountpoint/d' src/Makefile
make -C src %{?_smp_mflags}

%check

%install
mkdir -pv %{buildroot}/{etc,bin,sbin,usr,usr/{bin,include,share},usr/share/man/man{1,5,8}}
make -C src install ROOT=%{buildroot}

cat > %{buildroot}/etc/inittab << "EOF"
# Begin /etc/inittab

id:3:initdefault:

si::sysinit:/etc/rc.d/init.d/rc S

l0:0:wait:/etc/rc.d/init.d/rc 0
l1:S1:wait:/etc/rc.d/init.d/rc 1
l2:2:wait:/etc/rc.d/init.d/rc 2
l3:3:wait:/etc/rc.d/init.d/rc 3
l4:4:wait:/etc/rc.d/init.d/rc 4
l5:5:wait:/etc/rc.d/init.d/rc 5
l6:6:wait:/etc/rc.d/init.d/rc 6

ca:12345:ctrlaltdel:/sbin/shutdown -t1 -a -r now

su:S016:once:/sbin/sulogin

1:2345:respawn:/sbin/agetty --noclear tty1 9600
2:2345:respawn:/sbin/agetty tty2 9600
3:2345:respawn:/sbin/agetty tty3 9600
4:2345:respawn:/sbin/agetty tty4 9600
5:2345:respawn:/sbin/agetty tty5 9600
6:2345:respawn:/sbin/agetty tty6 9600

kd:5:respawn:/usr/bin/kdm

# End /etc/inittab
EOF

%files
%defattr(-,root,root,-)
%{_sysconfdir}/inittab
/sbin/bootlogd
/sbin/halt
/sbin/init
/sbin/poweroff
/sbin/reboot
/sbin/runlevel
/sbin/shutdown
/sbin/sulogin
/sbin/telinit
%{_bindir}/last
%{_bindir}/lastb
%{_bindir}/mesg
%{_includedir}/initreq.h

%files utils
%defattr(-,root,root,-)
/bin/pidof
/sbin/fstab-decode
/sbin/killall5

%files doc
%defattr(-,root,root,-)
%{_mandir}/man*/*.gz

%clean
rm -rf %{buildroot}

%changelog
* Sun Mar 3 2013 tanggeliang <tanggeliang@gmail.com>
* add kdm in inittab
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
