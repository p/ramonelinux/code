Name:       ncurses
Version:    5.9
Release:    7%{?dist}
Summary:    Ncurses

Group:      System Environment/core
License:    GPLv2+
Url:        http://www.gnu.org
Source:     ftp://ftp.gnu.org/gnu/ncurses/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build

%description
The Ncurses package contains libraries for terminal-independent handling of character screens.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr           \
            --mandir=/usr/share/man \
            --with-shared           \
            --without-debug         \
            --enable-pc-files       \
            --enable-widec          \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} PKG_CONFIG_LIBDIR=%{_libdir}/pkgconfig install

mkdir -pv %{buildroot}/%{_lib}
mv -v %{buildroot}/%{_libdir}/libncursesw.so.5* %{buildroot}/%{_lib}

ln -sfv ../../%{_lib}/$(readlink %{buildroot}/%{_libdir}/libncursesw.so) %{buildroot}/%{_libdir}/libncursesw.so

for lib in ncurses form panel menu ; do
    rm -vf                    %{buildroot}/%{_libdir}/lib${lib}.so
    echo "INPUT(-l${lib}w)" > %{buildroot}/%{_libdir}/lib${lib}.so
    ln -sfv lib${lib}w.a      %{buildroot}/%{_libdir}/lib${lib}.a
    ln -sfv ${lib}w.pc        %{buildroot}/%{_libdir}/pkgconfig/${lib}.pc
done
ln -sfv libncurses++w.a %{buildroot}/%{_libdir}/libncurses++.a

rm -vf %{buildroot}/%{_libdir}/libcursesw.so
echo "INPUT(-lncursesw)" > %{buildroot}/%{_libdir}/libcursesw.so
ln -sfv libncurses.so %{buildroot}/%{_libdir}/libcurses.so
ln -sfv libncursesw.a %{buildroot}/%{_libdir}/libcursesw.a
ln -sfv libncurses.a %{buildroot}/%{_libdir}/libcurses.a

mkdir -pv      %{buildroot}/usr/share/doc/ncurses-%{version}
cp -v -R doc/* %{buildroot}/usr/share/doc/ncurses-%{version}

%files
%defattr(-,root,root,-)
/%{_lib}/libncursesw.so.5*
%{_bindir}/captoinfo
%{_bindir}/clear
%{_bindir}/infocmp
%{_bindir}/infotocap
%{_bindir}/ncursesw5-config
%{_bindir}/reset
%{_bindir}/tabs
%{_bindir}/tic
%{_bindir}/toe
%{_bindir}/tput
%{_bindir}/tset
%{_includedir}/*.h
%{_libdir}/libcurses*.*
%{_libdir}/libform*.*
%{_libdir}/libmenu*.*
%{_libdir}/libncurses*.*
%{_libdir}/libpanel*.*
%{_libdir}/pkgconfig/form.pc
%{_libdir}/pkgconfig/menu.pc
%{_libdir}/pkgconfig/ncurses.pc
%{_libdir}/pkgconfig/panel.pc
%{_libdir}/pkgconfig/formw.pc
%{_libdir}/pkgconfig/menuw.pc
%{_libdir}/pkgconfig/ncurses++w.pc
%{_libdir}/pkgconfig/ncursesw.pc
%{_libdir}/pkgconfig/panelw.pc
%ifarch %{ix86}
%{_libdir}/terminfo
%endif
%{_datadir}/tabset/*
%{_datadir}/terminfo/*

%files doc
%defattr(-,root,root,-)
%{_docdir}/ncurses-%{version}/*
%{_mandir}/man*/*.gz

%clean
rm -rf %{buildroot}

%changelog
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
