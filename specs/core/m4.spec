Name:       m4
Version:    1.4.17
Release:    1%{?dist}
Summary:    M4

Group:      System Environment/Base
License:    GPLv2+
Url:        http://www.gnu.org
Source:     http://ftp.gnu.org/gnu/m4/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build

%description
The M4 package contains a macro processor.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

rm -f %{buildroot}/%{_infodir}/dir

%files
%defattr(-,root,root,-)
%{_bindir}/m4

%files doc
%defattr(-,root,root,-)
%{_infodir}/m4.info*.gz
%{_mandir}/man1/m4.1.gz

%clean
rm -rf %{buildroot}

%changelog
* Sun Oct 20 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.4.16 to 1.4.17
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
