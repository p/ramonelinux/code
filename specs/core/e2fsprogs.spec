Name:       e2fsprogs
Version:    1.42.9
Release:    1%{?dist}
Summary:    E2fsprogs

Group:      System Environment/Filesystem
License:    GPLv2+
Url:        http://e2fsprogs.sourceforge.net
Source:     http://prdownloads.sourceforge.net/e2fsprogs/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build

%description
The E2fsprogs package contains the utilities for handling the ext2 file system.
It also supports the ext3 and ext4 journaling file systems.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
mkdir -v build
cd build
../configure --prefix=/usr         \
             --with-root-prefix="" \
             --enable-elf-shlibs   \
             --disable-libblkid    \
             --disable-libuuid     \
             --disable-uuidd       \
             --disable-fsck        \
             --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
cd build
make DESTDIR=%{buildroot} install
make DESTDIR=%{buildroot} install-libs

chmod -v u+w %{buildroot}/%{_libdir}/{libcom_err,libe2p,libext2fs,libss,libquota}.a

%files
%defattr(-,root,root,-)
%{_sysconfdir}/mke2fs.conf
/sbin/badblocks
/sbin/debugfs
/sbin/dumpe2fs
/sbin/e2fsck
/sbin/e2image
/sbin/e2label
/sbin/e2undo
/sbin/fsck.ext2
/sbin/fsck.ext3
/sbin/fsck.ext4
/sbin/fsck.ext4dev
/sbin/logsave
/sbin/mke2fs
/sbin/mkfs.ext2
/sbin/mkfs.ext3
/sbin/mkfs.ext4
/sbin/mkfs.ext4dev
/sbin/resize2fs
/sbin/tune2fs
%{_bindir}/chattr
%{_bindir}/compile_et
%{_bindir}/lsattr
%{_bindir}/mk_cmds
%{_includedir}/com_err.h
%{_includedir}/e2p/e2p.h
%{_includedir}/et/com_err.h
%{_includedir}/ext2fs/*.h
%{_includedir}/quota/mkquota.h
%{_includedir}/ss/ss*.h
%{_libdir}/e2initrd_helper
%{_libdir}/libcom_err.*
%{_libdir}/libe2p.*
%{_libdir}/libext2fs.*
%{_libdir}/libquota.a
%{_libdir}/libss.*
%{_libdir}/pkgconfig/com_err.pc
%{_libdir}/pkgconfig/e2p.pc
%{_libdir}/pkgconfig/ext2fs.pc
%{_libdir}/pkgconfig/quota.pc
%{_libdir}/pkgconfig/ss.pc
%{_sbindir}/e2freefrag
%{_sbindir}/e4defrag
%{_sbindir}/filefrag
%{_sbindir}/mklost+found
%{_datadir}/et/et_*.awk
%{_datadir}/locale/*/LC_MESSAGES/e2fsprogs.mo
%{_datadir}/ss/ct_c.*

%files doc
%defattr(-,root,root,-)
%{_infodir}/libext2fs.info.gz
%{_mandir}/man*/*.gz

%clean
rm -rf %{buildroot}

%changelog
* Sun Jan 5 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.42.8 to 1.42.9
* Tue Jul 2 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.42.7 to 1.42.8
* Sun Mar 17 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.42.5 to 1.42.7
- add libquota in chmod
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
