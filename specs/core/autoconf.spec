Name:       autoconf
Version:    2.69
Release:    7%{?dist}
Summary:    Autoconf

Group:      Development/Tools
License:    GPLv2+
Url:        http://www.gnu.org
Source:     http://ftp.gnu.org/gnu/autoconf/%{name}-%{version}.tar.xz

Distribution: ramone linux
Vendor: ramone
Packager: tanggeliang <tanggeliang@gmail.com>

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires: inetutils m4

%description
The Autoconf package contains programs for producing shell scripts that can automatically configure source code.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

rm -f %{buildroot}/%{_infodir}/dir

%files
%defattr(-,root,root,-)
%{_bindir}/autoconf
%{_bindir}/autoheader
%{_bindir}/autom4te
%{_bindir}/autoreconf
%{_bindir}/autoscan
%{_bindir}/autoupdate
%{_bindir}/ifnames
%{_datadir}/autoconf/Autom4te/*.pm
%{_datadir}/autoconf/INSTALL
%{_datadir}/autoconf/autoconf/*.m4*
%{_datadir}/autoconf/autom4te.cfg
%{_datadir}/autoconf/autoscan/autoscan.list
%{_datadir}/autoconf/autotest/*.m4*
%{_datadir}/autoconf/m4sugar/*.m4*

%files doc
%defattr(-,root,root,-)
%{_infodir}/*.info.gz
%{_mandir}/man1/*.1.gz

%clean
rm -rf %{buildroot}

%changelog
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
