Name:       glibc
Version:    2.22
Release:    1%{?dist}
Summary:    Glibc

Group:      System Environment/Base
License:    GPLv2+
Url:        http://www.gnu.org
Source0:    http://ftp.gnu.org/gnu/%{name}/%{name}-%{version}.tar.xz
Source1:    http://www.iana.org/time-zones/repository/releases/tzdata2015f.tar.gz
Patch0:     %{name}-%{version}-fhs-1.patch
Patch1:     %{name}-%{version}-upstream_i386_fix-1.patch

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  gettext

%description
The Glibc package contains the main C library.
This library provides the basic routines for allocating memory, searching directories, opening and closing files, reading and writing files, string handling, pattern matching, arithmetic, and so on.

%package        devel
Summary:        Glibc devel
Requires:       linux-headers glibc
%description    devel

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}
%patch0 -p1
%patch1 -p1

%build
mkdir -v ../glibc-build
cd ../glibc-build

../glibc-%{version}/configure    \
    --prefix=/usr          \
    --disable-profile      \
    --enable-kernel=2.6.32 \
    --enable-obsolete-rpc  \
    --libexecdir=%{_libdir}/glibc \
    --libdir=%{_libdir}
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
cd ../glibc-build
make install_root=%{buildroot} install

cp -v ../glibc-%{version}/nscd/nscd.conf %{buildroot}/etc/nscd.conf
mkdir -pv %{buildroot}/var/cache/nscd

make install_root=%{buildroot} localedata/install-locales %{?_smp_mflags}

cat > %{buildroot}/etc/nsswitch.conf << "EOF"
# Begin /etc/nsswitch.conf

passwd: files
group: files
shadow: files

hosts: files dns
networks: files

protocols: files
services: files
ethers: files
rpc: files

# End /etc/nsswitch.conf
EOF

export PATH=$PATH:/usr/sbin
tar -xf %SOURCE1

ZONEINFO=/usr/share/zoneinfo
mkdir -pv %{buildroot}/$ZONEINFO/{posix,right} &&

for tz in etcetera southamerica northamerica europe africa antarctica  \
          asia australasia backward pacificnew systemv; do
    zic -L /dev/null   -d %{buildroot}/$ZONEINFO       -y "sh yearistype.sh" ${tz} &&
    zic -L /dev/null   -d %{buildroot}/$ZONEINFO/posix -y "sh yearistype.sh" ${tz} &&
    zic -L leapseconds -d %{buildroot}/$ZONEINFO/right -y "sh yearistype.sh" ${tz}
done

cp -v zone.tab iso3166.tab %{buildroot}/$ZONEINFO &&
zic -d %{buildroot}/$ZONEINFO -p America/New_York
unset ZONEINFO

cp -v --remove-destination %{buildroot}/usr/share/zoneinfo/GMT \
        %{buildroot}/etc/localtime

cat >> %{buildroot}/etc/ld.so.conf << "EOF"
# Add an include directory
include /etc/ld.so.conf.d/*.conf

EOF
mkdir %{buildroot}/etc/ld.so.conf.d

rm -f %{buildroot}/%{_infodir}/dir

%files
%defattr(-,root,root,-)
%{_sysconfdir}/ld.so.cache
%{_sysconfdir}/ld.so.conf
%dir %{_sysconfdir}/ld.so.conf.d
%{_sysconfdir}/localtime
%{_sysconfdir}/nscd.conf
%{_sysconfdir}/nsswitch.conf
%{_sysconfdir}/rpc
/%{_lib}/ld-%{version}.so
%ifarch %{ix86}
/%{_lib}/ld-linux.so.2
%else %ifarch x86_64
/%{_lib}/ld-linux-x86-64.so.2
%endif
/%{_lib}/libBrokenLocale*.so*
/%{_lib}/libSegFault.so
/%{_lib}/libanl*.so*
/%{_lib}/libc-%{version}.so
/%{_lib}/libc.so.6
/%{_lib}/libcidn*.so*
/%{_lib}/libcrypt*.so*
/%{_lib}/libdl*.so*
/%{_lib}/libm-%{version}.so
/%{_lib}/libm.so.6
/%{_lib}/libmemusage.so
/%{_lib}/libmvec*.so*
/%{_lib}/libnsl*.so*
/%{_lib}/libnss_*.so*
/%{_lib}/libpcprofile.so
/%{_lib}/libpthread*.so*
/%{_lib}/libresolv*.so*
/%{_lib}/librt*.so*
/%{_lib}/libthread_db*.so*
/%{_lib}/libutil*.so*
/sbin/ldconfig
/sbin/sln
%{_bindir}/catchsegv
%{_bindir}/gencat
%{_bindir}/getconf
%{_bindir}/getent
%{_bindir}/iconv
%{_bindir}/ldd
%ifarch %{ix86}
%{_bindir}/lddlibc4
%endif
%{_bindir}/locale
%{_bindir}/localedef
%{_bindir}/makedb
%{_bindir}/mtrace
%{_bindir}/pcprofiledump
%{_bindir}/pldd
%{_bindir}/rpcgen
%{_bindir}/sotruss
%{_bindir}/sprof
%{_bindir}/tzselect
%{_bindir}/xtrace
%{_libdir}/libBrokenLocale.so
%{_libdir}/libanl.so
%{_libdir}/libc.so
%{_libdir}/libcidn.so
%{_libdir}/libcrypt.so
%{_libdir}/libdl.so
%{_libdir}/libm.so
%{_libdir}/libmvec.*
%{_libdir}/libnsl.so
%{_libdir}/libnss_*.so
%{_libdir}/libpthread.so
%{_libdir}/libthread_db.so
%{_libdir}/libresolv.so
%{_libdir}/librt.so
%{_libdir}/libutil.so
%{_libdir}/audit/sotruss-lib.so
%{_libdir}/gconv/*.so
%{_libdir}/gconv/gconv-modules
%ifarch %{ix86}
%{_libdir}/glibc/getconf/POSIX_V*_ILP32_OFF*
%{_libdir}/glibc/getconf/XBS5_ILP32_OFF*
%else %ifarch x86_64
%{_libdir}/glibc/getconf/POSIX_V*_LP64_OFF64
%{_libdir}/glibc/getconf/XBS5_LP64_OFF64
%endif
%{_libdir}/locale/locale-archive
%{_sbindir}/iconvconfig
%{_sbindir}/nscd
%{_sbindir}/zdump
%{_sbindir}/zic
%{_datadir}/i18n/charmaps/*
%{_datadir}/i18n/locales/*
%{_datadir}/locale/locale.alias
%{_datadir}/locale/*/LC_MESSAGES/libc.mo
%{_datadir}/zoneinfo/*
%{_var}/cache/nscd
%{_var}/lib/nss_db/Makefile

%files devel
%defattr(-,root,root,-)
%{_includedir}/*.h
%{_includedir}/arpa/*.h
%{_includedir}/bits/*.h
%{_includedir}/bits/stab.def
%{_includedir}/gnu/*.h
%{_includedir}/net*/*.h
%{_includedir}/nfs/nfs.h
%{_includedir}/protocols/*.h
%{_includedir}/rpc/*.h
%{_includedir}/rpcsvc/*.h
%{_includedir}/rpcsvc/*.x
%{_includedir}/scsi/*.h
%{_includedir}/sys/*.h
%{_libdir}/*.o
%{_libdir}/libBrokenLocale.a
%{_libdir}/libanl.a
%{_libdir}/libc.a
%{_libdir}/libc_nonshared.a
%{_libdir}/libcrypt.a
%{_libdir}/libdl.a
%{_libdir}/libg.a
%{_libdir}/libieee.a
%{_libdir}/libm.a
%{_libdir}/libmcheck.a
%{_libdir}/libnsl.a
%{_libdir}/libpthread.a
%{_libdir}/libpthread_nonshared.a
%{_libdir}/libresolv.a
%{_libdir}/librpcsvc.a
%{_libdir}/librt.a
%{_libdir}/libutil.a

%files doc
%defattr(-,root,root,-)
%{_infodir}/libc.info*.gz

%clean
rm -rf %{buildroot}
rm -rf ../glibc-build

%changelog
* Thu Sep 17 2015 tanggeliang <tanggeliang@gmail.com>
- update from 2.21 to 2.22
* Sat Mar 28 2015 tanggeliang <tanggeliang@gmail.com>
- update from 2.20 to 2.21
* Fri Sep 19 2014 tanggeliang <tanggeliang@gmail.com>
- update from 2.19 to 2.20
* Fri Feb 28 2014 tanggeliang <tanggeliang@gmail.com>
- update from 2.18 to 2.19
* Sun Aug 18 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.17 to 2.18
* Sat Feb 23 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.16.0 to 2.17
* Wed Oct 3 2012 tanggeliang <tanggeliang@gmail.com>
- add arch x86-64.
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
