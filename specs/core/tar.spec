Name:       tar
Version:    1.28
Release:    1%{?dist}
Summary:    Tar

Group:      System Environment/Base
License:    GPLv2+
Url:        http://www.gnu.org
Source:     http://ftp.gnu.org/gnu/tar/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build

%description
The Tar package contains an archiving program.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
FORCE_UNSAFE_CONFIGURE=1  \
./configure --prefix=/usr \
            --bindir=/bin \
            --libexecdir=/usr/sbin &&

make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

make -C doc install-html docdir=%{buildroot}/usr/share/doc/tar-%{version}

rm -f %{buildroot}/%{_infodir}/dir

%files
%defattr(-,root,root,-)
/bin/tar
%{_sbindir}/rmt
%{_datadir}/locale/*/LC_MESSAGES/tar.mo

%files doc
%defattr(-,root,root,-)
%{_docdir}/tar-%{version}/tar.html/*
%{_infodir}/tar.info*.gz
%{_mandir}/man1/tar.1.gz
%{_mandir}/man8/rmt.8.gz

%clean
rm -rf %{buildroot}

%changelog
* Fri Sep 19 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.27.1 to 1.28
* Sun Dec 1 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.27 to 1.27.1
* Sun Oct 20 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.26 to 1.27
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
