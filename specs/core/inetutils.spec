Name:       inetutils
Version:    1.9.2
Release:    1%{?dist}
Summary:    Inetutils

Group:      System Environment/Base
License:    GPLv2+
Url:        http://www.gnu.org
Source:     http://ftp.gnu.org/gnu/inetutils/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build

%description
The Inetutils package contains programs for basic networking.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
echo '#define PATH_PROCNET_DEV "/proc/net/dev"' >> ifconfig/system/linux.h 

./configure --prefix=/usr  \
    --localstatedir=/var   \
    --disable-logger       \
    --disable-syslogd      \
    --disable-whois        \
    --disable-servers      \
    --enable-tftpd         \
    --libexecdir=/usr/sbin &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

mkdir %{buildroot}/{bin,sbin}
mv -v %{buildroot}/usr/bin/{hostname,ping,ping6,traceroute} %{buildroot}/bin
mv -v %{buildroot}/usr/bin/ifconfig %{buildroot}/sbin

rm -f %{buildroot}/%{_infodir}/dir

%files
%defattr(-,root,root,-)
/bin/hostname
%attr(4755,root,root) /bin/ping
%attr(4755,root,root) /bin/ping6
/bin/traceroute
/sbin/ifconfig
%{_bindir}/dnsdomainname
%{_bindir}/ftp
%{_bindir}/rcp
%{_bindir}/rexec
%{_bindir}/rlogin
%{_bindir}/rsh
%{_bindir}/talk
%{_bindir}/telnet
%{_bindir}/tftp
%{_sbindir}/tftpd

%files doc
%defattr(-,root,root,-)
%{_infodir}/inetutils.info.gz
%{_mandir}/man1/*.1.gz
%{_mandir}/man8/*.8.gz

%clean
rm -rf %{buildroot}

%changelog
* Fri Feb 28 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.9.1 to 1.9.2
* Fri Nov 29 2013 tanggeliang <tanggeliang@gmail.com>
- add "--enable-tftpd"
* Sun Sep 16 2012 tanggeliang <tanggeliang@gmail.com>
- remove "--disable-ifconfig", add "--disable-whois"
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
