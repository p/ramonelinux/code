Name:       libtool
Version:    2.4.3
Release:    1%{?dist}
Summary:    Libtool

Group:      System Environment/Base
License:    GPLv2+
Url:        http://www.gnu.org
Source:     http://ftp.gnu.org/gnu/libtool/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build

%description
The Libtool package contains the GNU generic library support script.
It wraps the complexity of using shared libraries in a consistent, portable interface.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

rm -f %{buildroot}/%{_infodir}/dir

%files
%defattr(-,root,root,-)
%{_bindir}/libtool
%{_bindir}/libtoolize
%{_includedir}/libltdl/lt_*.h
%{_includedir}/ltdl.h
%{_libdir}/libltdl.*a
%{_libdir}/libltdl.so*
%{_datadir}/aclocal/*.m4
%{_datadir}/libtool/*

%files doc
%defattr(-,root,root,-)
%{_infodir}/libtool.info*.gz
%{_mandir}/man1/libtool*.1.gz

%clean
rm -rf %{buildroot}

%changelog
* Wed Nov 19 2014 tanggeliang <tanggeliang@gmail.com>
- update from 2.4.2 to 2.4.3
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
