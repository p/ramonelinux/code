Name:       binutils
Version:    2.25.1
Release:    1%{?dist}
Summary:    Binutils

Group:      System Environment/tools
License:    GPLv2+
Url:        http://www.gnu.org
Source:     http://ftp.gnu.org/gnu/binutils/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  expect
Requires:       binutils-libbfd binutils-strip

%description
The Binutils package contains a linker, an assembler, and other tools for handling object files. 

%package        strip
Summary:        strip
%description    strip

%package        libbfd
Summary:        libbfd
%description    libbfd

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
expect -c "spawn ls"

mkdir -pv ../binutils-build
cd ../binutils-build
../binutils-%{version}/configure --prefix=/usr \
				 --enable-shared \
                           	 --disable-werror \
                                 --libdir=%{_libdir} &&
make tooldir=/usr %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
cd ../binutils-build
make DESTDIR=%{buildroot} tooldir=/usr install

cp -v %{_builddir}/binutils-%{version}/include/libiberty.h %{buildroot}/usr/include
rm -f %{buildroot}/%{_infodir}/dir

%files
%defattr(-,root,root,-)
%{_bindir}/addr2line
%{_bindir}/ar
%{_bindir}/as
%{_bindir}/c++filt
%{_bindir}/elfedit
%{_bindir}/gprof
%{_bindir}/ld
%{_bindir}/ld.bfd
%{_bindir}/nm
%{_bindir}/objcopy
%{_bindir}/objdump
%{_bindir}/ranlib
%{_bindir}/readelf
%{_bindir}/size
%{_bindir}/strings
%{_includedir}/*.h
/usr/lib/ldscripts/elf32_x86_64.x*
/usr/lib/ldscripts/elf_i386.x*
/usr/lib/ldscripts/i386linux.x*
%ifarch x86_64
/usr/lib/ldscripts/elf_k1om.*
/usr/lib/ldscripts/elf_l1om.*
/usr/lib/ldscripts/elf_x86_64.*
%endif
%{_libdir}/libopcodes*.*
%{_datadir}/locale/*/LC_MESSAGES/*.mo

%files strip
%defattr(-,root,root,-)
%{_bindir}/strip

%files libbfd
%defattr(-,root,root,-)
%{_libdir}/libbfd*.*

%files doc
%defattr(-,root,root,-)
%{_infodir}/*.info.gz
%{_mandir}/man1/*.1.gz

%clean
rm -rf %{buildroot}
rm -rf ../binutils-build

%changelog
* Thu Sep 17 2015 tanggeliang <tanggeliang@gmail.com>
- update from 2.25 to 2.25.1
* Sat Mar 28 2015 tanggeliang <tanggeliang@gmail.com>
- update from 2.24 to 2.25
* Mon Dec 9 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.23.2 to 2.24
* Wed Apr 10 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.23.1 to 2.23.2
* Sat Dec 1 2012 tanggeliang <tanggeliang@gmail.com>
- update from 2.22 to 2.23.1
* Wed Oct 3 2012 tanggeliang <tanggeliang@gmail.com>
- add arch x86-64.
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
