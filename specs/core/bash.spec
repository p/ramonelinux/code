Name:       bash
Version:    4.3.30
Release:    1%{?dist}
Summary:    Bash The GNU Bourne Again shell

Group:      System Environment/Shells
License:    GPLv2+
Url:        http://www.gnu.org/software/bash
Source:     ftp://ftp.gnu.org/gnu/bash/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  bison

%description
The Bash package contains the Bourne-Again SHell.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr                           \
            --bindir=/bin                           \
            --docdir=/usr/share/doc/bash-%{version} \
            --without-bash-malloc                   \
            --with-installed-readline &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

ln -vs bash %{buildroot}/bin/sh
rm -f %{buildroot}/%{_infodir}/dir

mkdir -pv %{buildroot}/etc
cat > %{buildroot}/etc/profile << "EOF"
# Begin /etc/profile
# Written for Beyond Linux From Scratch
# by James Robertson <jameswrobertson@earthlink.net>
# modifications by Dagmar d'Surreal <rivyqntzne@pbzpnfg.arg>

# System wide environment variables and startup programs.

# System wide aliases and functions should go in /etc/bashrc.  Personal
# environment variables and startup programs should go into
# ~/.bash_profile.  Personal aliases and functions should go into
# ~/.bashrc.

# Functions to help us manage paths.  Second argument is the name of the
# path variable to be modified (default: PATH)
pathremove () {
        local IFS=':'
        local NEWPATH
        local DIR
        local PATHVARIABLE=${2:-PATH}
        for DIR in ${!PATHVARIABLE} ; do
                if [ "$DIR" != "$1" ] ; then
                  NEWPATH=${NEWPATH:+$NEWPATH:}$DIR
                fi
        done
        export $PATHVARIABLE="$NEWPATH"
}

pathprepend () {
        pathremove $1 $2
        local PATHVARIABLE=${2:-PATH}
        export $PATHVARIABLE="$1${!PATHVARIABLE:+:${!PATHVARIABLE}}"
}

pathappend () {
        pathremove $1 $2
        local PATHVARIABLE=${2:-PATH}
        export $PATHVARIABLE="${!PATHVARIABLE:+${!PATHVARIABLE}:}$1"
}


# Set the initial path
export PATH=/bin:/usr/bin
export LANG="en_US.UTF-8"
export LC_ALL="en_US.UTF-8"

if [ $EUID -eq 0 ] ; then
        pathappend /sbin:/usr/sbin
        unset HISTFILE
fi

# Setup some environment variables.
export HISTSIZE=1000
export HISTIGNORE="&:[bf]g:exit"

PS1='\u@\h:\w\$ '

case $TERM in
xterm*)
  PROMPT_COMMAND='printf "\033]0;%s@%s:%s\007" "${USER}" "${HOSTNAME%%.*}" "    ${PWD/#$HOME/~}"'
  ;;
screen)
  PROMPT_COMMAND='printf "\033]0;%s@%s:%s\033\\" "${USER}" "${HOSTNAME%%.*}"     "${PWD/#$HOME/~}"'
  ;;
*)
  ;;
esac
export PROMPT_COMMAND

for script in /etc/profile.d/*.sh ; do
        if [ -r $script ] ; then
                . $script
        fi
done

# Use bash-completion, if available
if [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
fi

# Now to clean up
unset pathremove pathprepend pathappend

# End /etc/profile
EOF

install --directory --mode=0755 %{buildroot}/etc/profile.d

cat > %{buildroot}/etc/profile.d/dircolors.sh << "EOF"
# Setup for /bin/ls to support color, the alias is in /etc/bashrc.
if [ -f "/etc/dircolors" ] ; then
        eval $(dircolors -b /etc/dircolors)

        if [ -f "$HOME/.dircolors" ] ; then
                eval $(dircolors -b $HOME/.dircolors)
        fi
fi
alias ls='ls --color=auto'
EOF

cat > %{buildroot}/etc/profile.d/extrapaths.sh << "EOF"
if [ -d /usr/local/lib/pkgconfig ] ; then
        pathappend /usr/local/lib/pkgconfig PKG_CONFIG_PATH
fi
if [ -d /usr/local/bin ]; then
        pathprepend /usr/local/bin
fi
if [ -d /usr/local/sbin -a $EUID -eq 0 ]; then
        pathprepend /usr/local/sbin
fi

if [ -d ~/bin ]; then
        pathprepend ~/bin
fi
#if [ $EUID -gt 99 ]; then
#        pathappend .
#fi
EOF

cat > %{buildroot}/etc/profile.d/readline.sh << "EOF"
# Setup the INPUTRC environment variable.
if [ -z "$INPUTRC" -a ! -f "$HOME/.inputrc" ] ; then
        INPUTRC=/etc/inputrc
fi
export INPUTRC
EOF

cat > %{buildroot}/etc/profile.d/umask.sh << "EOF"
# By default we want the umask to get set.
if [ "$(id -gn)" = "$(id -un)" -a $EUID -gt 99 ] ; then
  umask 002
else
  umask 022
fi
EOF

cat > %{buildroot}/etc/profile.d/i18n.sh << "EOF"
# Set up i18n variables
export LANG=en_US.utf8
EOF

cat > %{buildroot}/etc/bashrc << "EOF"
# Begin /etc/bashrc
# Written for Beyond Linux From Scratch
# by James Robertson <jameswrobertson@earthlink.net>
# updated by Bruce Dubbs <bdubbs@linuxfromscratch.org>

# System wide aliases and functions.

# System wide environment variables and startup programs should go into
# /etc/profile.  Personal environment variables and startup programs
# should go into ~/.bash_profile.  Personal aliases and functions should
# go into ~/.bashrc

# Provides a colored /bin/ls command.  Used in conjunction with code in
# /etc/profile.

alias ls='ls --color=auto'
alias grep='grep --color'

# Provides prompt for non-login shells, specifically shells started
# in the X environment. [Review the LFS archive thread titled
# PS1 Environment Variable for a great case study behind this script
# addendum.]
PS1='\u@\h:\w\$ '

# End /etc/bashrc
EOF

dircolors -p > %{buildroot}/etc/dircolors

%files
%defattr(-,root,root,-)
/bin/sh
/bin/bash
/bin/bashbug
%{_datadir}/locale/*/LC_MESSAGES/bash.mo
/etc/profile
%dir /etc/profile.d
/etc/profile.d/dircolors.sh
/etc/profile.d/extrapaths.sh
/etc/profile.d/readline.sh
/etc/profile.d/umask.sh
/etc/profile.d/i18n.sh
/etc/bashrc
/etc/dircolors

%files doc
%defattr(-,root,root,-)
%{_infodir}/bash.info*
%{_docdir}/bash-%{version}/*
%{_mandir}/man1/bash*.1.gz

%clean
rm -rf %{buildroot}

%changelog
* Wed Nov 19 2014 tanggeliang <tanggeliang@gmail.com>
- update from 4.3 to 4.3.30
* Sat Mar 15 2014 tanggeliang <tanggeliang@gmail.com>
- update from 4.2 to 4.3
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
