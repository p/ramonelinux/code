Name:       iproute
Version:    3.8.0
Release:    2%{?dist}
Summary:    IPRoute2

Group:      System Environment/Base
License:    GPLv2+
Url:        http://devresources.linuxfoundation.org/dev/iproute2
Source:     http://devresources.linuxfoundation.org/dev/iproute2/download/%{name}2-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  bison, flex

%description
The IPRoute2 package contains programs for basic and advanced IPV4-based networking.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}2-%{version}

%build
sed -i '/^TARGETS/s@arpd@@g' misc/Makefile
sed -i /ARPD/d Makefile
sed -i 's/arpd.8//' man/man8/Makefile
sed -i 's/-Werror//' Makefile
make DESTDIR= %{?_smp_mflags}

%check

%install
make DESTDIR=%{buildroot}  \
     MANDIR=/usr/share/man \
     DOCDIR=/usr/share/doc/iproute2-%{version} \
     LIBDIR=%{_libdir} install

%files
%defattr(-,root,root,-)
%{_sysconfdir}/iproute2/ematch_map
%{_sysconfdir}/iproute2/group
%{_sysconfdir}/iproute2/rt_dsfield
%{_sysconfdir}/iproute2/rt_protos
%{_sysconfdir}/iproute2/rt_realms
%{_sysconfdir}/iproute2/rt_scopes
%{_sysconfdir}/iproute2/rt_tables
%{_libdir}/tc/experimental.dist
%{_libdir}/tc/normal.dist
%{_libdir}/tc/pareto.dist
%{_libdir}/tc/paretonormal.dist
/sbin/bridge
/sbin/ctstat
/sbin/genl
/sbin/ifcfg
/sbin/ifstat
/sbin/ip
/sbin/lnstat
/sbin/nstat
/sbin/routef
/sbin/routel
/sbin/rtacct
/sbin/rtmon
/sbin/rtpr
/sbin/rtstat
/sbin/ss
/sbin/tc

%files doc
%defattr(-,root,root,-)
%{_docdir}/iproute2-%{version}/*
%{_mandir}/man*/*.gz

%clean
rm -rf %{buildroot}

%changelog
* Sun Mar 17 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.5.1 to 3.8.0
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
