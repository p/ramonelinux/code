Name:       coreutils
Version:    8.23
Release:    1%{?dist}
Summary:    Coreutils

Group:      System Environment/Base
License:    GPLv2+
Url:        http://ftp.gnu.org/gnu/coreutils
Source:     http://ftp.gnu.org/gnu/coreutils/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build

%description
The Coreutils package contains utilities for showing and setting the basic system characteristics.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}
touch Makefile.in

%build
FORCE_UNSAFE_CONFIGURE=1 ./configure \
            --prefix=/usr         \
            --libexecdir=%{_libdir} \
            --enable-no-install-program=kill,uptime \
            --enable-install-program=arch &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

mkdir -pv %{buildroot}/{bin,usr/sbin,usr/share/man/man8}
mv -v %{buildroot}/usr/bin/{cat,chgrp,chmod,chown,cp,date,dd,df,echo} %{buildroot}/bin
mv -v %{buildroot}/usr/bin/{false,ln,ls,mkdir,mknod,mv,pwd,rm} %{buildroot}/bin
mv -v %{buildroot}/usr/bin/{rmdir,stty,sync,true,uname} %{buildroot}/bin
mv -v %{buildroot}/usr/bin/chroot %{buildroot}/usr/sbin
mv -v %{buildroot}/usr/bin/{head,sleep,nice,test,[} %{buildroot}/bin

rm -f %{buildroot}/%{_infodir}/dir

%files
%defattr(-,root,root,-)
/bin/[
/bin/cat
/bin/chgrp
/bin/chmod
/bin/chown
/bin/cp
/bin/date
/bin/dd
/bin/df
/bin/echo
/bin/false
/bin/head
/bin/ln
/bin/ls
/bin/mkdir
/bin/mknod
/bin/mv
/bin/nice
/bin/pwd
/bin/rm
/bin/rmdir
/bin/sleep
/bin/stty
/bin/sync
/bin/test
/bin/true
/bin/uname
%{_bindir}/arch
%{_bindir}/base64
%{_bindir}/basename
%{_bindir}/chcon
%{_bindir}/cksum
%{_bindir}/comm
%{_bindir}/csplit
%{_bindir}/cut
%{_bindir}/dir
%{_bindir}/dircolors
%{_bindir}/dirname
%{_bindir}/du
%{_bindir}/env
%{_bindir}/expand
%{_bindir}/expr
%{_bindir}/factor
%{_bindir}/fmt
%{_bindir}/fold
%{_bindir}/groups
%{_bindir}/hostid
%{_bindir}/id
%{_bindir}/install
%{_bindir}/join
%{_bindir}/link
%{_bindir}/logname
%{_bindir}/md5sum
%{_bindir}/mkfifo
%{_bindir}/mktemp
%{_bindir}/nl
%{_bindir}/nohup
%{_bindir}/nproc
%{_bindir}/numfmt
%{_bindir}/od
%{_bindir}/paste
%{_bindir}/pathchk
%{_bindir}/pinky
%{_bindir}/pr
%{_bindir}/printenv
%{_bindir}/printf
%{_bindir}/ptx
%{_bindir}/readlink
%{_bindir}/realpath
%{_bindir}/runcon
%{_bindir}/seq
%{_bindir}/sha1sum
%{_bindir}/sha224sum
%{_bindir}/sha256sum
%{_bindir}/sha384sum
%{_bindir}/sha512sum
%{_bindir}/shred
%{_bindir}/shuf
%{_bindir}/sort
%{_bindir}/split
%{_bindir}/stat
%{_bindir}/stdbuf
%{_bindir}/sum
%{_bindir}/tac
%{_bindir}/tail
%{_bindir}/tee
%{_bindir}/timeout
%{_bindir}/touch
%{_bindir}/tr
%{_bindir}/truncate
%{_bindir}/tsort
%{_bindir}/tty
%{_bindir}/unexpand
%{_bindir}/uniq
%{_bindir}/unlink
%{_bindir}/users
%{_bindir}/vdir
%{_bindir}/wc
%{_bindir}/who
%{_bindir}/whoami
%{_bindir}/yes
%{_libdir}/coreutils/libstdbuf.so
%{_sbindir}/chroot
%{_datadir}/locale/*/LC_MESSAGES/coreutils.mo
%{_datadir}/locale/*/LC_TIME/coreutils.mo

%files doc
%defattr(-,root,root,-)
%{_infodir}/coreutils.info.gz
%{_mandir}/man1/*.1.gz

%clean
rm -rf %{buildroot}

%changelog
* Mon Jul 21 2014 tanggeliang <tanggeliang@gmail.com>
- update from 8.22 to 8.23
* Tue Dec 17 2013 tanggeliang <tanggeliang@gmail.com>
- update from 8.21 to 8.22
* Sun Mar 17 2013 tanggeliang <tanggeliang@gmail.com>
- update from 8.19 to 8.21
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
