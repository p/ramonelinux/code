Name:       systemd
Version:    221
Release:    2%{?dist}
Summary:    systemd

Group:      System Environment/Base
License:    GPLv2+
Url:        http://www.freedesktop.org/wiki/Software/systemd
Source:     http://www.freedesktop.org/software/systemd/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  libcap dbus
BuildRequires:  intltool gettext xml-parser libxslt gtk-doc docbook-xml docbook-xsl
BuildRequires:  m4 gperf

%description
systemd is a system and service manager for Linux, compatible with SysV and LSB init scripts.
systemd provides aggressive parallelization capabilities, uses socket and D-Bus activation for starting services, offers on-demand starting of daemons, keeps track of processes using Linux control groups, supports snapshotting and restoring of the system state, maintains mount and automount points and implements an elaborate transactional dependency-based service control logic.

%package        udev
Summary:        udev
BuildRequires:  acl pciutils usbutils
BuildRequires:  kmod
%description    udev

%package        sysvinit
Summary:        sysvinit
%description    sysvinit

%package        libudev
Summary:        libudev
%description    libudev

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc 

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr 					    \
            --sysconfdir=/etc 					    \
            --localstatedir=/var 				    \
	    --config-cache                                          \
            --with-rootprefix= 					    \
            --with-rootlibdir=/%{_lib} 				    \
            --enable-split-usr 					    \
            --libexecdir=%{_libdir} 				    \
            --docdir=/usr/share/doc/systemd-%{version} 		    \
	    --with-dbuspolicydir=/etc/dbus-1/system.d               \
            --with-dbussessionservicedir=/usr/share/dbus-1/services \
            --with-dbussystemservicedir=/usr/share/dbus-1/system-services \
            --with-firmware-path="/lib/firmware/updates:/lib/firmware" 	  \
            --with-sysvinit-path=/etc/rc.d/init.d 			  \
            --with-rc-local-script-path-start=/etc/rc.d/rc.local 	  \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
make DESTDIR=%{buildroot} install

mkdir -p %{buildroot}/sbin
for tool in runlevel reboot shutdown poweroff halt telinit; do
     ln -sfv ../bin/systemctl %{buildroot}/sbin/$tool
done
ln -sfv ../lib/systemd/systemd %{buildroot}/sbin/init

sed -i "s@0775 root lock@0755 root root@g" %{buildroot}/usr/lib/tmpfiles.d/legacy.conf

mkdir -p %{buildroot}/etc/pam.d
ln -s /etc/pam.d/login %{buildroot}/etc/pam.d/systemd-shared

%files
%defattr(-,root,root,-)
/etc/dbus-1/system.d/org.freedesktop.*.conf
/etc/pam.d/systemd-shared
/etc/pam.d/systemd-user
/etc/systemd/*.conf
/etc/systemd/system/*
/etc/xdg/systemd/user
/etc/X11/xinit/xinitrc.d/50-systemd-user.sh
/bin/journalctl
/bin/loginctl
/bin/machinectl
/bin/networkctl
/bin/systemctl
/bin/systemd-ask-password
/bin/systemd-escape
/bin/systemd-firstboot
/bin/systemd-hwdb
/bin/systemd-inhibit
/bin/systemd-machine-id-setup
/bin/systemd-notify
/bin/systemd-sysusers
/bin/systemd-tmpfiles
/bin/systemd-tty-ask-password-agent
%{_bindir}/bootctl
%{_bindir}/busctl
%{_bindir}/coredumpctl
%{_bindir}/hostnamectl
%{_bindir}/kernel-install
%{_bindir}/localectl
%{_bindir}/timedatectl
%{_bindir}/systemd-*
%{_includedir}/systemd/sd-*.h
%{_includedir}/systemd/_sd-common.h
/%{_lib}/libsystemd.so.0*
/%{_lib}/security/pam_systemd.*
/lib/systemd/network/80-container-host0.network
/lib/systemd/network/80-container-ve.network
/lib/systemd/network/99-default.link
/lib/systemd/system-generators/systemd-debug-generator
/lib/systemd/system-generators/systemd-efi-boot-generator
/lib/systemd/system-generators/systemd-fstab-generator
/lib/systemd/system-generators/systemd-getty-generator
/lib/systemd/system-generators/systemd-gpt-auto-generator
/lib/systemd/system-generators/systemd-rc-local-generator
/lib/systemd/system-generators/systemd-system-update-generator
/lib/systemd/system-generators/systemd-sysv-generator
/lib/systemd/system-preset/90-systemd.preset
/lib/systemd/system/*
/lib/systemd/systemd
/lib/systemd/systemd-*
%{_libdir}/libnss_myhostname.*
%{_libdir}/libsystemd.*
%{_libdir}/pkgconfig/libsystemd.pc
%{_libdir}/libnss_mymachines.*
%{_libdir}/libnss_resolve.*
/usr/lib/rpm/macros.d/macros.systemd
/usr/lib/sysctl.d/50-coredump.conf
/usr/lib/sysctl.d/50-default.conf
/usr/lib/systemd/catalog/systemd*.catalog
/usr/lib/systemd/user-generators/systemd-dbus1-generator
/usr/lib/systemd/user/systemd-bus-proxyd.socket
/usr/lib/systemd/user/*.target
/usr/lib/systemd/user/*.service
/lib/systemd/system-generators/systemd-dbus1-generator
/lib/systemd/system-generators/systemd-hibernate-resume-generator
/usr/lib/sysusers.d/basic.conf
/usr/lib/sysusers.d/systemd.conf
/usr/lib/tmpfiles.d/*.conf
%{_datadir}/bash-completion/completions/*
%{_datadir}/dbus-1/services/org.freedesktop.systemd1.service
%{_datadir}/dbus-1/system-services/org.freedesktop.*.service
%{_datadir}/factory/etc/nsswitch.conf
%{_datadir}/factory/etc/pam.d/other
%{_datadir}/factory/etc/pam.d/system-auth
%{_datadir}/pkgconfig/systemd.pc
%{_datadir}/polkit-1/actions/org.freedesktop.*.policy
%{_datadir}/systemd/kbd-model-map
%{_datadir}/systemd/language-fallback-map
%{_datadir}/zsh/site-functions/_*
%{_datadir}/locale/*/LC_MESSAGES/systemd.mo
/var/log/README

%files udev
%defattr(-,root,root,-)
/etc/udev/udev.conf
/bin/udevadm
/lib/udev/accelerometer
/lib/udev/ata_id
/lib/udev/cdrom_id
/lib/udev/collect
/lib/udev/hwdb.d/*
/lib/udev/mtd_probe
/lib/udev/rules.d/*.rules
/lib/udev/scsi_id
/lib/udev/v4l_id
/usr/lib/kernel/install.d/50-depmod.install
/usr/lib/kernel/install.d/90-loaderentry.install
%{_datadir}/pkgconfig/udev.pc

%files sysvinit
%defattr(-,root,root,-)
/etc/rc.d/init.d/README
/sbin/init
/sbin/reboot
/sbin/runlevel
/sbin/halt
/sbin/poweroff
/sbin/shutdown
/sbin/telinit

%files libudev
%defattr(-,root,root,-)
/%{_lib}/libudev.so.1*
%{_includedir}/libudev.h
%{_libdir}/libudev.*
%{_libdir}/pkgconfig/libudev.pc

%files doc
%defattr(-,root,root,-)
%{_docdir}/systemd-%{version}/*
%{_mandir}/man*/*.gz

%post
cat > /etc/machine-id << "EOF"
systemd-machine-id-setup
EOF

%clean
rm -rf %{buildroot}

%changelog
* Sat Jun 27 2015 tanggeliang <tanggeliang@gmail.com>
- update from 215 to 221
* Sat Jul 26 2014 tanggeliang <tanggeliang@gmail.com>
- update from 208 to 215
* Sun Dec 1 2013 tanggeliang <tanggeliang@gmail.com>
- update from 207 to 208
* Mon Sep 23 2013 tanggeliang <tanggeliang@gmail.com>
- update from 206 to 207
* Sun Aug 25 2013 tanggeliang <tanggeliang@gmail.com>
- add systemd-shared to fix "Failed at step PAM spawning /lib/systemd/systemd: Operation not permitted"
* Thu Aug 8 2013 tanggeliang <tanggeliang@gmail.com>
- update from 204 to 206
* Sun Jun 16 2013 tanggeliang <tanggeliang@gmail.com>
- update from 197 to 204
* Sun Jun 16 2013 tanggeliang <tanggeliang@gmail.com>
- update from 187 to 197
* Mon Apr 30 2012 tanggeliang <tanggeliang@gmail.com>
- create
