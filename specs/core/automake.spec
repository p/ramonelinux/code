Name:       automake
Version:    1.14.1
Release:    1%{?dist}
Summary:    Automake

Group:      Development/Tools
License:    GPLv2+
Url:        http://www.gnu.org
Source:     http://ftp.gnu.org/gnu/automake/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  autoconf m4

%description
The Automake package contains programs for generating Makefiles for use with Autoconf.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --docdir=/usr/share/doc/automake-%{version} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

rm -f %{buildroot}/%{_infodir}/dir

%files
%defattr(-,root,root,-)
%{_bindir}/aclocal
%{_bindir}/aclocal-1.14
%{_bindir}/automake
%{_bindir}/automake-1.14
%{_datadir}/aclocal/README
%{_datadir}/aclocal-1.14/*.m4
%{_datadir}/aclocal-1.14/internal/ac-config-macro-dirs.m4
%{_datadir}/automake-1.14/*

%files doc
%defattr(-,root,root,-)
%{_docdir}/automake-%{version}/amhello-1.0.tar.gz
%{_infodir}/automake*.info*.gz
%{_mandir}/man1/*.1.gz

%clean
rm -rf %{buildroot}

%changelog
* Sun Jan 5 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.14 to 1.14.1
* Sun Jul 7 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.13.2 to 1.14
* Wed May 29 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.13.1 to 1.13.2
* Sun Mar 17 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.12.4 to 1.13.1
* Sun Oct 14 2012 tanggeliang <tanggeliang@gmail.com>
- update from 1.12.3 to 1.12.4
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
