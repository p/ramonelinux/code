Name:       kbd
Version:    2.0.2
Release:    1%{?dist}
Summary:    Kbd

Group:      System Environment/Base
License:    GPLv2+
Url:        http://ftp.altlinux.com/pub/people/legion/kbd
Source:     http://ftp.altlinux.com/pub/people/legion/kbd/%{name}-%{version}.tar.gz
Patch:      kbd-%{version}-backspace-1.patch

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  bison flex gettext check

%description
The Kbd package contains key-table files and keyboard utilities.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}
%patch -p1

%build
sed -i 's/\(RESIZECONS_PROGS=\)yes/\1no/g' configure
sed -i 's/resizecons.8 //' docs/man/man8/Makefile.in

PKG_CONFIG_PATH=%{_libdir}/pkgconfig ./configure --prefix=/usr --disable-vlock
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

mkdir -pv %{buildroot}%{_datadir}/doc/kbd-%{version}
cp -R -v docs/doc/* %{buildroot}%{_datadir}/doc/kbd-%{version}

%files
%defattr(-,root,root,-)
%{_bindir}/kbd_mode
%{_bindir}/loadkeys
%{_bindir}/openvt
%{_bindir}/setfont
%{_bindir}/chvt
%{_bindir}/deallocvt
%{_bindir}/dumpkeys
%{_bindir}/fgconsole
%{_bindir}/getkeycodes
%{_bindir}/kbdinfo
%{_bindir}/kbdrate
%{_bindir}/loadunimap
%{_bindir}/mapscrn
%{_bindir}/psfaddtable
%{_bindir}/psfgettable
%{_bindir}/psfstriptable
%{_bindir}/psfxtable
%{_bindir}/setkeycodes
%{_bindir}/setleds
%{_bindir}/setmetamode
%{_bindir}/setvtrgb
%{_bindir}/showconsolefont
%{_bindir}/showkey
%{_bindir}/unicode_start
%{_bindir}/unicode_stop
%{_datadir}/consolefonts/*
%{_datadir}/consoletrans/*
%{_datadir}/keymaps/*
%{_datadir}/locale/*/LC_MESSAGES/kbd.mo
%{_datadir}/unimaps/*

%files doc
%defattr(-,root,root,-)
%{_docdir}/kbd-%{version}/*
%{_mandir}/man*/*.gz

%clean
rm -rf %{buildroot}

%changelog
* Sat Jul 26 2014 tanggeliang <tanggeliang@gmail.com>
- update from 2.0.1 to 2.0.2
* Wed Dec 4 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.0.0 to 2.0.1
* Thu Oct 3 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.15.5 to 2.0.0
* Sun Mar 17 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.15.3 to 1.15.5
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
