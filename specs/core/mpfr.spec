Name:       mpfr
Version:    3.1.2
Release:    1%{?dist}
Summary:    MPFR

Group:      System Environment/Base
License:    GPLv2+
Url:        http://www.mpfr.org
Source:     http://www.mpfr.org/mpfr-3.1.1/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build

%description
The MPFR package contains functions for multiple precision math.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure  --prefix=/usr        \
             --enable-thread-safe \
             --docdir=%{_docdir}/mpfr-%{version} \
             --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

make html DESTDIR=%{buildroot}
make install-html DESTDIR=%{buildroot}

rm -f %{buildroot}/%{_infodir}/dir

%files
%defattr(-,root,root,-)
%{_includedir}/mpf2mpfr.h
%{_includedir}/mpfr.h
%{_libdir}/libmpfr.*a
%{_libdir}/libmpfr.so*

%files doc
%defattr(-,root,root,-)
%{_docdir}/mpfr-%{version}/*
%{_infodir}/mpfr.info.gz

%clean
rm -rf %{buildroot}

%changelog
* Wed Jul 31 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.1.1 to 3.1.2
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
