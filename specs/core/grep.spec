Name:       grep
Version:    2.21
Release:    1%{?dist}
Summary:    Grep

Group:      System Environment/Base
License:    GPLv2+
Url:        http://www.gnu.org
Source:     http://ftp.gnu.org/gnu/grep/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build

%description
The Grep package contains programs for searching through files.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
sed -i -e '/tp++/a  if (ep <= tp) break;' src/kwset.c

./configure --prefix=/usr --bindir=/bin &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

rm -f %{buildroot}/%{_infodir}/dir

%files
%defattr(-,root,root,-)
/bin/egrep
/bin/fgrep
/bin/grep
%{_datadir}/locale/*/LC_MESSAGES/grep.mo

%files doc
%defattr(-,root,root,-)
%{_infodir}/grep.info.gz
%{_mandir}/man1/*grep.1.gz

%clean
rm -rf %{buildroot}

%changelog
* Sat Mar 28 2015 tanggeliang <tanggeliang@gmail.com>
- update from 2.20 to 2.21
* Sun Jul 20 2014 tanggeliang <tanggeliang@gmail.com>
- update from 2.18 to 2.20
* Sun Mar 16 2014 tanggeliang <tanggeliang@gmail.com>
- updare from 2.16 to 2.18
* Sun Jan 5 2014 tanggeliang <tanggeliang@gmail.com>
- update from 2.14 to 2.16
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
