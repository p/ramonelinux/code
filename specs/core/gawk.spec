Name:       gawk
Version:    4.1.1
Release:    1%{?dist}
Summary:    Gawk

Group:      System Environment/Base
License:    GPLv2+
Url:        http://www.gnu.org
Source:     http://ftp.gnu.org/gnu/gawk/%{name}-%{version}.tar.xz
Patch:      gawk-4.1.1-build-baddest.patch

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build

%description
The Gawk package contains programs for manipulating text files.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}
%patch -p1

%build
./configure --prefix=/usr --libexecdir=%{_libdir} \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

mkdir -pv %{buildroot}/usr/share/doc/gawk-%{version}
cp    -v doc/{awkforai.txt,*.{eps,pdf,jpg}} %{buildroot}/usr/share/doc/gawk-%{version}

rm -f %{buildroot}/%{_infodir}/dir

%files
%defattr(-,root,root,-)
%{_bindir}/awk
%{_bindir}/gawk
%{_bindir}/gawk-%{version}
%{_bindir}/igawk
%{_includedir}/gawkapi.h
%{_libdir}/awk/grcat
%{_libdir}/awk/pwcat
%{_libdir}/gawk/filefuncs.*
%{_libdir}/gawk/fnmatch.*
%{_libdir}/gawk/fork.*
%{_libdir}/gawk/inplace.*
%{_libdir}/gawk/ordchr.*
%{_libdir}/gawk/readdir.*
%{_libdir}/gawk/readfile.*
%{_libdir}/gawk/revoutput.*
%{_libdir}/gawk/revtwoway.*
%{_libdir}/gawk/rwarray.*
%{_libdir}/gawk/testext.*
%{_libdir}/gawk/time.*
%{_datadir}/awk/*.awk
%{_datadir}/locale/*/LC_MESSAGES/gawk.mo

%files doc
%defattr(-,root,root,-)
%{_docdir}/gawk-%{version}/*
%{_infodir}/gawk*.info.gz
%{_mandir}/man1/*gawk.1.gz
%{_mandir}/man3/*.3am.gz

%clean
rm -rf %{buildroot}

%changelog
* Sat Jul 19 2014 tanggeliang <tanggeliang@gmail.com>
- update from 4.1.0 to 4.1.1
* Wed May 29 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.0.2 to 4.1.0
* Sun Mar 17 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.0.1 to 4.0.2
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
