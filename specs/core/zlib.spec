Name:       zlib
Version:    1.2.8
Release:    2%{?dist}
Summary:    Zlib

Group:      System Environment/Base
License:    GPLv2+
Url:        http://www.zlib.net
Source:     http://www.zlib.net/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build

%description
The Zlib package contains compression and decompression routines used by some programs. 

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

mkdir %{buildroot}/%{_lib}
mv -v %{buildroot}/%{_libdir}/libz.so.* %{buildroot}/%{_lib}
ln -sfv ../../%{_lib}/libz.so.%{version} %{buildroot}/%{_libdir}/libz.so

%files
%defattr(-,root,root,-)
/%{_lib}/libz.so.1*
%{_includedir}/z*.h
%{_libdir}/libz.a
%{_libdir}/libz.so
%{_libdir}/pkgconfig/zlib.pc

%files doc
%defattr(-,root,root,-)
%{_mandir}/man3/zlib.3.gz

%clean
rm -rf %{buildroot}

%changelog
* Wed May 29 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.2.7 to 1.2.8
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
