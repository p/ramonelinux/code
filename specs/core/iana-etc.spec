Name:       iana-etc
Version:    2.30
Release:    8%{?dist}
Summary:    Iana-Etc

Group:      System Environment/Base
License:    GPLv2+
Url:        http://sethwklein.net
Source:     http://sethwklein.net/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildArch:      noarch

%description
The Iana-Etc package provides data for network services and protocols.

%prep
%setup -q -n %{name}-%{version}

%build
make %{?_smp_mflags}

%check

%install
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_sysconfdir}/protocols
%{_sysconfdir}/services

%clean
rm -rf %{buildroot}

%changelog
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
