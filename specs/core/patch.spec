Name:       patch
Version:    2.7.1
Release:    2%{?dist}
Summary:    Patch

Group:      System Environment/Base
License:    GPLv2+
Url:        http://www.gnu.org
Source:     http://ftp.gnu.org/gnu/patch/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build

%description
The Patch package contains a program for modifying or creating files by applying a "patch" typically created by the diff program.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr &&
make %{?_smp_mflags}

%check

%install
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/patch

%files doc
%defattr(-,root,root,-)
%{_mandir}/man1/patch.1.gz

%clean
rm -rf %{buildroot}

%changelog
* Sat Nov 3 2012 tanggeliang <tanggeliang@gmail.com>
- update from 2.6.1 to 2.7.1
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
