Name:       perl
Version:    5.20.0
Release:    1%{?dist}
Summary:    Perl

Group:      System Environment/Base
License:    GPLv2+
Url:        http://www.perl.org
Source:     http://cpan.org/src/5.0/%{name}-%{version}.tar.bz2
Patch:      perl-5.8.0-libdir64.patch

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  less
Provides:       perl
AutoReq:        no

%description
The Perl package contains the Practical Extraction and Report Language.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}
%ifarch x86_64
%patch -p1
%endif

%build
sed -i -e "s|BUILD_ZLIB\s*= True|BUILD_ZLIB = False|"           \
       -e "s|INCLUDE\s*= ./zlib-src|INCLUDE    = /usr/include|" \
       -e "s|LIB\s*= ./zlib-src|LIB        = %{_libdir}|"       \
    cpan/Compress-Raw-Zlib/config.in

sh Configure -des -Dprefix=/usr                 \
                  -Dvendorprefix=/usr           \
                  -Dman1dir=/usr/share/man/man1 \
                  -Dman3dir=/usr/share/man/man3 \
                  -Dpager="/usr/bin/less -isR"  \
                  -Duseshrplib \
                  -Dsitearch="%{_libdir}/perl5"
make %{?_smp_mflags}

%check

%install
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/a2p
%{_bindir}/c2ph
%{_bindir}/config_data
%{_bindir}/corelist
%{_bindir}/cpan*
%{_bindir}/enc2xs
%{_bindir}/find2perl
%{_bindir}/h2ph
%{_bindir}/h2xs
%{_bindir}/instmodsh
%{_bindir}/json_pp
%{_bindir}/libnetcfg
%{_bindir}/perl*
%{_bindir}/piconv
%{_bindir}/pl2pm
%{_bindir}/pod*
%{_bindir}/prove
%{_bindir}/psed
%{_bindir}/pstruct
%{_bindir}/ptar*
%{_bindir}/s2p
%{_bindir}/shasum
%{_bindir}/splain
%{_bindir}/xsubpp
%{_bindir}/zipdetails
%{_libdir}/perl5/%{version}/*.pm
%{_libdir}/perl5/%{version}/*.pod
%{_libdir}/perl5/%{version}/*.pl
%{_libdir}/perl5/%{version}/App/*
%{_libdir}/perl5/%{version}/Archive/*
%{_libdir}/perl5/%{version}/Attribute/*
%{_libdir}/perl5/%{version}/B/*
%{_libdir}/perl5/%{version}/CGI/*
%{_libdir}/perl5/%{version}/CPAN/*
%{_libdir}/perl5/%{version}/Carp/*
%{_libdir}/perl5/%{version}/Class/*
%{_libdir}/perl5/%{version}/Compress/*
%{_libdir}/perl5/%{version}/Config/*
%{_libdir}/perl5/%{version}/DBM_Filter/*
%{_libdir}/perl5/%{version}/Devel/*
%{_libdir}/perl5/%{version}/Digest/*
%{_libdir}/perl5/%{version}/Encode/*
%{_libdir}/perl5/%{version}/Exporter/*
%{_libdir}/perl5/%{version}/ExtUtils/*
%{_libdir}/perl5/%{version}/File/*
%{_libdir}/perl5/%{version}/Filter/*
%{_libdir}/perl5/%{version}/Getopt/*
%{_libdir}/perl5/%{version}/HTTP/*
%{_libdir}/perl5/%{version}/I18N/*
%{_libdir}/perl5/%{version}/IO/*
%{_libdir}/perl5/%{version}/IPC/*
%{_libdir}/perl5/%{version}/JSON/*
%{_libdir}/perl5/%{version}/Locale/*
%{_libdir}/perl5/%{version}/Math/*
%{_libdir}/perl5/%{version}/Memoize/*
%{_libdir}/perl5/%{version}/Module/*
%{_libdir}/perl5/%{version}/Net/*
%{_libdir}/perl5/%{version}/Package/*
%{_libdir}/perl5/%{version}/Params/*
%{_libdir}/perl5/%{version}/Parse/*
%{_libdir}/perl5/%{version}/Perl/*
%{_libdir}/perl5/%{version}/PerlIO/*
%{_libdir}/perl5/%{version}/Pod/*
%{_libdir}/perl5/%{version}/Search/*
%{_libdir}/perl5/%{version}/TAP/*
%{_libdir}/perl5/%{version}/Term/*
%{_libdir}/perl5/%{version}/Test/*
%{_libdir}/perl5/%{version}/Text/*
%{_libdir}/perl5/%{version}/Thread/*
%{_libdir}/perl5/%{version}/Tie/*
%{_libdir}/perl5/%{version}/Time/*
%{_libdir}/perl5/%{version}/Unicode/*
%{_libdir}/perl5/%{version}/User/*
%{_libdir}/perl5/%{version}/autodie/*
%{_libdir}/perl5/%{version}/encoding/*
%ifarch %{ix86}
%{_libdir}/perl5/%{version}/i686-linux/*
%{_libdir}/perl5/%{version}/i686-linux/.packlist
%else %ifarch x86_64
%{_libdir}/perl5/%{version}/x86_64-linux/*
%{_libdir}/perl5/%{version}/x86_64-linux/.packlist
%endif
%{_libdir}/perl5/%{version}/inc/*
%{_libdir}/perl5/%{version}/overload/*
%{_libdir}/perl5/%{version}/pod/*
%{_libdir}/perl5/%{version}/unicore/*
%{_libdir}/perl5/%{version}/version/*
%{_libdir}/perl5/%{version}/warnings/*

%files doc
%defattr(-,root,root,-)
%{_mandir}/man1/*.1.gz
%{_mandir}/man3/*.3.gz

%clean
rm -rf %{buildroot}

%changelog
* Sat Jul 26 2014 tanggeliang <tanggeliang@gmail.com>
- update from 5.18.2 to 5.20.0
* Sat Mar 1 2014 tanggeliang <tanggeliang@gmail.com>
- update from 5.18.1 to 5.18.2
* Sat Aug 24 2013 tanggeliang <tanggeliang@gmail.com>
- update from 5.18.0 to 5.18.1
* Thu Jul 25 2013 tanggeliang <tanggeliang@gmail.com>
- update from 5.16.3 to 5.18.0
* Fri Mar 29 2013 tanggeliang <tanggeliang@gmail.com>
- update from 5.16.2 to 5.16.3
* Sun Mar 17 2013 tanggeliang <tanggeliang@gmail.com>
- update from 5.16.1 to 5.16.2
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
