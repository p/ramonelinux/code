Name:       less
Version:    458
Release:    1%{?dist}
Summary:    Less

Group:      System Environment/Base
License:    GPLv2+
Url:        http://www.greenwoodsoftware.com/less
Source:     http://www.greenwoodsoftware.com/less/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build

%description
The Less package contains a text file viewer.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --sysconfdir=/etc &&
make %{?_smp_mflags}

%check

%install
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/less
%{_bindir}/lessecho
%{_bindir}/lesskey

%files doc
%defattr(-,root,root,-)
%{_mandir}/man1/less*.1.gz

%clean
rm -rf %{buildroot}

%changelog
* Wed Jul 31 2013 tanggeliang <tanggeliang@gmail.com>
- update from 451 to 458
* Sat Nov 3 2012 tanggeliang <tanggeliang@gmail.com>
- update from 444 to 451
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
