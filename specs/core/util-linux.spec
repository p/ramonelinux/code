Name:       util-linux
Version:    2.26.1
Release:    1%{?dist}
Summary:    Util-linux

Group:      System Environment/Base
License:    GPLv2+
Url:        http://www.kernel.org/pub/linux/utils/util-linux
Source:     http://www.kernel.org/pub/linux/utils/util-linux/v2.26/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  systemd
Requires:       shadow
# disable requires /bin/tcsh
AutoReq:        no

%description
The Util-linux package contains miscellaneous utility programs.
Among them are utilities for handling file systems, consoles, partitions, and messages.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure ADJTIME_PATH=/var/lib/hwclock/adjtime     \
            --docdir=%{_docdir}/util-linux-%{version} \
            --disable-chfn-chsh  \
            --disable-login      \
            --disable-nologin    \
            --disable-su         \
            --disable-setpriv    \
            --disable-runuser    \
            --disable-pylibmount \
            --disable-static     \
            --without-python     \
            --disable-makeinstall-chown \
            --libdir=%{_libdir} \
            --with-systemdsystemunitdir=/lib/systemd/system &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

mkdir -pv %{buildroot}/var/lib/hwclock

%files
%defattr(-,root,root,-)
/bin/dmesg
/bin/findmnt
/bin/kill
/bin/lsblk
/bin/more
/bin/mountpoint
/bin/wdctl
/sbin/agetty
/sbin/blkdiscard
/sbin/blkid
/sbin/blockdev
/sbin/cfdisk
/sbin/chcpu
/sbin/ctrlaltdel
/sbin/fdisk
/sbin/findfs
/sbin/fsck
/sbin/fsck.cramfs
/sbin/fsck.minix
/sbin/fsfreeze
/sbin/fstrim
/sbin/hwclock
/sbin/losetup
/sbin/mkfs
/sbin/mkfs.bfs
/sbin/mkfs.cramfs
/sbin/mkfs.minix
/sbin/mkswap
/sbin/pivot_root
/sbin/raw
/sbin/sfdisk
/sbin/sulogin
/sbin/swaplabel
/sbin/swapoff
/sbin/swapon
/sbin/switch_root
/sbin/wipefs
/sbin/zramctl
%{_bindir}/cal
%{_bindir}/chrt
%{_bindir}/col
%{_bindir}/colcrt
%{_bindir}/colrm
%{_bindir}/column
%{_bindir}/eject
%{_bindir}/fallocate
%{_bindir}/flock
%{_bindir}/getopt
%{_bindir}/hexdump
%{_bindir}/i386
%ifarch x86_64
%{_bindir}/x86_64
%endif
%{_bindir}/ionice
%{_bindir}/ipcmk
%{_bindir}/ipcrm
%{_bindir}/ipcs
%{_bindir}/isosize
%{_bindir}/last
%{_bindir}/lastb
%{_bindir}/linux32
%{_bindir}/linux64
%{_bindir}/logger
%{_bindir}/look
%{_bindir}/lscpu
%{_bindir}/lslocks
%{_bindir}/lslogins
%{_bindir}/mcookie
%{_bindir}/mesg
%{_bindir}/namei
%{_bindir}/nsenter
%{_bindir}/pg
%{_bindir}/prlimit
%{_bindir}/rename
%{_bindir}/renice
%{_bindir}/rev
%{_bindir}/script
%{_bindir}/scriptreplay
%{_bindir}/setarch
%{_bindir}/setsid
%{_bindir}/setterm
%{_bindir}/tailf
%{_bindir}/taskset
%{_bindir}/ul
%{_bindir}/uname26
%{_bindir}/unshare
%{_bindir}/utmpdump
%{_bindir}/uuidgen
%{_bindir}/whereis
%{_includedir}/blkid/blkid.h
%{_includedir}/libfdisk/libfdisk.h
%{_includedir}/libmount/libmount.h
%{_includedir}/libsmartcols/libsmartcols.h
%{_includedir}/uuid/uuid.h
%{_libdir}/libblkid.*
%{_libdir}/libfdisk.*
%{_libdir}/libmount.*
%{_libdir}/libsmartcols.*
%{_libdir}/libuuid.*
%{_libdir}/pkgconfig/blkid.pc
%{_libdir}/pkgconfig/fdisk.pc
%{_libdir}/pkgconfig/mount.pc
%{_libdir}/pkgconfig/smartcols.pc
%{_libdir}/pkgconfig/uuid.pc
%{_sbindir}/addpart
%{_sbindir}/delpart
%{_sbindir}/fdformat
%{_sbindir}/ldattach
%{_sbindir}/partx
%{_sbindir}/readprofile
%{_sbindir}/resizepart
%{_sbindir}/rtcwake
%{_sbindir}/uuidd
%{_datadir}/bash-completion/completions/*
%{_docdir}/util-linux-%{version}/getopt/getopt-parse.*sh
%{_datadir}/locale/*/LC_MESSAGES/util-linux.mo
%dir %{_var}/lib/hwclock
%attr(4755,root,root)   /bin/mount
%attr(4755,root,root)   /bin/umount
%attr(2755,root,tty)    %{_bindir}/wall
/lib/systemd/system/fstrim.service
/lib/systemd/system/fstrim.timer
/lib/systemd/system/uuidd.service
/lib/systemd/system/uuidd.socket

%files doc
%defattr(-,root,root,-)
%{_mandir}/man1/*.1.gz
%{_mandir}/man3/*.3.gz
%{_mandir}/man5/*.5.gz
%{_mandir}/man8/*.8.gz

%clean
rm -rf %{buildroot}

%changelog
* Fri Mar 27 2015 tanggeliang <tanggeliang@gmail.com>
- update from 2.25.2 to 2.26.1
* Wed Nov 19 2014 tanggeliang <tanggeliang@gmail.com>
- update from 2.25.1 to 2.25.2
* Fri Sep 19 2014 tanggeliang <tanggeliang@gmail.com>
- update from 2.25 to 2.25.1
* Sat Jul 26 2014 tanggeliang <tanggeliang@gmail.com>
- update from 2.24.2 to 2.25
* Sun Jul 20 2014 tanggeliang <tanggeliang@gmail.com>
- update from 2.24.1 to 2.24.2
* Fri Feb 28 2014 tanggeliang <tanggeliang@gmail.com>
- update from 2.24 to 2.24.1
* Sun Dec 1 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.23.2 to 2.24
* Sun Aug 18 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.23.1 to 2.23.2
* Thu Jul 25 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.23 to 2.23.1
* Thu May 30 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.22.2 to 2.23
* Sun Mar 17 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.22.1 to 2.22.2
* Sat Nov 3 2012 tanggeliang <tanggeliang@gmail.com>
- update from 2.22 to 2.22.1
- add --disable-makeinstall-chown to fix chgrp wall Operation not permitte
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
