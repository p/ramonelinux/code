Name:       setup
Version:    0.99
Release:    1%{?dist}
Summary:    setup

Group:      System Environment/Base
License:    GPLv2+

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildArch:      noarch
Requires:       coreutils kbd systemd systemd-sysvinit systemd-udev util-linux
Requires:       iana-etc iproute inetutils consolekit e2fsprogs

%description
Setting up system configuration.

%install
mkdir -p %{buildroot}/etc/sysconfig

##
## Create the /etc/mtab File
##
touch %{buildroot}/etc/mtab

##
## Customizing the /etc/hosts File
##
cat > %{buildroot}/etc/hosts << "EOF"
# Begin /etc/hosts (network card version)

127.0.0.1 localhost
192.168.1.1 HOSTNAME.example.org [alias1] [alias2 ...]

# End /etc/hosts (network card version)
EOF

##
## Configuring the system hostname
##
echo "HOSTNAME=localhost.localdomain" > %{buildroot}/etc/sysconfig/network

##
## Configuring the setclock Script
##
cat > %{buildroot}/etc/sysconfig/clock << "EOF"
# Begin /etc/sysconfig/clock

UTC=1

# Set this to any options you might need to give to hwclock, 
# such as machine hardware clock type for Alphas.
CLOCKPARAMS=

# End /etc/sysconfig/clock
EOF

##
## Configuring the Linux Console
##
cat > %{buildroot}/etc/sysconfig/console << "EOF"
# Begin /etc/sysconfig/console

UNICODE="1"
KEYMAP="pl2"
FONT="lat2a-16 -m 8859-2"

# End /etc/sysconfig/console
EOF

##
## Creating the /etc/inputrc File
##
cat > %{buildroot}/etc/inputrc << "EOF"
# Begin /etc/inputrc
# Modified by Chris Lynn <roryo@roryo.dynup.net>

# Allow the command prompt to wrap to the next line
set horizontal-scroll-mode Off

# Enable 8bit input
set meta-flag On
set input-meta On

# Turns off 8th bit stripping
set convert-meta Off

# Keep the 8th bit for display
set output-meta On

# none, visible or audible
set bell-style none

# All of the following map the escape sequence of the value
# contained in the 1st argument to the readline specific functions
"\eOd": backward-word
"\eOc": forward-word

# for linux console
"\e[1~": beginning-of-line
"\e[4~": end-of-line
"\e[5~": beginning-of-history
"\e[6~": end-of-history
"\e[3~": delete-char
"\e[2~": quoted-insert

# for xterm
"\eOH": beginning-of-line
"\eOF": end-of-line

# for Konsole
"\e[H": beginning-of-line
"\e[F": end-of-line

# End /etc/inputrc
EOF

##
## Creating the /etc/fstab File
##
cat > %{buildroot}/etc/fstab << "EOF"
# Begin /etc/fstab

# file system  mount-point  type     options             dump  fsck
#                                                              order

/dev/sda1      /            ext4     defaults            1     1
#/dev/sda2      /home        ext4     defaults            1     2
#/dev/sda3      swap         swap     pri=1               0     0
proc           /proc        proc     nosuid,noexec,nodev 0     0
sysfs          /sys         sysfs    nosuid,noexec,nodev 0     0
devpts         /dev/pts     devpts   gid=5,mode=620      0     0
tmpfs          /run         tmpfs    defaults            0     0
devtmpfs       /dev         devtmpfs mode=0755,nosuid    0     0

# End /etc/fstab
EOF

##
## Create an /etc/lsb-release File
##
cat > %{buildroot}/etc/lsb-release << "EOF"
DISTRIB_ID="ramone"
DISTRIB_RELEASE="%{version}"
DISTRIB_CODENAME="201310"
DISTRIB_DESCRIPTION="Ramone Linux"
EOF

cat > %{buildroot}/etc/os-release << "EOF"
NAME=Ramone
VERSION="%{version}"
ID=ramone
VERSION_ID=%{version}
PRETTY_NAME="Ramone %{version}"
ANSI_COLOR="0;34"
CPE_NAME="cpe:/o:ramonelinux:ramone:%{version}"
HOME_URL="http://sourceforge.net/projects/ramonelinux/"
BUG_REPORT_URL="http://sourceforge.net/projects/ramonelinux/"
EOF

%files
%defattr(-,root,root,-)
/etc/mtab
/etc/hosts
/etc/sysconfig/clock
/etc/sysconfig/console
/etc/sysconfig/network
/etc/inputrc
/etc/fstab
/etc/lsb-release
/etc/os-release

%clean
rm -rf %{buildroot}

%changelog
* Sat Aug 17 2013 tanggeliang <tanggeliang@gmail.com>
- update from bootscripts to setup
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
