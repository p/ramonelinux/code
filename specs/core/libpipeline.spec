Name:       libpipeline
Version:    1.4.0
Release:    1%{?dist}
Summary:    Libpipeline

Group:      System Environment/Base
License:    GPLv2+
Url:        http://libpipeline.nongnu.org/
Source:     http://download.savannah.gnu.org/releases/libpipeline/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build

%description
The Libpipeline package contains a library for manipulating pipelines of subprocesses in a flexible and convenient way.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
PKG_CONFIG_PATH=%{_libdir}/pkgconfig ./configure --prefix=/usr \
                                                 --libdir=%{_libdir}
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_includedir}/pipeline.h
%{_libdir}/libpipeline.la
%{_libdir}/libpipeline.so*
%{_libdir}/pkgconfig/libpipeline.pc

%files doc
%defattr(-,root,root,-)
%{_mandir}/man3/*pipe*.3.gz

%clean
rm -rf %{buildroot}

%changelog
* Wed Nov 19 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.2.6 to 1.4.0
* Sun Jan 5 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.2.5 to 1.2.6
* Tue Dec 17 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.2.4 to 1.2.5
* Sat Aug 24 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.2.2 to 1.2.4
* Sun Oct 14 2012 tanggeliang <tanggeliang@gmail.com>
- update from 1.2.1 to 1.2.2
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
