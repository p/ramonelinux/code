Name:       bison
Version:    3.0.4
Release:    1%{?dist}
Summary:    Bison

Group:      Development/Tools
License:    GPLv2+
Url:        http://www.gnu.org
Source:     http://ftp.gnu.org/gnu/bison/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  m4
Requires:       m4

%description
The Bison package contains a parser generator.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --docdir=/usr/share/doc/bison-%{version} \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

rm -f %{buildroot}/%{_infodir}/dir

%files
%defattr(-,root,root,-)
%{_bindir}/bison
%{_bindir}/yacc
%{_libdir}/liby.a
%{_datadir}/aclocal/bison-i18n.m4
%{_datadir}/bison/*
%{_datadir}/locale/*/LC_MESSAGES/bison*.mo

%files doc
%defattr(-,root,root,-)
%{_docdir}/bison-%{version}/*
%{_infodir}/bison.info*.gz
%{_mandir}/man1/*.1.gz

%clean
rm -rf %{buildroot}

%changelog
* Sat Mar 28 2015 tanggeliang <tanggeliang@gmail.com>
- update from 3.0.2 to 3.0.4
* Tue Dec 17 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.0.1 to 3.0.2
* Sun Dec 1 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.0 to 3.0.1
* Sat Aug 24 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.7.1 to 3.0
* Wed May 29 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.7 to 2.7.1
* Sun Mar 17 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.6.5 to 2.7
* Sat Dec 1 2012 tanggeliang <tanggeliang@gmail.com>
- update from 2.6.4 to 2.6.5
* Tue Nov 6 2012 tanggeliang <tanggeliang@gmail.com>
- update from 2.6.2 to 2.6.4
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
