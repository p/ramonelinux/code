Name:       mpc
Version:    1.0.2
Release:    1%{?dist}
Summary:    MPC

Group:      System Environment/Base
License:    GPLv2+
Url:        http://www.multiprecision.org
Source:     http://www.multiprecision.org/mpc/download/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build

%description
The MPC package contains a library for the arithmetic of complex numbers with arbitrarily high precision and correct rounding of the result.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

rm -f %{buildroot}/%{_infodir}/dir

%files
%defattr(-,root,root,-)
%{_includedir}/mpc.h
%{_libdir}/libmpc.*a
%{_libdir}/libmpc.so*

%files doc
%defattr(-,root,root,-)
%{_infodir}/mpc.info.gz

%clean
rm -rf %{buildroot}

%changelog
* Fri Feb 28 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.0.1 to 1.0.2
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
