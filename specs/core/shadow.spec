Name:       shadow
Version:    4.2.1
Release:    2%{?dist}
Summary:    Shadow

Group:      System Environment/Base
License:    GPLv2+
Url:        ftp://pkg-shadow.alioth.debian.org/pub/pkg-shadow
Source:     ftp://pkg-shadow.alioth.debian.org/pub/pkg-shadow/%{name}-%{version}.tar.xz
Patch:      %{name}-%{version}-useradd-1-ram1.patch

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
Requires(post): coreutils sed

%description
The Shadow package contains programs for handling passwords in a secure way.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}
%patch -p1

%build
sed -i 's/groups$(EXEEXT) //' src/Makefile.in &&
find man -name Makefile.in -exec sed -i 's/groups\.1 / /' {} \; &&

sed -i -e 's@#ENCRYPT_METHOD DES@ENCRYPT_METHOD SHA512@' \
       -e 's@/var/spool/mail@/var/mail@' etc/login.defs &&

#sed -i 's/1000/999/' etc/useradd &&

./configure --sysconfdir=/etc --with-group-name-max-length=32
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

rm -rf %{buildroot}/%{_mandir}/man3/getspnam.3*
rm -rf %{buildroot}/%{_mandir}/man5/passwd.5*

mv -v %{buildroot}/usr/bin/passwd %{buildroot}/bin

sed -i 's/yes/no/' %{buildroot}/etc/default/useradd

install -v -m644 %{buildroot}/etc/login.defs %{buildroot}/etc/login.defs.orig &&
for FUNCTION in FAIL_DELAY FAILLOG_ENAB \
                LASTLOG_ENAB \
                MAIL_CHECK_ENAB \
                OBSCURE_CHECKS_ENAB \
                PORTTIME_CHECKS_ENAB \
                QUOTAS_ENAB \
                CONSOLE MOTD_FILE \
                FTMP_FILE NOLOGINS_FILE \
                ENV_HZ PASS_MIN_LEN \
                SU_WHEEL_ONLY \
                CRACKLIB_DICTPATH \
                PASS_CHANGE_TRIES \
                PASS_ALWAYS_WARN \
                CHFN_AUTH ENCRYPT_METHOD \
                ENVIRON_FILE
do
    sed -i "s/^${FUNCTION}/# &/" %{buildroot}/etc/login.defs
done

cat > %{buildroot}/etc/pam.d/system-account << "EOF"
# Begin /etc/pam.d/system-account

account     required    pam_unix.so

# End /etc/pam.d/system-account
EOF

cat > %{buildroot}/etc/pam.d/system-auth << "EOF"
# Begin /etc/pam.d/system-auth

auth    required    pam_unix.so

# End /etc/pam.d/system-auth
EOF

cat > %{buildroot}/etc/pam.d/system-password << "EOF"
# Begin /etc/pam.d/system-password

# use sha512 hash for encryption, use shadow, and try to use any previously
# defined authentication token (chosen password) set by any prior module

password    required    pam_unix.so     sha512 shadow try_first_pass

# End /etc/pam.d/system-password
EOF

#cat > %{buildroot}/etc/pam.d/system-session << "EOF"
# Begin /etc/pam.d/system-session

#session     required    pam_unix.so

# End /etc/pam.d/system-session
#EOF

cat > %{buildroot}/etc/pam.d/login << "EOF"
# Begin /etc/pam.d/login

# Set failure delay before next prompt to 3 seconds
auth    optional    pam_faildelay.so delay=3000000

# Check to make sure that the user is allowed to login
auth    requisite   pam_nologin.so

# Check to make sure that root is allowed to login
# Disabled by default. You will need to create /etc/securetty
# file for this module to function. See man 5 securetty.
#auth   required    pam_securetty.so

# Additional group memberships - disabled by default
#auth   optional    pam_group.so

# include the default auth settings
auth    include     system-auth

# check access for the user
account required    pam_access.so

# include the default account settings
account include     system-account

# Set default environment variables for the user
session required    pam_env.so

# Set resource limits for the user
session required    pam_limits.so

# Display date of last login - Disabled by default
#session optional pam_lastlog.so

# Display the message of the day - Disabled by default
#session optional pam_motd.so

# Check user's mail - Disabled by default
#session optional pam_mail.so standard quiet

# include the default session and password settings
session     include     system-session
password    include     system-password

# End /etc/pam.d/login
EOF

cat > %{buildroot}/etc/pam.d/passwd << "EOF"
# Begin /etc/pam.d/passwd

password    include     system-password

# End /etc/pam.d/passwd
EOF

cat > %{buildroot}/etc/pam.d/su << "EOF"
# Begin /etc/pam.d/su

# always allow root
auth    sufficient  pam_rootok.so
auth    include     system-auth

# include the default account settings
account include     system-account

# Set default environment variables for the service user
session required    pam_env.so

# include system session defaults
session include     system-session

# End /etc/pam.d/su
EOF

cat > %{buildroot}/etc/pam.d/chage << "EOF"
#Begin /etc/pam.d/chage

# always allow root
auth    sufficient  pam_rootok.so

# include system defaults for auth account and session
auth    include     system-auth
account include     system-account
session include     system-session

# Always permit for authentication updates
password required   pam_permit.so

# End /etc/pam.d/chage
EOF

for PROGRAM in chfn chgpasswd chpasswd chsh groupadd groupdel \
               groupmems groupmod newusers useradd userdel usermod
do
    install -v -m644 %{buildroot}/etc/pam.d/chage etc/pam.d/${PROGRAM}
    sed -i "s/chage/$PROGRAM/" %{buildroot}/etc/pam.d/${PROGRAM}
done

cat > %{buildroot}/etc/pam.d/other << "EOF"
# Begin /etc/pam.d/other

auth        required        pam_warn.so
auth        required        pam_deny.so
account     required        pam_warn.so
account     required        pam_deny.so
password    required        pam_warn.so
password    required        pam_deny.so
session     required        pam_warn.so
session     required        pam_deny.so

# End /etc/pam.d/other
EOF

[ -f %{buildroot}/etc/login.access ] && mv -v %{buildroot}/etc/login.access{,.NOUSE}
[ -f %{buildroot}/etc/limits ] && mv -v %{buildroot}/etc/limits{,.NOUSE}

## passwd
cat > %{buildroot}/etc/passwd << "EOF"
root:x:0:0:root:/root:/bin/bash
bin:x:1:1:bin:/dev/null:/bin/false
daemon:x:6:6:Daemon User:/dev/null:/bin/false
nobody:x:99:99:Unprivileged User:/dev/null:/bin/false
EOF

## group
cat > %{buildroot}/etc/group << "EOF"
root:x:0:
bin:x:1:daemon
sys:x:2:
kmem:x:3:
tape:x:4:
tty:x:5:
daemon:x:6:
floppy:x:7:
disk:x:8:
lp:x:9:
dialout:x:10:
audio:x:11:
video:x:12:
utmp:x:13:
usb:x:14:
cdrom:x:15:
adm:x:16:
systemd-journal:x:23:
input:x:24:
mail:x:34:
nogroup:x:99:
users:x:1000:
EOF

%files
%defattr(-,root,root,-)
%{_sysconfdir}/default/useradd
%{_sysconfdir}/login.defs
/bin/login
/bin/passwd
/bin/su
/sbin/nologin
%{_bindir}/chage
%{_bindir}/chfn
%{_bindir}/chsh
%{_bindir}/expiry
%{_bindir}/faillog
%{_bindir}/gpasswd
%{_bindir}/lastlog
%{_bindir}/newgidmap
%{_bindir}/newgrp
%{_bindir}/newuidmap
%{_bindir}/sg
%{_sbindir}/chgpasswd
%{_sbindir}/chpasswd
%{_sbindir}/groupadd
%{_sbindir}/groupdel
%{_sbindir}/groupmems
%{_sbindir}/groupmod
%{_sbindir}/grpck
%{_sbindir}/grpconv
%{_sbindir}/grpunconv
%{_sbindir}/logoutd
%{_sbindir}/newusers
%{_sbindir}/pwck
%{_sbindir}/pwconv
%{_sbindir}/pwunconv
%{_sbindir}/useradd
%{_sbindir}/userdel
%{_sbindir}/usermod
%{_sbindir}/vigr
%{_sbindir}/vipw
%{_datadir}/locale/*/LC_MESSAGES/shadow.mo
%{_sysconfdir}/passwd
%{_sysconfdir}/group
%{_sysconfdir}/login.defs.orig
%{_sysconfdir}/pam.d/chage
%{_sysconfdir}/pam.d/chfn
%{_sysconfdir}/pam.d/chgpasswd
%{_sysconfdir}/pam.d/chpasswd
%{_sysconfdir}/pam.d/chsh
%{_sysconfdir}/pam.d/groupadd
%{_sysconfdir}/pam.d/groupdel
%{_sysconfdir}/pam.d/groupmems
%{_sysconfdir}/pam.d/groupmod
%{_sysconfdir}/pam.d/login
%{_sysconfdir}/pam.d/newusers
%{_sysconfdir}/pam.d/other
%{_sysconfdir}/pam.d/passwd
%{_sysconfdir}/pam.d/su
%{_sysconfdir}/pam.d/system-account
%{_sysconfdir}/pam.d/system-auth
%{_sysconfdir}/pam.d/system-password
%{_sysconfdir}/pam.d/useradd
%{_sysconfdir}/pam.d/userdel
%{_sysconfdir}/pam.d/usermod

%files doc
%defattr(-,root,root,-)
%{_mandir}/*

%clean
rm -rf %{buildroot}

%post
cd /etc
sed -e "s/:.*/:*:`expr $(date +%s) / 86400`:0:99999:7:::/" passwd >shadow
sed -e 's/:[0-9]\+:/::/g' group >gshadow
sed -i -e 's/^\([^:]\+\):[^:]*:/\1:x:/' passwd group

%changelog
* Sun Jul 20 2014 tanggeliang <tanggeliang@gmail.com>
- update from 4.1.5.1 to 4.2.1
* Wed Sep 19 2012 tanggeliang <tanggeliang@gmail.com>
- add "sed -i '2s:1000$:100:' etc/useradd" fix "useradd: group '1000' does not exist".
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
