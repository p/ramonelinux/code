Name:       gzip
Version:    1.6
Release:    1%{?dist}
Summary:    Gzip

Group:      System Environment/Base
License:    GPLv2+
Url:        http://www.gnu.org
Source:     http://ftp.gnu.org/gnu/gzip/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build

%description
The Gzip package contains programs for compressing and decompressing files.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --bindir=/bin &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

mkdir %{buildroot}/usr/bin
mv -v %{buildroot}/bin/{gzexe,uncompress,zcmp,zdiff,zegrep} %{buildroot}/usr/bin
mv -v %{buildroot}/bin/{zfgrep,zforce,zgrep,zless,zmore,znew} %{buildroot}/usr/bin

rm -f %{buildroot}/%{_infodir}/dir

%files
%defattr(-,root,root,-)
/bin/gunzip
/bin/gzip
/bin/zcat
%{_bindir}/gzexe
%{_bindir}/uncompress
%{_bindir}/zcmp
%{_bindir}/zdiff
%{_bindir}/zegrep
%{_bindir}/zfgrep
%{_bindir}/zforce
%{_bindir}/zgrep
%{_bindir}/zless
%{_bindir}/zmore
%{_bindir}/znew

%files doc
%defattr(-,root,root,-)
%{_infodir}/gzip.info.gz
%{_mandir}/man1/*.1.gz

%clean
rm -rf %{buildroot}

%changelog
* Wed Jul 31 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.5 to 1.6
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
