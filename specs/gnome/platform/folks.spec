Name:       folks
Version:    0.11.1
Release:    1%{?dist}
Summary:    Folks

Group:      User Interface/GNOME
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/%{name}/0.11/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  gobject-introspection intltool libgee libzeitgeist telepathy-glib
BuildRequires:  evolution-data-server vala
BuildRequires:  glib gettext xml-parser

%description
Folks is a library that aggregates people from multiple sources (eg, Telepathy connection managers and eventually Evolution Data Server, Facebook, etc.) to create metacontacts.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --enable-vala \
            --disable-fatal-warnings \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/folks-import
%{_bindir}/folks-inspect
%{_includedir}/folks/folks*.h
%{_libdir}/girepository-1.0/Folks-0.6.typelib
%{_libdir}/girepository-1.0/FolksDummy-0.6.typelib
%{_libdir}/girepository-1.0/FolksEds-0.6.typelib
%{_libdir}/girepository-1.0/FolksTelepathy-0.6.typelib
%{_libdir}/folks/43/backends/bluez/bluez.*
%{_libdir}/folks/43/backends/dummy/dummy.*
%{_libdir}/folks/43/backends/eds/eds.*
%{_libdir}/folks/43/backends/key-file/key-file.*
%{_libdir}/folks/43/backends/ofono/ofono.*
%{_libdir}/folks/43/backends/telepathy/telepathy.*
%{_libdir}/libfolks*.la
%{_libdir}/libfolks*.so*
%{_libdir}/pkgconfig/folks-dummy.pc
%{_libdir}/pkgconfig/folks-eds.pc
%{_libdir}/pkgconfig/folks-telepathy.pc
%{_libdir}/pkgconfig/folks.pc
%{_datadir}/GConf/gsettings/folks.convert
%{_datadir}/gir-1.0/Folks-0.6.gir
%{_datadir}/gir-1.0/FolksDummy-0.6.gir
%{_datadir}/gir-1.0/FolksEds-0.6.gir
%{_datadir}/gir-1.0/FolksTelepathy-0.6.gir
%{_datadir}/glib-2.0/schemas/org.freedesktop.folks.gschema.xml
%{_datadir}/locale/*
%{_datadir}/vala/vapi/folks*.deps
%{_datadir}/vala/vapi/folks*.vapi

%clean
rm -rf %{buildroot}

%changelog
* Sat Sep 26 2015 tanggeliang <tanggeliang@gmail.com>
- update from 0.11.0 to 0.11.1
* Sun Mar 29 2015 tanggeliang <tanggeliang@gmail.com>
- update from 0.10.0 to 0.11.0
* Fri Sep 26 2014 tanggeliang <tanggeliang@gmail.com>
- update from 0.9.7.1 to 0.10.0
* Sat Jul 19 2014 tanggeliang <tanggeliang@gmail.com>
- update from 0.9.6 to 0.9.7.1
* Mon Jan 6 2014 tanggeliang <tanggeliang@gmail.com>
- update from 0.9.5 to 0.9.6
* Wed Sep 25 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.9.1 to 0.9.5
* Fri Apr 12 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.8.0 to 0.9.1
* Sun Oct 14 2012 tanggeliang <tanggeliang@gmail.com>
- update from 0.7.4.1 to 0.8.0
* Fri Sep 28 2012 tanggeliang <tanggeliang@gmail.com>
- update from 0.6.9 to 0.7.4.1
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
