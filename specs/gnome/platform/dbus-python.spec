Name:       dbus-python
Version:    1.1.1
Release:    6%{?dist}
Summary:    D-Bus Python Bindings

Group:      User Interface/GNOME
License:    GPLv2+
Url:        http://www.freedesktop.org
Source:     http://dbus.freedesktop.org/releases/%{name}/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  dbus-glib pygobject

%description

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --docdir=%{_docdir}/dbus-python-%{version} \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_includedir}/dbus-1.0/dbus/dbus-python.h
%{_libdir}/pkgconfig/dbus-python.pc
%{_libdir}/python2.7/site-packages/_dbus_bindings.*
%{_libdir}/python2.7/site-packages/_dbus_glib_bindings.*
/usr/lib/python2.7/site-packages/dbus/*.py*
/usr/lib/python2.7/site-packages/dbus/mainloop/*.py*

%files doc
%defattr(-,root,root,-)
%{_docdir}/dbus-python-%{version}/*

%clean
rm -rf %{buildroot}

%changelog
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
