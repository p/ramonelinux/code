Name:       yelp-xsl
Version:    3.18.0
Release:    1%{?dist}
Summary:    Yelp XSL

Group:      User Interface/GNOME
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/%{name}/3.18/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildArch:      noarch
BuildRequires:  libxslt intltool itstool
BuildRequires:  gettext xml-parser

%description
The Yelp XSL package contains XSL stylesheets that are used by the Yelp help browser to format Docbook and Mallard documents.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_datadir}/pkgconfig/yelp-xsl.pc
%{_datadir}/yelp-xsl/icons/hicolor/*/status/yelp-*.png
%{_datadir}/yelp-xsl/icons/hicolor/*/status/yelp-*.svg
%{_datadir}/yelp-xsl/js/jquery*.js
%{_datadir}/yelp-xsl/xslt/common/*
%{_datadir}/yelp-xsl/xslt/docbook/*
%{_datadir}/yelp-xsl/xslt/mallard/*

%clean
rm -rf %{buildroot}

%changelog
* Tue Sep 22 2015 tanggeliang <tanggeliang@gmail.com>
- update from 3.17.90 to 3.18.0
* Tue Sep 15 2015 tanggeliang <tanggeliang@gmail.com>
- update from 3.14.0 to 3.17.90
* Sun Sep 28 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.12.0 to 3.14.0
* Thu Mar 27 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.11.90 to 3.12.0
* Sat Mar 15 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.10.1 to 3.11.90
* Tue Oct 22 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.10.0 to 3.10.1
* Tue Sep 24 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.8.1 to 3.10.0
* Wed Aug 7 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.8.0 to 3.8.1
* Fri Apr 12 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.1 to 3.8.0
* Mon Oct 22 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.0 to 3.6.1
* Fri Sep 28 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.4.2 to 3.6.0
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
