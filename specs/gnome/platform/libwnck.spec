Name:       libwnck
Version:    3.14.0
Release:    1%{?dist}
Summary:    libwnck

Group:      User Interface/GNOME
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/%{name}/3.14/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  gtk+ intltool
BuildRequires:  gobject-introspection, startup-notification
BuildRequires:  gtk-doc gettext xml-parser

%description
The libwnck package contains the Window Navigator Construction Kit.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/wnck*
%{_includedir}/libwnck-3.0/libwnck/*.h
%{_libdir}/libwnck-3.*
%{_libdir}/pkgconfig/libwnck-3.0.pc
%{_libdir}/girepository-1.0/*
%{_datadir}/gir-1.0/*
%{_datadir}/locale/*

%files doc
%defattr(-,root,root,-)
%{_datadir}/gtk-doc/html/libwnck-3.0/*

%clean
rm -rf %{buildroot}

%changelog
* Sun Mar 29 2015 tanggeliang <tanggeliang@gmail.com>
- update from 3.4.9 to 3.14.0
* Sun Sep 28 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.4.7 to 3.4.9
* Sat Aug 24 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.4.6 to 3.4.7
* Sun Aug 18 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.4.5 to 3.4.6
* Fri Apr 12 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.4.4 to 3.4.5
* Mon Nov 26 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.4.3 to 3.4.4
* Fri Sep 28 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.4.2 to 3.4.3
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
