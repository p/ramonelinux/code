Name:       libgdata
Version:    0.17.3
Release:    1%{?dist}
Summary:    libgdata

Group:      Applications/Internet
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/libgdata/0.17/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  gnome-online-accounts libsoup liboauth
BuildRequires:  gobject-introspection gtk+ json-glib
BuildRequires:  gtk-doc intltool gettext xml-parser
BuildRequires:  gcr uhttpmock

%description
The libgdata package is a GLib-based library for accessing online service APIs using the GData protocol, most notably, Google's services.
It provides APIs to access the common Google services, and has full asynchronous support.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --disable-static \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_includedir}/libgdata/gdata/*
%{_libdir}/girepository-1.0/GData-0.0.typelib
%{_libdir}/libgdata.*
%{_libdir}/pkgconfig/libgdata.pc
%{_datadir}/gir-1.0/GData-0.0.gir
%{_datadir}/locale/*

%files doc
%defattr(-,root,root,-)
%{_datadir}/gtk-doc/html/gdata/*

%clean
rm -rf %{buildroot}

%changelog
* Wed Sep 23 2015 tanggeliang <tanggeliang@gmail.com>
- update from 0.16.1 to 0.17.3
* Sun Mar 29 2015 tanggeliang <tanggeliang@gmail.com>
- update from 0.15.1 to 0.16.1
* Sat Jul 19 2014 tanggeliang <tanggeliang@gmail.com>
- update from 0.15.0 to 0.15.1
* Sun Jan 5 2014 tanggeliang <tanggeliang@gmail.com>
- update from 0.13.3 to 0.15.0
* Fri Apr 12 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.13.2 to 0.13.3
* Sun Oct 14 2012 tanggeliang <tanggeliang@gmail.com>
- update from 0.13.1 to 0.13.2
* Fri Sep 28 2012 tanggeliang <tanggeliang@gmail.com>
- update from 0.12.0 to 0.13.1
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
