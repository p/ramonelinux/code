Name:       dconf
Version:    0.24.0
Release:    1%{?dist}
Summary:    DConf

Group:      User Interface/GNOME
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/%{name}/0.24/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  dbus gtk+ libxml vala gtk-doc
BuildRequires:  gettext intltool xml-parser
BuildRequires:  libxslt docbook-xsl docbook-xml docbook-xsl

%description
The DConf package contains a low-level configuration system.
Its main purpose is to provide a backend to GSettings on platforms that don't already have configuration storage systems.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --sysconfdir=/etc \
            --libexecdir=%{_libdir}/dconf \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/dconf
%{_includedir}/dconf-dbus-1/dconf-dbus-1.h
%{_includedir}/dconf/*.h
%{_includedir}/dconf/client/dconf-*.h
%{_includedir}/dconf/common/dconf-*.h
%{_libdir}/dconf/dconf-service
%{_libdir}/gio/modules/libdconfsettings.so
%{_libdir}/libdconf-dbus-1.so*
%{_libdir}/libdconf.so*
%{_libdir}/pkgconfig/dconf-dbus-1.pc
%{_libdir}/pkgconfig/dconf.pc
%{_datadir}/bash-completion/completions/dconf
%{_datadir}/dbus-1/services/ca.desrt.dconf.service
%{_datadir}/vala/vapi/dconf.deps
%{_datadir}/vala/vapi/dconf.vapi

%files doc
%defattr(-,root,root,-)
%{_datadir}/gtk-doc/html/dconf/*
%{_mandir}/man*/dconf*.*.gz

%post
gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
glib-compile-schemas %{_datadir}/glib-2.0/schemas &>/dev/null || :

%clean
rm -rf %{buildroot}

%changelog
* Sun Mar 29 2015 tanggeliang <tanggeliang@gmail.com>
- update from 0.22.0 to 0.24.0
* Sat Mar 28 2015 tanggeliang <tanggeliang@gmail.com>
- update from 0.21.0 to 0.22.0
* Tue Jul 22 2014 tanggeliang <tanggeliang@gmail.com>
- update from 0.20.0 to 0.21.0
* Sun Jul 20 2014 tanggeliang <tanggeliang@gmail.com>
- update from 0.19.91 to 0.20.0
* Sat Mar 15 2014 tanggeliang <tanggeliang@gmail.com>
- update from 0.18.0 to 0.19.91
* Tue Sep 24 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.17.0 to 0.18.0
* Sat Aug 10 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.16.0 to 0.17.0
* Fri Apr 12 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.14.1 to 0.16.0
* Mon Nov 26 2012 tanggeliang <tanggeliang@gmail.com>
- update from 0.14.0 to 0.14.1
* Fri Sep 28 2012 tanggeliang <tanggeliang@gmail.com>
- update from 0.12.1 to 0.14.0
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
