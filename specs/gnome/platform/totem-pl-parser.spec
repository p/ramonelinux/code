Name:       totem-pl-parser
Version:    3.10.5
Release:    1%{?dist}
Summary:    Totem PL Parser

Group:      User Interface/GNOME
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/%{name}/3.10/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  gmime intltool libsoup-gnome
BuildRequires:  gobject-introspection gnome-common
BuildRequires:  gtk-doc libarchive libgcrypt gettext xml-parser nettle

%description
The Totem PL Parser package contains a simple GObject-based library used to parse a host of playlist formats, as well as save those.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --disable-static \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_includedir}/totem-pl-parser/1/plparser/totem-*.h
%{_libdir}/girepository-1.0/TotemPlParser-1.0.typelib
%{_libdir}/libtotem-plparser-mini.*
%{_libdir}/libtotem-plparser.*
%{_libdir}/pkgconfig/totem-plparser-mini.pc
%{_libdir}/pkgconfig/totem-plparser.pc
%{_datadir}/gir-1.0/TotemPlParser-1.0.gir
%{_datadir}/locale/*/LC_MESSAGES/totem-pl-parser.mo

%files doc
%defattr(-,root,root,-)
%{_datadir}/gtk-doc/html/totem-pl-parser/*

%clean
rm -rf %{buildroot}

%changelog
* Wed Sep 23 2015 tanggeliang <tanggeliang@gmail.com>
- update from 3.10.2 to 3.10.5
* Thu Mar 27 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.10.1 to 3.10.2
* Sat Mar 15 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.10.0 to 3.10.1
* Tue Sep 24 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.4.5 to 3.10.0
* Thu May 30 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.4.4 to 3.4.5
* Sat Apr 13 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.4.3 to 3.4.4
* Fri Sep 28 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.4.2 to 3.4.3
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
