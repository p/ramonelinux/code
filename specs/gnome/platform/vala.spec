Name:       vala
Version:    0.30.0
Release:    1%{?dist}
Summary:    Vala

Group:      User Interface/GNOME
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/%{name}/0.30/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  glib libxslt
BuildRequires:  bison flex

%description
Vala is a new programming language that aims to bring modern programming language features to GNOME developers without imposing any additional runtime requirements and without using a different ABI compared to applications and libraries written in C.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/vala
%{_bindir}/vala-0.30
%{_bindir}/vala-gen-introspect
%{_bindir}/vala-gen-introspect-0.30
%{_bindir}/valac
%{_bindir}/valac-0.30
%{_bindir}/vapicheck
%{_bindir}/vapicheck-0.30
%{_bindir}/vapigen
%{_bindir}/vapigen-0.30
%{_includedir}/vala-0.30/vala*.h
%{_libdir}/libvala-0.30.la
%{_libdir}/libvala-0.30.so*
%{_libdir}/pkgconfig/libvala-0.30.pc
%{_libdir}/vala-0.30/gen-introspect-0.30
%{_datadir}/aclocal/va*.m4
%{_datadir}/pkgconfig/vapigen.pc
%{_datadir}/pkgconfig/vapigen-0.30.pc
%{_datadir}/vala/Makefile.vapigen
%{_datadir}/vala-0.30/vapi/*

%files doc
%defattr(-,root,root,-)
%{_datadir}/devhelp/books/vala-0.30/*
%{_mandir}/man1/*

%clean
rm -rf %{buildroot}

%changelog
* Tue Sep 22 2015 tanggeliang <tanggeliang@gmail.com>
- update from 0.29.3 to 0.30.0
* Tue Sep 15 2015 tanggeliang <tanggeliang@gmail.com>
- update from 0.24.0 to 0.29.3
* Sat Mar 29 2014 tanggeliang <tanggeliang@gmail.com>
- update from 0.22.1 to 0.24.0
* Tue Dec 3 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.22.0 to 0.22.1
* Wed Sep 25 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.20.1 to 0.22.0
* Fri Apr 12 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.18.1 to 0.20.1
* Mon Nov 26 2012 tanggeliang <tanggeliang@gmail.com>
- update from 0.18.0 to 0.18.1
* Fri Sep 28 2012 tanggeliang <tanggeliang@gmail.com>
- update from 0.16.1 to 0.18.0
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
