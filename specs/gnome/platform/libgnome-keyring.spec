Name:       libgnome-keyring
Version:    3.12.0
Release:    1%{?dist}
Summary:    libgnome-keyring

Group:      User Interface/GNOME
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/%{name}/3.12/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  dbus glib intltool libgcrypt
BuildRequires:  gobject-introspection vala
BuildRequires:  gtk-doc gettext xml-parser

%description
The libgnome-keyring is used by applications to integrate with the GNOME Keyring system.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_includedir}/gnome-keyring-1/gnome-keyring*.h
%{_libdir}/girepository-1.0/GnomeKeyring-1.0.typelib
%{_libdir}/libgnome-keyring.*
%{_libdir}/pkgconfig/gnome-keyring-1.pc
%{_datadir}/gir-1.0/GnomeKeyring-1.0.gir
%{_datadir}/locale/*
%{_datadir}/vala/vapi/gnome-keyring-1.vapi

%files doc
%defattr(-,root,root,-)
%{_datadir}/gtk-doc/html/gnome-keyring/*

%clean
rm -rf %{buildroot}

%changelog
* Sat Jul 19 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.10.1 to 3.12.0
* Sun Jan 5 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.10.0 to 3.10.1
* Tue Sep 24 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.8.0 to 3.10.0
* Fri Apr 12 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.0 to 3.8.0
* Fri Sep 28 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.4.1 to 3.6.0
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
