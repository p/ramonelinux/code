Name:       accountsservice
Version:    0.6.40
Release:    3%{?dist}
Summary:    AccountsService

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.freedesktop.org
Source:     http://www.freedesktop.org/software/%{name}/%{name}-%{version}.tar.xz
Patch:      %{name}-%{version}-systemd.diff

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  libxslt polkit systemd
BuildRequires:  gobject-introspection vala
BuildRequires:  intltool gettext xml-parser dbus-glib

%description
The AccountsService package provides a set of D-Bus interfaces for querying and manipulating user account information and an implementation of these interfaces based on the usermod(8), useradd(8) and userdel(8) commands.

%prep
%setup -q -n %{name}-%{version}
%patch -p1

%build
./configure --prefix=/usr \
            --sysconfdir=/etc \
            --localstatedir=/var \
            --libexecdir=%{_libdir}/accountsservice \
            --disable-static \
            --enable-systemd \
            --with-systemdsystemunitdir=/lib/systemd/system \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
/etc/dbus-1/system.d/org.freedesktop.Accounts.conf
%{_includedir}/accountsservice-1.0/act/act*.h
%{_libdir}/accountsservice/accounts-daemon
%{_libdir}/girepository-1.0/AccountsService-1.0.typelib
%{_libdir}/libaccountsservice.*
%{_libdir}/pkgconfig/accountsservice.pc
%{_datadir}/dbus-1/interfaces/org.freedesktop.Accounts*.xml
%{_datadir}/dbus-1/system-services/org.freedesktop.Accounts.service
%{_datadir}/gir-1.0/AccountsService-1.0.gir
%{_datadir}/gtk-doc/html/libaccountsservice/*
%{_datadir}/locale/*
%{_datadir}/polkit-1/actions/org.freedesktop.accounts.policy
%dir /var/lib/AccountsService/
%dir /var/lib/AccountsService/users
%dir /var/lib/AccountsService/icons
/lib/systemd/system/accounts-daemon.service

%post
systemctl enable accounts-daemon

%postun
systemctl disable accounts-daemon

%clean
rm -rf %{buildroot}

%changelog
* Sun Mar 29 2015 tanggeliang <tanggeliang@gmail.com>
- update from 0.6.37 to 0.6.40
* Mon Jul 21 2014 tanggeliang <tanggeliang@gmail.com>
- update from 0.6.34 to 0.6.37
* Thu Aug 8 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.6.31 to 0.6.34
* Thu May 30 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.6.30 to 0.6.31
* Sun Mar 17 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.6.26 to 0.6.30
* Mon Nov 26 2012 tanggeliang <tanggeliang@gmail.com>
- update from 0.6.25 to 0.6.26
* Mon Oct 22 2012 tanggeliang <tanggeliang@gmail.com>
- update from 0.6.24 to 0.6.25
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
