Name:       evolution-data-server
Version:    3.18.0
Release:    1%{?dist}
Summary:    Evolution Data Server

Group:      User Interface/GNOME
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/%{name}/3.18/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  db gnome-online-accounts gperf libgdata libical nss
BuildRequires:  libgweather gobject-introspection vala
BuildRequires:  bison flex libsoup sqlite gcr libgdata libsecret
BuildRequires:  intltool gettext xml-parser liboauth gtk-doc

%description
Evolution Data Server package provides a unified backend for programs that work with contacts, tasks, and calendar information.
It was originally developed for Evolution (hence the name), but is now used by other packages as well.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --libexecdir=%{_libdir}/evolution-data-server \
            --enable-vala-bindings \
            --libdir=%{_libdir} \
            --disable-uoa &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_includedir}/evolution-data-server/*
%{_libdir}/evolution-data-server/*
%{_libdir}/girepository-1.0/EBook-1.2.typelib
%{_libdir}/girepository-1.0/EBookContacts-1.2.typelib
%{_libdir}/girepository-1.0/EDataServer-1.2.typelib
%{_libdir}/libcamel-1.2.*
%{_libdir}/libebackend-1.2.*
%{_libdir}/libebook-1.2.*
%{_libdir}/libebook-contacts-1.2.*
%{_libdir}/libecal-1.2.*
%{_libdir}/libedata-book-1.2.*
%{_libdir}/libedata-cal-1.2.*
%{_libdir}/libedataserver-1.2.*
%{_libdir}/libedataserverui-1.2.*
%{_libdir}/pkgconfig/camel-1.2.pc
%{_libdir}/pkgconfig/evolution-data-server-1.2.pc
%{_libdir}/pkgconfig/libebackend-1.2.pc
%{_libdir}/pkgconfig/libebook-1.2.pc
%{_libdir}/pkgconfig/libecal-1.2.pc
%{_libdir}/pkgconfig/libedata-book-1.2.pc
%{_libdir}/pkgconfig/libedata-cal-1.2.pc
%{_libdir}/pkgconfig/libedataserver-1.2.pc
%{_libdir}/pkgconfig/libedataserverui-1.2.pc
%{_libdir}/pkgconfig/libebook-contacts-1.2.pc
%{_datadir}/GConf/gsettings/evolution-data-server.convert
%{_datadir}/dbus-1/services/org.gnome.evolution.dataserver.*.service
%{_datadir}/gir-1.0/E*-1.2.gir
%{_datadir}/glib-2.0/schemas/org.gnome.evolution*.gschema.xml
%{_datadir}/glib-2.0/schemas/org.gnome.Evolution*.gschema.xml
%{_datadir}/locale/*
%{_datadir}/pixmaps/evolution-data-server/category_*.png
%{_datadir}/vala/vapi/libebook-1.2.*
%{_datadir}/vala/vapi/libebook-contacts-1.2.*
%{_datadir}/vala/vapi/libedataserver-1.2.*

%post
glib-compile-schemas %{_datadir}/glib-2.0/schemas &>/dev/null || :

%clean
rm -rf %{buildroot}

%changelog
* Wed Sep 23 2015 tanggeliang <tanggeliang@gmail.com>
- update from 3.16.0 to 3.18.0
* Sun Mar 29 2015 tanggeliang <tanggeliang@gmail.com>
- update from 3.13.6 to 3.16.0
* Fri Sep 26 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.12.4 to 3.13.6
* Thu Jul 24 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.10.3 to 3.12.4
* Mon Jan 6 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.10.1 to 3.10.3
* Mon Oct 21 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.10.0 to 3.10.1
* Wed Sep 25 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.8.5 to 3.10.0
* Sat Aug 17 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.8.1 to 3.8.5
* Sat May 11 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.8.0 to 3.8.1
* Fri Apr 12 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.4 to 3.8.0
* Sun Mar 17 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.3 to 3.6.4
* Sun Feb 24 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.2 to 3.6.3
* Mon Nov 26 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.1 to 3.6.2
* Sat Oct 27 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.0 to 3.6.1
* Fri Sep 28 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.4.3 to 3.6.0
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
