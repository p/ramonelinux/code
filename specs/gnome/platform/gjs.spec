Name:       gjs
Version:    1.43.3
Release:    1%{?dist}
Summary:    Gjs

Group:      User Interface/GNOME
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/%{name}/1.43/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  cairo dbus-glib gobject-introspection js

%description
Gjs is a Javascript binding for GNOME.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/gjs
%{_bindir}/gjs-console
%{_includedir}/gjs-1.0/gi/*.h
%{_includedir}/gjs-1.0/gjs/*.h
%{_libdir}/gjs/girepository-1.0/GjsPrivate-1.0.typelib
%{_libdir}/libgjs.*
%{_libdir}/pkgconfig/gjs-1.0.pc
%{_libdir}/pkgconfig/gjs-internals-1.0.pc

%clean
rm -rf %{buildroot}

%changelog
* Sun Mar 29 2015 tanggeliang <tanggeliang@gmail.com>
- update from 1.42.0 to 1.43.3
* Thu Oct 9 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.40.1 to 1.42.0
* Sun Jul 20 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.40.0 to 1.40.1
* Thu Mar 27 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.39.91 to 1.40.0
* Sat Mar 15 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.38.1 to 1.39.91
* Tue Oct 22 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.37.6 to 1.38.1
* Tue Sep 24 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.37.4 to 1.37.6
* Wed Jul 24 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.36.0 to 1.37.4
* Fri Apr 12 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.34.0 to 1.36.0
* Fri Sep 28 2012 tanggeliang <tanggeliang@gmail.com>
- update from 1.32.0 to 1.34.0
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
