Name:       gcr
Version:    3.17.4
Release:    2%{?dist}
Summary:    Gcr

Group:      User Interface/GNOME
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/%{name}/3.17/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  gtk+ intltool libgcrypt libtasn1 p11-kit
BuildRequires:  gobject-introspection gnome-common vala
BuildRequires:  gtk-doc gettext xml-parser libxslt

%description
The Gcr package contains libraries used for displaying certificates and accessing key stores.
It also provides the viewer for crypto files on the GNOME Desktop.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --sysconfdir=/etc \
            --libexecdir=%{_libdir}/gnome-keyring \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/gcr-viewer
%{_includedir}/gck-1/gck/*.h
%{_includedir}/gcr-3/gcr/gcr*.h
%{_includedir}/gcr-3/ui/gcr-*.h
%{_libdir}/girepository-1.0/Gck-1.typelib
%{_libdir}/girepository-1.0/Gcr-3.typelib
%{_libdir}/girepository-1.0/GcrUi-3.typelib
%{_libdir}/gnome-keyring/gcr-prompter
%{_libdir}/libgck-1.*
%{_libdir}/libgcr-3.*
%{_libdir}/libgcr-base-3.*
%{_libdir}/libgcr-ui-3.*
%{_libdir}/pkgconfig/gck-1.pc
%{_libdir}/pkgconfig/gcr-3.pc
%{_libdir}/pkgconfig/gcr-base-3.pc
%{_libdir}/pkgconfig/gcr-ui-3.pc
%{_datadir}/GConf/gsettings/org.gnome.crypto.pgp.convert
%{_datadir}/GConf/gsettings/org.gnome.crypto.pgp_keyservers.convert
%{_datadir}/applications/gcr-prompter.desktop
%{_datadir}/applications/gcr-viewer.desktop
%{_datadir}/dbus-1/services/org.gnome.keyring.PrivatePrompter.service
%{_datadir}/dbus-1/services/org.gnome.keyring.SystemPrompter.service
%{_datadir}/gcr-3/ui/gcr-pkcs11-import-dialog.ui
%{_datadir}/gcr-3/ui/gcr-unlock-options-widget.ui
%{_datadir}/gir-1.0/Gck-1.gir
%{_datadir}/gir-1.0/Gcr-3.gir
%{_datadir}/gir-1.0/GcrUi-3.gir
%{_datadir}/glib-2.0/schemas/org.gnome.crypto.pgp.gschema.xml
%{_datadir}/icons/hicolor/16x16/apps/gcr-*.png
%{_datadir}/icons/hicolor/22x22/apps/gcr-*.png
%{_datadir}/icons/hicolor/24x24/apps/gcr-*.png
%{_datadir}/icons/hicolor/256x256/apps/gcr-*.png
%{_datadir}/icons/hicolor/32x32/apps/gcr-*.png
%{_datadir}/icons/hicolor/48x48/apps/gcr-*.png
%{_datadir}/locale/*
%{_datadir}/mime/packages/gcr-crypto-types.xml
%{_datadir}/vala/vapi/gck-1.*
%{_datadir}/vala/vapi/gcr-3.*
%{_datadir}/vala/vapi/gcr-ui-3.*
%{_datadir}/vala/vapi/pkcs11.vapi

%files doc
%defattr(-,root,root,-)
%{_datadir}/gtk-doc/html/gck/*
%{_datadir}/gtk-doc/html/gcr-3/*

%post
gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
glib-compile-schemas %{_datadir}/glib-2.0/schemas &>/dev/null || :

%clean
rm -rf %{buildroot}

%changelog
* Wed Sep 16 2015 tanggeliang <tanggeliang@gmail.com>
- update from 3.16.0 to 3.17.4
* Sun Mar 29 2015 tanggeliang <tanggeliang@gmail.com>
- update from 3.14.0 to 3.16.0
* Wed Sep 24 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.12.2 to 3.14.0
* Sun Jul 20 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.12.0 to 3.12.2
* Thu Mar 27 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.11.91 to 3.12.0
* Sat Mar 15 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.10.1 to 3.11.91
* Tue Dec 3 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.10.0 to 3.10.1
* Tue Sep 24 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.8.2 to 3.10.0
* Wed Jul 24 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.8.0 to 3.8.2
* Fri Apr 12 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.2 to 3.8.0
* Mon Nov 26 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.1 to 3.6.2
* Sat Oct 27 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.0 to 3.6.1
* Fri Sep 28 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.4.1 to 3.6.0
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
