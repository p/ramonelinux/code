Name:       libgtop
Version:    2.32.0
Release:    1%{?dist}
Summary:    LibGTop

Group:      User Interface/GNOME
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/%{name}/2.32/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  glib intltool
BuildRequires:  gobject-introspection
BuildRequires:  gtk-doc libx11 libice gettext xml-parser

%description
The LibGTop package contains the GNOME top libraries.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --disable-static \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

rm -f %{buildroot}/%{_infodir}/dir

%files
%defattr(-,root,root,-)
%{_includedir}/libgtop-2.0/*
%{_libdir}/girepository-1.0/GTop-2.0.typelib
%{_libdir}/libgtop-2.0.*
%{_libdir}/pkgconfig/libgtop-2.0.pc
%{_datadir}/gir-1.0/GTop-2.0.gir
%{_datadir}/locale/*

%files doc
%defattr(-,root,root,-)
%{_datadir}/gtk-doc/*
%{_infodir}/*

%clean
rm -rf %{buildroot}

%changelog
* Wed Sep 23 2015 tanggeliang <tanggeliang@gmail.com>
- update from 2.30.0 to 2.32.0
* Sat Jul 19 2014 tanggeliang <tanggeliang@gmail.com>
- update from 2.28.5 to 2.30.0
* Sat Aug 24 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.28.4 to 2.28.5
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
