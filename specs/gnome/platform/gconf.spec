Name:       gconf
Version:    3.2.6
Release:    3%{?dist}
Summary:    GConf

Group:      User Interface/GNOME
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/GConf/3.2/GConf-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  dbus-glib intltool libxml
BuildRequires:  gobject-introspection gtk+ polkit openldap
BuildRequires:  gtk-doc gettext xml-parser 

%description
The GConf package contains a configuration database system used by many GNOME applications.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n GConf-%{version}

%build
./configure --prefix=/usr \
            --sysconfdir=/etc \
            --libexecdir=%{_libdir}/GConf \
            --disable-orbit \
            --disable-static \
            --disable-orbit \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}

make DESTDIR=%{buildroot} install
ln -s gconf.xml.defaults %{buildroot}/etc/gconf/gconf.xml.system

mkdir -p %{buildroot}%{_sysconfdir}/gconf/schemas

%files
%defattr(-,root,root,-)
%{_sysconfdir}/dbus-1/system.d/org.gnome.GConf.Defaults.conf
%{_sysconfdir}/gconf/2/evoldap.conf
%{_sysconfdir}/gconf/2/path
%{_sysconfdir}/xdg/autostart/gsettings-data-convert.desktop
%dir %{_sysconfdir}/gconf
%dir %{_sysconfdir}/gconf/2
%dir %{_sysconfdir}/gconf/gconf.xml.defaults
%dir %{_sysconfdir}/gconf/gconf.xml.mandatory
%{_sysconfdir}/gconf/gconf.xml.system
%dir %{_sysconfdir}/gconf/schemas
%{_bindir}/gconf-merge-tree
%{_bindir}/gconftool-2
%{_bindir}/gsettings-data-convert
%{_bindir}/gsettings-schema-convert
%{_includedir}/gconf/2/gconf/gconf*.h
%{_libdir}/GConf/*
%{_libdir}/libgconf-2.*
%{_libdir}/pkgconfig/gconf-2.0.pc
%{_libdir}/gio/modules/libgsettingsgconfbackend.*
%{_libdir}/girepository-1.0/GConf-2.0.typelib
%{_datadir}/GConf/schema/evoldap.schema
%{_datadir}/aclocal/*
%{_datadir}/dbus-1/*
%{_datadir}/locale/*
%{_datadir}/polkit-1/*
%{_datadir}/sgml/*
%{_datadir}/gir-1.0/GConf-2.0.gir

%files doc
%defattr(-,root,root,-)
%{_datadir}/gtk-doc/html/gconf/*
%{_mandir}/*/*

%post
install -v -m755 -d /etc/gconf/gconf.xml.system

%postun
rm -rf /etc/gconf/gconf.xml.system

%clean
rm -rf %{buildroot}

%changelog
* Sun Feb 24 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.2.5 to 3.2.6
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
