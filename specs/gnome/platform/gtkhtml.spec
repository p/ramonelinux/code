Name:       gtkhtml
Version:    4.6.6
Release:    1%{?dist}
Summary:    GtkHTML

Group:      User Interface/GNOME
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/%{name}/4.6/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  enchant gnome-icon-theme gsettings-desktop-schemas gtk+ iso-codes
BuildRequires:  libsoup intltool gettext xml-parser

%description
The GtkHTML package contains a lightweight HTML rendering/printing/editing engine.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/gtkhtml-editor-test
%{_includedir}/libgtkhtml-4.0/editor/gtkhtml-*.h
%{_includedir}/libgtkhtml-4.0/gtkhtml/*html*.h
%{_libdir}/libgtkhtml-4.0.la
%{_libdir}/libgtkhtml-4.0.so*
%{_libdir}/libgtkhtml-editor-4.0.la
%{_libdir}/libgtkhtml-editor-4.0.so*
%{_libdir}/pkgconfig/gtkhtml-editor-4.0.pc
%{_libdir}/pkgconfig/libgtkhtml-4.0.pc
%{_datadir}/gtkhtml-4.0/*
%{_datadir}/locale/*/LC_MESSAGES/gtkhtml-4.0.mo

%clean
rm -rf %{buildroot}

%changelog
* Sat Aug 17 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.6.4 to 4.6.6
* Wed Apr 10 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.6.1 to 4.6.4
* Mon Nov 26 2012 tanggeliang <tanggeliang@gmail.com>
- update from 4.6.0 to 4.6.1
* Fri Sep 28 2012 tanggeliang <tanggeliang@gmail.com>
- update from 4.4.3 to 4.6.0
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
