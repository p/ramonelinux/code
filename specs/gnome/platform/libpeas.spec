Name:       libpeas
Version:    1.16.0
Release:    1%{?dist}
Summary:    libpeas

Group:      User Interface/GNOME
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/%{name}/1.16/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  gobject-introspection gtk+
BuildRequires:  gjs pygobject
BuildRequires:  gtk-doc intltool gettext xml-parser

%description
libpeas is a GObject based plugins engine, and is targetted at giving every application the chance to assume its own extensibility.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/peas-demo
%{_includedir}/libpeas-1.0/libpeas-gtk/peas-gtk*.h
%{_includedir}/libpeas-1.0/libpeas/peas*.h
%{_libdir}/girepository-1.0/Peas-1.0.typelib
%{_libdir}/girepository-1.0/PeasGtk-1.0.typelib
%{_libdir}/libpeas-1.0.*
%{_libdir}/libpeas-1.0/loaders/*.la
%{_libdir}/libpeas-1.0/loaders/*.so
%{_libdir}/libpeas-gtk-1.0.*
%{_libdir}/peas-demo/plugins/*
%{_libdir}/pkgconfig/libpeas-1.0.pc
%{_libdir}/pkgconfig/libpeas-gtk-1.0.pc
%{_datadir}/gir-1.0/Peas-1.0.gir
%{_datadir}/gir-1.0/PeasGtk-1.0.gir
%{_datadir}/icons/hicolor/*/actions/libpeas-plugin.png
%{_datadir}/icons/hicolor/scalable/actions/libpeas-plugin.svg
%{_datadir}/locale/*

%files doc
%defattr(-,root,root,-)
%{_datadir}/gtk-doc/html/libpeas/*

%post
gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :

%clean
rm -rf %{buildroot}

%changelog
* Sat Sep 26 2015 tanggeliang <tanggeliang@gmail.com>
- update from 1.14.0 to 1.16.0
* Sun Mar 29 2015 tanggeliang <tanggeliang@gmail.com>
- update from 1.12.1 to 1.14.0
* Wed Sep 24 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.10.0 to 1.12.1
* Sat Jul 19 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.9.0 to 1.10.0
* Wed Jul 24 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.8.0 to 1.9.0
* Fri Apr 12 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.6.2 to 1.8.0
* Tue Mar 19 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.6.1 to 1.6.2
* Sun Oct 28 2012 tanggeliang <tanggeliang@gmail.com>
- update from 1.6.0 to 1.6.1
* Fri Sep 28 2012 tanggeliang <tanggeliang@gmail.com>
- update from 1.4.0 to 1.6.0
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
