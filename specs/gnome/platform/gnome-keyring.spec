Name:       gnome-keyring
Version:    3.17.91
Release:    1%{?dist}
Summary:    GNOME Keyring

Group:      User Interface/GNOME
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/%{name}/3.17/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  dbus gcr
BuildRequires:  linux-pam
BuildRequires:  intltool gettext xml-parser
BuildRequires:  gtk+ p11-kit libgcrypt libtasn1
BuildRequires:  libxslt docbook-xml docbook-xsl

%description
The GNOME Keyring package contains a daemon that keeps passwords and other secrets for users.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --sysconfdir=/etc \
            --with-pam-dir=/%{_lib}/security \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
/etc/xdg/autostart/gnome-keyring-pkcs11.desktop
/etc/xdg/autostart/gnome-keyring-secrets.desktop
/etc/xdg/autostart/gnome-keyring-ssh.desktop
/%{_lib}/security/pam_gnome_keyring.*
%{_bindir}/gnome-keyring
%{_bindir}/gnome-keyring-3
%{_bindir}/gnome-keyring-daemon
%{_libdir}/gnome-keyring/devel/gkm-gnome2-store-standalone.*
%{_libdir}/gnome-keyring/devel/gkm-secret-store-standalone.*
%{_libdir}/gnome-keyring/devel/gkm-ssh-store-standalone.*
%{_libdir}/gnome-keyring/devel/gkm-xdg-store-standalone.*
%{_libdir}/pkcs11/gnome-keyring-pkcs11.*
%{_datadir}/GConf/gsettings/org.gnome.crypto.cache.convert
%{_datadir}/dbus-1/services/org.freedesktop.secrets.service
%{_datadir}/dbus-1/services/org.gnome.keyring.service
%{_datadir}/glib-2.0/schemas/org.gnome.crypto.cache.gschema.xml
%{_datadir}/locale/*/LC_MESSAGES/gnome-keyring.mo
%{_datadir}/p11-kit/modules/gnome-keyring.module
%{_mandir}/man1/gnome-keyring-3.1.gz
%{_mandir}/man1/gnome-keyring-daemon.1.gz
%{_mandir}/man1/gnome-keyring.1.gz

%clean
rm -rf %{buildroot}

%post
/sbin/ldconfig
update-mime-database %{_datadir}/mime &> /dev/null || :
touch %{_datadir}/icons/hicolor >&/dev/null || :
gtk-update-icon-cache %{_datadir}/icons/hicolor >&/dev/null || :
glib-compile-schemas %{_datadir}/glib-2.0/schemas

%postun
/sbin/ldconfig
update-mime-database %{_datadir}/mime &> /dev/null || :
touch --no-create %{_datadir}/icons/hicolor >&/dev/null || :

%changelog
* Sun Sep 20 2015 tanggeliang <tanggeliang@gmail.com>
- update from 3.15.92 to 3.17.91
* Sat Mar 28 2015 tanggeliang <tanggeliang@gmail.com>
- update from 3.14.0 to 3.15.92
* Wed Sep 24 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.12.2 to 3.14.0
* Sat Jul 19 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.12.0 to 3.12.2
* Thu Mar 27 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.10.1 to 3.12.0
* Tue Dec 3 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.10.0 to 3.10.1
* Tue Sep 24 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.8.2 to 3.10.0
* Sat May 11 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.8.0 to 3.8.2
* Sat Apr 13 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.2 to 3.8.0
* Mon Nov 26 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.1 to 3.6.2
* Sat Oct 27 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.0 to 3.6.1
* Fri Sep 28 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.4.1 to 3.6.0
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
