Name:       desktop-file-utils
Version:    0.22
Release:    1%{?dist}
Summary:    desktop-file-utils

Group:      User Interface/GNOME
License:    GPLv2+
Url:        http://www.freedesktop.org
Source:     http://freedesktop.org/software/%{name}/releases/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  glib
Requires:       coreutils  

%description
The desktop-file-utils package contains command line utilities for working with desktop entries.
These utilities are used by Desktop Environments and other applications to manipulate the MIME-types application databases and help adhere to the Desktop Entry Specification.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/desktop-file-edit
%{_bindir}/desktop-file-install
%{_bindir}/desktop-file-validate
%{_bindir}/update-desktop-database

%files doc
%defattr(-,root,root,-)
%{_mandir}/*/*

%post
cat >> /etc/profile << "EOF"
XDG_DATA_DIRS=/usr/share:/usr/local/share
XDG_CONFIG_DIRS=/etc/xdg
export XDG_DATA_DIRS XDG_CONFIG_DIRS
EOF

%clean
rm -rf %{buildroot}

%changelog
* Wed Oct 2 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.21 to 0.22
* Mon Oct 22 2012 tanggeliang <tanggeliang@gmail.com>
- update from 0.20 to 0.21
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
