Name:       libgweather
Version:    3.18.0
Release:    1%{?dist}
Summary:    libgweather

Group:      User Interface/GNOME
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/%{name}/3.18/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  gtk+ libsoup geocode-glib
BuildRequires:  gobject-introspection glib libxml
BuildRequires:  gtk-doc intltool gettext xml-parser

%description
The libgweather package is a library used to access weather information from online services for numerous locations.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --enable-locations-compression \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_includedir}/libgweather-3.0/libgweather/*.h
%{_libdir}/girepository-1.0/GWeather-3.0.typelib
%{_libdir}/libgweather-3.*
%{_libdir}/pkgconfig/gweather-3.0.pc
%{_datadir}/gir-1.0/GWeather-3.0.gir
%{_datadir}/glib-2.0/schemas/org.gnome.GWeather.enums.xml
%{_datadir}/glib-2.0/schemas/org.gnome.GWeather.gschema.xml
%{_datadir}/libgweather/*
%{_datadir}/locale/*

%files doc
%defattr(-,root,root,-)
%{_datadir}/gtk-doc/html/libgweather-3.0/*

%clean
rm -rf %{buildroot}

%post
gtk-update-icon-cache %{_datadir}/icons/gnome &>/dev/null || :
glib-compile-schemas %{_datadir}/glib-2.0/schemas &>/dev/null || :

%postun

%changelog
* Tue Sep 22 2015 tanggeliang <tanggeliang@gmail.com>
- update from 3.17.92 to 3.18.0
* Wed Sep 16 2015 tanggeliang <tanggeliang@gmail.com>
- update from 3.16.0 to 3.17.92
* Sun Mar 29 2015 tanggeliang <tanggeliang@gmail.com>
- update from 3.14.0 to 3.16.0
* Wed Sep 24 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.12.2 to 3.14.0
* Sat Jul 19 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.10.0 to 3.12.2
* Tue Sep 24 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.8.2 to 3.10.0
* Wed Jul 24 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.8.0 to 3.8.2
* Fri Apr 12 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.2 to 3.8.0
* Mon Nov 26 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.1 to 3.6.2
* Sat Oct 27 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.0 to 3.6.1
* Fri Sep 28 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.4.1 to 3.6.0
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
