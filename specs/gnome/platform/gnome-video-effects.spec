Name:       gnome-video-effects
Version:    0.4.1
Release:    1%{?dist}
Summary:    GNOME Video Effects

Group:      User Interface/GNOME
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/%{name}/0.4/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  intltool gettext xml-parser

%description
The GNOME Video Effects package contains a collection of GStreamer effects.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_datadir}/gnome-video-effects/*.effect
%{_datadir}/pkgconfig/gnome-video-effects.pc

%clean
rm -rf %{buildroot}

%changelog
* Sat Mar 15 2014 tanggeliang <tanggeliang@gmail.com>
- update from 0.4.0 to 0.4.1
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
