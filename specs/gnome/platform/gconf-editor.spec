Name:       gconf-editor
Version:    3.0.1
Release:    5%{?dist}
Summary:    Editor/admin tool for GConf

Group:      Applications/System
License:    GPLv2+ and GFDL
Url:        http://www.gnome.org
Source0:    http://download.gnome.org/sources/%{name}/3.0/%{name}-%{version}.tar.xz

BuildRequires:  gconf gtk+ desktop-file-utils gettext gnome-doc-utils intltool
BuildRequires:  autoconf automake m4 libtool xml-parser libxslt docbook-xml docbook-xsl
Requires(pre):  gconf
Requires(post): gconf
Requires(preun):gconf

%description
gconf-editor allows you to browse and modify GConf configuration sources.

%prep
%setup -q

%build
%configure --disable-scrollkeeper
make %{?_smp_mflags}

%install
export GCONF_DISABLE_MAKEFILE_SCHEMA_INSTALL=1
make install DESTDIR=$RPM_BUILD_ROOT
unset GCONF_DISABLE_MAKEFILE_SCHEMA_INSTALL

# stuff we don't want
rm -rf $RPM_BUILD_ROOT/var/scrollkeeper

desktop-file-install --vendor gnome --delete-original       \
  --dir $RPM_BUILD_ROOT%{_datadir}/applications             \
  $RPM_BUILD_ROOT%{_datadir}/applications/*

%find_lang %{name} --with-gnome

%post
export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
gconftool-2 --makefile-install-rule %{_sysconfdir}/gconf/schemas/gconf-editor.schemas > /dev/null || :

touch --no-create %{_datadir}/icons/hicolor >& /dev/null || :

%pre
if [ "$1" -gt 1 ]; then
  export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
  gconftool-2 --makefile-uninstall-rule %{_sysconfdir}/gconf/schemas/gconf-editor.schemas > /dev/null || :
fi

%preun
if [ "$1" -eq 0 ]; then
  export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
  gconftool-2 --makefile-uninstall-rule %{_sysconfdir}/gconf/schemas/gconf-editor.schemas > /dev/null || :
fi

%postun
if [ $1 -eq 0 ]; then
  touch --no-create %{_datadir}/icons/hicolor &>/dev/null
  gtk-update-icon-cache %{_datadir}/icons/hicolor >&/dev/null || :
fi

%posttrans
gtk-update-icon-cache %{_datadir}/icons/hicolor >&/dev/null || :

%files -f %{name}.lang
%defattr(-,root,root)
%{_sysconfdir}/gconf/schemas/gconf-editor.schemas
%{_bindir}/gconf-editor
%{_datadir}/icons/hicolor/*/apps/gconf-editor.png
%{_datadir}/gconf-editor
%{_datadir}/applications/gnome-gconf-editor.desktop
%dir %{_datadir}/omf/gconf-editor
%{_mandir}/man1/gconf-editor.1.gz

%changelog
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
