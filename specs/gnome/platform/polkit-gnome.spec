Name:       polkit-gnome
Version:    0.105
Release:    10%{?dist}
Summary:    Polkit GNOME

Group:      User Interface/GNOME
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/%{name}/0.105/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  gtk+ polkit
BuildRequires:  intltool gettext xml-parser

%description
The Polkit GNOME package provides an Authentication Agent for Polkit that integrates well with the GNOME Desktop environment.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --libexecdir=%{_libdir}/polkit-gnome &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

pushd %{buildroot}
mkdir -p etc/xdg/autostart &&
cat > etc/xdg/autostart/polkit-gnome-authentication-agent-1.desktop << "EOF"
[Desktop Entry]
Name=PolicyKit Authentication Agent
Comment=PolicyKit Authentication Agent
Exec=%{_libdir}/polkit-gnome/polkit-gnome-authentication-agent-1
Terminal=false
Type=Application
Categories=
NoDisplay=true
OnlyShowIn=GNOME;XFCE;Unity;
AutostartCondition=GNOME3 unless-session gnome
EOF
popd

%files
%defattr(-,root,root,-)
/etc/xdg/autostart/polkit-gnome-authentication-agent-1.desktop
%{_libdir}/polkit-gnome/polkit-gnome-authentication-agent-1
%{_datadir}/locale/*

%clean
rm -rf %{buildroot}

%changelog
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
