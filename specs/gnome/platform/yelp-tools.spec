Name:       yelp-tools
Version:    3.18.0
Release:    1%{?dist}
Summary:    Yelp Tools

Group:      User Interface/GNOME
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/%{name}/3.18/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  yelp-xsl
BuildRequires:  libxml libxslt itstool

%description
Yelp Tools is a collection of scripts and build utilities to help create, manage, and publish documentation for Yelp and the web.
Most of the heavy lifting is done by packages like Yelp Xsl and itstool.
This package just wraps things up in a developer-friendly way.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/yelp-build
%{_bindir}/yelp-check
%{_bindir}/yelp-new
%{_datadir}/aclocal/yelp.m4
%{_datadir}/yelp-tools/templates/task.page
%{_datadir}/yelp-tools/xslt/*.xsl

%clean
rm -rf %{buildroot}

%changelog
* Tue Sep 22 2015 tanggeliang <tanggeliang@gmail.com>
- update from 3.17.4 to 3.18.0
* Tue Sep 15 2015 tanggeliang <tanggeliang@gmail.com>
- update from 3.14.0 to 3.17.4
* Sun Sep 28 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.9.1 to 3.14.0
* Wed Aug 7 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.4.1 to 3.9.1
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
