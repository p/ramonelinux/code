%define _version 2.91

Name:       vte
Version:    0.42.0
Release:    1%{?dist}
Summary:    VTE

Group:      User Interface/GNOME
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/%{name}/0.42/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  gtk+ intltool
BuildRequires:  gobject-introspection
BuildRequires:  gtk-doc gettext xml-parser
BuildRequires:  vala gnutls

%description
The VTE package contains a termcap file implementation for terminal emulators.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --sysconfdir=/etc \
            --disable-static \
            --enable-introspection \
            --libexecdir=%{_libdir}/vte-2.90 \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
/etc/profile.d/vte.sh
%{_bindir}/vte-%{_version}
%{_includedir}/vte-%{_version}/vte/*.h
%{_libdir}/girepository-1.0/Vte-%{_version}.typelib
%{_libdir}/libvte-%{_version}.*
%{_libdir}/pkgconfig/vte-%{_version}.pc
%{_datadir}/gir-1.0/Vte-%{_version}.gir
%{_datadir}/locale/*
%{_datadir}/vala/vapi/vte-%{_version}.vapi

%files doc
%defattr(-,root,root,-)
%{_datadir}/gtk-doc/html/vte-%{_version}/*

%clean
rm -rf %{buildroot}

%changelog
* Wed Sep 23 2015 tanggeliang <tanggeliang@gmail.com>
- update from 0.41.90 to 0.42.0
* Wed Sep 16 2015 tanggeliang <tanggeliang@gmail.com>
- update from 0.40.0 to 0.41.90
* Sun Mar 29 2015 tanggeliang <tanggeliang@gmail.com>
- update from 0.38.0 to 0.40.0
* Fri Sep 26 2014 tanggeliang <tanggeliang@gmail.com>
- update from 0.26.3 to 0.38.0
* Sat Jul 19 2014 tanggeliang <tanggeliang@gmail.com>
- update from 0.36.0 to 0.36.3
* Thu Mar 27 2014 tanggeliang <tanggeliang@gmail.com>
- update from 0.35.2 to 0.36.0
* Sat Mar 15 2014 tanggeliang <tanggeliang@gmail.com>
- update from 0.34.9 to 0.35.2
* Wed Oct 23 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.34.8 to 0.34.9
* Tue Sep 24 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.34.7 to 0.34.8
* Fri Jul 26 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.34.3 to 0.34.7
* Sat Apr 13 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.34.2 to 0.34.3
* Mon Nov 26 2012 tanggeliang <tanggeliang@gmail.com>
- update from 0.34.1 to 0.34.2
* Sun Oct 28 2012 tanggeliang <tanggeliang@gmail.com>
- update from 0.34.0 to 0.34.1
* Fri Sep 28 2012 tanggeliang <tanggeliang@gmail.com>
- update from 0.32.2 to 0.34.0
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
