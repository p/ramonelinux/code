Name:       gnome-online-accounts
Version:    3.18.0
Release:    1%{?dist}
Summary:    GNOME Online Accounts

Group:      User Interface/GNOME
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/%{name}/3.18/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  gcr libgnome-keyring libnotify librest libsecret 
BuildRequires:	libsoup json-glib webkitgtk
BuildRequires:  gobject-introspection
BuildRequires:  gtk-doc intltool gettext
BuildRequires:  libxslt xml-parser geoclue dbus-glib
BuildRequires:  gst-plugins-base libxt mesa icu
BuildRequires:  docbook-xsl telepathy-glib libaccounts-glib

%description
The GNOME Online Accounts package contains a framework used to access the user's online accounts.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --libexecdir=%{_libdir}/gnome-online-accounts \
            --disable-static \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_includedir}/goa-1.0/goa/goa*.h
%{_includedir}/goa-1.0/goabackend/goa*.h
%{_libdir}/girepository-1.0/Goa-1.0.typelib
%{_libdir}/goa-1.0/include/goaconfig.h
%{_libdir}/goa-1.0/web-extensions/libgoawebextension.*
%{_libdir}/gnome-online-accounts/goa-daemon
%{_libdir}/libgoa-1.0.*
%{_libdir}/libgoa-backend-1.0.*
%{_libdir}/pkgconfig/goa-1.0.pc
%{_libdir}/pkgconfig/goa-backend-1.0.pc
%{_datadir}/dbus-1/services/org.gnome.OnlineAccounts.service
%{_datadir}/gir-1.0/Goa-1.0.gir
%{_datadir}/glib-2.0/schemas/org.gnome.online-accounts.gschema.xml
%{_datadir}/gnome-online-accounts/irc-networks.xml
%{_datadir}/icons/hicolor/*x*/apps/goa-account*.png
%{_datadir}/icons/hicolor/*x*/apps/im-*.png
%{_datadir}/icons/hicolor/scalable/apps/im-*.svg
%{_datadir}/locale/*

%files doc
%defattr(-,root,root,-)
%{_mandir}/man8/goa-daemon.8.gz

%post
glib-compile-schemas %{_datadir}/glib-2.0/schemas &> /dev/null || :
gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :

%clean
rm -rf %{buildroot}

%changelog
* Wed Sep 23 2015 tanggeliang <tanggeliang@gmail.com>
- update from 3.17.92.1 to 3.18.0
* Sun Sep 20 2015 tanggeliang <tanggeliang@gmail.com>
- update from 3.17.92 to 3.17.92.1
* Wed Sep 16 2015 tanggeliang <tanggeliang@gmail.com>
- update from 3.16.0 to 3.17.92
* Sat Mar 28 2015 tanggeliang <tanggeliang@gmail.com>
- update from 3.14.0 to 3.16.0
* Wed Sep 24 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.12.4 to 3.14.0
* Sat Jul 19 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.11.92 to 3.12.4
* Tue Mar 18 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.10.2 to 3.11.92
* Sun Jan 5 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.10.1 to 3.10.2
* Mon Oct 21 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.10.0 to 3.10.1
* Wed Sep 25 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.9.92 to 3.10.0
* Tue Sep 24 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.8.2 to 3.9.92
* Wed Jul 24 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.8.1 to 3.8.2
* Sat May 11 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.8.0 to 3.8.1
* Fri Apr 12 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.2 to 3.8.0
* Mon Nov 26 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.1 to 3.6.2
* Sat Oct 27 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.0 to 3.6.1
* Fri Sep 28 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.4.2 to 3.6.0
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
