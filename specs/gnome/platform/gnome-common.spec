Name:       gnome-common
Version:    3.18.0
Release:    1%{?dist}
Summary:    gnome-common

Group:      User Interface/GNOME
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/%{name}/3.18/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build

%description

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/gnome-autogen.sh
%{_datadir}/aclocal/ax_*.m4
%{_datadir}/aclocal/gnome-*.m4

%clean
rm -rf %{buildroot}

%changelog
* Tue Sep 22 2015 tanggeliang <tanggeliang@gmail.com>
- update from 3.14.0 to 3.18.0
* Tue Sep 15 2015 tanggeliang <tanggeliang@gmail.com>
- update from 3.10.0 to 3.14.0
* Sat Mar 22 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.7.4 to 3.10.0
* Sun Apr 14 2013 tanggeliang <tanggeliang@gmail.com>
- create
