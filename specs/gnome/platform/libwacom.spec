Name:       libwacom
Version:    0.15
Release:    1%{?dist}
Summary:    libWacom

Group:      User Interface/GNOME
License:    GPLv2+
Url:        http://linuxwacom.sourceforge.net
Source:     http://downloads.sourceforge.net/linuxwacom/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  glib systemd-gudev

%description
The libWacom is a library used to identify wacom tablets and their model-specific features.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --disable-static \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/libwacom-list-local-devices
%{_includedir}/libwacom-1.0/libwacom/libwacom.h
%{_libdir}/libwacom.*
%{_libdir}/pkgconfig/libwacom.pc
%{_datadir}/libwacom/*

%clean
rm -rf %{buildroot}

%changelog
* Wed Sep 23 2015 tanggeliang <tanggeliang@gmail.com>
- update from 0.13 to 0.15
* Fri Apr 12 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.6 to 0.7
* Fri Sep 28 2012 tanggeliang <tanggeliang@gmail.com>
- update from 0.4 to 0.6
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
