Name:       libsecret
Version:    0.18.3
Release:    2%{?dist}
Summary:    libsecret

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/%{name}/%{version}/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  glib
BuildRequires:  gobject-introspection libgcrypt vala
BuildRequires:  gtk-doc intltool gettext xml-parser libxslt
BuildRequires:  docbook-xml docbook-xsl

%description
The AccountsService package provides a set of D-Bus interfaces for querying and manipulating user account information and an implementation of these interfaces based on the usermod(8), useradd(8) and userdel(8) commands.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --disable-static \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/secret-tool
%{_includedir}/libsecret-1/libsecret/secret-*.h
%{_includedir}/libsecret-1/libsecret/secret.h
%{_libdir}/girepository-1.0/Secret-1.typelib
%{_libdir}/libsecret-1.la
%{_libdir}/libsecret-1.so*
%{_libdir}/pkgconfig/libsecret-1.pc
%{_libdir}/pkgconfig/libsecret-unstable.pc
%{_datadir}/gir-1.0/Secret-1.gir
%{_datadir}/locale/*/LC_MESSAGES/libsecret.mo
%{_datadir}/vala/vapi/libsecret-*

%files doc
%defattr(-,root,root,-)
%{_datadir}/gtk-doc/html/libsecret-1/*
%{_mandir}/man1/secret-tool.1.gz

%clean
rm -rf %{buildroot}

%changelog
* Wed Sep 16 2015 tanggeliang <tanggeliang@gmail.com>
- update from 0.18 to 0.18.3
* Sat Mar 15 2014 tanggeliang <tanggeliang@gmail.com>
- update from 0.16 to 0.18
* Tue Sep 24 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.15 to 0.16
* Fri Apr 12 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.14 to 0.15
* Thu Apr 11 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.11 to 0.14
* Sun Oct 28 2012 tanggeliang <tanggeliang@gmail.com>
- update from 0.10 to 0.11
* Fri Sep 28 2012 tanggeliang <tanggeliang@gmail.com>
- create
