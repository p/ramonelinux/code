Name:       libmediaart
Version:    1.9.0
Release:    1%{?dist}
Summary:    libmediaart

Group:      User Interface/GNOME
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/%{name}/1.9/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  gdk-pixbuf glib gtk-doc vala

%description

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_includedir}/libmediaart-2.0/libmediaart/*.h
%{_libdir}/libmediaart-2.0.la
%{_libdir}/libmediaart-2.0.so*
%{_libdir}/pkgconfig/libmediaart-2.0.pc
%{_datadir}/gtk-doc/html/libmediaart/*

%clean
rm -rf %{buildroot}

%changelog
* Wed Sep 23 2015 tanggeliang <tanggeliang@gmail.com>
- create
