Name:       gnome-js-common
Version:    0.1.2
Release:    6%{?dist}
Summary:    GNOME JS Common

Group:      User Interface/GNOME
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/%{name}/0.1/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  intltool gettext xml-parser

%description
The GNOME JS Common package provides common modules for GNOME JavaScript bindings.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_libdir}/gnome-js/*.js
%{_libdir}/gnome-js/tweener/*.js
%{_libdir}/pkgconfig/gnome-js-common.pc

%files doc
%defattr(-,root,root,-)
%{_docdir}/gnome_js_common/*

%clean
rm -rf %{buildroot}

%changelog
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
