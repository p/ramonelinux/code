Name:       gtksourceview
Version:    3.18.0
Release:    1%{?dist}
Summary:    GtkSourceView

Group:      User Interface/GNOME
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/%{name}/3.18/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  gtk+ intltool
BuildRequires:  gobject-introspection
BuildRequires:  gtk-doc libxml gettext xml-parser

%description
The GtkSourceView package contains libraries used for extending the GTK+ text functions to include syntax highlighting.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_includedir}/gtksourceview-3.0/gtksourceview/*
%{_libdir}/girepository-1.0/GtkSource-3.0.typelib
%{_libdir}/libgtksourceview-3.0.*
%{_libdir}/pkgconfig/gtksourceview-3.0.pc
%{_datadir}/gir-1.0/GtkSource-3.0.gir
%{_datadir}/gtksourceview-3.0/language-specs/*
%{_datadir}/gtksourceview-3.0/styles/*
%{_datadir}/locale/*/LC_MESSAGES/gtksourceview-3.0.mo

%files doc
%defattr(-,root,root,-)
%{_datadir}/gtk-doc/html/gtksourceview-3.0/*

%clean
rm -rf %{buildroot}

%changelog
* Sat Sep 26 2015 tanggeliang <tanggeliang@gmail.com>
- update from 3.16.0 to 3.18.0
* Sun Mar 29 2015 tanggeliang <tanggeliang@gmail.com>
- update from 3.14.0 to 3.16.0
* Fri Sep 26 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.12.2 to 3.14.0
* Sat Jul 19 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.12.1 to 3.12.2
* Sun Apr 20 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.12.0 to 3.12.1
* Thu Mar 27 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.11.90 to 3.12.0
* Sat Mar 15 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.10.1 to 3.11.90
* Tue Oct 22 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.10.0 to 3.10.1
* Tue Sep 24 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.8.2 to 3.10.0
* Wed Jul 24 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.8.1 to 3.8.2
* Sun Jun 9 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.8.0 to 3.8.1
* Sat Apr 13 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.2 to 3.8.0
* Tue Mar 19 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.1 to 3.6.2
* Mon Nov 26 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.0 to 3.6.1
* Fri Sep 28 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.4.2 to 3.6.0
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
