Name:       gfbgraph
Version:    0.2.2
Release:    1%{?dist}
Summary:    LibGFBGraph

Group:      User Interface/GNOME
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/%{name}/0.2.2/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  json-glib librest gnome-online-accounts gobject-introspection
BuildRequires:  intltool gettext xml-parser

%description
GLib/GObject wrapper for the Facebook Graph API.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} libgfbgraphdocdir=%{_docdir}/libgfbgraph-%{version} install

%files
%defattr(-,root,root,-)
%{_includedir}/gfbgraph-0.2/gfbgraph/gfbgraph-*.h
%{_includedir}/gfbgraph-0.2/gfbgraph/gfbgraph.h
%{_libdir}/girepository-1.0/GFBGraph-0.2.typelib
%{_libdir}/libgfbgraph-0.2.*a
%{_libdir}/libgfbgraph-0.2.so*
%{_libdir}/pkgconfig/libgfbgraph-0.2.pc
%{_datadir}/gir-1.0/GFBGraph-0.2.gir

%files doc
%defattr(-,root,root,-)
%{_docdir}/libgfbgraph-%{version}/*
%{_datadir}/gtk-doc/html/gfbgraph/*

%clean
rm -rf %{buildroot}

%changelog
* Thu Mar 20 2014 tanggeliang <tanggeliang@gmail.com>
- create
