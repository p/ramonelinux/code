Name:       eog
Version:    3.17.92
Release:    1%{?dist}
Summary:    EOG

Group:      User Interface/GNOME
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/%{name}/3.17/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  gnome-desktop gnome-icon-theme libpeas shared-mime-info yelp-xsl
BuildRequires:  gobject-introspection librsvg
BuildRequires:  exempi gtk-doc lcms2 libexif libjpeg-turbo libpeas
BuildRequires:  intltool gettext xml-parser itstool

%description
EOG is an application used for viewing and cataloging image files on the GNOME Desktop.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/eog
%{_includedir}/eog-3.0/eog/eog-*.h
%{_libdir}/eog/girepository-1.0/Eog-3.0.typelib
%{_libdir}/eog/plugins/*
%{_libdir}/eog/libeog.*
%{_libdir}/pkgconfig/eog.pc
%{_datadir}/GConf/gsettings/eog.convert
%{_datadir}/appdata/eog.appdata.xml
%{_datadir}/applications/eog.desktop
%{_datadir}/eog/gir-1.0/Eog-3.0.gir
%{_datadir}/eog/icons/hicolor/*/actions/*.png
%{_datadir}/eog/icons/hicolor/scalable/actions/*.svg
%{_datadir}/glib-2.0/schemas/org.gnome.eog.*.xml
%{_datadir}/icons/hicolor/*/apps/eog.png
%{_datadir}/icons/hicolor/scalable/apps/eog-symbolic.svg
%{_datadir}/locale/*

%files doc
%defattr(-,root,root,-)
%{_datadir}/gtk-doc/html/eog/*
%{_datadir}/help/*/eog/*

%post
gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
glib-compile-schemas %{_datadir}/glib-2.0/schemas &>/dev/null || :

%clean
rm -rf %{buildroot}

%changelog
* Sat Sep 26 2015 tanggeliang <tanggeliang@gmail.com>
- update from 3.17.90 to 3.17.92
* Sun Sep 28 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.12.2 to 3.14.0
* Sat Jul 19 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.12.0 to 3.12.2
* Sat Mar 29 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.11.91 to 3.12.0
* Sun Mar 16 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.10.2 to 3.11.91
* Wed Dec 4 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.10.1 to 3.10.2
* Mon Oct 21 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.10.0 to 3.10.1
* Wed Sep 25 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.8.2 to 3.10.0
* Sun Jun 9 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.8.0 to 3.8.2
* Fri May 10 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.2 to 3.8.0
* Tue Mar 19 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.1 to 3.6.2
* Mon Oct 29 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.0 to 3.6.1
* Sun Sep 30 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.4.3 to 3.6.0
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
