Name:       gssdp
Version:    0.14.11
Release:    1%{?dist}
Summary:    Gssdp

Group:      User Interface/GNOME
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/%{name}/0.14/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  libsoup, gobject-introspection
BuildRequires:  gtk2 gtk+

%description
The Gssdp package provides a GObject based API for handling resource discovery and announcement over SSDP (Simple Service Discovery Protocol).

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --disable-static \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/gssdp-device-sniffer
%{_includedir}/gssdp-1.0/libgssdp/gssdp*.h
%{_libdir}/girepository-1.0/GSSDP-1.0.typelib
%{_libdir}/libgssdp-1.0.*
%{_libdir}/pkgconfig/gssdp-1.0.pc
%{_datadir}/gir-1.0/GSSDP-1.0.gir
%{_datadir}/gssdp/gssdp-device-sniffer.ui
%{_datadir}/gtk-doc/html/gssdp/*

%post

%clean
rm -rf %{buildroot}

%changelog
* Wed Sep 23 2015 tanggeliang <tanggeliang@gmail.com>
- update from 0.14.10 to 0.14.11
* Wed Sep 24 2014 tanggeliang <tanggeliang@gmail.com>
- update from 0.12.1 to 0.14.10
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
