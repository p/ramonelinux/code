Name:       gnome-devel-docs
Version:    3.8.1
Release:    1%{?dist}
Summary:    gnome-devel-docs

Group:      User Interface/GNOME
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/%{name}/3.8/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  itstool gettext libxml

%description
This package contains documents which will be packaged together and shipped as gnome-devel-docs in the GNOME Fifth Toe distribution.
They should be documents targeted for GNOME developers.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_datadir}/help/*

%clean
rm -rf %{buildroot}

%changelog
* Mon Jul 22 2013 tanggeliang <tanggeliang@gmail.com>
- create
