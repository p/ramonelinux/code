Name:       gnome-tweak-tool
Version:    3.8.1
Release:    1%{?dist}
Summary:    GNOME Tweak Tool

Group:      User Interface/GNOME
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/%{name}/3.8/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  gconf gsettings-desktop-schemas pygobject
BuildRequires:  intltool gettext xml-parser
Requires:       pygobject

%description
GNOME Tweak Tool is a simple program used to tweak advanced GNOME settings.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/gnome-tweak-tool
%{_libdir}/python2.7/site-packages/gtweak/*.py*
%{_libdir}/python2.7/site-packages/gtweak/tweaks/*.py*
%{_datadir}/applications/gnome-tweak-tool.desktop
%{_datadir}/gnome-tweak-tool/shell.ui
%{_datadir}/icons/hicolor/*/apps/gnome-tweak-tool.png
%{_datadir}/locale/*/LC_MESSAGES/gnome-tweak-tool.mo

%post
gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :

%clean
rm -rf %{buildroot}

%changelog
* Mon Aug 19 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.1 to 3.8.1
* Fri Nov 2 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.5.5 to 3.6.1
* Mon Oct 1 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.4.0 to 3.5.5
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
