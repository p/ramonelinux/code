Name:       gnome-calculator
Version:    3.14.0
Release:    1%{?dist}
Summary:    GNOME Calculator

Group:      User Interface/GNOME
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/%{name}/3.14/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  gtk+ yelp-xsl
BuildRequires:  vala gtksourceview
BuildRequires:  intltool gettext xml-parser itstool

%description
GNOME Calculator is a powerful graphical calculator with financial, logical and scientific modes.
It uses a multiple precision package to do its arithmetic to give a high degree of accuracy.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --libexecdir=%{_libdir}/%{name} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/gcalccmd
%{_bindir}/gnome-calculator
%{_libdir}/%{name}/gnome-calculator-search-provider
%{_datadir}/appdata/gnome-calculator.appdata.xml
%{_datadir}/applications/gnome-calculator.desktop
%{_datadir}/dbus-1/services/org.gnome.Calculator.SearchProvider.service
%{_datadir}/gnome-shell/search-providers/gnome-calculator-search-provider.ini
%{_datadir}/glib-2.0/schemas/org.gnome.calculator.gschema.xml
%{_datadir}/locale/*/LC_MESSAGES/gnome-calculator.mo

%files doc
%defattr(-,root,root,-)
%{_datadir}/help/*/gnome-calculator/*
%{_mandir}/man1/gcalccmd.1.gz
%{_mandir}/man1/gnome-calculator.1.gz

%post
glib-compile-schemas %{_datadir}/glib-2.0/schemas &>/dev/null || :

%clean
rm -rf %{buildroot}

%changelog
* Sun Sep 28 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.12.3 to 3.14.0
* Sat Jul 19 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.12.1 to 3.12.3
* Sun Apr 20 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.12.0 to 3.12.1
* Sat Mar 29 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.11.91 to 3.12.0
* Mon Mar 17 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.10.2 to 3.11.91
* Wed Dec 4 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.10.1 to 3.10.2
* Wed Oct 23 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.10.0 to 3.10.1
* Thu Sep 26 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.8.2 to 3.10.0
* Wed Aug 7 2013 tanggeliang <tanggeliang@gmail.com>
- update from gcalctool 6.6.2 to gnome-calculator 3.8.2
