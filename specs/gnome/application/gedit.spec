Name:       gedit
Version:    3.18.0
Release:    1%{?dist}
Summary:    Gedit

Group:      User Interface/GNOME
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/%{name}/3.18/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  gsettings-desktop-schemas gtksourceview libpeas yelp-xsl
BuildRequires:  enchant iso-codes libsoup libzeitgeist pygobject
BuildRequires:  gtk-doc intltool gettext xml-parser itstool
BuildRequires:  python3
AutoReq:        no

%description
The Gedit package contains a lightweight UTF-8 text editor for the GNOME Desktop.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}
sed -i 's:/bin/env:/usr/bin/env:'   plugins/externaltools/data/send-to-fpaste.tool.in

%build
./configure --prefix=/usr --libexecdir=%{_libdir} \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/gedit
%{_bindir}/gnome-text-editor
%{_includedir}/gedit-*/gedit/gedit-*.h
%{_libdir}/gedit/gedit-bugreport.sh
%{_libdir}/gedit/girepository-1.0/Gedit-3.0.typelib
%{_libdir}/gedit/libgedit.*
%{_libdir}/gedit/plugins/*
%{_libdir}/pkgconfig/gedit.pc
%{_datadir}/GConf/gsettings/gedit.convert
%{_datadir}/appdata/org.gnome.gedit.appdata.xml
%{_datadir}/applications/org.gnome.gedit.desktop
%{_datadir}/dbus-1/services/org.gnome.gedit.service
%{_datadir}/gedit/gir-1.0/Gedit-3.0.gir
%{_datadir}/gedit/logo/gedit-logo.png
%{_datadir}/gedit/plugins/*
%{_datadir}/glib-2.0/schemas/org.gnome.gedit.*.xml
%{_datadir}/locale/*/LC_MESSAGES/gedit.mo
/usr/lib/python3.3/site-packages/gi/overrides/*

%files doc
%defattr(-,root,root,-)
%{_datadir}/gtk-doc/html/gedit/*
%{_datadir}/help/*/gedit/*
%{_mandir}/man1/gedit.1.gz

%post
glib-compile-schemas %{_datadir}/glib-2.0/schemas &>/dev/null || :

%clean
rm -rf %{buildroot}

%changelog
* Sat Sep 26 2015 tanggeliang <tanggeliang@gmail.com>
- update from 3.14.0 to 3.18.0
* Sun Sep 28 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.12.2 to 3.14.0
* Sat Jul 19 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.12.0 to 3.12.2
* Sat Mar 29 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.11.91 to 3.12.0
* Mon Mar 17 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.10.2 to 3.11.91
* Wed Dec 4 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.10.1 to 3.10.2
* Mon Oct 21 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.10.0 to 3.10.1
* Thu Sep 26 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.8.3 to 3.10.0
* Fri Jul 26 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.8.2 to 3.8.3
* Sun Jun 9 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.8.1 to 3.8.2
* Fri May 10 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.2 to 3.8.1
* Tue Mar 19 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.1 to 3.6.2
* Fri Nov 2 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.0 to 3.6.1
* Mon Oct 1 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.4.2 to 3.6.0
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
