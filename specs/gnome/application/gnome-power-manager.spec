Name:       gnome-power-manager
Version:    3.12.0
Release:    1%{?dist}
Summary:    GNOME Power Manager

Group:      User Interface/GNOME
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/%{name}/3.12/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  gtk+ upower
BuildRequires:  intltool gettext xml-parser

%description
The GNOME Power Manager package contains a tool used to report on power management on the system.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/gnome-power-statistics
%{_datadir}/appdata/gnome-power-statistics.appdata.xml
%{_datadir}/applications/gnome-power-statistics.desktop
%{_datadir}/glib-2.0/schemas/org.gnome.power-manager.gschema.xml
%{_datadir}/icons/HighContrast/*x*/apps/gnome-power-statistics.png
%{_datadir}/icons/hicolor/*/apps/gnome-power-statistics.*
%{_datadir}/locale/*/LC_MESSAGES/gnome-power-manager.mo

%post
gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
glib-compile-schemas %{_datadir}/glib-2.0/schemas &>/dev/null || :

%postun

%clean
rm -rf %{buildroot}

%changelog
* Sat Mar 29 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.11.90 to 3.12.0
* Sat Mar 15 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.10.1 to 3.11.90
* Wed Oct 23 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.10.0 to 3.10.1
* Thu Sep 26 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.8.2 to 3.10.0
* Sat Aug 17 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.8.1 to 3.8.2
* Sat May 11 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.8.0 to 3.8.1
* Sat Apr 13 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.0 to 3.8.0
* Fri Sep 28 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.4.0 to 3.6.0
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
