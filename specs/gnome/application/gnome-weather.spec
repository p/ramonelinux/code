Name:       gnome-weather
Version:    3.16.0
Release:    1%{?dist}
Summary:    GNOME Weather

Group:      User Interface/GNOME
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/%{name}/3.16/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  gjs libgweather
BuildRequires:  intltool gettext xml-parser

%description
A small application that allows you to monitor the current weather conditions for your city, or anywhere in the world, and to access updated forecasts provided by various internet services.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/gnome-weather
%{_datadir}/appdata/org.gnome.Weather.Application.appdata.xml
%{_datadir}/applications/org.gnome.Weather.Application.desktop
%{_datadir}/dbus-1/services/org.gnome.Weather.*.service
%{_datadir}/glib-2.0/schemas/org.gnome.Weather.Application.gschema.xml
%{_datadir}/gnome-shell/search-providers/org.gnome.Weather.Application.search-provider.ini
%{_datadir}/icons/HighContrast/*/apps/org.gnome.Weather.Application.png
%{_datadir}/icons/hicolor/*/apps/org.gnome.Weather.Application.png
%{_datadir}/locale/*/LC_MESSAGES/org.gnome.Weather.mo
%{_datadir}/org.gnome.Weather/org.gnome.Weather.*

%post
gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
glib-compile-schemas %{_datadir}/glib-2.0/schemas &>/dev/null || :

%clean
rm -rf %{buildroot}

%changelog
* Sun Mar 29 2015 tanggeliang <tanggeliang@gmail.com>
- create
