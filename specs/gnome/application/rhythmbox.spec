Name:       rhythmbox
Version:    3.0.1
Release:    2%{?dist}
Summary:    Music Management Application

Group:      Applications/Multimedia
License:    GPLv2+ with exceptions and GFDL
URL:        http://www.gnome.org
Source:     http://download.gnome.org/sources/rhythmbox/3.0/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  brasero clutter-gst clutter-gtk
BuildRequires:  gettext gobject-introspection intltool itstool
BuildRequires:  gstreamer gst-plugins-base
BuildRequires:  gtk+ json-glib systemd
BuildRequires:  libnotify libpeas libsecret libsm libsoup
BuildRequires:  pygobject totem-pl-parser webkitgtk
BuildRequires:  xml-parser libtdb python3
#BuildRequires:  grilo libdmapsharing libgpod libmtp libmx lirc
Requires:       gnome-icon-theme desktop-file-utils
Requires:       gvfs pygobject
#Requires:       media-player-info python-mako

%description
Rhythmbox is an integrated music management application based on the powerful GStreamer media framework.
It has a number of features, including an easy to use music browser, searching and sorting, comprehensive audio format support through GStreamer, Internet Radio support, playlists and more.
Rhythmbox is extensible through a plugin system.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --libdir=%{_libdir} \
            #--with-ipod \
            --without-hal &&
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

%post
/sbin/ldconfig
update-desktop-database >&/dev/null || :
touch --no-create %{_datadir}/icons/hicolor >&/dev/null || :

%postun
/sbin/ldconfig
update-desktop-database >&/dev/null || :
if [ $1 -eq 0 ]; then
  gtk-update-icon-cache %{_datadir}/icons/hicolor >&/dev/null || :
  glib-compile-schemas %{_datadir}/glib-2.0/schemas >&/dev/null || :
fi

%posttrans
gtk-update-icon-cache %{_datadir}/icons/hicolor >&/dev/null || :
glib-compile-schemas %{_datadir}/glib-2.0/schemas >&/dev/null || :

%files
%{_bindir}/*
%{_datadir}/rhythmbox/
%{_datadir}/applications/rhythmbox.desktop
%{_datadir}/applications/rhythmbox-device.desktop
%{_datadir}/dbus-1/services/org.gnome.Rhythmbox3.service
%{_datadir}/glib-2.0/schemas/org.gnome.rhythmbox.gschema.xml
%{_datadir}/icons/hicolor/*/apps/*.png
%{_datadir}/icons/hicolor/*/apps/*.svg
%{_datadir}/icons/hicolor/*/status/*.png
%{_libdir}/librhythmbox-core.la
%{_libdir}/librhythmbox-core.so*
%{_libdir}/mozilla/plugins/librhythmbox-itms-detection-plugin.la
%{_libdir}/mozilla/plugins/*.so
%dir %{_libdir}/rhythmbox
%dir %{_libdir}/rhythmbox/plugins
%{_libdir}/girepository-1.0/*.typelib
%{_libdir}/rhythmbox/plugins/artsearch/
%{_libdir}/rhythmbox/plugins/audiocd/
%{_libdir}/rhythmbox/plugins/audioscrobbler/
%{_libdir}/rhythmbox/plugins/cd-recorder/
%{_libdir}/rhythmbox/plugins/context/
%{_libdir}/rhythmbox/plugins/dbus-media-server/
%{_libdir}/rhythmbox/plugins/fmradio/
%{_libdir}/rhythmbox/plugins/generic-player/
%{_libdir}/rhythmbox/plugins/im-status/
%{_libdir}/rhythmbox/plugins/iradio/
%{_libdir}/rhythmbox/plugins/lyrics/
%{_libdir}/rhythmbox/plugins/magnatune/
%{_libdir}/rhythmbox/plugins/mmkeys/
%{_libdir}/rhythmbox/plugins/mpris/
%{_libdir}/rhythmbox/plugins/notification/
%{_libdir}/rhythmbox/plugins/power-manager/
%{_libdir}/rhythmbox/plugins/python-console/
%{_libdir}/rhythmbox/plugins/rb/
%{_libdir}/rhythmbox/plugins/rbzeitgeist/
%{_libdir}/rhythmbox/plugins/replaygain/
%{_libdir}/rhythmbox/plugins/sendto/
%{_libdir}/rhythmbox/sample-plugins/
%{_libexecdir}/rhythmbox-metadata
%{_includedir}/rhythmbox
%{_libdir}/pkgconfig/rhythmbox.pc
%{_datadir}/gir-1.0/*.gir

%files doc
%defattr(-,root,root,-)
%{_datadir}/gtk-doc/html/rhythmbox/*.html
%{_datadir}/gtk-doc/html/rhythmbox/*.png
%{_datadir}/gtk-doc/html/rhythmbox/style.css
%{_datadir}/gtk-doc/html/rhythmbox/index.sgml
%{_datadir}/gtk-doc/html/rhythmbox/rhythmbox.devhelp2
%{_datadir}/help/*/rhythmbox/fdl-appendix.xml
%{_datadir}/help/*/rhythmbox/figures/rb-*.png
%{_datadir}/help/*/rhythmbox/index.docbook
%{_datadir}/help/*/rhythmbox/legal.xml
%{_datadir}/locale/*/LC_MESSAGES/rhythmbox.mo
%{_mandir}/man1/rhythmbox*.1.gz

%changelog
* Mon Oct 21 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.0 to 3.0.1
* Thu Sep 26 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.99.1 to 3.0
* Wed Jun 19 2013 tanggeliang <tanggeliang@gmail.com>
- create
