Name:       gegl
Version:    0.2.0
Release:    1%{?dist}
Summary:    GEGL

Group:      User Interface/GNOME
License:    GPLv2+
Url:        http://www.gegl.org
Source:     ftp://ftp.gimp.org/pub/gegl/0.2/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  babl glib imagemagick gobject-introspection
BuildRequires:  intltool gettext xml-parser

%description
GEGL (Generic Graphics Library) is a graph based image processing framework.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/gegl
%{_includedir}/gegl-0.2/gegl*.h
%{_includedir}/gegl-0.2/opencl/*.h
%{_includedir}/gegl-0.2/operation/gegl-operation*.h
%{_libdir}/gegl-0.2/add.la
%{_libdir}/gegl-0.2/add.so
%{_libdir}/gegl-0.2/bilateral-filter.la
%{_libdir}/gegl-0.2/bilateral-filter.so
%{_libdir}/gegl-0.2/box-blur.la
%{_libdir}/gegl-0.2/box-blur.so
%{_libdir}/gegl-0.2/brightness-contrast.la
%{_libdir}/gegl-0.2/brightness-contrast.so
%{_libdir}/gegl-0.2/buffer-sink.la
%{_libdir}/gegl-0.2/buffer-sink.so
%{_libdir}/gegl-0.2/buffer-source.la
%{_libdir}/gegl-0.2/buffer-source.so
%{_libdir}/gegl-0.2/c2g.la
%{_libdir}/gegl-0.2/c2g.so
%{_libdir}/gegl-0.2/checkerboard.la
%{_libdir}/gegl-0.2/checkerboard.so
%{_libdir}/gegl-0.2/clear.la
%{_libdir}/gegl-0.2/clear.so
%{_libdir}/gegl-0.2/clone.la
%{_libdir}/gegl-0.2/clone.so
%{_libdir}/gegl-0.2/color-burn.la
%{_libdir}/gegl-0.2/color-burn.so
%{_libdir}/gegl-0.2/color-dodge.la
%{_libdir}/gegl-0.2/color-dodge.so
%{_libdir}/gegl-0.2/color-temperature.la
%{_libdir}/gegl-0.2/color-temperature.so
%{_libdir}/gegl-0.2/color-to-alpha.la
%{_libdir}/gegl-0.2/color-to-alpha.so
%{_libdir}/gegl-0.2/color.la
%{_libdir}/gegl-0.2/color.so
%{_libdir}/gegl-0.2/contrast-curve.la
%{_libdir}/gegl-0.2/contrast-curve.so
%{_libdir}/gegl-0.2/convert-format.la
%{_libdir}/gegl-0.2/convert-format.so
%{_libdir}/gegl-0.2/crop.la
%{_libdir}/gegl-0.2/crop.so
%{_libdir}/gegl-0.2/darken.la
%{_libdir}/gegl-0.2/darken.so
%{_libdir}/gegl-0.2/difference-of-gaussians.la
%{_libdir}/gegl-0.2/difference-of-gaussians.so
%{_libdir}/gegl-0.2/difference.la
%{_libdir}/gegl-0.2/difference.so
%{_libdir}/gegl-0.2/display.la
%{_libdir}/gegl-0.2/display.so
%{_libdir}/gegl-0.2/divide.la
%{_libdir}/gegl-0.2/divide.so
%{_libdir}/gegl-0.2/dropshadow.la
%{_libdir}/gegl-0.2/dropshadow.so
%{_libdir}/gegl-0.2/dst-atop.la
%{_libdir}/gegl-0.2/dst-atop.so
%{_libdir}/gegl-0.2/dst-in.la
%{_libdir}/gegl-0.2/dst-in.so
%{_libdir}/gegl-0.2/dst-out.la
%{_libdir}/gegl-0.2/dst-out.so
%{_libdir}/gegl-0.2/dst-over.la
%{_libdir}/gegl-0.2/dst-over.so
%{_libdir}/gegl-0.2/dst.la
%{_libdir}/gegl-0.2/dst.so
%{_libdir}/gegl-0.2/edge-laplace.la
%{_libdir}/gegl-0.2/edge-laplace.so
%{_libdir}/gegl-0.2/edge-sobel.la
%{_libdir}/gegl-0.2/edge-sobel.so
%{_libdir}/gegl-0.2/exclusion.la
%{_libdir}/gegl-0.2/exclusion.so
%{_libdir}/gegl-0.2/exp-combine.la
%{_libdir}/gegl-0.2/exp-combine.so
%{_libdir}/gegl-0.2/fattal02.la
%{_libdir}/gegl-0.2/fattal02.so
%{_libdir}/gegl-0.2/fractal-explorer.la
%{_libdir}/gegl-0.2/fractal-explorer.so
%{_libdir}/gegl-0.2/gamma.la
%{_libdir}/gegl-0.2/gamma.so
%{_libdir}/gegl-0.2/gaussian-blur.la
%{_libdir}/gegl-0.2/gaussian-blur.so
%{_libdir}/gegl-0.2/gegl-buffer-load-op.la
%{_libdir}/gegl-0.2/gegl-buffer-load-op.so
%{_libdir}/gegl-0.2/gegl-buffer-save-op.la
%{_libdir}/gegl-0.2/gegl-buffer-save-op.so
%{_libdir}/gegl-0.2/grey.la
%{_libdir}/gegl-0.2/grey.so
%{_libdir}/gegl-0.2/grid.la
%{_libdir}/gegl-0.2/grid.so
%{_libdir}/gegl-0.2/hard-light.la
%{_libdir}/gegl-0.2/hard-light.so
%{_libdir}/gegl-0.2/introspect.la
%{_libdir}/gegl-0.2/introspect.so
%{_libdir}/gegl-0.2/invert.la
%{_libdir}/gegl-0.2/invert.so
%{_libdir}/gegl-0.2/jpg-load.la
%{_libdir}/gegl-0.2/jpg-load.so
%{_libdir}/gegl-0.2/jpg-save.la
%{_libdir}/gegl-0.2/jpg-save.so
%{_libdir}/gegl-0.2/layer.la
%{_libdir}/gegl-0.2/layer.so
%{_libdir}/gegl-0.2/lens-distortion.la
%{_libdir}/gegl-0.2/lens-distortion.so
%{_libdir}/gegl-0.2/levels.la
%{_libdir}/gegl-0.2/levels.so
%{_libdir}/gegl-0.2/lighten.la
%{_libdir}/gegl-0.2/lighten.so
%{_libdir}/gegl-0.2/load.la
%{_libdir}/gegl-0.2/load.so
%{_libdir}/gegl-0.2/magick-load.la
%{_libdir}/gegl-0.2/magick-load.so
%{_libdir}/gegl-0.2/mantiuk06.la
%{_libdir}/gegl-0.2/mantiuk06.so
%{_libdir}/gegl-0.2/map-absolute.la
%{_libdir}/gegl-0.2/map-absolute.so
%{_libdir}/gegl-0.2/map-relative.la
%{_libdir}/gegl-0.2/map-relative.so
%{_libdir}/gegl-0.2/matting-global.la
%{_libdir}/gegl-0.2/matting-global.so
%{_libdir}/gegl-0.2/mblur.la
%{_libdir}/gegl-0.2/mblur.so
%{_libdir}/gegl-0.2/mirrors.la
%{_libdir}/gegl-0.2/mirrors.so
%{_libdir}/gegl-0.2/mono-mixer.la
%{_libdir}/gegl-0.2/mono-mixer.so
%{_libdir}/gegl-0.2/motion-blur.la
%{_libdir}/gegl-0.2/motion-blur.so
%{_libdir}/gegl-0.2/multiply.la
%{_libdir}/gegl-0.2/multiply.so
%{_libdir}/gegl-0.2/noise-reduction.la
%{_libdir}/gegl-0.2/noise-reduction.so
%{_libdir}/gegl-0.2/noise.la
%{_libdir}/gegl-0.2/noise.so
%{_libdir}/gegl-0.2/nop.la
%{_libdir}/gegl-0.2/nop.so
%{_libdir}/gegl-0.2/opacity.la
%{_libdir}/gegl-0.2/opacity.so
%{_libdir}/gegl-0.2/open-buffer.la
%{_libdir}/gegl-0.2/open-buffer.so
%{_libdir}/gegl-0.2/over.la
%{_libdir}/gegl-0.2/over.so
%{_libdir}/gegl-0.2/overlay.la
%{_libdir}/gegl-0.2/overlay.so
%{_libdir}/gegl-0.2/path.la
%{_libdir}/gegl-0.2/path.so
%{_libdir}/gegl-0.2/pixelize.la
%{_libdir}/gegl-0.2/pixelize.so
%{_libdir}/gegl-0.2/plus.la
%{_libdir}/gegl-0.2/plus.so
%{_libdir}/gegl-0.2/png-load.la
%{_libdir}/gegl-0.2/png-load.so
%{_libdir}/gegl-0.2/png-save.la
%{_libdir}/gegl-0.2/png-save.so
%{_libdir}/gegl-0.2/polar-coordinates.la
%{_libdir}/gegl-0.2/polar-coordinates.so
%{_libdir}/gegl-0.2/posterize.la
%{_libdir}/gegl-0.2/posterize.so
%{_libdir}/gegl-0.2/ppm-load.la
%{_libdir}/gegl-0.2/ppm-load.so
%{_libdir}/gegl-0.2/ppm-save.la
%{_libdir}/gegl-0.2/ppm-save.so
%{_libdir}/gegl-0.2/raw-load.la
%{_libdir}/gegl-0.2/raw-load.so
%{_libdir}/gegl-0.2/rectangle.la
%{_libdir}/gegl-0.2/rectangle.so
%{_libdir}/gegl-0.2/reinhard05.la
%{_libdir}/gegl-0.2/reinhard05.so
%{_libdir}/gegl-0.2/remap.la
%{_libdir}/gegl-0.2/remap.so
%{_libdir}/gegl-0.2/rgbe-load.la
%{_libdir}/gegl-0.2/rgbe-load.so
%{_libdir}/gegl-0.2/rgbe-save.la
%{_libdir}/gegl-0.2/rgbe-save.so
%{_libdir}/gegl-0.2/ripple.la
%{_libdir}/gegl-0.2/ripple.so
%{_libdir}/gegl-0.2/save.la
%{_libdir}/gegl-0.2/save.so
%{_libdir}/gegl-0.2/screen.la
%{_libdir}/gegl-0.2/screen.so
%{_libdir}/gegl-0.2/snn-mean.la
%{_libdir}/gegl-0.2/snn-mean.so
%{_libdir}/gegl-0.2/soft-light.la
%{_libdir}/gegl-0.2/soft-light.so
%{_libdir}/gegl-0.2/src-atop.la
%{_libdir}/gegl-0.2/src-atop.so
%{_libdir}/gegl-0.2/src-in.la
%{_libdir}/gegl-0.2/src-in.so
%{_libdir}/gegl-0.2/src-out.la
%{_libdir}/gegl-0.2/src-out.so
%{_libdir}/gegl-0.2/src-over.la
%{_libdir}/gegl-0.2/src-over.so
%{_libdir}/gegl-0.2/src.la
%{_libdir}/gegl-0.2/src.so
%{_libdir}/gegl-0.2/stress.la
%{_libdir}/gegl-0.2/stress.so
%{_libdir}/gegl-0.2/stretch-contrast.la
%{_libdir}/gegl-0.2/stretch-contrast.so
%{_libdir}/gegl-0.2/subtract.la
%{_libdir}/gegl-0.2/subtract.so
%{_libdir}/gegl-0.2/svg-huerotate.la
%{_libdir}/gegl-0.2/svg-huerotate.so
%{_libdir}/gegl-0.2/svg-luminancetoalpha.la
%{_libdir}/gegl-0.2/svg-luminancetoalpha.so
%{_libdir}/gegl-0.2/svg-matrix.la
%{_libdir}/gegl-0.2/svg-matrix.so
%{_libdir}/gegl-0.2/svg-multiply.la
%{_libdir}/gegl-0.2/svg-multiply.so
%{_libdir}/gegl-0.2/svg-saturate.la
%{_libdir}/gegl-0.2/svg-saturate.so
%{_libdir}/gegl-0.2/text.la
%{_libdir}/gegl-0.2/text.so
%{_libdir}/gegl-0.2/threshold.la
%{_libdir}/gegl-0.2/threshold.so
%{_libdir}/gegl-0.2/transformops.la
%{_libdir}/gegl-0.2/transformops.so
%{_libdir}/gegl-0.2/unsharp-mask.la
%{_libdir}/gegl-0.2/unsharp-mask.so
%{_libdir}/gegl-0.2/value-invert.la
%{_libdir}/gegl-0.2/value-invert.so
%{_libdir}/gegl-0.2/vector-fill.la
%{_libdir}/gegl-0.2/vector-fill.so
%{_libdir}/gegl-0.2/vector-stroke.la
%{_libdir}/gegl-0.2/vector-stroke.so
%{_libdir}/gegl-0.2/vignette.la
%{_libdir}/gegl-0.2/vignette.so
%{_libdir}/gegl-0.2/waves.la
%{_libdir}/gegl-0.2/waves.so
%{_libdir}/gegl-0.2/weighted-blend.la
%{_libdir}/gegl-0.2/weighted-blend.so
%{_libdir}/gegl-0.2/write-buffer.la
%{_libdir}/gegl-0.2/write-buffer.so
%{_libdir}/gegl-0.2/xor.la
%{_libdir}/gegl-0.2/xor.so
%{_libdir}/libgegl-0.2.la
%{_libdir}/libgegl-0.2.so*
%{_libdir}/pkgconfig/gegl-0.2.pc
%{_datadir}/gtk-doc/html/gegl/gegl.css
%{_datadir}/gtk-doc/html/gegl/operations.html
%{_datadir}/locale/*/LC_MESSAGES/gegl-0.2.mo

%clean
rm -rf %{buildroot}

%changelog
* Sat Sep 28 2013 tanggeliang <tanggeliang@gmail.com>
- create
