Name:       mobile-broadband-provider-info
Version:    20120614
Release:    6%{?dist}
Summary:    Mobile Broadband Provider Info

Group:      User Interface/GNOME
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/%{name}/20120614/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildArch:      noarch

%description
The Mobile Broadband Provider Info package contains listings of mobile broadband (3G) providers and associated network and plan information.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_datadir}/mobile-broadband-provider-info/serviceproviders.2.dtd
%{_datadir}/mobile-broadband-provider-info/serviceproviders.xml
%{_datadir}/pkgconfig/mobile-broadband-provider-info.pc

%clean
rm -rf %{buildroot}

%changelog
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
