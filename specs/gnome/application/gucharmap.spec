Name:       gucharmap
Version:    3.12.0
Release:    1%{?dist}
Summary:    Gucharmap

Group:      User Interface/GNOME
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/%{name}/3.12/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  gtk+ yelp-xsl
BuildRequires:  gobject-introspection vala
BuildRequires:  gtk-doc intltool gettext xml-parser
BuildRequires:  libxml libxslt docbook-xml itstool desktop-file-utils

%description
Gucharmap is a Unicode character map and font viewer.
It allows you to browse through all the available Unicode characters and categories for the installed fonts, and to examine their detailed properties.
It is an easy way to find the character you might only know by its Unicode name or code point.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --enable-vala \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/charmap
%{_bindir}/gnome-character-map
%{_bindir}/gucharmap
%{_includedir}/gucharmap-2.90/gucharmap/gucharmap-*.h
%{_includedir}/gucharmap-2.90/gucharmap/gucharmap.h
%{_libdir}/girepository-1.0/Gucharmap-2.90.typelib
%{_libdir}/libgucharmap_2_90.la
%{_libdir}/libgucharmap_2_90.so*
%{_libdir}/pkgconfig/gucharmap-2.90.pc
%{_datadir}/applications/gucharmap.desktop
%{_datadir}/gir-1.0/Gucharmap-2.90.gir
%{_datadir}/glib-2.0/schemas/org.gnome.Charmap.*.xml
%{_datadir}/locale/*/LC_MESSAGES/gucharmap.mo
%{_datadir}/vala/vapi/gucharmap-2.90.vapi

%files doc
%defattr(-,root,root,-)
%{_datadir}/help/*/gucharmap/*

%post
glib-compile-schemas %{_datadir}/glib-2.0/schemas &>/dev/null || :

%clean
rm -rf %{buildroot}

%changelog
* Sat Mar 29 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.10.1 to 3.12.0
* Thu Dec 5 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.9.99 to 3.10.1
* Wed Oct 9 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.8.2 to 3.9.99
* Wed Aug 7 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.5.99 to 3.8.2
* Mon Oct 1 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.4.1 to 3.5.99
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
