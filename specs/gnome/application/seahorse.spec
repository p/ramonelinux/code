Name:       seahorse
Version:    3.12.0
Release:    1%{?dist}
Summary:    Seahorse

Group:      User Interface/GNOME
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/%{name}/3.12/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  gcr gpgme gnupg libsecret yelp-xsl
BuildRequires:  libsoup openssh avahi openldap libxslt docbook-xml docbook-xsl
BuildRequires:  intltool gettext xml-parser itstool

%description
Seahorse is a graphical interface for managing and using encryption keys.
Currently it supports PGP keys (using GPG/GPGME) and SSH keys.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --disable-static \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/seahorse
%{_libdir}/seahorse/seahorse-ssh-askpass
%{_libdir}/seahorse/xloadimage
%{_datadir}/GConf/gsettings/org.gnome.seahorse*.convert
%{_datadir}/appdata/seahorse.appdata.xml
%{_datadir}/applications/seahorse.desktop
%{_datadir}/dbus-1/services/org.gnome.seahorse.Application.service
%{_datadir}/glib-2.0/schemas/org.gnome.seahorse*.gschema.xml
%{_datadir}/gnome-shell/search-providers/seahorse-search-provider.ini
%ifarch %{ix86}
%{_datadir}/icons/hicolor/*/apps/seahorse*.png
%else %ifarch x86_64
%{_datadir}/icons/hicolor/hicolor_apps_*x*_seahorse*.png
%endif
%{_datadir}/locale/*/LC_MESSAGES/seahorse.mo
%{_datadir}/seahorse/icons/hicolor/*
%{_datadir}/seahorse/ui/seahorse-*.xml
%{_datadir}/seahorse/ui/seahorse.css
%{_datadir}/seahorse/ui/seahorse-key*.ui

%files doc
%defattr(-,root,root,-)
%{_datadir}/help/*/seahorse/*
%{_mandir}/man1/seahorse.1.gz

%post
gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
glib-compile-schemas %{_datadir}/glib-2.0/schemas &>/dev/null || :

%clean
rm -rf %{buildroot}

%changelog
* Sat Mar 29 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.11.92 to 3.12.0
* Mon Mar 17 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.10.1 to 3.11.92
* Thu Dec 5 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.10.0 to 3.10.1
* Wed Oct 9 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.8.2 to 3.10.0
* Wed Aug 7 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.2 to 3.8.2
* Fri Nov 2 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.0 to 3.6.2
* Mon Oct 1 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.4.1 to 3.6.0
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
