Name:       vino
Version:    3.8.1
Release:    1%{?dist}
Summary:    Vino

Group:      User Interface/GNOME
License:    GPLv2+
Url:        http://www..gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/%{name}/3.8/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  gtk+ intltool libsoup
BuildRequires:  libnotify libsecret networkmanager telepathy-glib
BuildRequires:  gnutls libgcrypt nettle libsm
BuildRequires:  intltool gettext xml-parser

%description
The Vino package is a VNC server for GNOME.
VNC is a protocol that allows remote display of a user's desktop.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --sysconfdir=/etc \
            --libexecdir=%{_libdir}/vino &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
/etc/xdg/autostart/vino-server.desktop
%{_bindir}/vino-passwd
%{_bindir}/vino-preferences
%{_libdir}/vino/vino-server
%{_datadir}/GConf/gsettings/org.gnome.Vino.convert
%{_datadir}/applications/vino-preferences.desktop
%{_datadir}/dbus-1/services/org.freedesktop.Telepathy.Client.Vino.service
%{_datadir}/glib-2.0/schemas/org.gnome.Vino.*.xml
%{_datadir}/locale/*/LC_MESSAGES/vino.mo
%{_datadir}/telepathy/clients/Vino.client
%{_datadir}/vino/*

%post
glib-compile-schemas %{_datadir}/glib-2.0/schemas &>/dev/null || :

%clean
rm -rf %{buildroot}

%changelog
* Wed Aug 7 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.1 to 3.8.1
* Fri Nov 2 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.0 to 3.6.1
* Mon Oct 1 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.4.2 to 3.6.0
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
