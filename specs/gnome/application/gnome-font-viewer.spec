Name:       gnome-font-viewer
Version:    3.16.2
Release:    1%{?dist}
Summary:    GNOME Font Viewer

Group:      User Interface/GNOME
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/%{name}/3.16/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  gtk+ gnome-desktop
BuildRequires:  intltool gettext xml-parser

%description
The GNOME Font Viewer package contains font viewer for the GNOME Desktop.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/gnome-font-viewer
%{_bindir}/gnome-thumbnail-font
%{_datadir}/appdata/org.gnome.font-viewer.appdata.xml
%{_datadir}/applications/org.gnome.font-viewer.desktop
%{_datadir}/dbus-1/services/org.gnome.font-viewer.service
%{_datadir}/locale/*/LC_MESSAGES/gnome-font-viewer.mo
%{_datadir}/thumbnailers/gnome-font-viewer.thumbnailer

%clean
rm -rf %{buildroot}

%changelog
* Sat Sep 26 2015 tanggeliang <tanggeliang@gmail.com>
- update from 3.14.0 to 3.16.2
* Sun Sep 28 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.12.0 to 3.14.0
* Sun Jul 27 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.10.0 to 3.12.0
* Wed Sep 25 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.8.0 to 3.10.0
* Sat May 11 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.2 to 3.8.0
* Tue Mar 19 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.4.0 to 3.6.2
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
