Name:       evolution
Version:    3.10.3
Release:    1%{?dist}
Summary:    Evolution

Group:      User Interface/GNOME
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/%{name}/3.10/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  evolution-data-server gnome-desktop gnome-icon-theme gtkhtml shared-mime-info
BuildRequires:  gstreamer libcanberra libgweather
BuildRequires:  clutter-gtk geoclue gtk-doc krb
BuildRequires:  intltool gettext xml-parser itstool

%description
The Evolution package contains an integrated mail, calendar and address book suite designed for the GNOME environment.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --sysconfdir=/etc \
            --libexecdir=%{_libdir} \
            --disable-pst-import \
            --disable-bogofilter \
            --disable-spamassassin \
            --disable-text-highlight \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
/etc/xdg/autostart/evolution-alarm-notify.desktop
%{_bindir}/evolution
%{_includedir}/evolution-3.10/*
%{_libdir}/evolution/3.10/*
%{_libdir}/pkgconfig/evolution-calendar-3.0.pc
%{_libdir}/pkgconfig/evolution-mail-3.0.pc
%{_libdir}/pkgconfig/evolution-plugin-3.0.pc
%{_libdir}/pkgconfig/evolution-shell-3.0.pc
%{_libdir}/pkgconfig/libemail-engine.pc
%{_datadir}/GConf/gsettings/evolution.convert
%{_datadir}/appdata/evolution.appdata.xml
%{_datadir}/applications/evolution.desktop
%{_datadir}/evolution/3.10/*
%{_datadir}/glib-2.0/schemas/org.gnome.evolution.*.gschema.xml
%{_datadir}/glib-2.0/schemas/org.gnome.evolution.gschema.xml
%ifarch %{ix86}
%{_datadir}/icons/hicolor/*/apps/evolution*.png
%else %ifarch x86_64
%{_datadir}/icons/hicolor/hicolor_apps_*x*_evolution*.png
%endif
%{_datadir}/locale/*/LC_MESSAGES/evolution-3.10.mo

%files doc
%defattr(-,root,root,-)
%{_datadir}/gtk-doc/html/evolution-*/*
%{_datadir}/help/*/evolution/*

%post
gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
glib-compile-schemas %{_datadir}/glib-2.0/schemas &>/dev/null || :

%clean
rm -rf %{buildroot}

%changelog
* Mon Jan 6 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.10.1 to 3.10.3
* Mon Oct 21 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.10.0 to 3.10.1
* Fri Oct 4 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.8.5 to 3.10.0
* Sat Aug 17 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.8.1 to 3.8.5
* Fri May 10 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.4 to 3.8.1
- add "--disable-bogofilter" "--disable-spamassassin" "--disable-text-highlight"
* Wed Apr 10 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.1 to 3.6.4
* Fri Nov 2 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.0 to 3.6.1
* Sun Sep 30 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.4.3 to 3.6.0
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
