Name:       sushi
Version:    3.8.1
Release:    1%{?dist}
Summary:    Sushi

Group:      User Interface/GNOME
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/%{name}/3.8/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  clutter-gtk clutter-gst evince gjs gtksourceview libmusicbrainz
BuildRequires:  intltool gettext xml-parser webkitgtk

%description
The Sushi package contains a quick file previewer for Nautilus.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --libexecdir=%{_libdir}/sushi &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/sushi
%{_libdir}/sushi/girepository-1.0/Sushi-1.0.typelib
%{_libdir}/sushi/libsushi-1.0.la
%{_libdir}/sushi/libsushi-1.0.so
%{_libdir}/sushi/sushi-start
%{_datadir}/dbus-1/services/org.gnome.Sushi.service
%{_datadir}/glib-2.0/schemas/org.gnome.sushi.gschema.xml
%{_datadir}/locale/*/LC_MESSAGES/sushi.mo
%{_datadir}/sushi/gir-1.0/Sushi-1.0.gir
%{_datadir}/sushi/js/*/*.js
%{_datadir}/sushi/style/gtk-style.css

%post
glib-compile-schemas %{_datadir}/glib-2.0/schemas &>/dev/null || :

%clean
rm -rf %{buildroot}

%changelog
* Wed Aug 7 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.0 to 3.8.1
* Mon Oct 1 2012 tanggeliang <tanggeliang@gmail.com>
- update from 0.4.1 to 3.6.0
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
