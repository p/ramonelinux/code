Name:       gnome-dictionary
Version:    3.9.0
Release:    1%{?dist}
Summary:    GNOME Dictionary

Group:      User Interface/GNOME
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/%{name}/3.9/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  gtk+ yelp-xsl
BuildRequires:  gtk-doc itstool
BuildRequires:  intltool gettext xml-parser libxml libxslt docbook-xml

%description
The GNOME Dictionary package contains dictionary for the GNOME Desktop.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/gnome-dictionary
%{_includedir}/gdict-1.0/gdict/gdict*.h
%{_libdir}/libgdict-1.0.*
%{_libdir}/pkgconfig/gdict-1.0.pc
%{_datadir}/applications/gnome-dictionary.desktop
%{_datadir}/gdict-1.0/sources/default.desktop
%{_datadir}/gdict-1.0/sources/spanish.desktop
%{_datadir}/gdict-1.0/sources/thai.desktop
%{_datadir}/glib-2.0/schemas/org.gnome.dictionary.gschema.xml
%{_datadir}/gnome-dictionary/gnome-dictionary-menus.ui
%{_datadir}/gnome-dictionary/gnome-dictionary-preferences.ui
%{_datadir}/gnome-dictionary/gnome-dictionary-source.ui
%{_datadir}/locale/*

%files doc
%defattr(-,root,root,-)
%{_datadir}/gtk-doc/html/gdict/*
%{_datadir}/help/*/gnome-dictionary/*
%{_mandir}/man1/gnome-dictionary.1.gz

%post
glib-compile-schemas %{_datadir}/glib-2.0/schemas &>/dev/null || :

%clean
rm -rf %{buildroot}

%changelog
* Thu Sep 26 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.8.0 to 3.9.0
* Fri Jul 26 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.0 to 3.8.0
* Mon Oct 1 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.4.0 to 3.6.0
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
