Name:       network-manager-applet
Version:    1.0.6
Release:    1%{?dist}
Summary:    NetworkManager Applet

Group:      User Interface/GNOME
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/%{name}/1.0/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  gtk+ iso-codes libgnome-keyring libnotify
BuildRequires:	networkmanager networkmanager-glib networkmanager-util
BuildRequires:  polkit-gnome
BuildRequires:  gnome-bluetooth mobile-broadband-provider-info
BuildRequires:  intltool gettext xml-parser
BuildRequires:  libsecret gobject-introspection
Requires:       polkit-gnome

%description
The NetworkManager Applet is a tool used to configure wired and wireless network connections through GUI.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --sysconfdir=/etc \
            --libexecdir=%{_libdir}/NetworkManager \
            --disable-migration \
            --disable-static \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
/etc/xdg/autostart/nm-applet.desktop
%{_bindir}/nm-applet
%{_bindir}/nm-connection-editor
%{_includedir}/libnm-gtk/nm-*.h
%{_libdir}/girepository-1.0/NMGtk-1.0.typelib
%{_libdir}/libnm-gtk.*
%{_libdir}/pkgconfig/libnm-gtk.pc
%{_datadir}/GConf/gsettings/nm-applet.convert
%{_datadir}/appdata/org.gnome.nm-connection-editor.appdata.xml
%{_datadir}/applications/nm-applet.desktop
%{_datadir}/applications/nm-connection-editor.desktop
%{_datadir}/gir-1.0/NMGtk-1.0.gir
%{_datadir}/glib-2.0/schemas/org.gnome.nm-applet.gschema.xml
%{_datadir}/icons/hicolor/16x16/apps/nm-*.png
%{_datadir}/icons/hicolor/22x22/apps/nm-*.png
%{_datadir}/icons/hicolor/32x32/apps/nm-*.png
%{_datadir}/icons/hicolor/48x48/apps/nm-*.png
%{_datadir}/icons/hicolor/scalable/apps/nm-*.svg
%{_datadir}/libnm-gtk/wifi.ui
%{_datadir}/locale/*
%{_datadir}/nm-applet/*.ui
%{_datadir}/nm-applet/keyring.png
%{_mandir}/man1/nm-applet.1.gz
%{_mandir}/man1/nm-connection-editor.1.gz

%clean
rm -rf %{buildroot}

%post
gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
glib-compile-schemas %{_datadir}/glib-2.0/schemas &>/dev/null || :

%postun

%changelog
* Wed Sep 23 2015 tanggeliang <tanggeliang@gmail.com>
- update from 0.9.10.0 to 1.0.6
* Tue Jul 22 2014 tanggeliang <tanggeliang@gmail.com>
- update from 0.9.8.8 to 0.9.10.0
* Tue Dec 3 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.9.8.4 to 0.9.8.8
* Wed Sep 25 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.9.8.2 to 0.9.8.4
* Sat Jun 8 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.9.8.0 to 0.9.8.2
* Sun Mar 17 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.9.6.4 to 0.9.8.0
* Sat Nov 3 2012 tanggeliang <tanggeliang@gmail.com>
- update from 0.9.6.2 to 0.9.6.4
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
