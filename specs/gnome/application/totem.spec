Name:       totem
Version:    3.18.0
Release:    1%{?dist}
Summary:    Totem

Group:      User Interface/GNOME
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/%{name}/3.18/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  clutter-gst clutter-gtk gnome-icon-theme gst-plugins-bad gst-plugins-good libpeas totem-pl-parser yelp-xsl
BuildRequires:  dbus-glib gobject-introspection libzeitgeist nautilus pygobject rarian vala
BuildRequires:  grilo grilo-plugins gtk-doc intltool gettext xml-parser itstool shared-mime-info
BuildRequires:  docbook-xml docbook-xsl appstream-glib gnome-desktop
Requires:       gst-libav gst-plugins-ugly libdvdcss

%description
Totem package contains the official movie player of the GNOME Desktop based on GStreamer.
It features a playlist, a full-screen mode, seek and volume controls, as well as keyboard navigation.
This is useful for playing any GStreamer supported file, DVD, VCD or digital CD.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --libexecdir=%{_libdir}/totem \
            --disable-static \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/totem
%{_bindir}/totem-audio-preview
%{_bindir}/totem-video-thumbnailer
%{_includedir}/totem/1.0/totem*.h
%{_libdir}/girepository-1.0/Totem-1.0.typelib
%{_libdir}/libtotem.*
%{_libdir}/nautilus/extensions-3.0/libtotem-properties-page.*
%{_libdir}/pkgconfig/totem.pc
%{_libdir}/totem/plugins/*
%{_datadir}/GConf/gsettings/*.convert
%{_datadir}/appdata/org.gnome.Totem.appdata.xml
%{_datadir}/applications/org.gnome.Totem.desktop
%{_datadir}/dbus-1/services/org.gnome.Totem.service
%{_datadir}/gir-1.0/Totem-1.0.gir
%{_datadir}/glib-2.0/schemas/org.gnome.totem.*.xml
%{_datadir}/icons/hicolor/*/apps/totem.png
%{_datadir}/icons/hicolor/scalable/apps/totem-symbolic.svg
%{_datadir}/locale/*/LC_MESSAGES/totem.mo
%{_datadir}/thumbnailers/totem.thumbnailer
%{_datadir}/totem/*

%files doc
%defattr(-,root,root,-)
%{_datadir}/gtk-doc/html/totem/*
%{_datadir}/help/*/totem/*
%{_mandir}/man1/totem*.1.gz

%post
gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
glib-compile-schemas %{_datadir}/glib-2.0/schemas &>/dev/null || :

%clean
rm -rf %{buildroot}

%changelog
* Sat Sep 26 2015 tanggeliang <tanggeliang@gmail.com>
- update from 3.12.0 to 3.18.0
* Sat Mar 29 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.11.90 to 3.12.0
* Mon Mar 17 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.10.1 to 3.11.90
* Mon Oct 7 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.8.2 to 3.10.1
* Wed Aug 7 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.2 to 3.8.2
* Fri Nov 2 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.0 to 3.6.2
* Mon Oct 1 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.4.3 to 3.6.0
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
