Name:       alacarte
Version:    3.10.0
Release:    1%{?dist}
Summary:    Alacarte

Group:      User Interface/GNOME
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/%{name}/3.10/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  gnome-menus pygobject
BuildRequires:  intltool gettext xml-parser

%description
Alacarte is a menu editor for GNOME Desktop using the freedesktop.org menu specification.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/alacarte
/usr/lib/python2.7/site-packages/Alacarte/*.py*
%{_datadir}/alacarte/alacarte.ui
%{_datadir}/alacarte/directory-editor.ui
%{_datadir}/alacarte/launcher-editor.ui
%{_datadir}/applications/alacarte.desktop
%{_datadir}/icons/hicolor/*/apps/alacarte.png
%{_datadir}/locale/*/LC_MESSAGES/alacarte.mo

%clean
rm -rf %{buildroot}

%changelog
* Thu Sep 26 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.7.90 to 3.10.0
* Sat May 11 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.1 to 3.7.90
* Fri Nov 2 2012 tanggeliang <tanggeliang@gmail.com>
- update from 0.13.4 to 3.6.1
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
