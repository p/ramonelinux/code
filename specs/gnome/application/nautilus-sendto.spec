Name:       nautilus-sendto
Version:    3.8.2
Release:    1%{?dist}
Summary:    Nautilus Sendto

Group:      User Interface/GNOME
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/%{name}/3.8/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  nautilus evolution-data-server
BuildRequires:  gtk-doc gupnp gobject-introspection
BuildRequires:  intltool gettext xml-parser libical

%description
The Nautilus Sendto package provides the Nautilus file manager with a context menu component for quickly sending files to accounts in an Evolution email address book, contacts on a Pidgin, Gajim instant messaging list, through Thunderbird, or through Claws Mail (formerly Sylpheed Claws).

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/nautilus-sendto
%{_datadir}/locale/*
%{_mandir}/man1/*

%post
glib-compile-schemas %{_datadir}/glib-2.0/schemas &>/dev/null || :

%clean
rm -rf %{buildroot}

%changelog
* Wed Sep 24 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.8.1 to 3.8.2
* Tue Oct 8 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.8.0 to 3.8.1
* Sat Apr 13 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.1 to 3.8.0
* Mon Mar 18 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.0 to 3.6.1
* Fri Sep 28 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.0.2 to 3.6.0
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
