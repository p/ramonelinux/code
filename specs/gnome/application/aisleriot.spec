Name:       aisleriot
Version:    3.10.0
Release:    1%{?dist}
Summary:    Aisleriot

Group:      User Interface/GNOME
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/%{name}/3.10/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  gconf gtk+ guile libcanberra librsvg yelp-xsl
BuildRequires:  intltool gettext xml-parser
BuildRequires:  itstool gc libunistring clutter desktop-file-utils

%description
Aisleriot (also known as Solitaire or sol) is a collection of card games which are easy to play with the aid of a mouse.
The rules for the games have been coded for your pleasure in the GNOME scripting language (Scheme).

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --sysconfdir=/etc \
            --with-card-theme-formats=svg \
            --libexecdir=%{_libdir} \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
/etc/gconf/schemas/aisleriot.schemas
%{_bindir}/sol
%{_libdir}/aisleriot/ar-cards-renderer
%{_libdir}/aisleriot/guile/2.0/*.go
%{_libdir}/aisleriot/guile/2.0/aisleriot/api.go
%{_libdir}/valgrind/aisleriot.supp
%{_datadir}/aisleriot/aisleriot.catalog
%{_datadir}/aisleriot/cards/*.svgz
%{_datadir}/aisleriot/icons/hicolor/*/actions/cards-deal.png
%{_datadir}/aisleriot/icons/hicolor/scalable/actions/cards-deal.svg
%{_datadir}/aisleriot/sounds/*.ogg
%{_datadir}/applications/sol.desktop
%{_datadir}/glib-2.0/schemas/org.gnome.Patience.WindowState.gschema.xml
%{_datadir}/icons/hicolor/*/apps/gnome-*.png
%{_datadir}/icons/HighContrast/scalable/apps/gnome-aisleriot.svg
%{_datadir}/locale/*/LC_MESSAGES/aisleriot.mo

%files doc
%defattr(-,root,root,-)
%{_datadir}/help/*/aisleriot/*
%{_mandir}/man6/sol.6.gz

%post
gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
glib-compile-schemas %{_datadir}/glib-2.0/schemas &>/dev/null || :

%clean
rm -rf %{buildroot}

%changelog
* Thu Sep 26 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.8.0 to 3.10.0
* Wed Aug 7 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.7.91 to 3.8.0
* Sat May 11 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.2 to 3.7.91
* Sat Dec 1 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.1 to 3.6.2
* Mon Oct 29 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.0 to 3.6.1
* Sun Sep 30 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.4.1 to 3.6.0
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
