Name:       babl
Version:    0.1.10
Release:    1%{?dist}
Summary:    BABL

Group:      User Interface/GNOME
License:    GPLv2+
Url:        http://www.gtk.org
Source:     ftp://ftp.gtk.org/pub/babl/0.1/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  gobject-introspection

%description
babl is a dynamic, any to any, pixel format translation library.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_includedir}/babl-0.1/babl/babl*.h
%{_libdir}/babl-0.1/CIE.la
%{_libdir}/babl-0.1/CIE.so
%{_libdir}/babl-0.1/cairo.la
%{_libdir}/babl-0.1/cairo.so
%{_libdir}/babl-0.1/fast-float.la
%{_libdir}/babl-0.1/fast-float.so
%{_libdir}/babl-0.1/float.la
%{_libdir}/babl-0.1/float.so
%{_libdir}/babl-0.1/gegl-fixups.la
%{_libdir}/babl-0.1/gegl-fixups.so
%{_libdir}/babl-0.1/gggl-lies.la
%{_libdir}/babl-0.1/gggl-lies.so
%{_libdir}/babl-0.1/gggl.la
%{_libdir}/babl-0.1/gggl.so
%{_libdir}/babl-0.1/gimp-8bit.la
%{_libdir}/babl-0.1/gimp-8bit.so
%{_libdir}/babl-0.1/naive-CMYK.la
%{_libdir}/babl-0.1/naive-CMYK.so
%{_libdir}/babl-0.1/sse-fixups.la
%{_libdir}/babl-0.1/sse-fixups.so
%{_libdir}/libbabl-0.1.la
%{_libdir}/libbabl-0.1.so*
%{_libdir}/pkgconfig/babl.pc

%clean
rm -rf %{buildroot}

%changelog
* Sat Sep 28 2013 tanggeliang <tanggeliang@gmail.com>
- create
