Name:       gnome-photos
Version:    3.16.0
Release:    1%{?dist}
Summary:    GNOME Photos

Group:      User Interface/GNOME
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/%{name}/3.16/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  glib babl cairo exempi gegl grilo gfbgraph tracker
BuildRequires:  gdk-pixbuf gnome-desktop gnome-online-accounts
BuildRequires:  lcms2 libexif librsvg tracker desktop-file-utils
BuildRequires:  intltool itstool gettext xml-parser libgdata

%description
Photos - access, organize and share your photos on GNOME.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --libexecdir=%{_libdir}/%{name} \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/gnome-photos
%{_libdir}/gnome-photos/gnome-photos-service
%{_datadir}/appdata/org.gnome.Photos.appdata.xml
%{_datadir}/applications/org.gnome.Photos.desktop
%{_datadir}/dbus-1/services/org.gnome.Photos.service
%{_datadir}/glib-2.0/schemas/org.gnome.photos.gschema.xml
%{_datadir}/gnome-photos/icons/thumbnail-frame.png
%{_datadir}/gnome-shell/search-providers/org.gnome.Photos.search-provider.ini
%{_datadir}/icons/hicolor/*x*/apps/gnome-photos.png
%{_datadir}/locale/*/LC_MESSAGES/gnome-photos.mo

%files doc
%defattr(-,root,root,-)
%{_docdir}/gnome-photos/*
%{_datadir}/help/*/gnome-photos/*

%post
gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
glib-compile-schemas %{_datadir}/glib-2.0/schemas &>/dev/null || :

%clean
rm -rf %{buildroot}

%changelog
* Sun Mar 29 2015 tanggeliang <tanggeliang@gmail.com>
- update from 3.12.1 to 3.16.0
* Sun Jul 27 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.11.92 to 3.12.1
* Thu Mar 20 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.10.1 to 3.11.92
* Mon Oct 21 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.10.0 to 3.10.1
* Sat Sep 28 2013 tanggeliang <tanggeliang@gmail.com>
- create
