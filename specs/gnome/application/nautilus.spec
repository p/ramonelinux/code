Name:       nautilus
Version:    3.18.0
Release:    1%{?dist}
Summary:    Nautilus

Group:      User Interface/GNOME
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/%{name}/3.18/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  gnome-desktop gsettings-desktop-schemas
BuildRequires:  gobject-introspection pango
BuildRequires:  intltool gettext xml-parser libnotify
BuildRequires:  exempi libexif libxml shared-mime-info
Requires:       gvfs

%description
The Nautilus package contains the GNOME file manager.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --sysconfdir=/etc \
            --disable-tracker \
            --disable-packagekit \
            --libexecdir=%{_libdir}/nautilus \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
/etc/xdg/autostart/nautilus-autostart.desktop
%{_bindir}/nautilus*
%{_includedir}/nautilus/libnautilus-extension/nautilus-*.h
%{_libdir}/girepository-1.0/Nautilus-3.0.typelib
%{_libdir}/libnautilus-extension.*
%{_libdir}/nautilus/extensions-3.0/libnautilus-sendto.*
%{_libdir}/nautilus/nautilus-convert-metadata
%{_libdir}/pkgconfig/libnautilus-extension.pc
%{_datadir}/GConf/gsettings/nautilus.convert
%{_datadir}/appdata/org.gnome.Nautilus.appdata.xml
%{_datadir}/applications/*
%{_datadir}/dbus-1/services/org.freedesktop.FileManager1.service
%{_datadir}/dbus-1/services/org.gnome.Nautilus*.service
%{_datadir}/gir-1.0/Nautilus-3.0.gir
%{_datadir}/glib-2.0/schemas/org.gnome.nautilus.gschema.xml
%{_datadir}/gnome-shell/search-providers/nautilus-search-provider.ini
%{_datadir}/locale/*/LC_MESSAGES/nautilus.mo

%files doc
%defattr(-,root,root,-)
%{_datadir}/gtk-doc/*
%{_mandir}/*/*

%post
glib-compile-schemas %{_datadir}/glib-2.0/schemas &>/dev/null || :

%clean
rm -rf %{buildroot}

%changelog
* Wed Sep 23 2015 tanggeliang <tanggeliang@gmail.com>
- update from 3.17.91 to 3.18.0
* Sun Sep 20 2015 tanggeliang <tanggeliang@gmail.com>
- update from 3.16.0 to 3.17.91
* Sun Mar 29 2015 tanggeliang <tanggeliang@gmail.com>
- update from 3.14.0 to 3.16.0
* Wed Sep 24 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.12.2 to 3.14.0
* Sun Jul 20 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.12.0 to 3.12.2
* Sat Mar 29 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.11.90 to 3.12.0
* Sun Mar 16 2014 tanggeliang <tanggeliang@gmail.com>
- updare from 3.10.1 to 3.11.90
* Wed Dec 4 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.10.0 to 3.10.1
- add "--disable-update-mimedb" to fix "%{_datadir}/mime/*" conflicts shared-mime-info.
* Wed Sep 25 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.8.2 to 3.10.0
* Fri Jul 26 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.8.1 to 3.8.2
* Sat May 11 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.8.0 to 3.8.1
* Sat Apr 13 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.3 to 3.8.0
* Sun Feb 24 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.2 to 3.6.3
* Mon Nov 26 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.1 to 3.6.2
* Sat Oct 27 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.0 to 3.6.1
* Fri Sep 28 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.4.2 to 3.6.0
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
