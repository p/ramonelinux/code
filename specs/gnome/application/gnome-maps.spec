Name:       gnome-maps
Version:    3.12.2
Release:    1%{?dist}
Summary:    GNOME Maps

Group:      User Interface/GNOME
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/%{name}/3.12/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  gtk+ gobject-introspection
BuildRequires:  pkg-config intltool gettext xml-parser
BuildRequires:  gjs
Requires:       libchamplain geocode-glib geoclue

%description
Maps is a map application for GNOME.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/gnome-maps
%{_datadir}/appdata/gnome-maps.appdata.xml
%{_datadir}/applications/gnome-maps.desktop
%{_datadir}/glib-2.0/schemas/org.gnome.maps.gschema.xml
%{_datadir}/gnome-maps/pixmaps/bubble.svg
%{_datadir}/gnome-maps/pixmaps/pin.svg
%{_datadir}/icons/hicolor/*x*/apps/gnome-maps.png
%{_datadir}/locale/*/LC_MESSAGES/gnome-maps.mo

%post
gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
glib-compile-schemas %{_datadir}/glib-2.0/schemas &>/dev/null || :

%clean
rm -rf %{buildroot}

%changelog
* Sun Jul 27 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.11.92 to 3.12.2
* Thu Mar 20 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.10.2 to 3.11.92
* Wed Dec 4 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.10.0 to 3.10.2
* Thu Sep 26 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.9.5 to 3.10.0
* Wed Aug 7 2013 tanggeliang <tanggeliang@gmail.com>
- create
