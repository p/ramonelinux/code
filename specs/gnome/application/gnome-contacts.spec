Name:       gnome-contacts
Version:    3.18.0
Release:    1%{?dist}
Summary:    GNOME Contacts

Group:      User Interface/GNOME
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/%{name}/3.18/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  evolution-data-server folks gnome-desktop gnome-online-accounts vala
BuildRequires:  cheese intltool gettext xml-parser telepathy-glib libgee
BuildRequires:  libnotify gobject-introspection libchamplain
Requires:       telepathy-mission-control

%description
GNOME Contacts is a simple contacts application that allows you to view your contacts as well as edit their details and create new contacts.
Online integration is an important aspect of GNOME Contacts - it will seamlessly synchronise with the contacts stored in online accounts.
If you have the same person in multiple online accounts, GNOME Contacts will automatically link them together into the same contact.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --libexecdir=%{_libdir}/gnome-contacts &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/gnome-contacts
%{_libdir}/gnome-contacts/gnome-contacts-search-provider
%{_datadir}/appdata/org.gnome.Contacts.appdata.xml
%{_datadir}/applications/org.gnome.Contacts.desktop
%{_datadir}/dbus-1/services/org.gnome.Contacts.service
%{_datadir}/dbus-1/services/org.gnome.Contacts.SearchProvider.service
%{_datadir}/glib-2.0/schemas/org.gnome.Contacts.enums.xml
%{_datadir}/glib-2.0/schemas/org.gnome.Contacts.gschema.xml
%{_datadir}/gnome-shell/search-providers/org.gnome.Contacts.search-provider.ini
%{_datadir}/locale/*

%post
glib-compile-schemas %{_datadir}/glib-2.0/schemas &>/dev/null || :

%clean
rm -rf %{buildroot}

%changelog
* Sat Sep 26 2015 tanggeliang <tanggeliang@gmail.com>
- update from 3.16.2 to 3.18.0
* Sun Mar 29 2015 tanggeliang <tanggeliang@gmail.com>
- update from 3.14.0 to 3.16.0
* Wed Sep 24 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.12.0 to 3.14.0
* Thu Jul 24 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.11.91 to 3.12.0
* Sun Mar 16 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.10.1 to 3.11.91
* Tue Oct 22 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.10 to 3.10.1
* Wed Sep 25 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.8.3 to 3.10
* Fri Jul 26 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.8.2 to 3.8.3
* Sun Jun 9 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.8.1 to 3.8.2
* Fri May 10 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.2 to 3.8.1
* Tue Mar 19 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.1 to 3.6.2
* Fri Nov 2 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.0 to 3.6.1
* Mon Oct 1 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.4.1 to 3.6.0
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
