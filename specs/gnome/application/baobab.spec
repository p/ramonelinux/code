Name:       baobab
Version:    3.12.0
Release:    1%{?dist}
Summary:    Baobab

Group:      User Interface/GNOME
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/%{name}/3.12/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  gtk+ vala yelp-xsl
BuildRequires:  intltool gettext xml-parser
BuildRequires:  itstool libxml gobject-introspection

%description
The Baobab package contains a graphical directory tree analyzer.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/baobab
%{_datadir}/appdata/baobab.appdata.xml
%{_datadir}/applications/baobab.desktop
%{_datadir}/glib-2.0/schemas/org.gnome.baobab.gschema.xml
%{_datadir}/help/*/baobab/legal.xml
%ifarch %{ix86}
%{_datadir}/icons/HighContrast/*x*/apps/baobab.png
%{_datadir}/icons/hicolor/*x*/apps/baobab.png
%else %ifarch x86_64
%{_datadir}/icons/HighContrast/HighContrast_apps_*x*_baobab.png
%{_datadir}/icons/hicolor/hicolor_apps_*x*_baobab.png
%endif
%{_datadir}/icons/hicolor/scalable/actions/view-*-symbolic.svg
%{_datadir}/locale/*/LC_MESSAGES/baobab.mo

%files doc
%defattr(-,root,root,-)
%{_datadir}/help/*/baobab/*.page
%{_mandir}/man1/baobab.1.gz

%post
gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
glib-compile-schemas %{_datadir}/glib-2.0/schemas &>/dev/null || :

%clean
rm -rf %{buildroot}

%changelog
* Sat Mar 29 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.11.90 to 3.12.0
* Sun Mar 16 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.10.1 to 3.11.90
* Wed Dec 4 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.8.2 to 3.10.1
* Sat Aug 17 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.8.1 to 3.8.2
* Sat May 11 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.3 to 3.8.1
* Sat Dec 1 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.1 to 3.6.3
* Fri Nov 2 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.0 to 3.6.1
* Sat Sep 29 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.4.1 to 3.6.0
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
