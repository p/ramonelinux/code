Name:       sound-juicer
Version:    3.14.0
Release:    1%{?dist}
Summary:    Sound Juicer

Group:      User Interface/GNOME
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/%{name}/3.14/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  gconf gst-plugins-base gtk+ libcanberra libmusicbrainz
BuildRequires:  rarian brasero libxslt libdiscid
BuildRequires:  intltool gettext xml-parser iso-codes
BuildRequires:  docbook-xml docbook-xsl itstool
Requires:       gst-plugins-good gst-plugins-ugly

%description
The Sound Juicer package contains the simple CD ripping tool which is useful for extracting the audio tracks from audio compact discs and converting them into audio files.
It can also play the audio tracks directly from the CD, allowing you to preview the CD before ripping it.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --sysconfdir=/etc &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/sound-juicer
%{_datadir}/GConf/gsettings/sound-juicer.convert
%{_datadir}/applications/sound-juicer.desktop
%{_datadir}/glib-2.0/schemas/org.gnome.sound-juicer.gschema.xml
%{_datadir}/icons/hicolor/*/apps/sound-juicer.png
%{_datadir}/locale/*/LC_MESSAGES/sound-juicer.mo
%{_datadir}/sound-juicer/rhythmbox.gep
%{_datadir}/sound-juicer/sound-juicer.ui
%{_datadir}/sound-juicer/sound-juicer-menu.ui

%files doc
%defattr(-,root,root,-)
%{_datadir}/help/*/sound-juicer/*
%{_mandir}/man1/sound-juicer.1.gz

%post
gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :

%clean
rm -rf %{buildroot}

%changelog
* Fri Sep 26 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.5.0 to 3.14.0
* Mon Oct 1 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.4.0 to 3.5.0
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
