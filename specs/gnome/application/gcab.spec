Name:       gcab
Version:    0.6
Release:    1%{?dist}
Summary:    gcab

Group:      User Interface/GNOME
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/%{name}/0.6/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  glib vala zlib intltool gettext xml-parser

%description

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/gcab
%{_includedir}/libgcab-1.0/libgcab.h
%{_includedir}/libgcab-1.0/libgcab/gcab-*.h
%{_libdir}/libgcab-1.0.*a
%{_libdir}/libgcab-1.0.so*
%{_libdir}/pkgconfig/libgcab-1.0.pc
%{_datadir}/gtk-doc/html/gcab/*
%{_datadir}/locale/*/LC_MESSAGES/gcab.mo
%{_mandir}/man1/gcab.1.gz

%clean
rm -rf %{buildroot}

%changelog
* Wed Sep 23 2015 tanggeliang <tanggeliang@gmail.com>
- create
