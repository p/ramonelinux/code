Name:       gnome-system-log
Version:    3.9.90
Release:    1%{?dist}
Summary:    GNOME System Log

Group:      User Interface/GNOME
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/%{name}/3.9/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  gtk+ yelp-xsl
BuildRequires:  intltool gettext xml-parser libxml itstool

%description
The GNOME System Log package contains a simple utility used to display system log files.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/gnome-system-log
%{_datadir}/GConf/gsettings/logview.convert
%{_datadir}/applications/gnome-system-log.desktop
%{_datadir}/glib-2.0/schemas/org.gnome.gnome-system-log.gschema.xml
%{_datadir}/icons/hicolor/*/apps/logview.png
%{_datadir}/icons/HighContrast/*/apps/logview.png
%{_datadir}/locale/*/LC_MESSAGES/gnome-system-log.mo

%files doc
%defattr(-,root,root,-)
%{_datadir}/help/*/gnome-system-log/*
%{_mandir}/man1/gnome-system-log.1.gz

%post
gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
glib-compile-schemas %{_datadir}/glib-2.0/schemas &>/dev/null || :

%clean
rm -rf %{buildroot}

%changelog
* Thu Sep 26 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.8.1 to 3.9.90
* Sat May 11 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.1 to 3.8.1
* Tue Mar 19 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.0 to 3.6.1
* Mon Oct 1 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.4.1 to 3.6.0
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
