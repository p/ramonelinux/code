Name:       gnome-terminal
Version:    3.18.0
Release:    1%{?dist}
Summary:    GNOME Terminal

Group:      User Interface/GNOME
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/%{name}/3.18/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  gconf gsettings-desktop-schemas vte yelp-xsl nautilus
BuildRequires:  rarian gnome-shell appstream-glib
BuildRequires:  intltool gettext xml-parser gtk2 libxslt docbook-xml
BuildRequires:  libsm libice itstool vala glib gtk+ yelp-tools
BuildRequires:  dconf desktop-file-utils
Requires(post): glib

%description
The GNOME Terminal package contains the terminal emulator for GNOME Desktop.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr             \
            --disable-static          \
            --disable-migration       \
            --with-nautilus-extension \
            --libexecdir=%{_libdir}/gnome-terminal \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/gnome-terminal
%{_libdir}/gnome-terminal/gnome-terminal-server
%{_libdir}/nautilus/extensions-3.0/libterminal-nautilus.*
%{_datadir}/appdata/gnome-terminal.appdata.xml
%{_datadir}/applications/gnome-terminal.desktop
%{_datadir}/locale/*/LC_MESSAGES/gnome-terminal.mo
%{_datadir}/dbus-1/services/org.gnome.Terminal.service
%{_datadir}/glib-2.0/schemas/org.gnome.Terminal.gschema.xml
%{_datadir}/gnome-shell/search-providers/gnome-terminal-search-provider.ini

%files doc
%defattr(-,root,root,-)
%{_datadir}/help/*/gnome-terminal/*

%clean
rm -rf %{buildroot}

%post
/usr/bin/glib-compile-schemas %{_datadir}/glib-2.0/schemas &> /dev/null || :

%postun
if [ $1 -eq 0 ] ; then
    /usr/bin/glib-compile-schemas %{_datadir}/glib-2.0/schemas &> /dev/null || :
fi

%changelog
* Wed Sep 23 2015 tanggeliang <tanggeliang@gmail.com>
- update from 3.17.91 to 3.18.0
* Wed Sep 16 2015 tanggeliang <tanggeliang@gmail.com>
- update from 3.16.0 to 3.17.91
* Sun Mar 29 2015 tanggeliang <tanggeliang@gmail.com>
- update from 3.14.0 to 3.16.0
* Tue Sep 23 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.12.3 to 3.14.0
* Sat Jul 19 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.12.1 to 3.12.3
* Wed Apr 30 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.12.0 to 3.12.1
* Sat Mar 29 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.11.3 to 3.12.0
* Mon Mar 17 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.10.2 to 3.11.3
* Wed Dec 4 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.10.1 to 3.10.2
* Wed Oct 23 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.10.0 to 3.10.1
* Thu Sep 26 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.8.4 to 3.10.0
* Fri Jul 26 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.8.2 to 3.8.4
* Sun Jun 9 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.8.1 to 3.8.2
* Sat May 11 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.8.0 to 3.8.1
* Sun Apr 14 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.1 to 3.8.0
* Sat Oct 27 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.0 to 3.6.1
* Sat Sep 29 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.4.1.1 to 3.6.0
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
