Name:       cheese
Version:    3.18.0
Release:    1%{?dist}
Summary:    Cheese

Group:      User Interface/GNOME
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/%{name}/3.18/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  clutter-gst clutter-gtk gnome-desktop gnome-video-effects gst-plugins-bad gst-plugins-good libgee libcanberra librsvg systemd-udev yelp-xsl
BuildRequires:  gobject-introspection nautilus-sendto vala
BuildRequires:  gtk-doc intltool gettext xml-parser
BuildRequires:  itstool docbook-xml docbook-xsl
BuildRequires:  systemd-gudev
Requires:       gst-plugins-bad

%description
The Cheese package is used to take photos and videos with fun graphical effects.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --libdir=%{_libdir} \
            --libexecdir=%{_libdir}/%{name} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/cheese
%{_includedir}/cheese/cheese*.h
%{_libdir}/girepository-1.0/Cheese-3.0.typelib
%{_libdir}/libcheese-gtk.*
%{_libdir}/libcheese.*
%{_libdir}/pkgconfig/cheese*.pc
%{_libdir}/%{name}/gnome-camera-service
%{_datadir}/appdata/org.gnome.Cheese.appdata.xml
%{_datadir}/applications/org.gnome.Cheese.desktop
%{_datadir}/dbus-1/services/org.gnome.Camera.service
%{_datadir}/dbus-1/services/org.gnome.Cheese.service
%{_datadir}/gir-1.0/Cheese-3.0.gir
%{_datadir}/glib-2.0/schemas/org.gnome.Cheese.gschema.xml
%{_datadir}/icons/hicolor/*/apps/cheese.png
%{_datadir}/icons/hicolor/symbolic/apps/cheese-symbolic.svg
%{_datadir}/locale/*/LC_MESSAGES/cheese.mo

%files doc
%defattr(-,root,root,-)
%{_datadir}/gtk-doc/html/cheese/*
%{_datadir}/help/*/cheese/*

%post
gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
glib-compile-schemas %{_datadir}/glib-2.0/schemas &>/dev/null || :

%clean
rm -rf %{buildroot}

%changelog
* Wed Sep 23 2015 tanggeliang <tanggeliang@gmail.com>
- update from 3.17.91 to 3.18.0
* Sun Mar 29 2015 tanggeliang <tanggeliang@gmail.com>
- update from 3.14.0 to 3.16.0
* Wed Sep 24 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.12.2 to 3.14.0
* Sun Jul 20 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.12.0 to 3.12.2
* Sat Mar 29 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.11.91 to 3.12.0
* Sun Mar 16 2014 tanggeliang <tanggeliang@gmail.com>
- updare from 3.10.2 to 3.11.91
* Wed Dec 4 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.10.1 to 3.10.2
* Mon Oct 21 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.10.0 to 3.10.1
* Wed Sep 25 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.8.3 to 3.10.0
* Fri Jul 26 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.8.1 to 3.8.3
* Fri May 10 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.2 to 3.8.1
* Tue Mar 19 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.1 to 3.6.2
* Fri Nov 2 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.0 to 3.6.1
* Sun Sep 30 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.4.2 to 3.6.0
* Sun Aug 26 2012 tanggeliang <tanggeliang@gmail.com>
- add gst-plugins-bad fix "GStreamer elements are missing vp8enc".
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
