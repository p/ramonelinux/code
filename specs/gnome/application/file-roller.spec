Name:       file-roller
Version:    3.14.0
Release:    1%{?dist}
Summary:    File Roller

Group:      User Interface/GNOME
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/%{name}/3.14/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  gtk+ yelp-xsl
BuildRequires:  json-glib libarchive libnotify nautilus
BuildRequires:  libsm intltool gettext xml-parser itstool
Requires:       unrar unzip zip

%description
File Roller is an archive manager for GNOME with support for tar, bzip2, gzip, zip, jar, compress, lzop and many other archive formats.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --libexecdir=%{_libdir} \
            --disable-packagekit \
            --disable-static \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/file-roller
%{_libdir}/file-roller/isoinfo.sh
%{_libdir}/file-roller/rpm2cpio
%{_libdir}/nautilus/extensions-3.0/libnautilus-fileroller.*
%{_datadir}/GConf/gsettings/file-roller.convert
%{_datadir}/appdata/org.gnome.FileRoller.appdata.xml
%{_datadir}/applications/org.gnome.FileRoller.desktop
%{_datadir}/dbus-1/services/org.gnome.FileRoller.ArchiveManager1.service
%{_datadir}/dbus-1/services/org.gnome.FileRoller.service
%{_datadir}/file-roller/packages.match
%{_datadir}/glib-2.0/schemas/org.gnome.FileRoller.gschema.xml
%{_datadir}/icons/hicolor/*/apps/file-roller.png
%{_datadir}/locale/*/LC_MESSAGES/file-roller.mo

%files doc
%defattr(-,root,root,-)
%{_datadir}/help/*/file-roller/*

%post
gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
glib-compile-schemas %{_datadir}/glib-2.0/schemas &>/dev/null || :

%clean
rm -rf %{buildroot}

%changelog
* Fri Sep 26 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.12.2 to 3.14.0
* Tue Jul 22 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.12.0 to 3.12.2
* Sat Mar 29 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.11.91 to 3.12.0
* Sun Mar 16 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.10.2.1 to 3.11.91
* Wed Dec 4 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.10.1 to 3.10.2.1
* Wed Oct 23 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.10.0 to 3.10.1
* Wed Oct 2 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.8.4 to 3.10.0
* Sat Aug 24 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.8.3 to 3.8.4
* Sun Jul 28 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.8.2 to 3.8.3
* Thu May 30 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.8.1 to 3.8.2
* Fri May 10 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.3 to 3.8.1
* Thu Apr 4 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.1.1 to 3.6.3
* Fri Nov 2 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.0 to 3.6.1.1
* Sat Sep 29 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.4.2 to 3.6.0
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
