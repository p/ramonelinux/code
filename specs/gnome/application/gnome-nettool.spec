Name:       gnome-nettool
Version:    3.8.1
Release:    1%{?dist}
Summary:    GNOME Nettool

Group:      User Interface/GNOME
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/%{name}/3.8/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  gnome-doc-utils gtk+ libgtop yelp-xsl
BuildRequires:  rarian
BuildRequires:  intltool gettext xml-parser
BuildRequires:  libxml libxslt docbook-xml docbook-xsl
BuildRequires:  itstool
#Requires:       bind net-tools traceroute whois

%description
The GNOME Nettool package is a network information tool which provides GUI interface for some of the most common command line network tools.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/gnome-nettool
%{_datadir}/applications/gnome-nettool.desktop
%{_datadir}/glib-2.0/schemas/org.gnome.gnome-nettool.gschema.xml
%{_datadir}/gnome-nettool/pixmaps/*.xpm
%{_datadir}/gnome-nettool/pixmaps/*.png
%{_datadir}/gnome-nettool/ui/gnome-nettool.ui
%{_datadir}/icons/hicolor/*/apps/gnome-nettool.png
%{_datadir}/icons/hicolor/scalable/apps/gnome-nettool.svg
%{_datadir}/locale/*/LC_MESSAGES/gnome-nettool.mo

%files doc
%defattr(-,root,root,-)
%{_datadir}/help/*/gnome-nettool/*

%post
gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
glib-compile-schemas %{_datadir}/glib-2.0/schemas &>/dev/null || :

%postun

%clean
rm -rf %{buildroot}

%changelog
* Sat Aug 17 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.8.0 to 3.8.1
* Fri May 10 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.2.0 to 3.8.0
- add "enable_compile_warnings=no"
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
