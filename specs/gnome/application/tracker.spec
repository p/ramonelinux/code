Name:       tracker
Version:    1.6.0
Release:    1%{?dist}
Summary:    Tracker

Group:      User Interface/GNOME
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/%{name}/1.6/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  glib libpng icu libxml
BuildRequires:  libsecret networkmanager libexif exempi taglib gdk-pixbuf
BuildRequires:  gstreamer poppler giflib libjpeg-turbo libtiff
BuildRequires:  nautilus gobject-introspection
BuildRequires:  intltool gettext xml-parser libmediaart libosinfo

%description
Tracker is a search engine and that allows the user to find their data as fast as possible. Users can search for their files and search for content in their files too.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --sysconfdir=/etc \
            --libexecdir=%{_libdir}/tracker \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
/etc/xdg/autostart/tracker-extract.desktop
/etc/xdg/autostart/tracker-miner-fs.desktop
/etc/xdg/autostart/tracker-store.desktop
/etc/xdg/autostart/tracker-miner-apps.desktop
/etc/xdg/autostart/tracker-miner-user-guides.desktop
%{_bindir}/tracker
%{_bindir}/tracker-control
%{_bindir}/tracker-import
%{_bindir}/tracker-info
%{_bindir}/tracker-needle
%{_bindir}/tracker-preferences
%{_bindir}/tracker-search
%{_bindir}/tracker-sparql
%{_bindir}/tracker-stats
%{_bindir}/tracker-tag
%{_includedir}/tracker-1.0/libtracker-control/tracker-*.h
%{_includedir}/tracker-1.0/libtracker-miner/tracker-*.h
%{_includedir}/tracker-1.0/libtracker-sparql/tracker-*.h
%{_libdir}/girepository-1.0/Tracker*-1.0.typelib
%{_libdir}/libtracker-control-1.0.*
%{_libdir}/libtracker-miner-1.0.*a
%{_libdir}/libtracker-miner-1.0.so*
%{_libdir}/libtracker-sparql-1.0.*a
%{_libdir}/libtracker-sparql-1.0.so*
%{_libdir}/nautilus/extensions-3.0/libnautilus-tracker-tags.*
%{_libdir}/pkgconfig/tracker-control-1.0.pc
%{_libdir}/pkgconfig/tracker-miner-1.0.pc
%{_libdir}/pkgconfig/tracker-sparql-1.0.pc
%{_libdir}/tracker-1.0/extract-modules/libextract-*.*
%{_libdir}/tracker-1.0/writeback-modules/libwriteback-taglib.*
%{_libdir}/tracker-1.0/writeback-modules/libwriteback-xmp.*
%{_libdir}/tracker-1.0/libtracker-extract.*
%{_libdir}/tracker-1.0/libtracker-common.*
%{_libdir}/tracker-1.0/libtracker-data.*
%{_libdir}/tracker/tracker-extract
%{_libdir}/tracker/tracker-miner-apps
%{_libdir}/tracker/tracker-miner-fs
%{_libdir}/tracker/tracker-miner-user-guides
%{_libdir}/tracker/tracker-store
%{_libdir}/tracker/tracker-writeback
%{_datadir}/appdata/tracker-*.appdata.xml
%{_datadir}/applications/tracker-*.desktop
%{_datadir}/dbus-1/services/org.freedesktop.Tracker1*.service
%{_datadir}/gir-1.0/Tracker*-1.0.gir
%{_datadir}/glib-2.0/schemas/org.freedesktop.Tracker*.xml
%{_datadir}/icons/hicolor/*x*/apps/tracker.png
%{_datadir}/icons/hicolor/scalable/apps/tracker.svg
%{_datadir}/locale/*/LC_MESSAGES/tracker.mo
%{_datadir}/tracker-tests/01-writeback.py
%{_datadir}/tracker/extract-rules/*.rule
%{_datadir}/tracker/miners/org.freedesktop.Tracker1.Miner.*.service
%{_datadir}/tracker/stop-words/stopwords.*
%{_datadir}/tracker/ontologies/*.description
%{_datadir}/tracker/ontologies/*.ontology
%{_datadir}/tracker/tracker-*.xml
%{_datadir}/tracker/tracker-*.ui
%{_datadir}/vala/vapi/tracker-*-1.0.deps
%{_datadir}/vala/vapi/tracker-*-1.0.vapi
%{_datadir}/bash-completion/completions/tracker

%files doc
%defattr(-,root,root,-)
%{_datadir}/gtk-doc/html/libtracker-control/*
%{_datadir}/gtk-doc/html/libtracker-miner/*
%{_datadir}/gtk-doc/html/libtracker-sparql/*
%{_mandir}/man1/tracker-*.1.gz
%{_mandir}/man5/tracker-*.5.gz

%post
gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
glib-compile-schemas %{_datadir}/glib-2.0/schemas &>/dev/null || :

%clean
rm -rf %{buildroot}

%changelog
* Wed Sep 23 2015 tanggeliang <tanggeliang@gmail.com>
- update from 1.5.2 to 1.6.0
* Sat Mar 28 2015 tanggeliang <tanggeliang@gmail.com>
- update from 1.1.1 to 1.4.0
* Sun Jul 27 2014 tanggeliang <tanggeliang@gmail.com>
- update from 0.17.8 to 1.1.1
* Sat Mar 22 2014 tanggeliang <tanggeliang@gmail.com>
- update from 0.17.7 to 0.17.8
* Thu Mar 20 2014 tanggeliang <tanggeliang@gmail.com>
- update from 0.16.2 to 0.17.7
* Sat Sep 28 2013 tanggeliang <tanggeliang@gmail.com>
- create
