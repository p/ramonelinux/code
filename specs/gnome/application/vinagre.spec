Name:       vinagre
Version:    3.8.3
Release:    1%{?dist}
Summary:    Vinagre

Group:      User Interface/GNOME
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/%{name}/3.8/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  gnome-icon-theme gtk-vnc libsecret yelp-xsl
BuildRequires:  telepathy-glib vala vte itstool
BuildRequires:  intltool gettext xml-parser
BuildRequires:  docbook-xml docbook-xsl

%description
Vinagre is a VNC client for the GNOME Desktop.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/vinagre
%{_datadir}/GConf/gsettings/org.gnome.Vinagre.convert
%{_datadir}/applications/vinagre*.desktop
%{_datadir}/dbus-1/services/org.freedesktop.Telepathy.Client.Vinagre.service
%{_datadir}/glib-2.0/schemas/org.gnome.Vinagre.gschema.xml
%{_datadir}/icons/hicolor/*/mimetypes/application-x-*.png
%{_datadir}/icons/hicolor/*/status/view-minimize.png
%{_datadir}/icons/hicolor/scalable/mimetypes/application-x-remote-connection.svg
%{_datadir}/icons/hicolor/scalable/mimetypes/application-x-vnc.svg
%{_datadir}/locale/*/LC_MESSAGES/vinagre.mo
%{_datadir}/mime/packages/vinagre-mime.xml
%{_datadir}/telepathy/clients/Vinagre.client
%{_datadir}/vinagre/vinagre*ui*

%files doc
%defattr(-,root,root,-)
%{_datadir}/help/*/vinagre/*
%{_mandir}/man1/vinagre.1.gz

%post
gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
glib-compile-schemas %{_datadir}/glib-2.0/schemas &>/dev/null || :

%clean
rm -rf %{buildroot}

%changelog
* Wed Aug 7 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.1 to 3.8.3
* Fri Nov 2 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.0 to 3.6.1
* Mon Oct 1 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.4.2 to 3.6.0
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
