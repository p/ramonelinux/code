Name:       gupnp-av
Version:    0.12.7
Release:    1%{?dist}
Summary:    gupnp-av

Group:      User Interface/GNOME
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/%{name}/0.12/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  glib gupnp gobject-introspection

%description

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_includedir}/gupnp-av-1.0/libgupnp-av/gupnp-*.h
%{_libdir}/girepository-1.0/GUPnPAV-1.0.typelib
%{_libdir}/libgupnp-av-1.0.*a
%{_libdir}/libgupnp-av-1.0.so*
%{_libdir}/pkgconfig/gupnp-av-1.0.pc
%{_datadir}/gir-1.0/GUPnPAV-1.0.gir
%{_datadir}/gtk-doc/html/gupnp-av/*
%{_datadir}/gupnp-av/*.xsd

%clean
rm -rf %{buildroot}

%changelog
* Wed Sep 23 2015 tanggeliang <tanggeliang@gmail.com>
- create
