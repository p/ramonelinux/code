Name:       mousetweaks
Version:    3.12.0
Release:    1%{?dist}
Summary:    Mouse Tweaks

Group:      User Interface/GNOME
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/%{name}/3.12/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  gsettings-desktop-schemas gtk+
BuildRequires:  libxtst libxslt docbook-xml
BuildRequires:  intltool gettext xml-parser

%description
The Mouse Tweaks package provides mouse accessibility enhancements for the GNOME desktop.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/mousetweaks
%{_datadir}/GConf/gsettings/mousetweaks.convert
%{_datadir}/glib-2.0/schemas/org.gnome.mousetweaks.*.xml
%{_datadir}/locale/*/LC_MESSAGES/mousetweaks.mo
%{_datadir}/mousetweaks/*
%{_mandir}/man1/mousetweaks.1.gz

%post
glib-compile-schemas %{_datadir}/glib-2.0/schemas &>/dev/null || :

%clean
rm -rf %{buildroot}

%changelog
* Wed Sep 23 2015 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.0 to 3.12.0
* Mon Oct 1 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.4.2 to 3.6.0
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
