Name:       empathy
Version:    3.11.91
Release:    1%{?dist}
Summary:    Empathy

Group:      User Interface/GNOME
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/%{name}/3.11/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  clutter-gst clutter-gtk evolution-data-server folks libcanberra pulseaudio telepathy-farstream telepathy-logger telepathy-mission-control telepathy-account-widgets yelp-xsl
BuildRequires:  enchant iso-codes nautilus-sendto systemd
BuildRequires:  cheese geoclue intltool gettext xml-parser itstool vala
BuildRequires:  libnotify libaccounts-glib

%description
Empathy is an instant messaging program which supports text, voice, and video chat and file transfers over many different protocols.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --libexecdir=%{_libdir}/empathy \
            --disable-static \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/empathy*
%{_libdir}/empathy/empathy-*
%{_libdir}/empathy/libempathy-%{version}.so
%{_libdir}/empathy/libempathy-gtk*.*
%{_libdir}/empathy/libempathy.*
%{_libdir}/mission-control-plugins.0/mcp-account-manager-goa.*
%{_datadir}/GConf/gsettings/empathy.convert
%{_datadir}/adium/message-styles/*
%{_datadir}/appdata/empathy.appdata.xml
%{_datadir}/applications/empathy.desktop
%{_datadir}/dbus-1/services/org.freedesktop.Telepathy.Client.Empathy.*.service
%{_datadir}/empathy/*.*
%ifarch %{ix86}
%{_datadir}/empathy/icons/hicolor/*/actions/*.png
%{_datadir}/empathy/icons/hicolor/*/apps/*.png
%{_datadir}/empathy/icons/hicolor/*/status/*.png
%{_datadir}/empathy/icons/hicolor/scalable/apps/*.svg
%{_datadir}/empathy/icons/hicolor/scalable/status/*.svg
%{_datadir}/icons/hicolor/*/apps/empathy.png
%else %ifarch x86_64
%{_datadir}/empathy/icons/hicolor/hicolor_*.png
%{_datadir}/empathy/icons/hicolor/hicolor_*.svg
%{_datadir}/icons/hicolor/hicolor_apps_*_empathy.png
%endif
%{_datadir}/glib-2.0/schemas/org.gnome.Empathy.gschema.xml
%{_datadir}/glib-2.0/schemas/org.gnome.telepathy-account-widgets.gschema.xml
%{_datadir}/locale/*/LC_MESSAGES/empathy.mo
%{_datadir}/locale/*/LC_MESSAGES/empathy-tpaw.mo
%{_datadir}/telepathy/clients/Empathy.*.client

%files doc
%defattr(-,root,root,-)
%{_datadir}/help/*/empathy/*
%{_mandir}/man1/empathy*.1.gz

%post
gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
glib-compile-schemas %{_datadir}/glib-2.0/schemas &>/dev/null || :

%clean
rm -rf %{buildroot}

%changelog
* Sat Mar 15 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.10.2 to 3.11.91
* Mon Jan 6 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.10.1 to 3.10.2
* Mon Oct 21 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.10.0 to 3.10.1
* Wed Sep 25 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.8.3 to 3.10.0
* Sat Aug 17 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.8.1 to 3.8.3
* Fri May 10 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.3 to 3.8.1
* Sun Feb 24 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.1 to 3.6.3
* Fri Nov 2 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.0 to 3.6.1
* Sun Sep 30 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.4.2.3 to 3.6.0
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
