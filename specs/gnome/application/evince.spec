Name:       evince
Version:    3.18.0
Release:    1%{?dist}
Summary:    Evince

Group:      User Interface/GNOME
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/%{name}/3.18/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  gnome-icon-theme gsettings-desktop-schemas gtk+ yelp-xsl
BuildRequires:  gobject-introspection libgnome-keyring nautilus poppler
BuildRequires:  libsm libice itstool adwaita-icon-theme
BuildRequires:  gtk-doc intltool gettext xml-parser
Requires:       shared-mime-info

%description
Evince is a document viewer for multiple document formats.
It supports PDF, Postscript, DjVu, TIFF and DVI.
It is useful for viewing documents of various types using one simple application instead of the multiple document viewers that once existed on the GNOME Desktop.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --enable-introspection \
            --disable-static \
            --libexecdir=%{_libdir}/evince \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/evince*
%{_includedir}/evince/3.0/evince-*.h
%{_includedir}/evince/3.0/libdocument/ev-*.h
%{_includedir}/evince/3.0/libview/ev-*.h
%{_libdir}/evince/4/backends/lib*document.*
%{_libdir}/evince/4/backends/*document.evince-backend
%{_libdir}/evince/evinced
%{_libdir}/girepository-1.0/Evince*-3.0.typelib
%{_libdir}/mozilla/plugins/libevbrowserplugin.*
%{_libdir}/libevdocument3.*
%{_libdir}/libevview3.*
%{_libdir}/nautilus/extensions-3.0/libevince-properties-page.*
%{_libdir}/pkgconfig/evince-document-3.0.pc
%{_libdir}/pkgconfig/evince-view-3.0.pc
%{_datadir}/GConf/gsettings/evince.convert
%{_datadir}/appdata/evince*.xml
%{_datadir}/applications/evince*.desktop
%{_datadir}/dbus-1/services/org.gnome.evince.Daemon.service
%{_datadir}/evince/*.*
%{_datadir}/evince/icons/hicolor/*/actions/*
%{_datadir}/evince/icons/hicolor/*/mimetypes/*
%{_datadir}/evince/icons/hicolor/scalable/apps/evince-symbolic.svg
%{_datadir}/gir-1.0/EvinceDocument-3.0.gir
%{_datadir}/gir-1.0/EvinceView-3.0.gir
%{_datadir}/glib-2.0/schemas/org.gnome.Evince.gschema.xml
%{_datadir}/icons/hicolor/*/apps/evince.png
%{_datadir}/locale/*/LC_MESSAGES/evince.mo
%{_datadir}/thumbnailers/evince.thumbnailer

%files doc
%defattr(-,root,root,-)
%{_datadir}/gtk-doc/html/evince/*
%{_datadir}/gtk-doc/html/libevdocument-3.0/*
%{_datadir}/gtk-doc/html/libevview-3.0/*
%{_datadir}/help/*/evince/*
%{_mandir}/man1/evince.1.gz

%post
gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
glib-compile-schemas %{_datadir}/glib-2.0/schemas &>/dev/null || :

%clean
rm -rf %{buildroot}

%changelog
* Sat Oct 17 2015 tanggeliang <tanggeliang@gmail.com>
- update from 3.14.0 to 3.18.0
* Sun Sep 28 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.12.1 to 3.14.0
* Wed Jul 23 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.12.0 to 3.12.1
* Sat Mar 29 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.11.90 to 3.12.0
* Sun Mar 16 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.10.3 to 3.11.90
* Wed Dec 4 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.10.0 to 3.10.3
* Wed Oct 9 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.8.3 to 3.10.0
* Fri Jul 26 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.8.2 to 3.8.3
* Tue Jun 25 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.8.0 to 3.8.2
* Fri May 10 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.1 to 3.8.0
* Fri Nov 2 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.0 to 3.6.1
* Sat Sep 29 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.4.0 to 3.6.0
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
