Name:       gnome-music
Version:    3.12.2
Release:    1%{?dist}
Summary:    GNOME Music

Group:      User Interface/GNOME
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/%{name}/3.12/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  python3 gobject-introspection gtk+
BuildRequires:  grilo itstool
BuildRequires:  intltool gettext xml-parser

%description
Music is the new GNOME music playing application.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/gnome-music
/usr/lib/python3.3/site-packages/gnomemusic/*.py
/usr/lib/python3.3/site-packages/gnomemusic/__pycache__/*.py*
%{_libdir}/gnome-music/girepository-1.0/Gd-1.0.typelib
%{_libdir}/gnome-music/libgd.la
%{_libdir}/gnome-music/libgd.so
%{_datadir}/appdata/gnome-music.appdata.xml
%{_datadir}/applications/gnome-music.desktop
%{_datadir}/glib-2.0/schemas/org.gnome.Music.gschema.xml
%{_datadir}/help/*/gnome-music/*
%{_datadir}/gnome-music/gir-1.0/Gd-1.0.gir
%{_datadir}/gnome-music/gnome-music.gresource
%{_datadir}/icons/hicolor/*x*/apps/gnome-music.png
%{_datadir}/locale/*/LC_MESSAGES/gnome-music.mo

%post
gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
glib-compile-schemas %{_datadir}/glib-2.0/schemas &>/dev/null || :

%clean
rm -rf %{buildroot}

%changelog
* Sun Jul 27 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.10.0 to 3.12.2
* Sat Sep 28 2013 tanggeliang <tanggeliang@gmail.com>
- create
