Name:       gnome-system-monitor
Version:    3.17.92
Release:    1%{?dist}
Summary:    GNOME System Monitor

Group:      User Interface/GNOME
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/%{name}/3.17/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  gnome-icon-theme gtkmm libgtop librsvg libwnck yelp-xsl
BuildRequires:  intltool gettext xml-parser libxslt docbook-xml itstool
Requires:       lsb_release

%description
The GNOME System Monitor package contains GNOME's replacement for gtop.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --libexecdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/gnome-system-monitor
%{_libdir}/gnome-system-monitor/gsm-kill
%{_libdir}/gnome-system-monitor/gsm-renice
%{_datadir}/appdata/gnome-system-monitor.appdata.xml
%{_datadir}/applications/gnome-system-monitor.desktop
%{_datadir}/applications/gnome-system-monitor-kde.desktop
%{_datadir}/glib-2.0/schemas/org.gnome.gnome-system-monitor.*.xml
%{_datadir}/locale/*/LC_MESSAGES/gnome-system-monitor.mo
%{_datadir}/polkit-1/actions/org.gnome.gnome-system-monitor.policy

%files doc
%defattr(-,root,root,-)
%{_datadir}/help/*/gnome-system-monitor/*

%clean
rm -rf %{buildroot}

%post
gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
glib-compile-schemas %{_datadir}/glib-2.0/schemas &>/dev/null || :

%changelog
* Sun Sep 20 2015 tanggeliang <tanggeliang@gmail.com>
- update from 3.14.0 to 3.17.92
* Sun Sep 28 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.12.2 to 3.14.0
* Sun Jul 20 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.12.1 to 3.12.2
* Sun Apr 20 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.12.0 to 3.12.1
* Sat Mar 29 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.11.90 to 3.12.0
* Mon Mar 17 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.10.2 to 3.11.90
* Wed Dec 4 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.10.0 to 3.10.2
* Thu Sep 26 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.8.2.1 to 3.10.0
* Sat Aug 17 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.8.2 to 3.8.2.1
* Sun Jun 9 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.8.0 to 3.8.2
* Sat May 11 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.1 to 3.8.0
* Fri Nov 2 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.0 to 3.6.1
* Mon Oct 1 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.4.1 to 3.6.0
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
