Name:       gnome-color-manager
Version:    3.14.0
Release:    1%{?dist}
Summary:    GNOME Color Manager

Group:      User Interface/GNOME
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/%{name}/3.14/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  colord-gtk gnome-desktop lcms2 libcanberra
BuildRequires:  exiv libexif vte
BuildRequires:  clutter-gtk
BuildRequires:  intltool gettext xml-parser
BuildRequires:  libxslt itstool

%description
GNOME Color Manager is a session framework for the GNOME desktop environment that makes it easy to manage, install and generate color profiles.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --libexecdir=%{_libdir}/gnome-color-manager \
            --disable-packagekit &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/gcm-calibrate
%{_bindir}/gcm-import
%{_bindir}/gcm-inspect
%{_bindir}/gcm-picker
%{_bindir}/gcm-viewer
%{_libdir}/gnome-color-manager/gcm-helper-exiv
%{_datadir}/appdata/gcm-viewer.appdata.xml
%{_datadir}/applications/gcm-*.desktop
%{_datadir}/gnome-color-manager/figures/viewer-example-0*.png
%{_datadir}/gnome-color-manager/icons/*.svg
%{_datadir}/gnome-color-manager/targets/*.png
%{_datadir}/gnome-color-manager/ti1/*.ti1
%{_datadir}/icons/hicolor/*
%{_datadir}/locale/*/LC_MESSAGES/gnome-color-manager.mo

%files doc
%defattr(-,root,root,-)
%doc
%{_datadir}/help/*/gnome-color-manager/*

%post
gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :

%clean
rm -rf %{buildroot}

%changelog
* Fri Sep 26 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.12.3 to 3.14.0
* Sat Jul 19 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.11.90 to 3.12.3
* Sun Mar 16 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.10.1 to 3.11.90
* Tue Oct 22 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.10.0 to 3.10.1
* Wed Sep 25 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.8.2 to 3.10.0
* Sun Jun 9 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.8.1 to 3.8.2
* Sat May 11 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.1 to 3.8.1
* Tue Mar 19 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.0 to 3.6.1
* Mon Oct 1 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.4.2 to 3.6.0
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
