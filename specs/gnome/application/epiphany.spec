Name:       epiphany
Version:    3.12.0
Release:    1%{?dist}
Summary:    Epiphany

Group:      User Interface/GNOME
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/%{name}/3.12/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  avahi gcr gnome-desktop iso-codes libgnome-keyring libnotify webkitgtk
BuildRequires:  gobject-introspection nss
BuildRequires:  gtk-doc intltool gettext xml-parser docbook-xml
BuildRequires:  gsettings-desktop-schemas
BuildRequires:  libwnck webkitgtk-webkit2 itstool

%description
Epiphany is a simple yet powerful GNOME web browser targeted at non-technical users.
Its principles are simplicity and standards compliance.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --libdir=%{_libdir} \
            --libexecdir=%{_libdir}/epiphany &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/ephy-profile-migrator
%{_bindir}/epiphany
%{_libdir}/epiphany/3.12/web-extensions/libephywebextension.la
%{_libdir}/epiphany/3.12/web-extensions/libephywebextension.so
%{_libdir}/epiphany/epiphany-search-provider
%{_datadir}/GConf/gsettings/epiphany.convert
%{_datadir}/appdata/epiphany.appdata.xml
%{_datadir}/applications/epiphany.desktop
%{_datadir}/dbus-1/services/org.gnome.Epiphany.service
%{_datadir}/epiphany/*
%{_datadir}/glib-2.0/schemas/org.gnome.*.xml
%{_datadir}/gnome-shell/search-providers/epiphany-search-provider.ini
%{_datadir}/locale/*/LC_MESSAGES/epiphany.mo

%files doc
%defattr(-,root,root,-)
%{_datadir}/help/*/epiphany/*
%{_mandir}/man1/epiphany.1.gz

%post
glib-compile-schemas %{_datadir}/glib-2.0/schemas &>/dev/null || :

%clean
rm -rf %{buildroot}

%changelog
* Sat Mar 29 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.11.91 to 3.12.0
* Sun Mar 16 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.10.3 to 3.11.91
* Sun Dec 15 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.10.2 to 3.10.3
* Wed Dec 4 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.10.1 to 3.10.2
* Mon Oct 21 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.10.0 to 3.10.1
* Thu Oct 3 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.8.2 to 3.10.0
* Sat Aug 3 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.8.1 to 3.8.2
* Sat May 11 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.1 to 3.8.1
* Sat Oct 27 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.0 to 3.6.1
* Sun Sep 30 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.4.3 to 3.6.0
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
