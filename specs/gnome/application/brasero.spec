Name:       brasero
Version:    3.11.3
Release:    1%{?dist}
Summary:    Brasero

Group:      User Interface/GNOME
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/%{name}/3.11/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  gst-plugins-base libcanberra libnotify yelp-xsl
BuildRequires:  gobject-introspection libburn libisofs nautilus totem-pl-parser
BuildRequires:  gtk-doc intltool gettext xml-parser nettle itstool
Requires:       dvd-rw-tools gvfs
#Requires:       cdrdao cdrkit cdrtools libdvdcss

%description
Brasero is an application used to burn CD/DVD on the GNOME Desktop.
It is designed to be as simple as possible and has some unique features that enable users to create their discs easily and quickly.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/brasero
%{_includedir}/brasero3/brasero-*.h
%{_libdir}/brasero3/plugins/libbrasero-*.la
%{_libdir}/brasero3/plugins/libbrasero-*.so
%{_libdir}/girepository-1.0/BraseroBurn-3.1.typelib
%{_libdir}/girepository-1.0/BraseroMedia-3.1.typelib
%{_libdir}/libbrasero-burn3.*
%{_libdir}/libbrasero-media3.*
%{_libdir}/libbrasero-utils3.*
%{_libdir}/nautilus/extensions-3.0/libnautilus-brasero-extension.*
%{_libdir}/pkgconfig/libbrasero-burn3.pc
%{_libdir}/pkgconfig/libbrasero-media3.pc
%{_datadir}/GConf/gsettings/brasero.convert
%{_datadir}/appdata/brasero.appdata.xml
%{_datadir}/applications/brasero-nautilus.desktop
%{_datadir}/applications/brasero.desktop
%ifarch %{ix86}
%{_datadir}/brasero/icons/hicolor/*/actions/*
%{_datadir}/brasero/icons/hicolor/*/status/*
%{_datadir}/icons/hicolor/*/apps/brasero.png
%else %ifarch x86_64
%{_datadir}/brasero/icons/hicolor/hicolor_*.png
%{_datadir}/brasero/icons/hicolor/hicolor_*.svg
%{_datadir}/icons/hicolor/hicolor_apps_*_brasero.png
%endif
%{_datadir}/gir-1.0/BraseroBurn-3.1.gir
%{_datadir}/gir-1.0/BraseroMedia-3.1.gir
%{_datadir}/glib-2.0/schemas/org.gnome.brasero.gschema.xml
%{_datadir}/locale/*/LC_MESSAGES/brasero.mo
%{_datadir}/mime/packages/brasero.xml

%files doc
%defattr(-,root,root,-)
%{_datadir}/gtk-doc/html/libbrasero-burn/*
%{_datadir}/gtk-doc/html/libbrasero-media/*
%{_datadir}/help/*/brasero/*
%{_mandir}/man1/brasero.1.gz

%post
gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
glib-compile-schemas %{_datadir}/glib-2.0/schemas &>/dev/null || :

%clean
rm -rf %{buildroot}

%changelog
* Sun Mar 16 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.10.0 to 3.11.3
* Wed Dec 4 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.8.0 to 3.10.0
* Sat May 11 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.1 to 3.8.0
* Tue Mar 19 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.0 to 3.6.1
* Sun Sep 30 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.4.1 to 3.6.0
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
