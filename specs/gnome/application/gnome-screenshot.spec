Name:       gnome-screenshot
Version:    3.14.0
Release:    1%{?dist}
Summary:    GNOME Screenshot

Group:      User Interface/GNOME
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/%{name}/3.14/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  gtk+ libcanberra
BuildRequires:  intltool gettext xml-parser

%description
The GNOME Screenshot is an utility used for taking screenshots of the entire screen, a window or an user- defined area of the screen, with optional beautifying border effects.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/gnome-screenshot
%{_datadir}/GConf/gsettings/gnome-screenshot.convert
%{_datadir}/applications/org.gnome.Screenshot.desktop
%{_datadir}/dbus-1/services/org.gnome.Screenshot.service
%{_datadir}/glib-2.0/schemas/org.gnome.gnome-screenshot.gschema.xml
%{_datadir}/locale/*/LC_MESSAGES/gnome-screenshot.mo
%{_mandir}/man1/gnome-screenshot.1.gz

%post
glib-compile-schemas %{_datadir}/glib-2.0/schemas &>/dev/null || :

%clean
rm -rf %{buildroot}

%changelog
* Sun Sep 28 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.12.0 to 3.14.0
* Sat Mar 29 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.11.90 to 3.12.0
* Mon Mar 17 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.10.1 to 3.11.90
* Wed Dec 4 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.10.0 to 3.10.1
* Thu Sep 26 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.8.2 to 3.10.0
* Sun Jun 9 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.0 to 3.8.2
* Mon Oct 1 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.4.1 to 3.6.0
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
