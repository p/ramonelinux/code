Name:       gnome-disk-utility
Version:    3.17.91
Release:    1%{?dist}
Summary:    GNOME Disk Utility

Group:      User Interface/GNOME
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/%{name}/3.17/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  gtk+ intltool libsecret libpwquality udisks
BuildRequires:  gettext xml-parser libxslt
BuildRequires:  docbook-xml docbook-xsl
BuildRequires:  libcanberra libdvdread gnome-settings-daemon

%description
The GNOME Disk Utility package provides applications used for dealing with storage devices.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/gnome-disk-image-mounter
%{_bindir}/gnome-disks
%{_libdir}/gnome-settings-daemon-3.0/gdu-sd-plugin.gnome-settings-plugin
%{_libdir}/gnome-settings-daemon-3.0/libgdu-sd.*a
%{_libdir}/gnome-settings-daemon-3.0/libgdu-sd.so
%{_datadir}/appdata/org.gnome.DiskUtility.appdata.xml
%{_datadir}/applications/gnome-disk-image-mounter.desktop
%{_datadir}/applications/gnome-disk-image-writer.desktop
%{_datadir}/applications/org.gnome.DiskUtility.desktop
%{_datadir}/dbus-1/services/org.gnome.DiskUtility.service
%{_datadir}/glib-2.0/schemas/org.gnome.Disks.gschema.xml
%{_datadir}/glib-2.0/schemas/org.gnome.settings-daemon.plugins.gdu-sd.gschema.xml
%{_datadir}/icons/hicolor/*/apps/gnome-disks.png
%{_datadir}/icons/hicolor/scalable/apps/gnome-disks-*.svg
%{_datadir}/locale/*/LC_MESSAGES/gnome-disk-utility.mo
%{_mandir}/man1/gnome-disk*.1.gz

%post
gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
glib-compile-schemas %{_datadir}/glib-2.0/schemas &>/dev/null || :

%clean
rm -rf %{buildroot}

%changelog
* Sun Sep 20 2015 tanggeliang <tanggeliang@gmail.com>
- update from 3.12.1 to 3.17.91
* Sat Jul 26 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.10.0 to 3.12.1
* Thu Oct 10 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.9.0 to 3.10.0
* Thu Sep 26 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.8.2 to 3.9.0
* Sun Jun 9 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.8.0 to 3.8.2
- remove "Werror=format" in configure.
* Sat May 11 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.1 to 3.8.0
* Sun Oct 14 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.0 to 3.6.1
* Fri Sep 28 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.4.1 to 3.6.0
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
