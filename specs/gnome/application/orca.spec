Name:       orca
Version:    3.6.1
Release:    1%{?dist}
Summary:    Orca

Group:      User Interface/GNOME
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/%{name}/3.6/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  gtk+ pygobject yelp-xsl
BuildRequires:  py2cairo libxml
BuildRequires:  intltool gettext xml-parser itstool

%description
Orca enables users with limited or no vision to use the GNOME Desktop and applications effectively.
It provides a number of features, including magnification, focus tracking, braille output, automatic screen reading and more.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --sysconfdir=/etc &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
/etc/xdg/autostart/orca-autostart.desktop
%{_bindir}/orca
%{_libdir}/python2.7/site-packages/orca/*
%{_datadir}/applications/orca.desktop
%{_datadir}/icons/hicolor/*/apps/orca.png
%{_datadir}/icons/hicolor/scalable/apps/orca.svg
%{_datadir}/locale/*/LC_MESSAGES/orca.mo
%{_datadir}/orca/gfx/orca-splash.png
%{_datadir}/orca/ui/orca-*.ui

%files doc
%defattr(-,root,root,-)
%{_datadir}/help/*/orca/*
%{_mandir}/man1/orca.1.gz

%post
gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :

%clean
rm -rf %{buildroot}

%changelog
* Fri Nov 2 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.0 to 3.6.1
* Mon Oct 1 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.4.2 to 3.6.0
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
