Name:       gupnp
Version:    0.20.14
Release:    1%{?dist}
Summary:    GUPnP

Group:      User Interface/GNOME
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/%{name}/0.20/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  gssdp shared-mime-info gobject-introspection
BuildRequires:  libsoup

%description
GUPnP is an elegant, object-oriented open source framework for creating UPnP devices and control points, written in C using GObject and libsoup.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --disable-static \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/gupnp-binding-tool
%{_includedir}/gupnp-1.0/libgupnp/gupnp*.h
%{_libdir}/girepository-1.0/GUPnP-1.0.typelib
%{_libdir}/libgupnp-1.0.*
%{_libdir}/pkgconfig/gupnp-1.0.pc
%{_datadir}/gir-1.0/GUPnP-1.0.gir
%{_datadir}/gtk-doc/html/gupnp/*

%post

%clean
rm -rf %{buildroot}

%changelog
* Wed Sep 23 2015 tanggeliang <tanggeliang@gmail.com>
- update from 0.20.12 to 0.20.14
* Wed Sep 24 2014 tanggeliang <tanggeliang@gmail.com>
- update from 0.18.3 to 0.20.12
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
