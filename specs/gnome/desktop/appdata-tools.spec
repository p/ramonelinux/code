Name:       appdata-tools
Version:    0.1.8
Release:    1%{?dist}
Summary:    Tools for AppData files

Group:      User Interface/GNOME
License:    GPLv2+
Url:        http://www.freedesktop.org
Source:     http://people.freedesktop.org/~hughsient/releases/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  gettext intltool libxslt docbook-xsl glib
BuildRequires:  xml-parser appstream appstream-glib

%description
appdata-tools contains a command line program designed to validate AppData application descriptions for standards compliance and to the style guide.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/appdata-validate
%{_datadir}/aclocal/appdata-xml.m4
%{_datadir}/appdata/schema/appdata.rnc
%{_datadir}/appdata/schema/appdata.xsd
%{_datadir}/appdata/schema/schema-locating-rules.xml
%{_datadir}/emacs/site-lisp/site-start.d/appdata-rng-init.el
%{_datadir}/locale/en_GB/LC_MESSAGES/appdata-tools.mo

%clean
rm -rf %{buildroot}

%changelog
* Fri Mar 28 2014 tanggeliang <tanggeliang@gmail.com>
- create
