Name:       gnome-icon-theme-symbolic
Version:    3.12.0
Release:    1%{?dist}
Summary:    GNOME Icon Theme Symbolic

Group:      User Interface/GNOME
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/%{name}/3.12/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildArch:      noarch
BuildRequires:  gtk+ icon-naming-utils
Requires:       gnome-icon-theme

%description
The GNOME Icon Theme Symbolic package contains symbolic icons for the default GNOME icon theme.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_datadir}/icons/gnome/scalable/*
%{_datadir}/pkgconfig/gnome-icon-theme-symbolic.pc

%clean
rm -rf %{buildroot}

%changelog
* Fri Mar 28 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.11.91 to 3.12.0
* Sat Mar 15 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.10.1 to 3.11.91
* Wed Oct 23 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.10.0 to 3.10.1
* Tue Sep 24 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.8.3 to 3.10.0
* Sun Aug 18 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.8.2.2 to 3.8.3
* Sun Jun 9 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.8.0.1 to 3.8.2.2
* Sat Apr 13 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.2 to 3.8.0.1
* Mon Nov 26 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.0 to 3.6.2
* Fri Sep 28 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.4.0 to 3.6.0
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
