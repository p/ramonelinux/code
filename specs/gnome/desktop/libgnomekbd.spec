Name:       libgnomekbd
Version:    3.6.0
Release:    4%{?dist}
Summary:    libgnomekbd

Group:      User Interface/GNOME
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/%{name}/3.6/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  gtk+ intltool libxklavier
BuildRequires:  gobject-introspection
BuildRequires:  gettext xml-parser

%description
The libgnomekbd package contains xkb hooks used by the GNOME Desktop.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --disable-static \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/gkbd-keyboard-display
%{_includedir}/libgnomekbd/*
%{_libdir}/libgnomekbd.so
%{_libdir}/libgnomekbdui.so
%{_libdir}/girepository-1.0/Gkbd-3.0.typelib
%{_libdir}/libgnomekbd*.*
%{_libdir}/pkgconfig/libgnomekbd*.pc
%{_datadir}/GConf/gsettings/libgnomekbd.convert
%{_datadir}/applications/gkbd-keyboard-display.desktop
%{_datadir}/gir-1.0/Gkbd-3.0.gir
%{_datadir}/glib-2.0/schemas/org.gnome.libgnomekbd.*.xml
%{_datadir}/libgnomekbd/*
%{_datadir}/locale/*

%clean
rm -rf %{buildroot}

%changelog
* Fri Sep 28 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.4.0.2 to 3.6.0
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
