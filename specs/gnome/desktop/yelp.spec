Name:       yelp
Version:    3.16.0
Release:    1%{?dist}
Summary:    Yelp

Group:      User Interface/GNOME
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/%{name}/3.16/%{name}-%{version}.tar.xz
Patch:	    %{name}-%{version}-webkitgtk.diff

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  webkitgtk yelp-xsl
BuildRequires:  gtk-doc intltool gettext xml-parser itstool

%description
The Yelp package contains the help browser used for viewing help files.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}
%patch -p1

%build
./configure --prefix=/usr --disable-static \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/gnome-help
%{_bindir}/yelp
%{_includedir}/libyelp/yelp-*.h
%{_libdir}/libyelp.*
%{_datadir}/applications/yelp.desktop
%{_datadir}/glib-2.0/schemas/org.gnome.yelp.gschema.xml
%{_datadir}/locale/*/LC_MESSAGES/yelp.mo
%{_datadir}/yelp/dtd/*
%{_datadir}/yelp/icons/hicolor/16x16/status/*.png
%{_datadir}/yelp/icons/hicolor/scalable/actions/*.svg
%{_datadir}/yelp/icons/hicolor/scalable/status/*.svg
%{_datadir}/yelp/xslt/*.xsl
%{_datadir}/yelp-xsl/xslt/common/domains/yelp.xml
%{_datadir}/yelp/mathjax/*

%files doc
%defattr(-,root,root,-)
%{_datadir}/gtk-doc/html/libyelp/*

%clean
rm -rf %{buildroot}

%changelog
* Sun Mar 29 2015 tanggeliang <tanggeliang@gmail.com>
- update from 3.14.0 to 3.16.0
* Sun Sep 28 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.12.0 to 3.14.0
* Fri Mar 28 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.11.91 to 3.12.0
* Sat Mar 15 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.10.1 to 3.11.91
* Mon Oct 21 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.10.0 to 3.10.1
* Tue Sep 24 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.8.1 to 3.10.0
* Sat Aug 17 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.8.0 to 3.8.1
* Sun Apr 14 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.2 to 3.8.0
* Mon Nov 26 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.1 to 3.6.2
* Mon Oct 22 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.0 to 3.6.1
* Sun Sep 30 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.4.2 to 3.6.0
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
