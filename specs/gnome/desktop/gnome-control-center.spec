Name:       gnome-control-center
Version:    3.18.0
Release:    1%{?dist}
Summary:    GNOME Control Center

Group:      User Interface/GNOME
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/%{name}/3.18/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  gnome-menus gnome-online-accounts gnome-settings-daemon iso-codes libgtop libpwquality krb
BuildRequires:  accountsservice consolekit
BuildRequires:  cups gnome-bluetooth ibus network-manager-applet modemmanager
BuildRequires:  intltool gettext xml-parser upower colord
BuildRequires:  libgnomekbd libxklavier libcanberra pulseaudio libwacom glib
BuildRequires:  docbook-xml docbook-xsl
BuildRequires:  gobject-introspection colord-gtk grilo
BuildRequires:  clutter-gtk samba

%description
The GNOME Control Center package contains the GNOME settings manager.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --sysconfdir=/etc \
            --disable-static \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/gnome-control-center
/usr/libexec/cc-remote-login-helper
/usr/libexec/gnome-control-center-search-provider
%{_datadir}/appdata/gnome-control-center.appdata.xml
%{_datadir}/applications/*
%{_datadir}/bash-completion/completions/gnome-control-center
%{_datadir}/dbus-1/services/org.gnome.ControlCenter.SearchProvider.service
%{_datadir}/dbus-1/services/org.gnome.ControlCenter.service
%{_datadir}/gnome-control-center/*
%{_datadir}/gnome-shell/search-providers/gnome-control-center-search-provider.ini
%{_datadir}/icons/*
%{_datadir}/locale/*
%{_datadir}/pixmaps/faces/*.jpg
%{_datadir}/pixmaps/faces/*.png
%{_datadir}/pkgconfig/gnome-keybindings.pc
%{_datadir}/polkit-1/actions/org.gnome.controlcenter.datetime.policy
%{_datadir}/polkit-1/actions/org.gnome.controlcenter.remote-login-helper.policy
%{_datadir}/polkit-1/actions/org.gnome.controlcenter.user-accounts.policy
%{_datadir}/polkit-1/rules.d/gnome-control-center.rules
%{_datadir}/sounds/gnome/default/alerts/*.ogg

%files doc
%defattr(-,root,root,-)
%{_mandir}/man1/gnome-control-center.1.gz

%post
gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :

%postun

%clean
rm -rf %{buildroot}

%changelog
* Wed Sep 23 2015 tanggeliang <tanggeliang@gmail.com>
- update from 3.17.92 to 3.18.0
* Wed Sep 16 2015 tanggeliang <tanggeliang@gmail.com>
- update from 3.16.0 to 3.17.92
* Sat Mar 28 2015 tanggeliang <tanggeliang@gmail.com>
- update from 3.14.0 to 3.16.0
* Wed Sep 24 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.12.1 to 3.14.0
* Sat Jul 19 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.12.0 to 3.12.1
* Fri Mar 28 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.11.92 to 3.12.0
* Fri Mar 21 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.11.91 to 3.11.92
* Sat Mar 15 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.10.2 to 3.11.91
* Mon Jan 6 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.10.1 to 3.10.2
* Mon Oct 21 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.10.0 to 3.10.1
* Tue Sep 24 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.8.4 to 3.10.0
* Wed Jul 24 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.8.1 to 3.8.4
* Sat May 11 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.8.0 to 3.8.1
* Sat Apr 13 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.3 to 3.8.0
* Mon Nov 26 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.2 to 3.6.3
* Sat Oct 27 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.0 to 3.6.2
* Fri Sep 28 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.4.2 to 3.6.0
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
