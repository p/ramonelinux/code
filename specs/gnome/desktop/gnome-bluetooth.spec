Name:       gnome-bluetooth
Version:    3.18.0
Release:    1%{?dist}
Summary:    GNOME Bluetooth

Group:      User Interface/GNOME
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/%{name}/3.18/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  libnotify yelp-xsl libcanberra
BuildRequires:  gobject-introspection nautilus-sendto
BuildRequires:  gtk-doc intltool gettext xml-parser dbus
BuildRequires:  docbook-xml itstool desktop-file-utils gtk+
Requires:       bluez consolekit obexd

%description
The GNOME Bluetooth package contains tools for managing and manipulating Bluetooth devices using the GNOME desktop.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --sysconfdir=/etc \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

mkdir -p %{buildroot}/lib/udev/rules.d
cat > %{buildroot}/lib/udev/rules.d/61-gnome-bluetooth.rules << "EOF"
# Get access to /dev/rfkill for users
# See https://bugzilla.redhat.com/show_bug.cgi?id=514798
#
# Updated for udev >= 154
# http://bugs.debian.org/582188
# https://bugzilla.redhat.com/show_bug.cgi?id=588660

ENV{ACL_MANAGE}=="0", GOTO="gnome_bluetooth_end"
ACTION!="add|change", GOTO="gnome_bluetooth_end"
KERNEL=="rfkill", TAG+="udev-acl", MODE="0666"
LABEL="gnome_bluetooth_end"
EOF

%files
%defattr(-,root,root,-)
%{_bindir}/bluetooth-sendto
%{_includedir}/gnome-bluetooth/bluetooth-*.h
/lib/udev/rules.d/61-gnome-bluetooth.rules
%{_libdir}/girepository-1.0/GnomeBluetooth-1.0.typelib
%{_libdir}/libgnome-bluetooth.*
%{_libdir}/pkgconfig/gnome-bluetooth-1.0.pc
%{_datadir}/applications/bluetooth-sendto.desktop
%{_datadir}/applications/mimeinfo.cache
%{_datadir}/gir-1.0/GnomeBluetooth-1.0.gir
%{_datadir}/gnome-bluetooth/pin-code-database.xml
%{_datadir}/icons/hicolor/16x16/*
%{_datadir}/icons/hicolor/22x22/*
%{_datadir}/icons/hicolor/24x24/*
%{_datadir}/icons/hicolor/32x32/*
%{_datadir}/icons/hicolor/48x48/*
%{_datadir}/icons/hicolor/icon-theme.cache
%{_datadir}/icons/hicolor/scalable/apps/bluetooth.svg
%{_datadir}/icons/hicolor/scalable/status/bluetooth-paired.svg
%{_datadir}/locale/*

%files doc
%defattr(-,root,root,-)
%{_datadir}/gtk-doc/html/gnome-bluetooth/*
%{_mandir}/man1/*

%post
gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
glib-compile-schemas %{_datadir}/glib-2.0/schemas &>/dev/null || :

%postun

%clean
rm -rf %{buildroot}

%changelog
* Wed Sep 23 2015 tanggeliang <tanggeliang@gmail.com>
- update from 3.17.92 to 3.18.0
* Sun Sep 20 2015 tanggeliang <tanggeliang@gmail.com>
- update from 3.10.0 to 3.17.92
* Thu Sep 26 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.8.1 to 3.10.0
* Sun Jun 9 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.8.0 to 3.8.1
* Sat Apr 13 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.1 to 3.8.0
* Mon Nov 26 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.0 to 3.6.1
* Fri Sep 28 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.4.2 to 3.6.0
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
