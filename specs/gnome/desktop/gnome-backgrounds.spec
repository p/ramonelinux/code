Name:       gnome-backgrounds
Version:    3.18.0
Release:    1%{?dist}
Summary:    GNOME Backgrounds

Group:      User Interface/GNOME
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/%{name}/3.18/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildArch:      noarch
BuildRequires:  intltool
BuildRequires:  gettext xml-parser

%description
The GNOME Backgrounds package contains a collection of graphics files which can be used as backgrounds in the GNOME Desktop environment.
Additionally, the package creates the proper framework and directory structure so that you can add your own files to the collection.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_datadir}/gnome-background-properties/adwaita.xml
%{_datadir}/gnome-background-properties/gnome-*.xml
%{_datadir}/locale/*
%{_datadir}/backgrounds/gnome/*.jpg
%{_datadir}/backgrounds/gnome/*.png
%{_datadir}/backgrounds/gnome/adwaita-timed.xml

%clean
rm -rf %{buildroot}

%changelog
* Wed Sep 23 2015 tanggeliang <tanggeliang@gmail.com>
- update from 3.16.0 to 3.18.0
* Sun Mar 29 2015 tanggeliang <tanggeliang@gmail.com>
- update from 3.14.0 to 3.16.0
* Wed Sep 24 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.10.0 to 3.14.0
* Thu Sep 26 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.8.1 to 3.10.0
* Sat May 11 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.8.0 to 3.8.1
* Sat Apr 13 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.1 to 3.8.0
* Sat Oct 27 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.0 to 3.6.1
* Fri Sep 28 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.4.2 to 3.6.0
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
