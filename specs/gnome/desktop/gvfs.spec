Name:       gvfs
Version:    1.26.0
Release:    1%{?dist}
Summary:    Gvfs

Group:      User Interface/GNOME
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/%{name}/1.26/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  dbus glib intltool gcr
BuildRequires:  gtk+ libsecret libsoup udisks
BuildRequires:  dbus-glib libarchive libgcrypt systemd gnome-online-accounts
BuildRequires:  gettext xml-parser libxslt docbook-xml docbook-xsl

%description
The Gvfs package is an userspace virtual filesystem designed to work with the I/O abstractions of GLib's GIO library.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --sysconfdir=/etc \
            --libexecdir=%{_libdir}/gvfs \
            --disable-gphoto2 \
            --enable-libsystemd-login \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/gvfs-cat
%{_bindir}/gvfs-copy
%{_bindir}/gvfs-info
%{_bindir}/gvfs-less
%{_bindir}/gvfs-ls
%{_bindir}/gvfs-mime
%{_bindir}/gvfs-mkdir
%{_bindir}/gvfs-monitor-dir
%{_bindir}/gvfs-monitor-file
%{_bindir}/gvfs-mount
%{_bindir}/gvfs-move
%{_bindir}/gvfs-open
%{_bindir}/gvfs-rename
%{_bindir}/gvfs-rm
%{_bindir}/gvfs-save
%{_bindir}/gvfs-set-attribute
%{_bindir}/gvfs-trash
%{_bindir}/gvfs-tree
%{_includedir}/gvfs-client/gvfs/gvfsuri*.h
%{_libdir}/gio/modules/libgioremote-volume-monitor.*
%{_libdir}/gio/modules/libgvfsdbus.*
%{_libdir}/gvfs/gvfs-goa-volume-monitor
%{_libdir}/gvfs/gvfs-udisks2-volume-monitor
%{_libdir}/gvfs/gvfsd
%{_libdir}/gvfs/gvfsd-*
%{_libdir}/gvfs/libgvfscommon.*
%{_libdir}/gvfs/libgvfsdaemon.*
%{_datadir}/bash-completion/completions/gvfs-*
%{_datadir}/dbus-1/services/gvfs-*.service
%{_datadir}/dbus-1/services/org.gtk.vfs.*.service
%{_datadir}/glib-2.0/schemas/org.gnome.system.gvfs.enums.xml
%{_datadir}/gvfs/mounts/*.mount
%{_datadir}/gvfs/remote-volume-monitors/goa.monitor
%{_datadir}/gvfs/remote-volume-monitors/udisks2.monitor
%{_datadir}/locale/*/LC_MESSAGES/gvfs.mo
%{_mandir}/man*/gvfs*.*.gz

%post
glib-compile-schemas %{_datadir}/glib-2.0/schemas &>/dev/null || :

%postun

%clean
rm -rf %{buildroot}

%changelog
* Wed Sep 23 2015 tanggeliang <tanggeliang@gmail.com>
- update from 1.25.90 to 1.26.0
* Sun Mar 29 2015 tanggeliang <tanggeliang@gmail.com>
- update from 1.21.4 to 1.24.0
* Sat Jul 19 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.20.0 to 1.21.4
* Thu Mar 27 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.19.90 to 1.20.0
* Sat Mar 15 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.19.3 to 1.19.90
* Mon Jan 6 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.18.3 to 1.19.3
* Tue Dec 3 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.18.2 to 1.18.3
* Tue Oct 22 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.18.1 to 1.18.2
* Thu Sep 26 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.18.0 to 1.18.1
* Tue Sep 24 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.17.3 to 1.18.0
* Wed Jul 24 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.17.1 to 1.17.3
* Thu May 30 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.17.0 to 1.17.1
* Sat May 11 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.16.0 to 1.17.0
* Sat Apr 13 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.14.2 to 1.16.0
* Mon Nov 26 2012 tanggeliang <tanggeliang@gmail.com>
- update from 1.14.1 to 1.14.2
* Sun Oct 28 2012 tanggeliang <tanggeliang@gmail.com>
- update from 1.14.0 to 1.14.1
* Fri Sep 28 2012 tanggeliang <tanggeliang@gmail.com>
- update from 1.12.3 to 1.14.0
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
