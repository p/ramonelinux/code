Name:       gnome-icon-theme-extras
Version:    3.12.0
Release:    1%{?dist}
Summary:    GNOME Icon Theme Extras

Group:      User Interface/GNOME
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/%{name}/3.12/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildArch:      noarch
BuildRequires:  gnome-icon-theme
BuildRequires:  icon-naming-utils

%description
The GNOME Icon Theme Extras package contains extra icons for the GNOME Desktop.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_datadir}/icons/gnome/*

%clean
rm -rf %{buildroot}

%changelog
* Fri Mar 28 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.11.2 to 3.12.0
* Sat Mar 15 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.2 to 3.11.2
* Mon Nov 26 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.4.0 to 3.6.2
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
