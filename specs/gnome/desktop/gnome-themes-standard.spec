Name:       gnome-themes-standard
Version:    3.18.0
Release:    1%{?dist}
Summary:    GNOME Themes Standard

Group:      User Interface/GNOME
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/%{name}/3.18/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  gtk+ intltool librsvg
BuildRequires:  gettext xml-parser gtk2

%description
The GNOME Themes Standard package contains various components of the default GNOME 3 theme.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_libdir}/gtk-2.0/2.10.0/engines/libadwaita.la
%{_libdir}/gtk-2.0/2.10.0/engines/libadwaita.so
%{_datadir}/icons/HighContrast/*
%{_datadir}/themes/Adwaita/*
%{_datadir}/themes/HighContrast/*

%clean
rm -rf %{buildroot}

%changelog
* Wed Sep 23 2015 tanggeliang <tanggeliang@gmail.com>
- update from 3.14.0 to 3.18.0
* Wed Sep 24 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.12.0 to 3.14.0
* Fri Mar 28 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.11.90 to 3.12.0
* Sat Mar 15 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.10.0 to 3.11.90
* Tue Sep 24 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.8.3 to 3.10.0
* Sun Aug 18 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.8.2 to 3.8.3
* Wed Jul 24 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.8.1 to 3.8.2
* Sat May 11 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.8.0 to 3.8.1
* Sat Apr 13 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.5 to 3.8.0
* Sun Mar 17 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.2 to 3.6.5
* Mon Nov 26 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.1 to 3.6.2
* Sat Oct 27 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.0.2 to 3.6.1
* Fri Sep 28 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.4.2 to 3.6.0.2
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
