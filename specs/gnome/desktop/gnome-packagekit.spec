Name:       gnome-packagekit
Version:    3.18.0
Release:    1%{?dist}
Summary:    gnome-packagekit

Group:      User Interface/GNOME
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/%{name}/3.18/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  PackageKit gtk+ libnotify libcanberra
BuildRequires:	intltool gettext xml-parser libxslt

%description

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/gpk-application
%{_bindir}/gpk-log
%{_bindir}/gpk-prefs
%{_bindir}/gpk-update-viewer
%{_datadir}/GConf/gsettings/org.gnome.packagekit.gschema.migrate
%{_datadir}/appdata/gpk-application.appdata.xml
%{_datadir}/appdata/gpk-update-viewer.appdata.xml
%{_datadir}/applications/gpk-*.desktop
%{_datadir}/glib-2.0/schemas/org.gnome.packagekit.gschema.xml
%{_datadir}/gnome-packagekit/gpk-*.ui
%{_datadir}/gnome-packagekit/icons/hicolor/*/*/pk-*
%{_datadir}/icons/hicolor/*x*/apps/gpk-*.png
%{_datadir}/icons/hicolor/*x*/mimetypes/application-x-*.png
%{_datadir}/icons/hicolor/scalable/apps/gpk-*.svg
%{_datadir}/icons/hicolor/scalable/mimetypes/application-x-*.svg
%{_datadir}/locale/*/LC_MESSAGES/gnome-packagekit.mo

%clean
rm -rf %{buildroot}

%changelog
* Wed Sep 23 2015 tanggeliang <tanggeliang@gmail.com>
- create
