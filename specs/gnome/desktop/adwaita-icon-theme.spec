Name:       adwaita-icon-theme
Version:    3.18.0
Release:    1%{?dist}
Summary:    adwaita icon theme

Group:      User Interface/GNOME
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/%{name}/3.18/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  gtk+ gnome-common hicolor-icon-theme
BuildRequires:  librsvg libpng libjpeg-turbo
BuildRequires:  intltool gettext xml-parser

%description
Adwaita icon theme.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_datadir}/icons/Adwaita/*
%{_datadir}/pkgconfig/adwaita-icon-theme.pc

%clean
rm -rf %{buildroot}

%changelog
* Wed Sep 23 2015 tanggeliang <tanggeliang@gmail.com>
- update from 3.17.4 to 3.18.0
* Fri Sep 26 2014 tanggeliang <tanggeliang@gmail.com>
- create
