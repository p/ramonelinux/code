Name:       gnome-user-docs
Version:    3.12.2
Release:    1%{?dist}
Summary:    GNOME User Docs

Group:      User Interface/GNOME
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/%{name}/3.12/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildArch:      noarch
BuildRequires:  yelp-xsl
BuildRequires:  itstool gettext libxml

%description
The GNOME User Docs package contains documentation for GNOME.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_datadir}/help/*/gnome-help/*
%{_datadir}/help/*/system-admin-guide/*

%clean
rm -rf %{buildroot}

%changelog
* Sat Jul 19 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.10.0 to 3.12.2
* Thu Sep 26 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.8.0 to 3.10.0
* Sun Apr 14 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.2 to 3.8.0
* Mon Nov 26 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.1 to 3.6.2
* Sat Oct 27 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.0 to 3.6.1
* Sun Sep 30 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.4.2 to 3.6.0
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
