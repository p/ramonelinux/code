Name:       gnome-menus
Version:    3.13.3
Release:    1%{?dist}
Summary:    GNOME Menus

Group:      User Interface/GNOME
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/%{name}/3.13/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  glib intltool gnome-common
BuildRequires:  gobject-introspection
BuildRequires:  gettext xml-parser

%description
The GNOME Menus package contains an implementation of the draft "Desktop Menu Specification" from freedesktop.org (http://www.freedesktop.org/Standards/menu-spec). 
Also contained are the GNOME menu layout configuration files, .directory files and a menu related utility program.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --sysconfdir=/etc \
            --disable-static \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
/etc/xdg/menus/gnome-applications.menu
%{_includedir}/gnome-menus-3.0/gmenu-tree.h
%{_libdir}/girepository-1.0/*
%{_libdir}/libgnome-menu-3.*
%{_libdir}/pkgconfig/libgnome-menu-3.0.pc
%{_datadir}/desktop-directories/*
%{_datadir}/gir-1.0/*
%{_datadir}/locale/*

%clean
rm -rf %{buildroot}

%changelog
* Sat Sep 26 2015 tanggeliang <tanggeliang@gmail.com>
- update from 3.10.1 to 3.13.3
* Sat Jul 19 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.10.0 to 3.10.1
* Tue Oct 8 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.8.0 to 3.10.0
* Sat Apr 13 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.1 to 3.8.0
* Mon Nov 26 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.0 to 3.6.1
* Fri Sep 28 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.4.2 to 3.6.0
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
