Name:       gnome-session
Version:    3.18.0
Release:    1%{?dist}
Summary:    GNOME Session

Group:      User Interface/GNOME
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/%{name}/3.18/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  gconf gtk+ json-glib upower
BuildRequires:  libsm mesa pango xtrans
BuildRequires:  intltool gettext xml-parser libxslt
BuildRequires:  gnome-desktop systemd docbook-xsl docbook-xml xserver-wayland
Requires:       consolekit polkit-gnome
Requires:       xinit dconf
Requires:       gconf gnome-control-center gnome-backgrounds gnome-desktop
Requires:       gnome-icon-theme-extras gnome-themes-standard
Requires:       gnome-settings-daemon notification-daemon
Requires:       icon-naming-utils hicolor-icon-theme nautilus desktop-file-utils

%description
The GNOME Session package contains the GNOME session manager.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
	    --libexecdir=%{_libdir}/gnome-session \
	    --enable-session-selector \
	    --enable-systemd &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/gnome-session
%{_bindir}/gnome-session-custom-session
%{_bindir}/gnome-session-inhibit
%{_bindir}/gnome-session-quit
%{_bindir}/gnome-session-selector
%{_datadir}/gnome-session/*
%{_datadir}/icons/*
%{_datadir}/locale/*
%{_datadir}/xsessions/*
%{_libdir}/gnome-session/gnome-session-check-accelerated
%{_libdir}/gnome-session/gnome-session-check-accelerated-helper
%{_libdir}/gnome-session/gnome-session-failed
%{_datadir}/GConf/gsettings/gnome-session.convert
%{_datadir}/glib-2.0/schemas/org.gnome.SessionManager.gschema.xml
%{_datadir}/wayland-sessions/gnome-wayland.desktop

%files doc
%defattr(-,root,root,-)
%{_mandir}/*/*

%clean
rm -rf %{buildroot}

%post
/sbin/ldconfig
touch --no-create %{_datadir}/icons/hicolor &>/dev/null || :
gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
glib-compile-schemas %{_datadir}/glib-2.0/schemas &>/dev/null || :
update-desktop-database %{_datadir}/applications &>/dev/null || :

%postun
/sbin/ldconfig
if [ $1 -eq 0 ] ; then
  touch --no-create %{_datadir}/icons/hicolor &>/dev/null || :
  gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
  glib-compile-schemas %{_datadir}/glib-2.0/schemas &>/dev/null || :
fi

%changelog
* Wed Sep 23 2015 tanggeliang <tanggeliang@gmail.com>
- update from 3.17.92 to 3.18.0
* Sun Sep 20 2015 tanggeliang <tanggeliang@gmail.com>
- update from 3.16.0 to 3.17.92
* Sun Mar 29 2015 tanggeliang <tanggeliang@gmail.com>
- update from 3.14.0 to 3.16.0
* Tue Sep 23 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.12.1 to 3.14.0
* Sat Jul 19 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.11.4 to 3.12.1
* Sun Mar 16 2014 tanggeliang <tanggeliang@gmail.com>
- updare from 3.10.1 to 3.11.4
* Tue Oct 22 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.10.0 to 3.10.1
* Wed Sep 25 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.8.4 to 3.10.0
* Wed Aug 7 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.8.2 to 3.8.4
* Sun Jun 9 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.8.1 to 3.8.2
- remove gnome-panel require.
* Sat May 11 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.8.0 to 3.8.1
* Sun Apr 14 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.2 to 3.8.0
* Mon Nov 26 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.1 to 3.6.2
* Sat Oct 27 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.0 to 3.6.1
* Sun Sep 30 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.4.2.1 to 3.6.0
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
