Name:       gnome-icon-theme
Version:    3.12.0
Release:    1%{?dist}
Summary:    GNOME Icon Theme

Group:      User Interface/GNOME
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/%{name}/3.12/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildArch:      noarch
BuildRequires:  gtk+ hicolor-icon-theme icon-naming-utils intltool
BuildRequires:  gettext xml-parser xml-simple

%description
The GNOME Icon Theme package contains an assortment of non-scalable icons of different sizes and themes.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_datadir}/icons/*
%{_datadir}/locale/*
%{_datadir}/pkgconfig/gnome-icon-theme.pc

%clean
rm -rf %{buildroot}

%changelog
* Fri Mar 28 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.11.5 to 3.12.0
* Sat Mar 15 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.10.0 to 3.11.5
* Tue Sep 24 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.8.3 to 3.10.0
* Sun Aug 18 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.8.2 to 3.8.3
* Sun Jun 9 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.8.0 to 3.8.2
* Sat Apr 13 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.2 to 3.8.0
* Mon Nov 26 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.0 to 3.6.2
* Fri Sep 28 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.4.0 to 3.6.0
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
