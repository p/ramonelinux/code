Name:       mutter
Version:    3.18.0
Release:    1%{?dist}
Summary:    Mutter - Window and compositing manager based on Clutter

Group:      User Interface/GNOME
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/%{name}/3.18/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  clutter gtk+ zenity clutter-wayland
BuildRequires:  libcanberra gobject-introspection startup-notification
BuildRequires:  rarian gsettings-desktop-schemas mesa
BuildRequires:  intltool gettext xml-parser libxkbcommon
BuildRequires:  upower gnome-desktop libxkbfile xkeyboard-config
BuildRequires:	libinput wayland xserver-wayland cogl

%description
Mutter is the window manager for GNOME.
It is not invoked directly, but from gnome-session (on a machine with a hardware accelerated video driver).

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}
%ifarch %{ix86}
sed -i 's/-Wsign-compare//'   configure
%endif

%build
./configure --prefix=/usr --disable-static \
            --libdir=%{_libdir} \
            --libexecdir=%{_libdir}/%{name} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/mutter
%{_includedir}/mutter/meta/*.h
%{_libdir}/libmutter.*
%{_libdir}/mutter/Meta-3.0.gir
%{_libdir}/mutter/Meta-3.0.typelib
%{_libdir}/mutter/plugins/default.so
%{_libdir}/pkgconfig/libmutter.pc
%{_libdir}/%{name}/mutter-restart-helper
%{_datadir}/GConf/gsettings/mutter-schemas.convert
%{_datadir}/applications/mutter-wayland.desktop
%{_datadir}/applications/mutter.desktop
%{_datadir}/glib-2.0/schemas/org.gnome.mutter.gschema.xml
%{_datadir}/glib-2.0/schemas/org.gnome.mutter.wayland.gschema.xml
%{_datadir}/gnome-control-center/keybindings/50-mutter-navigation.xml
%{_datadir}/gnome-control-center/keybindings/50-mutter-system.xml
%{_datadir}/gnome-control-center/keybindings/50-mutter-windows.xml
%{_datadir}/locale/*

%files doc
%defattr(-,root,root,-)
#%{_datadir}/gtk-doc/html/meta/*
%{_mandir}/man1/*

%post
glib-compile-schemas %{_datadir}/glib-2.0/schemas &>/dev/null || :

%clean
rm -rf %{buildroot}

%changelog
* Wed Sep 23 2015 tanggeliang <tanggeliang@gmail.com>
- update from 3.17.91 to 3.18.0
* Wed Sep 16 2015 tanggeliang <tanggeliang@gmail.com>
- update from 3.17.90 to 3.17.91
* Mon Sep 14 2015 tanggeliang <tanggeliang@gmail.com>
- update from 3.16.2 to 3.17.90
* Sat May 16 2015 tanggeliang <tanggeliang@gmail.com>
- update from 3.16.0 to 3.16.2
* Sun Mar 29 2015 tanggeliang <tanggeliang@gmail.com>
- update from 3.14.0 to 3.16.0
* Tue Sep 23 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.12.2 to 3.14.0
* Sat Jul 19 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.12.1 to 3.12.2
* Wed Apr 30 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.12.0 to 3.12.1
* Fri Mar 28 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.11.92 to 3.12.0
* Fri Mar 21 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.11.91 to 3.11.92
* Sat Mar 15 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.10.1 to 3.11.91
* Tue Oct 22 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.10.0.1 to 3.10.1
* Sun Sep 29 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.10.0 to 3.10.0.1
* Wed Sep 25 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.8.3 to 3.10.0
* Fri Jul 26 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.8.1 to 3.8.3
* Sat May 11 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.8.0 to 3.8.1
* Sun Apr 14 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.3 to 3.8.0
* Mon Mar 18 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.2 to 3.6.3
* Mon Nov 26 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.1 to 3.6.2
* Sat Oct 27 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.0 to 3.6.1
* Sat Sep 29 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.4.1 to 3.6.0
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
