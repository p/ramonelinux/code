Name:       gnome-initial-setup
Version:    3.17.91
Release:    1%{?dist}
Summary:    Initial System Setup

Group:      User Interface/GNOME
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/%{name}/3.17/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  gtk+ networkmanager polkit accountsservice
BuildRequires:  gnome-desktop libgweather gnome-online-accounts gdm geoclue
BuildRequires:	gnome-session gnome-shell
BuildRequires:  librest json-glib libsecret libpwquality
BuildRequires:  intltool gettext xml-parser

%description
After acquiring or installing a new system there are a few essential things to set up before use.
It would be nice if GNOME had a simple, easy, and safe way to prepare a new system.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --sysconfdir=/etc \
            --libexecdir=%{_libdir}/%{name} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
/etc/xdg/autostart/gnome-initial-setup-copy-worker.desktop
/etc/xdg/autostart/gnome-initial-setup-first-login.desktop
/etc/xdg/autostart/gnome-welcome-tour.desktop
%{_libdir}/%{name}/gnome-initial-setup
%{_libdir}/%{name}/gnome-initial-setup-copy-worker
%{_libdir}/%{name}/gnome-welcome-tour
/usr/share/gdm/greeter/applications/gnome-initial-setup.desktop
/usr/share/gdm/greeter/applications/setup-shell.desktop
/usr/share/gnome-session/sessions/gnome-initial-setup.session
/usr/share/gnome-shell/modes/initial-setup.json
/usr/share/locale/*/LC_MESSAGES/gnome-initial-setup.mo
/usr/share/polkit-1/rules.d/20-gnome-initial-setup.rules

%clean
rm -rf %{buildroot}

%changelog
* Wed Sep 16 2015 tanggeliang <tanggeliang@gmail.com>
- update from 3.16.2 to 3.17.91
* Fri May 15 2015 tanggeliang <tanggeliang@gmail.com>
- update from 3.14.0 to 3.16.2
* Tue Sep 23 2014 tanggeliang <tanggeliang@gmail.com>
- create
