Name:       gnome-shell
Version:    3.18.0
Release:    1%{?dist}
Summary:    GNOME Shell

Group:      User Interface/GNOME
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/%{name}/3.18/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  evolution-data-server gcr gjs gnome-menus 
BuildRequires:  gnome-settings-daemon gst-plugins-base json-glib libcroco 
BuildRequires:  libgnome-keyring mutter networkmanager telepathy-glib telepathy-logger
BuildRequires:  gnome-bluetooth network-manager-applet
BuildRequires:  gtk-doc intltool gettext xml-parser docbook-xml docbook-xsl
BuildRequires:  caribou gnome-control-center startup-notification
BuildRequires:	python3 accountsservice dconf gdm gnome-desktop adwaita-icon-theme
BuildRequires:	gobject-introspection libcanberra libsoup polkit pulseaudio upower 
Requires:       accountsservice caribou dconf gnome-icon-theme gnome-icon-theme-symbolic
Requires:       telepathy-mission-control adwaita-icon-theme gnome-bluetooth

%description
The GNOME Shell is the core user interface of the GNOME Desktop environment.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --sysconfdir=/etc \
            --libexecdir=%{_libdir}/gnome-shell \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/gnome-shell
%{_bindir}/gnome-shell-extension-prefs
%{_bindir}/gnome-shell-extension-tool
%{_bindir}/gnome-shell-perf-tool
%{_libdir}/gnome-shell/Gvc-1.0.typelib
%{_libdir}/gnome-shell/Shell-0.1.typelib
%{_libdir}/gnome-shell/ShellJS-0.1.typelib
%{_libdir}/gnome-shell/ShellMenu-0.1.typelib
%{_libdir}/gnome-shell/St-1.0.typelib
%{_libdir}/gnome-shell/gnome-shell-calendar-server
%{_libdir}/gnome-shell/gnome-shell-hotplug-sniffer
%{_libdir}/gnome-shell/gnome-shell-perf-helper
%{_libdir}/gnome-shell/gnome-shell-portal-helper
%{_libdir}/gnome-shell/libgnome-shell-js.*
%{_libdir}/gnome-shell/libgnome-shell-menu.*
%{_libdir}/gnome-shell/libgnome-shell.*
%{_libdir}/mozilla/plugins/libgnome-shell-browser-plugin.*
%{_datadir}/GConf/gsettings/gnome-shell-overrides.convert
%{_datadir}/applications/org.gnome.Shell.PortalHelper.desktop
%{_datadir}/applications/evolution-calendar.desktop
%{_datadir}/applications/gnome-shell.desktop
%{_datadir}/applications/gnome-shell-extension-prefs.desktop
%{_datadir}/applications/gnome-shell-wayland.desktop
%{_datadir}/dbus-1/services/org.gnome.Shell.PortalHelper.service
%{_datadir}/dbus-1/interfaces/org.gnome.Shell.Screencast.xml
%{_datadir}/dbus-1/interfaces/org.gnome.Shell.Screenshot.xml
%{_datadir}/dbus-1/interfaces/org.gnome.ShellSearchProvider.xml
%{_datadir}/dbus-1/interfaces/org.gnome.ShellSearchProvider2.xml
%{_datadir}/dbus-1/services/org.gnome.Shell.CalendarServer.service
%{_datadir}/dbus-1/services/org.gnome.Shell.HotplugSniffer.service
%{_datadir}/glib-2.0/schemas/org.gnome.shell.gschema.xml
%{_datadir}/gnome-control-center/keybindings/50-gnome-shell-system.xml
%{_datadir}/gnome-shell/gnome-shell-theme.gresource
%{_datadir}/gnome-shell/perf-background.xml
%{_datadir}/locale/*

%files doc
%defattr(-,root,root,-)
%{_datadir}/gtk-doc/html/shell/*
%{_datadir}/gtk-doc/html/st/*
%{_mandir}/man1/*

%post
glib-compile-schemas %{_datadir}/glib-2.0/schemas &>/dev/null || :

%clean
rm -rf %{buildroot}

%changelog
* Wed Sep 23 2015 tanggeliang <tanggeliang@gmail.com>
- update from 3.17.91 to 3.18.0
* Wed Sep 16 2015 tanggeliang <tanggeliang@gmail.com>
- update from 3.17.90 to 3.17.91
* Mon Sep 14 2015 tanggeliang <tanggeliang@gmail.com>
- update from 3.16.2 to 3.17.90
* Fri May 15 2015 tanggeliang <tanggeliang@gmail.com>
- update from 3.16.0 to 3.16.2
* Sun Mar 29 2015 tanggeliang <tanggeliang@gmail.com>
- update from 3.14.0 to 3.16.0
* Wed Sep 24 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.12.2 to 3.14.0
* Sat Jul 19 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.12.1 to 3.12.2
* Wed Apr 30 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.12.0 to 3.12.1
* Fri Mar 28 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.11.92 to 3.12.0
* Fri Mar 21 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.11.91 to 3.11.92
* Sat Mar 15 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.10.2.1 to 3.11.91
* Wed Dec 4 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.10.1 to 3.10.2.1
* Tue Oct 22 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.10.0.1 to 3.10.1
* Sun Sep 29 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.10.0 to 3.10.0.1
* Wed Sep 25 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.8.4 to 3.10.0
* Fri Aug 9 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.8.3 to 3.8.4
* Fri Jul 26 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.8.1 to 3.8.3
* Sat May 11 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.8.0 to 3.8.1
* Sun Apr 14 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.3.1 to 3.8.0
* Mon Mar 18 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.2 to 3.6.3.1
* Mon Nov 26 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.1 to 3.6.2
* Sat Oct 27 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.0 to 3.6.1
* Sat Sep 29 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.4.2 to 3.6.0
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
