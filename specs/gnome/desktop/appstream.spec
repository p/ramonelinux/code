Name:       appstream
Version:    0.7.0
Release:    1%{?dist}
Summary:    Utilities to generate, maintain and access the AppStream Xapian database

Group:      User Interface/GNOME
License:    GPLv2+
Url:        http://www.freedesktop.org
Source:     http://www.freedesktop.org/software/appstream/releases/AppStream-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  cmake gettext intltool pkg-config gobject-introspection
BuildRequires:  libxml glib vala xapian-core PackageKit xmlto
BuildRequires:  docbook-xml docbook-xsl libxslt

%description
AppStream-Core makes it easy to access application information from the AppStream database over a nice GObject-based interface.
It uses a PackageKit plugin to automatically (re)generate the AppStream Xapian database of applications.

%prep
%setup -q -n AppStream-%{version}

%build
mkdir build &&
cd build &&
cmake -DCMAKE_INSTALL_PREFIX=/usr \
      -DCMAKE_BUILD_TYPE=Release \
      -DTESTS:BOOL=ON -DVAPI:BOOL=ON \
%ifarch x86_64
      -DLIB_SUFFIX=64 \
%endif
      .. &&
make -j1

%check

%install
rm -rf %{buildroot}
cd build
make install DESTDIR=%{buildroot}

%files
%defattr(-,root,root,-)
/etc/appstream.conf
%{_bindir}/appstream-index
%{_bindir}/appstream-validate
%{_includedir}/Appstream/appstream.h
%{_includedir}/Appstream/as-*.h
%{_libdir}/girepository-1.0/Appstream-0.7.typelib
%{_libdir}/libappstream.so*
%{_libdir}/packagekit-plugins/libpk_plugin_appstream.so
%{_libdir}/pkgconfig/appstream.pc
%{_datadir}/app-info/categories.xml
%{_datadir}/gir-1.0/Appstream-0.7.gir
%{_datadir}/locale/de/LC_MESSAGES/appstream.mo
%{_datadir}/vala/vapi/appstream.vapi
%{_mandir}/man1/appstream-index.1.gz
%{_mandir}/man1/appstream-validate.1.gz

%clean
rm -rf %{buildroot}

%changelog
* Fri Mar 28 2014 tanggeliang <tanggeliang@gmail.com>
- create
