Name:       PackageKit
Version:    1.0.10
Release:    1%{?dist}
Summary:    Package management service

Group:      User Interface/GNOME
License:    GPLv2+
Url:        http://www.freedesktop.org
Source:     http://www.freedesktop.org/software/PackageKit/releases/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  glib dbus dbus-glib linux-pam libx11 sqlite
BuildRequires:  networkmanager polkit libtool gtk2 gtk+
BuildRequires:  intltool gettext pango fontconfig gobject-introspection
BuildRequires:  xml-parser bash-completion
BuildRequires:  docbook-xml docbook-xsl systemd

%description
PackageKit is a D-Bus abstraction layer that allows the session user to manage packages in a secure way using a cross-distro, cross-architecture API.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --sysconfdir=/etc \
            --localstatedir=/var \
            --libexecdir=%{_libdir}/%{name} \
            --disable-static \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
/etc/PackageKit/*.conf
/etc/cron.daily/packagekit-background.cron
/etc/dbus-1/system.d/org.freedesktop.PackageKit.conf
/etc/profile.d/PackageKit.sh
/etc/sysconfig/packagekit-background
/lib/systemd/system/packagekit-offline-update.service
/lib/systemd/system/packagekit.service
/lib/systemd/system/system-update.target.wants/packagekit-offline-update.service
%{_bindir}/pkcon
%{_bindir}/pkmon
%{_includedir}/PackageKit/packagekit-glib2/*.h
%{_libdir}/girepository-1.0/PackageKitGlib-1.0.typelib
%{_libdir}/gnome-settings-daemon-3.0/gtk-modules/pk-gtk-module.desktop
%{_libdir}/gtk-2.0/modules/libpk-gtk-module.*
%{_libdir}/gtk-3.0/modules/libpk-gtk-module.*
%{_libdir}/libpackagekit-glib2.*
%{_libdir}/packagekit-backend/libpk_backend_*.*
%{_libdir}/pkgconfig/packagekit-glib2.pc
%{_libdir}/PackageKit/packagekit-direct
%{_libdir}/PackageKit/packagekitd
%{_libdir}/PackageKit/pk-command-not-found
%{_libdir}/PackageKit/pk-offline-update
%{_datadir}/PackageKit/helpers/test_spawn/search-name.sh
%{_datadir}/PackageKit/pk-upgrade-distro.sh
%{_datadir}/bash-completion/completions/pkcon
%{_datadir}/dbus-1/interfaces/org.freedesktop.PackageKit.Transaction.xml
%{_datadir}/dbus-1/interfaces/org.freedesktop.PackageKit.xml
%{_datadir}/dbus-1/system-services/org.freedesktop.PackageKit.service
%{_datadir}/gir-1.0/PackageKitGlib-1.0.gir
%{_datadir}/gtk-doc/html/PackageKit/*
%{_datadir}/locale/*/LC_MESSAGES/PackageKit.mo
%{_datadir}/polkit-1/actions/org.freedesktop.packagekit.policy
%{_datadir}/polkit-1/rules.d/org.freedesktop.packagekit.rules
/var/lib/PackageKit/transactions.db

%clean
rm -rf %{buildroot}

%changelog
* Wed Sep 23 2015 tanggeliang <tanggeliang@gmail.com>
- update from 1.0.8 to 1.0.10
* Tue Sep 23 2014 tanggeliang <tanggeliang@gmail.com>
- update from 0.9.4 to 1.0.0
* Fri Mar 28 2014 tanggeliang <tanggeliang@gmail.com>
- create
