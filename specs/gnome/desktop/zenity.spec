Name:       zenity
Version:    3.18.0
Release:    1%{?dist}
Summary:    Zenity

Group:      User Interface/GNOME
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/%{name}/3.18/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  gtk+ yelp-xsl
BuildRequires:  libnotify webkitgtk
BuildRequires:  intltool gettext xml-parser libxml itstool

%description
Zenity is a rewrite of gdialog, the GNOME port of dialog which allows you to display GTK+ dialog boxes from the command line and shell scripts.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/gdialog
%{_bindir}/zenity
%{_datadir}/locale/*
%{_datadir}/zenity/*

%files doc
%defattr(-,root,root,-)
%{_datadir}/help/*/zenity/*
%{_mandir}/*/*

%clean
rm -rf %{buildroot}

%changelog
* Wed Sep 23 2015 tanggeliang <tanggeliang@gmail.com>
- update from 3.14.0 to 3.18.0
* Tue Sep 23 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.8.0 to 3.14.0
* Sun Apr 14 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.0 to 3.8.0
* Sat Sep 29 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.4.0 to 3.6.0
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
