Name:       gdm
Version:    3.18.0
Release:    1%{?dist}
Summary:    GDM

Group:      User Interface/GNOME
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/%{name}/3.18/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  accountsservice dconf libcanberra linux-pam nss yelp-xsl
BuildRequires:  gobject-introspection iso-codes upower check dbus-glib fontconfig
BuildRequires:  intltool gettext xml-parser docbook-xml libxslt itstool
BuildRequires:  fontconfig desktop-file-utils automake autoconf attr libxklavier
BuildRequires:  at-spi2-core systemd gnome-initial-setup
#plymouth
Requires:       consolekit gnome-session gnome-shell metacity xhost systemd
Requires:       cantarell-fonts

%description
GDM is a system service that is responsible for providing graphical logins and managing local and remote displays.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --sysconfdir=/etc \
            --localstatedir=/var \
            --libexecdir=%{_libdir}/gdm \
            --with-initial-vt=7 \
            --with-at-spi-registryd-directory=%{_libdir}/at-spi2-core \
            --with-authentication-agent-directory=%{_libdir}/polkit-gnome \
            --with-check-accelerated-directory=%{_libdir}/gnome-session \
            --with-consolekit-directory=%{_libdir}/ConsoleKit \
            --disable-static \
            --with-default-pam-config=lfs \
            --with-systemd \
            --with-systemdsystemunitdir=/lib/systemd/system \
            --enable-systemd-journal \
	    --without-plymouth \
	    --enable-gdm-xsession \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

cat > %{buildroot}%{_datadir}/glib-2.0/schemas/org.gnome.login-screen.gschema.override << "EOF"
[org.gnome.login-screen]
logo='%{_datadir}/pixmaps/ram-gdm-logo.png'
enable-smartcard-authentication=false
EOF

%files
%defattr(-,root,root,-)
/etc/dbus-1/system.d/gdm.conf
/etc/gdm/Init/Default
/etc/gdm/PostLogin/Default.sample
/etc/gdm/PostSession/Default
/etc/gdm/PreSession/Default
/etc/gdm/Xsession
/etc/gdm/custom.conf
/etc/pam.d/gdm
/etc/pam.d/gdm-autologin
/etc/pam.d/gdm-fingerprint
/etc/pam.d/gdm-launch-environment
/etc/pam.d/gdm-password
/etc/pam.d/gdm-pin
/etc/pam.d/gdm-smartcard
%{_bindir}/gdm-screenshot
%{_bindir}/gdmflexiserver
%{_includedir}/gdm/*
%{_libdir}/gdm/gdm-*
%{_libdir}/girepository-1.0/Gdm-1.0.typelib
%{_libdir}/libgdm.*
%{_libdir}/pkgconfig/gdm.pc
%{_sbindir}/gdm
%{_datadir}/dconf/profile/gdm
%{_datadir}/gdm/gdb*
%{_datadir}/gdm/gdm*
%{_datadir}/gdm/greeter-dconf-defaults
%{_datadir}/gdm/greeter/autostart/orca-autostart.desktop
%{_datadir}/gdm/greeter/applications/*.desktop
%{_datadir}/gdm/greeter/applications/mimeapps.list
%{_datadir}/gdm/locale.alias
%{_datadir}/gir-1.0/Gdm-1.0.gir
%{_datadir}/glib-2.0/schemas/org.gnome.login-screen.gschema.xml
%{_datadir}/glib-2.0/schemas/org.gnome.login-screen.gschema.override
%{_datadir}/icons/hicolor/*/apps/gdm-*.png
%{_datadir}/locale/*/LC_MESSAGES/gdm.mo
%{_datadir}/pixmaps/*.png
%attr(1755, root, gdm) %dir /var/cache/gdm
%attr(1770, gdm, gdm) %dir /var/lib/gdm
%dir /var/log/gdm
%attr(1755, gdm, gdm) %dir /var/run/gdm/greeter
%attr(1777, root, gdm) %dir /var/run/gdm
/lib/systemd/system/gdm.service

%files doc
%defattr(-,root,root,-)
%{_datadir}/help/*/gdm/*

%clean
rm -rf %{buildroot}

%pre
groupadd -g 21 gdm &&
useradd -c "GDM Daemon Owner" -d /var/lib/gdm -u 21 \
        -g gdm -s /bin/false gdm &&
usermod -a -G audio gdm &&
usermod -a -G video gdm

%post
chown -R gdm:gdm /var/lib/gdm /var/cache/gdm /var/log/gdm
systemctl enable gdm
gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
glib-compile-schemas %{_datadir}/glib-2.0/schemas &>/dev/null || :
dconf update

%postun
systemctl disable gdm
userdel gdm

%changelog
* Wed Sep 23 2015 tanggeliang <tanggeliang@gmail.com>
- update from 3.17.92 to 3.18.0
* Wed Sep 16 2015 tanggeliang <tanggeliang@gmail.com>
- update from 3.17.90 to 3.17.92
- add '--enable-gdm-xsession' for '/etc/gdm/Xsession'
* Mon Sep 14 2015 tanggeliang <tanggeliang@gmail.com>
- update from 3.16.0 to 3.17.90
* Sun Mar 29 2015 tanggeliang <tanggeliang@gmail.com>
- update from 3.14.0 to 3.16.0
* Tue Sep 23 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.12.2 to 3.14.0
* Sat Jul 19 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.11.90 to 3.12.2
* Mon Mar 17 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.10.0.1 to 3.11.90
* Tue Oct 22 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.10.0 to 3.10.0.1
* Wed Sep 25 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.8.4 to 3.10.0
* Wed Aug 7 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.8.3 to 3.8.4
- remove pam_systemd.so in /etc/pam.d/gdm-launch-environment
* Fri Jul 26 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.8.1 to 3.8.3
* Sat May 11 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.8.0 to 3.8.1
* Sun Apr 14 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.2 to 3.8.0
* Mon Nov 26 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.1 to 3.6.2
* Sat Oct 27 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.0 to 3.6.1
* Sun Sep 30 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.4.1 to 3.6.0
- add --with-default-pam-config=lfs
* Sun Aug 26 2012 tanggeliang <tanggeliang@gmail.com>
- add "dconf update" to fix "Unable to open '/etc/dconf/db/gdm'"
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
