Name:       gnome-panel
Version:    3.16.0
Release:    1%{?dist}
Summary:    GNOME Panel

Group:      User Interface/GNOME
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/%{name}/3.16/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  dconf gconf gnome-desktop gnome-menus libcanberra libgweather librsvg libwnck yelp-xsl
BuildRequires:  evolution-data-server gobject-introspection networkmanager telepathy-glib
BuildRequires:  gtk-doc libcroco
BuildRequires:  intltool gettext xml-parser docbook-xml
BuildRequires:  itstool

%description
The GNOME Panel package contains hooks to the menu sub-system and the applet sub-system.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --sysconfdir=/etc \
            --libexecdir=%{_libdir}/gnome-applets \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/gnome-desktop-item-edit
%{_bindir}/gnome-panel
%{_bindir}/panel-test-applets
%{_includedir}/gnome-panel/libpanel-applet/panel-applet*.h
%{_libdir}/girepository-1.0/PanelApplet-5.0.typelib
%{_libdir}/gnome-panel/5.0/libclock-applet.*
%{_libdir}/gnome-panel/5.0/libfish-applet.*
%{_libdir}/gnome-panel/5.0/libnotification-area-applet.*
%{_libdir}/gnome-panel/5.0/libwnck-applet.*
%{_libdir}/libpanel-applet.*
%{_libdir}/pkgconfig/libpanel-applet.pc
%{_datadir}/applications/gnome-panel.desktop
%{_datadir}/gir-1.0/PanelApplet-5.0.gir
%{_datadir}/gnome-panel/5.0/applets/org.gnome.panel.*.panel-applet
%{_datadir}/gnome-panel/fish/*.fish
%{_datadir}/gnome-panel/fish/*.png
%{_datadir}/gnome-panel/panel-default-layout.layout
%{_datadir}/icons/hicolor/*/apps/gnome-panel*.png
%{_datadir}/icons/hicolor/scalable/apps/gnome-panel*.svg
%{_datadir}/locale/*/LC_MESSAGES/gnome-panel-3.0.mo
%{_datadir}/glib-2.0/schemas/org.gnome.gnome-panel.*.xml

%files doc
%defattr(-,root,root,-)
%{_datadir}/gtk-doc/*
%{_datadir}/help/*/*/index.docbook
%{_datadir}/help/*/*/legal.xml
%{_mandir}/*/*

%clean
rm -rf %{buildroot}

%post
gtk-update-icon-cache -q %{_datadir}/icons/hicolor >&/dev/null || :
glib-compile-schemas %{_datadir}/glib-2.0/schemas &>/dev/null || :

%postun

%changelog
* Sun Mar 29 2015 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.2 to 3.16.0
* Mon Nov 26 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.0 to 3.6.2
* Fri Sep 28 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.4.2.1 to 3.6.0
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
