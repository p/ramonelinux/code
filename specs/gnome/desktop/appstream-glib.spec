Name:       appstream-glib
Version:    0.5.1
Release:    1%{?dist}
Summary:    Library for AppStream metadata

Group:      User Interface/GNOME
License:    GPLv2+
Url:        http://www.freedesktop.org
Source:     http://people.freedesktop.org/~hughsient/appstream-glib/releases/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  glib libtool gobject-introspection gperf libarchive libsoup
BuildRequires:  gdk-pixbuf gettext intltool fontconfig freetype gtk+ pango
BuildRequires:  libxslt docbook-xml docbook-xsl xml-parser
BuildRequires:  elfutils elfutils-libelf yaml gcab

%description
This library provides GObjects and helper methods to make it easy to read and write AppStream metadata. It also provides a simple DOM implementation that makes it easy to edit nodes and convert to and from the standardized XML representation.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --disable-static \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

%files
%defattr(-,root,root,-)
%{_bindir}/appdata-validate
%{_bindir}/appstream-builder
%{_bindir}/appstream-util
%{_includedir}/libappstream-builder/appstream-builder.h
%{_includedir}/libappstream-builder/asb-app.h
%{_includedir}/libappstream-glib/appstream-glib.h
%{_includedir}/libappstream-glib/as-*.h
%{_libdir}/asb-plugins-2/libasb_plugin_*.la
%{_libdir}/asb-plugins-2/libasb_plugin_*.so
%{_libdir}/girepository-1.0/AppStreamBuilder-1.0.typelib
%{_libdir}/girepository-1.0/AppStreamGlib-1.0.typelib
%{_libdir}/libappstream-builder.*
%{_libdir}/libappstream-glib.*
%{_libdir}/pkgconfig/appstream-builder.pc
%{_libdir}/pkgconfig/appstream-glib.pc
%{_datadir}/aclocal/appdata-xml.m4
%{_datadir}/aclocal/appstream-xml.m4
%{_datadir}/bash-completion/completions/appstream-builder
%{_datadir}/bash-completion/completions/appstream-util
%{_datadir}/gir-1.0/AppStreamBuilder-1.0.gir
%{_datadir}/gir-1.0/AppStreamGlib-1.0.gir
%{_datadir}/gtk-doc/html/%{name}/*
%{_datadir}/installed-tests/appstream-glib/*.test
%{_datadir}/locale/ru/LC_MESSAGES/%{name}.mo
%{_mandir}/man1/appstream-builder.1.gz
%{_mandir}/man1/appstream-util.1.gz

%clean
rm -rf %{buildroot}

%changelog
* Wed Sep 23 2015 tanggeliang <tanggeliang@gmail.com>
- update from 0.3.0 to 0.5.1
* Tue Sep 23 2014 tanggeliang <tanggeliang@gmail.com>
- update from 0.2.3 to 0.3.0
* Fri Mar 28 2014 tanggeliang <tanggeliang@gmail.com>
- create
