Name:       gnome-desktop
Version:    3.18.0
Release:    1%{?dist}
Summary:    GNOME Desktop

Group:      User Interface/GNOME
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/%{name}/3.18/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  gsettings-desktop-schemas gtk+ yelp-xsl
BuildRequires:  gobject-introspection startup-notification
BuildRequires:  gtk-doc intltool gettext xml-parser libxslt
BuildRequires:  xkeyboard-config libxkbfile itstool iso-codes
BuildRequires:  intltool libxkbfile libxrandr gdk-pixbuf gvfs yelp-tools xkeyboard-config

%description
The GNOME Desktop package contains library which provides API shared by several applications on the GNOME Desktop.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --with-gnome-distributor="BLFS" \
	    --libexecdir=%{_libdir}/gnome-desktop-3.0 \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_includedir}/gnome-desktop-3.0/*
%{_libdir}/libgnome-desktop-3.*
%{_libdir}/pkgconfig/gnome-desktop-3.0.pc
%{_libdir}/girepository-1.0/GnomeDesktop-3.0.typelib
%{_libdir}/gnome-desktop-3.0/gnome-rr-debug
%{_datadir}/gnome/*
%{_datadir}/locale/*
%{_datadir}/gir-1.0/GnomeDesktop-3.0.gir
%{_datadir}/libgnome-desktop-3.0/pnp.ids

%files doc
%defattr(-,root,root,-)
%{_datadir}/gtk-doc/*
%{_datadir}/help/*/*/index.docbook

%clean
rm -rf %{buildroot}

%changelog
* Wed Sep 23 2015 tanggeliang <tanggeliang@gmail.com>
- update from 3.17.92 to 3.18.0
* Sun Sep 20 2015 tanggeliang <tanggeliang@gmail.com>
- update from 3.16.0 to 3.17.92
* Sun Mar 29 2015 tanggeliang <tanggeliang@gmail.com>
- update from 3.14.0 to 3.16.0
* Tue Sep 23 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.12.2 to 3.14.0
* Sat Jul 19 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.12.1 to 3.12.2
* Sun Apr 20 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.12.0 to 3.12.1
* Thu Mar 27 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.11.90 to 3.12.0
* Sat Mar 15 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.10.2 to 3.11.90
* Tue Dec 3 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.10.1 to 3.10.2
* Tue Oct 22 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.10.0 to 3.10.1
* Tue Sep 24 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.8.3 to 3.10.0
* Wed Jul 24 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.8.1 to 3.8.3
* Sat May 11 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.8.0.1 to 3.8.1
* Sat Apr 13 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.2 to 3.8.0.1
* Mon Nov 26 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.1 to 3.6.2
* Sat Oct 27 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.0.1 to 3.6.1
* Fri Sep 28 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.4.2 to 3.6.0.1
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
