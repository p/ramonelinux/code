Name:       gnome-settings-daemon
Version:    3.18.0
Release:    1%{?dist}
Summary:    GNOME Settings Daemon

Group:      User Interface/GNOME
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/%{name}/3.18/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  colord gnome-desktop libcanberra libnotify libgnomekbd libwacom pulseaudio upower xf86-input-wacom
BuildRequires:  cups ibus nss
BuildRequires:  intltool gettext xml-parser
BuildRequires:  lcms2 libxtst libxkbfile json-c libsndfile flac gdbm
BuildRequires:  libxslt docbook-xml docbook-xsl librsvg
BuildRequires:  geoclue geocode-glib libgweather
BuildRequires:  xkeyboard-config systemd-gudev
BuildRequires:  gsettings-desktop-schemas gnome-desktop
BuildRequires:  PackageKit polkit pulseaudio upower ibus cups
BuildRequires:	networkmanager networkmanager-glib networkmanager-util

%description
The GNOME Settings Daemon is responsible for setting various parameters of a GNOME Session and the applications that run under it.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --sysconfdir=/etc \
            --libexecdir=%{_libdir}/gnome-settings-daemon \
            --disable-packagekit \
            --disable-static \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install udevrulesdir=/lib/udev/rules.d

%files
%defattr(-,root,root,-)
/etc/xdg/autostart/gnome-settings-daemon.desktop
/lib/udev/rules.d/61-gnome-settings-daemon-rfkill.rules
%{_includedir}/gnome-settings-daemon-3.0/*
%{_libdir}/gnome-settings-daemon-3.0/*
%{_libdir}/pkgconfig/gnome-settings-daemon.pc
%{_libdir}/gnome-settings-daemon/*
%{_datadir}/gnome-settings-daemon/*
%{_datadir}/icons/*
%{_datadir}/locale/*
%{_datadir}/GConf/gsettings/gnome-settings-daemon.convert
%{_datadir}/glib-2.0/schemas/org.gnome.settings-daemon.*.xml
%{_datadir}/gnome-settings-daemon-3.0/input-device-example.sh
%{_datadir}/polkit-1/actions/org.gnome.settings-daemon.plugins.power.policy
%{_datadir}/polkit-1/actions/org.gnome.settings-daemon.plugins.wacom.policy

%files doc
%defattr(-,root,root,-)
%{_mandir}/*

%post
gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
glib-compile-schemas %{_datadir}/glib-2.0/schemas &>/dev/null || :

%clean
rm -rf %{buildroot}

%changelog
* Wed Sep 23 2015 tanggeliang <tanggeliang@gmail.com>
- update from 3.17.92 to 3.18.0
* Wed Sep 16 2015 tanggeliang <tanggeliang@gmail.com>
- update from 3.16.0 to 3.17.92
* Sat Mar 28 2015 tanggeliang <tanggeliang@gmail.com>
- update from 3.14.0 to 3.16.0
* Wed Sep 24 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.12.2 to 3.14.0
* Sat Jul 19 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.11.92 to 3.12.2
* Fri Mar 21 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.11.91 to 3.11.92
* Sat Mar 15 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.10.1 to 3.11.91
* Mon Oct 21 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.10.0 to 3.10.1
* Tue Sep 24 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.8.4 to 3.10.0
* Wed Jul 24 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.8.1 to 3.8.4
* Sat May 11 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.8.0 to 3.8.1
* Sat Apr 13 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.4 to 3.8.0
* Sun Feb 24 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.3 to 3.6.4
* Mon Nov 26 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.1 to 3.6.3
* Sat Oct 27 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.0 to 3.6.1
* Fri Sep 28 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.4.2 to 3.6.0
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
