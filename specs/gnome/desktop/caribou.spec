Name:       caribou
Version:    0.4.18
Release:    1%{?dist}
Summary:    Caribou

Group:      User Interface/GNOME
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/%{name}/0.4/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  clutter gtk+ libgee libxklavier pygobject
BuildRequires:  vala gobject-introspection
BuildRequires:  libxtst intltool gettext xml-parser libxslt
Requires:       pyatspi dbus-python dconf

%description
Caribou is an input assistive technology intended for switch and pointer users.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --sysconfdir=/etc \
            --libexecdir=%{_libdir}/caribou \
            --disable-gtk2-module \
            --disable-static \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
/etc/xdg/autostart/caribou-autostart.desktop
%{_bindir}/caribou-preferences
%{_includedir}/libcaribou/caribou.h
%{_libdir}/caribou/antler-keyboard
%{_libdir}/caribou/caribou
%{_libdir}/girepository-1.0/Caribou-1.0.typelib
%{_libdir}/gnome-settings-daemon-3.0/gtk-modules/caribou-gtk-module.desktop
%{_libdir}/gtk-3.0/modules/libcaribou-gtk-module.*
%{_libdir}/pkgconfig/caribou-1.0.pc
%{_libdir}/libcaribou.*
/usr/lib/python2.7/site-packages/caribou/*
%{_datadir}/antler/dark-key-border.svg
%{_datadir}/antler/style.css
%{_datadir}/caribou/layouts/*
%{_datadir}/dbus-1/services/org.gnome.Caribou.Antler.service
%{_datadir}/dbus-1/services/org.gnome.Caribou.Daemon.service
%{_datadir}/gir-1.0/Caribou-1.0.gir
%{_datadir}/glib-2.0/schemas/org.gnome.antler.gschema.xml
%{_datadir}/glib-2.0/schemas/org.gnome.caribou.gschema.xml
%{_datadir}/locale/*
%{_datadir}/vala/vapi/caribou-1.0.*

%clean
rm -rf %{buildroot}

%changelog
* Wed Sep 23 2015 tanggeliang <tanggeliang@gmail.com>
- update from 0.4.13 to 0.4.18
* Sun Mar 16 2014 tanggeliang <tanggeliang@gmail.com>
- updare from 0.4.12 to 0.4.13
* Wed Sep 25 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.4.11 to 0.4.12
* Tue Aug 6 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.4.10 to 0.4.11
* Sun Apr 14 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.4.4.2 to 0.4.10
* Mon Nov 26 2012 tanggeliang <tanggeliang@gmail.com>
- update from 0.4.4 to 0.4.4.2
* Sat Sep 29 2012 tanggeliang <tanggeliang@gmail.com>
- update from 0.4.2 to 0.4.4
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
