Name:       bash-completion
Version:    2.1
Release:    1%{?dist}
Summary:    Programmable completion for Bash

Group:      User Interface/Shells
License:    GPLv2+
Url:        http://bash-completion.alioth.debian.org
Source:     http://bash-completion.alioth.debian.org/files/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  bash
BuildArch:      noarch

%description
bash-completion is a collection of shell functions that take advantage of the programmable completion feature of bash.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --sysconfdir=/etc &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

rm -rf %{buildroot}/%{_datadir}/bash-completion/completions/{cal,chsh,dmesg,eject,hexdump,hwclock,ionice,look,renice,rtcwake,nmcli}

%files
%defattr(-,root,root,-)
/etc/profile.d/bash_completion.sh
%{_datadir}/bash-completion/bash_completion
%{_datadir}/bash-completion/completions/*
%{_datadir}/bash-completion/helpers/perl
%{_datadir}/pkgconfig/bash-completion.pc

%clean
rm -rf %{buildroot}

%changelog
* Fri Mar 28 2014 tanggeliang <tanggeliang@gmail.com>
- create
