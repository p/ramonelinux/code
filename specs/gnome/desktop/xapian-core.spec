Name:       xapian-core
Version:    1.2.18
Release:    1%{?dist}
Summary:    The Xapian Probabilistic Information Retrieval Library

Group:      User Interface/GNOME
License:    GPLv2+
Url:        http://www.xapian.org
Source:     oligarchy.co.uk/xapian/%{version}/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  zlib util-linux

%description
Xapian is an Open Source Probabilistic Information Retrieval Library.
It offers a highly adaptable toolkit that allows developers to easily add advanced indexing and search facilities to applications

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --disable-static \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/copydatabase
%{_bindir}/delve
%{_bindir}/quest
%{_bindir}/simpleexpand
%{_bindir}/simpleindex
%{_bindir}/simplesearch
%{_bindir}/xapian-check
%{_bindir}/xapian-chert-update
%{_bindir}/xapian-compact
%{_bindir}/xapian-config
%{_bindir}/xapian-inspect
%{_bindir}/xapian-metadata
%{_bindir}/xapian-progsrv
%{_bindir}/xapian-replicate
%{_bindir}/xapian-replicate-server
%{_bindir}/xapian-tcpsrv
%{_includedir}/xapian.h
%{_includedir}/xapian/*.h
%{_libdir}/cmake/xapian/xapian-config*.cmake
%{_libdir}/libxapian.la
%{_libdir}/libxapian.so*
%{_datadir}/aclocal/xapian.m4
%{_docdir}/xapian-core/*
%{_mandir}/man1/*.1.gz

%clean
rm -rf %{buildroot}

%changelog
* Fri Mar 28 2014 tanggeliang <tanggeliang@gmail.com>
- create
