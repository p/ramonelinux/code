Name:       mutter-wayland
Version:    3.12.1
Release:    3%{?dist}
Summary:    Mutter Wayland

Group:      User Interface/GNOME
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/%{name}/3.12/%{name}-%{version}.tar.xz
Patch:      %{name}-%{version}-systemd-215.diff

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  glib libdrm pango systemd wayland
BuildRequires:  gtk+ clutter cogl upower
BuildRequires:  libcanberra clutter-wayland zenity
BuildRequires:  gnome-desktop gsettings-desktop-schemas
BuildRequires:  intltool gtk-doc gettext xml-parser pkg-config linux-pam
BuildRequires:  libsm libx11 libxdamage libxext libxrandr libxrender libxcursor libxcomposite
BuildRequires: startup-notification gobject-introspection desktop-file-utils

%description
Mutter is the window manager for GNOME.
It is not invoked directly, but from gnome-session (on a machine with a hardware accelerated video driver).

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}
%patch -p1

%build
sed -i '17790s/-Werror//' configure
./configure --prefix=/usr --disable-static \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/mutter-launch
%{_bindir}/mutter-wayland
%{_includedir}/mutter-wayland/meta/*.h
%{_libdir}/libmutter-wayland.la
%{_libdir}/libmutter-wayland.so*
%{_libdir}/mutter-wayland/plugins/default.so
%{_libdir}/mutter-wayland/Meta-3.0.gir
%{_libdir}/mutter-wayland/Meta-3.0.typelib
%{_libdir}/pkgconfig/libmutter-wayland.pc
%{_datadir}/GConf/gsettings/mutter-schemas.convert
%{_datadir}/applications/mutter-wayland.desktop
%{_datadir}/glib-2.0/schemas/org.gnome.mutter.gschema.xml
%{_datadir}/glib-2.0/schemas/org.gnome.mutter.wayland.gschema.xml
%{_datadir}/gnome-control-center/keybindings/50-mutter-*.xml
%{_datadir}/locale/*/LC_MESSAGES/mutter-wayland.mo

%files doc
%defattr(-,root,root,-)
%{_datadir}/gtk-doc/html/meta/*
%{_mandir}/man1/mutter.1.gz

%post
glib-compile-schemas %{_datadir}/glib-2.0/schemas &>/dev/null || :

%clean
rm -rf %{buildroot}

%changelog
* Wed Apr 30 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.12.0 to 3.12.1
* Fri Mar 28 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.11.92 to 3.12.0
* Fri Mar 21 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.11.91 to 3.11.92
* Sun Mar 16 2014 tanggeliang <tanggeliang@gmail.com>
- updare from 3.10.1 to 3.11.91
* Tue Oct 22 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.10.0.1 to 3.10.1
* Sun Sep 29 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.10.0 to 3.10.0.1
* Sat Sep 28 2013 tanggeliang <tanggeliang@gmail.com>
- create
