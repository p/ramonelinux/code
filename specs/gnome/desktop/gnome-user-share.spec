Name:       gnome-user-share
Version:    3.13.2
Release:    1%{?dist}
Summary:    GNOME User Share

Group:      User Interface/GNOME
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/%{name}/3.13/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  gnome-bluetooth libcanberra yelp-xsl
BuildRequires:  nautilus intltool gettext xml-parser
BuildRequires:  libxslt docbook-xml itstool dbus-glib

%description
The GNOME User Share package allows easy user-level file sharing via WebDAV or ObexFTP.
The shared files are announced on the network by Avahi.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --sysconfdir=/etc \
            --libexecdir=%{_libdir}/gnome-user-share \
            --with-modules-path=%{_libdir}/apache \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
/etc/xdg/autostart/gnome-user-share-*.desktop
%{_libdir}/gnome-user-share/gnome-user-share-obexpush
%{_libdir}/gnome-user-share/gnome-user-share-webdav
%{_libdir}/nautilus/extensions-3.0/libnautilus-share-extension.*
%{_datadir}/applications/gnome-user-share-webdav.desktop
%{_datadir}/GConf/gsettings/gnome-user-share.convert
%{_datadir}/glib-2.0/schemas/org.gnome.desktop.file-sharing.gschema.xml
%{_datadir}/gnome-user-share/dav_groupfile
%{_datadir}/gnome-user-share/dav_user_2.0.conf
%{_datadir}/gnome-user-share/dav_user_2.2.conf
%{_datadir}/gnome-user-share/dav_user_2.4.conf
%{_datadir}/icons/hicolor/*/apps/gnome-obex-server.png
%{_datadir}/locale/*/LC_MESSAGES/gnome-user-share.mo

%post
gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
glib-compile-schemas %{_datadir}/glib-2.0/schemas &>/dev/null || :

%postun

%clean
rm -rf %{buildroot}

%changelog
* Sat Jul 19 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.10.0 to 3.13.2
* Thu Sep 26 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.8.3 to 3.10.0
* Sat Aug 3 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.8.0 to 3.8.3
* Sat Apr 13 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.0.4 to 3.8.0
* Fri Sep 28 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.0.2 to 3.0.4
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
