Name:       metacity
Version:    3.15.2
Release:    1%{?dist}
Summary:    Metacity

Group:      User Interface/GNOME
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/%{name}/3.15/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  gtk2 libcanberra yelp-xsl zenity
BuildRequires:  startup-notification notification-daemon polkit-gnome
BuildRequires:  intltool gettext xml-parser
BuildRequires:  gsettings-desktop-schemas xcb-util
BuildRequires:  libgtop libxml
BuildRequires:  libice libsm itstool
Requires:       notification-daemon gtk-engines

%description
Metacity is the fallback window manager for GNOME, used if the video driver does not provide hardware acceleration.
It is conventionally run from gnome-session, which will start the necessary GNOME daemons.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --disable-static \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/metacity*
%{_includedir}/metacity/metacity-private/*.h
%{_libdir}/libmetacity-private.*
%{_libdir}/pkgconfig/libmetacity-private.pc
%{_datadir}/applications/*
%{_datadir}/gnome/*
%{_datadir}/gnome-control-center/*
%{_datadir}/locale/*
%{_datadir}/metacity/*
%{_datadir}/themes/*
%{_datadir}/GConf/gsettings/metacity-schemas.convert
%{_datadir}/glib-2.0/schemas/org.gnome.metacity.gschema.xml

%files doc
%defattr(-,root,root,-)
%{_datadir}/help/*/creating-metacity-themes/index.docbook
%{_mandir}/*/*

%post
glib-compile-schemas %{_datadir}/glib-2.0/schemas &>/dev/null || :

%postun

%clean
rm -rf %{buildroot}

%changelog
* Sun Mar 29 2015 tanggeliang <tanggeliang@gmail.com>
- update from 3.14.0 to 3.15.2
* Fri Oct 10 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.12.0 to 3.14.0
* Sat Jul 19 2014 tanggeliang <tanggeliang@gmail.com>
- update from 2.34.13 to 3.12.0
* Mon Nov 26 2012 tanggeliang <tanggeliang@gmail.com>
- update from 2.34.8 to 2.34.13
* Sat Sep 29 2012 tanggeliang <tanggeliang@gmail.com>
- update from 2.34.5 to 2.34.8
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
