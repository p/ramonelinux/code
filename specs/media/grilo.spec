Name:       grilo
Version:    0.2.14
Release:    1%{?dist}
Summary:    Grilo

Group:      User Interface/GNOME
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/%{name}/0.2/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  glib libxml vala
BuildRequires:  intltool gettext xml-parser
BuildRequires:  liboauth libsoup totem-pl-parser
BuildRequires:  gtk+ gobject-introspection

%description
Grilo is a framework focused on making media discovery and browsing easy for application developers. 

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --disable-static \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/grl-launch-0.2
%{_bindir}/grilo-test-ui-0.2
%{_bindir}/grl-inspect-0.2
%{_includedir}/grilo-0.2/grilo.h
%{_includedir}/grilo-0.2/grl-*.h
%{_includedir}/grilo-0.2/net/grl-net*.h
%{_includedir}/grilo-0.2/pls/grl-pls.h
%{_libdir}/girepository-1.0/Grl*-0.2.typelib
%{_libdir}/libgrilo-0.2.*a
%{_libdir}/libgrilo-0.2.so*
%{_libdir}/libgrlnet-0.2.*
%{_libdir}/libgrlpls-0.2.*
%{_libdir}/pkgconfig/grilo-0.2.pc
%{_libdir}/pkgconfig/grilo-net-0.2.pc
%{_libdir}/pkgconfig/grilo-pls-0.2.pc
%{_datadir}/gir-1.0/Grl*-0.2.gir
%{_datadir}/locale/*/LC_MESSAGES/grilo.mo
%{_mandir}/man1/gr*.1.gz
%{_datadir}/vala/vapi/grilo*-0.2.*

%clean
rm -rf %{buildroot}

%changelog
* Wed Sep 23 2015 tanggeliang <tanggeliang@gmail.com>
- update from 0.2.12 to 0.2.14
* Sat Mar 29 2014 tanggeliang <tanggeliang@gmail.com>
- update from 0.2.9 to 0.2.10
* Mon Mar 17 2014 tanggeliang <tanggeliang@gmail.com>
- update from 0.2.7 to 0.2.9
* Sat Sep 28 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.2.0 to 0.2.7
* Mon Oct 1 2012 tanggeliang <tanggeliang@gmail.com>
- create
