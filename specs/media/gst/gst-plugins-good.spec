Name:       gst-plugins-good
Version:    1.5.91
Release:    1%{?dist}
Summary:    GStreamer Good Plug-ins

Group:      Applications/Multimedia
License:    GPLv2+
Url:        http://www.freedesktop.org
Source:     http://gstreamer.freedesktop.org/src/%{name}/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  gst-plugins-base
BuildRequires:  cairo flac gdk-pixbuf libjpeg-turbo libpng libsoup libvpx libx11
BuildRequires:  aalib libdv pulseaudio taglib systemd
BuildRequires:  gconf esound mesa

%description
The GStreamer Good Plug-ins is a set of plug-ins considered by the GStreamer developers to have good quality code, correct functionality, and the preferred license (LGPL for the plug-in code, LGPL or LGPL-compatible for the supporting library).
A wide range of video and audio decoders, encoders, and filters are included.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --with-package-name="GStreamer Good Plugins %{version} BLFS" \
            --with-package-origin="http://www.linuxfromscratch.org/blfs/view/svn/" \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_libdir}/gstreamer-1.0/libgstaasink.la
%{_libdir}/gstreamer-1.0/libgstaasink.so
%{_libdir}/gstreamer-1.0/libgstalaw.la
%{_libdir}/gstreamer-1.0/libgstalaw.so
%{_libdir}/gstreamer-1.0/libgstalpha.la
%{_libdir}/gstreamer-1.0/libgstalpha.so
%{_libdir}/gstreamer-1.0/libgstalphacolor.la
%{_libdir}/gstreamer-1.0/libgstalphacolor.so
%{_libdir}/gstreamer-1.0/libgstapetag.la
%{_libdir}/gstreamer-1.0/libgstapetag.so
%{_libdir}/gstreamer-1.0/libgstaudiofx.la
%{_libdir}/gstreamer-1.0/libgstaudiofx.so
%{_libdir}/gstreamer-1.0/libgstaudioparsers.la
%{_libdir}/gstreamer-1.0/libgstaudioparsers.so
%{_libdir}/gstreamer-1.0/libgstauparse.la
%{_libdir}/gstreamer-1.0/libgstauparse.so
%{_libdir}/gstreamer-1.0/libgstautodetect.la
%{_libdir}/gstreamer-1.0/libgstautodetect.so
%{_libdir}/gstreamer-1.0/libgstavi.la
%{_libdir}/gstreamer-1.0/libgstavi.so
%{_libdir}/gstreamer-1.0/libgstcairo.la
%{_libdir}/gstreamer-1.0/libgstcairo.so
%{_libdir}/gstreamer-1.0/libgstcutter.la
%{_libdir}/gstreamer-1.0/libgstcutter.so
%{_libdir}/gstreamer-1.0/libgstdebug.la
%{_libdir}/gstreamer-1.0/libgstdebug.so
%{_libdir}/gstreamer-1.0/libgstdeinterlace.la
%{_libdir}/gstreamer-1.0/libgstdeinterlace.so
%{_libdir}/gstreamer-1.0/libgstdtmf.la
%{_libdir}/gstreamer-1.0/libgstdtmf.so
%{_libdir}/gstreamer-1.0/libgstdv.la
%{_libdir}/gstreamer-1.0/libgstdv.so
%{_libdir}/gstreamer-1.0/libgsteffectv.la
%{_libdir}/gstreamer-1.0/libgsteffectv.so
%{_libdir}/gstreamer-1.0/libgstequalizer.la
%{_libdir}/gstreamer-1.0/libgstequalizer.so
%{_libdir}/gstreamer-1.0/libgstflac.la
%{_libdir}/gstreamer-1.0/libgstflac.so
%{_libdir}/gstreamer-1.0/libgstflv.la
%{_libdir}/gstreamer-1.0/libgstflv.so
%{_libdir}/gstreamer-1.0/libgstflxdec.la
%{_libdir}/gstreamer-1.0/libgstflxdec.so
%{_libdir}/gstreamer-1.0/libgstgdkpixbuf.la
%{_libdir}/gstreamer-1.0/libgstgdkpixbuf.so
%{_libdir}/gstreamer-1.0/libgstgoom.la
%{_libdir}/gstreamer-1.0/libgstgoom.so
%{_libdir}/gstreamer-1.0/libgstgoom2k1.la
%{_libdir}/gstreamer-1.0/libgstgoom2k1.so
%{_libdir}/gstreamer-1.0/libgsticydemux.la
%{_libdir}/gstreamer-1.0/libgsticydemux.so
%{_libdir}/gstreamer-1.0/libgstid3demux.la
%{_libdir}/gstreamer-1.0/libgstid3demux.so
%{_libdir}/gstreamer-1.0/libgstimagefreeze.la
%{_libdir}/gstreamer-1.0/libgstimagefreeze.so
%{_libdir}/gstreamer-1.0/libgstinterleave.la
%{_libdir}/gstreamer-1.0/libgstinterleave.so
%{_libdir}/gstreamer-1.0/libgstisomp4.la
%{_libdir}/gstreamer-1.0/libgstisomp4.so
%{_libdir}/gstreamer-1.0/libgstjpeg.la
%{_libdir}/gstreamer-1.0/libgstjpeg.so
%{_libdir}/gstreamer-1.0/libgstlevel.la
%{_libdir}/gstreamer-1.0/libgstlevel.so
%{_libdir}/gstreamer-1.0/libgstmatroska.la
%{_libdir}/gstreamer-1.0/libgstmatroska.so
%{_libdir}/gstreamer-1.0/libgstmulaw.la
%{_libdir}/gstreamer-1.0/libgstmulaw.so
%{_libdir}/gstreamer-1.0/libgstmultifile.la
%{_libdir}/gstreamer-1.0/libgstmultifile.so
%{_libdir}/gstreamer-1.0/libgstmultipart.la
%{_libdir}/gstreamer-1.0/libgstmultipart.so
%{_libdir}/gstreamer-1.0/libgstnavigationtest.la
%{_libdir}/gstreamer-1.0/libgstnavigationtest.so
%{_libdir}/gstreamer-1.0/libgstoss4audio.la
%{_libdir}/gstreamer-1.0/libgstoss4audio.so
%{_libdir}/gstreamer-1.0/libgstossaudio.la
%{_libdir}/gstreamer-1.0/libgstossaudio.so
%{_libdir}/gstreamer-1.0/libgstpng.la
%{_libdir}/gstreamer-1.0/libgstpng.so
%{_libdir}/gstreamer-1.0/libgstpulse.la
%{_libdir}/gstreamer-1.0/libgstpulse.so
%{_libdir}/gstreamer-1.0/libgstreplaygain.la
%{_libdir}/gstreamer-1.0/libgstreplaygain.so
%{_libdir}/gstreamer-1.0/libgstrtp.la
%{_libdir}/gstreamer-1.0/libgstrtp.so
%{_libdir}/gstreamer-1.0/libgstrtpmanager.la
%{_libdir}/gstreamer-1.0/libgstrtpmanager.so
%{_libdir}/gstreamer-1.0/libgstrtsp.la
%{_libdir}/gstreamer-1.0/libgstrtsp.so
%{_libdir}/gstreamer-1.0/libgstshapewipe.la
%{_libdir}/gstreamer-1.0/libgstshapewipe.so
%{_libdir}/gstreamer-1.0/libgstsmpte.la
%{_libdir}/gstreamer-1.0/libgstsmpte.so
%{_libdir}/gstreamer-1.0/libgstsouphttpsrc.la
%{_libdir}/gstreamer-1.0/libgstsouphttpsrc.so
%{_libdir}/gstreamer-1.0/libgstspectrum.la
%{_libdir}/gstreamer-1.0/libgstspectrum.so
%{_libdir}/gstreamer-1.0/libgstspeex.la
%{_libdir}/gstreamer-1.0/libgstspeex.so
%{_libdir}/gstreamer-1.0/libgsttaglib.la
%{_libdir}/gstreamer-1.0/libgsttaglib.so
%{_libdir}/gstreamer-1.0/libgstudp.la
%{_libdir}/gstreamer-1.0/libgstudp.so
%{_libdir}/gstreamer-1.0/libgstvideo4linux2.la
%{_libdir}/gstreamer-1.0/libgstvideo4linux2.so
%{_libdir}/gstreamer-1.0/libgstvideobox.la
%{_libdir}/gstreamer-1.0/libgstvideobox.so
%{_libdir}/gstreamer-1.0/libgstvideocrop.la
%{_libdir}/gstreamer-1.0/libgstvideocrop.so
%{_libdir}/gstreamer-1.0/libgstvideofilter.la
%{_libdir}/gstreamer-1.0/libgstvideofilter.so
%{_libdir}/gstreamer-1.0/libgstvideomixer.la
%{_libdir}/gstreamer-1.0/libgstvideomixer.so
%{_libdir}/gstreamer-1.0/libgstvpx.la
%{_libdir}/gstreamer-1.0/libgstvpx.so
%{_libdir}/gstreamer-1.0/libgstwavenc.la
%{_libdir}/gstreamer-1.0/libgstwavenc.so
%{_libdir}/gstreamer-1.0/libgstwavparse.la
%{_libdir}/gstreamer-1.0/libgstwavparse.so
%{_libdir}/gstreamer-1.0/libgstximagesrc.la
%{_libdir}/gstreamer-1.0/libgstximagesrc.so
%{_libdir}/gstreamer-1.0/libgsty4menc.la
%{_libdir}/gstreamer-1.0/libgsty4menc.so
%{_datadir}/gstreamer-1.0/presets/GstIirEqualizer10Bands.prs
%{_datadir}/gstreamer-1.0/presets/GstIirEqualizer3Bands.prs
%{_datadir}/gstreamer-1.0/presets/GstVP8Enc.prs
%{_datadir}/gtk-doc/html/gst-plugins-good-plugins-1.0/*
%{_datadir}/locale/*/LC_MESSAGES/gst-plugins-good-1.0.mo

%clean
rm -rf %{buildroot}

%changelog
* Tue Sep 22 2015 tanggeliang <tanggeliang@gmail.com>
- update from 1.4.3 to 1.5.91
* Fri Sep 26 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.4.0 to 1.4.3
* Sat Jul 26 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.2.4 to 1.4.0
* Sat Jul 26 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.2.3 to 1.2.4
* Sat Mar 22 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.2.1 to 1.2.3
* Tue Dec 3 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.2.0 to 1.2.1
* Mon Oct 7 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.0.9 to 1.2.0
* Sun Aug 18 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.0.8 to 1.0.9
* Wed Jul 17 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.0.5 to 1.0.8
* Sun Mar 17 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.0.2 to 1.0.5
* Fri Nov 2 2012 tanggeliang <tanggeliang@gmail.com>
- update from 1.0.1 to 1.0.2
* Sun Oct 14 2012 tanggeliang <tanggeliang@gmail.com>
- update from 1.0.0 to 1.0.1
* Sat Sep 29 2012 tanggeliang <tanggeliang@gmail.com>
- update from 0.10.31 to 1.0.0
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
