Name:       gst-libav
Version:    1.4.0
Release:    1%{?dist}
Summary:    GStreamer Libav

Group:      Applications/Multimedia
License:    GPLv2+
Url:        http://www.freedesktop.org
Source:     http://gstreamer.freedesktop.org/src/%{name}/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  gst-plugins-base yasm

%description
 The GStreamer Libav package contains GStreamer plugins for Libav (a fork of FFmpeg).

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --with-package-name="GStreamer Libav Plugins %{version} BLFS" \
            --with-package-origin="http://www.linuxfromscratch.org/blfs/view/svn/" \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_libdir}/gstreamer-1.0/libgstlibav.la
%{_libdir}/gstreamer-1.0/libgstlibav.so
%{_datadir}/gtk-doc/html/gst-libav-plugins-1.0/*

%clean
rm -rf %{buildroot}

%changelog
* Sat Jul 26 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.2.4 to 1.4.0
* Sat Jul 26 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.2.3 to 1.2.4
* Sat Mar 22 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.2.1 to 1.2.3
* Sun Dec 15 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.2.0 to 1.2.1
* Mon Oct 7 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.0.9 to 1.2.0
* Wed Aug 7 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.0.8 to 1.0.9
* Wed Jul 17 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.0.2 to 1.0.8
* Fri Nov 2 2012 tanggeliang <tanggeliang@gmail.com>
- update from 1.0.1 to 1.0.2
* Sun Oct 14 2012 tanggeliang <tanggeliang@gmail.com>
- update from 1.0.0 to 1.0.1
* Sat Sep 29 2012 tanggeliang <tanggeliang@gmail.com>
- create
