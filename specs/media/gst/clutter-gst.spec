Name:       clutter-gst
Version:    3.0.10
Release:    1%{?dist}
Summary:    Clutter Gst

Group:      System Environment/xlib
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/%{name}/3.0/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  clutter gst-plugins-base
BuildRequires:  gobject-introspection gst-plugins-bad cogl-gst
BuildRequires:  gtk-doc mesa

%description
The Clutter Gst is an integration library for using GStreamer with Clutter.
Its purpose is to implement the ClutterMedia interface using GStreamer.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_includedir}/clutter-gst-3.0/clutter-gst/clutter-gst-*.h
%{_includedir}/clutter-gst-3.0/clutter-gst/clutter-gst.h
%{_libdir}/girepository-1.0/ClutterGst-3.0.typelib
%{_libdir}/gstreamer-1.0/libgstclutter-3.0.*
%{_libdir}/libclutter-gst-3.0.la
%{_libdir}/libclutter-gst-3.0.so*
%{_libdir}/pkgconfig/clutter-gst-3.0.pc
%{_datadir}/gir-1.0/ClutterGst-3.0.gir

%files doc
%defattr(-,root,root,-)
%{_datadir}/gtk-doc/html/clutter-gst-3.0/*

%clean
rm -rf %{buildroot}

%changelog
* Tue Sep 22 2015 tanggeliang <tanggeliang@gmail.com>
- update from 2.0.12 to 3.0.10
* Sun Jul 27 2014 tanggeliang <tanggeliang@gmail.com>
- update from 2.0.10 to 2.0.12
* Sun Mar 16 2014 tanggeliang <tanggeliang@gmail.com>
- updare from 2.0.8 to 2.0.10
* Wed Sep 25 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.0.6 to 2.0.8
* Wed Aug 14 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.0.4 to 2.0.6
* Wed Jul 17 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.0.2 to 2.0.4
* Tue Mar 19 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.9.92 to 2.0.2
* Fri Sep 28 2012 tanggeliang <tanggeliang@gmail.com>
- update from 1.6.0 to 1.9.92
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
