Name:       gst-plugins-ugly
Version:    1.5.91
Release:    1%{?dist}
Summary:    GStreamer Ugly Plug-ins

Group:      Applications/Multimedia
License:    GPLv2+
Url:        http://www.freedesktop.org
Source:     http://gstreamer.freedesktop.org/src/%{name}/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  gst-plugins-base
BuildRequires:  lame libdvdread
BuildRequires:  liba52 libmad libmpeg2
BuildRequires:  gtk-doc

%description
The GStreamer Ugly Plug-ins is a set of plug-ins considered by the GStreamer developers to have good quality and correct functionality, but distributing them might pose problems.
The license on either the plug-ins or the supporting libraries might not be how the GStreamer developers would like.
The code might be widely known to present patent problems.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --with-package-name="GStreamer Ugly Plugins %{version} BLFS" \
            --with-package-origin="http://www.linuxfromscratch.org/blfs/view/svn/" \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_libdir}/gstreamer-1.0/libgsta52dec.la
%{_libdir}/gstreamer-1.0/libgsta52dec.so
%{_libdir}/gstreamer-1.0/libgstasf.la
%{_libdir}/gstreamer-1.0/libgstasf.so
%{_libdir}/gstreamer-1.0/libgstdvdlpcmdec.la
%{_libdir}/gstreamer-1.0/libgstdvdlpcmdec.so
%{_libdir}/gstreamer-1.0/libgstdvdread.la
%{_libdir}/gstreamer-1.0/libgstdvdread.so
%{_libdir}/gstreamer-1.0/libgstdvdsub.la
%{_libdir}/gstreamer-1.0/libgstdvdsub.so
%{_libdir}/gstreamer-1.0/libgstlame.la
%{_libdir}/gstreamer-1.0/libgstlame.so
%{_libdir}/gstreamer-1.0/libgstmad.la
%{_libdir}/gstreamer-1.0/libgstmad.so
%{_libdir}/gstreamer-1.0/libgstmpeg2dec.la
%{_libdir}/gstreamer-1.0/libgstmpeg2dec.so
%{_libdir}/gstreamer-1.0/libgstrmdemux.la
%{_libdir}/gstreamer-1.0/libgstrmdemux.so
%{_libdir}/gstreamer-1.0/libgstxingmux.la
%{_libdir}/gstreamer-1.0/libgstxingmux.so
%{_datadir}/gtk-doc/html/gst-plugins-ugly-plugins-1.0/*
%{_datadir}/locale/*/LC_MESSAGES/gst-plugins-ugly-1.0.mo

%clean
rm -rf %{buildroot}

%changelog
* Tue Sep 22 2015 tanggeliang <tanggeliang@gmail.com>
- update from 1.4.3 to 1.5.91
* Fri Sep 26 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.4.0 to 1.4.3
* Sat Jul 26 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.2.4 to 1.4.0
* Sat Jul 26 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.2.3 to 1.2.4
* Sat Mar 22 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.2.1 to 1.2.3
* Tue Dec 3 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.2.0 to 1.2.1
* Mon Oct 7 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.0.9 to 1.2.0
* Sun Aug 18 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.0.8 to 1.0.9
* Wed Jul 17 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.0.5 to 1.0.8
* Sun Mar 17 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.0.2 to 1.0.5
* Fri Nov 2 2012 tanggeliang <tanggeliang@gmail.com>
- update from 1.0.1 to 1.0.2
* Sun Oct 14 2012 tanggeliang <tanggeliang@gmail.com>
- update from 1.0.0 to 1.0.1
* Sat Sep 29 2012 tanggeliang <tanggeliang@gmail.com>
- update from 0.10.19 to 1.0.0
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
