Name:       gstreamer
Version:    1.5.91
Release:    1%{?dist}
Summary:    GStreamer

Group:      Applications/Multimedia
License:    GPLv2+
Url:        http://www.freedesktop.org
Source:     http://gstreamer.freedesktop.org/src/%{name}/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  glib
BuildRequires:  gobject-introspection
BuildRequires:  gsl gtk-doc
BuildRequires:  bison flex

%description
GStreamer is a streaming media framework that enables applications to share a common set of plugins for things like video encoding and decoding, audio encoding and decoding, audio and video filters, audio visualisation, web streaming and anything else that streams in real-time or otherwise.
This package only provides base functionality and libraries.
You will need at least gst-plugins-base-1.0.1 and one of Good, Bad, Ugly or Libav plugins.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --with-package-name="GStreamer %{version} BLFS" \
            --with-package-origin="http://www.linuxfromscratch.org/blfs/view/svn/" \
            --libexecdir=%{_libdir} \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/gst-inspect-1.0
%{_bindir}/gst-launch-1.0
%{_bindir}/gst-typefind-1.0
%{_includedir}/gstreamer-1.0/gst/base/*.h
%{_includedir}/gstreamer-1.0/gst/check/*.h
%{_includedir}/gstreamer-1.0/gst/controller/*.h
%{_includedir}/gstreamer-1.0/gst/*.h
%{_includedir}/gstreamer-1.0/gst/net/*.h
%{_libdir}/girepository-1.0/Gst*-1.0.typelib
%{_libdir}/gstreamer-1.0/gst-plugin-scanner
%{_libdir}/gstreamer-1.0/libgstcoreelements.*
%{_libdir}/gstreamer-1.0/gst-ptp-helper
%{_libdir}/gstreamer-1.0/include/gst/gstconfig.h
%{_libdir}/libgstbase-1.0.*
%{_libdir}/libgstcheck-1.0.*
%{_libdir}/libgstcontroller-1.0.*
%{_libdir}/libgstnet-1.0.*
%{_libdir}/libgstreamer-1.0.*
%{_libdir}/pkgconfig/gstreamer-1.0.pc
%{_libdir}/pkgconfig/gstreamer-base-1.0.pc
%{_libdir}/pkgconfig/gstreamer-check-1.0.pc
%{_libdir}/pkgconfig/gstreamer-controller-1.0.pc
%{_libdir}/pkgconfig/gstreamer-net-1.0.pc
%{_datadir}/bash-completion/completions/gst-inspect-1.0
%{_datadir}/bash-completion/completions/gst-launch-1.0
%{_datadir}/bash-completion/helpers/gst
%{_datadir}/bash-completion/helpers/gst-completion-helper-1.0
%{_datadir}/aclocal/gst-element-check-1.0.m4
%{_datadir}/gir-1.0/Gst*-1.0.gir
%{_datadir}/locale/*/LC_MESSAGES/gstreamer-1.0.mo

%files doc
%defattr(-,root,root,-)
%{_datadir}/gtk-doc/html/gstreamer-1.0/*
%{_datadir}/gtk-doc/html/gstreamer-libs-1.0/*
%{_datadir}/gtk-doc/html/gstreamer-plugins-1.0/*
%{_mandir}/man1/gst-*-1.0.1.gz

%clean
rm -rf %{buildroot}

%changelog
* Tue Sep 22 2015 tanggeliang <tanggeliang@gmail.com>
- update from 1.5.90 to 1.5.91
* Wed Sep 16 2015 tanggeliang <tanggeliang@gmail.com>
- update from 1.4.3 to 1.5.90
* Fri Sep 26 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.4.0 to 1.4.3
* Sat Jul 26 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.2.4 to 1.4.0
* Sat Jul 26 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.2.3 to 1.2.4
* Sat Mar 22 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.2.1 to 1.2.3
* Tue Dec 3 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.2.0 to 1.2.1
* Mon Oct 7 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.0.9 to 1.2.0
* Sun Aug 18 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.0.8 to 1.0.9
* Wed Jul 17 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.0.6 to 1.0.8
* Tue Apr 16 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.0.5 to 1.0.6
* Sun Mar 17 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.0.2 to 1.0.5
* Fri Nov 2 2012 tanggeliang <tanggeliang@gmail.com>
- update from 1.0.1 to 1.0.2
* Sun Oct 14 2012 tanggeliang <tanggeliang@gmail.com>
- update from 1.0.0 to 1.0.1
* Sat Sep 29 2012 tanggeliang <tanggeliang@gmail.com>
- update from 0.10.36 to 1.0.0
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
