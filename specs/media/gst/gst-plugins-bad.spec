Name:       gst-plugins-bad
Version:    1.5.91
Release:    1%{?dist}
Summary:    Gstreamer Bad Plug-ins

Group:      Applications/Multimedia
License:    GPLv2+
Url:        http://www.freedesktop.org
Source:     http://gstreamer.freedesktop.org/src/%{name}/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  gst-plugins-base
BuildRequires:  libdvdread libdvdnav soundtouch
BuildRequires:  curl faac faad gtk+ libexif libmpeg2 mesa mpg123 neon openssl libx11
BuildRequires:  gettext glu

%description
The GStreamer Bad Plug-ins package contains a set a set of plug-ins that aren't up to par compared to the rest.
They might be close to being good quality, but they're missing something - be it a good code review, some documentation, a set of tests, a real live maintainer, or some actual wide use.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --with-package-name="GStreamer Bad Plugins %{version} BLFS" \
            --with-package-origin="http://www.linuxfromscratch.org/blfs/view/svn/" \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_includedir}/gstreamer-1.0/gst/basecamerabinsrc/gst*.h
%{_includedir}/gstreamer-1.0/gst/codecparsers/gst*.h
%{_includedir}/gstreamer-1.0/gst/insertbin/gstinsertbin.h
%{_includedir}/gstreamer-1.0/gst/interfaces/photography*.h
%{_includedir}/gstreamer-1.0/gst/mpegts/*.h
%{_includedir}/gstreamer-1.0/gst/uridownloader/gst*.h
%{_includedir}/gstreamer-1.0/gst/gl/egl/*.h
%{_includedir}/gstreamer-1.0/gst/gl/*.h
%{_includedir}/gstreamer-1.0/gst/gl/glprototypes/*.h
%{_includedir}/gstreamer-1.0/gst/gl/x11/gstgldisplay_x11.h
%{_libdir}/gstreamer-1.0/include/gst/gl/gstglconfig.h
%{_libdir}/gstreamer-1.0/libgstaccurip.la
%{_libdir}/gstreamer-1.0/libgstaccurip.so
%{_libdir}/gstreamer-1.0/libgstadpcmdec.la
%{_libdir}/gstreamer-1.0/libgstadpcmdec.so
%{_libdir}/gstreamer-1.0/libgstadpcmenc.la
%{_libdir}/gstreamer-1.0/libgstadpcmenc.so
%{_libdir}/gstreamer-1.0/libgstaiff.la
%{_libdir}/gstreamer-1.0/libgstaiff.so
%{_libdir}/gstreamer-1.0/libgstasfmux.la
%{_libdir}/gstreamer-1.0/libgstasfmux.so
%{_libdir}/gstreamer-1.0/libgstaudiofxbad.la
%{_libdir}/gstreamer-1.0/libgstaudiofxbad.so
%{_libdir}/gstreamer-1.0/libgstaudiovisualizers.la
%{_libdir}/gstreamer-1.0/libgstaudiovisualizers.so
%{_libdir}/gstreamer-1.0/libgstautoconvert.la
%{_libdir}/gstreamer-1.0/libgstautoconvert.so
%{_libdir}/gstreamer-1.0/libgstbayer.la
%{_libdir}/gstreamer-1.0/libgstbayer.so
%{_libdir}/gstreamer-1.0/libgstbz2.la
%{_libdir}/gstreamer-1.0/libgstbz2.so
%{_libdir}/gstreamer-1.0/libgstcamerabin2.la
%{_libdir}/gstreamer-1.0/libgstcamerabin2.so
%{_libdir}/gstreamer-1.0/libgstcoloreffects.la
%{_libdir}/gstreamer-1.0/libgstcoloreffects.so
%{_libdir}/gstreamer-1.0/libgstcurl.la
%{_libdir}/gstreamer-1.0/libgstcurl.so
%{_libdir}/gstreamer-1.0/libgstdashdemux.la
%{_libdir}/gstreamer-1.0/libgstdashdemux.so
%{_libdir}/gstreamer-1.0/libgstdataurisrc.la
%{_libdir}/gstreamer-1.0/libgstdataurisrc.so
%{_libdir}/gstreamer-1.0/libgstdebugutilsbad.la
%{_libdir}/gstreamer-1.0/libgstdebugutilsbad.so
%{_libdir}/gstreamer-1.0/libgstdecklink.la
%{_libdir}/gstreamer-1.0/libgstdecklink.so
%{_libdir}/gstreamer-1.0/libgstdvb.la
%{_libdir}/gstreamer-1.0/libgstdvb.so
%{_libdir}/gstreamer-1.0/libgstdvbsuboverlay.la
%{_libdir}/gstreamer-1.0/libgstdvbsuboverlay.so
%{_libdir}/gstreamer-1.0/libgstdvdspu.la
%{_libdir}/gstreamer-1.0/libgstdvdspu.so
%{_libdir}/gstreamer-1.0/libgstfaac.la
%{_libdir}/gstreamer-1.0/libgstfaac.so
%{_libdir}/gstreamer-1.0/libgstfaad.la
%{_libdir}/gstreamer-1.0/libgstfaad.so
%{_libdir}/gstreamer-1.0/libgstfbdevsink.la
%{_libdir}/gstreamer-1.0/libgstfbdevsink.so
%{_libdir}/gstreamer-1.0/libgstfestival.la
%{_libdir}/gstreamer-1.0/libgstfestival.so
%{_libdir}/gstreamer-1.0/libgstfieldanalysis.la
%{_libdir}/gstreamer-1.0/libgstfieldanalysis.so
%{_libdir}/gstreamer-1.0/libgstfragmented.la
%{_libdir}/gstreamer-1.0/libgstfragmented.so
%{_libdir}/gstreamer-1.0/libgstfreeverb.la
%{_libdir}/gstreamer-1.0/libgstfreeverb.so
%{_libdir}/gstreamer-1.0/libgstfrei0r.la
%{_libdir}/gstreamer-1.0/libgstfrei0r.so
%{_libdir}/gstreamer-1.0/libgstgaudieffects.la
%{_libdir}/gstreamer-1.0/libgstgaudieffects.so
%{_libdir}/gstreamer-1.0/libgstgdp.la
%{_libdir}/gstreamer-1.0/libgstgdp.so
%{_libdir}/gstreamer-1.0/libgstgeometrictransform.la
%{_libdir}/gstreamer-1.0/libgstgeometrictransform.so
%{_libdir}/gstreamer-1.0/libgstid3tag.la
%{_libdir}/gstreamer-1.0/libgstid3tag.so
%{_libdir}/gstreamer-1.0/libgstinter.la
%{_libdir}/gstreamer-1.0/libgstinter.so
%{_libdir}/gstreamer-1.0/libgstinterlace.la
%{_libdir}/gstreamer-1.0/libgstinterlace.so
%{_libdir}/gstreamer-1.0/libgstivtc.la
%{_libdir}/gstreamer-1.0/libgstivtc.so
%{_libdir}/gstreamer-1.0/libgstjpegformat.la
%{_libdir}/gstreamer-1.0/libgstjpegformat.so
%{_libdir}/gstreamer-1.0/libgstliveadder.la
%{_libdir}/gstreamer-1.0/libgstliveadder.so
%{_libdir}/gstreamer-1.0/libgstmidi.*
%{_libdir}/gstreamer-1.0/libgstmpegpsdemux.la
%{_libdir}/gstreamer-1.0/libgstmpegpsdemux.so
%{_libdir}/gstreamer-1.0/libgstmpegpsmux.la
%{_libdir}/gstreamer-1.0/libgstmpegpsmux.so
%{_libdir}/gstreamer-1.0/libgstmpegtsdemux.la
%{_libdir}/gstreamer-1.0/libgstmpegtsdemux.so
%{_libdir}/gstreamer-1.0/libgstmpegtsmux.la
%{_libdir}/gstreamer-1.0/libgstmpegtsmux.so
%{_libdir}/gstreamer-1.0/libgstmpg123.la
%{_libdir}/gstreamer-1.0/libgstmpg123.so
%{_libdir}/gstreamer-1.0/libgstneonhttpsrc.la
%{_libdir}/gstreamer-1.0/libgstneonhttpsrc.so
%{_libdir}/gstreamer-1.0/libgstpcapparse.la
%{_libdir}/gstreamer-1.0/libgstpcapparse.so
%{_libdir}/gstreamer-1.0/libgstpnm.la
%{_libdir}/gstreamer-1.0/libgstpnm.so
%{_libdir}/gstreamer-1.0/libgstrawparse.la
%{_libdir}/gstreamer-1.0/libgstrawparse.so
%{_libdir}/gstreamer-1.0/libgstremovesilence.la
%{_libdir}/gstreamer-1.0/libgstremovesilence.so
%{_libdir}/gstreamer-1.0/libgstresindvd.la
%{_libdir}/gstreamer-1.0/libgstresindvd.so
%{_libdir}/gstreamer-1.0/libgstrfbsrc.la
%{_libdir}/gstreamer-1.0/libgstrfbsrc.so
%{_libdir}/gstreamer-1.0/libgstsdpelem.la
%{_libdir}/gstreamer-1.0/libgstsdpelem.so
%{_libdir}/gstreamer-1.0/libgstsegmentclip.la
%{_libdir}/gstreamer-1.0/libgstsegmentclip.so
%{_libdir}/gstreamer-1.0/libgstshm.la
%{_libdir}/gstreamer-1.0/libgstshm.so
%{_libdir}/gstreamer-1.0/libgstsiren.la
%{_libdir}/gstreamer-1.0/libgstsiren.so
%{_libdir}/gstreamer-1.0/libgstsmooth.la
%{_libdir}/gstreamer-1.0/libgstsmooth.so
%{_libdir}/gstreamer-1.0/libgstsmoothstreaming.la
%{_libdir}/gstreamer-1.0/libgstsmoothstreaming.so
%{_libdir}/gstreamer-1.0/libgstsoundtouch.la
%{_libdir}/gstreamer-1.0/libgstsoundtouch.so
%{_libdir}/gstreamer-1.0/libgstspeed.la
%{_libdir}/gstreamer-1.0/libgstspeed.so
%{_libdir}/gstreamer-1.0/libgstsubenc.la
%{_libdir}/gstreamer-1.0/libgstsubenc.so
%{_libdir}/gstreamer-1.0/libgstvideofiltersbad.la
%{_libdir}/gstreamer-1.0/libgstvideofiltersbad.so
%{_libdir}/gstreamer-1.0/libgstvideoparsersbad.la
%{_libdir}/gstreamer-1.0/libgstvideoparsersbad.so
%{_libdir}/gstreamer-1.0/libgsty4mdec.la
%{_libdir}/gstreamer-1.0/libgsty4mdec.so
%{_libdir}/gstreamer-1.0/libgstwaylandsink.la
%{_libdir}/gstreamer-1.0/libgstwaylandsink.so
%{_libdir}/gstreamer-1.0/libgstyadif.la
%{_libdir}/gstreamer-1.0/libgstyadif.so
%{_libdir}/gstreamer-1.0/libgstaudiomixer.la
%{_libdir}/gstreamer-1.0/libgstaudiomixer.so
%{_libdir}/gstreamer-1.0/libgstcompositor.la
%{_libdir}/gstreamer-1.0/libgstcompositor.so
%{_libdir}/gstreamer-1.0/libgstdfbvideosink.la
%{_libdir}/gstreamer-1.0/libgstdfbvideosink.so
%{_libdir}/gstreamer-1.0/libgstivfparse.la
%{_libdir}/gstreamer-1.0/libgstivfparse.so
%{_libdir}/gstreamer-1.0/libgstjp2kdecimator.la
%{_libdir}/gstreamer-1.0/libgstjp2kdecimator.so
%{_libdir}/gstreamer-1.0/libgstmxf.la
%{_libdir}/gstreamer-1.0/libgstmxf.so
%{_libdir}/gstreamer-1.0/libgstsndfile.la
%{_libdir}/gstreamer-1.0/libgstsndfile.so
%{_libdir}/gstreamer-1.0/libgststereo.la
%{_libdir}/gstreamer-1.0/libgststereo.so
%{_libdir}/gstreamer-1.0/libgstvideosignal.la
%{_libdir}/gstreamer-1.0/libgstvideosignal.so
%{_libdir}/gstreamer-1.0/libgstvmnc.la
%{_libdir}/gstreamer-1.0/libgstvmnc.so
%{_libdir}/gstreamer-1.0/libgstdtls.la
%{_libdir}/gstreamer-1.0/libgstdtls.so
%{_libdir}/gstreamer-1.0/libgstgtksink.la
%{_libdir}/gstreamer-1.0/libgstgtksink.so
%{_libdir}/gstreamer-1.0/libgstopengl.la
%{_libdir}/gstreamer-1.0/libgstopengl.so
%{_libdir}/gstreamer-1.0/libgstrtpbad.la
%{_libdir}/gstreamer-1.0/libgstrtpbad.so
%{_libdir}/gstreamer-1.0/libgstrtponvif.la
%{_libdir}/gstreamer-1.0/libgstrtponvif.so
%{_libdir}/gstreamer-1.0/libgstvcdsrc.la
%{_libdir}/gstreamer-1.0/libgstvcdsrc.so
%{_libdir}/libgstbasecamerabinsrc-1.0.la
%{_libdir}/libgstbasecamerabinsrc-1.0.so*
%{_libdir}/libgstcodecparsers-1.0.la
%{_libdir}/libgstcodecparsers-1.0.so*
%{_libdir}/libgstbadbase-1.0.la
%{_libdir}/libgstbadbase-1.0.so*
%{_libdir}/libgstbadvideo-1.0.la
%{_libdir}/libgstbadvideo-1.0.so*
%{_libdir}/libgstinsertbin-1.0.la
%{_libdir}/libgstinsertbin-1.0.so*
%{_libdir}/libgstmpegts-1.0.la
%{_libdir}/libgstmpegts-1.0.so*
%{_libdir}/libgstphotography-1.0.la
%{_libdir}/libgstphotography-1.0.so*
%{_libdir}/libgsturidownloader-1.0.la
%{_libdir}/libgsturidownloader-1.0.so*
%{_libdir}/libgstwayland-1.0.la
%{_libdir}/libgstwayland-1.0.so*
%{_libdir}/libgstadaptivedemux-1.0.la
%{_libdir}/libgstadaptivedemux-1.0.so*
%{_libdir}/libgstgl-1.0.la
%{_libdir}/libgstgl-1.0.so*
%{_libdir}/pkgconfig/gstreamer-codecparsers-1.0.pc
%{_libdir}/pkgconfig/gstreamer-insertbin-1.0.pc
%{_libdir}/pkgconfig/gstreamer-mpegts-1.0.pc
%{_libdir}/pkgconfig/gstreamer-plugins-bad-1.0.pc
%{_libdir}/pkgconfig/gstreamer-gl-1.0.pc
%{_datadir}/gtk-doc/html/gst-plugins-bad-plugins-1.0/*
%{_datadir}/locale/*/LC_MESSAGES/gst-plugins-bad-1.0.mo
%{_datadir}/gstreamer-1.0/presets/GstFreeverb.prs

%files doc
%defattr(-,root,root,-)
%{_datadir}/gtk-doc/html/gst-plugins-bad-libs-1.0/*

%clean
rm -rf %{buildroot}

%changelog
* Tue Sep 22 2015 tanggeliang <tanggeliang@gmail.com>
- update from 1.4.3 to 1.5.91
* Fri Sep 26 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.4.0 to 1.4.3
* Sat Jul 26 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.2.4 to 1.4.0
* Sat Jul 26 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.2.3 to 1.2.4
* Sat Mar 22 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.2.1 to 1.2.3
* Tue Dec 3 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.2.0 to 1.2.1
* Mon Oct 7 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.0.9 to 1.2.0
* Sun Aug 18 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.0.8 to 1.0.9
* Wed Jul 17 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.0.5 to 1.0.8
* Sun Mar 17 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.0.2 to 1.0.5
* Fri Nov 2 2012 tanggeliang <tanggeliang@gmail.com>
- update from 1.0.1 to 1.0.2
* Sun Oct 14 2012 tanggeliang <tanggeliang@gmail.com>
- update from 1.0.0 to 1.0.1
* Sat Sep 29 2012 tanggeliang <tanggeliang@gmail.com>
- update from 0.10.23 to 1.0.0
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
