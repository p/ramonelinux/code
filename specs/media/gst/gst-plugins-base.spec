Name:       gst-plugins-base
Version:    1.5.91
Release:    1%{?dist}
Summary:    GStreamer Base Plug-ins

Group:      Applications/Multimedia
License:    GPLv2+
Url:        http://www.freedesktop.org
Source:     http://gstreamer.freedesktop.org/src/%{name}/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  gstreamer libxml
BuildRequires:  alsa-lib gobject-introspection iso-codes libogg libtheora libvorbis pango
BuildRequires:  libx11 libice libxv libxext libsm
BuildRequires:  gtk+ gtk-doc gettext

%description
The GStreamer Base Plug-ins is a well-groomed and well-maintained collection of GStreamer plug-ins and elements, spanning the range of possible types of elements one would want to write for GStreamer.
You will need at least one of Good, Bad, Ugly or Libav plugins for GStreamer applications to function properly.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --with-package-name="GStreamer Base Plugins %{version} BLFS" \
            --with-package-origin="http://www.linuxfromscratch.org/blfs/view/svn/" \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/gst-device-monitor-1.0
%{_bindir}/gst-discoverer-1.0
%{_bindir}/gst-play-1.0
%{_includedir}/gstreamer-1.0/gst/*/*.h
%{_libdir}/girepository-1.0/Gst*-1.0.typelib
%{_libdir}/gstreamer-1.0/libgst*.la
%{_libdir}/gstreamer-1.0/libgst*.so
%{_libdir}/libgstallocators-1.0.*
%{_libdir}/libgstapp-1.0.*
%{_libdir}/libgstaudio-1.0.*
%{_libdir}/libgstfft-1.0.*
%{_libdir}/libgstpbutils-1.0.*
%{_libdir}/libgstriff-1.0.*
%{_libdir}/libgstrtp-1.0.*
%{_libdir}/libgstrtsp-1.0.*
%{_libdir}/libgstsdp-1.0.*
%{_libdir}/libgsttag-1.0.*
%{_libdir}/libgstvideo-1.0.*
%{_libdir}/pkgconfig/gstreamer-allocators-1.0.pc
%{_libdir}/pkgconfig/gstreamer-app-1.0.pc
%{_libdir}/pkgconfig/gstreamer-audio-1.0.pc
%{_libdir}/pkgconfig/gstreamer-fft-1.0.pc
%{_libdir}/pkgconfig/gstreamer-pbutils-1.0.pc
%{_libdir}/pkgconfig/gstreamer-plugins-base-1.0.pc
%{_libdir}/pkgconfig/gstreamer-riff-1.0.pc
%{_libdir}/pkgconfig/gstreamer-rtp-1.0.pc
%{_libdir}/pkgconfig/gstreamer-rtsp-1.0.pc
%{_libdir}/pkgconfig/gstreamer-sdp-1.0.pc
%{_libdir}/pkgconfig/gstreamer-tag-1.0.pc
%{_libdir}/pkgconfig/gstreamer-video-1.0.pc
%{_datadir}/gir-1.0/Gst*-1.0.gir
%{_datadir}/gst-plugins-base/1.0/license-translations.dict
%{_datadir}/locale/*/LC_MESSAGES/gst-plugins-base-1.0.mo

%files doc
%defattr(-,root,root,-)
%{_datadir}/gtk-doc/html/gst-plugins-base-libs-1.0/*
%{_datadir}/gtk-doc/html/gst-plugins-base-plugins-1.0/*
%{_mandir}/man1/gst-device-monitor-1.0.1.gz
%{_mandir}/man1/gst-discoverer-1.0.1.gz
%{_mandir}/man1/gst-play-1.0.1.gz

%clean
rm -rf %{buildroot}

%changelog
* Tue Sep 22 2015 tanggeliang <tanggeliang@gmail.com>
- update from 1.4.3 to 1.5.91
* Fri Sep 26 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.4.0 to 1.4.3
* Sat Jul 26 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.2.4 to 1.4.0
* Sat Jul 26 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.2.3 to 1.2.4
* Sat Mar 22 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.2.1 to 1.2.3
* Tue Dec 3 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.2.0 to 1.2.1
* Mon Oct 7 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.0.9 to 1.2.0
* Sun Aug 18 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.0.8 to 1.0.9
* Wed Jul 17 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.0.6 to 1.0.8
* Tue Apr 16 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.0.5 to 1.0.6
* Sun Mar 17 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.0.2 to 1.0.5
* Fri Nov 2 2012 tanggeliang <tanggeliang@gmail.com>
- update from 1.0.1 to 1.0.2
* Sun Oct 14 2012 tanggeliang <tanggeliang@gmail.com>
- update from 1.0.0 to 1.0.1
* Sat Sep 29 2012 tanggeliang <tanggeliang@gmail.com>
- update from 0.10.36 to 1.0.0
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
