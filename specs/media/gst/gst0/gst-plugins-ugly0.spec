Name:       gst-plugins-ugly0
Version:    0.10.19
Release:    7%{?dist}
Summary:    GStreamer Ugly Plug-ins

Group:      Applications/Multimedia
License:    GPLv2+
Url:        http://www.freedesktop.org
Source:     http://gstreamer.freedesktop.org/src/gst-plugins-ugly/gst-plugins-ugly-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  gst-plugins-base0
BuildRequires:  lame libdvdnav libdvdread
BuildRequires:  liba52 libmad libmpeg2
BuildRequires:  gtk-doc

%description
The GStreamer Ugly Plug-ins is a set of plug-ins considered by the GStreamer developers to have good quality and correct functionality, but distributing them might pose problems.
The license on either the plug-ins or the supporting libraries might not be how the GStreamer developers would like.
The code might be widely known to present patent problems. Also see the gst-plugins-bad-0.10.23, gst-plugins-good-0.10.31 and gst-ffmpeg-0.10.13 packages.

%prep
%setup -q -n gst-plugins-ugly-%{version}

%build
./configure --prefix=/usr \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install
make -C docs/plugins install-data DESTDIR=%{buildroot}

%files
%defattr(-,root,root,-)
%{_libdir}/gstreamer-0.10/libgst*.*
%{_datadir}/locale/*/LC_MESSAGES/gst-plugins-ugly-0.10.mo
%{_datadir}/gtk-doc/html/gst-plugins-ugly-plugins-0.10/*

%clean
rm -rf %{buildroot}

%changelog
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
