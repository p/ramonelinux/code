Name:       gst-plugins-good0
Version:    0.10.31
Release:    3%{?dist}
Summary:    GStreamer Good Plug-ins

Group:      Applications/Multimedia
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/gst-plugins-good/0.10/gst-plugins-good-%{version}.tar.xz
Patch:      gst-plugins-good-0.10.31-v4l2-fix-build-with-recent-kernels.patch

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  gst-plugins-base0
BuildRequires:  cairo flac libjpeg-turbo libpng libx11
BuildRequires:  gconf gtk+ esound mesa

%description
The GStreamer Good Plug-ins is a set of plug-ins considered by the GStreamer developers to have good quality code, correct functionality, and the preferred license (LGPL for the plug-in code, LGPL or LGPL-compatible for the supporting library).
A wide range of video and audio decoders, encoders, and filters are included.
Also see the gst-plugins-ugly-0.10.19, gst-plugins-bad-0.10.23 and gst-ffmpeg-0.10.13 packages.

%prep
%setup -q -n gst-plugins-good-%{version}
%patch -p1

%build
sed -i -e '/input:/d' sys/v4l2/gstv4l2bufferpool.c &&
./configure --prefix=/usr \
            --sysconfdir=/etc \
            --with-gtk=3.0 \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
/etc/gconf/schemas/gstreamer-0.10.schemas
%{_libdir}/gstreamer-0.10/libgst*.*
%{_datadir}/gstreamer-0.10/presets/GstIirEqualizer*Bands.prs
%{_datadir}/locale/*/LC_MESSAGES/gst-plugins-good-0.10.mo

%clean
rm -rf %{buildroot}

%changelog
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
