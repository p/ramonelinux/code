Name:       gstreamer0
Version:    0.10.36
Release:    2%{?dist}
Summary:    GStreamer

Group:      Applications/Multimedia
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/gstreamer/0.10/gstreamer-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  glib, libxml
BuildRequires:  gobject-introspection
BuildRequires:  bison flex

%description
GStreamer is a streaming media framework that enables applications to share a common set of plugins for things like video decoding and encoding, audio encoding and decoding, audio and video filters, audio visualisation, Web streaming and anything else that streams in real-time or otherwise.
It is modelled after research software worked on at the Oregon Graduate Institute.
After installing GStreamer, you'll likely need to install one or more of the gst-plugins-bad-0.10.23, gst-plugins-good-0.10.31, gst-plugins-ugly-0.10.19 and gst-ffmpeg-0.10.13 packages.

%prep
%setup -q -n gstreamer-%{version}

%build
sed -i 's/\(.*gtkdoc-rebase --relative.* \)\(;.*\)/\1|| true\2/' \
  docs/{gst,libs}/Makefile.in &&
sed -i -e '/YYLEX_PARAM/d' \
    -e '/parse-param.*scanner/i %lex-param { void *scanner }' \
    gst/parse/grammar.y &&
./configure --prefix=/usr \
            --libexecdir=%{_libdir} \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/gst-feedback*
%{_bindir}/gst-inspect*
%{_bindir}/gst-launch*
%{_bindir}/gst-typefind*
%{_bindir}/gst-xmlinspect*
%{_bindir}/gst-xmllaunch*
%{_includedir}/gstreamer-0.10/gst/*
%{_libdir}/girepository-1.0/*
%{_libdir}/gstreamer-0.10/*
%{_libdir}/libgstbase-0.10.*
%{_libdir}/libgstcheck-0.10.*
%{_libdir}/libgstcontroller-0.10.*
%{_libdir}/libgstdataprotocol-0.10.*
%{_libdir}/libgstnet-0.10.*
%{_libdir}/libgstreamer-0.10.*
%{_libdir}/pkgconfig/gstreamer-0.10.pc
%{_libdir}/pkgconfig/gstreamer-base-0.10.pc
%{_libdir}/pkgconfig/gstreamer-check-0.10.pc
%{_libdir}/pkgconfig/gstreamer-controller-0.10.pc
%{_libdir}/pkgconfig/gstreamer-dataprotocol-0.10.pc
%{_libdir}/pkgconfig/gstreamer-net-0.10.pc
%{_datadir}/aclocal/gst-element-check-0.10.m4
%{_datadir}/gir-1.0/Gst-0.10.gir
%{_datadir}/gir-1.0/GstBase-0.10.gir
%{_datadir}/gir-1.0/GstCheck-0.10.gir
%{_datadir}/gir-1.0/GstController-0.10.gir
%{_datadir}/gir-1.0/GstNet-0.10.gir
%{_datadir}/gtk-doc/html/gstreamer-0.10/*
%{_datadir}/gtk-doc/html/gstreamer-libs-0.10/*
%{_datadir}/gtk-doc/html/gstreamer-plugins-0.10/*
%{_datadir}/locale/*
%{_mandir}/man1/*

%clean
rm -rf %{buildroot}

%changelog
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
