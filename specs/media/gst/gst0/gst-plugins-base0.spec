Name:       gst-plugins-base0
Version:    0.10.36
Release:    2%{?dist}
Summary:    GStreamer Base Plug-ins

Group:      Applications/Multimedia
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/gst-plugins-base/0.10/gst-plugins-base-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  gstreamer0 pango
BuildRequires:  alsa-lib libogg libtheora libvorbis systemd-udev
BuildRequires:  libx11 libice libxv libxext libsm
BuildRequires:  gobject-introspection gettext

%description
The GStreamer Base Plug-ins is a well-groomed and well-maintained collection of GStreamer plug-ins and elements, spanning the range of possible types of elements one would want to write for GStreamer.
It also contains helper libraries and base classes useful for writing elements.
A wide range of video and audio decoders, encoders, and filters are included.
Also see the gst-plugins-bad-0.10.23, gst-plugins-good-0.10.31, gst-plugins-ugly-0.10.19, and gst-ffmpeg-0.10.13 packages.

%prep
%setup -q -n gst-plugins-base-%{version}

%build
sed -i 's/\(.*gtkdoc-rebase --relative.* \)\(;.*\)/\1|| true\2/' \
  docs/libs/Makefile.in &&
./configure --prefix=/usr \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/gst-discoverer-0.10
%{_bindir}/gst-visualise-0.10
%{_includedir}/gstreamer-0.10/gst/*
%{_libdir}/girepository-1.0/Gst*-0.10.typelib
%{_libdir}/gstreamer-0.10/libgst*
%{_libdir}/libgst*
%{_libdir}/pkgconfig/gstreamer-*-0.10.pc
%{_datadir}/gir-1.0/Gst*-0.10.gir
%{_datadir}/gst-plugins-base/license-translations.dict
%{_datadir}/gtk-doc/html/gst-plugins-base-libs-0.10/*
%{_datadir}/gtk-doc/html/gst-plugins-base-plugins-0.10/*
%{_datadir}/locale/*
%{_mandir}/man1/gst-visualise-0.10.1.gz

%clean
rm -rf %{buildroot}

%changelog
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
