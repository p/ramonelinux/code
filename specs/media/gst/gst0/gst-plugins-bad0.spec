Name:       gst-plugins-bad0
Version:    0.10.23
Release:    7%{?dist}
Summary:    Gstreamer Bad Plug-ins

Group:      Applications/Multimedia
License:    GPLv2+
Url:        http://www.freedesktop.org
Source:     http://gstreamer.freedesktop.org/src/gst-plugins-bad/gst-plugins-bad-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  gst-plugins-base0
BuildRequires:  faac libpng libvpx openssl xvid
BuildRequires:  gtk+ gettext

%description
The GStreamer Bad Plug-ins package contains a set a set of plug-ins that aren't up to par compared to the rest.
They might be close to being good quality, but they're missing something - be it a good code review, some documentation, a set of tests, a real live maintainer, or some actual wide use.
Also see the gst-plugins-good-0.10.31, gst-plugins-ugly-0.10.19 and gst-ffmpeg-0.10.13 packages.

%prep
%setup -q -n gst-plugins-bad-%{version}

%build
./configure --prefix=/usr --with-gtk=3.0 --disable-examples \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_includedir}/gstreamer-0.10/gst/*/*.h
%{_libdir}/gstreamer-0.10/libgst*.*
%{_libdir}/libgst*.*
%{_libdir}/pkgconfig/gstreamer-basevideo-0.10.pc
%{_libdir}/pkgconfig/gstreamer-codecparsers-0.10.pc
%{_libdir}/pkgconfig/gstreamer-plugins-bad-0.10.pc
%{_datadir}/glib-2.0/schemas/org.freedesktop.gstreamer-0.10.default-elements.gschema.xml
%{_datadir}/gtk-doc/html/gst-plugins-bad-libs-0.10/*
%{_datadir}/locale/*/LC_MESSAGES/gst-plugins-bad-0.10.mo

%clean
rm -rf %{buildroot}

%changelog
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
