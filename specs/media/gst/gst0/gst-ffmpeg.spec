Name:       gst-ffmpeg
Version:    0.10.13
Release:    3%{?dist}
Summary:    Gst FFMpeg

Group:      Applications/Multimedia
License:    GPLv2+
Url:        http://www.freedesktop.org
Source:     http://gstreamer.freedesktop.org/src/gst-ffmpeg/%{name}-%{version}.tar.bz2
Patch:      gst-ffmpeg-0.10.13-gcc-4.7-1.patch

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  gst-plugins-base0 yasm

%description
The Gst FFMpeg contains GStreamer plugins for FFMpeg.

%prep
%setup -q -n %{name}-%{version}
%patch -p1

%build
./configure --prefix=/usr \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_libdir}/gstreamer-0.10/libgstffmpeg.*
%{_libdir}/gstreamer-0.10/libgstffmpegscale.*
%{_libdir}/gstreamer-0.10/libgstpostproc.*

%clean
rm -rf %{buildroot}

%changelog
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
