Name:       farstream
Version:    0.2.7
Release:    1%{?dist}
Summary:    Farstream

Group:      Applications/Multimedia
License:    GPLv2+
Url:        http://www.freedesktop.org
Source:     http://freedesktop.org/software/farstream/releases/farstream/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  gst-plugins-base libnice
BuildRequires:  gobject-introspection
BuildRequires:  gtk-doc
Requires:       gst-plugins-bad gst-plugins-good

%description
The Farstream package contains a collection of GStreamer modules and libraries used for videoconferencing.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_includedir}/farstream-0.2/farstream/fs-*.h
%{_libdir}/farstream-0.2/libmulticast-transmitter.*
%{_libdir}/farstream-0.2/libnice-transmitter.*
%{_libdir}/farstream-0.2/librawudp-transmitter.*
%{_libdir}/farstream-0.2/libshm-transmitter.*
%{_libdir}/girepository-1.0/Farstream-0.2.typelib
%{_libdir}/gstreamer-1.0/libfsmsnconference.*
%{_libdir}/gstreamer-1.0/libfsrawconference.*
%{_libdir}/gstreamer-1.0/libfsrtpconference.*
%{_libdir}/gstreamer-1.0/libfsrtpxdata.*
%{_libdir}/gstreamer-1.0/libfsvideoanyrate.*
%{_libdir}/libfarstream-0.2.*
%{_libdir}/pkgconfig/farstream-0.2.pc
%{_datadir}/farstream/0.2/fsrawconference/default-element-properties
%{_datadir}/farstream/0.2/fsrtpconference/default-codec-preferences
%{_datadir}/farstream/0.2/fsrtpconference/default-element-properties
%{_datadir}/gir-1.0/Farstream-0.2.gir
%{_datadir}/gtk-doc/html/farstream-libs-0.2/*
%{_datadir}/gtk-doc/html/farstream-plugins-0.2/*

%clean
rm -rf %{buildroot}

%changelog
* Wed Sep 23 2015 tanggeliang <tanggeliang@gmail.com>
- update from 0.2.3 to 0.2.7
* Fri May 10 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.2.1 to 0.2.3
* Sat Nov 3 2012 tanggeliang <tanggeliang@gmail.com>
- update from 0.2.0 to 0.2.1
* Sun Sep 30 2012 tanggeliang <tanggeliang@gmail.com>
- update from 0.1.2 to 0.2.0
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
