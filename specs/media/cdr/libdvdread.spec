Name:       libdvdread
Version:    4.2.1
Release:    1%{?dist}
Summary:    Libdvdread

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://dvdnav.mplayerhq.hu
Source:     http://dvdnav.mplayerhq.hu/releases/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  autoconf automake m4
BuildRequires:  libtool

%description
libdvdread is a library which provides a simple foundation for reading DVDs.

%prep
%setup -q -n %{name}-%{version}

%build
./autogen.sh --prefix=/usr \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/dvdread-config
%{_includedir}/dvdread/*.h
%{_libdir}/libdvdread.*
%{_libdir}/pkgconfig/dvdread.pc
%{_datadir}/aclocal/dvdread.m4

%clean
rm -rf %{buildroot}

%changelog
* Fri Mar 21 2014 tanggeliang <tanggeliang@gmail.com>
- update from 4.2.0 to 4.2.1
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
