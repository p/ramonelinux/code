Name:       libburn
Version:    1.3.8
Release:    1%{?dist}
Summary:    libburn

Group:      Applications/Multimedia
License:    GPLv2+
Url:        http://www.libburnia-project.org
Source:     http://files.libburnia-project.org/releases/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build

%description
libburn is a library for writing preformatted data onto optical media: CD, DVD and BD (Blu-Ray).

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --disable-static \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/cdrskin
%{_includedir}/libburn/libburn.h
%{_libdir}/libburn.*
%{_libdir}/pkgconfig/libburn-1.pc
%{_mandir}/man1/cdrskin.1.gz

%clean
rm -rf %{buildroot}

%changelog
* Sat Jul 26 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.3.6 to 1.3.8
* Fri Mar 21 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.3.4 to 1.3.6
* Tue Dec 17 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.3.2 to 1.3.4
* Thu Aug 22 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.3.0 to 1.3.2
* Wed Jul 31 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.2.6 to 1.3.0
* Tue Mar 19 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.2.4 to 1.2.6
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
