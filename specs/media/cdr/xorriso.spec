Name:       xorriso
Version:    1.3.4
Release:    1%{?dist}
Summary:    GNU xorriso

Group:      System Environment/Base
License:    GPLv2+
Url:        http://www.gnu.org
Source:     http://ftp.gnu.org/gnu/%{name}/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build

%description
xorriso copies file objects from POSIX compliant filesystems into Rock Ridge enhanced ISO 9660 filesystems and allows session-wise manipulation of such filesystems.
It can load the management information of existing ISO images and it writes the session results to optical media or to filesystem objects.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

rm -f %{buildroot}/%{_infodir}/dir

%files
%defattr(-,root,root,-)
%{_bindir}/osirrox
%{_bindir}/xorrecord
%{_bindir}/xorriso
%{_bindir}/xorriso-tcltk
%{_bindir}/xorrisofs

%files doc
%defattr(-,root,root,-)
%{_infodir}/xorrecord.info.gz
%{_infodir}/xorriso.info.gz
%{_infodir}/xorrisofs.info.gz
%{_mandir}/man1/xorrecord.1.gz
%{_mandir}/man1/xorriso.1.gz
%{_mandir}/man1/xorrisofs.1.gz

%clean
rm -rf %{buildroot}

%changelog
* Thu Dec 26 2013 tanggeliang <tanggeliang@gmail.com>
- create
