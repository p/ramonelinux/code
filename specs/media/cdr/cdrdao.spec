Name:       cdrdao
Version:    1.2.3
Release:    1%{?dist}
Summary:    cdrdao

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://cdrdao.sourceforge.net
Source:     http://downloads.sourceforge.net/cdrdao/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  libao libvorbis libmad lame

%description
The Cdrdao package contains CD recording utilities.
These are useful for burning a CD in disk-at-once mode.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
sed -i '/ioctl/a #include <sys/stat.h>' dao/ScsiIf-linux.cc &&
./configure --prefix=/usr --mandir=/usr/share/man &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install &&

install -v -m755 -d %{buildroot}/usr/share/doc/cdrdao-%{version} &&
install -v -m644 README %{buildroot}/usr/share/doc/cdrdao-%{version}

%files
%defattr(-,root,root,-)
%{_bindir}/cdrdao
%{_bindir}/cue2toc
%{_bindir}/toc2cddb
%{_bindir}/toc2cue
%{_bindir}/toc2mp3
%{_datadir}/cdrdao/drivers
%{_mandir}/man1/*.1.gz

%files doc
%defattr(-,root,root,-)
%{_docdir}/cdrdao-%{version}

%clean
rm -rf %{buildroot}

%changelog
* Tue Mar 19 2013 tanggeliang <tanggeliang@gmail.com>
- create
