Name:       libdvdnav
Version:    4.2.1
Release:    1%{?dist}
Summary:    Libdvdread

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://dvdnav.mplayerhq.hu
Source:     http://dvdnav.mplayerhq.hu/releases/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  libdvdread
BuildRequires:  autoconf automake m4 libtool

%description
libdvdread is a library which provides a simple foundation for reading DVDs.

%prep
%setup -q -n %{name}-%{version}

%build
./autogen.sh --prefix=/usr \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/dvdnav-config
%{_includedir}/dvdnav/dvd*.h
%{_libdir}/libdvdnav.*
%{_libdir}/libdvdnavmini.*
%{_libdir}/pkgconfig/dvdnav.pc
%{_libdir}/pkgconfig/dvdnavmini.pc
%{_datadir}/aclocal/dvdnav.m4

%clean
rm -rf %{buildroot}

%changelog
* Fri Mar 21 2014 tanggeliang <tanggeliang@gmail.com>
- update from 4.2.0 to 4.2.1
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
