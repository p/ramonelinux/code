Name:       dvd-rw-tools
Version:    7.1
Release:    3%{?dist}
Summary:    dvd-rw-tools

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://fy.chalmers.se
Source:     http://fy.chalmers.se/~appro/linux/DVD+RW/tools/dvd+rw-tools-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  libisoburn m4

%description
The dvd+rw-tools package contains several utilities to master the DVD media, both +RW/+R and -R[W].
The principle tool is growisofs which provides a way to both lay down and grow an ISO9660 file system on (as well as to burn an arbitrary pre-mastered image to) all supported DVD media.
This is useful for creating a new DVD or adding to an existing image on a partially burned DVD.

%prep
%setup -q -n dvd+rw-tools-%{version}

%build
sed -i '/stdlib/a #include <limits.h>' transport.hxx &&
sed -i 's#mkisofs"#xorrisofs"#' growisofs.c &&
sed -i 's#mkisofs#xorrisofs#;s#MKISOFS#XORRISOFS#' growisofs.1 &&
make all rpl8 %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make prefix=%{buildroot}/usr install &&
install -v -m644 -D index.html \
    %{buildroot}/usr/share/doc/dvd+rw-tools-7.1/index.html

%files
%defattr(-,root,root,-)
%{_bindir}/dvd+rw-booktype
%{_bindir}/dvd+rw-format
%{_bindir}/dvd+rw-mediainfo
%{_bindir}/dvd-ram-control
%{_bindir}/growisofs
%{_bindir}/rpl8
%{_docdir}/dvd+rw-tools-7.1/index.html
%{_mandir}/man1/growisofs.1.gz

%clean
rm -rf %{buildroot}

%changelog
* Mon Jul 22 2013 tanggeliang <tanggeliang@gmail.com>
- remove btcflash in make all, conflicts with cdrtools
* Tue Mar 19 2013 tanggeliang <tanggeliang@gmail.com>
- create
