Name:       libdiscid
Version:    0.6.1
Release:    1%{?dist}
Summary:    libdiscid

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.musicbrainz.org
Source:     http://ftp.musicbrainz.org/pub/musicbrainz/libdiscid/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build

%description
The libdiscid package is a library for creating MusicBrainz DiscIDs from audio CDs.
It reads a CD's table of contents (TOC) and generates an identifier which can be used to lookup the CD at MusicBrainz (http://musicbrainz.org).
Additionally, it provides a submission URL for adding the DiscID to the database.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --disable-static \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_includedir}/discid/discid.h
%{_libdir}/libdiscid.*
%{_libdir}/pkgconfig/libdiscid.pc

%clean
rm -rf %{buildroot}

%changelog
* Tue Oct 22 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.5.2 to 0.6.1
* Sat Aug 24 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.3.2 to 0.5.2
* Wed Apr 10 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.2.2 to 0.3.2
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
