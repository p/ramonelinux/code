Name:       libisofs
Version:    1.3.8
Release:    1%{?dist}
Summary:    libisofs

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.libburnia-project.org
Source:     http://files.libburnia-project.org/releases/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  acl attr

%description
libisofs is a library to create an ISO-9660 filesystem with extensions like RockRidge or Joliet.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --disable-static \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_includedir}/libisofs/libisofs.h
%{_libdir}/libisofs.*
%{_libdir}/pkgconfig/libisofs-1.pc

%clean
rm -rf %{buildroot}

%changelog
* Sun Jul 27 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.3.6 to 1.3.8
* Fri Mar 21 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.3.4 to 1.3.6
* Tue Dec 17 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.3.2 to 1.3.4
* Sun Aug 18 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.3.0 to 1.3.2
* Sat Jul 6 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.2.6 to 1.3.0
* Tue Mar 19 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.2.4 to 1.2.6
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
