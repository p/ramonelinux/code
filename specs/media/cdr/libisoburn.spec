Name:       libisoburn
Version:    1.3.6
Release:    1%{?dist}
Summary:    libisoburn

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.libburnia-project.org
Source:     http://files.libburnia-project.org/releases/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  libburn libisofs

%description
libisoburn is a frontend for libraries libburn and libisofs which enables creation and expansion of ISO-9660 filesystems on all CD/DVD/BD media supported by libburn.
This includes media like DVD+RW, which do not support multi-session management on media level and even plain disk files or block devices.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr              \
            --disable-static           \
            --enable-pkg-check-modules \
            --libdir=%{_libdir}        &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

rm -f %{buildroot}/%{_infodir}/dir

%files
%defattr(-,root,root,-)
%{_bindir}/osirrox
%{_bindir}/xorrecord
%{_bindir}/xorriso
%{_bindir}/xorriso-tcltk
%{_bindir}/xorrisofs
%{_includedir}/libisoburn/libisoburn.h
%{_includedir}/libisoburn/xorriso.h
%{_libdir}/libisoburn.la
%{_libdir}/libisoburn.so*
%{_libdir}/pkgconfig/libisoburn-1.pc
%{_infodir}/*.info.gz
%{_mandir}/man1/*.1.gz

%clean
rm -rf %{buildroot}

%changelog
* Fri Mar 21 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.3.4 to 1.3.6
* Tue Dec 17 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.3.2 to 1.3.4
* Sat Aug 24 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.3.0 to 1.3.2
* Wed Jul 31 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.2.6 to 1.3.0
* Tue Mar 19 2013 tanggeliang <tanggeliang@gmail.com>
- create
