Name:       libdvdcss
Version:    1.2.13
Release:    1%{?dist}
Summary:    libdvdcss

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.libdvdcss.org
Source:     http://www.videolan.org/pub/libdvdcss/1.2.13/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build

%description
libdvdcss is a simple library designed for accessing DVDs as a block device without having to bother about the decryption.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr    \
            --disable-static \
            --docdir=/usr/share/doc/libdvdcss-%{version} \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_includedir}/dvdcss/dvdcss.h
%{_libdir}/libdvdcss.la
%{_libdir}/libdvdcss.so*
%{_libdir}/pkgconfig/libdvdcss.pc
%{_docdir}/libdvdcss-%{version}/*

%clean
rm -rf %{buildroot}

%changelog
* Wed Aug 7 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.2.12 to 1.2.13
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
