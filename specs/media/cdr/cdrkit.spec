Name:       cdrkit
Version:    1.1.11
Release:    1%{?dist}
Summary:    A collection of CD/DVD utilities

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://cdrkit.org
Source:     http://cdrkit.org/releases/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  cmake libcap

%description
cdrkit is a collection of CD/DVD utilities.

%prep
%setup -q -n %{name}-%{version}

%build
mkdir build &&
cd build &&
cmake CMAKE_VERBOSE=1 \
    -DCMAKE_INSTALL_PREFIX=/usr \
    -DBUILD_SHARED_LIBS:BOOL=ON \
.. &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
cd build &&
make DESTDIR=%{buildroot} install

ln -s genisoimage %{buildroot}/%{_bindir}/mkisofs
ln -s genisoimage %{buildroot}/%{_bindir}/mkhybrid
ln -s icedax %{buildroot}/%{_bindir}/cdda2wav
ln -s wodim %{buildroot}/%{_bindir}/cdrecord
ln -s wodim %{buildroot}/%{_bindir}/dvdrecord

%files
%defattr(-,root,root,-)
%{_bindir}/cdda2mp3
%{_bindir}/cdda2ogg
%{_bindir}/devdump
%{_bindir}/dirsplit
%{_bindir}/genisoimage
%{_bindir}/icedax
%{_bindir}/isodebug
%{_bindir}/isodump
%{_bindir}/isoinfo
%{_bindir}/isovfy
%{_bindir}/pitchplay
%{_bindir}/readmult
%{_bindir}/readom
%{_bindir}/wodim
%{_bindir}/mkisofs
%{_bindir}/mkhybrid
%{_bindir}/cdda2wav
%{_bindir}/cdrecord
%{_bindir}/dvdrecord
%{_sbindir}/netscsid
%{_mandir}/man*/*.gz

%clean
rm -rf %{buildroot}

%changelog
* Tue Mar 19 2013 tanggeliang <tanggeliang@gmail.com>
- create
