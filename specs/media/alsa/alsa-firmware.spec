Name:       alsa-firmware
Version:    1.0.29
Release:    1%{?dist}
Summary:    ALSA Firmware

Group:      Applications/Multimedia
License:    GPLv2+
Url:        http://www.alsa-project.org
Source:     ftp://ftp.alsa-project.org/firmware/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  alsa-tools

%description
The ALSA Firmware package contains firmware for certain sound cards.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
/lib/firmware/*.bin
/lib/firmware/asihpi/*
/lib/firmware/cs46xx/*
/lib/firmware/ea/*
/lib/firmware/emu/*
/lib/firmware/ess/*
/lib/firmware/korg/*
/lib/firmware/mixart/*
/lib/firmware/pcxhr/*
/lib/firmware/sb16/*
/lib/firmware/turtlebeach/*
/lib/firmware/vx/*
/lib/firmware/yamaha/*
%{_datadir}/alsa/firmware/hdsploader/*
%{_datadir}/alsa/firmware/mixartloader/*
%{_datadir}/alsa/firmware/pcxhrloader/*
%{_datadir}/alsa/firmware/usx2yloader/*
%{_datadir}/alsa/firmware/vxloader/*

%clean
rm -rf %{buildroot}

%changelog
* Sat Sep 26 2015 tanggeliang <tanggeliang@gmail.com>
- update from 1.0.28 to 1.0.29
* Sun Jul 20 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.0.27 to 1.0.28
* Thu Dec 5 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.0.25 to 1.0.27
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
