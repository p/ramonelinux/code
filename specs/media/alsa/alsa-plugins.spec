Name:       alsa-plugins
Version:    1.0.29
Release:    1%{?dist}
Summary:    ALSA Plugins

Group:      Applications/Multimedia
License:    GPLv2+
Url:        http://www.alsa-project.org
Source:     ftp://ftp.alsa-project.org/plugins/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  alsa-lib
BuildRequires:  ffmpeg libsamplerate pulseaudio speex

%description
The ALSA Plugins package contains plugins for various audio libraries and sound servers.

%prep
%setup -q -n %{name}-%{version}

%build
./configure \
    --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_libdir}/alsa-lib/libasound_module_conf_pulse.*
%{_libdir}/alsa-lib/libasound_module_ctl_arcam_av.*
%{_libdir}/alsa-lib/libasound_module_ctl_oss.*
%{_libdir}/alsa-lib/libasound_module_ctl_pulse.*
%{_libdir}/alsa-lib/libasound_module_pcm_a52.*
%{_libdir}/alsa-lib/libasound_module_pcm_oss.*
%{_libdir}/alsa-lib/libasound_module_pcm_pulse.*
%{_libdir}/alsa-lib/libasound_module_pcm_speex.*
%{_libdir}/alsa-lib/libasound_module_pcm_upmix.*
%{_libdir}/alsa-lib/libasound_module_pcm_usb_stream.*
%{_libdir}/alsa-lib/libasound_module_pcm_vdownmix.*
%{_libdir}/alsa-lib/libasound_module_rate_lavcrate.*
%{_libdir}/alsa-lib/libasound_module_rate_lavcrate_fast*.so
%{_libdir}/alsa-lib/libasound_module_rate_lavcrate_high*.so
%{_libdir}/alsa-lib/libasound_module_rate_samplerate.*
%{_libdir}/alsa-lib/libasound_module_rate_samplerate_*.so
%{_libdir}/alsa-lib/libasound_module_rate_speexrate.*
%{_libdir}/alsa-lib/libasound_module_rate_speexrate_best.so
%{_libdir}/alsa-lib/libasound_module_rate_speexrate_medium.so
%{_datadir}/alsa/alsa.conf.d/50-pulseaudio.conf
%{_datadir}/alsa/alsa.conf.d/99-pulseaudio-default.conf.example

%clean
rm -rf %{buildroot}

%changelog
* Sat Sep 26 2015 tanggeliang <tanggeliang@gmail.com>
- update from 1.0.28 to 1.0.29
* Sun Jul 20 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.0.27 to 1.0.28
* Mon Apr 15 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.0.26 to 1.0.27
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
