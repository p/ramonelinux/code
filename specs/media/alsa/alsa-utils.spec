Name:       alsa-utils
Version:    1.0.29
Release:    1%{?dist}
Summary:    ALSA Utilities

Group:      Applications/Multimedia
License:    GPLv2+
Url:        http://www.alsa-project.org
Source:     ftp://ftp.alsa-project.org/utils/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  alsa-lib
BuildRequires:  gettext xmlto libxslt docbook-xml docbook-xsl

%description
The ALSA Utilities package contains various utilities which are useful for controlling your sound card.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --disable-alsaconf --disable-xmlto \
            --with-udev-rules-dir=/lib/udev/rules.d \
            --with-systemdsystemunitdir=/lib/systemd/system &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
/lib/udev/rules.d/90-alsa-restore.rules
%{_bindir}/aconnect
%{_bindir}/alsaloop
%{_bindir}/alsamixer
%{_bindir}/alsaucm
%{_bindir}/amidi
%{_bindir}/amixer
%{_bindir}/aplay
%{_bindir}/aplaymidi
%{_bindir}/arecord
%{_bindir}/arecordmidi
%{_bindir}/aseqdump
%{_bindir}/aseqnet
%{_bindir}/iecset
%{_bindir}/speaker-test
%{_sbindir}/alsactl
%{_sbindir}/alsa-info.sh
%{_datadir}/alsa/init/*
%{_datadir}/alsa/speaker-test/sample_map.csv
%{_datadir}/locale/*/LC_MESSAGES/alsa*.mo
%{_datadir}/sounds/alsa/*.wav
/lib/systemd/system/alsa-restore.service
/lib/systemd/system/alsa-state.service
/lib/systemd/system/alsa-store.service
/lib/systemd/system/basic.target.wants/alsa-restore.service
/lib/systemd/system/basic.target.wants/alsa-state.service
/lib/systemd/system/shutdown.target.wants/alsa-store.service
%{_mandir}/*

%post
mkdir -p /var/lib/alsa
touch /var/lib/alsa/asound.state &&
alsactl store
/usr/sbin/usermod -a -G audio root
/usr/sbin/usermod -a -G audio `whoami`

%clean
rm -rf %{buildroot}

%changelog
* Sat Sep 26 2015 tanggeliang <tanggeliang@gmail.com>
- update from 1.0.28 to 1.0.29
* Sun Jul 20 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.0.27.2 to 1.0.28
* Thu Jul 25 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.0.27 to 1.0.27.2
* Tue Apr 16 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.0.26 to 1.0.27
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
