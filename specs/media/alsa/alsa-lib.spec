Name:       alsa-lib
Version:    1.0.29
Release:    1%{?dist}
Summary:    ALSA Library

Group:      Applications/Multimedia
License:    GPLv2+
Url:        http://www.alsa-project.org
Source:     ftp://ftp.alsa-project.org/lib/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build

%description
The ALSA Library package contains the ALSA library.
This is used by programs (including ALSA Utilities) requiring access to the ALSA sound interface.

%prep
%setup -q -n %{name}-%{version}

%build
./configure \
    --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/aserver
%{_includedir}/alsa/*.h
%{_includedir}/alsa/sound/*.h
%{_includedir}/sys/asoundlib.h
%{_libdir}/alsa-lib/smixer/smixer-*.*
%{_libdir}/libasound.*
%{_libdir}/pkgconfig/alsa.pc
%{_datadir}/aclocal/alsa.m4
%{_datadir}/alsa/alsa.conf
%{_datadir}/alsa/alsa.conf.d/README
%{_datadir}/alsa/cards/*
%{_datadir}/alsa/pcm/*.conf
%{_datadir}/alsa/smixer.conf
%{_datadir}/alsa/sndo-mixer.alisp
%{_datadir}/alsa/ucm/DAISY-I2S/*
%{_datadir}/alsa/ucm/PandaBoard/*
%{_datadir}/alsa/ucm/PandaBoardES/*
%{_datadir}/alsa/ucm/SDP4430/*
%{_datadir}/alsa/ucm/tegraalc5632/*
%{_datadir}/alsa/ucm/GoogleNyan/*
%{_datadir}/alsa/ucm/PAZ00/*

%clean
rm -rf %{buildroot}

%changelog
* Sat Sep 26 2015 tanggeliang <tanggeliang@gmail.com>
- update from 1.0.28 to 1.0.29
* Sun Jul 20 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.0.27.2 to 1.0.28
* Thu Jul 25 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.0.27 to 1.0.27.2
* Mon Apr 15 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.0.26 to 1.0.27
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
