%define _subdirs  { as10k1 echomixer envy24control hdajackretask \
                    hda-verb hdspconf hdsploader hdspmixer \
                    hwmixvolume ld10k1 mixartloader pcxhrloader \
                    rmedigicontrol sb16_csp sscape_ctl us428control \
                    usx2yloader vxloader }
                    #qlo10k1 seq
Name:       alsa-tools
Version:    1.0.29
Release:    1%{?dist}
Summary:    ALSA Tools

Group:      Applications/Multimedia
License:    GPLv2+
Url:        http://www.alsa-project.org
Source:     ftp://ftp.alsa-project.org/tools/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  alsa-lib
BuildRequires:  gtk2 gtk+ fltk
BuildRequires:  libtool autoconf automake m4

%description
The ALSA Tools package contains advanced tools for certain sound cards.

%prep
%setup -q -n %{name}-%{version}

%build
for tool in %{_subdirs}; do
    pushd $tool
    ./configure --prefix=/usr \
                --libdir=%{_libdir} &&
    make %{?_smp_mflags}
    popd
done

%check

%install
rm -rf %{buildroot}
for tool in %{_subdirs}; do
    make -C $tool install DESTDIR=%{buildroot}
done

%files
%defattr(-,root,root,-)
/etc/hotplug/usb/tascam_fpga
/etc/hotplug/usb/tascam_fw
/etc/hotplug/usb/tascam_fw.usermap
%{_bindir}/as10k1
%{_bindir}/cspctl
%{_bindir}/echomixer
%{_bindir}/envy24control
%{_bindir}/hda-verb
%{_bindir}/hdajackretask
%{_bindir}/hdspconf
%{_bindir}/hdsploader
%{_bindir}/hdspmixer
%{_bindir}/hwmixvolume
%{_bindir}/init_audigy
%{_bindir}/init_audigy_eq10
%{_bindir}/init_live
%{_bindir}/lo10k1
%{_bindir}/mixartloader
%{_bindir}/pcxhrloader
%{_bindir}/rmedigicontrol
%{_bindir}/sscape_ctl
%{_bindir}/us428control
%{_bindir}/usx2yloader
%{_bindir}/vxloader
%{_includedir}/lo10k1/*.h
%{_libdir}/liblo10k1.*
%{_sbindir}/dl10k1
%{_sbindir}/ld10k1
%{_sbindir}/ld10k1d
%{_datadir}/aclocal/ld10k1.m4
%{_datadir}/applications/hdspconf.desktop
%{_datadir}/applications/hdspmixer.desktop
%{_datadir}/ld10k1/effects/*.emu10k1
%{_datadir}/pixmaps/hdspconf.png
%{_datadir}/pixmaps/hdspmixer.png
%{_mandir}/man1/cspctl.1.gz
%{_mandir}/man1/envy24control.1.gz

%clean
rm -rf %{buildroot}

%changelog
* Sat Sep 26 2015 tanggeliang <tanggeliang@gmail.com>
- update from 1.0.28 to 1.0.29
* Sun Jul 20 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.0.27 to 1.0.28
* Sat Jun 22 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.0.25 to 1.0.27
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
