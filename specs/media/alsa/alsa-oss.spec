Name:       alsa-oss
Version:    1.0.28
Release:    1%{?dist}
Summary:    ALSA OSS

Group:      Applications/Multimedia
License:    GPLv2+
Url:        http://www.alsa-project.org
Source:     ftp://ftp.alsa-project.org/oss-lib/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  alsa-lib

%description
The ALSA OSS package contains the ALSA OSS compatibility library.
This is used by programs which wish to use the ALSA OSS sound interface.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --disable-static \
    --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/aoss
%{_includedir}/oss-redir.h
%{_libdir}/libalsatoss.*
%{_libdir}/libaoss.*
%{_libdir}/libossredir.*
%{_mandir}/man1/aoss.1.gz

%clean
rm -rf %{buildroot}

%changelog
* Sun Jul 20 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.0.25 to 1.0.28
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
