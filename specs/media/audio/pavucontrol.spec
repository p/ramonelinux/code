Name:       pavucontrol
Version:    2.0
Release:    1%{?dist}
Summary:    PulseAudio Volume Control

Group:      Applications/Multimedia
License:    GPLv2+
Url:        http://www.freedesktop.org
Source:     http://freedesktop.org/software/pulseaudio/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  pulseaudio
BuildRequires:  gtkmm libcanberra
BuildRequires:  intltool gettext

%description
PulseAudio Volume Control (pavucontrol) is a simple GTK based volume control tool ("mixer") for the PulseAudio sound server.
In contrast to classic mixer tools this one allows you to control both the volume of hardware devices and of each playback stream separately.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/pavucontrol
%{_datadir}/applications/pavucontrol.desktop
%{_datadir}/locale/*/LC_MESSAGES/pavucontrol.mo
%{_datadir}/pavucontrol/pavucontrol.glade
%{_docdir}/pavucontrol/*

%clean
rm -rf %{buildroot}

%changelog
* Mon Jul 1 2013 tanggeliang <tanggeliang@gmail.com>
- create
