Name:       libsamplerate
Version:    0.1.8
Release:    5%{?dist}
Summary:    libsamplerate

Group:      Applications/Multimedia
License:    GPLv2+
Url:        http://www.mega-nerd.com
Source:     http://www.mega-nerd.com/SRC/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  libsndfile

%description
libsamplerate is a sample rate converter for audio.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

%files
%defattr(-,root,root,-)
%{_bindir}/sndfile-resample
%{_includedir}/samplerate.h
%{_libdir}/libsamplerate.*a
%{_libdir}/libsamplerate.so*
%{_libdir}/pkgconfig/samplerate.pc
%{_docdir}/libsamplerate0-dev/html/*

%clean
rm -rf %{buildroot}

%changelog
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
