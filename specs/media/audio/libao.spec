Name:       libao
Version:    1.2.0
Release:    1%{?dist}
Summary:    Library Audio Output

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://downloads.xiph.org
Source:     http://downloads.xiph.org/releases/ao/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  libx11 esound alsa-lib pulseaudio libatomic_ops

%description
The libao package contains a cross-platform audio library.
This is useful to output audio on a wide variety of platforms.
It currently supports WAV files, OSS (Open Sound System), ESD (Enlighten Sound Daemon), ALSA (Advanced Linux Sound Architecture), NAS (Network Audio system), aRTS (analog Real-Time Synthesizer and PulseAudio (next generation GNOME sound architecture).

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install &&
install -v -m644 README %{buildroot}/usr/share/doc/libao-%{version}

%files
%defattr(-,root,root,-)
%{_includedir}/ao/*.h
%{_libdir}/ao/plugins-4/libalsa.la
%{_libdir}/ao/plugins-4/libalsa.so
%{_libdir}/ao/plugins-4/libesd.la
%{_libdir}/ao/plugins-4/libesd.so
%{_libdir}/ao/plugins-4/liboss.la
%{_libdir}/ao/plugins-4/liboss.so
%{_libdir}/ao/plugins-4/libpulse.la
%{_libdir}/ao/plugins-4/libpulse.so
%{_libdir}/ckport/db/libao.ckport
%{_libdir}/libao.la
%{_libdir}/libao.so*
%{_libdir}/pkgconfig/ao.pc
%{_datadir}/aclocal/ao.m4
%{_docdir}/libao-%{version}/*
%{_mandir}/man5/libao.conf.5.gz

%clean
rm -rf %{buildroot}

%changelog
* Sat Mar 22 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.1.0 to 1.2.0
* Tue Mar 19 2013 tanggeliang <tanggeliang@gmail.com>
- create
