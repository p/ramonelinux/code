Name:       libmusicbrainz
Version:    5.0.1
Release:    2%{?dist}
Summary:    libmusicbrainz

Group:      Applications/Multimedia
License:    GPLv2+
URL:        http://www.musicbrainz.org
Source:     https://github.com/downloads/metabrainz/libmusicbrainz/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  cmake libdiscid neon

%description
The libmusicbrainz package contains a library which allows you to access the data held on the MusicBrainz server.
This is useful for adding MusicBrainz lookup capabilities to other applications.

%prep
%setup -q -n %{name}-%{version}

%build
cmake -DCMAKE_INSTALL_PREFIX=/usr \
%ifarch x86_64
      -DLIB_SUFFIX=64 \
%endif
      . &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_includedir}/musicbrainz5/*.h
%{_libdir}/libmusicbrainz5.so*
%{_libdir}/pkgconfig/libmusicbrainz5.pc

%clean
rm -rf %{buildroot}

%changelog
* Mon Oct 1 2012 tanggeliang <tanggeliang@gmail.com>
- create
