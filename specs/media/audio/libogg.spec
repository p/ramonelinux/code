Name:       libogg
Version:    1.3.2
Release:    1%{?dist}
Summary:    libogg

Group:      Applications/Multimedia
License:    GPLv2+
Url:        http://downloads.xiph.org/releases/ogg
Source:     http://downloads.xiph.org/releases/ogg/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build

%description
The libogg package contains the Ogg file structure.
This is useful for creating (encoding) or playing (decoding) a single physical bit stream.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --docdir=%{_docdir}/libogg-%{version} \
            --disable-static \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_includedir}/ogg/*.h
%{_libdir}/libogg.la
%{_libdir}/libogg.so*
%{_libdir}/pkgconfig/ogg.pc
%{_datadir}/aclocal/ogg.m4

%files doc
%defattr(-,root,root,-)
%{_docdir}/libogg-%{version}/*

%clean
rm -rf %{buildroot}

%changelog
* Sun Jul 20 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.3.1 to 1.3.2
* Sat Jul 6 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.3.0 to 1.3.1
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
