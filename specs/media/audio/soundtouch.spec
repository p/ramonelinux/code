Name:       soundtouch
Version:    1.8.0
Release:    1%{?dist}
Summary:    SoundTouch

Group:      Applications/Multimedia
License:    GPLv2+
Url:        http://www.surina.net/soundtouch
Source:     http://www.surina.net/soundtouch/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  autoconf automake m4
BuildRequires:  gettext libtool

%description
The SoundTouch package contains an open-source audio processing library that allows changing the sound tempo, pitch and playback rate parameters independently from each other.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}

%build
sed -i 's#AM_CONFIG_HEADER#AC_CONFIG_HEADERS#' configure.ac
./bootstrap
./configure --prefix=/usr \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make pkgdocdir=/usr/share/doc/soundtouch-%{version} install DESTDIR=%{buildroot}

%files
%defattr(-,root,root,-)
%{_bindir}/soundstretch
%{_includedir}/soundtouch/*.h
%{_libdir}/libSoundTouch.la
%{_libdir}/libSoundTouch.so*
%{_libdir}/pkgconfig/soundtouch.pc
%{_datadir}/aclocal/soundtouch.m4

%files doc
%defattr(-,root,root,-)
%{_docdir}/soundtouch-%{version}/*

%clean
rm -rf %{buildroot}

%changelog
* Tue Mar 18 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.7.1 to 1.8.0
* Sun Mar 31 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.7.0 to 1.7.1
* Sat Sep 29 2012 tanggeliang <tanggeliang@gmail.com>
- create
