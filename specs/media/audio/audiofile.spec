Name:       audiofile
Version:    0.3.6
Release:    1%{?dist}
Summary:    Audio File

Group:      Applications/Multimedia
License:    GPLv2+
Url:        http://www.68k.org/~michael/audiofile
Source:     http://www.68k.org/~michael/audiofile/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  alsa-lib

%description
The Audio File package contains the audio file libraries and two sound file support programs useful to support basic sound file formats.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make install docdir=/usr/share/doc/esound-0.2.41 DESTDIR=%{buildroot}

%files
%defattr(-,root,root,-)
%{_bindir}/sfconvert
%{_bindir}/sfinfo
%{_includedir}/*.h
%{_libdir}/libaudiofile.*
%{_libdir}/pkgconfig/audiofile.pc
%{_mandir}/man1/*.1.gz
%{_mandir}/man3/*.3.gz

%clean
rm -rf %{buildroot}

%changelog
* Thu May 30 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.3.4 to 0.3.6
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
