Name:       esound
Version:    0.2.41
Release:    5%{?dist}
Summary:    EsounD

Group:      Applications/Multimedia
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/esound/0.2/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  audiofile alsa-lib

%description
The EsounD package contains the Enlightened Sound Daemon.
This is useful for mixing together several digitized audio streams for playback by a single device.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --sysconfdir=/etc \
            --libdir=%{_libdir} &&
make

%check

%install
rm -rf %{buildroot}
make install docdir=/usr/share/doc/esound-0.2.41 DESTDIR=%{buildroot}

%files
%defattr(-,root,root,-)
/etc/esd.conf
%{_bindir}/esd*
%{_includedir}/esd.h
%{_libdir}/libesd*.*
%{_libdir}/pkgconfig/esound.pc
%{_datadir}/aclocal/*
%{_mandir}/*/*

%clean
rm -rf %{buildroot}

%changelog
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
