Name:       libid3tag
Version:    0.15.1
Release:    1%{?dist}
Summary:    ID3 tag manipulation library

Group:      Applications/Multimedia
License:    GPLv2+
Url:        http://mad.sourceforge.net
Source:     http://downloads.sourceforge.net/%{name}/%{name}-%{version}b.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build

%description
libid3tag is a library for reading and (eventually) writing ID3 tags, both ID3v1 and the various versions of ID3v2.

%prep
%setup -q -n %{name}-%{version}b

%build
./configure --prefix=/usr \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

%files
%defattr(-,root,root,-)
%{_includedir}/id3tag.h
%{_libdir}/libid3tag.*a
%{_libdir}/libid3tag.so*

%clean
rm -rf %{buildroot}

%changelog
* Tue Jun 18 2013 tanggeliang <tanggeliang@gmail.com>
- create
