Name:       faad
Version:    2.7
Release:    6%{?dist}
Summary:    FAAD2

Group:      Applications/Multimedia
License:    GPLv2+
Url:        http://faac.sourceforge.net
Source:     http://downloads.sourceforge.net/faac/%{name}2-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build

%description
FAAD2 is a decoder for a lossy sound compression scheme specified in MPEG-2 Part 7 and MPEG-4 Part 3 standards and known as Advanced Audio Coding (AAC).

%prep
%setup -q -n %{name}2-%{version}

%build
./configure --prefix=/usr \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

%files
%defattr(-,root,root,-)
%{_bindir}/faad
%{_includedir}/*.h
%{_libdir}/libfaad.*a
%{_libdir}/libfaad.so*
%{_libdir}/libmp4ff.a
%{_mandir}/manm/faad.man.gz

%clean
rm -rf %{buildroot}

%changelog
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
