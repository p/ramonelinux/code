Name:       libvorbis
Version:    1.3.4
Release:    1%{?dist}
Summary:    libvorbis

Group:      Applications/Multimedia
License:    GPLv2+
Url:        http://downloads.xiph.org/releases/vorbis
Source:     http://downloads.xiph.org/releases/vorbis/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  libogg

%description
The libvorbis package contains a general purpose audio and music encoding format.
This is useful for creating (encoding) and playing (decoding) sound in an open (patent free) format.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --disable-static \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

install -v -m644 doc/Vorbis* %{buildroot}/usr/share/doc/libvorbis-%{version}

%files
%defattr(-,root,root,-)
%{_includedir}/vorbis/*.h
%{_libdir}/libvorbis*.*
%{_libdir}/pkgconfig/vorbis*.pc
%{_datadir}/aclocal/vorbis.m4

%files doc
%defattr(-,root,root,-)
%{_docdir}/libvorbis-%{version}/*

%clean
rm -rf %{buildroot}

%changelog
* Tue Mar 18 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.3.3 to 1.3.4
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
