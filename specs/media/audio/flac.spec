Name:       flac
Version:    1.3.0
Release:    1%{?dist}
Summary:    FLAC

Group:      Applications/Multimedia
License:    GPLv2+
Url:        http://www.sourceforge.net/flac
Source:     http://downloads.sourceforge.net/flac/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  libogg

%description
FLAC is an audio CODEC similar to MP3, but lossless, meaning that audio is compressed without losing any information.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --disable-thorough-tests \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

rm -f %{buildroot}/%{_infodir}/dir

%files
%defattr(-,root,root,-)
%{_bindir}/flac
%{_bindir}/metaflac
%{_includedir}/FLAC++/*.h
%{_includedir}/FLAC/*.h
%{_libdir}/libFLAC*.*
%{_libdir}/pkgconfig/flac++.pc
%{_libdir}/pkgconfig/flac.pc
%{_datadir}/aclocal/libFLAC*.m4

%files doc
%defattr(-,root,root,-)
%{_docdir}/flac-%{version}/*
%{_mandir}/man1/*flac.1.gz

%clean
rm -rf %{buildroot}

%changelog
* Sat Jul 6 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.2.1 to 1.3.0
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
