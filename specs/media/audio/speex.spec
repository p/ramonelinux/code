Name:       speex
Version:    1.2rc1
Release:    6%{?dist}
Summary:    Speex

Group:      Applications/Multimedia
License:    GPLv2+
Url:        http://downloads.us.xiph.org/releases/speex/
Source:     http://downloads.us.xiph.org/releases/speex/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  libogg

%description
Speex is an audio compression format designed especially for speech.
It is well-adapted to internet applications and provides useful features that are not present in most other CODECs.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --disable-static \
            --docdir=%{_docdir}/speex-%{version} \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/speexdec
%{_bindir}/speexenc
%{_includedir}/speex/speex*.h
%{_libdir}/libspeex*.*
%{_libdir}/pkgconfig/speex.pc
%{_libdir}/pkgconfig/speexdsp.pc
%{_datadir}/aclocal/speex.m4

%files doc
%defattr(-,root,root,-)
%{_docdir}/speex-%{version}/manual.pdf
%{_mandir}/*

%clean
rm -rf %{buildroot}

%changelog
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
