Name:       libsndfile
Version:    1.0.25
Release:    7%{?dist}
Summary:    libsndfile

Group:      Applications/Multimedia
License:    GPLv2+
Url:        http://www.mega-nerd.com/libsndfile
Source:     http://www.mega-nerd.com/libsndfile/files/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  alsa-lib flac libogg libvorbis sqlite

%description
Libsndfile is a library of C routines for reading and writing files containing sampled audio data.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} htmldocdir=/usr/share/doc/libsndfile-1.0.25 install

%files
%defattr(-,root,root,-)
%{_bindir}/sndfile-*
%{_includedir}/sndfile.h
%{_includedir}/sndfile.hh
%{_libdir}/libsndfile.*
%{_libdir}/pkgconfig/sndfile.pc

%files doc
%defattr(-,root,root,-)
%{_docdir}/libsndfile-1.0.25/*
%{_mandir}/*

%clean
rm -rf %{buildroot}

%changelog
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
