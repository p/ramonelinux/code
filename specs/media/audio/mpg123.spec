Name:       mpg123
Version:    1.22.4
Release:    1%{?dist}
Summary:    Mpg123

Group:      Applications/Multimedia
License:    GPLv2+
Url:        http://mpg123.sourceforge.net
Source:     http://downloads.sourceforge.net/mpg123/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  alsa-lib esound pulseaudio sdl

%description
The Mpg123 package contains a console-based MP3 player.
It claims to be the fastest MP3 decoder for Unix.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --with-module-suffix=.so \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

%files
%defattr(-,root,root,-)
%{_bindir}/mpg123
%{_bindir}/mpg123-id3dump
%{_bindir}/mpg123-strip
%{_bindir}/out123
%{_includedir}/mpg123.h
%{_libdir}/libmpg123.*
%{_libdir}/mpg123/output_alsa.*
%{_libdir}/mpg123/output_dummy.*
%{_libdir}/mpg123/output_esd.*
%{_libdir}/mpg123/output_oss.*
%{_libdir}/mpg123/output_pulse.*
%{_libdir}/mpg123/output_sdl.*
%{_libdir}/pkgconfig/libmpg123.pc
%{_mandir}/man1/mpg123.1.gz
%{_mandir}/man1/out123.1.gz

%clean
rm -rf %{buildroot}

%changelog
* Wed Sep 16 2015 tanggeliang <tanggeliang@gmail.com>
- update from 1.20.1 to 1.22.4
* Sat Jul 26 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.19.0 to 1.20.1
* Fri Mar 21 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.16.0 to 1.19.0
* Tue Oct 22 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.15.4 to 1.16.0
* Thu Jul 25 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.15.1 to 1.15.4
* Sun Mar 17 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.14.4 to 1.15.1
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
