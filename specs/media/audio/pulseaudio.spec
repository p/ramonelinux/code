Name:       pulseaudio
Version:    6.99.2
Release:    1%{?dist}
Summary:    PulseAudio

Group:      Applications/Multimedia
License:    GPLv2+
Url:        http://www.freedesktop.org
Source:     http://freedesktop.org/software/pulseaudio/releases/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  intltool json-c libsndfile pkg-config
BuildRequires:  alsa-lib dbus openssl speex
BuildRequires:  libx11 libxcb libice libsm libxtst
BuildRequires:  gconf gtk2 gtk+
BuildRequires:  flac
BuildRequires:  m4 libtool gettext xml-parser systemd
BuildRequires:  libcap
BuildRequires:  avahi
# for groupadd
Requires:       shadow

%description
PulseAudio is a sound system for POSIX OSes, meaning that it is a proxy for sound applications.
It allows you to do advanced operations on your sound data as it passes between your application and your hardware.
Things like transferring the audio to a different machine, changing the sample format or channel count and mixing several sounds into one are easily achieved using a sound server.

%prep
%setup -q -n %{name}-%{version}

%build
find . -name "Makefile.in" | xargs sed -i "s|(libdir)/@PACKAGE@|(libdir)/pulse|" &&
./configure --prefix=/usr         \
            --sysconfdir=/etc     \
            --localstatedir=/var  \
            --disable-bluez4      \
            --disable-rpath       \
	    --with-bash-completion-dir=/etc/bash_completion.d \
            --with-module-dir=%{_libdir}/pulse/modules \
	    --with-systemduserunitdir=/lib/systemd/system \
            --libexecdir=%{_libdir} \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
/etc/bash_completion.d/pacat
/etc/bash_completion.d/pacmd
/etc/bash_completion.d/pactl
/etc/bash_completion.d/padsp
/etc/bash_completion.d/paplay
/etc/bash_completion.d/parec
/etc/bash_completion.d/parecord
/etc/bash_completion.d/pasuspender
/etc/bash_completion.d/pulseaudio
/etc/dbus-1/system.d/pulseaudio-system.conf
/etc/pulse/*.conf
/etc/pulse/*.pa
/etc/xdg/autostart/pulseaudio*.desktop
/lib/udev/rules.d/90-pulseaudio.rules
%{_bindir}/esdcompat
%{_bindir}/pacat
%{_bindir}/pacmd
%{_bindir}/pactl
%{_bindir}/padsp
%{_bindir}/pamon
%{_bindir}/paplay
%{_bindir}/parec
%{_bindir}/parecord
%{_bindir}/pasuspender
%{_bindir}/pax11publish
%{_bindir}/pulseaudio
%{_bindir}/start-pulseaudio-x11
%{_includedir}/pulse/*.h
%{_libdir}/cmake/PulseAudio/PulseAudioConfig*.cmake
%{_libdir}/pulse/gconf-helper
%{_libdir}/pulse/libpulsecommon-*.la
%{_libdir}/pulse/libpulsecommon-*.so
%{_libdir}/pulse/libpulsedsp.la
%{_libdir}/pulse/libpulsedsp.so
%{_libdir}/pulse/modules/libavahi-wrap.so
%{_libdir}/pulse/modules/libalsa-util.so
%{_libdir}/pulse/modules/libcli.so
%{_libdir}/pulse/modules/liboss-util.so
%{_libdir}/pulse/modules/libprotocol-*.so
%{_libdir}/pulse/modules/libraop.so
%{_libdir}/pulse/modules/librtp.so
%{_libdir}/pulse/modules/module-*.so
%{_datadir}/locale/*/LC_MESSAGES/pulseaudio.mo
%{_datadir}/pulseaudio/alsa-mixer/paths/*.conf*
%{_datadir}/pulseaudio/alsa-mixer/profile-sets/*.conf
%{_datadir}/vala/vapi/libpulse-mainloop-glib.*
%{_datadir}/vala/vapi/libpulse*
%{_datadir}/zsh/site-functions/_pulseaudio
/lib/systemd/system/pulseaudio.service
/lib/systemd/system/pulseaudio.socket
%{_mandir}/man1/*.1.gz
%{_mandir}/man5/*.5.gz

#libpulse
%{_libdir}/libpulse.la
%{_libdir}/libpulse.so*
%{_libdir}/libpulsecore-*.la
%{_libdir}/libpulsecore-*.so
%{_libdir}/pkgconfig/libpulse.pc

#libpulse-mainloop-glib
%{_libdir}/libpulse-mainloop-glib.la
%{_libdir}/libpulse-mainloop-glib.so*
%{_libdir}/pkgconfig/libpulse-mainloop-glib.pc

#libpulse-simple
%{_libdir}/libpulse-simple.la
%{_libdir}/libpulse-simple.so*
%{_libdir}/pkgconfig/libpulse-simple.pc

%post
groupadd -g 58 pulse &&
groupadd -g 59 pulse-access &&
useradd -c "Pulseaudio User" -d /var/run/pulse -g pulse \
        -s /bin/false -u 58 pulse &&
usermod -a -G audio pulse

%clean
rm -rf %{buildroot}

%changelog
* Wed Sep 16 2015 tanggeliang <tanggeliang@gmail.com>
- update from 6.0 to 6.99.2
* Fri Mar 21 2014 tanggeliang <tanggeliang@gmail.com>
- update from 4.0 to 5.0
* Thu Jun 6 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.0 to 4.0
* Mon Apr 15 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.1 to 3.0
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
