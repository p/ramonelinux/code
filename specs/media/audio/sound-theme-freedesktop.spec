Name:       sound-theme-freedesktop
Version:    0.8
Release:    1%{?dist}
Summary:    sound-theme-freedesktop

Group:      Applications/Multimedia
License:    GPLv2+
Url:        http://www.freedesktop.org
Source:     http://people.freedesktop.org/~mccann/dist/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildArch:      noarch
BuildRequires:  intltool gettext xml-parser

%description
The sound-theme-freedesktop package contains sound themes for the desktop.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_datadir}/sounds/freedesktop/index.theme
%{_datadir}/sounds/freedesktop/stereo/*.oga

%clean
rm -rf %{buildroot}

%changelog
* Mon Oct 22 2012 tanggeliang <tanggeliang@gmail.com>
- update from 0.7 to 0.8
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
