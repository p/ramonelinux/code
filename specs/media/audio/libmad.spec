Name:       libmad
Version:    0.15.1
Release:    6%{?dist}
Summary:    MPEG audio decoder library

Group:      Applications/Multimedia
License:    GPLv2+
Url:        http://mad.sourceforge.net
Source:     http://downloads.sourceforge.net/mad/%{name}-%{version}b.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build

%description
libmad is a high-quality MPEG audio decoder capable of 24-bit output.

%prep
%setup -q -n %{name}-%{version}b

%build
sed -i '/-fforce-mem/d' configure &&
./configure --prefix=/usr \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

mkdir -pv %{buildroot}/%{_libdir}/pkgconfig
cat > %{buildroot}/%{_libdir}/pkgconfig/mad.pc << "EOF"
prefix=/usr
exec_prefix=${prefix}
libdir=${exec_prefix}/%{_lib}
includedir=${prefix}/include

Name: mad
Description: MPEG audio decoder
Requires:
Version: 0.15.1b
Libs: -L${libdir} -lmad
Cflags: -I${includedir}
EOF

%files
%defattr(-,root,root,-)
%{_includedir}/mad.h
%{_libdir}/libmad.*a
%{_libdir}/libmad.so*
%{_libdir}/pkgconfig/mad.pc

%clean
rm -rf %{buildroot}

%changelog
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
