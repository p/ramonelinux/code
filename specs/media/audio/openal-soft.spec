Name:       openal-soft
Version:    1.15.1
Release:    1%{?dist}
Summary:    OpenAL Soft

Group:      Applications/Multimedia
License:    GPLv2+
Url:        http://kcat.strangesoft.net/openal.html
Source:     http://kcat.strangesoft.net/openal-releases/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  cmake
BuildRequires:  pulseaudio ffmpeg

%description
OpenAL Soft is an LGPL-licensed, cross-platform, software implementation of the OpenAL 3D audio API. It's forked from the open-sourced Windows version available originally from the SVN repository at openal.org.
OpenAL provides capabilities for playing audio in a virtual 3D environment. Distance attenuation, doppler shift, and directional sound emitters are among the features handled by the API. More advanced effects, including air absorption, occlusion, and environmental reverb, are available through the EFX extension. It also facilitates streaming audio, multi-channel buffers, and audio capture.

%prep
%setup -q -n %{name}-%{version}

%build
cd build &&
cmake -DCMAKE_INSTALL_PREFIX=/usr \
      -DCMAKE_PREFIX_PATH=/usr \
      -DCMAKE_BUILD_TYPE=Release \
%ifarch x86_64
      -DLIB_SUFFIX=64 \
%endif
      .. &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
cd build
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/allatency
%{_bindir}/alreverb
%{_bindir}/alstream
%{_bindir}/makehrtf
%{_bindir}/openal-info
%{_includedir}/AL/al*.h
%{_includedir}/AL/efx*.h
%{_libdir}/libopenal.so*
%{_libdir}/pkgconfig/openal.pc
%{_datadir}/openal/alsoftrc.sample

%clean
rm -rf %{buildroot}

%changelog
* Thu Nov 21 2013 tanggeliang <tanggeliang@gmail.com>
- create
