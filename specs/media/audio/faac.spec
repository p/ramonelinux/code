Name:       faac
Version:    1.28
Release:    6%{?dist}
Summary:    FAAC

Group:      Applications/Multimedia
License:    GPLv2+
Url:        http://faac.sourceforge.net
Source:     http://downloads.sourceforge.net/faac/%{name}-%{version}.tar.gz
Patch:      faac-1.28-glibc_fixes-1.patch

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build

%description
FAAC is an encoder for a lossy sound compression scheme specified in MPEG-2 Part 7 and MPEG-4 Part 3 standards and known as Advanced Audio Coding (AAC).
This encoder is useful for producing files that can be played back on iPod. Moreover, iPod does not understand other sound compression schemes in video files.

%prep
%setup -q -n %{name}-%{version}
%patch -p1

%build
sed -i -e '/obj-type/d' -e '/Long Term/d' frontend/main.c &&
./configure --prefix=/usr \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/faac
%{_includedir}/faac*.h
%{_libdir}/libfaac.*a
%{_libdir}/libfaac.so*
%{_mandir}/man1/faac.1.gz

%clean
rm -rf %{buildroot}

%changelog
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
