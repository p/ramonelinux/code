Name:       cdparanoia
Version:    10.2
Release:    5%{?dist}
Summary:    CDParanoia III

Group:      Applications/Multimedia
License:    GPLv2+
Url:        http://www.xiph.org
Source:     http://downloads.xiph.org/releases/cdparanoia/%{name}-III-%{version}.src.tgz
Patch0:      cdparanoia-III-10.2-gcc_fixes-1.patch
Patch1:      cdparanoia-10.2-install.patch

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build

%description
The CDParanoia package contains a CD audio extraction tool.
This is useful for extracting .wav files from audio CDs.
A CDDA capable CDROM drive is needed.
Practically all drives supported by Linux can be used.

%prep
%setup -q -n %{name}-III-%{version}
%patch0 -p1
%patch1 -p1

%build
./configure --prefix=/usr --includedir=%{_includedir}/cdda &&
make -j1

%check

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}
chmod -v 755 %{buildroot}/usr/lib/libcdda_*.so.0.10.2

%files
%defattr(-,root,root,-)
%{_bindir}/cdparanoia
%{_includedir}/cdda/cdda_*.h
%{_includedir}/cdda/utils.h
%{_libdir}/libcdda_interface.*
%{_libdir}/libcdda_paranoia.*
/usr/man/man1/cdparanoia.1.gz

%clean
rm -rf %{buildroot}

%changelog
* Thu Aug 30 2012 tanggeliang <tanggeliang@gmail.com>
- add "--includedir=%{_includedir}/cdda" to install headers.
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
