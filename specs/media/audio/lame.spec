Name:       lame
Version:    3.99.5
Release:    6%{?dist}
Summary:    LAME

Group:      Applications/Multimedia
License:    GPLv2+
Url:        http://lame.sourceforge.net
Source:     http://downloads.sourceforge.net/lame/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build

%description
The LAME package contains an MP3 encoder and optionally, an MP3 frame analyzer.
This is useful for creating and analyzing compressed audio files.

%prep
%setup -q -n %{name}-%{version}
sed -i -e '/xmmintrin\.h/d' configure

%build
./configure --prefix=/usr --enable-mp3rtp --disable-static \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make pkghtmldir=/usr/share/doc/lame-3.99.5 install DESTDIR=%{buildroot}

%files
%defattr(-,root,root,-)
%{_bindir}/lame
%{_bindir}/mp3rtp
%{_includedir}/lame/lame.h
%{_libdir}/libmp3lame.*
%{_docdir}/lame-3.99.5/*
%{_mandir}/man1/lame.1.gz

%clean
rm -rf %{buildroot}

%changelog
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
