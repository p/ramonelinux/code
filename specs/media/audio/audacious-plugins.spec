Name:       audacious-plugins
Version:    3.4.3
Release:    1%{?dist}
Summary:    Audacious plugins

Group:      Applications/Multimedia
License:    GPLv2+
Url:        http://audacious-media-player.org
Source:     http://distfiles.audacious-media-player.org/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  audacious desktop-file-utils alsa-lib libsamplerate libogg
BuildRequires:  curl neon lame flac libvorbis faad ffmpeg sdl mpg123 libnotify pulseaudio libsndfile
BuildRequires:  gettext libxml

%description
Audacious is a Gtk+ audio player.

%prep
%setup -q -n %{name}-%{version}

%build
TPUT=/bin/true ./configure --prefix=/usr \
                           --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

%files
%defattr(-,root,root,-)
%{_libdir}/audacious/Container/*.so
%{_libdir}/audacious/Effect/*.so
%{_libdir}/audacious/General/*.so
%{_libdir}/audacious/Input/*.so
%{_libdir}/audacious/Input/amidi-plug/ap-alsa.so
%{_libdir}/audacious/Output/*.so
%{_libdir}/audacious/Transport/*.so
%{_libdir}/audacious/Visualization/*.so
%{_datadir}/audacious/Skins/*
%{_datadir}/audacious/ui/*.ui
%{_datadir}/locale/*/LC_MESSAGES/audacious-plugins.mo

%clean
rm -rf %{buildroot}

%changelog
* Sat Mar 22 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.4.2 to 3.4.3
* Wed Dec 4 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.4.1 to 3.4.2
* Sat Sep 28 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.4 to 3.4.1
* Fri Jul 5 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.3.4 to 3.4
* Tue Jun 18 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.3.1 to 3.3.4
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
