Name:       audacious
Version:    3.4.3
Release:    1%{?dist}
Summary:    Audacious

Group:      Applications/Multimedia
License:    GPLv2+
Url:        http://audacious-media-player.org
Source:     http://distfiles.audacious-media-player.org/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  gtk+
BuildRequires:  dbus dbus-glib alsa-lib
BuildRequires:  pcre desktop-file-utils
BuildRequires:  curl neon lame flac libvorbis faad ffmpeg sdl mpg123 libnotify pulseaudio libsndfile libsamplerate
BuildRequires:  gettext
Requires:       gtk+ desktop-file-utils

%description
Audacious is a Gtk+ audio player.

%prep
%setup -q -n %{name}-%{version}

%build
TPUT=/bin/true ./configure --prefix=/usr \
                           --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

%files
%defattr(-,root,root,-)
%{_bindir}/audacious
%{_bindir}/audtool
%{_includedir}/audacious/*.h
%{_includedir}/libaudcore/*.h
%{_includedir}/libaudgui/*.h
%{_libdir}/libaudclient.so*
%{_libdir}/libaudcore.so*
%{_libdir}/libaudgui.so*
%{_libdir}/libaudtag.so*
%{_libdir}/pkgconfig/audacious.pc
%{_libdir}/pkgconfig/audclient.pc
%{_datadir}/applications/audacious.desktop
%{_datadir}/audacious/images/*.png
%{_datadir}/audacious/AUTHORS
%{_datadir}/audacious/COPYING
%{_datadir}/icons/hicolor/48x48/apps/audacious.png
%{_datadir}/icons/hicolor/scalable/apps/audacious.svg
%{_datadir}/locale/*/LC_MESSAGES/audacious.mo
%{_mandir}/man1/aud*.1.gz

%post
gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
update-desktop-database

%postun
gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
update-desktop-database

%clean
rm -rf %{buildroot}

%changelog
* Sat Mar 22 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.4.2 to 3.4.3
* Wed Dec 4 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.4.1 to 3.4.2
* Fri Sep 27 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.4 to 3.4.1
* Fri Jul 5 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.3.4 to 3.4
* Tue Jun 18 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.3.1 to 3.3.4
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
