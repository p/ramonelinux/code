Name:       freealut
Version:    1.1.0
Release:    1%{?dist}
Summary:    OpenAL's ALUT

Group:      Applications/Multimedia
License:    GPLv2+
Url:        https://github.com/vancegroup/freealut
Source:     http://pkgs.fedoraproject.org/lookaside/pkgs/freealut/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  cmake
BuildRequires:  openal-soft

%description
freealut is a free implementation of OpenAL's ALUT standard.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/freealut-config
%{_includedir}/AL/alut.h
%{_libdir}/libalut.*a
%{_libdir}/libalut.so*
%{_libdir}/pkgconfig/freealut.pc

%clean
rm -rf %{buildroot}

%changelog
* Thu Nov 28 2013 tanggeliang <tanggeliang@gmail.com>
- create
