Name:       libcanberra
Version:    0.30
Release:    6%{?dist}
Summary:    libcanberra

Group:      Applications/Multimedia
License:    GPLv2+
Url:        http://0pointer.de/lennart/projects/libcanberra
Source:     http://0pointer.de/lennart/projects/libcanberra/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  libvorbis
BuildRequires:  alsa-lib gstreamer gtk+
BuildRequires:  gtk2 gtk-doc pulseaudio
BuildRequires:  libtool systemd-udev

%description
Libcanberra is an implementation of the XDG Sound Theme and Name Specifications, for generating event sounds on free desktops, such as GNOME.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --disable-oss \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/canberra-gtk-play
%{_bindir}/canberra-boot
%{_includedir}/canberra*.h
%{_libdir}/gnome-settings-daemon-3.0/*
%{_libdir}/gtk-3.0/*
%{_libdir}/gtk-2.0/modules/libcanberra-gtk-module.*
%{_libdir}/libcanberra*.*
%{_libdir}/pkgconfig/libcanberra*.pc
%{_datadir}/gdm/*
%{_datadir}/gnome/*
%{_datadir}/vala/*
/lib/systemd/system/canberra-system-bootup.service
/lib/systemd/system/canberra-system-shutdown-reboot.service
/lib/systemd/system/canberra-system-shutdown.service

%files doc
%defattr(-,root,root,-)
%{_docdir}/*
%{_datadir}/gtk-doc/*

%clean
rm -rf %{buildroot}

%changelog
* Sat Nov 3 2012 tanggeliang <tanggeliang@gmail.com>
- update from 0.28 to 0.30
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
