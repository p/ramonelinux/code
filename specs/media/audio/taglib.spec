Name:       taglib
Version:    1.9.1
Release:    1%{?dist}
Summary:    Taglib

Group:      Applications/Multimedia
License:    GPLv2+
Url:        http://taglib.github.com
Source:     https://github.com/taglib/taglib/releases/download/v1.9.1/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  cmake

%description
The libogg package contains the Ogg file structure.
This is useful for creating (encoding) or playing (decoding) a single physical bit stream.

%prep
%setup -q -n %{name}-%{version}

%build
mkdir build &&
cd build &&
cmake -DCMAKE_INSTALL_PREFIX=/usr \
      -DCMAKE_BUILD_TYPE=Release  \
%ifarch x86_64
      -DLIB_SUFFIX=64 \
%endif
      .. &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
cd build &&
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/taglib-config
%{_includedir}/taglib/*.h
%{_includedir}/taglib/*.tcc
%{_libdir}/libtag.so*
%{_libdir}/libtag_c.so*
%{_libdir}/pkgconfig/taglib.pc
%{_libdir}/pkgconfig/taglib_c.pc

%clean
rm -rf %{buildroot}

%changelog
* Tue Oct 22 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.8 to 1.9.1
* Sun Mar 31 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.7.2 to 1.8
* Sat Sep 29 2012 tanggeliang <tanggeliang@gmail.com>
- create
