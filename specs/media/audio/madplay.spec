Name:       madplay
Version:    0.15.2
Release:    1%{?dist}
Summary:    MPEG audio decoder and player

Group:      Applications/Multimedia
License:    GPLv2+
Url:        http://mad.sourceforge.net
Source:     http://downloads.sourceforge.net/mad/%{name}-%{version}b.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  alsa-lib
BuildRequires:  libmad libid3tag

%description
'madplay' is a command-line MPEG audio decoder and player based on the MAD library (libmad).
For details about MAD, see the libmad package distributed separately.

%prep
%setup -q -n %{name}-%{version}b

%build
./configure --prefix=/usr \
            --mandir=%{_mandir} \
            --with-alsa &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

%files
%defattr(-,root,root,-)
%{_bindir}/abxtest
%{_bindir}/madplay
%{_mandir}/man1/abxtest.1.gz
%{_mandir}/man1/madplay.1.gz
%{_datadir}/locale/*/LC_MESSAGES/madplay.mo

%clean
rm -rf %{buildroot}

%changelog
* Tue Jun 18 2013 tanggeliang <tanggeliang@gmail.com>
- create
