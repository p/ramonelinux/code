Name:       grilo-plugins
Version:    0.2.15
Release:    1%{?dist}
Summary:    grilo-plugins

Group:      User Interface/GNOME
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/%{name}/0.2/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  grilo libgcrypt sqlite itstool
BuildRequires:  libsoup gobject-introspection totem-pl-parser
BuildRequires:  gmime liboauth libmediaart
BuildRequires:  intltool gettext xml-parser

%description
Grilo-Plugins is a collection of plugins (Apple Trailers, Blip.tv, Bookmarks, Filesystem, Flickr, Jamendo, Magnatune, Rai.tv, Tracker, Youtube, between others) to make media discovery and browsing easy for applications that support Grilo framework, such as Totem (some plugins are disabled in Totem). 

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --disable-pocket \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_libdir}/grilo-0.2/grl-*.xml
%{_libdir}/grilo-0.2/libgrl*.*
%{_datadir}/help/C/*
%{_datadir}/locale/*/LC_MESSAGES/grilo-plugins.mo

%clean
rm -rf %{buildroot}

%changelog
* Wed Sep 23 2015 tanggeliang <tanggeliang@gmail.com>
- update from 0.2.12 to 0.2.15
* Sat Mar 29 2014 tanggeliang <tanggeliang@gmail.com>
- update from 0.2.11 to 0.2.12
* Mon Mar 17 2014 tanggeliang <tanggeliang@gmail.com>
- create
