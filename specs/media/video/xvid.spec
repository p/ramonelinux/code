Name:       xvid
Version:    1.3.3
Release:    1%{?dist}
Summary:    XviD

Group:      Applications/Multimedia
License:    GPLv2+
Url:        http://www.xvid.org
Source:     http://downloads.xvid.org/downloads/%{name}core-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  yasm

%description
XviD is an MPEG-4 compliant video CODEC.

%prep
%setup -q -n %{name}core

%build
cd build/generic &&
sed -i 's/^LN_S=@LN_S@/& -f -v/' platform.inc.in &&
./configure --prefix=/usr \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
cd build/generic &&
make DESTDIR=%{buildroot} install &&

chmod -v 755 %{buildroot}/%{_libdir}/libxvidcore.so.4.3 &&
ln -v -sf libxvidcore.so.4.3 %{buildroot}/%{_libdir}/libxvidcore.so.4 &&
ln -v -sf libxvidcore.so.4   %{buildroot}/%{_libdir}/libxvidcore.so   &&

install -v -m755 -d %{buildroot}/usr/share/doc/xvidcore-%{version}/examples &&
install -v -m644 ../../doc/* %{buildroot}/usr/share/doc/xvidcore-%{version} &&
install -v -m644 ../../examples/* \
    %{buildroot}/usr/share/doc/xvidcore-%{version}/examples

%files
%defattr(-,root,root,-)
%{_includedir}/xvid.h
%{_libdir}/libxvidcore.a
%{_libdir}/libxvidcore.so*
%{_docdir}/xvidcore-%{version}/*

%clean
rm -rf %{buildroot}

%changelog
* Sat Jul 26 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.3.2 to 1.3.3
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
