Name:       gnome-mplayer
Version:    1.0.8
Release:    1%{?dist}
Summary:    An MPlayer GUI, a full-featured binary

Group:      Applications/Multimedia
License:    GPLv2+
Url:        https://code.google.com/p/gnome-mplayer
Source:     http://gnome-mplayer.googlecode.com/files/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  alsa-lib dbus-glib desktop-file-utils gettext
BuildRequires:  gtk+ curl libnotify nautilus pulseaudio
BuildRequires:  gmtk mplayer
Requires:       desktop-file-utils

%description
GNOME MPlayer is a simple GUI for MPlayer.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

desktop-file-install --vendor=rpmfusion \
       --delete-original --dir %{buildroot}%{_datadir}/applications \
       %{buildroot}%{_datadir}/applications/%{name}.desktop

%files
%defattr(-,root,root,-)
%{_bindir}/gnome-mplayer
%{_libdir}/nautilus/extensions-3.0/libgnome-mplayer-properties-page.so
%{_datadir}/applications/rpmfusion-gnome-mplayer.desktop
%{_datadir}/gnome-control-center/default-apps/gnome-mplayer.xml
%{_datadir}/glib-2.0/schemas/apps.gecko-mediaplayer.preferences.gschema.xml
%{_datadir}/glib-2.0/schemas/apps.gnome-mplayer.preferences.*
%{_datadir}/locale/*/LC_MESSAGES/gnome-mplayer.mo
%{_datadir}/icons/hicolor/*/apps/gnome-mplayer.*
%{_docdir}/gnome-mplayer/*
%{_mandir}/man1/gnome-mplayer.1*

%post
/sbin/ldconfig
touch --no-create %{_datadir}/icons/hicolor &>/dev/null || :
gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
glib-compile-schemas %{_datadir}/glib-2.0/schemas &>/dev/null || :
update-desktop-database %{_datadir}/applications &>/dev/null || :

%postun
/sbin/ldconfig
if [ $1 -eq 0 ] ; then
  touch --no-create %{_datadir}/icons/hicolor &>/dev/null || :
  gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
  glib-compile-schemas %{_datadir}/glib-2.0/schemas &>/dev/null || :
fi

%changelog
* Tue Jun 18 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.0.6 to 1.0.8
