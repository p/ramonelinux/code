Name:       sdl
Version:    1.2.15
Release:    8%{?dist}
Summary:    SDL

Group:      Applications/Multimedia
License:    GPLv2+
Url:        http://www.libsdl.org
Source:     http://www.libsdl.org/release/SDL-%{version}.tar.gz
Patch:      SDL-1.2.15-const_XData32.patch

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  alsa-lib esound pulseaudio nasm libusb libx11 libxrandr aalib pth
BuildRequires:  directfb
BuildRequires:  glu libusb-compat

%description
The Simple DirectMedia Layer (SDL for short) is a cross-platform library designed to make it easy to write multimedia software, such as games and emulators.

%prep
%setup -q -n SDL-%{version}
%patch -p1

%build
./configure --prefix=/usr --disable-static \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install &&

install -v -m755 -d %{buildroot}/usr/share/doc/SDL-%{version}/html &&
install -v -m644    docs/html/*.html \
                    %{buildroot}/usr/share/doc/SDL-%{version}/html

%files
%defattr(-,root,root,-)
%{_bindir}/sdl-config
%{_includedir}/SDL/SDL.h
%{_includedir}/SDL/SDL_*.h
%{_includedir}/SDL/begin_code.h
%{_includedir}/SDL/close_code.h
%{_libdir}/libSDL-1.2.so.0*
%{_libdir}/libSDL.*
%{_libdir}/libSDLmain.*
%{_libdir}/pkgconfig/sdl.pc
%{_datadir}/aclocal/sdl.m4
%{_docdir}/SDL-%{version}/html/*
%{_mandir}/man3/SDL*.3.gz

%clean
rm -rf %{buildroot}

%changelog
* Mon Apr 30 2012 tanggeliang <tanggeliang@gmail.com>
- create
