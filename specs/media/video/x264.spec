Name:       x264
Version:    2245
Release:    1%{?dist}
Summary:    x264

Group:      System Environment/libraries
License:    GPLv2+
Url:        ftp://ftp.videolan.org/pub/videolan/x264
Source:     ftp://ftp.videolan.org/pub/videolan/x264/snapshots/%{name}-snapshot-20131007-%{version}-stable.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  yasm

%description
x264 package provides a library for encoding video streams into the H.264/MPEG-4 AVC format.

%prep
%setup -q -n %{name}-snapshot-20131007-%{version}-stable

%build
./configure --prefix=/usr \
            --enable-shared \
            --disable-cli \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_includedir}/x264*.h
%{_libdir}/libx264.so*
%{_libdir}/pkgconfig/x264.pc

%clean
rm -rf %{buildroot}

%changelog
* Sun Oct 20 2013 tanggeliang <tanggeliang@gmail.com>
- create
