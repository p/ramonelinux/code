Name:       liba52
Version:    0.7.4
Release:    6%{?dist}
Summary:    Liba52

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://liba52.sourceforge.net
Source:     http://liba52.sourceforge.net/files/a52dec-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build

%description
liba52 is a free library for decoding ATSC A/52 (also known as AC-3) streams.
The A/52 standard is used in a variety of applications, including digital television and DVD.

%prep
%setup -q -n a52dec-%{version}

%build
./configure --prefix=/usr \
            --mandir=/usr/share/man \
            --enable-shared \
            --disable-static \
            --libdir=%{_libdir} \
           CFLAGS="-g -O2 $([ $(uname -m) = x86_64 ] && echo -fPIC)" &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install &&
cp liba52/a52_internal.h %{buildroot}/usr/include/a52dec &&
install -v -m644 -D doc/liba52.txt \
    %{buildroot}/usr/share/doc/liba52-0.7.4/liba52.txt

%files
%defattr(-,root,root,-)
%{_bindir}/a52dec
%{_bindir}/extract_a52
%{_includedir}/a52dec/*.h
%{_libdir}/liba52.la
%{_libdir}/liba52.so*
%{_docdir}/liba52-0.7.4/*
%{_mandir}/man1/a52dec.1.gz
%{_mandir}/man1/extract_a52.1.gz

%clean
rm -rf %{buildroot}

%changelog
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
