Name:       fltk
Version:    1.3.3
Release:    1%{?dist}
Summary:    FLTK

Group:      Applications/Multimedia
License:    GPLv2+
Url:        http://www.fltk.org
Source:     http://ftp.easysw.com/pub/fltk/%{version}/%{name}-%{version}-source.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  libx11 mesa glu alsa-lib
BuildRequires:	hicolor-icon-theme libjpeg-turbo libpng

%description
FLTK (pronounced "fulltick") is a cross-platform C++ GUI toolkit for UNIX/Linux (X11), Microsoft Windows, and MacOS X.
FLTK provides modern GUI functionality without the bloat and supports 3D graphics via OpenGL? and its built-in GLUT emulation.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr    \
	    --enable-shared  \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/fltk-config
%{_bindir}/fluid
%{_includedir}/FL/*.H
%{_includedir}/FL/*.h
%{_libdir}/libfltk*.a
%{_libdir}/libfltk*.so*
%{_docdir}/fltk/examples/*
%{_mandir}/*

%clean
rm -rf %{buildroot}

%changelog
* Sat Sep 26 2015 tanggeliang <tanggeliang@gmail.com>
- update from 1.3.2 to 1.3.3
* Thu Dec 5 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.3.0 to 1.3.2
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
