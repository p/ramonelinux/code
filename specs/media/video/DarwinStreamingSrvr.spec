Name:       DarwinStreamingSrvr
Version:    6.0.3
Release:    1%{?dist}
Summary:    Darwin Streaming Server

Group:      Applications/Multimedia
License:    GPLv2+
Url:        http://dss.macosforge.org
Source:     dss.macosforge.org/downloads/%{name}%{version}-Source.tar
Patch:      http://www.abrahamsson.com/dss-6.0.3.patch

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build

%description
Darwin Streaming Server (DSS) is an open source project intended for developers who need to stream QuickTime and MPEG-4 media on alternative platforms such as Mac, Windows, Linux, and Solaris, or those developers who need to extend and/or modify the existing streaming server code to fit their needs.
Darwin Streaming Server is only supported by the open source community and is not eligible for technical support from Apple.

%prep
%setup -q -n %{name}%{version}-Source
%patch -p1

%build
./Buildit
./buildtarball

%check

%install
rm -rf %{buildroot}

mkdir -p %{buildroot}/tmp
tar xvf DarwinStreamingSrvr-Linux.tar.gz -C %{buildroot}/tmp

%files
%defattr(-,root,root,-)
/tmp/DarwinStreamingSrvr-Linux/*

%post
groupadd qtss
useradd qtss -g qtss
#cd /tmp/DarwinStreamingSrvr-Linux/
#./Install

%clean
rm -rf %{buildroot}

%changelog
* Sat Jul 13 2013 tanggeliang <tanggeliang@gmail.com>
- create
