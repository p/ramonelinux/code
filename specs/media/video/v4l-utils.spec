Name:           v4l-utils
Version:        1.0.0
Release:        1%{?dist}
Summary:        Collection of video4linux support libraries

Group:          System Environment/Libraries
License:        LGPLv2+ and GPLv2
URL:            http://linuxtv.org
Source:         http://linuxtv.org/downloads/%{name}/%{name}-%{version}.tar.bz2
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  libjpeg-turbo systemd

%description
v4l-utils is a collection of various video4linux (V4L) and DVB utilities.
libv4l is an accompanying collection of libraries that adds a thin abstraction layer on top of video4linux2 (V4L2) devices.
The purpose of this layer is to make it easy for application writers to support a wide variety of devices without having to write separate code for different devices in the same class.
It consists of 3 different libraries.
libv4lconvert offers functions to convert from any (known) pixel format to V4l2_PIX_FMT_BGR24 or V4l2_PIX_FMT_YUV420.
libv4l1 offers the (deprecated) v4l1 API on top of v4l2 devices, independent of the drivers for those devices supporting v4l1 compatibility (which many v4l2 drivers do not).
libv4l2 offers the v4l2 API on top of v4l2 devices, while adding support for the application transparent libv4lconvert conversion where necessary.

%package        libv4l
Summary:        libv4l
%description    libv4l

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --sysconfdir=/etc \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%clean
rm -rf %{buildroot}

%files 
%defattr(-,root,root,-)
/etc/rc_maps.cfg
/lib/udev/rc_keymaps/*
/lib/udev/rules.d/70-infrared.rules
%{_bindir}/cx18-ctl
%{_bindir}/decode_tm6000
%{_bindir}/dvb-fe-tool
%{_bindir}/dvb-format-convert
%{_bindir}/dvbv5-scan
%{_bindir}/dvbv5-zap
%{_bindir}/ir-keytable
%{_bindir}/ivtv-ctl
%{_bindir}/rds-ctl
%{_bindir}/v4l2-compliance
%{_bindir}/v4l2-ctl
%{_bindir}/v4l2-sysfs-path
%{_sbindir}/v4l2-dbg
%{_mandir}/man1/ir-keytable.1.gz

%files libv4l
%defattr(-,root,root,-)
%{_includedir}/libv4l*.h
%{_libdir}/libv4l*.so.*
%{_libdir}/libv4l
%{_libdir}/libv4l*.*a
%{_libdir}/libv4l*.so
%{_libdir}/v4l1compat.so
%{_libdir}/v4l2convert.so
%{_libdir}/pkgconfig/libv4l*.pc

%changelog
* Wed Aug 14 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.9.5 to 1.0.0
* Mon Jun 10 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.8.3 to 0.9.5
