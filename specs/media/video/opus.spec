Name:       opus
Version:    1.1
Release:    1%{?dist}
Summary:    fdk-aac

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://downloads.xiph.org/releases/opus
Source:     http://downloads.xiph.org/releases/opus/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build

%description
Opus is a lossy audio compression format developed by the Internet Engineering Task Force (IETF) that is particularly suitable for interactive speech and audio transmission over the Internet.
This package provides the Opus development library and headers.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --disable-static \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_includedir}/opus/opus*.h
%{_libdir}/libopus.la
%{_libdir}/libopus.so*
%{_libdir}/pkgconfig/opus.pc
%{_datadir}/aclocal/opus.m4

%clean
rm -rf %{buildroot}

%changelog
* Sat Mar 22 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.0.3 to 1.1
* Sun Oct 20 2013 tanggeliang <tanggeliang@gmail.com>
- create
