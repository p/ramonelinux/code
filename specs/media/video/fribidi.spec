Name:       fribidi
Version:    0.19.6
Release:    1%{?dist}
Summary:    FriBidi

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://fribidi.org
Source:     http://fribidi.org/download/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  glib

%description
The FriBidi package is an implementation of the Unicode Bidirectional Algorithm (BIDI).
This is useful for supporting Arabic and Hebrew alphabets in other packages.

%prep
%setup -q -n %{name}-%{version}

%build
sed -i "s|glib/gstrfuncs\.h|glib.h|" charset/fribidi-char-sets.c &&
sed -i "s|glib/gmem\.h|glib.h|"      lib/mem.h                   &&
./configure --prefix=/usr                                        \
            --libdir=%{_libdir}                                  &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/fribidi
%{_includedir}/fribidi/fribidi-*.h
%{_includedir}/fribidi/fribidi.h
%{_libdir}/libfribidi.la
%{_libdir}/libfribidi.so*
%{_libdir}/pkgconfig/fribidi.pc
%{_mandir}/man3/fribidi_*.3.gz

%clean
rm -rf %{buildroot}

%changelog
* Thu Mar 20 2014 tanggeliang <tanggeliang@gmail.com>
- update from 0.19.5 to 0.19.6
* Sun Oct 20 2013 tanggeliang <tanggeliang@gmail.com>
- create
