Name:       ffmpeg1
Version:    1.2.2
Release:    1%{?dist}
Summary:    FFmpeg

Group:      Applications/Multimedia
License:    GPLv2+
Url:        http://www.ffmpeg.org
Source:     http://ffmpeg.org/releases/ffmpeg-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  faac freetype lame openjpeg pulseaudio speex libtheora libvorbis libvpx xvid openssl sdl libx11 libxfixes yasm
BuildRequires:  fontconfig gnutls
BuildRequires:  libva

%description
FFmpeg is a solution to record, convert and stream audio and video.
It is a very fast video and audio converter and it can also acquire from a live audio/video source.
Designed to be intuitive, the command-line interface (ffmpeg) tries to figure out all the parameters, when possible.
FFmpeg can also convert from any sample rate to any other, and resize video on the fly with a high quality polyphase filter.
FFmpeg can use a video4linux compatible video source and any Open Sound System audio source.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n ffmpeg-%{version}

%build
sed -i 's/-lflite"/-lflite -lasound"/' configure &&
./configure --prefix=/usr        \
            --enable-gpl         \
            --enable-version3    \
            --enable-nonfree     \
            --disable-static     \
            --enable-shared      \
            --enable-x11grab     \
            --enable-libfaac     \
            --enable-libfreetype \
            --enable-libmp3lame  \
            --enable-libopenjpeg \
            --enable-libpulse    \
            --enable-libspeex    \
            --enable-libtheora   \
            --enable-libvorbis   \
            --enable-libvpx      \
            --enable-libxvid     \
            --enable-openssl     \
            --disable-debug      \
            --libdir=%{_libdir} &&
make %{?_smp_mflags} &&
gcc tools/qt-faststart.c -o tools/qt-faststart &&

%check

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot} &&
install -v -m755    tools/qt-faststart %{buildroot}/usr/bin &&
install -v -m755 -d %{buildroot}/usr/share/doc/ffmpeg-%{version} &&
install -v -m644    doc/*.txt \
                    %{buildroot}/usr/share/doc/ffmpeg-%{version}

%files
%defattr(-,root,root,-)
%{_bindir}/ffmpeg
%{_bindir}/ffplay
%{_bindir}/ffprobe
%{_bindir}/ffserver
%{_bindir}/qt-faststart
%{_includedir}/libavcodec/*.h
%{_includedir}/libavdevice/*.h
%{_includedir}/libavfilter/*.h
%{_includedir}/libavformat/*.h
%{_includedir}/libavutil/*.h
%{_includedir}/libpostproc/*.h
%{_includedir}/libswresample/*.h
%{_includedir}/libswscale/*.h
/usr/lib/libavcodec.so*
/usr/lib/libavdevice.so*
/usr/lib/libavfilter.so*
/usr/lib/libavformat.so*
/usr/lib/libavutil.so*
/usr/lib/libpostproc.so*
/usr/lib/libswresample.so*
/usr/lib/libswscale.so*
%{_libdir}/pkgconfig/libavcodec.pc
%{_libdir}/pkgconfig/libavdevice.pc
%{_libdir}/pkgconfig/libavfilter.pc
%{_libdir}/pkgconfig/libavformat.pc
%{_libdir}/pkgconfig/libavutil.pc
%{_libdir}/pkgconfig/libpostproc.pc
%{_libdir}/pkgconfig/libswresample.pc
%{_libdir}/pkgconfig/libswscale.pc
%{_datadir}/ffmpeg/examples/*
%{_datadir}/ffmpeg/ffprobe.xsd
%{_datadir}/ffmpeg/*.ffpreset

%files doc
%defattr(-,root,root,-)
%{_docdir}/ffmpeg-%{version}/*
%{_mandir}/man1/ff*.1.gz
%{_mandir}/man1/lib*.3.gz

%clean
rm -rf %{buildroot}

%changelog
* Sat Aug 24 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.2.1 to 1.2.2
* Wed May 29 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.2 to 1.2.1
* Thu Mar 21 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.0.1 to 1.2
* Sat Dec 29 2012 tanggeliang <tanggeliang@gmail.com>
- update from 0.11.1 to 1.0.1
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
