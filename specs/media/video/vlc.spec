Name:       vlc
Version:    2.1.2
Release:    1%{?dist}
Summary:    VLC

Group:      Applications/Multimedia
License:    GPLv2+
Url:        http://www.videolan.org
Source:     http://download.videolan.org/pub/videolan/vlc/%{version}/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  alsa-lib dbus ffmpeg gnutls liba52 libgcrypt libmad lua
BuildRequires:  qt4 kdelibs inetutils
BuildRequires:  libproxy libdv libdvdread libdvdnav
BuildRequires:  libogg
BuildRequires:  faad flac libmpeg2 libvorbis speex libtheora libpng
BuildRequires:  sdl freetype fontconfig librsvg aalib
BuildRequires:  pulseaudio libsamplerate
BuildRequires:  xcb-util-keysyms avahi libxml taglib
BuildRequires:  libnotify sqlite
BuildRequires:  gettext autoconf automake m4

%description
VLC is a media player, streamer, and encoder.
It can play from many inputs like files, network streams, capture device, desktops, or DVD, SVCD, VCD, and audio CD.
It can play most audio and video codecs (MPEG 1/2/4, H264, VC-1, DivX, WMV, Vorbis, AC3, AAC, etc.), but can also convert to different formats and/or send streams through the network.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
sed -i 's:libsmbclient.h:samba-4.0/&:' configure.ac modules/access/smb.c &&
./bootstrap &&
./configure --prefix=/usr \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make docdir=%{_datadir}/doc/vlc-%{version} DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/cvlc
%{_bindir}/qvlc
%{_bindir}/rvlc
%{_bindir}/vlc
%{_bindir}/vlc-wrapper
%{_includedir}/vlc/deprecated.h
%{_includedir}/vlc/libvlc*.h
%{_includedir}/vlc/plugins/vlc_*.h
%{_includedir}/vlc/vlc.h
%{_libdir}/libvlc.la
%{_libdir}/libvlc.so*
%{_libdir}/libvlccore.la
%{_libdir}/libvlccore.so*
%{_libdir}/pkgconfig/libvlc.pc
%{_libdir}/pkgconfig/vlc-plugin.pc
%{_libdir}/vlc/lua/*
%{_libdir}/vlc/plugins/*
%{_libdir}/vlc/vlc-cache-gen
%{_libdir}/vlc/libcompat.*a
%{_datadir}/applications/vlc.desktop
%{_datadir}/apps/solid/actions/vlc-open*.desktop
%{_datadir}/icons/hicolor/*/apps/vlc*.png
%{_datadir}/icons/hicolor/*/apps/vlc*.xpm
%{_datadir}/locale/*/LC_MESSAGES/vlc.mo
%{_datadir}/vlc/lua/*
%{_datadir}/vlc/utils/*-vlc-default.sh
%{_datadir}/vlc/vlc.ico

%files doc
%defattr(-,root,root,-)
%{_docdir}/vlc-%{version}/*
%{_mandir}/man1/vlc*.1.gz

%post
gtk-update-icon-cache &&
update-desktop-database

%clean
rm -rf %{buildroot}

%changelog
* Sun Dec 15 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.1.1 to 2.1.2
* Thu Dec 5 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.1.0 to 2.1.1
* Mon Oct 7 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.0.8 to 2.1.0
* Fri Aug 2 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.0.6 to 2.0.8
* Mon Jun 10 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.0.5 to 2.0.6
* Wed Feb 27 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.0.3 to 2.0.5
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
