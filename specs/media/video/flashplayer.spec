Name:       flashplayer
Version:    11.2.202.394
Release:    1%{?dist}
Summary:    Flash Player

Group:      Applications/Multimedia
License:    GPLv2+
Url:        www.adobe.com
Source0:    www.adobe.com/cn/products/flashplayer/install_flash_player_11_linux.i386.tar.gz
Source1:    www.adobe.com/cn/products/flashplayer/install_flash_player_11_linux.x86_64.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
Requires:       firefox

%description
Adobe Flash Player.

%package        kde
Summary:        KDE
%description    kde

%prep
%setup -c -T
%ifarch %{ix86}
tar xvf %{SOURCE0}
%else %ifarch x86_64
tar xvf %{SOURCE1}
%endif

%build

%check

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}/%{_libdir}/mozilla/plugins/
cp -a usr %{buildroot}/
cp libflashplayer.so %{buildroot}/%{_libdir}/mozilla/plugins/

%files
%defattr(-,root,root,-)
%{_bindir}/flash-player-properties
%{_libdir}/mozilla/plugins/libflashplayer.so
%{_datadir}/applications/flash-player-properties.desktop
%{_datadir}/icons/hicolor/*/apps/flash-player-properties.png
%{_datadir}/kde4/services/kcm_adobe_flash_player.desktop
%{_datadir}/pixmaps/flash-player-properties.png

%files kde
%defattr(-,root,root,-)
/usr/lib/kde4/kcm_adobe_flash_player.so
%{_libdir}/kde4/kcm_adobe_flash_player.so

%clean
rm -rf %{buildroot}

%changelog
* Sat Jul 26 2014 tanggeliang <tanggeliang@gmail.com>
- update from 11.2.202.297 to 11.2.202.394
* Sun Jul 14 2013 tanggeliang <tanggeliang@gmail.com>
- update from 11 to 11.2.202.297
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
