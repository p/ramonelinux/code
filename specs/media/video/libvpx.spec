Name:       libvpx
Version:    1.3.0
Release:    1%{?dist}
Summary:    libvpx

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://webm.googlecode.com
Source:     http://webm.googlecode.com/files/%{name}-v%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  yasm which

%description
This package provides the reference implementation of the vp8 Codec from the WebM project, used in most current html5 video.

%prep
%setup -q -n %{name}-v%{version}

%build
sed -i 's/cp -p/cp/' build/make/Makefile &&
chmod -v 644 vpx/*.h &&
mkdir ../libvpx-build &&
cd ../libvpx-build &&
../libvpx-v%{version}/configure --prefix=/usr \
                                --enable-shared \
                                --disable-static \
                                --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
cd ../libvpx-build &&
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/vp8_scalable_patterns
%{_bindir}/vp9_spatial_scalable_encoder
%{_bindir}/vpxdec
%{_bindir}/vpxenc
%{_includedir}/vpx/*.h
%{_libdir}/libvpx.so*
%{_libdir}/pkgconfig/vpx.pc

%clean
rm -rf %{buildroot}
rm -rf ../libvpx-build

%changelog
* Fri Mar 21 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.1.0 to 1.3.0
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
