Name:       sdl2
Version:    2.0.0
Release:    1%{?dist}
Summary:    SDL 2.0

Group:      Applications/Multimedia
License:    GPLv2+
Url:        http://www.libsdl.org
Source:     http://www.libsdl.org/release/SDL2-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  alsa-lib esound pulseaudio nasm libusb libx11 libxrandr aalib pth
BuildRequires:  directfb
BuildRequires:  glu libusb-compat

%description
The Simple DirectMedia Layer (SDL for short) is a cross-platform library designed to make it easy to write multimedia software, such as games and emulators.

%prep
%setup -q -n SDL2-%{version}

%build
./configure --prefix=/usr \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install &&

%files
%defattr(-,root,root,-)
%{_bindir}/sdl2-config
%{_includedir}/SDL2/SDL.h
%{_includedir}/SDL2/SDL_*.h
%{_includedir}/SDL2/begin_code.h
%{_includedir}/SDL2/close_code.h
%{_libdir}/libSDL2-2.0.so.0*
%{_libdir}/libSDL2.*
%{_libdir}/libSDL2_test.a
%{_libdir}/libSDL2main.a
%{_libdir}/pkgconfig/sdl2.pc
%{_datadir}/aclocal/sdl2.m4

%clean
rm -rf %{buildroot}

%changelog
* Mon Oct 14 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.2.15 to 2.0.0
* Mon Apr 30 2012 tanggeliang <tanggeliang@gmail.com>
- create
