Name:       libmpeg2
Version:    0.5.1
Release:    2%{?dist}
Summary:    libmpeg2

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://libmpeg2.sourceforge.net
Source:     http://libmpeg2.sourceforge.net/files/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  libx11 sdl

%description
The libmpeg2 package contains a library for decoding MPEG-2 and MPEG-1 video streams. The library is able to decode all MPEG streams that conform to certain restrictions: “constrained parameters” for MPEG-1, and “main profile” for MPEG-2.
This is useful for programs and applications needing to decode MPEG-2 and MPEG-1 video streams.

%prep
%setup -q -n %{name}-%{version}

%build
sed -i 's/static const/static/' libmpeg2/idct_mmx.c
./configure --prefix=/usr --enable-shared \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install &&
install -v -m755 -d %{buildroot}/usr/share/doc/mpeg2dec-0.5.1 &&
install -v -m644 README doc/libmpeg2.txt \
                    %{buildroot}/usr/share/doc/mpeg2dec-0.5.1

%files
%defattr(-,root,root,-)
%{_bindir}/corrupt_mpeg2
%{_bindir}/extract_mpeg2
%{_bindir}/mpeg2dec
%{_includedir}/mpeg2dec/mpeg2*.h
%{_libdir}/libmpeg2.*
%{_libdir}/libmpeg2convert.*
%{_libdir}/pkgconfig/libmpeg2.pc
%{_libdir}/pkgconfig/libmpeg2convert.pc
%{_docdir}/mpeg2dec-0.5.1/*
%{_mandir}/man1/*mpeg2*.1.gz

%clean
rm -rf %{buildroot}

%changelog
* Sat Sep 29 2012 tanggeliang <tanggeliang@gmail.com>
- create
