Name:       mplayer
Version:    1.1.1
Release:    3%{?dist}
Summary:    MPlayer

Group:      Applications/Multimedia
License:    GPLv2+
Url:        http://www.mplayerhq.hu
Source0:    http://www.mplayerhq.hu/MPlayer/releases/MPlayer-%{version}.tar.xz
Source1:    Clearlooks-1.5.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  yasm
BuildRequires:  gtk2 gtk+
BuildRequires:  alsa-lib esound pulseaudio sdl
BuildRequires:  aalib directfb giflib libjpeg-turbo libmng libpng openjpeg
BuildRequires:  ffmpeg libmad speex libtheora mpg123 faac libdv xvid libvpx lame
BuildRequires:  fontconfig freetype unrar libxslt docbook-xml docbook-xsl

%description
MPlayer is a powerful audio/video player controlled via the command line or graphical interface which is able to play almost every popular audio and video file format.
With supported video hardware and additional drivers, MPlayer can play video files without an X Window System installed.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n MPlayer-%{version}

%build
./configure --prefix=/usr \
            --confdir=/etc/mplayer \
            --enable-dynamic-plugins \
            --enable-menu \
            --enable-gui &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

tar -xvf %SOURCE1 \
        -C %{buildroot}/usr/share/mplayer/skins &&
ln -sfv clearplayer %{buildroot}/usr/share/mplayer/skins/default

%files
%defattr(-,root,root,-)
%{_bindir}/gmplayer
%{_bindir}/mencoder
%{_bindir}/mplayer
%{_datadir}/applications/mplayer.desktop
%{_datadir}/icons/hicolor/*/apps/mplayer.png

%{_datadir}/mplayer/skins/Clearlooks/*
%{_datadir}/mplayer/skins/default

%files doc
%defattr(-,root,root,-)
%{_mandir}/man1/*.gz

%post
gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :

%clean
rm -rf %{buildroot}

%changelog
* Tue Jun 18 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.1 to 1.1.1
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
