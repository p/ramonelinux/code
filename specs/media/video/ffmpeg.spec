Name:       ffmpeg
Version:    2.7.2
Release:    1%{?dist}
Summary:    FFmpeg

Group:      Applications/Multimedia
License:    GPLv2+
Url:        http://www.ffmpeg.org
Source:     http://ffmpeg.org/releases/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  yasm libass fdk-aac lame opus libtheora libvorbis libvpx x264
BuildRequires:  libx11 libxfixes alsa-lib sdl libva libvdpau
BuildRequires:  faac freetype openjpeg pulseaudio speex xvid openssl fontconfig gnutls

%description
FFmpeg is a solution to record, convert and stream audio and video.
It is a very fast video and audio converter and it can also acquire from a live audio/video source.
Designed to be intuitive, the command-line interface (ffmpeg) tries to figure out all the parameters, when possible.
FFmpeg can also convert from any sample rate to any other, and resize video on the fly with a high quality polyphase filter.
FFmpeg can use a video4linux compatible video source and any Open Sound System audio source.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
sed -i 's/-lflite"/-lflite -lasound"/' configure &&
./configure --prefix=/usr        \
            --enable-gpl         \
            --enable-version3    \
            --enable-nonfree     \
            --disable-static     \
            --enable-shared      \
            --disable-debug      \
            --enable-libass      \
            --enable-libfdk-aac  \
            --enable-libfreetype \
            --enable-libmp3lame  \
            --enable-libopus     \
            --enable-libtheora   \
            --enable-libvorbis   \
            --enable-libvpx      \
            --enable-libx264     \
            --enable-x11grab     \
            --docdir=%{_docdir}/ffmpeg-%{version} \
            --libdir=%{_libdir} &&
make %{?_smp_mflags} &&
gcc tools/qt-faststart.c -o tools/qt-faststart &&

%check

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot} &&
install -v -m755    tools/qt-faststart %{buildroot}/usr/bin &&
install -v -m755 -d %{buildroot}/usr/share/doc/ffmpeg-%{version} &&
install -v -m644    doc/*.txt \
                    %{buildroot}/usr/share/doc/ffmpeg-%{version}

%files
%defattr(-,root,root,-)
%{_bindir}/ffmpeg
%{_bindir}/ffplay
%{_bindir}/ffprobe
%{_bindir}/ffserver
%{_bindir}/qt-faststart
%{_includedir}/libavcodec/*.h
%{_includedir}/libavdevice/*.h
%{_includedir}/libavfilter/*.h
%{_includedir}/libavformat/*.h
%{_includedir}/libavutil/*.h
%{_includedir}/libpostproc/*.h
%{_includedir}/libswresample/*.h
%{_includedir}/libswscale/*.h
%{_libdir}/libavcodec.so*
%{_libdir}/libavdevice.so*
%{_libdir}/libavfilter.so*
%{_libdir}/libavformat.so*
%{_libdir}/libavutil.so*
%{_libdir}/libpostproc.so*
%{_libdir}/libswresample.so*
%{_libdir}/libswscale.so*
%{_libdir}/pkgconfig/libavcodec.pc
%{_libdir}/pkgconfig/libavdevice.pc
%{_libdir}/pkgconfig/libavfilter.pc
%{_libdir}/pkgconfig/libavformat.pc
%{_libdir}/pkgconfig/libavutil.pc
%{_libdir}/pkgconfig/libpostproc.pc
%{_libdir}/pkgconfig/libswresample.pc
%{_libdir}/pkgconfig/libswscale.pc
%{_datadir}/ffmpeg/examples/*
%{_datadir}/ffmpeg/ffprobe.xsd
%{_datadir}/ffmpeg/*.ffpreset

%files doc
%defattr(-,root,root,-)
%{_docdir}/ffmpeg-%{version}/*
%{_mandir}/man1/ff*.1.gz
%{_mandir}/man3/lib*.3.gz

%clean
rm -rf %{buildroot}

%changelog
* Sat Sep 26 2015 tanggeliang <tanggeliang@gmail.com>
- update from 2.1.4 to 2.7.2
* Sat Mar 22 2014 tanggeliang <tanggeliang@gmail.com>
- update from 2.1.1 to 2.1.4
* Wed Dec 4 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.0.2 to 2.1.1
* Sun Oct 20 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.2.2 to 2.0.2
* Sat Aug 24 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.2.1 to 1.2.2
* Wed May 29 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.2 to 1.2.1
* Thu Mar 21 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.0.1 to 1.2
* Sat Dec 29 2012 tanggeliang <tanggeliang@gmail.com>
- update from 0.11.1 to 1.0.1
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
