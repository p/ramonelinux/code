Name:       fdk-aac
Version:    0.1.4
Release:    1%{?dist}
Summary:    fdk-aac

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://sourceforge.net/projects/opencore-amr
Source:     http://sourceforge.net/projects/opencore-amr/files/fdk-aac/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  libtool gettext
BuildRequires:  autoconf automake m4

%description
fdk-aac package provides the Fraunhofer FDK AAC library, which is purported to be a high quality Advanced Audio Coding implementation.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --disable-static \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_includedir}/fdk-aac/*.h
%{_libdir}/libfdk-aac.la
%{_libdir}/libfdk-aac.so*
%{_libdir}/pkgconfig/fdk-aac.pc

%clean
rm -rf %{buildroot}

%changelog
* Sat Sep 26 2015 tanggeliang <tanggeliang@gmail.com>
- update from 0.1.2 to 0.1.4
* Sun Oct 20 2013 tanggeliang <tanggeliang@gmail.com>
- create
