Name:       libass
Version:    0.11.2
Release:    1%{?dist}
Summary:    libass

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://libass.googlecode.com
Source:     http://libass.googlecode.com/files/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  freetype fribidi
BuildRequires:  fontconfig harfbuzz

%description
libass is a portable subtitle renderer for the ASS/SSA (Advanced Substation Alpha/Substation Alpha) subtitle format that allows for more advanced subtitles than the conventional SRT and similar formats.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --disable-static \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_includedir}/ass/ass*.h
%{_libdir}/libass.la
%{_libdir}/libass.so*
%{_libdir}/pkgconfig/libass.pc

%clean
rm -rf %{buildroot}

%changelog
* Sat Jul 26 2014 tanggeliang <tanggeliang@gmail.com>
- update from 0.11.1 to 0.11.2
* Sat Mar 22 2014 tanggeliang <tanggeliang@gmail.com>
- update from 0.10.2 to 0.11.1
* Sun Oct 20 2013 tanggeliang <tanggeliang@gmail.com>
- create
