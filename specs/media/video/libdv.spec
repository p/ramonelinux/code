Name:       libdv
Version:    1.0.0
Release:    2%{?dist}
Summary:    Libdv

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://libdv.sourceforge.net
Source:     http://downloads.sourceforge.net/libdv/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  popt sdl libx11

%description
The Quasar DV Codec (libdv) is a software CODEC for DV video, the encoding format used by most digital camcorders.
It can be used to copy videos from camcorders using a firewire (IEEE 1394) connection.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --disable-xv \
            --disable-static \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install &&
install -v -m755 -d      %{buildroot}/usr/share/doc/libdv-1.0.0 &&
install -v -m644 README* %{buildroot}/usr/share/doc/libdv-1.0.0

%files
%defattr(-,root,root,-)
%{_bindir}/dubdv
%{_bindir}/dvconnect
%{_bindir}/encodedv
%{_includedir}/libdv/dv*.h
%{_libdir}/libdv.la
%{_libdir}/libdv.so*
%{_libdir}/pkgconfig/libdv.pc
%{_docdir}/libdv-1.0.0/README*
%{_mandir}/man1/*.1.gz

%clean
rm -rf %{buildroot}

%changelog
* Sat Sep 29 2012 tanggeliang <tanggeliang@gmail.com>
- create
