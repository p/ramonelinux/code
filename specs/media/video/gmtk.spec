Name:       gmtk
Version:    1.0.8
Release:    3%{?dist}
Summary:    gnome-mplayer toolkit

Group:      User Interface/GNOME
License:    GPLv2+
Url:        http://code.google.com/gmtk
Source:     http://%{name}.googlecode.com/files/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  alsa-lib pulseaudio gtk+
BuildRequires:  intltool gettext xml-parser
Requires:       mplayer

%description
Library of common functions and widgets for gnome-mplayer and gecko-mediaplayer

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

%files
%defattr(-,root,root,-)
%{_includedir}/gmtk/gm*.h
%{_libdir}/libgmlib.*a
%{_libdir}/libgmlib.so*
%{_libdir}/libgmtk.*a
%{_libdir}/libgmtk.so*
%{_libdir}/pkgconfig/gmlib.pc
%{_libdir}/pkgconfig/gmtk.pc
%{_datadir}/locale/*/LC_MESSAGES/gmtk.mo
%{_docdir}/gmtk/*

%changelog
* Tue Jun 18 2013 tanggeliang tanggeliang@gmail.com
- update from 1.0.6 to 1.0.8
