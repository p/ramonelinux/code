Name:       libtheora
Version:    1.1.1
Release:    7%{?dist}
Summary:    libtheora

Group:      Applications/Multimedia
License:    GPLv2+
Url:        http://www.xiph.org
Source:     http://downloads.xiph.org/releases/theora/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  libogg, libvorbis

%description
Theora is a free and open video compression format.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_includedir}/theora/*.h
%{_libdir}/libtheora.*
%{_libdir}/libtheoradec.*
%{_libdir}/libtheoraenc.*
%{_libdir}/pkgconfig/theora.pc
%{_libdir}/pkgconfig/theoradec.pc
%{_libdir}/pkgconfig/theoraenc.pc

%files doc
%defattr(-,root,root,-)
%{_docdir}/libtheora-1.1.1/*

%clean
rm -rf %{buildroot}

%changelog
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
