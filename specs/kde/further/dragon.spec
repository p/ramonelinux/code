Name:	    dragon
Version:	4.11.4
Release:	1%{?dist}
Summary:	dragon

Group:		User Interface/KDE
License:	GPLv2+
Url:		http://www.kde.org
Source:     ftp://ftp.kde.org/pub/kde/stable/%{version}/src/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:	cmake kdelibs
BuildRequires:  automoc docbook-xml docbook-xsl

%description
Dragon is a video player.

%prep
%setup -q -n %{name}-%{version}

%build
mkdir build &&
cd build &&
cmake -DCMAKE_INSTALL_PREFIX=/usr \
      -DCMAKE_BUILD_TYPE=Release \
%ifarch x86_64
      -DLIB_SUFFIX=64 \
%endif
      .. &&
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
cd build
make install DESTDIR=%{buildroot}

%files
%defattr(-,root,root,-)
%{_bindir}/dragon
%{_libdir}/kde4/dragonpart.so
%{_datadir}/applications/kde4/dragonplayer.desktop
%{_datadir}/apps/dragonplayer/dragonlogo.png
%{_datadir}/apps/dragonplayer/dragonplayerui.rc
%{_datadir}/apps/solid/actions/dragonplayer-opendvd.desktop
%{_datadir}/config/dragonplayerrc
%{_datadir}/icons/hicolor/*x*/apps/dragonplayer.png
%{_datadir}/icons/hicolor/scalable/apps/dragonplayer.svgz
%{_datadir}/icons/oxygen/*x*/actions/player-volume-muted.png
%{_datadir}/icons/oxygen/scalable/actions/player-volume-muted.svgz
%{_datadir}/kde4/services/ServiceMenus/dragonplayer_play_dvd.desktop
%{_datadir}/kde4/services/dragonplayer_part.desktop
%{_docdir}/HTML/en/dragonplayer/*
%{_mandir}/man1/dragon.1.gz

%clean
rm -rf %{buildroot}

%changelog
* Mon Dec 16 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.11.0 to 4.11.4
* Mon Aug 19 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.10.5 to 4.11.0
* Sun Aug 18 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.10.3 to 4.10.5
* Mon Jun 10 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.10.2 to 4.10.3
* Wed Apr 10 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.10.1 to 4.10.2
* Sun Mar 17 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.10.0 to 4.10.1
* Sun Mar 3 2013 tanggeliang <tanggeliang@gmail.com>
- create
