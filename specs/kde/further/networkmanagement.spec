Name:	    networkmanagement
Version:	0.9.0.9
Release:	2%{?dist}
Summary:	KDE Network

Group:		User Interface/KDE
License:	GPLv2+
Url:		http://www.kde.org
Source:     ftp://ftp.kde.org/pub/kde/unstable/%{name}/%{version}/src/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:	cmake kdelibs networkmanager
BuildRequires:  automoc kde-workspace mobile-broadband-provider-info
BuildRequires:  gettext
Requires:       networkmanager

%description
A Plasma applet to control your wired and wireless network(s) in KDE 4 using the default NetworkManager service.

%prep
%setup -q -n %{name}-%{version}

%build
mkdir build &&
cd build &&
cmake -DCMAKE_INSTALL_PREFIX=/usr \
      -DCMAKE_BUILD_TYPE=Release \
%ifarch x86_64
      -DLIB_SUFFIX=64 \
%endif
      .. &&
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
cd build
make install DESTDIR=%{buildroot}

%files
%defattr(-,root,root,-)
%{_includedir}/solid/controlnm09/ifaces/*.h
%{_includedir}/solid/controlnm09/*.h
%{_libdir}/kde4/kcm_networkmanagement.so
%{_libdir}/kde4/kcm_networkmanagement_tray.so
%{_libdir}/kde4/kded_networkmanagement.so
%{_libdir}/kde4/libexec/networkmanagement_configshell
%{_libdir}/kde4/networkmanagement_*.so
%{_libdir}/kde4/plasma_applet_networkmanagement.so
%{_libdir}/kde4/plasma_engine_networkmanagement.so
%{_libdir}/kde4/solid_modemmanager05.so
%{_libdir}/kde4/solid_networkmanager09.so
%{_libdir}/libknm_nm.so
%{_libdir}/libknmclient.so*
%{_libdir}/libknminternals.so*
%{_libdir}/libknmservice.so*
%{_libdir}/libknmui.so*
%{_libdir}/libsolidcontrolfuture.so
%{_libdir}/libsolidcontrolnm09.so*
%{_libdir}/libsolidcontrolnm09ifaces.so*
%{_datadir}/apps/desktoptheme/default/icons/network2.svgz
%{_datadir}/apps/networkmanagement/networkmanagement.notifyrc
%{_datadir}/icons/oxygen/*x*/apps/networkmanager.png
%{_datadir}/icons/oxygen/*x*/devices/network-*.png
%{_datadir}/kde4/services/kcm_networkmanagement.desktop
%{_datadir}/kde4/services/kcm_networkmanagement_tray.desktop
%{_datadir}/kde4/services/kded/networkmanagement.desktop
%{_datadir}/kde4/services/networkmanagement_*.desktop
%{_datadir}/kde4/services/plasma-applet-networkmanagement.desktop
%{_datadir}/kde4/services/plasma-engine-networkmanagement.desktop
%{_datadir}/kde4/services/solidbackends/solid_modemmanager05.desktop
%{_datadir}/kde4/services/solidbackends/solid_networkmanager09.desktop
%{_datadir}/kde4/servicetypes/networkmanagement_vpnuiplugin.desktop
%{_datadir}/kde4/servicetypes/solidmodemmanagernm09.desktop
%{_datadir}/kde4/servicetypes/solidnetworkmanagernm09.desktop
%{_datadir}/locale/*/LC_MESSAGES/*.mo

%clean
rm -rf %{buildroot}

%changelog
* Sun Aug 18 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.9.0.8 to 0.9.0.9
* Wed Apr 10 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.9.0.7 to 0.9.0.8
* Wed Mar 13 2013 tanggeliang <tanggeliang@gmail.com>
- create
