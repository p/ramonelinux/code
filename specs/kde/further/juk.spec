Name:	    juk
Version:	4.11.4
Release:	1%{?dist}
Summary:	juk

Group:		User Interface/KDE
License:	GPLv2+
Url:		http://www.kde.org
Source:     ftp://ftp.kde.org/pub/kde/stable/%{version}/src/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:	cmake kdelibs
BuildRequires:  automoc taglib docbook-xml docbook-xsl

%description
Juk is a lightweight music player.

%prep
%setup -q -n %{name}-%{version}

%build
mkdir build &&
cd build &&
cmake -DCMAKE_INSTALL_PREFIX=/usr \
      -DCMAKE_BUILD_TYPE=Release \
      .. &&
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
cd build
make install DESTDIR=%{buildroot}

%files
%defattr(-,root,root,-)
%{_bindir}/juk
%{_datadir}/applications/kde4/juk.desktop
%{_datadir}/apps/juk/juk.notifyrc
%{_datadir}/apps/juk/jukui*.rc
%{_datadir}/apps/juk/pics/playing.png
%{_datadir}/apps/juk/pics/splash.png
%{_datadir}/apps/juk/pics/theme.svg
%{_datadir}/dbus-1/interfaces/org.kde.juk.*.xml
%{_datadir}/icons/hicolor/*x*/apps/juk.png
%{_datadir}/kde4/services/ServiceMenus/jukservicemenu.desktop
%{_docdir}/HTML/en/juk/*

%clean
rm -rf %{buildroot}

%changelog
* Mon Dec 16 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.11.0 to 4.11.4
* Mon Aug 19 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.10.5 to 4.11.0
* Sun Aug 18 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.10.3 to 4.10.5
* Mon Jun 10 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.10.2 to 4.10.3
* Wed Apr 10 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.10.1 to 4.10.2
* Sun Mar 17 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.10.0 to 4.10.1
* Sun Mar 3 2013 tanggeliang <tanggeliang@gmail.com>
- create
