Name:	    kdenetwork
Version:	4.11.0
Release:	1%{?dist}
Summary:	KDE Network

Group:		User Interface/KDE
License:	GPLv2+
Url:		http://www.kde.org
Source:     ftp://ftp.kde.org/pub/kde/stable/%{version}/src/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:	cmake kdelibs
BuildRequires:  automoc kdepimlibs qimageblitz
BuildRequires:  jasper kdepimlibs v4l-utils-libv4l docbook-xml docbook-xsl

%description
KDE Network.

%prep
%setup -q -n %{name}-%{version}

%build
mkdir build &&
cd build &&
cmake -DCMAKE_INSTALL_PREFIX=/usr \
      -DCMAKE_BUILD_TYPE=Release \
%ifarch x86_64
      -DLIB_SUFFIX=64 \
%endif
      .. &&
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
cd build
make install DESTDIR=%{buildroot}

%files
%defattr(-,root,root,-)
%{_bindir}/kget
%{_bindir}/kopete
%{_bindir}/kopete_latexconvert.sh
%{_bindir}/kppp
%{_bindir}/kppplogview
%{_bindir}/krdc
%{_bindir}/krfb
%{_bindir}/winpopup-install
%{_bindir}/winpopup-send
%{_includedir}/kopete/*.h
%{_includedir}/kopete/ui/*.h
%{_includedir}/krdc/*.h
%{_libdir}/kde4/kcm_*so
%{_libdir}/kde4/kded_dnssdwatcher.so
%{_libdir}/kde4/kget_*.so
%{_libdir}/kde4/kio_zeroconf.so
%{_libdir}/kde4/kopete_*.so
%{_libdir}/kde4/krdc_*.so
%{_libdir}/kde4/krfb_framebuffer_*.so
%{_libdir}/kde4/krunner_kget.so
%{_libdir}/kde4/libchattexteditpart.so
%{_libdir}/kde4/plasma_*.so
%{_libdir}/kde4/plugins/accessible/chatwindowaccessiblewidgetfactory.so
%{_libdir}/kde4/sambausershareplugin.so
%{_libdir}/libkgetcore.so*
%{_libdir}/libkopete.so*
%{_libdir}/libkopete_oscar.so*
%{_libdir}/libkopete_videodevice.so*
%{_libdir}/libkopeteaddaccountwizard.so*
%{_libdir}/libkopetechatwindow_shared.so*
%{_libdir}/libkopetecontactlist.so*
%{_libdir}/libkopeteidentity.so*
%{_libdir}/libkopeteprivacy.so*
%{_libdir}/libkopetestatusmenu.so*
%{_libdir}/libkrdccore.so*
%{_libdir}/libkrfbprivate.so.%{version}
%{_libdir}/libkyahoo.so*
%{_libdir}/liboscar.so*
%{_libdir}/libqgroupwise.so
%{_libdir}/mozilla/plugins/skypebuttons.so
%{_libdir}/strigi/strigita_torrent_analyzer.so
%{_datadir}/applications/kde4/*.desktop
%{_datadir}/apps/dolphinpart/kpartplugins/kget_plug_in.*
%{_datadir}/apps/kconf_update/*
%{_datadir}/apps/kget/*
%{_datadir}/apps/khtml/kpartplugins/kget_plug_in.*
%{_datadir}/apps/kopete_*/*.rc
%{_datadir}/apps/kopete/*
%{_datadir}/apps/kopeterichtexteditpart/kopeterichtexteditpartfull.rc
%{_datadir}/apps/kppp/*
%{_datadir}/apps/krdc/*
%{_datadir}/apps/krfb/krfb.notifyrc
%{_datadir}/apps/kwebkitpart/kpartplugins/kget_plug_in.*
%{_datadir}/apps/remoteview/zeroconf.desktop
%{_datadir}/config.kcfg/*.kcfg
%{_datadir}/config/kopeterc
%{_datadir}/dbus-1/interfaces/org.kde.*.xml
%{_datadir}/dbus-1/services/org.kde.kget.service
%{_datadir}/icons/hicolor/*x*/apps/kget.png
%{_datadir}/icons/hicolor/*x*/apps/kopete-offline.png
%{_datadir}/icons/hicolor/*x*/apps/kopete.png
%{_datadir}/icons/hicolor/*x*/apps/kppp.png
%{_datadir}/icons/hicolor/scalable/apps/kget.svgz
%{_datadir}/icons/hicolor/scalable/apps/kopete-offline.svgz
%{_datadir}/icons/hicolor/scalable/apps/kopete.svgz
%{_datadir}/icons/oxygen/*x*/actions/*.png
%{_datadir}/icons/oxygen/32x32/actions/newmessage.mng
%{_datadir}/icons/oxygen/scalable/actions/*.svgz
%{_datadir}/kde4/services/*.protocol
%{_datadir}/kde4/services/*.desktop
%{_datadir}/kde4/services/*/*.desktop
%{_datadir}/kde4/servicetypes/*.desktop
%{_datadir}/sounds/KDE-Im-Phone-Ring.wav
%{_datadir}/sounds/Kopete_*.ogg
%{_docdir}/HTML/en/*

%clean
rm -rf %{buildroot}

%changelog
* Mon Aug 19 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.10.5 to 4.11.0
* Sun Aug 18 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.10.3 to 4.10.5
* Mon Jun 10 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.10.2 to 4.10.3
* Wed Apr 10 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.10.1 to 4.10.2
* Sun Mar 17 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.10.0 to 4.10.1
* Mon Mar 4 2013 tanggeliang <tanggeliang@gmail.com>
- create
