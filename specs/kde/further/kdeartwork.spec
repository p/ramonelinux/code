Name:	    kdeartwork
Version:	4.11.0
Release:	1%{?dist}
Summary:	kdeartwork

Group:		User Interface/KDE
License:	GPLv2+
Url:		http://www.kde.org
Source:     ftp://ftp.kde.org/pub/kde/stable/%{version}/src/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:	cmake kdelibs
BuildRequires:  automoc kde-workspace

%description
Kdeartwork is a collection of wallpapers, icon themes, screensavers, widget styles etc. 

%prep
%setup -q -n %{name}-%{version}

%build
mkdir build &&
cd build &&
cmake -DCMAKE_INSTALL_PREFIX=/usr \
      -DCMAKE_BUILD_TYPE=Release \
%ifarch x86_64
      -DLIB_SUFFIX=64 \
%endif
      .. &&
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
cd build
make install DESTDIR=%{buildroot}

%files
%defattr(-,root,root,-)
%{_bindir}/kbanner.kss
%{_bindir}/kblob.kss
%{_bindir}/kclock.kss
%{_bindir}/kdeasciiquarium.kss
%{_bindir}/klines.kss
%{_bindir}/klorenz.kss
%{_bindir}/kpartsaver.kss
%{_bindir}/kpolygon.kss
%{_bindir}/kscience.kss
%{_bindir}/kslideshow.kss
%{_bindir}/kvm.kss
%{_libdir}/kde4/kstyle_phase_config.so
%{_libdir}/kde4/kwin3_*.so
%{_libdir}/kde4/kwin_*.so
%{_libdir}/kde4/plugins/styles/phasestyle.so
%{_datadir}/apps/color-schemes/*.colors
%{_datadir}/apps/desktoptheme/*/colors
%{_datadir}/apps/desktoptheme/*/metadata.desktop
%{_datadir}/apps/desktoptheme/Androbit/dialogs/*.svgz
%{_datadir}/apps/desktoptheme/Androbit/opaque/dialogs/*.svgz
%{_datadir}/apps/desktoptheme/Androbit/opaque/widgets/*.svgz
%{_datadir}/apps/desktoptheme/Androbit/widgets/*.svgz
%{_datadir}/apps/desktoptheme/Aya/dialogs/*.svgz
%{_datadir}/apps/desktoptheme/Aya/opaque/dialogs/*.svgz
%{_datadir}/apps/desktoptheme/Aya/opaque/widgets/*.svgz
%{_datadir}/apps/desktoptheme/Aya/widgets/*.svgz
%{_datadir}/apps/desktoptheme/Produkt/dialogs/*.svgz
%{_datadir}/apps/desktoptheme/Produkt/icons/*.svgz
%{_datadir}/apps/desktoptheme/Produkt/translucent/dialogs/*.svgz
%{_datadir}/apps/desktoptheme/Produkt/translucent/widgets/*.svgz
%{_datadir}/apps/desktoptheme/Produkt/widgets/*.svgz
%{_datadir}/apps/desktoptheme/Tibanna/dialogs/*.svgz
%{_datadir}/apps/desktoptheme/Tibanna/opaque/dialogs/*.svgz
%{_datadir}/apps/desktoptheme/Tibanna/opaque/widgets/*.svgz
%{_datadir}/apps/desktoptheme/Tibanna/widgets/*.svgz
%{_datadir}/apps/desktoptheme/slim-glow/LICENSE
%{_datadir}/apps/desktoptheme/slim-glow/dialogs/*.svgz
%{_datadir}/apps/desktoptheme/slim-glow/icons/*.svgz
%{_datadir}/apps/desktoptheme/slim-glow/opaque/dialogs/*.svgz
%{_datadir}/apps/desktoptheme/slim-glow/opaque/widgets/*.svgz
%{_datadir}/apps/desktoptheme/slim-glow/widgets/*.svgz
%{_datadir}/apps/kscreensaver/*.png
%{_datadir}/apps/kstyle/themes/phase.themerc
%{_datadir}/apps/kwin/*.desktop
%{_datadir}/emoticons/Boxed/*.png
%{_datadir}/emoticons/KMess-Blue/*.png
%{_datadir}/emoticons/KMess-Cartoon/*.png
%{_datadir}/emoticons/KMess-Violet/*.png
%{_datadir}/emoticons/KMess/*.png
%{_datadir}/emoticons/Plain/*.png
%{_datadir}/emoticons/RedOnes/*.png
%{_datadir}/emoticons/ccmathteam.com/*.png
%{_datadir}/emoticons/greggman.com/*.png
%{_datadir}/emoticons/phpBB/*.png
%{_datadir}/emoticons/tweakers.net/*.png
%{_datadir}/emoticons/*/emoticons.xml
%{_datadir}/icons/mono/*x*/animations/process-idle.png
%{_datadir}/icons/mono/*x*/animations/process-working.png
%{_datadir}/icons/mono/256x256/actions/kde.png
%{_datadir}/icons/mono/index.theme
%{_datadir}/icons/mono/scalable/actions/*.svgz
%{_datadir}/icons/mono/scalable/apps/*.svgz
%{_datadir}/icons/mono/scalable/categories/*.svgz
%{_datadir}/icons/mono/scalable/devices/*.svgz
%{_datadir}/icons/mono/scalable/emblems/*.svgz
%{_datadir}/icons/mono/scalable/emotes/*.svgz
%{_datadir}/icons/mono/scalable/mimetypes/*.svgz
%{_datadir}/icons/mono/scalable/places/*.svgz
%{_datadir}/icons/mono/scalable/status/*.svgz
%{_datadir}/icons/nuvola/*x*/actions/*.png
%{_datadir}/icons/nuvola/*x*/apps/*.png
%{_datadir}/icons/nuvola/*x*/categories/*.png
%{_datadir}/icons/nuvola/*x*/devices/*.png
%{_datadir}/icons/nuvola/*x*/emblems/*.png
%{_datadir}/icons/nuvola/*x*/mimetypes/*.png
%{_datadir}/icons/nuvola/*x*/places/*.png
%{_datadir}/icons/nuvola/*x*/status/*.png
%{_datadir}/icons/nuvola/index.theme
%{_datadir}/icons/nuvola/scalable/actions/*.svg
%{_datadir}/icons/nuvola/scalable/apps/*.svg
%{_datadir}/icons/nuvola/scalable/devices/*.svg
%{_datadir}/icons/nuvola/scalable/mimetypes/*.svg
%{_datadir}/icons/nuvola/scalable/places/*.svg
%{_datadir}/icons/nuvola/scalable/status/*.svg
%{_datadir}/kde4/services/ScreenSavers/*.desktop
%{_datadir}/sounds/KDE_*_new.wav
%{_datadir}/wallpapers/Aghi/contents/images/*x*.jpg
%{_datadir}/wallpapers/Air/contents/images/*x*.jpg
%{_datadir}/wallpapers/Atra_Dot/contents/images/*x*.jpg
%{_datadir}/wallpapers/Autumn/contents/images/*x*.jpg
%{_datadir}/wallpapers/Beach_Reflecting_Clouds/contents/images/*x*.jpg
%{_datadir}/wallpapers/Blue_Curl/contents/images/*x*.jpg
%{_datadir}/wallpapers/Blue_Wood/contents/images/*x*.jpg
%{_datadir}/wallpapers/Chess/contents/images/*x*.jpg
%{_datadir}/wallpapers/City_at_Night/contents/images/*x*.jpg
%{_datadir}/wallpapers/Code_Poets_Dream/contents/images/*x*.jpg
%{_datadir}/wallpapers/Colorado_Farm/contents/images/*x*.jpg
%{_datadir}/wallpapers/Curls_on_Green/contents/images/*x*.jpg
%{_datadir}/wallpapers/Damselfly/contents/images/*x*.jpg
%{_datadir}/wallpapers/EOS/contents/images/*x*.jpg
%{_datadir}/wallpapers/Emotion/contents/images/*x*.jpg
%{_datadir}/wallpapers/Ethais/contents/images/*x*.png
%{_datadir}/wallpapers/Evening/contents/images/*x*.jpg
%{_datadir}/wallpapers/Field/contents/images/*x*.jpg
%{_datadir}/wallpapers/Fields_of_Peace/contents/images/*x*.JPG
%{_datadir}/wallpapers/Finally_Summer_in_Germany/contents/images/*x*.jpg
%{_datadir}/wallpapers/Flower_drops/contents/images/*x*.jpg
%{_datadir}/wallpapers/Fresh_Morning/contents/images/*x*.jpg
%{_datadir}/wallpapers/Golden_Ripples/contents/images/*x*.jpg
%{_datadir}/wallpapers/Grass/contents/images/3648x2280.jpg
%{_datadir}/wallpapers/Green_Concentration/contents/images/*.jpg
%{_datadir}/wallpapers/Hanami/contents/images/*.jpg
%{_datadir}/wallpapers/HighTide/contents/images/*.jpg
%{_datadir}/wallpapers/Holiday_Cactus/contents/images/*.jpg
%{_datadir}/wallpapers/Icy_Tree/contents/images/*.jpg
%{_datadir}/wallpapers/JK_Bridge_at_Night/contents/images/*.jpg
%{_datadir}/wallpapers/Korea/contents/images/*.jpg
%{_datadir}/wallpapers/Ladybuggin/contents/images/*.jpg
%{_datadir}/wallpapers/Leafs_Labyrinth/contents/images/*.jpg
%{_datadir}/wallpapers/Media_Life/contents/images/*.jpg
%{_datadir}/wallpapers/Midnight_in_Karelia/contents/images/*.jpg
%{_datadir}/wallpapers/Plasmalicious/contents/images/*x*jpg
%{_datadir}/wallpapers/Quadros/contents/images/*x*.jpg
%{_datadir}/wallpapers/Red_Leaf/contents/images/*x*.jpg
%{_datadir}/wallpapers/Skeeter_Hawk/contents/images/*x*.jpg
%{_datadir}/wallpapers/Spring_Sunray/contents/images/*x*.jpg
%{_datadir}/wallpapers/Storm/contents/images/*x*.jpg
%{_datadir}/wallpapers/Storm/contents/contents.png
%{_datadir}/wallpapers/The_Rings_of_Saturn/contents/images/*x*.jpg
%{_datadir}/wallpapers/There_is_Rain_on_the_Table/contents/images/*x*.jpg
%{_datadir}/wallpapers/Vector_Sunset/contents/images/*x*.jpg
%{_datadir}/wallpapers/Winter_Track/contents/images/*x*.jpg
%{_datadir}/wallpapers/Yellow_Flowers/contents/images/*x*.jpg
%{_datadir}/wallpapers/*/contents/screenshot.jpg
%{_datadir}/wallpapers/*/contents/screenshot.png
%{_datadir}/wallpapers/*/metadata.desktop

%clean
rm -rf %{buildroot}

%changelog
* Mon Aug 19 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.10.5 to 4.11.0
* Sun Aug 18 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.10.3 to 4.10.5
* Mon Jun 10 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.10.2 to 4.10.3
* Wed Apr 10 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.10.1 to 4.10.2
* Sun Mar 17 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.10.0 to 4.10.1
* Sun Mar 3 2013 tanggeliang <tanggeliang@gmail.com>
- create
