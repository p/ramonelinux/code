Name:	    kde-wallpapers
Version:	4.11.4
Release:	1%{?dist}
Summary:	KDE Wallpapers

Group:		User Interface/KDE
License:	GPLv2+
Url:		http://www.kde.org
Source:     ftp://ftp.kde.org/pub/kde/stable/%{version}/src/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildArch:      noarch
BuildRequires:	cmake kdelibs
BuildRequires:  automoc

%description
This package provides wallpapers for KDE.

%prep
%setup -q -n %{name}-%{version}

%build
mkdir build &&
cd build &&
cmake -DCMAKE_INSTALL_PREFIX=/usr \
      -DCMAKE_BUILD_TYPE=Release \
      .. &&
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
cd build
make install DESTDIR=%{buildroot}

%files
%defattr(-,root,root,-)
%{_datadir}/wallpapers/Ariya/contents/images/*x*.png
%{_datadir}/wallpapers/Auros/contents/images/5408x3464.png
%{_datadir}/wallpapers/Autumn/contents/images/*x*.jpg
%{_datadir}/wallpapers/Azul/contents/images/*x*.jpg
%{_datadir}/wallpapers/Blue_Wood/contents/images/*x*.jpg
%{_datadir}/wallpapers/Castilla_Sky/contents/images/*x*.jpg
%{_datadir}/wallpapers/Elarun/contents/images/2560x1600.png
%{_datadir}/wallpapers/Flores/contents/images/1920x1278.jpg
%{_datadir}/wallpapers/Flying_Field/contents/images/*x*.jpg
%{_datadir}/wallpapers/Fog_on_the_West_Lake/contents/images/*x*.jpg
%{_datadir}/wallpapers/Grass/contents/images/*x*.jpg
%{_datadir}/wallpapers/Hanami/contents/images/*x*.jpg
%{_datadir}/wallpapers/Horos/contents/images/*x*.png
%{_datadir}/wallpapers/Media_Life/contents/images/*x*.jpg
%{_datadir}/wallpapers/Prato/contents/images/4386x2920.jpg
%{_datadir}/wallpapers/*/contents/screenshot.png
%{_datadir}/wallpapers/*/metadata.desktop

%clean
rm -rf %{buildroot}

%changelog
* Mon Dec 16 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.11.2 to 4.11.4
* Tue Oct 15 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.11.0 to 4.11.2
* Mon Aug 19 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.10.5 to 4.11.0
* Sun Aug 18 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.10.3 to 4.10.5
* Mon Jun 10 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.10.2 to 4.10.3
* Wed Apr 10 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.10.1 to 4.10.2
* Sun Mar 17 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.10.0 to 4.10.1
* Sun Mar 3 2013 tanggeliang <tanggeliang@gmail.com>
- create
