Name:		polkit-qt
Version:	0.103.0
Release:	6%{?dist}
Summary:	Polkit-Qt

Group:		User Interface/KDE
License:	GPLv2+
Url:		http://www.kde.org
Source:     ftp://ftp.kde.org/pub/kde/stable/apps/KDE4.x/admin/%{name}-1-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:	cmake automoc polkit glib

%description
Polkit-Qt provides an API to polkit in the Qt environment.

%prep
%setup -q -n %{name}-1-%{version}

%build
mkdir build &&
cd build &&
CMAKE_PREFIX_PATH=/usr \
      cmake -DCMAKE_INSTALL_PREFIX=/usr \
%ifarch x86_64
      -DLIB_SUFFIX=64 \
%endif
      .. &&
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
cd build
make install DESTDIR=%{buildroot}

%files
%defattr(-,root,root,-)
%doc
%{_includedir}/polkit-qt-1/PolkitQt1/*
%{_includedir}/polkit-qt-1/polkitqt1-*.h
%{_libdir}/cmake/PolkitQt-1/PolkitQt-1Config*.cmake
%{_libdir}/libpolkit-qt-agent-1.so*
%{_libdir}/libpolkit-qt-core-1.so*
%{_libdir}/libpolkit-qt-gui-1.so*
%{_libdir}/pkgconfig/polkit-qt-*1.pc

%clean
rm -rf %{buildroot}

%changelog
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
