Name:		kde-runtime
Version:	4.11.4
Release:	1%{?dist}
Summary:	Kde-runtime

Group:		User Interface/KDE
License:	GPLv2+
Url:		http://www.kde.org
Source:     ftp://ftp.kde.org/pub/kde/stable/%{version}/src/%{name}-%{version}.tar.xz
Patch:      %{name}-%{version}-rpc_fix-1.patch

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:	cmake kdelibs libtirpc
BuildRequires:  kactivities kdepimlibs alsa-lib libjpeg-turbo exiv
BuildRequires:  pulseaudio networkmanager
BuildRequires:  automoc shared-mime-info phonon
BuildRequires:  docbook-xml docbook-xsl

%description
Kde-runtime contains runtime applications and libraries essential for KDE.

%prep
%setup -q -n %{name}-%{version}
%patch -p1

%build
mkdir build &&
cd build &&
cmake -DCMAKE_INSTALL_PREFIX=/usr \
      -DSYSCONF_INSTALL_DIR=/etc \
      -DCMAKE_BUILD_TYPE=Release \
%ifarch x86_64
      -DLIB_SUFFIX=64 \
%endif
      .. &&
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
cd build
make install DESTDIR=%{buildroot} &&
ln -s ../%{_lib}/kde4/libexec/kdesu %{buildroot}/usr/bin/kdesu

rm -rf %{buildroot}/%{_datadir}/icons/hicolor/index.theme

%files
%defattr(-,root,root,-)
%doc
/etc/dbus-1/system.d/org.kde.kcontrol.kcmremotewidgets.conf
/etc/xdg/menus/kde-information.menu
%{_bindir}/kcmshell4
%{_bindir}/kde-cp
%{_bindir}/kde-mv
%{_bindir}/kde-open
%{_bindir}/kde4
%{_bindir}/kde4-menu
%{_bindir}/kdebugdialog
%{_bindir}/kdesu
%{_bindir}/keditfiletype
%{_bindir}/kfile4
%{_bindir}/kglobalaccel
%{_bindir}/khelpcenter
%{_bindir}/khotnewstuff-upload
%{_bindir}/khotnewstuff4
%{_bindir}/kiconfinder
%{_bindir}/kioclient
%{_bindir}/kmimetypefinder
%{_bindir}/knotify4
%{_bindir}/kquitapp
%{_bindir}/kreadconfig
%{_bindir}/kstart
%{_bindir}/ksvgtopng
%{_bindir}/ktraderclient
%{_bindir}/ktrash
%{_bindir}/kuiserver
%{_bindir}/kwalletd
%{_bindir}/kwriteconfig
%{_bindir}/plasma-remote-helper
%{_bindir}/plasmapkg
%{_bindir}/solid-hardware
%{_includedir}/knotify*.h
%{_libdir}/attica_kde.so
%{_libdir}/kconf_update_bin/phonon_device*_update
%{_libdir}/kde4/*
%{_libdir}/libkdeinit4_*.so
%{_libdir}/libknotifyplugin.so
%{_libdir}/libkwalletbackend.so*
%{_libdir}/libmolletnetwork.so*
%{_datadir}/applications/kde4/*.desktop
%{_datadir}/apps/*
%{_datadir}/config.kcfg/*.kcfg
%{_datadir}/config/*.knsrc
%{_datadir}/config/kshorturifilterrc
%{_datadir}/dbus-1/interfaces/org.kde.*.xml
%{_datadir}/dbus-1/services/org.kde.*.service
%{_datadir}/dbus-1/system-services/org.kde.kcontrol.kcmremotewidgets.service
%{_datadir}/desktop-directories/kde-*.directory
%{_datadir}/emoticons/kde4/*
%{_datadir}/icons/default.kde4
%{_datadir}/icons/hicolor/*/apps/knetattach.png
%{_datadir}/icons/hicolor/scalable/apps/knetattach.svgz
%{_datadir}/kde4/services/*
%{_datadir}/kde4/servicetypes/*.desktop
%{_datadir}/locale/currency/*.desktop
%{_datadir}/locale/l10n/*.desktop
%{_datadir}/locale/l10n/*/entry.desktop
%{_datadir}/locale/l10n/*/flag.png
%{_datadir}/mime/packages/network.xml
%{_datadir}/polkit-1/actions/org.kde.kcontrol.kcmremotewidgets.policy
%{_datadir}/sounds/KDE-*.ogg
%{_docdir}/HTML/*
%{_mandir}/man*/*.gz

%clean
rm -rf %{buildroot}

%changelog
* Sun Dec 15 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.11.2 to 4.11.4
* Tue Oct 29 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.11.1 to 4.11.2
* Mon Sep 23 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.11.0 to 4.11.1
* Mon Aug 19 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.10.5 to 4.11.0
* Sun Aug 18 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.10.3 to 4.10.5
* Mon Jun 10 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.10.2 to 4.10.3
* Wed Apr 10 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.10.1 to 4.10.2
* Sat Mar 16 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.10.0 to 4.10.1
* Fri Mar 1 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.9.0 to 4.10.0
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
