Name:		phonon-backend-vlc
Version:	0.7.1
Release:	1%{?dist}
Summary:	Phonon-backend-vlc

Group:		User Interface/KDE
License:	GPLv2+
Url:		http://www.kde.org
Source:     ftp://ftp.kde.org/pub/kde/stable/phonon/%{name}/src/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:	phonon vlc
BuildRequires:	cmake automoc

%description
This package provides a Phonon backend which utilizes the VLC media framework.

%prep
%setup -q -n %{name}-%{version}

%build
mkdir build &&
cd build &&
cmake -DCMAKE_INSTALL_PREFIX=/usr \
      -DCMAKE_BUILD_TYPE=Release \
      .. &&
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
cd build
make install DESTDIR=%{buildroot}

%files
%defattr(-,root,root,-)
%{_libdir}/kde4/plugins/phonon_backend/phonon_vlc.so
%{_datadir}/kde4/services/phononbackends/vlc.desktop

%clean
rm -rf %{buildroot}

%changelog
* Sun Dec 15 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.6.2 to 0.7.1
* Sat Mar 16 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.6.0 to 0.6.2
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
