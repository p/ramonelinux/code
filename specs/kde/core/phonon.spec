Name:		phonon
Version:	4.7.1
Release:	1%{?dist}
Summary:	Phonon

Group:		User Interface/KDE
License:	GPLv2+
Url:		http://www.kde.org
Source:     ftp://ftp.kde.org/pub/kde/stable/%{name}/%{version}/src/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:	cmake automoc glib pulseaudio

%description
Phonon is the multimedia API for KDE4. It replaces the old aRts, that is no longer supported by KDE.
It supports backends like GStreamer and VLC.

%prep
%setup -q -n %{name}-%{version}

%build
mkdir build &&
cd build &&
cmake -DCMAKE_INSTALL_PREFIX=/usr \
      -DCMAKE_BUILD_TYPE=Release \
      -DPHONON_INSTALL_QT_EXTENSIONS_INTO_SYSTEM_QT=TRUE \
      -DDBUS_INTERFACES_INSTALL_DIR=/usr/share/dbus-1/interfaces \
%ifarch x86_64
      -DLIB_SUFFIX=64 \
%endif
      .. &&
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
cd build
make install DESTDIR=%{buildroot}

%files
%defattr(-,root,root,-)
%{_includedir}/KDE/Phonon/*
%{_includedir}/phonon/*
%{_libdir}/cmake/phonon/Phonon*.cmake
%{_libdir}/libphonon.so*
%{_libdir}/libphononexperimental.so*
%{_libdir}/pkgconfig/phonon.pc
%{_libdir}/qt4/plugins/designer/libphononwidgets.so
%{_datadir}/dbus-1/interfaces/org.kde.Phonon.AudioOutput.xml
%{_datadir}/phonon/buildsystem/*
%{_datadir}/qt4/mkspecs/modules/qt_phonon.pri

%clean
rm -rf %{buildroot}

%changelog
* Sun Dec 15 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.7.0 to 4.7.1
* Thu Dec 5 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.6.0 to 4.7.0
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
