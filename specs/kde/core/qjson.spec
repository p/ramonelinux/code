Name:		qjson
Version:	0.8.1
Release:	2%{?dist}
Summary:	QJson

Group:		User Interface/KDE
License:	GPLv2+
Url:		http://qjson.sourceforge.net
Source:     http://downloads.sourceforge.net/project/%{name}/%{name}/%{version}/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:	cmake qt

%description
JSON is a lightweight data-interchange format.
It can represents integer, real number, string, an ordered sequence of value, and a collection of name/value pairs.
QJson is a qt-based library that maps JSON data to QVariant objects.

%prep
%setup -q -n %{name}-%{version}

%build
mkdir build &&
cd build &&
cmake -DCMAKE_INSTALL_PREFIX=/usr \
%ifarch x86_64
      -DLIB_SUFFIX=64 \
%endif
      .. &&
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
cd build
make install DESTDIR=%{buildroot}

%files
%defattr(-,root,root,-)
%{_includedir}/qjson/*.h
%{_libdir}/cmake/qjson/QJSON*.cmake
%{_libdir}/libqjson.so*
%{_libdir}/pkgconfig/QJson.pc

%clean
rm -rf %{buildroot}

%changelog
* Fri Mar 1 2013 tanggeliang <tanggeliang@gmail.com>
- create
