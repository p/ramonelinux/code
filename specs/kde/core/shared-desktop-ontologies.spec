Name:		shared-desktop-ontologies
Version:	0.11.0
Release:	1%{?dist}
Summary:	Shared-Desktop-Ontologies

Group:		User Interface/KDE
License:	GPLv2+
Url:		http://www.kde.org
Source:     ftp://ftp.kde.org/pub/kde/stable/%{name}/src/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:	cmake

%description
The Shared desktop ontologies provide RDF vocabularies for the Semantic Desktop.
This includes basic ontologies like RDF and RDFS and all the Nepomuk ontologies like NRL, NIE, or NFO which are also maintained and developed in this open-source project.

%prep
%setup -q -n %{name}-%{version}

%build
mkdir build &&
cd build &&
cmake -DCMAKE_INSTALL_PREFIX=/usr \
%ifarch x86_64
      -DLIB_SUFFIX=64 \
%endif
      .. &&

%install
rm -rf %{buildroot}
cd build
make install DESTDIR=%{buildroot}

%files
%defattr(-,root,root,-)
%doc
%{_datadir}/cmake/SharedDesktopOntologies/SharedDesktopOntologiesConfig*.cmake
%{_datadir}/ontology/*/*.ontology
%{_datadir}/ontology/*/*.trig
%{_datadir}/pkgconfig/shared-desktop-ontologies.pc

%clean
rm -rf %{buildroot}

%changelog
* Sun Aug 18 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.10.0 to 0.11.0
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
