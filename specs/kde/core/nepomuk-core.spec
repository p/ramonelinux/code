Name:		nepomuk-core
Version:	4.11.4
Release:	1%{?dist}
Summary:	Nepomuk-core

Group:		User Interface/KDE
License:	GPLv2+
Url:		http://www.kde.org
Source:     ftp://ftp.kde.org/pub/kde/stable/%{version}/src/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:	cmake kdelibs
BuildRequires:  poppler taglib exiv ffmpeg
BuildRequires:	automoc shared-desktop-ontologies

%description
Nepomuk-core contains the Semantik Desktop core libraries.
This includes central services like file indexing, file system monitoring, query, and of course storage, as well as the corresponding client libraries.

%prep
%setup -q -n %{name}-%{version}

%build
mkdir build &&
cd build &&
cmake -DCMAKE_INSTALL_PREFIX=/usr \
      -DCMAKE_BUILD_TYPE=Release \
%ifarch x86_64
      -DLIB_SUFFIX=64 \
%endif
      .. &&
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
cd build
make install DESTDIR=%{buildroot}

%files
%defattr(-,root,root,-)
/etc/dbus-1/system.d/org.kde.nepomuk.filewatch.conf
%{_bindir}/nepomuk-simpleresource-rcgen
%{_bindir}/nepomuk2-rcgen
%{_bindir}/nepomukbackup
%{_bindir}/nepomukcleaner
%{_bindir}/nepomukcmd
%{_bindir}/nepomukctl
%{_bindir}/nepomukfileindexer
%{_bindir}/nepomukfilewatch
%{_bindir}/nepomukindexer
%{_bindir}/nepomukmigrator
%{_bindir}/nepomuksearch
%{_bindir}/nepomukserver
%{_bindir}/nepomukservicestub
%{_bindir}/nepomukshow
%{_bindir}/nepomukstorage
%{_includedir}/Nepomuk2/*
%{_includedir}/nepomuk2/*.h
%{_libdir}/cmake/NepomukCore/Nepomuk*.cmake
%{_libdir}/kde4/libexec/kde_nepomuk_filewatch_raiselimit
%{_libdir}/kde4/nepomuk*.so
%{_libdir}/libkdeinit4_nepomukserver.so
%{_libdir}/libnepomukcleaner.so*
%{_libdir}/libnepomukcommon.so
%{_libdir}/libnepomukcore.so*
%{_libdir}/libnepomukextractor.so
%{_datadir}/applications/kde4/nepomukbackup.desktop
%{_datadir}/applications/kde4/nepomukcleaner.desktop
%{_datadir}/apps/fileindexerservice/nepomukfileindexer.notifyrc
%{_datadir}/apps/nepomukfilewatch/nepomukfilewatch.notifyrc
%{_datadir}/apps/nepomukstorage/nepomukstorage.notifyrc
%{_datadir}/autostart/nepomukserver.desktop
%{_datadir}/dbus-1/interfaces/org.kde.NepomukServer.xml
%{_datadir}/dbus-1/interfaces/org.kde.nepomuk.*.xml
%{_datadir}/dbus-1/system-services/org.kde.nepomuk.filewatch.service
%{_datadir}/kde4/services/nepomuk*.desktop
%{_datadir}/kde4/servicetypes/nepomukcleaningjob.desktop
%{_datadir}/kde4/servicetypes/nepomukextractor.desktop
%{_datadir}/kde4/servicetypes/nepomukservice.desktop
%{_datadir}/kde4/servicetypes/nepomukservice2.desktop
%{_datadir}/ontology/kde/*.ontology
%{_datadir}/ontology/kde/*.trig
%{_datadir}/polkit-1/actions/org.kde.nepomuk.filewatch.policy

%clean
rm -rf %{buildroot}

%changelog
* Sun Dec 15 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.11.2 to 4.11.4
* Mon Oct 21 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.11.1 to 4.11.2
* Mon Sep 23 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.11.0 to 4.11.1
* Mon Aug 19 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.10.5 to 4.11.0
* Tue Aug 6 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.10.3 to 4.10.5
* Mon Jun 10 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.10.2 to 4.10.3
* Wed Apr 10 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.10.1 to 4.10.2
* Sat Mar 16 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.10.0 to 4.10.1
* Fri Mar 1 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.9.0 to 4.10.0
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
