Name:		kde-workspace
Version:	4.11.4
Release:	1%{?dist}
Summary:	Kde-workspace

Group:		User Interface/KDE
License:	GPLv2+
Url:		http://www.kde.org
Source:     ftp://ftp.kde.org/pub/kde/stable/%{version}/src/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:	cmake kactivities qimageblitz xcb-util-image xcb-util-renderutil xcb-util-keysyms xcb-util-wm
BuildRequires:  kdepimlibs nepomuk-core boost freetype pciutils consolekit
BuildRequires:  linux-pam libusb networkmanager
BuildRequires:  libxcomposite libxrandr libxcursor libxft shared-mime-info
BuildRequires:  automoc libxkbfile docbook-xml docbook-xsl phonon
Requires:       systemd

%description
The Kde-workspace package contains components that are central to providing the KDE desktop environment.
Of particular importance are KWin, the KDE window manager, and Plasma, which provides the workspace interface.

%prep
%setup -q -n %{name}-%{version}

%build
mkdir build &&
cd build &&
cmake -DCMAKE_INSTALL_PREFIX=/usr \
      -DSYSCONF_INSTALL_DIR=/etc \
      -DCMAKE_BUILD_TYPE=Release \
      -DINSTALL_PYTHON_FILES_IN_PYTHON_PREFIX=TRUE \
%ifarch x86_64
      -DLIB_SUFFIX=64 \
%endif
      .. &&
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
cd build
make install DESTDIR=%{buildroot}
mkdir -p %{buildroot}/usr/share/xsessions &&
ln -sf /usr/share/apps/kdm/sessions/kde-plasma.desktop \
       %{buildroot}/usr/share/xsessions/kde-plasma.desktop

mkdir -p %{buildroot}/etc/pam.d
cat >> %{buildroot}/etc/pam.d/kde << "EOF" &&
# Begin /etc/pam.d/kde

auth     requisite      pam_nologin.so
auth     required       pam_env.so

auth     required       pam_succeed_if.so uid >= 1000 quiet
auth     include        system-auth

account  include        system-account
password include        system-password
session  include        system-session

# End /etc/pam.d/kde
EOF
cat > %{buildroot}/etc/pam.d/kde-np << "EOF" &&
# Begin /etc/pam.d/kde-np

auth     requisite      pam_nologin.so
auth     required       pam_env.so

auth     required       pam_succeed_if.so uid >= 1000 quiet
auth     required       pam_permit.so

account  include        system-account
password include        system-password
session  include        system-session

# End /etc/pam.d/kde-np
EOF
cat > %{buildroot}/etc/pam.d/kscreensaver << "EOF"
# Begin /etc/pam.d/kscreensaver

auth    include system-auth
account include system-account

# End /etc/pam.d/kscreensaver
EOF

mkdir -p %{buildroot}/lib/systemd/system
cat > %{buildroot}/lib/systemd/system/kdm.service << "EOF"
[Unit]
Description=K Display Manager
Requires=getty@tty7.service plymouth-quit.service
After=systemd-user-sessions.service getty@tty7.service plymouth-quit.service

[Service]
ExecStart=/usr/bin/kdm
StandardOutput=syslog

[Install]
Alias=display-manager.service
EOF

%files
%defattr(-,root,root,-)
/etc/pam.d/kde
/etc/pam.d/kde-np
/etc/pam.d/kscreensaver
/etc/dbus-1/system.d/org.kde.kcontrol.kcmclock.conf
/etc/dbus-1/system.d/org.kde.kcontrol.kcmkdm.conf
/etc/dbus-1/system.d/org.kde.ksysguard.processlisthelper.conf
/etc/dbus-1/system.d/org.kde.powerdevil.backlighthelper.conf
/etc/ksysguarddrc
%{_bindir}/genkdmconf
%{_bindir}/kaccess
%{_bindir}/kapplymousetheme
%{_bindir}/kblankscrn.kss
%{_bindir}/kcheckrunning
%{_bindir}/kcminit
%{_bindir}/kcminit_startup
%{_bindir}/kdm
%{_bindir}/kdmctl
%{_bindir}/kdostartupconfig4
%{_bindir}/kinfocenter
%{_bindir}/klipper
%{_bindir}/kmenuedit
%{_bindir}/krandom.kss
%{_bindir}/krandrstartup
%{_bindir}/krandrtray
%{_bindir}/krdb
%{_bindir}/krunner
%{_bindir}/ksmserver
%{_bindir}/ksplashqml
%{_bindir}/ksplashsimple
%{_bindir}/ksplashx
%{_bindir}/ksplashx_scale
%{_bindir}/kstartupconfig4
%{_bindir}/ksysguard
%{_bindir}/ksysguardd
%{_bindir}/ksystraycmd
%{_bindir}/kwin
%{_bindir}/kwin_gles
%{_bindir}/kwrited
%{_bindir}/oxygen-demo
%{_bindir}/oxygen-settings
%{_bindir}/oxygen-shadow-demo
%{_bindir}/plasma-desktop
%{_bindir}/plasma-netbook
%{_bindir}/plasma-overlay
%{_bindir}/plasma-windowed
%{_bindir}/solid-action-desktop-gen
%{_bindir}/startkde
%{_bindir}/systemsettings
%{_includedir}/*
%{_libdir}/cmake/KDE4Workspace/KDE4Workspace*.cmake
%{_libdir}/kconf_update_bin/*
%{_libdir}/kde4/*
%{_libdir}/libkdecorations.so*
%{_libdir}/libkdeinit4_*.so
%{_libdir}/libkephal.so*
%{_libdir}/libkhotkeysprivate.so.4*
%{_libdir}/libkickoff.so
%{_libdir}/libkscreensaver.so*
%{_libdir}/libksgrd.so*
%{_libdir}/libksignalplotter.so*
%{_libdir}/libkwineffects.so*
%{_libdir}/libkwinglesutils.so*
%{_libdir}/libkwinglutils.so*
%{_libdir}/libkworkspace.so*
%{_libdir}/liblsofui.so*
%{_libdir}/liboxygenstyle.so*
%{_libdir}/liboxygenstyleconfig.so*
%{_libdir}/libplasma-geolocation-interface.so*
%{_libdir}/libplasma_applet-system-monitor.so*
%{_libdir}/libplasmaclock.so*
%{_libdir}/libplasmagenericshell.so*
%{_libdir}/libpowerdevilconfigcommonprivate.so*
%{_libdir}/libpowerdevilcore.so*
%{_libdir}/libpowerdevilui.so*
%{_libdir}/libprocesscore.so*
%{_libdir}/libprocessui.so*
%{_libdir}/libsystemsettingsview.so*
%{_libdir}/libtaskmanager.so*
%{_libdir}/libweather_ion.so*
%{_libdir}/python2.7/site-packages/PyKDE4/plasmascript.py*
%{_datadir}/applications/kde4/*.desktop
%{_datadir}/apps/*
%{_datadir}/autostart/*.desktop
%{_datadir}/config.kcfg/*.kcfg
%{_datadir}/config/*
%{_datadir}/dbus-1/interfaces/org.kde.*.xml
%{_datadir}/dbus-1/interfaces/com.canonical.AppMenu.Registrar.xml
%{_datadir}/dbus-1/services/org.kde.*.service
%{_datadir}/dbus-1/system-services/org.kde.*.service
%{_datadir}/icons/KDE_Classic/cursors/*
%{_datadir}/icons/KDE_Classic/index.theme
%{_datadir}/icons/Oxygen_Black/cursors/*
%{_datadir}/icons/Oxygen_Black/index.theme
%{_datadir}/icons/Oxygen_Blue/cursors/*
%{_datadir}/icons/Oxygen_Blue/index.theme
%{_datadir}/icons/Oxygen_White/cursors/*
%{_datadir}/icons/Oxygen_White/index.theme
%{_datadir}/icons/Oxygen_Yellow/cursors/*
%{_datadir}/icons/Oxygen_Yellow/index.theme
%{_datadir}/icons/Oxygen_Zion/cursors/*
%{_datadir}/icons/Oxygen_Zion/index.theme
%{_datadir}/icons/hicolor/*/apps/*.png
%{_datadir}/icons/oxygen/*/apps/*.png
%{_datadir}/icons/oxygen/scalable/apps/*.svgz
%{_datadir}/kde4/services/*
%{_datadir}/kde4/servicetypes/*.desktop
%{_datadir}/polkit-1/actions/org.kde.*.policy
%{_datadir}/sounds/pop.wav
%{_datadir}/wallpapers/stripes.png*
%{_datadir}/xsessions/kde-plasma.desktop
%{_docdir}/HTML/en/*
/lib/systemd/system/kdm.service

%post
groupadd -g 37 kdm &&
useradd -c "KDM Daemon Owner" -d /var/lib/kdm -g kdm \
        -u 37 -s /bin/false kdm &&
install -o kdm -g kdm -dm755 /var/lib/kdm
systemctl enable kdm

%postun
systemctl disable kdm
userdel kdm

%clean
rm -rf %{buildroot}

%changelog
* Sun Dec 15 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.11.2 to 4.11.4
* Tue Oct 29 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.11.1 to 4.11.2
* Mon Sep 23 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.11.0 to 4.11.1
* Mon Aug 19 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.10.5 to 4.11.0
* Mon Aug 12 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.10.3 to 4.10.5
* Mon Jun 10 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.10.2 to 4.10.3
* Wed Apr 10 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.10.1 to 4.10.2
* Sat Mar 16 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.10.0 to 4.10.1
* Fri Mar 1 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.9.0 to 4.10.0
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
