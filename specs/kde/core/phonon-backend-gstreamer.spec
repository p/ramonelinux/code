Name:		phonon-backend-gstreamer
Version:	4.7.1
Release:	1%{?dist}
Summary:	Phonon-backend-gstreamer

Group:		User Interface/KDE
License:	GPLv2+
Url:		http://www.kde.org
Source:     ftp://ftp.kde.org/pub/kde/stable/phonon/%{name}/%{version}/src/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:	cmake phonon gstreamer0
BuildRequires:	gst-plugins-base0 gst-plugins-good0 gst-plugins-bad0 gst-plugins-ugly0
BuildRequires:	automoc

%description
This package provides a Phonon backend which utilizes the GStreamer media framework.

%prep
%setup -q -n %{name}-%{version}

%build
mkdir build &&
cd build &&
cmake -DCMAKE_INSTALL_PREFIX=/usr \
      -DCMAKE_BUILD_TYPE=Release  \
%ifarch x86_64
      -DLIB_SUFFIX=64 \
%endif
      .. &&
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
cd build
make install DESTDIR=%{buildroot}

%files
%defattr(-,root,root,-)
%{_libdir}/kde4/plugins/phonon_backend/phonon_gstreamer.so
%{_datadir}/icons/hicolor/*/apps/phonon-gstreamer.png
%{_datadir}/icons/hicolor/scalable/apps/phonon-gstreamer.svgz
%{_datadir}/kde4/services/phononbackends/gstreamer.desktop

%clean
rm -rf %{buildroot}

%changelog
* Sun Dec 15 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.6.3 to 4.7.1
* Wed Feb 27 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.6.1 to 4.6.3
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
