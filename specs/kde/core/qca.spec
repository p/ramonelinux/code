Name:       qca
Version:    2.0.3
Release:    6%{?dist}
Summary:    Qca

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.affinix.com
Source:     http://delta.affinix.com/download/qca/2.0/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  qt which

%description
Qca aims to provide a straightforward and cross-platform crypto API, using Qt datatypes and conventions.
Qca separates the API from the implementation, using plugins known as Providers.

%prep
%setup -q -n %{name}-%{version}

%build
sed -i '217s@set@this->set@' src/botantools/botan/botan/secmem.h &&
./configure --prefix=/usr \
            --certstore-path=/etc/ssl/ca-bundle.crt \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make install INSTALL_ROOT=%{buildroot}

%files
%defattr(-,root,root,-)
%{_bindir}/qcatool2
%{_bindir}/qcatool2.debug
%{_includedir}/QtCrypto/QtCrypto
%{_includedir}/QtCrypto/qca*.h
%{_includedir}/QtCrypto/qpipe.h
%{_libdir}/libqca.prl
%{_libdir}/libqca.so*
%{_libdir}/pkgconfig/qca2.pc
%{_datadir}/qt4/mkspecs/features/crypto.prf
%{_mandir}/man1/qcatool2.1.gz

%clean
rm -rf %{buildroot}

%changelog
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
