Name:		kde-baseapps
Version:	4.11.4
Release:	1%{?dist}
Summary:	Kde-baseapps

Group:		User Interface/KDE
License:	GPLv2+
Url:		http://www.kde.org
Source:     ftp://ftp.kde.org/pub/kde/stable/%{version}/src/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:	cmake kdelibs
BuildRequires:  kactivities nepomuk-widgets
BuildRequires:  automoc shared-desktop-ontologies
BuildRequires:	docbook-xml docbook-xsl phonon

%description
This package provides various applications, such as Dolphin (file manager) and Konqueror (web browser).
Infrastructure files and libraries are also provided.

%prep
%setup -q -n %{name}-%{version}

%build
mkdir build &&
cd build &&
cmake -DCMAKE_INSTALL_PREFIX=/usr \
      -DCMAKE_BUILD_TYPE=Release \
%ifarch x86_64
      -DLIB_SUFFIX=64 \
%endif
      .. &&
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
cd build
make install DESTDIR=%{buildroot}

%files
%defattr(-,root,root,-)
%doc
%{_bindir}/dolphin
%{_bindir}/fsview
%{_bindir}/kbookmarkmerger
%{_bindir}/kdepasswd
%{_bindir}/kdialog
%{_bindir}/keditbookmarks
%{_bindir}/kfind
%{_bindir}/kfmclient
%{_bindir}/konqueror
%{_bindir}/servicemenudeinstallation
%{_bindir}/servicemenuinstallation
%{_includedir}/*.h
%{_libdir}/kde4/*.so
%{_libdir}/libdolphinprivate.so*
%{_libdir}/libkbookmarkmodel_private.so*
%{_libdir}/libkdeinit4_*.so
%{_libdir}/libkonq.so*
%{_libdir}/libkonqsidebarplugin.so*
%{_libdir}/libkonquerorprivate.so.4*
%{_datadir}/applications/kde4/*.desktop
%{_datadir}/apps/*
%{_datadir}/autostart/konqy_preload.desktop
%{_datadir}/config.kcfg/*.kcfg
%{_datadir}/config/konqsidebartngrc
%{_datadir}/config/servicemenu.knsrc
%{_datadir}/config/translaterc
%{_datadir}/dbus-1/interfaces/org.kde.*.xml
%{_datadir}/icons/hicolor/*/apps/*.png
%{_datadir}/icons/hicolor/scalable/apps/konqueror.svgz
%{_datadir}/icons/oxygen/*/actions/*.png
%{_datadir}/icons/oxygen/scalable/actions/*.svgz
%{_datadir}/kde4/services/*.desktop
%{_datadir}/kde4/services/*.protocol
%{_datadir}/kde4/services/*/*.desktop
%{_datadir}/kde4/servicetypes/*.desktop
%{_datadir}/templates/.source/*
%{_datadir}/templates/*.desktop
%{_docdir}/HTML/en/*
%{_mandir}/man1/*.1.gz

%clean
rm -rf %{buildroot}

%changelog
* Sun Dec 15 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.11.2 to 4.11.4
* Tue Oct 29 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.11.1 to 4.11.2
* Mon Sep 23 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.11.0 to 4.11.1
* Mon Aug 19 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.10.5 to 4.11.0
* Sun Aug 18 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.10.3 to 4.10.5
* Mon Jun 10 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.10.2 to 4.10.3
* Wed Apr 10 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.10.1 to 4.10.2
* Sat Mar 16 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.10.0 to 4.10.1
* Fri Mar 1 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.9.0 to 4.10.0
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
