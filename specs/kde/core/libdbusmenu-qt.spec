Name:		libdbusmenu-qt
Version:	0.9.2
Release:	6%{?dist}
Summary:	libdbusmenu-qt

Group:		User Interface/KDE
License:	GPLv2+
Url:	    http://www.launchpad.net
Source:     http://launchpad.net/libdbusmenu-qt/trunk/0.9.2/+download/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:	cmake qt

%description
This library provides a Qt implementation of the DBusMenu specs which goal is to expose menus on DBus.

%prep
%setup -q -n %{name}-%{version}

%build
mkdir build &&
cd build &&
cmake -DCMAKE_INSTALL_PREFIX=/usr \
%ifarch x86_64
      -DLIB_SUFFIX=64 \
%endif
      -DWITH_DOC=OFF .. &&
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
cd build
make install DESTDIR=%{buildroot}

%files
%defattr(-,root,root,-)
%doc
%{_includedir}/dbusmenu-qt/dbusmenu*.h
%{_libdir}/libdbusmenu-qt.so*
%{_libdir}/pkgconfig/dbusmenu-qt.pc

%clean
rm -rf %{buildroot}

%changelog
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
