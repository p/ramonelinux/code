Name:		kdepimlibs
Version:	4.11.4
Release:	1%{?dist}
Summary:	Kdepimlibs

Group:		User Interface/KDE
License:	GPLv2+
Url:		http://www.kde.org
Source:     ftp://ftp.kde.org/pub/kde/stable/%{version}/src/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:	cmake nepomuk-core libxslt gpgme libical akonadi cyrus-sasl boost
BuildRequires:  openldap qjson
BuildRequires:  automoc shared-mime-info shared-desktop-ontologies docbook-xml docbook-xsl

%description
Kdepimlibs is the common library for KDE PIM applications like kmail, kalarm etc.

%prep
%setup -q -n %{name}-%{version}

%build
mkdir build &&
cd build &&
cmake -DCMAKE_INSTALL_PREFIX=/usr \
      -DCMAKE_BUILD_TYPE=Release \
%ifarch x86_64
      -DLIB_SUFFIX=64 \
%endif
      .. &&
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
cd build
make install DESTDIR=%{buildroot}

%files
%defattr(-,root,root,-)
%doc
%{_bindir}/akonadi_benchmarker
%{_bindir}/akonaditest
%{_includedir}/*
%{_libdir}/cmake/KdepimLibs/*.cmake
%{_libdir}/gpgmepp/Gpgmepp*.cmake
%{_libdir}/kde4/*.so
%{_libdir}/kde4/plugins/designer/akonadiwidgets.so
%{_libdir}/kde4/plugins/designer/kholidayswidgets.so
%{_libdir}/libakonadi-calendar.so*
%{_libdir}/libakonadi-contact.so*
%{_libdir}/libakonadi-kabc.so*
%{_libdir}/libakonadi-kcal.so*
%{_libdir}/libakonadi-kde.so*
%{_libdir}/libakonadi-kmime.so*
%{_libdir}/libakonadi-notes.so*
%{_libdir}/libakonadi-socialutils.so*
%{_libdir}/libgpgme++-pthread.so*
%{_libdir}/libgpgme++.so*
%{_libdir}/libkabc.so*
%{_libdir}/libkabc_file_core.so*
%{_libdir}/libkalarmcal.so*
%{_libdir}/libkblog.so*
%{_libdir}/libkcal.so*
%{_libdir}/libkcalcore.so*
%{_libdir}/libkcalutils.so*
%{_libdir}/libkholidays.so*
%{_libdir}/libkimap.so*
%{_libdir}/libkimaptest.a
%{_libdir}/libkldap.so*
%{_libdir}/libkmbox.so*
%{_libdir}/libkmime.so*
%{_libdir}/libkontactinterface.so*
%{_libdir}/libkpimidentities.so*
%{_libdir}/libkpimtextedit.so*
%{_libdir}/libkpimutils.so*
%{_libdir}/libkresources.so*
%{_libdir}/libktnef.so*
%{_libdir}/libkxmlrpcclient.so*
%{_libdir}/libmailtransport.so*
%{_libdir}/libmicroblog.so*
%{_libdir}/libqgpgme.so*
%{_libdir}/libsyndication.so*
%{_datadir}/apps/*
%{_datadir}/config.kcfg/*.kcfg
%{_datadir}/dbus-1/interfaces/org.kde.*.xml
%{_docdir}/HTML/en/kcontrol/*
%{_docdir}/HTML/en/kioslave/*
%{_datadir}/kde4/*
%{_datadir}/mime/packages/kdepimlibs-mime.xml
%{_datadir}/mime/packages/x-vnd.akonadi.socialfeeditem.xml

%clean
rm -rf %{buildroot}

%changelog
* Sun Dec 15 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.11.2 to 4.11.4
* Tue Oct 29 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.11.1 to 4.11.2
* Mon Sep 23 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.11.0 to 4.11.1
* Mon Aug 19 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.10.5 to 4.11.0
* Tue Aug 6 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.10.3 to 4.10.5
* Mon Jun 10 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.10.2 to 4.10.3
* Wed Apr 10 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.10.1 to 4.10.2
* Sat Mar 16 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.10.0 to 4.10.1
* Fri Mar 1 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.9.0 to 4.10.0
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
