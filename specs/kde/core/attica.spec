Name:		attica
Version:	0.4.2
Release:	1%{?dist}
Summary:	Attica

Group:		User Interface/KDE
License:	GPLv2+
Url:		http://www.kde.org
Source:     ftp://ftp.kde.org/pub/kde/stable/%{name}/src/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:	cmake qt

%description
Attica is a library to access "Open Collaboration Service" providers.

%prep
%setup -q -n %{name}-%{version}

%build
mkdir build &&
cd build &&
cmake -DCMAKE_INSTALL_PREFIX=/usr \
      -DCMAKE_BUILD_TYPE=Release \
%ifarch x86_64
      -DLIB_SUFFIX=64 \
%endif
      .. &&
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
cd build
make install DESTDIR=%{buildroot}

%files
%defattr(-,root,root,-)
%doc
%{_includedir}/attica/*.h
%{_libdir}/libattica.so*
%{_libdir}/pkgconfig/libattica.pc

%clean
rm -rf %{buildroot}

%changelog
* Wed Aug 14 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.4.1 to 0.4.2
* Wed Feb 27 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.4.0 to 0.4.1
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
