Name:		oxygen-icons
Version:	4.11.4
Release:	1%{?dist}
Summary:	Oxygen-icons

Group:		User Interface/KDE
License:	GPLv2+
Url:		http://www.kde.org
Source:     ftp://ftp.kde.org/pub/kde/stable/%{version}/src/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildArch:      noarch
BuildRequires:	cmake

%description
The Oxygen theme is a photo-realistic icon style, with a high standard of graphics quality.

%prep
%setup -q -n %{name}-%{version}

%build
mkdir build &&
cd build &&

cmake -DCMAKE_INSTALL_PREFIX=/usr ..

%install
rm -rf %{buildroot}
cd build
make install DESTDIR=%{buildroot}

%files
%defattr(-,root,root,-)
%doc
%{_datadir}/icons/oxygen/*/actions/*.png
%{_datadir}/icons/oxygen/*/animations/*.png
%{_datadir}/icons/oxygen/*/apps/*.png
%{_datadir}/icons/oxygen/*/categories/*.png
%{_datadir}/icons/oxygen/*/devices/*.png
%{_datadir}/icons/oxygen/*/emblems/*.png
%{_datadir}/icons/oxygen/*/emotes/*.png
%{_datadir}/icons/oxygen/*/mimetypes/*.png
%{_datadir}/icons/oxygen/*/places/*.png
%{_datadir}/icons/oxygen/*/special/*.png
%{_datadir}/icons/oxygen/*/status/*.png
%{_datadir}/icons/oxygen/index.theme

%clean
rm -rf %{buildroot}

%changelog
* Sun Dec 15 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.11.2 to 4.11.4
* Tue Oct 29 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.11.1 to 4.11.2
* Mon Sep 23 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.11.0 to 4.11.1
* Mon Aug 19 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.10.5 to 4.11.0
* Sun Aug 18 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.10.3 to 4.10.5
* Fri May 10 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.10.2 to 4.10.3
* Wed Apr 10 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.10.1 to 4.10.2
* Sat Mar 16 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.10.0 to 4.10.1
* Wed Feb 27 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.9.0 to 4.10.0
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
