Name:		automoc
Version:	0.9.88
Release:	6%{?dist}
Summary:	Automoc4

Group:		User Interface/KDE
License:	GPLv2+
Url:		http://www.kde.org
Source:     ftp://ftp.kde.org/pub/kde/stable/%{name}4/0.9.88/%{name}4-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:	cmake qt

%description
Automoc4 is a tool to add rules for generating Qt moc files automatically to projects that use CMake as the buildsystem.

%prep
%setup -q -n %{name}4-%{version}

%build
mkdir build &&
cd build &&
cmake -DCMAKE_INSTALL_PREFIX=/usr \
%ifarch x86_64
      -DLIB_SUFFIX=64 \
%endif
      .. &&
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
cd build
make install DESTDIR=%{buildroot}

%files
%defattr(-,root,root,-)
%doc
%{_bindir}/automoc4
%{_libdir}/automoc4/Automoc4Config.cmake
%{_libdir}/automoc4/Automoc4Version.cmake
%{_libdir}/automoc4/automoc4.files.in

%clean
rm -rf %{buildroot}

%changelog
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
