Name:		polkit-kde-agent
Version:	0.99.0
Release:	6%{?dist}
Summary:	Polkit-kde-agent

Group:		User Interface/KDE
License:	GPLv2+
Url:		http://www.kde.org
Source:     ftp://ftp.kde.org/pub/kde/stable/apps/KDE4.x/admin/%{name}-1-%{version}.tar.bz2
Patch:      polkit-kde-agent-1-0.99.0-remember_password-1.patch

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:	cmake polkit-qt kdelibs automoc gettext

%description
Polkit-kde-agent provides a graphical authentication prompt so non-priviledged users can authenticate themselves for performing administrative tasks in KDE.

%prep
%setup -q -n %{name}-1-%{version}
%patch -p1

%build
mkdir build &&
cd build &&
cmake -DCMAKE_INSTALL_PREFIX=/usr \
%ifarch x86_64
      -DLIB_SUFFIX=64 \
%endif
      .. &&
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
cd build
make install DESTDIR=%{buildroot}

%files
%defattr(-,root,root,-)
%{_libdir}/kde4/libexec/polkit-kde-authentication-agent-1
%{_datadir}/apps/policykit1-kde/policykit1-kde.notifyrc
%{_datadir}/autostart/polkit-kde-authentication-agent-1.desktop
%{_datadir}/locale/*/LC_MESSAGES/polkit-kde-authentication-agent-1.mo

%clean
rm -rf %{buildroot}

%changelog
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
