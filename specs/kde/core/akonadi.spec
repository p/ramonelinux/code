Name:		akonadi
Version:	1.11.0
Release:	1%{?dist}
Summary:	Akonadi

Group:		User Interface/KDE
License:	GPLv2+
Url:		http://www.kde.org
Source:     ftp://ftp.kde.org/pub/kde/stable/%{name}/src/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:	cmake automoc
BuildRequires:  shared-mime-info boost soprano sqlite
BuildRequires:	libxslt

%description
Akonadi is an extensible cross-desktop storage service for PIM data and meta data providing concurrent read, write, and query access.
It will provide unique desktop wide object identification and retrieval.

%prep
%setup -q -n %{name}-%{version}

%build
mkdir build &&
cd build &&

cmake -DCMAKE_INSTALL_PREFIX=/usr \
      -DCMAKE_PREFIX_PATH=/usr \
      -DCMAKE_BUILD_TYPE=Release \
      -DINSTALL_QSQLITE_IN_QT_PREFIX=TRUE \
%ifarch x86_64
      -DLIB_SUFFIX=64 \
%endif
      .. &&
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
cd build
make install DESTDIR=%{buildroot}

%files
%defattr(-,root,root,-)
%{_bindir}/akonadi_agent_launcher
%{_bindir}/akonadi_agent_server
%{_bindir}/akonadi_control
%{_bindir}/akonadi_rds
%{_bindir}/akonadictl
%{_bindir}/akonadiserver
%{_bindir}/asapcat
%{_includedir}/akonadi/private/*.h
%{_libdir}/cmake/Akonadi/Akonadi*.cmake
%{_libdir}/libakonadiprotocolinternals.so*
%{_libdir}/pkgconfig/akonadi.pc
%{_libdir}/qt4/plugins/sqldrivers/libqsqlite3.so
%{_datadir}/config/akonadi/mysql-global-mobile.conf
%{_datadir}/config/akonadi/mysql-global.conf
%{_datadir}/dbus-1/interfaces/org.freedesktop.Akonadi.*.xml
%{_datadir}/dbus-1/services/org.freedesktop.Akonadi.Control.service
%{_datadir}/mime/packages/akonadi-mime.xml

%clean
rm -rf %{buildroot}

%changelog
* Sun Dec 15 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.10.2 to 1.11.0
* Tue Aug 6 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.9.2 to 1.10.2
* Fri May 10 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.9.1 to 1.9.2
* Mon Mar 18 2013 tanggeliang <tanggeliang@gmail.com>
- add '#ifndef Q_MOC_RUN' to fix 'Parse error at "BOOST_JOIN"'
* Sat Mar 16 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.9.0 to 1.9.1
* Wed Feb 27 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.8.0 to 1.9.0
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
