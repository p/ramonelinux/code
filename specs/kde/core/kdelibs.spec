Name:		kdelibs
Version:	4.11.4
Release:	1%{?dist}
Summary:	Kdelibs

Group:		User Interface/KDE
License:	GPLv2+
Url:		http://www.kde.org
Source:     ftp://ftp.kde.org/pub/kde/stable/%{version}/src/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:	cmake phonon attica soprano strigi qca libdbusmenu-qt docbook-xml docbook-xsl shared-desktop-ontologies shared-mime-info
BuildRequires:  polkit-qt libpng libjpeg-turbo giflib upower udisks
BuildRequires:  pcre openssl acl aspell enchant
BuildRequires:  automoc libxslt

%description
This package includes programs and libraries that are central to development and execution of KDE programs.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
sed -i "s@{SYSCONF_INSTALL_DIR}/xdg/menus@& RENAME kde-applications.menu@" \
        kded/CMakeLists.txt &&
sed -i "s@applications.menu@kde-&@" \
        kded/kbuildsycoca.cpp
mkdir build &&
cd build &&
cmake -DCMAKE_INSTALL_PREFIX=/usr \
      -DSYSCONF_INSTALL_DIR=/etc \
      -DCMAKE_BUILD_TYPE=Release \
      -DDOCBOOKXML_CURRENTDTD_DIR=/usr/share/xml/docbook/xml-dtd-4.5 \
%ifarch x86_64
      -DLIB_SUFFIX=64 \
%endif
      .. &&
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
cd build
make install DESTDIR=%{buildroot}

%files
%defattr(-,root,root,-)
/etc/dbus-1/system.d/org.kde.auth.conf
/etc/xdg/menus/kde-applications.menu
%{_bindir}/checkXML
%{_bindir}/kbuildsycoca4
%{_bindir}/kconfig_compiler
%{_bindir}/kcookiejar4
%{_bindir}/kde4-config
%{_bindir}/kded4
%{_bindir}/kdeinit4
%{_bindir}/kdeinit4_shutdown
%{_bindir}/kdeinit4_wrapper
%{_bindir}/kfilemetadatareader
%{_bindir}/kjs
%{_bindir}/kjscmd
%{_bindir}/kmailservice
%{_bindir}/kross
%{_bindir}/kshell4
%{_bindir}/ktelnetservice
%{_bindir}/kunittestmodrunner
%{_bindir}/kwrapper4
%{_bindir}/makekdewidgets
%{_bindir}/meinproc4
%{_bindir}/meinproc4_simple
%{_bindir}/nepomuk-rcgen
%{_bindir}/preparetips
%{_includedir}/*
%{_libdir}/cmake/KDeclarative/KDeclarative*.cmake
%{_libdir}/kde4/*
%{_libdir}/libkcmutils.so*
%{_libdir}/libkde3support.so*
%{_libdir}/libkdeclarative.so*
%{_libdir}/libkdecore.so*
%{_libdir}/libkdefakes.so*
%{_libdir}/libkdeinit4_*.so
%{_libdir}/libkdesu.so*
%{_libdir}/libkdeui.so*
%{_libdir}/libkdewebkit.so*
%{_libdir}/libkdnssd.so*
%{_libdir}/libkemoticons.so*
%{_libdir}/libkfile.so*
%{_libdir}/libkhtml.so*
%{_libdir}/libkidletime.so*
%{_libdir}/libkimproxy.so*
%{_libdir}/libkio.so*
%{_libdir}/libkjs.so*
%{_libdir}/libkjsapi.so*
%{_libdir}/libkjsembed.so*
%{_libdir}/libkmediaplayer.so*
%{_libdir}/libknewstuff2.so*
%{_libdir}/libknewstuff3.so*
%{_libdir}/libknotifyconfig.so*
%{_libdir}/libkntlm.so*
%{_libdir}/libkparts.so*
%{_libdir}/libkprintutils.so*
%{_libdir}/libkpty.so*
%{_libdir}/libkrosscore.so*
%{_libdir}/libkrossui.so*
%{_libdir}/libktexteditor.so*
%{_libdir}/libkunitconversion.so*
%{_libdir}/libkunittest.so*
%{_libdir}/libkutils.so*
%{_libdir}/libnepomuk.so*
%{_libdir}/libnepomukquery.so*
%{_libdir}/libnepomukutils.so*
%{_libdir}/libplasma.so*
%{_libdir}/libsolid.so*
%{_libdir}/libthreadweaver.so*
%{_datadir}/applications/kde4/*.desktop
%{_datadir}/apps/*
%{_datadir}/config/*
%{_datadir}/dbus-1/interfaces/org.freedesktop.PowerManagement*.xml
%{_datadir}/dbus-1/interfaces/org.kde.*.xml
%{_datadir}/icons/hicolor/*/actions/presence_*.png
%{_datadir}/kde4/services/*
%{_datadir}/kde4/servicetypes/*
%{_datadir}/locale/all_languages
%{_datadir}/locale/en_US/entry.desktop
%{_datadir}/mime/packages/kde.xml

%files doc
%defattr(-,root,root,-)
%doc
%{_docdir}/HTML/en/*
%{_mandir}/man*/*.gz

%clean
rm -rf %{buildroot}

%changelog
* Sun Dec 15 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.11.2 to 4.11.4
* Tue Oct 29 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.11.1 to 4.11.2
* Mon Sep 23 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.11.0 to 4.11.1
* Mon Aug 19 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.10.5 to 4.11.0
* Wed Aug 14 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.10.3 to 4.10.5
* Mon Jun 10 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.10.2 to 4.10.3
* Wed Apr 10 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.10.1 to 4.10.2
* Sat Mar 16 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.10.0 to 4.10.1
* Wed Feb 27 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.9.0 to 4.10.0
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
