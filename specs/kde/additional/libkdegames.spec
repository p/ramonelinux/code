Name:		libkdegames
Version:	4.11.2
Release:	1%{?dist}
Summary:	The library for the kdegames package

Group:		User Interface/KDE
License:	GPLv2+
Url:		http://www.kde.org
Source:     ftp://ftp.kde.org/pub/kde/stable/%{version}/src/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:	cmake kdelibs
BuildRequires:  automoc docbook-xml docbook-xsl

%description
It is a collection of functions used by some games or which are useful for other games.

%prep
%setup -q -n %{name}-%{version}

%build
mkdir build &&
cd build &&
cmake -DCMAKE_INSTALL_PREFIX=/usr \
      -DCMAKE_BUILD_TYPE=Release \
%ifarch x86_64
      -DLIB_SUFFIX=64 \
%endif
      .. &&
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
cd build
make install DESTDIR=%{buildroot}

%files
%defattr(-,root,root,-)
%{_includedir}/KDE/*
%{_includedir}/highscore/*.h
%{_includedir}/*.h
%{_includedir}/libkdegamesprivate/*
%{_libdir}/cmake/KDEGames/KDEGames*.cmake
%{_libdir}/kde4/imports/org/kde/games/core/KgItem.qml
%{_libdir}/kde4/imports/org/kde/games/core/libcorebindingsplugin.so
%{_libdir}/kde4/imports/org/kde/games/core/qmldir
%{_libdir}/libkdegames.so*
%{_libdir}/libkdegamesprivate.so*
%{_datadir}/apps/carddecks/svg-ancient-egyptians/*
%{_datadir}/apps/carddecks/svg-dondorf/*
%{_datadir}/apps/carddecks/svg-future/*
%{_datadir}/apps/carddecks/svg-gm-paris/*
%{_datadir}/apps/carddecks/svg-jolly-royal/*
%{_datadir}/apps/carddecks/svg-konqi-modern/*
%{_datadir}/apps/carddecks/svg-nicu-ornamental/*
%{_datadir}/apps/carddecks/svg-nicu-white/*
%{_datadir}/apps/carddecks/svg-oxygen-air/*
%{_datadir}/apps/carddecks/svg-oxygen-white/*
%{_datadir}/apps/carddecks/svg-oxygen/*
%{_datadir}/apps/carddecks/svg-penguins/*
%{_datadir}/apps/carddecks/svg-standard/*
%{_datadir}/apps/carddecks/svg-tigullio-international/*
%{_datadir}/apps/carddecks/svg-xskat-french/*
%{_datadir}/apps/carddecks/svg-xskat-german/*
%{_datadir}/apps/kconf_update/kgthemeprovider-migration.upd

%clean
rm -rf %{buildroot}

%changelog
* Tue Oct 15 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.11.0 to 4.11.2
* Mon Aug 19 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.10.3 to 4.11.0
* Mon Jun 10 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.10.2 to 4.10.3
* Wed Apr 10 2013 tanggeliang <tanggeliang@gmail.com>
- create
