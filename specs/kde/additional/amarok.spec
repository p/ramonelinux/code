Name:		amarok
Version:	2.8.0
Release:	1%{?dist}
Summary:    Amarok

Group:		User Interface/KDE
License:	GPLv2+
Url:		http://www.kde.org
Source:     ftp://ftp.kde.org/pub/kde/stable/%{name}/%{version}/src/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:	cmake kdelibs mysql taglib
BuildRequires:  ffmpeg nepomuk-core
BuildRequires:  curl libxml openssl qjson
BuildRequires:  automoc docbook-xml docbook-xsl
BuildRequires:  gettext

%description
This package provides an archiving utility for KDE.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
mkdir build &&
cd build &&
cmake -DCMAKE_INSTALL_PREFIX=/usr        \
      -DCMAKE_BUILD_TYPE=Release         \
      -DKDE4_BUILD_TESTS=OFF             \
%ifarch x86_64
      -DLIB_SUFFIX=64                    \
%endif
      .. &&
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
cd build
make install DESTDIR=%{buildroot}

%files
%defattr(-,root,root,-)
%{_bindir}/amarok
%{_bindir}/amarok_afttagger
%{_bindir}/amarokcollectionscanner
%{_bindir}/amarokpkg
%{_bindir}/amzdownloader
%{_libdir}/kde4/*amarok_*.so
%{_libdir}/libamarok*.so*
%{_libdir}/libampache_account_login.so
%{_datadir}/applications/kde4/*.desktop
%{_datadir}/apps/amarok/amarok.notifyrc
%{_datadir}/apps/amarok/data/*
%{_datadir}/apps/amarok/icons/hicolor/128x128/status/audio-volume-*.png
%{_datadir}/apps/amarok/icons/hicolor/*x*/actions/*.png
%{_datadir}/apps/amarok/images/*.png
%{_datadir}/apps/amarok/images/*.svg
%{_datadir}/apps/amarok/images/*.svgz
%{_datadir}/apps/desktoptheme/default/widgets/amarok-*.svg
%{_datadir}/apps/kconf_update/amarok-2.4.1-tokens_syntax_update.pl
%{_datadir}/apps/kconf_update/amarok.upd
%{_datadir}/apps/solid/actions/amarok-play-audiocd.desktop
%{_datadir}/config.kcfg/amarokconfig.kcfg
%{_datadir}/config/amarok*
%{_datadir}/dbus-1/interfaces/org.*.xml
%{_datadir}/icons/hicolor/*x*/apps/amarok.png
%{_datadir}/kde4/services/ServiceMenus/amarok_append.desktop
%{_datadir}/kde4/services/amarok*.desktop
%{_datadir}/kde4/services/amarok*.protocol
%{_datadir}/kde4/servicetypes/amarok_*.desktop
%{_datadir}/locale/*/LC_MESSAGES/amarok*.mo

%files doc
%defattr(-,root,root,-)
%{_docdir}/HTML/*/amarok/*

%clean
rm -rf %{buildroot}

%changelog
* Mon Aug 19 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.7.1 to 2.8.0
* Tue Jun 18 2013 tanggeliang <tanggeliang@gmail.com>
- create
