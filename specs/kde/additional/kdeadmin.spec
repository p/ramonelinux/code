Name:		kdeadmin
Version:	4.11.0
Release:	1%{?dist}
Summary:	Kdeadmin

Group:		User Interface/KDE
License:	GPLv2+
Url:		http://www.kde.org
Source:     ftp://ftp.kde.org/pub/kde/stable/%{version}/src/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:	cmake kdelibs kdepimlibs
BuildRequires:  automoc docbook-xml docbook-xsl

%description
This package provides several administration tools for KDE.
These include a tool for managing users, a cron command schedule editor, a printer administration tool and a log file viewer.

%prep
%setup -q -n %{name}-%{version}

%build
mkdir build &&
cd build &&
cmake -DCMAKE_INSTALL_PREFIX=/usr \
      -DCMAKE_BUILD_TYPE=Release \
%ifarch x86_64
      -DLIB_SUFFIX=64 \
%endif
      .. &&
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
cd build
make install DESTDIR=%{buildroot}

%files
%defattr(-,root,root,-)
%{_bindir}/ksystemlog
%{_bindir}/kuser
%{_libdir}/kde4/kcm_cron.so
%{_datadir}/applications/kde4/*.desktop
%{_datadir}/apps/ksystemlog/ksystemlogui.rc
%{_datadir}/apps/kuser/icons/hicolor/22x22/actions/user-group-*.png
%{_datadir}/apps/kuser/kuserui.rc
%{_datadir}/apps/kuser/pics/*.png
%{_datadir}/config.kcfg/kuser.kcfg
%{_datadir}/icons/hicolor/*x*/apps/kuser.png
%{_datadir}/kde4/services/kcm_cron.desktop
%{_docdir}/HTML/en/*

%clean
rm -rf %{buildroot}

%changelog
* Mon Aug 19 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.10.5 to 4.11.0
* Sun Aug 18 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.10.3 to 4.10.5
* Mon Jun 10 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.10.2 to 4.10.3
* Wed Apr 10 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.10.1 to 4.10.2
* Sat Mar 16 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.10.0 to 4.10.1
* Sat Mar 2 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.9.0 to 4.10.0
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
