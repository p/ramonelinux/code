Name:		konsole
Version:	4.11.4
Release:	1%{?dist}
Summary:	Konsole

Group:		User Interface/KDE
License:	GPLv2+
Url:		http://www.kde.org
Source:     ftp://ftp.kde.org/pub/kde/stable/%{version}/src/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:	cmake kdelibs kde-baseapps
BuildRequires:	automoc docbook-xml docbook-xsl

%description
This package provides a terminal emulator for KDE.

%prep
%setup -q -n %{name}-%{version}

%build
mkdir build &&
cd build &&
cmake -DCMAKE_INSTALL_PREFIX=/usr \
      -DCMAKE_BUILD_TYPE=Release \
%ifarch x86_64
      -DLIB_SUFFIX=64 \
%endif
      .. &&
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
cd build
make install DESTDIR=%{buildroot}

%files
%defattr(-,root,root,-)
%doc
%{_bindir}/konsole
%{_bindir}/konsoleprofile
%{_libdir}/kde4/libkonsolepart.so
%{_libdir}/libkdeinit4_konsole.so
%{_libdir}/libkonsoleprivate.so
%{_datadir}/applications/kde4/konsole.desktop
%{_datadir}/apps/kconf_update/konsole_remove_background_mode.upd
%{_datadir}/apps/kconf_update/konsole-2.9.0-*.sh
%{_datadir}/apps/kconf_update/konsole-2.10.0-remove-menuindex-from-profiles.sh
%{_datadir}/apps/kconf_update/konsole.upd
%{_datadir}/apps/konsole/*
%{_datadir}/kde4/services/ServiceMenus/konsolehere.desktop
%{_datadir}/kde4/services/konsolepart.desktop
%{_datadir}/kde4/servicetypes/terminalemulator.desktop
%{_docdir}/HTML/en/konsole/*

%clean
rm -rf %{buildroot}

%changelog
* Sun Dec 15 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.11.2 to 4.11.4
* Tue Oct 29 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.11.1 to 4.11.2
* Sat Sep 28 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.11.0 to 4.11.1
* Mon Aug 19 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.10.5 to 4.11.0
* Sun Aug 18 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.10.3 to 4.10.5
* Mon Jun 10 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.10.2 to 4.10.3
* Wed Apr 10 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.10.1 to 4.10.2
* Sat Mar 16 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.10.0 to 4.10.1
* Sat Mar 2 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.9.0 to 4.10.0
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
