Name:		kate
Version:	4.11.4
Release:	1%{?dist}
Summary:	Kate

Group:		User Interface/KDE
License:	GPLv2+
Url:		http://www.kde.org
Source:     ftp://ftp.kde.org/pub/kde/stable/%{version}/src/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:	cmake kdelibs kactivities
BuildRequires:	automoc docbook-xml docbook-xsl

%description
This package provides two texteditors: Kate and KWrite.
Kate is a powerful programmer's text editor with syntax highlighting for many programming and scripting languages.
KWrite is the lightweight cousin of Kate.

%prep
%setup -q -n %{name}-%{version}

%build
mkdir build &&
cd build &&
cmake -DCMAKE_INSTALL_PREFIX=/usr \
      -DCMAKE_BUILD_TYPE=Release \
      -DINSTALL_PYTHON_FILES_IN_PYTHON_PREFIX=TRUE \
%ifarch x86_64
      -DLIB_SUFFIX=64 \
%endif
      .. &&
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
cd build
make install DESTDIR=%{buildroot}

%files
%defattr(-,root,root,-)
%doc
%{_bindir}/kate
%{_bindir}/kwrite
%{_includedir}/kate/*.h
%{_includedir}/*.h
%{_libdir}/kde4/kate*.so
%{_libdir}/kde4/ktexteditor_*.so
%{_libdir}/kde4/plasma_applet_katesession.so
%{_libdir}/libkateinterfaces.so*
%{_libdir}/libkatepartinterfaces.so*
%{_libdir}/libkdeinit4_*.so
%{_datadir}/applications/kde4/kate.desktop
%{_datadir}/applications/kde4/kwrite.desktop
%{_datadir}/apps/*
%{_datadir}/config/*
%{_datadir}/icons/hicolor/*/apps/kate.png
%{_datadir}/icons/hicolor/22x22/actions/debug-kategdb.png
%{_datadir}/icons/hicolor/scalable/apps/*.svgz
%{_datadir}/kde4/services/kate*.desktop
%{_datadir}/kde4/services/ktexteditor_*.desktop
%{_datadir}/kde4/services/plasma-applet-katesession.desktop
%{_datadir}/kde4/servicetypes/kateplugin.desktop
%{_docdir}/HTML/en/kate/*
%{_docdir}/HTML/en/kwrite/*
%{_mandir}/man1/kate.1.gz

%clean
rm -rf %{buildroot}

%changelog
* Sun Dec 15 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.11.2 to 4.11.4
* Tue Oct 29 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.11.1 to 4.11.2
* Sat Sep 28 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.11.0 to 4.11.1
* Mon Aug 19 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.10.5 to 4.11.0
* Sun Aug 18 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.10.3 to 4.10.5
* Mon Jun 10 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.10.2 to 4.10.3
* Wed Apr 10 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.10.1 to 4.10.2
* Sat Mar 16 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.10.0 to 4.10.1
* Sat Mar 2 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.9.0 to 4.10.0
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
