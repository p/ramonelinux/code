Name:		kmix
Version:	4.11.4
Release:	1%{?dist}
Summary:	Kmix

Group:		User Interface/KDE
License:	GPLv2+
Url:		http://www.kde.org
Source:     ftp://ftp.kde.org/pub/kde/stable/%{version}/src/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:	cmake kdelibs
BuildRequires:  pulseaudio
BuildRequires:  automoc docbook-xml docbook-xsl

%description
This packages provides an audio mixer application for KDE.

%prep
%setup -q -n %{name}-%{version}

%build
mkdir build &&
cd build &&
cmake -DCMAKE_INSTALL_PREFIX=/usr \
      -DCMAKE_BUILD_TYPE=Release \
%ifarch x86_64
      -DLIB_SUFFIX=64 \
%endif
      .. &&
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
cd build
make install DESTDIR=%{buildroot}

%files
%defattr(-,root,root,-)
%{_bindir}/kmix
%{_bindir}/kmixctrl
%{_libdir}/kde4/kded_kmixd.so
%{_libdir}/kde4/plasma_engine_mixer.so
%{_libdir}/libkdeinit4_kmix*.so
%{_datadir}/applications/kde4/kmix.desktop
%{_datadir}/apps/kmix/kmixui.rc
%{_datadir}/apps/kmix/pics/kmixdocked*.png
%{_datadir}/apps/kmix/pics/mixer-*.png
%{_datadir}/apps/kmix/profiles/ALSA.*.xml
%{_datadir}/apps/kmix/profiles/OSS.default.xml
%{_datadir}/apps/plasma/services/mixer.operations
%{_datadir}/autostart/kmix_autostart.desktop
%{_datadir}/autostart/restore_kmix_volumes.desktop
%{_datadir}/dbus-1/interfaces/org.kde.kmix.control.xml
%{_datadir}/dbus-1/interfaces/org.kde.kmix.mixer.xml
%{_datadir}/dbus-1/interfaces/org.kde.kmix.mixset.xml
%{_datadir}/icons/hicolor/*x*/apps/kmix.png
%{_datadir}/kde4/services/kded/kmixd.desktop
%{_datadir}/kde4/services/*.desktop
%{_docdir}/HTML/en/kmix/*

%clean
rm -rf %{buildroot}

%changelog
* Sun Dec 15 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.11.2 to 4.11.4
* Tue Oct 29 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.11.1 to 4.11.2
* Sat Sep 28 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.11.0 to 4.11.1
* Mon Aug 19 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.10.5 to 4.11.0
* Sun Aug 18 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.10.3 to 4.10.5
* Mon Jun 10 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.10.2 to 4.10.3
* Wed Apr 10 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.10.1 to 4.10.2
* Sat Mar 16 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.10.0 to 4.10.1
* Wed Mar 13 2013 tanggeliang <tanggeliang@gmail.com>
- remove libcanberra BuildRequires
* Sat Mar 2 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.9.0 to 4.10.0
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
