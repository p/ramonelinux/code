Name:		okular
Version:	4.11.4
Release:	1%{?dist}
Summary:	Okular

Group:		User Interface/KDE
License:	GPLv2+
Url:		http://www.kde.org
Source:     ftp://ftp.kde.org/pub/kde/stable/%{version}/src/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:	cmake kdelibs
BuildRequires:  kactivities freetype qimageblitz libtiff libjpeg-turbo poppler
BuildRequires:  automoc docbook-xml docbook-xsl

%description
Okular is a document viewer for KDE. It can view documents of many types including PDF, PostScript, TIFF, Microsoft CHM, DjVu, DVI, XPS and ePub.

%prep
%setup -q -n %{name}-%{version}

%build
mkdir build &&
cd build &&
cmake -DCMAKE_INSTALL_PREFIX=/usr \
      -DCMAKE_BUILD_TYPE=Release \
%ifarch x86_64
      -DLIB_SUFFIX=64 \
%endif
      .. &&
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
cd build
make install DESTDIR=%{buildroot}

%files
%defattr(-,root,root,-)
%{_bindir}/okular
%{_includedir}/okular/core/*.h
%{_includedir}/okular/interfaces/*.h
%{_libdir}/cmake/Okular/OkularConfig*.cmake
%{_libdir}/kde4/imports/org/kde/okular/libokularplugin.so
%{_libdir}/kde4/imports/org/kde/okular/qmldir
%{_libdir}/kde4/okularGenerator_*.so
%{_libdir}/kde4/okularpart.so
%{_libdir}/libokularcore.so*
%{_datadir}/applications/kde4/active-documentviewer_*.desktop
%{_datadir}/applications/kde4/okular.desktop
%{_datadir}/applications/kde4/okularApplication_*.desktop
%{_datadir}/apps/kconf_update/okular.upd
%{_datadir}/apps/okular/part-viewermode.rc
%{_datadir}/apps/okular/part.rc
%{_datadir}/apps/okular/pics/*.png
%{_datadir}/apps/okular/pics/stamps.svg
%{_datadir}/apps/okular/shell.rc
%{_datadir}/apps/okular/tools.xml
%{_datadir}/config.kcfg/okular*.kcfg
%{_datadir}/icons/hicolor/*x*/apps/okular.png
%{_datadir}/icons/hicolor/scalable/apps/okular.svgz
%{_datadir}/kde4/services/libokularGenerator_*.desktop
%{_datadir}/kde4/services/okular*.desktop
%{_datadir}/kde4/servicetypes/okularGenerator.desktop
%{_docdir}/HTML/en/okular/*
%{_mandir}/man1/okular.1.gz

%clean
rm -rf %{buildroot}

%changelog
* Sun Dec 15 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.11.2 to 4.11.4
* Tue Oct 29 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.11.1 to 4.11.2
* Sat Sep 28 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.11.0 to 4.11.1
* Mon Aug 19 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.10.5 to 4.11.0
* Sun Aug 18 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.10.3 to 4.10.5
* Mon Jun 10 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.10.2 to 4.10.3
* Wed Apr 10 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.10.1 to 4.10.2
* Sat Mar 16 2013 tanggeliang <tanggeliang@gmail.com>
- uodate from 4.9.0 to 4.10.1
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
