Name:		kdepim
Version:	4.11.4
Release:	1%{?dist}
Summary:	Kdepim

Group:		User Interface/KDE
License:	GPLv2+
Url:		http://www.kde.org
Source:     ftp://ftp.kde.org/pub/kde/stable/%{version}/src/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:	cmake kdepim-runtime
BuildRequires:  nepomuk-widgets boost libassuan
BuildRequires:  shared-desktop-ontologies grantlee
BuildRequires:  automoc docbook-xml docbook-xsl

%description
This package provides several KDE programs for managing personal information.
Programs include a contact manager, calendar, mail client, newsreader, X.509 certificate manager and sticky notes.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
mkdir build &&
cd build &&
cmake -DCMAKE_INSTALL_PREFIX=/usr \
      -DSYSCONF_INSTALL_DIR=/etc \
      -DCMAKE_BUILD_TYPE=Release \
%ifarch x86_64
      -DLIB_SUFFIX=64 \
%endif
      .. &&
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
cd build
make install DESTDIR=%{buildroot}

%files
%defattr(-,root,root,-)
/etc/dbus-1/system.d/org.kde.kalarmrtcwake.conf
%{_bindir}/akonadi_*_agent
%{_bindir}/akonadiconsole
%{_bindir}/akregator
%{_bindir}/akregatorstorageexporter
%{_bindir}/blogilo
%{_bindir}/headerthemeeditor
%{_bindir}/ical2vcal
%{_bindir}/importwizard
%{_bindir}/kabc2mutt
%{_bindir}/kabcclient
%{_bindir}/kaddressbook*
%{_bindir}/kalarm*
%{_bindir}/karm
%{_bindir}/kgpgconf
%{_bindir}/kincidenceeditor
%{_bindir}/kjots
%{_bindir}/kleopatra
%{_bindir}/kmail*
%{_bindir}/knode
%{_bindir}/knotes
%{_bindir}/konsolekalendar
%{_bindir}/kontact
%{_bindir}/korgac
%{_bindir}/korganizer*
%{_bindir}/ksendemail
%{_bindir}/ktimetracker
%{_bindir}/ktnef
%{_bindir}/kwatchgnupg
%{_bindir}/pimsettingexporter
%{_libdir}/akonadi/contact/editorpageplugins/cryptopageplugin.so
%{_libdir}/kde4/akregator_*.so
%{_libdir}/kde4/akregatorpart.so
%{_libdir}/kde4/kaddressbookpart.so
%{_libdir}/kde4/kcal_*.so
%{_libdir}/kde4/kcm_*.so
%{_libdir}/kde4/kjotspart.so
%{_libdir}/kde4/kmailpart.so
%{_libdir}/kde4/knodepart.so
%{_libdir}/kde4/knotes_local.so
%{_libdir}/kde4/kontact_*.so
%{_libdir}/kde4/korg_*.so
%{_libdir}/kde4/korganizerpart.so
%{_libdir}/kde4/ktexteditorkabcbridge.so
%{_libdir}/kde4/ktimetrackerpart.so
%{_libdir}/kde4/libexec/kalarm_helper
%{_libdir}/kde4/messageviewer_bodypartformatter_*.so
%{_libdir}/kde4/plasma_applet_akonotes_*.so
%{_libdir}/kde4/plugins/accessible/messagevieweraccessiblewidgetfactory.so
%{_libdir}/kde4/plugins/designer/kdepimwidgets.so
%{_libdir}/kde4/plugins/designer/mailcommonwidgets.so
%{_libdir}/kde4/plugins/designer/pimcommonwidgets.so
%{_libdir}/kde4/plugins/grantlee/0.3/grantlee_messageheaderfilters.so
%{_libdir}/libakonadi_next.so*
%{_libdir}/libakregatorinterfaces.so*
%{_libdir}/libakregatorprivate.so.4*
%{_libdir}/libcalendarsupport.so*
%{_libdir}/libcomposereditorng.so*
%{_libdir}/libeventviews.so*
%{_libdir}/libgrammar.so*
%{_libdir}/libincidenceeditorsng.so*
%{_libdir}/libincidenceeditorsngmobile.so*
%{_libdir}/libkaddressbookprivate.so.4*
%{_libdir}/libkcal_resourceblog.so*
%{_libdir}/libkcal_resourceremote.so*
%{_libdir}/libkdepim.so*
%{_libdir}/libkdepimdbusinterfaces.so*
%{_libdir}/libkdgantt2.so*
%{_libdir}/libkleo.so*
%{_libdir}/libkleopatraclientcore.so*
%{_libdir}/libkleopatraclientgui.so*
%{_libdir}/libkmailprivate.so.4*
%{_libdir}/libkmanagesieve.so*
%{_libdir}/libknodecommon.so*
%{_libdir}/libkontactprivate.so.4*
%{_libdir}/libkorganizer_core.so*
%{_libdir}/libkorganizer_interfaces.so*
%{_libdir}/libkorganizerprivate.so.4*
%{_libdir}/libkpgp.so*
%{_libdir}/libksieve.so*
%{_libdir}/libksieveui.so*
%{_libdir}/libmailcommon.so*
%{_libdir}/libmailimporter.so*
%{_libdir}/libmessagecomposer.so*
%{_libdir}/libmessagecore.so*
%{_libdir}/libmessagelist.so*
%{_libdir}/libmessageviewer.so*
%{_libdir}/libpimcommon.so*
%{_libdir}/libsendlater.so*
%{_libdir}/libtemplateparser.so*
%{_datadir}/akonadi/agents/*.desktop
%{_datadir}/applications/kde4/*.desktop
%{_datadir}/apps/akonadi_archivemail_agent/*
%{_datadir}/apps/akonadi_mailfilter_agent/*
%{_datadir}/apps/akonadi_sendlater_agent/akonadi_sendlater_agent.notifyrc
%{_datadir}/apps/akonadiconsole/*
%{_datadir}/apps/akregator_sharemicroblog_plugin/*
%{_datadir}/apps/akregator/*
%{_datadir}/apps/blogilo/*
%{_datadir}/apps/composereditor/composereditorinitialhtml
%{_datadir}/apps/desktoptheme/default/widgets/stickynote.svgz
%{_datadir}/apps/headerthemeeditor/headerthemeeditorui.rc
%{_datadir}/apps/kaddressbook/*
%{_datadir}/apps/kalarm/*
%{_datadir}/apps/kconf_update/*
%{_datadir}/apps/kdepimwidgets/*
%{_datadir}/apps/kjots/*
%{_datadir}/apps/kleopatra/*
%{_datadir}/apps/kmail2/*
%{_datadir}/apps/kmailcvt/*
%{_datadir}/apps/knode/*
%{_datadir}/apps/knotes/*
%{_datadir}/apps/kontact/*
%{_datadir}/apps/kontactsummary/*
%{_datadir}/apps/korgac/*
%{_datadir}/apps/korganizer/*
%{_datadir}/apps/ktimetracker/*
%{_datadir}/apps/ktnef/*
%{_datadir}/apps/kwatchgnupg/*
%{_datadir}/apps/libkleopatra/pics/*
%{_datadir}/apps/libmessageviewer/pics/*.png
%{_datadir}/apps/messagelist/pics/mail-*.png
%{_datadir}/apps/messageviewer/*
%{_datadir}/apps/pimsettingexporter/*
%{_datadir}/autostart/*.desktop
%{_datadir}/config.kcfg/*.kcfg
%{_datadir}/config/*rc
%{_datadir}/dbus-1/interfaces/org.kde.*.xml
%{_datadir}/dbus-1/system-services/org.kde.kalarmrtcwake.service
%{_datadir}/icons/*/*x*/actions/*.png
%{_datadir}/icons/*/*x*/apps/*.png
%{_datadir}/icons/*/scalable/apps/*.svgz
%{_datadir}/icons/oxygen/*x*/mimetypes/x-mail-distribution-list.png
%{_datadir}/icons/oxygen/scalable/actions/edit-delete-page.svgz
%{_datadir}/icons/oxygen/scalable/mimetypes/x-mail-distribution-list.svgz
%{_datadir}/kde4/services/ServiceMenus/kmail_addattachmentservicemenu.desktop
%{_datadir}/kde4/services/*.desktop
%{_datadir}/kde4/services/*.protocol
%{_datadir}/kde4/services/kontact/*.desktop
%{_datadir}/kde4/services/korganizer/*.desktop
%{_datadir}/kde4/services/kresources/kcal/*.desktop
%{_datadir}/kde4/services/kresources/knotes/*.desktop
%{_datadir}/kde4/services/kresources/*.desktop
%{_datadir}/kde4/servicetypes/*.desktop
%{_datadir}/ontology/kde/messagetag.*
%{_datadir}/polkit-1/actions/org.kde.kalarmrtcwake.policy

%files doc
%defattr(-,root,root,-)
%{_docdir}/HTML/en/*
%{_mandir}/man1/kabcclient.1.gz

%clean
rm -rf %{buildroot}

%changelog
* Sun Dec 15 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.11.2 to 4.11.4
* Tue Oct 29 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.11.1 to 4.11.2
* Sat Sep 28 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.11.0 to 4.11.1
* Mon Aug 19 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.10.5 to 4.11.0
* Sat Aug 17 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.10.3 to 4.10.5
* Mon Jun 10 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.10.2 to 4.10.3
* Wed Apr 10 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.10.1 to 4.10.2
* Sat Mar 16 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.10.0 to 4.10.1
* Sat Mar 2 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.9.0 to 4.10.0
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
