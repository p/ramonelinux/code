Name:		grantlee
Version:	0.4.0
Release:	1%{?dist}
Summary:	Qt string template engine based on the Django template system

Group:		User Interface/KDE
License:	GPLv2+
Url:		http://www.gitorious.org
Source:     http://downloads.grantlee.org/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:	cmake qt

%description
Grantlee is a plug-in based String Template system written using the Qt framework.
The goals of the project are to make it easier for application developers to separate the structure of documents from the data they contain, opening the door for theming.

%prep
%setup -q -n %{name}-%{version}

%build
mkdir build &&
cd build &&
cmake -DCMAKE_INSTALL_PREFIX=/usr \
      -DCMAKE_BUILD_TYPE=Release \
%ifarch x86_64
      -DLIB_SUFFIX=64 \
%endif
      .. &&
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
cd build
make install DESTDIR=%{buildroot}

%files
%defattr(-,root,root,-)
%{_includedir}/grantlee/*.h
%{_includedir}/grantlee_*.h
%{_libdir}/cmake/grantlee/Grantlee*.cmake
%{_libdir}/grantlee/0.3/grantlee_*.so
%{_libdir}/libgrantlee_core.so*
%{_libdir}/libgrantlee_gui.so*

%clean
rm -rf %{buildroot}

%changelog
* Sun Mar 30 2014 tanggeliang <tanggeliang@gmail.com>
- update from 0.3.0 to 0.4.0
* Wed Aug 21 2013 tanggeliang <tanggeliang@gmail.com>
- create
