Name:		ark
Version:	4.11.4
Release:	1%{?dist}
Summary:	Ark

Group:		User Interface/KDE
License:	GPLv2+
Url:		http://www.kde.org
Source:     ftp://ftp.kde.org/pub/kde/stable/%{version}/src/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:	cmake kde-baseapps libarchive
BuildRequires:  automoc docbook-xml docbook-xsl

%description
This package provides an archiving utility for KDE.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
mkdir build &&
cd build &&
cmake -DCMAKE_INSTALL_PREFIX=/usr \
      -DCMAKE_BUILD_TYPE=Release \
%ifarch x86_64
      -DLIB_SUFFIX=64 \
%endif
      .. &&
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
cd build
make install DESTDIR=%{buildroot}

%files
%defattr(-,root,root,-)
%{_bindir}/ark
%{_libdir}/kde4/arkpart.so
%{_libdir}/kde4/kerfuffle_*.so
%{_libdir}/kde4/libextracthere.so
%{_libdir}/libkerfuffle.so*
%{_datadir}/applications/kde4/ark.desktop
%{_datadir}/apps/ark/ark*.rc
%{_datadir}/config.kcfg/ark.kcfg
%{_datadir}/icons/hicolor/*x*/apps/ark.png
%{_datadir}/icons/hicolor/scalable/apps/ark.svgz
%{_datadir}/kde4/services/ServiceMenus/ark_*.desktop
%{_datadir}/kde4/services/ark_*.desktop
%{_datadir}/kde4/services/kerfuffle_*.desktop
%{_datadir}/kde4/servicetypes/kerfufflePlugin.desktop

%files doc
%defattr(-,root,root,-)
%{_docdir}/HTML/en/ark/*
%{_mandir}/man1/ark.1.gz

%clean
rm -rf %{buildroot}

%changelog
* Sun Dec 15 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.11.2 to 4.11.4
* Tue Oct 29 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.11.1 to 4.11.2
* Sat Sep 28 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.11.0 to 4.11.1
* Mon Aug 19 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.10.5 to 4.11.0
* Sat Aug 17 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.10.3 to 4.10.5
* Mon Jun 10 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.10.2 to 4.10.3
* Wed Apr 10 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.10.1 to 4.10.2
* Sat Mar 16 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.10.0 to 4.10.1
* Sat Mar 2 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.9.0 to 4.10.0
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
