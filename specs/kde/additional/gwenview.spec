Name:		gwenview
Version:	4.11.4
Release:	1%{?dist}
Summary:	Gwenview

Group:		User Interface/KDE
License:	GPLv2+
Url:		http://www.kde.org
Source:     ftp://ftp.kde.org/pub/kde/stable/%{version}/src/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:	cmake kdelibs
BuildRequires:  kactivities kde-baseapps nepomuk-core libkexiv libjpeg-turbo
BuildRequires:  automoc lcms2 shared-desktop-ontologies docbook-xml docbook-xsl

%description
Gwenview is a fast and easy-to-use image viewer for KDE.

%prep
%setup -q -n %{name}-%{version}

%build
mkdir build &&
cd build &&
cmake -DCMAKE_INSTALL_PREFIX=/usr \
      -DCMAKE_BUILD_TYPE=Release \
      .. &&
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
cd build
make install DESTDIR=%{buildroot}

%files
%defattr(-,root,root,-)
%{_bindir}/gwenview
%{_bindir}/gwenview_importer
%{_libdir}/kde4/gvpart.so
%{_libdir}/libgwenviewlib.so*
%{_datadir}/applications/kde4/gwenview.desktop
%{_datadir}/apps/gvpart/gvpart.rc
%{_datadir}/apps/gwenview/color-schemes/fullscreen.colors
%{_datadir}/apps/gwenview/cursors/zoom.png
%{_datadir}/apps/gwenview/gwenviewui.rc
%{_datadir}/apps/gwenview/images/background.png
%{_datadir}/apps/solid/actions/gwenview_importer*.desktop
%{_datadir}/icons/hicolor/*/actions/document-share.*
%{_datadir}/icons/hicolor/*/apps/gwenview.*
%{_datadir}/kde4/services/ServiceMenus/slideshow.desktop
%{_datadir}/kde4/services/gvpart.desktop
%{_docdir}/HTML/en/gwenview/*

%clean
rm -rf %{buildroot}

%changelog
* Sun Dec 15 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.11.2 to 4.11.4
* Tue Oct 29 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.11.1 to 4.11.2
* Sat Sep 28 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.11.0 to 4.11.1
* Mon Aug 19 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.10.5 to 4.11.0
* Sat Aug 17 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.10.3 to 4.10.5
* Mon Jun 10 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.10.2 to 4.10.3
* Wed Apr 10 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.10.1 to 4.10.2
* Sat Mar 16 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.9.0 to 4.10.1
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
