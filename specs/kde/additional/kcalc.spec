Name:		kcalc
Version:	4.11.2
Release:	1%{?dist}
Summary:	Kcalc

Group:		User Interface/KDE
License:	GPLv2+
Url:		http://www.kde.org
Source:     ftp://ftp.kde.org/pub/kde/stable/%{version}/src/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:	cmake kdelibs
BuildRequires:  automoc docbook-xml docbook-xsl

%description
A scientific calculator.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
mkdir build &&
cd build &&
cmake -DCMAKE_INSTALL_PREFIX=/usr \
      -DCMAKE_BUILD_TYPE=Release \
%ifarch x86_64
      -DLIB_SUFFIX=64 \
%endif
      .. &&
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
cd build
make install DESTDIR=%{buildroot}

%files
%defattr(-,root,root,-)
%{_bindir}/kcalc
%{_libdir}/libkdeinit4_kcalc.so
%{_datadir}/applications/kde4/kcalc.desktop
%{_datadir}/apps/kcalc/kcalcui.rc
%{_datadir}/apps/kcalc/scienceconstants.xml
%{_datadir}/apps/kconf_update/kcalcrc.upd
%{_datadir}/config.kcfg/kcalc.kcfg

%files doc
%defattr(-,root,root,-)
%{_docdir}/HTML/en/kcalc/*

%clean
rm -rf %{buildroot}

%changelog
* Tue Oct 15 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.11.0 to 4.11.2
* Mon Aug 19 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.10.3 to 4.11.0
* Mon Jun 10 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.10.2 to 4.10.3
* Wed Apr 10 2013 tanggeliang <tanggeliang@gmail.com>
- create
