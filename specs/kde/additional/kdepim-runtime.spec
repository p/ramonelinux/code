Name:		kdepim-runtime
Version:	4.11.4
Release:	1%{?dist}
Summary:	Kdepim-runtime

Group:		User Interface/KDE
License:	GPLv2+
Url:		http://www.kde.org
Source:     ftp://ftp.kde.org/pub/kde/stable/%{version}/src/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:	cmake kdepimlibs
BuildRequires:  automoc docbook-xml docbook-xsl
BuildRequires:  shared-desktop-ontologies shared-mime-info

%description
This package provides additional resources for Akonadi.

%prep
%setup -q -n %{name}-%{version}

%build
mkdir build &&
cd build &&
cmake -DCMAKE_INSTALL_PREFIX=/usr \
      -DCMAKE_BUILD_TYPE=Release \
%ifarch x86_64
      -DLIB_SUFFIX=64 \
%endif
      .. &&
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
cd build
make install DESTDIR=%{buildroot}

%files
%defattr(-,root,root,-)
%{_bindir}/accountwizard
%{_bindir}/akonadi2xml
%{_bindir}/akonadi_*
%{_bindir}/akonaditray
%{_bindir}/kaddressbookmigrator
%{_bindir}/kjotsmigrator
%{_bindir}/kmail-migrator
%{_bindir}/kres-migrator
%{_bindir}/nepomukpimindexerutility
%{_libdir}/kde4/accountwizard_plugin.so
%{_libdir}/kde4/akonadi_*.so
%{_libdir}/kde4/imports/org/kde/*.qml
%{_libdir}/kde4/imports/org/kde/akonadi/*.qml
%{_libdir}/kde4/imports/org/kde/akonadi/*.png
%{_libdir}/kde4/imports/org/kde/akonadi/qmldir
%{_libdir}/kde4/imports/org/kde/*.png
%{_libdir}/kde4/imports/org/kde/libkdeqmlplugin.so
%{_libdir}/kde4/imports/org/kde/qmldir
%{_libdir}/kde4/kabc_akonadi.so
%{_libdir}/kde4/kcal_akonadi.so
%{_libdir}/kde4/kcm_akonadi*.so
%{_libdir}/kde4/kio_akonadi.so
%{_libdir}/libakonadi-filestore.so*
%{_libdir}/libakonadi-xml.so*
%{_libdir}/libkdepim-copy.so*
%{_libdir}/libkmindexreader.so*
%{_libdir}/libmaildir.so*
%{_libdir}/libnepomukfeederpluginlib.a
%{_datadir}/akonadi/agents/*.desktop
%{_datadir}/applications/kde4/*.desktop
%{_datadir}/apps/akonadi/accountwizard/*/*.desktop
%{_datadir}/apps/akonadi/accountwizard/*/*.es
%{_datadir}/apps/akonadi/accountwizard/*/*.ui
%{_datadir}/apps/akonadi/accountwizard/pop3/pop3wizard.js
%{_datadir}/apps/akonadi/akonadi-xml.xsd
%{_datadir}/apps/akonadi/firstrun/default*
%{_datadir}/apps/akonadi/plugins/serializer/akonadi_serializer_*.desktop
%{_datadir}/apps/akonadi_knut_resource/knut-template.xml
%{_datadir}/apps/akonadi_maildispatcher_agent/akonadi_maildispatcher_agent.notifyrc
%{_datadir}/apps/akonadi_newmailnotifier_agent/akonadi_newmailnotifier_agent.notifyrc
%{_datadir}/apps/kconf_update/newmailnotifier.upd
%{_datadir}/apps/nepomukpimindexerutility/nepomukpimindexerutility.rc
%{_datadir}/autostart/kaddressbookmigrator.desktop
%{_datadir}/config/accountwizard.knsrc
%{_datadir}/config/*-migratorrc
%{_datadir}/dbus-1/interfaces/org.kde.Akonadi.*.Settings.xml
%{_datadir}/icons/hicolor/*x*/apps/ox.png
%{_datadir}/icons/hicolor/*x*/apps/akonaditray.png
%{_datadir}/icons/hicolor/scalable/apps/akonaditray.svgz
%{_datadir}/kde4/services/akonadi.protocol
%{_datadir}/kde4/services/akonadi/davgroupware-providers/*.desktop
%{_datadir}/kde4/services/*.desktop
%{_datadir}/kde4/services/kresources/*/akonadi.desktop
%{_datadir}/kde4/servicetypes/*.desktop
%{_datadir}/mime/packages/accountwizard-mime.xml
%{_datadir}/mime/packages/kdepim-mime.xml
%{_datadir}/ontology/kde/aneo.ontology
%{_datadir}/ontology/kde/aneo.trig

%clean
rm -rf %{buildroot}

%changelog
* Sun Dec 15 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.11.2 to 4.11.4
* Tue Oct 29 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.11.1 to 4.11.2
* Sat Sep 28 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.11.0 to 4.11.1
* Mon Aug 19 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.10.5 to 4.11.0
* Sat Aug 17 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.10.3 to 4.10.5
* Mon Jun 10 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.10.2 to 4.10.3
* Wed Apr 10 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.10.1 to 4.10.2
* Sat Mar 16 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.10.0 to 4.10.1
* Sat Mar 2 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.9.0 to 4.10.0
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
