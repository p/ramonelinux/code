Name:		kdeplasma-addons
Version:	4.11.4
Release:	1%{?dist}
Summary:	Kdeplasma-addons

Group:		User Interface/KDE
License:	GPLv2+
Url:		http://www.kde.org
Source:     ftp://ftp.kde.org/pub/kde/stable/%{version}/src/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:	cmake kde-workspace kdepimlibs libkexiv
BuildRequires:  automoc shared-mime-info

%description
This package provides extra Plasma applets and engines like lancelot, calculator, wallpapers etc.

%prep
%setup -q -n %{name}-%{version}

%build
mkdir build &&
cd build &&
cmake -DCMAKE_INSTALL_PREFIX=/usr \
      -DCMAKE_BUILD_TYPE=Release \
%ifarch x86_64
      -DLIB_SUFFIX=64 \
%endif
      .. &&
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
cd build
make install DESTDIR=%{buildroot}

%files
%defattr(-,root,root,-)
%{_bindir}/lancelot
%{_includedir}/KDE/Lancelot/*
%{_includedir}/lancelot-datamodels/*.h
%{_includedir}/lancelot/*.h
%{_includedir}/lancelot/layouts/*.h
%{_includedir}/lancelot/models/*.h
%{_includedir}/lancelot/widgets/*.h
%{_libdir}/kde4/kcm_krunner_*.so
%{_libdir}/kde4/kcm_plasma_runner_events.so
%{_libdir}/kde4/krunner_*.so
%{_libdir}/kde4/plasma*.so
%{_libdir}/liblancelot*.so*
%{_libdir}/libplasma_groupingcontainment.so*
%{_libdir}/libplasmacomicprovidercore.so*
%{_libdir}/libplasmapotdprovidercore.so*
%{_libdir}/libplasmaweather.so*
%{_libdir}/librtm.so*
%{_datadir}/apps/bball/*
%{_datadir}/apps/cmake/*
%{_datadir}/apps/desktoptheme/*
%{_datadir}/apps/kdeplasma-addons/*
%{_datadir}/apps/lancelot/*
%{_datadir}/apps/plasma-applet-frame/*
%{_datadir}/apps/plasma-applet-opendesktop-activities/*
%{_datadir}/apps/plasma-applet-opendesktop/*
%{_datadir}/apps/plasma/*
%{_datadir}/apps/plasma_pastebin/*
%{_datadir}/apps/plasma_wallpaper_pattern/patterns/*
%{_datadir}/apps/plasmaboard/*
%{_datadir}/apps/rssnow/*
%{_datadir}/config.kcfg/kimpanelconfig.kcfg
%{_datadir}/config/*.knsrc
%{_datadir}/icons/hicolor/*x*/apps/*.png
%{_datadir}/icons/hicolor/*x*/actions/*.png
%{_datadir}/icons/hicolor/scalable/apps/*.svgz
%{_datadir}/icons/hicolor/scalable/actions/*.svgz
%{_datadir}/kde4/services/*.desktop
%{_datadir}/kde4/services/ServiceMenus/preview.desktop
%{_datadir}/kde4/servicetypes/plasma_*.desktop
%{_datadir}/mime/packages/lancelotpart-mime.xml

%clean
rm -rf %{buildroot}

%changelog
* Sun Dec 15 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.11.2 to 4.11.4
* Tue Oct 29 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.11.1 to 4.11.2
* Sat Sep 28 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.11.0 to 4.11.1
* Mon Aug 19 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.10.5 to 4.11.0
* Sun Aug 18 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.10.3 to 4.10.5
* Mon Jun 10 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.10.2 to 4.10.3
* Wed Apr 10 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.10.1 to 4.10.2
* Sat Mar 16 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.9.0 to 4.10.1
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
