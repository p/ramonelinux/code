Name:       telepathy-account-widgets
Version:    20140203
Release:    1%{?dist}
Summary:    telepathy-account-widgets

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.freedesktop.org
Source:     http://telepathy.freedesktop.org/releases/%{name}-master.zip

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  autoconf automake m4 xml-parser
BuildRequires:  gnome-common intltool libtool gettext glib
BuildRequires:  libice dbus-glib libsecret libxml telepathy-glib gtk+

%description

%prep
%setup -q -n %{name}-master

%build
./autogen.sh --prefix=/usr \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_datadir}/glib-2.0/schemas/org.gnome.telepathy-account-widgets.gschema.xml
%{_datadir}/locale/*/LC_MESSAGES/tpaw.mo
%{_datadir}/telepathy-account-widgets/icons/hicolor/*x*/apps/im-*.png
%{_datadir}/telepathy-account-widgets/icons/hicolor/scalable/apps/im-*.svg
%{_datadir}/telepathy-account-widgets/irc-networks.xml

%clean
rm -rf %{buildroot}

%changelog
* Sat Mar 22 2014 tanggeliang <tanggeliang@gmail.com>
- create
