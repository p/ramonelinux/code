%define scripts_version 20130123

Name:       setup
Version:    7.3
Release:    2%{?dist}
Summary:    setup

Group:      System Environment/Base
License:    GPLv2+
Url:        http://www.linuxfromscratch.org
Source:     http://www.linuxfromscratch.org/lfs/downloads/stable/lfs-bootscripts-%{scripts_version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildArch:      noarch
Requires:       coreutils kbd systemd util-linux
Requires:       iana-etc iproute inetutils consolekit e2fsprogs

%description

%package        bootscripts
Summary:        LFS bootscripts
BuildArch:      noarch
Requires:       findutils grep gzip
%description    bootscripts

%prep
%setup -q -n lfs-bootscripts-%{scripts_version}

%build

%check

%install
make DESTDIR=%{buildroot} install

##
## Create the /etc/mtab File
##
touch %{buildroot}/etc/mtab

##
## Customizing the /etc/hosts File
##
cat > %{buildroot}/etc/hosts << "EOF"
# Begin /etc/hosts (network card version)

127.0.0.1 localhost
192.168.1.1 HOSTNAME.example.org [alias1] [alias2 ...]

# End /etc/hosts (network card version)
EOF

##
## Configuring the system hostname
##
echo "HOSTNAME=localhost.localdomain" > %{buildroot}/etc/sysconfig/network

##
## Configuring the setclock Script
##
cat > %{buildroot}/etc/sysconfig/clock << "EOF"
# Begin /etc/sysconfig/clock

UTC=1

# Set this to any options you might need to give to hwclock, 
# such as machine hardware clock type for Alphas.
CLOCKPARAMS=

# End /etc/sysconfig/clock
EOF

##
## Configuring the Linux Console
##
cat > %{buildroot}/etc/sysconfig/console << "EOF"
# Begin /etc/sysconfig/console

UNICODE="1"
KEYMAP="pl2"
FONT="lat2a-16 -m 8859-2"

# End /etc/sysconfig/console
EOF

##
## Creating the /etc/inputrc File
##
cat > %{buildroot}/etc/inputrc << "EOF"
# Begin /etc/inputrc
# Modified by Chris Lynn <roryo@roryo.dynup.net>

# Allow the command prompt to wrap to the next line
set horizontal-scroll-mode Off

# Enable 8bit input
set meta-flag On
set input-meta On

# Turns off 8th bit stripping
set convert-meta Off

# Keep the 8th bit for display
set output-meta On

# none, visible or audible
set bell-style none

# All of the following map the escape sequence of the value
# contained in the 1st argument to the readline specific functions
"\eOd": backward-word
"\eOc": forward-word

# for linux console
"\e[1~": beginning-of-line
"\e[4~": end-of-line
"\e[5~": beginning-of-history
"\e[6~": end-of-history
"\e[3~": delete-char
"\e[2~": quoted-insert

# for xterm
"\eOH": beginning-of-line
"\eOF": end-of-line

# for Konsole
"\e[H": beginning-of-line
"\e[F": end-of-line

# End /etc/inputrc
EOF

##
## Creating the /etc/fstab File
##
cat > %{buildroot}/etc/fstab << "EOF"
# Begin /etc/fstab

# file system  mount-point  type     options             dump  fsck
#                                                              order

/dev/sda3      /            ext4     defaults            1     1
/dev/sda6      /home        ext4     defaults            1     2
/dev/sda7      swap         swap     pri=1               0     0
proc           /proc        proc     nosuid,noexec,nodev 0     0
sysfs          /sys         sysfs    nosuid,noexec,nodev 0     0
devpts         /dev/pts     devpts   gid=4,mode=620      0     0
tmpfs          /run         tmpfs    defaults            0     0
devtmpfs       /dev         devtmpfs mode=0755,nosuid    0     0

# End /etc/fstab
EOF

##
## Create an /etc/lsb-release File
##
echo 0.97 > %{buildroot}/etc/ram-release
cat > %{buildroot}/etc/lsb-release << "EOF"
DISTRIB_ID="ramone"
DISTRIB_RELEASE="0.97"
DISTRIB_CODENAME="201308"
DISTRIB_DESCRIPTION="Ramone Linux"
EOF

cp %{buildroot}/etc/ram-release %{buildroot}/etc/os-release

%files
%defattr(-,root,root,-)
/etc/mtab
/etc/hosts
/etc/sysconfig/clock
/etc/sysconfig/console
/etc/sysconfig/network
/etc/inputrc
/etc/fstab
/etc/lsb-release
/etc/ram-release
/etc/os-release

%files bootscripts
%defattr(-,root,root,-)
/etc/init.d
/etc/rc.d/init.d/checkfs
/etc/rc.d/init.d/cleanfs
/etc/rc.d/init.d/console
/etc/rc.d/init.d/functions
/etc/rc.d/init.d/halt
/etc/rc.d/init.d/localnet
/etc/rc.d/init.d/modules
/etc/rc.d/init.d/mountfs
/etc/rc.d/init.d/mountvirtfs
/etc/rc.d/init.d/network
/etc/rc.d/init.d/rc
/etc/rc.d/init.d/reboot
/etc/rc.d/init.d/sendsignals
/etc/rc.d/init.d/setclock
/etc/rc.d/init.d/swap
/etc/rc.d/init.d/sysctl
/etc/rc.d/init.d/sysklogd
/etc/rc.d/init.d/template
/etc/rc.d/init.d/udev
/etc/rc.d/init.d/udev_retry
/etc/rc.d/rc0.d/K80network
/etc/rc.d/rc0.d/K90sysklogd
/etc/rc.d/rc0.d/S60sendsignals
/etc/rc.d/rc0.d/S65swap
/etc/rc.d/rc0.d/S70mountfs
/etc/rc.d/rc0.d/S90localnet
/etc/rc.d/rc0.d/S99halt
/etc/rc.d/rc1.d/K80network
/etc/rc.d/rc1.d/K90sysklogd
/etc/rc.d/rc2.d/K80network
/etc/rc.d/rc2.d/K90sysklogd
/etc/rc.d/rc3.d/S10sysklogd
/etc/rc.d/rc3.d/S20network
/etc/rc.d/rc4.d/S10sysklogd
/etc/rc.d/rc4.d/S20network
/etc/rc.d/rc5.d/S10sysklogd
/etc/rc.d/rc5.d/S20network
/etc/rc.d/rc6.d/K80network
/etc/rc.d/rc6.d/K90sysklogd
/etc/rc.d/rc6.d/S60sendsignals
/etc/rc.d/rc6.d/S65swap
/etc/rc.d/rc6.d/S70mountfs
/etc/rc.d/rc6.d/S90localnet
/etc/rc.d/rc6.d/S99reboot
/etc/rc.d/rcS.d/S00mountvirtfs
/etc/rc.d/rcS.d/S05modules
/etc/rc.d/rcS.d/S08localnet
/etc/rc.d/rcS.d/S10udev
/etc/rc.d/rcS.d/S20swap
/etc/rc.d/rcS.d/S30checkfs
/etc/rc.d/rcS.d/S40mountfs
/etc/rc.d/rcS.d/S45cleanfs
/etc/rc.d/rcS.d/S50udev_retry
/etc/rc.d/rcS.d/S70console
/etc/rc.d/rcS.d/S90sysctl
/etc/sysconfig/createfiles
/etc/sysconfig/modules
/etc/sysconfig/rc.site
/etc/sysconfig/udev_retry
/lib/lsb
/lib/services/*
/sbin/ifdown
/sbin/ifup
/usr/share/man/man8/ifdown.8.gz
/usr/share/man/man8/ifup.8.gz

%clean
rm -rf %{buildroot}

%changelog
* Sat Aug 17 2013 tanggeliang <tanggeliang@gmail.com>
- update from bootscripts to setup
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
