%if 0%{?fedora} >= 12 || 0%{?rhel} >= 6
  %bcond_with usermode
  %bcond_with polkit0
  %bcond_without polkit1
%else
  %if 0%{?fedora} >= 10
    %bcond_with usermode
    %bcond_without polkit0
  %else
    %bcond_without usermode
    %bcond_with polkit0
  %endif
  %bcond_with polkit1
%endif

Summary: A graphical interface for basic firewall setup
Name: system-config-firewall
Version: 1.2.29
Release: 8%{?dist}
URL: http://fedorahosted.org/system-config-firewall
License: GPLv2+
ExclusiveOS: Linux
Group: System Environment/Base
BuildRoot: %(mktemp -ud %{_tmppath}/%{name}-%{version}-%{release}-XXXXXX)
Source0: https://fedorahosted.org/released/system-config-firewall/%{name}-%{version}.tar.bz2
# replace pickle by json (CVE-2011-2520):
Patch0: system-config-firewall-1.2.27-rhbz#717985.patch
# always allow ipv6-dhcp
Patch1: system-config-firewall-1.2.29-ipv6-dhcp.patch
BuildRequires: desktop-file-utils
BuildRequires: gettext
BuildRequires: intltool
Requires: hicolor-icon-theme
Requires: pygtk
#Requires: pygtk2-libglade
Requires: gtk2 >= 2.6
Requires: dbus-python
%if %{with usermode}
#Requires: usermode-gtk >= 1.94
%endif
%if %{with polkit0}
#Requires: python-slip-dbus >= 0.1.15
%endif
%if %{with polkit1}
#Requires: python-slip-dbus >= 0.2.7
%endif

%description
system-config-firewall is a graphical user interface for basic firewall setup.

%prep
%setup -q
%patch0 -p1 -b .rhbz#717985
%patch1 -p1 -b .ipv6-dhcp

%build
%configure %{?with_usermode: --enable-usermode} \
	   %{?with_polkit0: --enable-policykit0} \
	   %{!?with_polkit1: --disable-policykit1}

%install
rm -rf %{buildroot}

make install DESTDIR=%{buildroot}

desktop-file-install --vendor system --delete-original \
	--dir %{buildroot}%{_datadir}/applications \
	%{buildroot}%{_datadir}/applications/system-config-firewall.desktop

%find_lang %{name} --all-name

%clean
rm -rf %{buildroot}

%post
if [ $1 -eq 2 ]; then
   # kill the D-BUS mechanism on update
   killall -TERM system-config-firewall-mechanism.py >&/dev/null || :
fi
touch --no-create %{_datadir}/icons/hicolor &>/dev/null || :

%postun
if [ $1 -eq 0 ] ; then
    touch --no-create %{_datadir}/icons/hicolor &>/dev/null
    gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
fi

%posttrans
gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :

%triggerpostun -- %{name} < 1.1.0
%{_datadir}/system-config-firewall/convert-config

%triggerpostun -- system-config-securitylevel
%{_datadir}/system-config-firewall/convert-config

%files
%defattr(-,root,root)
%{_bindir}/system-config-firewall
%if %{with usermode}
%{_datadir}/system-config-firewall/system-config-firewall
%endif
%defattr(0644,root,root)
#%{_sysconfdir}/dbus-1/system.d/org.fedoraproject.Config.Firewall.conf
#%{_datadir}/dbus-1/system-services/org.fedoraproject.Config.Firewall.service
%if %{with polkit0}
%{_datadir}/PolicyKit/policy/org.fedoraproject.config.firewall.0.policy
%endif
%if %{with polkit1}
%{_datadir}/polkit-1/actions/org.fedoraproject.config.firewall.policy
%endif
%{_datadir}/system-config-firewall/fw_gui.*
%{_datadir}/system-config-firewall/fw_dbus.*
%{_datadir}/system-config-firewall/fw_nm.*
%{_datadir}/system-config-firewall/gtk_*
%{_datadir}/system-config-firewall/*.glade
%attr(0755,root,root) %{_datadir}/system-config-firewall/system-config-firewall-mechanism.*
%{_datadir}/applications/system-config-firewall.desktop
%{_datadir}/icons/hicolor/*/apps/preferences-system-firewall*.*
%if %{with usermode}
#%config /etc/security/console.apps/system-config-firewall
#%config /etc/pam.d/system-config-firewall
%endif

%doc COPYING
%{_sbindir}/lokkit
%attr(0755,root,root) %{_datadir}/system-config-firewall/convert-config
%dir %{_datadir}/system-config-firewall
%defattr(0644,root,root)
%{_datadir}/system-config-firewall/etc_services.*
%{_datadir}/system-config-firewall/fw_compat.*
%{_datadir}/system-config-firewall/fw_config.*
%{_datadir}/system-config-firewall/fw_firewalld.*
%{_datadir}/system-config-firewall/fw_functions.*
%{_datadir}/system-config-firewall/fw_icmp.*
%{_datadir}/system-config-firewall/fw_iptables.*
%{_datadir}/system-config-firewall/fw_lokkit.*
%{_datadir}/system-config-firewall/fw_parser.*
%{_datadir}/system-config-firewall/fw_selinux.*
%{_datadir}/system-config-firewall/fw_services.*
%{_datadir}/system-config-firewall/fw_sysconfig.*
%{_datadir}/system-config-firewall/fw_sysctl.*
%ghost %config(missingok,noreplace) /etc/sysconfig/system-config-firewall
%{_bindir}/system-config-firewall-tui
%{_datadir}/system-config-firewall/fw_tui.*

/etc/pam.d/system-config-firewall.pam
/etc/security/console.apps/system-config-firewall.console
%{_datadir}/locale/*/LC_MESSAGES/system-config-firewall.mo

%changelog
