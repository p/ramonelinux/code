%{!?python_sitearch: %global python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")}
Name:       newt
Version:    0.52.14
Release:    5%{?dist}
Summary:    A library for text mode user interfaces

Group:      System Environment/Libraries
License:    LGPLv2
URL:        https://fedorahosted.org/newt/
Source:     https://fedorahosted.org/released/newt/newt-%{version}.tar.gz

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  popt python slang
#BuildRequires:  docbook-utils
Provides:       snack = %{version}-%{release}

%Description
Newt is a programming library for color text mode, widget based user
interfaces.  Newt can be used to add stacked windows, entry widgets,
checkboxes, radio buttons, labels, plain text fields, scrollbars,
etc., to text mode user interfaces.  This package also contains the
shared library needed by programs built with newt, as well as a
/usr/bin/dialog replacement called whiptail.  Newt is based on the
slang library.

%prep
%setup -q

%build
# gpm support seems to smash the stack w/ we use help in anaconda??
# --with-gpm-support
%configure --without-tcl
make %{?_smp_mflags} all
chmod 0644 peanuts.py popcorn.py
#docbook2txt tutorial.sgml

%install
rm -rf $RPM_BUILD_ROOT
make DESTDIR=$RPM_BUILD_ROOT install

%find_lang %{name}

%clean
rm -rf $RPM_BUILD_ROOT

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files -f %{name}.lang
%defattr (-,root,root)
%doc COPYING
%{_bindir}/whiptail
%{_libdir}/libnewt.so.*
%{_mandir}/man1/whiptail.1*

%doc tutorial.*
%{_includedir}/newt.h
%{_libdir}/libnewt.so
%{_libdir}/pkgconfig/libnewt.pc

%{_libdir}/libnewt.a

%doc peanuts.py popcorn.py
%{python_sitearch}/*.so
%{python_sitearch}/*.py*

%changelog
