%{!?python_sitelib: %global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(0)")}
%{!?python_version: %global python_version %(%{__python} -c "from distutils.sysconfig import get_python_version; print get_python_version()")}

# Command line configurables

%if 0%{?fedora}%{?rhel} == 0 || 0%{?fedora} >= 8 || 0%{?rhel} >= 6
%bcond_without newt_python
%else
%bcond_with newt_python
%endif

%if 0%{?fedora}%{?rhel} == 0 || 0%{?fedora} >= 9 || 0%{?rhel} >= 6
%bcond_without console_util
%else
%bcond_with console_util
%endif

# Enterprise versions pull in docs automatically
%if 0%{?rhel} > 0
%bcond_without require_docs
%else
%bcond_with require_docs
%endif

# Use systemd instead of traditional init
%if ! 0%{?fedora}%{?rhel} || 0%{?fedora} >= 15 || 0%{?rhel} >= 7
%bcond_without systemd
%else
%bcond_with systemd
%endif

Summary: A graphical interface for modifying system date and time
Name: system-config-date
Version: 1.9.67
Release: 5%{?dist}
URL: http://fedorahosted.org/%{name}
License: GPLv2+
Group: System Environment/Base
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Source0: http://fedorahosted.org/released/%{name}/%{name}-%{version}.tar.bz2
# Until version 1.9.34, system-config-date contained online documentation.
# From version 1.9.35 on, online documentation is split off into its own
# package system-config-date-docs. The following ensures that updating from
# earlier versions gives you both the main package and documentation.
Obsoletes: system-config-date < 1.9.35
%if %{with require_docs}
Requires: system-config-date-docs
%endif
BuildRequires: desktop-file-utils
BuildRequires: gettext
BuildRequires: intltool
BuildRequires: python expat

Requires: python >= 2.0
Requires: pygtk >= 2.12.0
#Requires: pygtk2-libglade
#Requires: gnome-python2-canvas
%if %{with console_util}
#Requires: usermode-gtk >= 1.94
%else
#Requires: usermode-gtk >= 1.36
%endif

%if %{with systemd}
#Requires: systemd-units
%else
Requires: chkconfig
%endif

%if %{with newt_python}
Requires: newt
%else
Requires: newt
%endif
Requires: hicolor-icon-theme
# system-config-date can act as a plugin to set the time/date, configure NTP or
# the timezone for firstboot if the latter is present, but doesn't require it.
# It won't work with old versions of firstboot however.
Conflicts: firstboot <= 1.3.26

%description
system-config-date is a graphical interface for changing the system date and
time, configuring the system time zone, and setting up the NTP daemon to
synchronize the time of the system with an NTP time server.

%prep
%setup -q

%build
make \
%if 0%{?fedora} > 0
    POOL_NTP_ORG_VENDOR=fedora \
%endif
%if 0%{?rhel} > 0
    POOL_NTP_ORG_VENDOR=rhel \
%endif
%if %{with console_util}
    CONSOLE_USE_CONFIG_UTIL=1 \
%endif
    %{?_smp_mflags}

%install
rm -rf $RPM_BUILD_ROOT
make DESTDIR=$RPM_BUILD_ROOT install
desktop-file-install --vendor system --delete-original       \
  --dir $RPM_BUILD_ROOT%{_datadir}/applications             \
  --add-category X-Red-Hat-Base                             \
  $RPM_BUILD_ROOT%{_datadir}/applications/system-config-date.desktop

%find_lang %name

%clean
rm -rf $RPM_BUILD_ROOT

%post
touch --no-create %{_datadir}/icons/hicolor
if [ -x %{_bindir}/gtk-update-icon-cache ]; then
  %{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor || :
fi

%postun
touch --no-create %{_datadir}/icons/hicolor
if [ -x %{_bindir}/gtk-update-icon-cache ]; then
  %{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor || :
fi

%files -f %{name}.lang
%defattr(-,root,root,-)
%doc COPYING
%{_bindir}/system-config-date
%{_datadir}/system-config-date
%{_datadir}/applications/system-config-date.desktop
%{_datadir}/icons/hicolor/*/apps/system-config-date.*
%{_mandir}/man8/system-config-date*
%{_mandir}/fr/man8/system-config-date*
%{_mandir}/ja/man8/system-config-date*
%config(noreplace) %{_sysconfdir}/security/console.apps/system-config-date
%config(noreplace) %{_sysconfdir}/pam.d/system-config-date
%{python_sitelib}/scdate
%{python_sitelib}/scdate-%{version}-py%{python_version}.egg-info
#%{python_sitelib}/scdate.dbus-%{version}-py%{python_version}.egg-info

%changelog
