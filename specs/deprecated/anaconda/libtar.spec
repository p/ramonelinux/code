Name:       libtar
Version:    1.2.11
Release:    24%{?dist}
Summary:    Tar file manipulation API

Group:      System Environment/Libraries
License:    MIT
URL:        http://www.feep.net/libtar/
Source:     ftp://ftp.feep.net/pub/software/libtar/libtar-%{version}.tar.gz
Patch0:     http://ftp.debian.org/debian/pool/main/libt/libtar/libtar_1.2.11-4.diff.gz
Patch1:     libtar-1.2.11-missing-protos.patch
Patch2:     libtar-macro.patch
Patch3:     libtar-1.2.11-tar_header.patch
Patch4:     libtar-1.2.11-mem-deref.patch
Patch5:     libtar-1.2.11-fix-memleak.patch
Patch6:     libtar-1.2.11-bz729009.patch

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-buildroot
BuildRequires:  libtool autoconf automake m4

%description
libtar is a C library for manipulating tar archives. It supports both the strict POSIX tar format and many of the commonly-used GNU extensions.

%prep
%setup -q
%patch0 -p1 -z .deb
%patch1 -p1
%patch2 -p1
%patch3 -p1 -b .tar_header
%patch4 -p1 -b .deref
%patch5 -p1 -b .fixmem
%patch6 -p1

# set correct version for .so build
%global ltversion %(echo %{version} | tr '.' ':')
sed -i 's/-rpath $(libdir)/-rpath $(libdir) -version-number %{ltversion}/' \
  lib/Makefile.in
# sanitize the macro definitions so that aclocal can find them:
cd autoconf
sed '/^m4_include/d;s/ m4_include/ m4][_include/g' aclocal.m4 >psg.m4
rm acsite.m4 aclocal.m4
cd ..

%build
cp -p /usr/share/libtool/config/config.sub autoconf
# config.guess is not needed, macro %%configure specifies --build
libtoolize --copy
aclocal -I autoconf
autoconf
%configure --disable-static
make %{?_smp_mflags}

%install
make install DESTDIR=$RPM_BUILD_ROOT
# Without this we get no debuginfo and stripping
chmod +x $RPM_BUILD_ROOT%{_libdir}/libtar.so.%{version}
rm $RPM_BUILD_ROOT%{_libdir}/*.la

%files
%doc COPYRIGHT TODO README ChangeLog*
%{_bindir}/%{name}
%{_libdir}/lib*.so.*
%{_includedir}/libtar.h
%{_includedir}/libtar_listhash.h
%{_libdir}/lib*.so
%{_mandir}/man3/*.3*

%changelog
