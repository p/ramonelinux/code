Name:       cryptsetup
Version:    1.5.0
Release:    2%{?dist}
Summary:    A utility for setting up encrypted disks

Group:      Applications/System
License:    GPLv2 and LGPLv2+
URL:        http://cryptsetup.googlecode.com
Source:     http://cryptsetup.googlecode.com/files/%{name}-%{version}.tar.bz2

BuildRequires:  libgcrypt, popt, lvm
BuildRequires:  libgpg-error, libsepol
BuildRequires:  libselinux, python
BuildRequires:  fipscheck
#libuuid-devel
Requires:       fipscheck

%description
The cryptsetup package contains a utility for setting up disk encryption using dm-crypt kernel module.

%prep
%setup -q -n %{name}-%{version}
chmod -x python/pycryptsetup-test.py
chmod -x misc/dracut_90reencrypt/*

%build
%configure --enable-python --enable-fips --enable-cryptsetup-reencrypt
# remove rpath
sed -i 's|^hardcode_libdir_flag_spec=.*|hardcode_libdir_flag_spec=""|g' libtool
sed -i 's|^runpath_var=LD_RUN_PATH|runpath_var=DIE_RPATH_DIE|g' libtool
make %{?_smp_mflags}

%install
# Generate HMAC checksums (FIPS)
%define __spec_install_post \
  %{?__debug_package:%{__debug_install_post}} \
  %{__arch_install_post} \
  %{__os_install_post} \
  fipshmac -d %{buildroot}/%{_libdir}/fipscheck %{buildroot}/%{_sbindir}/cryptsetup \
  fipshmac -d %{buildroot}/%{_libdir}/fipscheck %{buildroot}/%{_libdir}/libcryptsetup.so.* \
%{nil}

make install DESTDIR=%{buildroot}
rm -rf %{buildroot}/%{_libdir}/*.la
install -d %{buildroot}/%{_libdir}/fipscheck
%find_lang cryptsetup

%files
%doc COPYING ChangeLog AUTHORS TODO FAQ
%{_mandir}/man8/cryptsetup.8.gz
%{_sbindir}/cryptsetup
%{_libdir}/fipscheck/cryptsetup.hmac

%{_mandir}/man8/veritysetup.8.gz
%{_sbindir}/veritysetup

%{_mandir}/man8/cryptsetup-reencrypt.8.gz
%{_sbindir}/cryptsetup-reencrypt

%{_includedir}/libcryptsetup.h
%{_libdir}/libcryptsetup.so
%{_libdir}/pkgconfig/libcryptsetup.pc

%{_libdir}/libcryptsetup.so.*
%{_libdir}/fipscheck/libcryptsetup.so.*.hmac

%exclude %{python_sitearch}/pycryptsetup.la
%{python_sitearch}/pycryptsetup.so

%clean

%changelog
