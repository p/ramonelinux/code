Name:       isomd5sum
Version:    1.0.11
Release:    1%{?dist}
Summary:    Utilities for working with md5sum implanted in ISO images

Group:      Applications/System
License:    GPLv2+
URL:        http://git.fedorahosted.org/git/?p=isomd5sum.git;a=summary
Source:     http://fedorahosted.org/releases/i/s/isomd5sum/%{name}-%{version}.tar.bz2

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  popt

%description
The isomd5sum package contains utilities for implanting and verifying an md5sum implanted into an ISO9660 image.

%prep
%setup -q

%build
CFLAGS="$RPM_OPT_FLAGS -Wno-strict-aliasing"; export CFLAGS
make checkisomd5 implantisomd5 pyisomd5sum.so

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install-bin install-devel install-python

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%{_includedir}/*.h
%{_bindir}/implantisomd5
%{_bindir}/checkisomd5
%{_libdir}/libcheckisomd5.a
%{_libdir}/libimplantisomd5.a
%{_libdir}/python2.7/site-packages/pyisomd5sum.so
%doc COPYING
%{_mandir}/man*/*

%changelog
* Tue Jul 23 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.0.9 to 1.0.11
