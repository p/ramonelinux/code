%global debug_package %{nil}
%{!?python_sitelib: %global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}

Name:           system-config-keyboard
Version:        1.3.1
Release:        9%{?dist}
Summary:        A graphical interface for modifying the keyboard

Group:          System Environment/Base
License:        GPLv2+
URL:            https://fedorahosted.org/system-config-keyboard/
Source0:        https://fedorahosted.org/releases/s/y/system-config-keyboard/%{name}-%{version}.tar.gz
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  desktop-file-utils
BuildRequires:  gettext
BuildRequires:  intltool

Requires:       usermode >= 1.36

Patch0:         s-c-keyboard-do_not_remove_the_OK_button.patch
Patch1:	        sck-1.3.1-no-pyxf86config.patch

%description
system-config-keyboard is a graphical user interface that allows 
the user to change the default keyboard of the system.

%prep
%setup -q
%patch0 -p1
%patch1 -p1

%build
make

%install
rm -rf $RPM_BUILD_ROOT
make INSTROOT=$RPM_BUILD_ROOT install
desktop-file-install --vendor system --delete-original      \
  --dir $RPM_BUILD_ROOT%{_datadir}/applications             \
   $RPM_BUILD_ROOT%{_datadir}/applications/system-config-keyboard.desktop

%find_lang %{name}

%clean
rm -rf $RPM_BUILD_ROOT

%post
touch --no-create %{_datadir}/icons/hicolor
if [ -x /usr/bin/gtk-update-icon-cache ]; then
  gtk-update-icon-cache -q %{_datadir}/icons/hicolor
fi

%postun
touch --no-create %{_datadir}/icons/hicolor
if [ -x /usr/bin/gtk-update-icon-cache ]; then
  gtk-update-icon-cache -q %{_datadir}/icons/hicolor
fi

%files
%defattr(-,root,root)
%{_sbindir}/system-config-keyboard
%{_bindir}/system-config-keyboard
%{_datadir}/system-config-keyboard
%attr(0755,root,root) %dir %{_datadir}/firstboot/modules
%{_datadir}/firstboot/modules/*
%attr(0644,root,root) %{_datadir}/applications/system-config-keyboard.desktop
%attr(0644,root,root) %config %{_sysconfdir}/security/console.apps/system-config-keyboard
%attr(0644,root,root) %config %{_sysconfdir}/pam.d/system-config-keyboard
%attr(0644,root,root) %{_datadir}/icons/hicolor/48x48/apps/system-config-keyboard.png
%doc COPYING
%{python_sitelib}/system_config_keyboard
/usr/share/locale/*/LC_MESSAGES/system-config-keyboard.mo

%changelog
