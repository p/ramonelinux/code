%{!?python_sitelib: %global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib())")}

Name:           transifex-client
Version:        0.7.3
Release:        5%{?dist}
Summary:        Command line tool for Transifex translation management

Group:          Development/Languages
License:        GPLv2
URL:            http://transifex.org
Source:         http://pypi.python.org/packages/source/t/transifex-client/%{name}-%{version}.tar.gz
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  python
BuildRequires:  python-setuptools

%description
The Transifex Command-line Client is a command line tool that enables
you to easily manage your translations within a project without the
need of an elaborate UI system.

%prep
%setup -q

%build
# Remove CFLAGS=... for noarch packages (unneeded)
CFLAGS="$RPM_OPT_FLAGS" %{__python} setup.py build

%install
rm -rf $RPM_BUILD_ROOT
%{__python} setup.py install -O1 --skip-build --root $RPM_BUILD_ROOT

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc LICENSE README.rst
%{python_sitelib}/*
%{_bindir}/*

%changelog
