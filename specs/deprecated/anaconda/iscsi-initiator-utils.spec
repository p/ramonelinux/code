%{!?python_sitearch: %global python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")}

Summary: iSCSI daemon and utility programs
Name: iscsi-initiator-utils
Version: 6.2.0.872
Release: 18%{?dist}
Source0: http://people.redhat.com/mchristi/iscsi/rhel6.0/source/open-iscsi-2.0-872-rc4-bnx2i.tar.gz
Source1: iscsid.init
Source2: iscsidevs.init
Source3: 04-iscsi
# sync brcm to 0.7.0.12
Patch0: iscsi-initiator-utils-sync-uio-0.7.0.8.patch
# sync iscsi tools to upstream commit e8c5b1d34ee5ce0a755ff54518829156dfa5fabe 
Patch1: iscsi-initiator-utils-sync-iscsi.patch
# Add Red Hat specific info to docs.
Patch2: iscsi-initiator-utils-update-initscripts-and-docs.patch
# Upstream uses /etc/iscsi for iscsi db info, but use /var/lib/iscsi.
Patch3: iscsi-initiator-utils-use-var-for-config.patch
# Add redhat.com string to default initiator name.
Patch4: iscsi-initiator-utils-use-red-hat-for-name.patch
# Add a lib for use by anaconda.
Patch5: iscsi-initiator-utils-add-libiscsi.patch
# Add bnx2i support.
Patch6: iscsi-initiator-utils-uip-mgmt.patch
# Don't compile iscsistart as static
Patch7: iscsi-initiator-utils-dont-use-static.patch
# Remove the OFFLOAD_BOOT_SUPPORTED #ifdef.
Patch8: iscsi-initiator-utils-remove-the-offload-boot-supported-ifdef.patch
# brcm uio: handle the different iface_rec structures in iscsid and brcm. 
Patch9: iscsi-initiator-utils-uio-handle-different-iface_rec.patch
# Document missing brcm arguments
Patch10: iscsi-initiator-utils-brcm-man.patch
# setup default ifaces for all ifaces in kernel
Patch11: iscsi-initiator-utils-fix-default-bindings.patch
# fix iscsiadm return value/msg when login fails
Patch12: iscsi-initiator-utils-fix-iscsiadm-return.patch
# don't use openssl-devel
Patch13: iscsi-initiator-utils-dont-use-openssl.patch
# sync uio to 0.7.0.14
Patch14: iscsi-initiator-utils-sync-uio-0.7.0.14.patch
# fix nl msglen
Patch15: iscsi-initiator-utils-fix-nlmsglen.patch
# fixes for offload iface support
Patch16: iscsi-initiator-utils-ofl-iface-fixes.patch
# fix ipv6 ibft/firmware boot
Patch17: iscsi-initiator-utils-fix-ipv6-boot.patch
# add rhel version info to iscsi tools
Patch18: iscsi-initiator-utils-add-rh-ver.patch

Group: System Environment/Daemons
License: GPLv2+
URL: http://www.open-iscsi.org
Buildroot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: openssl flex bison doxygen
# For dir ownership

%description
The iscsi package provides the server daemon for the iSCSI protocol,
as well as the utility programs used to manage it. iSCSI is a protocol
for distributed disk access using SCSI commands sent over Internet
Protocol networks.

%prep
%setup -q -n open-iscsi-2.0-872-rc4-bnx2i
%patch0 -p1 -b .sync-uio-0.7.0.8
%patch1 -p1 -b .sync-iscsi
%patch2 -p1 -b .update-initscripts-and-docs
%patch3 -p1 -b .use-var-for-config
%patch4 -p1 -b .use-red-hat-for-name
%patch5 -p1 -b .add-libiscsi
%patch6 -p1 -b .uip-mgmt
%patch7 -p1 -b .dont-use-static
%patch8 -p1 -b .remove-the-offload-boot-supported-ifdef
%patch9 -p1 -b .uio-handle-different-iface_rec
%patch10 -p1 -b .brcm-man
%patch11 -p1 -b .fix-default-bindings
%patch12 -p1 -b .fix-iscsiadm-return
%patch13 -p1 -b .dont-use-openssl
%patch14 -p1 -b .sync-uio-0.7.0.14
%patch15 -p1 -b .fix-nlmsglen
%patch16 -p1 -b .ofl-iface-fixes
%patch17 -p1 -b .fix-ipv6-boot
%patch18 -p1 -b .add-rh-ver

%build
cd utils/open-isns
./configure --with-security=no
make OPTFLAGS="%{optflags}"
cd ../../
make OPTFLAGS="%{optflags}" -C utils/sysdeps
make OPTFLAGS="%{optflags}" -C utils/fwparam_ibft
make OPTFLAGS="%{optflags}" -C usr
make OPTFLAGS="%{optflags}" -C utils
make OPTFLAGS="%{optflags}" -C libiscsi

cd iscsiuio
chmod u+x configure
./configure --enable-debug
make OPTFLAGS="%{optflags}"
cd ..

pushd libiscsi
python setup.py build
touch -r libiscsi.doxy html/*
popd

%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/sbin
mkdir -p ${RPM_BUILD_ROOT}%{_mandir}/man8
mkdir -p $RPM_BUILD_ROOT/etc/rc.d/init.d
mkdir -p $RPM_BUILD_ROOT/etc/iscsi
mkdir -p $RPM_BUILD_ROOT/etc/logrotate.d
mkdir -p $RPM_BUILD_ROOT/etc/NetworkManager/dispatcher.d
mkdir -p $RPM_BUILD_ROOT/var/lib/iscsi
mkdir -p $RPM_BUILD_ROOT/var/lib/iscsi/nodes
mkdir -p $RPM_BUILD_ROOT/var/lib/iscsi/send_targets
mkdir -p $RPM_BUILD_ROOT/var/lib/iscsi/static
mkdir -p $RPM_BUILD_ROOT/var/lib/iscsi/isns
mkdir -p $RPM_BUILD_ROOT/var/lib/iscsi/slp
mkdir -p $RPM_BUILD_ROOT/var/lib/iscsi/ifaces
mkdir -p $RPM_BUILD_ROOT/var/lock/iscsi
mkdir -p $RPM_BUILD_ROOT%{_libdir}
mkdir -p $RPM_BUILD_ROOT%{_includedir}
mkdir -p $RPM_BUILD_ROOT%{python_sitearch}

install -p -m 755 usr/iscsid usr/iscsiadm utils/iscsi-iname usr/iscsistart $RPM_BUILD_ROOT/sbin
install -p -m 644 doc/iscsiadm.8 $RPM_BUILD_ROOT/%{_mandir}/man8
install -p -m 644 doc/iscsid.8 $RPM_BUILD_ROOT/%{_mandir}/man8
install -p -m 644 etc/iscsid.conf $RPM_BUILD_ROOT%{_sysconfdir}/iscsi
install -p -m 644 doc/iscsistart.8 $RPM_BUILD_ROOT/%{_mandir}/man8
install -p -m 644 doc/iscsi-iname.8 $RPM_BUILD_ROOT/%{_mandir}/man8
install -p -m 644 iscsiuio/docs/iscsiuio.8 $RPM_BUILD_ROOT/%{_mandir}/man8
install -p -m 644 iscsiuio/iscsiuiolog $RPM_BUILD_ROOT%{_sysconfdir}/logrotate.d
install -p -m 755 iscsiuio/src/unix/iscsiuio $RPM_BUILD_ROOT/sbin

install -p -m 755 %{SOURCE1} $RPM_BUILD_ROOT%{_initrddir}/iscsid
install -p -m 755 %{SOURCE2} $RPM_BUILD_ROOT%{_initrddir}/iscsi
install -p -m 755 %{SOURCE3} $RPM_BUILD_ROOT/etc/NetworkManager/dispatcher.d

install -p -m 755 libiscsi/libiscsi.so.0 $RPM_BUILD_ROOT%{_libdir}
ln -s libiscsi.so.0 $RPM_BUILD_ROOT%{_libdir}/libiscsi.so
install -p -m 644 libiscsi/libiscsi.h $RPM_BUILD_ROOT%{_includedir}

install -p -m 755 libiscsi/build/lib.linux-*/libiscsimodule.so \
	$RPM_BUILD_ROOT%{python_sitearch}

# for %%ghost
touch $RPM_BUILD_ROOT/var/lock/iscsi/lock

%clean
rm -rf $RPM_BUILD_ROOT

%post
/sbin/ldconfig
if [ "$1" -eq "1" ]; then
	if [ ! -f %{_sysconfdir}/iscsi/initiatorname.iscsi ]; then
		echo "InitiatorName=`/sbin/iscsi-iname`" > %{_sysconfdir}/iscsi/initiatorname.iscsi
	fi
	/sbin/chkconfig --add iscsid
	/sbin/chkconfig --add iscsi
fi
# To make sure iscsid autostart works when upgrading from a version which
# did not have this in its config file
if ! grep -q 'iscsid\.startup' %{_sysconfdir}/iscsi/iscsid.conf; then
	echo -e "\n\n# For iscsid autostart" \
		"\niscsid.startup = /etc/rc.d/init.d/iscsid force-start" >> \
		%{_sysconfdir}/iscsi/iscsid.conf
fi

%postun -p /sbin/ldconfig

%preun
if [ "$1" = "0" ]; then
	# stop iscsi
	/sbin/service iscsi stop > /dev/null 2>&1
	# delete service
	/sbin/chkconfig --del iscsi
	# stop iscsid
	/sbin/service iscsid stop > /dev/null 2>&1
	# delete service
	/sbin/chkconfig --del iscsid
fi

%files
%defattr(-,root,root)
%doc README
%dir %{_var}/lib/iscsi
%dir %{_var}/lib/iscsi/nodes
%dir %{_var}/lib/iscsi/isns
%dir %{_var}/lib/iscsi/static
%dir %{_var}/lib/iscsi/slp
%dir %{_var}/lib/iscsi/ifaces
%dir %{_var}/lib/iscsi/send_targets
%ghost %{_var}/lock/iscsi
%ghost %{_var}/lock/iscsi/lock
%{_initrddir}/iscsi
%{_initrddir}/iscsid
%{_sysconfdir}/NetworkManager/dispatcher.d/04-iscsi
%dir %{_sysconfdir}/iscsi
%attr(0600,root,root) %config(noreplace) %{_sysconfdir}/iscsi/iscsid.conf
/sbin/*
%{_libdir}/libiscsi.so.0
%{python_sitearch}/libiscsimodule.so
%{_mandir}/man8/*
%{_sysconfdir}/logrotate.d/iscsiuiolog

%doc libiscsi/html
%{_libdir}/libiscsi.so
%{_includedir}/libiscsi.h

%changelog
