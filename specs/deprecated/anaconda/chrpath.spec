Name:       chrpath
Version:    0.13
Release:    5%{?dist}
Summary:    Modify rpath of compiled programs

Group:      Development/Tools
License:    GPL+
URL:        ftp://ftp.hungry.com/pub/hungry/chrpath/
Patch0:     chrpath-0.13-NULL-entry.patch
Source0:    ftp://ftp.hungry.com/pub/hungry/chrpath/%{name}-%{version}.tar.gz

BuildRoot:  %{_tmppath}/%{name}-%{version}-%{release}-build

%description
chrpath allows you to modify the dynamic library load path (rpath) of compiled programs.
Currently, only removing and modifying the rpath is supported.

%prep
%setup -q
%patch0 -p1 -b .NULL

%build
%configure
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}
rm -fr %{buildroot}/usr/doc

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING README NEWS ChangeLog*
%{_bindir}/chrpath
%{_mandir}/man1/chrpath.1*

%changelog
