Summary: Convenience library for kernel netlink sockets
Group: Development/Libraries
License: LGPLv2
Name: libnl1
Version: 1.1
Release: 15%{?dist}
URL: http://www.infradead.org/~tgr/libnl/
Source: http://www.infradead.org/~tgr/libnl/files/libnl-%{version}.tar.gz
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root
BuildRequires: doxygen
Patch1: libnl-1.0-pre5-static.patch
Patch2: libnl-1.0-pre5-debuginfo.patch
Patch3: libnl-1.0-pre8-use-vasprintf-retval.patch
Patch4: libnl-1.0-pre8-more-build-output.patch
Patch5: libnl-1.1-include-limits-h.patch
Patch6: libnl-1.1-doc-inlinesrc.patch
Patch7: libnl-1.1-no-extern-inline.patch
Patch8: libnl-1.1-align.patch
Patch9: libnl-1.1-disable-static-by-default.patch
Patch10: libnl-1.1-fix-portmap-position.patch
Patch11: libnl-1.1-threadsafe-port-allocation.patch

%description
This package contains a convenience library to simplify
using the Linux kernel's netlink sockets interface for
network manipulation

%prep
%setup -q -n libnl-%{version}
%patch1 -p1 -b .build-static
%patch2 -p1 -b .debuginfo
%patch3 -p1 -b .use-vasprintf-retval
%patch4 -p1 -b .more-build-output
%patch5 -p1 -b .limits
%patch6 -p1 -b .doc-inlinesrc
%patch7 -p1 -b .no-extern-inline
%patch8 -p1 -b .align
%patch9 -p1 -b .disable-static-by-default
%patch10 -p1 -b .fix-portmap-position
%patch11 -p1 -b .threadsafe-port-allocation

# a quick hack to make doxygen stripping builddir from html outputs.
sed -i.org -e "s,^STRIP_FROM_PATH.*,STRIP_FROM_PATH = `pwd`," doc/Doxyfile.in

%build
%configure
make
make gendoc

%install
%{__rm} -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT

mkdir $RPM_BUILD_ROOT/%{_lib}
mv $RPM_BUILD_ROOT%{_libdir}/libnl.so.* $RPM_BUILD_ROOT/%{_lib}
for l in $RPM_BUILD_ROOT%{_libdir}/libnl.so; do
    ln -sf $(echo %{_libdir} | \
        sed 's,\(^/\|\)[^/][^/]*,..,g')/%{_lib}/$(readlink $l) $l
done

%clean
%{__rm} -rf $RPM_BUILD_ROOT

%post -p /sbin/ldconfig
%postun -p /sbin/ldconfig

%files
%defattr(-,root,root)
/%{_lib}/libnl.so.*
%doc COPYING
%{_includedir}/netlink/
%doc doc/html
%{_libdir}/libnl.so
%{_libdir}/pkgconfig/libnl-1.pc

%changelog
