%define livearches %{ix86} x86_64 ppc ppc64

Name:    anaconda
Version: 17.29
Release: 2%{?dist}
Summary: Graphical system installer

Group:   Applications/System
License: GPLv2+
URL:     http://fedoraproject.org/wiki/Anaconda
# To generate Source0 do:
# git clone http://git.fedorahosted.org/git/anaconda.git
# git checkout -b archive-branch anaconda-%{version}-%{release}
# ./autogen.sh
# make dist
Source0: %{name}-%{version}.tar.bz2

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

# Versions of required components (done so we make sure the buildrequires
# match the requires versions of things).
%define dmver 1.02.17-6
%define gettextver 0.11
%define genisoimagever 1.1.9-4
%define gconfversion 2.28.1
%define intltoolver 0.31.2-3
%define libnlver 1.0
%define libselinuxver 1.6
%define pykickstartver 1.99.4
%define rpmpythonver 4.2-0.61
%define slangver 2.0.6-2
%define yumver 2.9.2
%define partedver 1.8.1
%define pypartedver 2.5-2
%define syscfgdatever 1.9.48
%define pythonpyblockver 0.45
%define e2fsver 1.41.0
%define nmver 1:0.7.1-3.git20090414
%define dbusver 1.2.3
%define createrepover 0.4.7
%define yumutilsver 1.1.11-3
%define iscsiver 6.2.0.870-3
%define pythoncryptsetupver 0.1.1
%define mehver 0.8
%define sckeyboardver 1.3.1
%define libblkidver 2.17.1-1
%define fcoeutilsver 1.0.12-3.20100323git
%define isomd5sumver 1.0.6

BuildRequires: audit systemd-udev
BuildRequires: bzip
BuildRequires: lvm
BuildRequires: e2fsprogs >= %{e2fsver}
BuildRequires: elfutils
BuildRequires: gettext >= %{gettextver}
BuildRequires: gtk2
BuildRequires: intltool >= %{intltoolver}
BuildRequires: isomd5sum >= %{isomd5sumver}
BuildRequires: libarchive
BuildRequires: libx11
BuildRequires: libxt
BuildRequires: libxxf86misc
BuildRequires: curl
BuildRequires: libnl >= %{libnlver} libnl1
BuildRequires: libselinux
BuildRequires: libsepol
BuildRequires: libxml
BuildRequires: newt
BuildRequires: pango
BuildRequires: pykickstart >= %{pykickstartver}
BuildRequires: python
BuildRequires: python-pyblock >= %{pythonpyblockver}
BuildRequires: urlgrabber
BuildRequires: python-nose
BuildRequires: rpm
BuildRequires: slang >= %{slangver}
BuildRequires: transifex-client
#BuildRequires: xmlto
BuildRequires: yum >= %{yumver}
BuildRequires: zlib
BuildRequires: networkmanager
BuildRequires: dbus dbus-python
BuildRequires: system-config-keyboard
%ifarch %livearches
BuildRequires: desktop-file-utils
%endif
BuildRequires: iscsi-initiator-utils >= %{iscsiver}

Requires: python-meh >= %{mehver}
#Requires: policycoreutils
Requires: rpm >= %{rpmpythonver}
#Requires: comps-extras
Requires: parted >= %{partedver}
Requires: pyparted >= %{pypartedver}
Requires: yum >= %{yumver}
Requires: libxml
Requires: urlgrabber
#Requires: system-logos
Requires: pykickstart >= %{pykickstartver}
Requires: system-config-date >= %{syscfgdatever}
#Requires: dosfstools
Requires: lvm
Requires: e2fsprogs >= %{e2fsver}
Requires: gzip
Requires: libarchive
Requires: python-pyblock >= %{pythonpyblockver}
Requires: libuser
Requires: newt
#Requires: authconfig
Requires: system-config-firewall
Requires: cryptsetup
#Requires: mdadm
Requires: util-linux >= 2.15.1
Requires: system-config-keyboard >= %{sckeyboardver}
Requires: dbus-python
#Requires: python-pwquality
#Requires: python-bugzilla
Requires: python-nss
#Requires: tigervnc-server-minimal
%ifarch %livearches
Requires: usermode
Requires: zenity
%endif
Requires: createrepo >= %{createrepover}
Requires: squashfs
#Requires: hfsplus-tools
Requires: cdrkit
Requires: gconf >= %{gconfversion}
%ifarch %{ix86} x86_64
Requires: syslinux >= 3.73
#Requires: makebootfat
Requires: lvm
%endif
Requires: isomd5sum
Requires: yum-utils >= %{yumutilsver}
Requires: networkmanager
#Requires: dhclient
#Requires: anaconda-yum-plugins
Requires: libselinux
#Requires: fcoe-utils >= %{fcoeutilsver}

%description
The anaconda package contains the program which was used to install your system.

%prep
%setup -q

%build
%configure --disable-static --disable-selinux
%{__make} %{?_smp_mflags}

%install
%{__rm} -rf %{buildroot}
%{__make} install DESTDIR=%{buildroot}
find %{buildroot} -type f -name "*.la" | xargs %{__rm}

%ifarch %livearches
desktop-file-install --vendor="" --dir=%{buildroot}%{_datadir}/applications %{buildroot}%{_datadir}/applications/liveinst.desktop
%else
%{__rm} -rf %{buildroot}%{_bindir}/liveinst %{buildroot}%{_sbindir}/liveinst
%endif

%find_lang %{name}

%clean
%{__rm} -rf %{buildroot}

%ifarch %livearches
%post
update-desktop-database &> /dev/null || :
%endif

%ifarch %livearches
%postun
update-desktop-database &> /dev/null || :
%endif

%files -f %{name}.lang
%defattr(-,root,root)
%doc COPYING
%doc docs/command-line.txt
%doc docs/install-methods.txt
%doc docs/mediacheck.txt
/lib/systemd/system/*
/lib/systemd/system-generators/*
/lib/udev/rules.d/70-anaconda.rules
%{_bindir}/instperf
%{_sbindir}/anaconda
%{_sbindir}/logpicker
%ifarch i386 i486 i586 i686 x86_64
%{_sbindir}/gptsync
%{_sbindir}/showpart
%endif
%{_datadir}/anaconda
%{_prefix}/libexec/anaconda
%{_libdir}/python*/site-packages/pyanaconda/*
%{_libdir}/python*/site-packages/log_picker/*
%{_bindir}/analog
%{_bindir}/anaconda-cleanup
%ifarch %livearches
%{_bindir}/liveinst
%{_sbindir}/liveinst
%config(noreplace) %{_sysconfdir}/pam.d/*
%config(noreplace) %{_sysconfdir}/security/console.apps/*
%{_sysconfdir}/X11/xinit/xinitrc.d/*
%{_datadir}/applications/*.desktop
%{_datadir}/icons/hicolor/*
%endif

%dir %{_libdir}/dracut/modules.d/80%{name}
%{_libdir}/dracut/modules.d/80%{name}/*

%changelog
