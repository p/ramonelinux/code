%{!?python_sitelib: %define python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")}

Name:       firstboot
Version:    18.2
Release:    1%{?dist}
Summary:    Initial system configuration utility

Group:      System Environment/Base
License:    GPLv2+
URL:        http://fedoraproject.org/wiki/FirstBoot
Source:     %{name}-%{version}.tar.gz

ExclusiveOS:    Linux
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  gettext
BuildRequires:  python, python-setuptools
#BuildRequires: systemd-units
Requires:       pygtk, python
Requires:       setuptool, libuser-python, system-config-date
Requires:       system-config-users >= 1.2.111-1
Requires:       authconfig-gtk, python-meh
Requires:       system-config-keyboard
Requires:       python-ethtool
Requires:       python-pwquality
Requires(post): systemd-units systemd-sysv chkconfig
Requires(preun): systemd-units
Requires(postun): systemd-units
Requires:       firstboot(windowmanager)
Requires:       libreport-python

%define debug_package %{nil}

Obsoletes: firstboot-tui < 1.90-1

%description
The firstboot utility runs after installation.
It guides the user through a series of steps that allows for easier configuration of the machine.

%prep
%setup -q

%build

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} SITELIB=%{python_sitelib} install
rm %{buildroot}/%{_datadir}/firstboot/modules/additional_cds.py*
%find_lang %{name}

%clean
rm -rf %{buildroot}

%post
if [ $1 -ne 2 -a ! -f /etc/sysconfig/firstboot ]; then
  platform="$(arch)"
  if [ "$platform" = "s390" -o "$platform" = "s390x" ]; then
    echo "RUN_FIRSTBOOT=YES" > /etc/sysconfig/firstboot
  else
    systemctl enable firstboot-graphical.service >/dev/null 2>&1 || :
  fi
fi

%preun
if [ $1 = 0 ]; then
  rm -rf /usr/share/firstboot/*.pyc
  rm -rf /usr/share/firstboot/modules/*.pyc
  /bin/systemctl --no-reload firstboot-graphical.service > /dev/null 2>&1 || :
  /bin/systemctl stop firstboot-graphical.service > /dev/null 2>&1 || :
fi

%postun
/bin/systemctl daemon-reload > /dev/null 2>&1 || :
if [ $1 -ge 1 ] ; then
    /bin/systemctl try-restart firstboot-graphical.service > /dev/null 2>&1 || :
fi

%triggerun -- firstboot < 1.117
%{_bindir}/systemd-sysv-convert --save firstboot > /dev/null 2>&1 ||:
/bin/systemctl enable firstboot-graphical.service > /dev/null 2>&1
/sbin/chkconfig --del firstboot > /dev/null 2>&1 || :
/bin/systemctl try-restart firstboot-graphical.service > /dev/null 2>&1 || :

%files -f %{name}.lang
%defattr(-,root,root,-)
%dir %{_datadir}/firstboot/
%dir %{_datadir}/firstboot/modules/
%dir %{_datadir}/firstboot/themes/
%dir %{_datadir}/firstboot/themes/default
%{python_sitelib}/*
%{_sbindir}/firstboot
%{_datadir}/firstboot/modules/create_user.py*
%{_datadir}/firstboot/modules/date.py*
%{_datadir}/firstboot/modules/eula.py*
%{_datadir}/firstboot/modules/welcome.py*
%{_datadir}/firstboot/themes/default/*
/lib/systemd/system/firstboot-graphical.service
%ifarch s390 s390x
%dir %{_sysconfdir}/profile.d
%{_sysconfdir}/profile.d/firstboot.sh
%{_sysconfdir}/profile.d/firstboot.csh
%endif

%changelog
