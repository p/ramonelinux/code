%{!?python_sitelib: %global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}
%{!?python_sitearch: %global python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")}

Name:		libprelude
Epoch:		1
Version:	1.0.0
Release:	12%{?dist}
Summary:	The prelude library
Group:		System Environment/Libraries
License:	GPLv2+
URL:		http://prelude-ids.org/
Source0:	http://www.prelude-ids.org/download/releases/%{name}/%{name}-%{version}.tar.gz
Patch1:		libprelude-1.0.0-gcc46.patch
Patch2:     libprelude-1.0.0-Fix-building-with-glibc-2.16.6.patch
Patch3:     fix-ltdl-hack.patch
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:	gnutls python lua
BuildRequires:	swig chrpath libgcrypt ruby

%description
Libprelude is a library that guarantees secure connections between
all sensors and the Prelude Manager. Libprelude provides an 
Application Programming Interface (API) for the communication with
Prelude sub-systems, it supplies the necessary functionality for
generating and emitting IDMEF events with Prelude and automates the
saving and re-transmission of data in times of temporary interruption
of one of the components of the system.

%prep
%setup -q
%patch1 -p1
%patch2 -p1
%patch3 -p1

%build
# remove #include <gnutls/extra.h>
sed -i '60s:^:\/\/:' prelude-admin/prelude-admin.c
sed -i '42s:^:\/\/:' prelude-admin/server.c

%configure	--disable-static \
		--with-html-dir=%{_defaultdocdir}/%{name}-%{version}/html \
		--with-ruby=no \
		--with-perl-installdirs=vendor \
		--enable-easy-bindings

# removing rpath
sed -i.rpath -e 's|LD_RUN_PATH=""||' bindings/Makefile.in
sed -i.rpath -e 's|^sys_lib_dlsearch_path_spec="/lib /usr/lib|sys_lib_dlsearch_path_spec="/%{_lib} %{_libdir}|' libtool

make %{?_smp_mflags} 

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{_defaultdocdir}/%{name}-%{version}
mkdir -p %{buildroot}%{perl_vendorarch}
make install DESTDIR=%{buildroot} INSTALL="%{__install} -c -p"
cp -p AUTHORS ChangeLog README NEWS COPYING LICENSE.README HACKING.README \
	%{buildroot}%{_defaultdocdir}/%{name}-%{version}
rm -f %{buildroot}/%{_libdir}/libprelude.la
chmod 755 %{buildroot}%{python_sitearch}/_prelude.so
find %{buildroot} -type f \( -name .packlist -o -name perllocal.pod \) -exec rm -f {} ';'
find %{buildroot} -type f -name '*.bs' -a -size 0 -exec rm -f {} ';'
rm -f %{buildroot}%{_libdir}/*.la
#rm -f %{buildroot}%{ruby_sitearch}/PreludeEasy.la
chmod +w %{buildroot}%{perl_vendorarch}/auto/Prelude/Prelude.so
chrpath -d %{buildroot}%{perl_vendorarch}/auto/Prelude/Prelude.so
chmod -w %{buildroot}%{perl_vendorarch}/auto/Prelude/Prelude.so
chmod +w %{buildroot}%{perl_vendorarch}/auto/PreludeEasy/PreludeEasy.so
chrpath -d %{buildroot}%{perl_vendorarch}/auto/PreludeEasy/PreludeEasy.so
chmod -w %{buildroot}%{perl_vendorarch}/auto/PreludeEasy/PreludeEasy.so

# Fix time stamp for both 32 and 64 bit libraries
touch -r ./configure.in %{buildroot}%{_sysconfdir}/prelude/default/*

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%{_bindir}/prelude-admin
%{_bindir}/prelude-adduser
%{_libdir}/*.so.*
%{_mandir}/man1/prelude-admin.1.gz
%config(noreplace) %{_sysconfdir}/*
%{_localstatedir}/spool/*
%dir %{_defaultdocdir}/%{name}-%{version}/
%doc %{_defaultdocdir}/%{name}-%{version}/*

%{_bindir}/libprelude-config
%{_libdir}/*.so
%{_libdir}/pkgconfig/libprelude.pc
%dir %{_includedir}/libprelude/
%{_includedir}/libprelude/*
%{_datadir}/aclocal/libprelude.m4

%{python_sitearch}/*

%attr(0644,root,root) %{perl_vendorarch}/Prelude*.pm
%{perl_vendorarch}/auto/Prelude*/

%changelog
