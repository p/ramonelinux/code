%{!?python_sitearch: %global python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")}

Name:       btparser
Version:    0.18
Release:    2%{?dist}
Summary:    Parser and analyzer for backtraces produced by GDB

Group:      Development/Libraries
License:    GPLv2+
URL:        http://fedorahosted.org/btparser
Source:     https://fedorahosted.org/released/btparser/btparser-%{version}.tar.xz

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  glib
BuildRequires:  python
BuildRequires:  elfutils
BuildRequires:  binutils
Requires:       elfutils
Requires:       glib
Requires:       binutils

%description
Btparser is a backtrace parser and analyzer, which works with backtraces produced by the GNU Project Debugger.
It can parse a text file with a backtrace to a tree of C structures, allowing to analyze the threads and frames of the backtrace and work with them.

%prep
%setup -q

%build
%configure --disable-static
make %{?_smp_mflags}

%install
make install DESTDIR=%{buildroot}

# Remove all libtool archives (*.la) from modules directory.
find %{buildroot} -regex ".*\.la$" | xargs rm -f --

%check

%files
%doc README NEWS COPYING TODO ChangeLog
%{_includedir}/btparser/*.h
%{_bindir}/btparser
%{_mandir}/man1/%{name}.1.gz
%{_libdir}/lib*.so.*
%{_libdir}/lib*.so
%{_libdir}/pkgconfig/*
%dir %{python_sitearch}/%{name}
%{python_sitearch}/%{name}/*

%changelog
