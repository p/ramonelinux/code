Name:		xmlrpc-c
Version:	1.31.4
Release:    5%{?dist}
Summary:	A lightweight RPC library based on XML and HTTP

Group:		System Environment/Libraries
License:	BSD and MIT
URL:		http://xmlrpc-c.sourceforge.net/
Source:	http://dl.sourceforge.net/sourceforge/xmlrpc-c/%{name}-%{version}.tar.xz
Patch100:	xmlrpc-c-cmake.patch
Patch102:	xmlrpc-c-printf-size_t.patch
Patch105:	xmlrpc-c-longlong.patch
Patch107:	xmlrpc-c-uninit-curl.patch
Patch108:	xmlrpc-c-30x-redirect.patch
Patch109:	xmlrpc-c-check-vasprintf-return-value.patch
Patch110:	xmlrpc-c-include-string_int.h.patch

BuildRoot:	%_tmppath/%name-%version-%release-root
BuildRequires:	cmake
BuildRequires:	curl libxml readline ncurses

%description
XML-RPC is a quick-and-easy way to make procedure calls over the Internet.
It converts the procedure call into XML document, sends it to a remote server using HTTP, and gets back the response as XML.
This library provides a modular implementation of XML-RPC for C.

%prep
%setup -q
%patch100 -p1
%patch102 -p1
%patch105 -p1
%patch107 -p1
%patch108 -p1
%patch109 -p1
%patch110 -p1

%build
mkdir -p build
cd build
export CFLAGS="-O2 -Wno-uninitialized -Wno-unknown-pragmas"
export CXXFLAGS="-O2"
export LDFLAGS="-Wl,-as-needed"
cmake .. \
	-D_lib:STRING=%_lib			\
	-DMUST_BUILD_CURL_CLIENT:BOOL=ON	\
	-DMUST_BUILD_LIBWWW_CLIENT:BOOL=OFF	\
        -DCMAKE_INSTALL_PREFIX:PATH=%_prefix	\
        -DBUILD_SHARED_LIBS:BOOL=ON		\
	-DENABLE_TOOLS:BOOL=ON
make VERBOSE=1 %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
cd build
make install DESTDIR=%{buildroot}

chmod +x %{buildroot}%_libdir/*.so

%post   -p /sbin/ldconfig
%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%{_bindir}/xml-rpc-api2cpp
%{_bindir}/xml-rpc-api2txt
%{_bindir}/xmlrpc
%{_bindir}/xmlrpc-c-config
%{_bindir}/xmlrpc_cpp_proxy
%{_bindir}/xmlrpc_parsecall
%{_bindir}/xmlrpc_transport
%{_includedir}/XmlRpcCpp.h
%{_includedir}/xmlrpc-c/*.h*
%{_includedir}/xmlrpc*.h
%{_libdir}/libxmlrpc++.so*
%{_libdir}/libxmlrpc.so*
%{_libdir}/libxmlrpc_*.so*
%{_libdir}/pkgconfig/xmlrpc*.pc
%{_mandir}/man1/xml-rpc-api2*.1.gz

%clean
rm -rf %{buildroot}

%changelog
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- porting from Fedora
