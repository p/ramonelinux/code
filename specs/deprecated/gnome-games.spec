Name:       gnome-games
Version:    3.6.1
Release:    1%{?dist}
Summary:    GNOME Games

Group:      User Interface/GNOME
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/%{name}/3.6/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  clutter-gtk libcanberra librsvg pygobject yelp-xsl
BuildRequires:  gobject-introspection vala
BuildRequires:  mesa glu intltool gettext xml-parser itstool

%description
The GNOME Games is a collection of simple, but addictive games from the GNOME Desktop project.
They represent many of the popular games and include card games, puzzle games and arcade games.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --localstatedir=/var &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/glchess
%{_bindir}/glines
%{_bindir}/gnect
%{_bindir}/gnibbles
%{_bindir}/gnobots2
%{_bindir}/gnome-mahjongg
%{_bindir}/gnome-sudoku
%{_bindir}/gnomine
%{_bindir}/gnotravex
%{_bindir}/gnotski
%{_bindir}/gtali
%{_bindir}/iagno
%{_bindir}/lightsoff
%{_bindir}/quadrapassel
%{_bindir}/swell-foop
/usr/lib/python2.7/site-packages/gnome_sudoku/*
%{_datadir}/applications/*.desktop
%{_datadir}/glchess/*
%{_datadir}/glib-2.0/schemas/org.gnome.*.gschema.xml
%{_datadir}/glines/*
%{_datadir}/gnect/*
%{_datadir}/gnibbles/*
%{_datadir}/gnobots2/*
%{_datadir}/gnome-mahjongg/*
%{_datadir}/gnome-sudoku/*
%{_datadir}/gnomine/*
%{_datadir}/gnotravex/gnotravex.ui
%{_datadir}/gnotski/gnotski.svg
%{_datadir}/gtali/*.svg
%{_datadir}/iagno/*
%{_datadir}/icons/hicolor/*/apps/*.png
%{_datadir}/icons/hicolor/*/actions/*.png
%{_datadir}/icons/hicolor/scalable/apps/*.svg
%{_datadir}/lightsoff/*.svg
%{_datadir}/locale/*/LC_MESSAGES/gnome-games.mo
%{_datadir}/quadrapassel/sounds/*.ogg
%{_datadir}/swell-foop/*
%attr(755,games,games) %{_localstatedir}/games/*.scores

%files doc
%defattr(-,root,root,-)
%{_datadir}/help/*/glchess/*
%{_datadir}/help/*/glines/*
%{_datadir}/help/*/gnect/*
%{_datadir}/help/*/gnibbles/*
%{_datadir}/help/*/gnobots2/*
%{_datadir}/help/*/gnome-mahjongg/*
%{_datadir}/help/*/gnome-sudoku/*
%{_datadir}/help/*/gnomine/*
%{_datadir}/help/*/gnotravex/*
%{_datadir}/help/*/gnotski/*
%{_datadir}/help/*/gtali/*
%{_datadir}/help/*/iagno/*
%{_datadir}/help/*/lightsoff/*
%{_datadir}/help/*/quadrapassel/*
%{_datadir}/help/*/swell-foop/*
%{_mandir}/man6/*.6.gz

%pre
groupadd -fg 60 games &&
useradd -c 'Games High Score Owner' -d /var/games \
        -g games -s /bin/false -u 60 games

%post
gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
glib-compile-schemas %{_datadir}/glib-2.0/schemas &>/dev/null || :

%postun
userdel games

%clean
rm -rf %{buildroot}

%changelog
* Fri Nov 2 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.0.2 to 3.6.1
* Mon Oct 1 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.4.2 to 3.6.0.2
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
