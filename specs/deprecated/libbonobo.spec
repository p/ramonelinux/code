#
# Ramone Linux 1.0
#

Summary: libbonobo
Name: libbonobo
Version: 2.24.3
Release: 1%{?dist}
License: GPLv2+
Group: System Environment/libraries
Source: http://ftp.gnome.org/pub/gnome/sources/libbonobo/2.24/%{name}-%{version}.tar.bz2
Url: http://ftp.gnome.org
Distribution: ramone linux
Vendor: ramone
Packager: tanggeliang <tanggeliang@gmail.com>

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires: bison, flex, ORBit2, intltool, libxml2, popt, dbus-glib
Requires: perl, ORBit2, glib
AutoReqProv: no

%description

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --sysconfdir=/etc/gnome/2.30.2 \
            --libexecdir=/usr/lib/bonobo-2.0 \
            --mandir=/usr/share/man &&
make

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
/etc/gnome/2.30.2/bonobo-activation/bonobo-activation-config.xml
%{_bindir}/activation-client
%{_bindir}/bonobo-activation-run-query
%{_bindir}/bonobo-slay
%{_bindir}/echo-client-2
%{_includedir}/bonobo-activation-2.0/*
%{_includedir}/libbonobo-2.0/*
%{_libdir}/bonobo/*
%{_libdir}/bonobo-2.0/*
%{_libdir}/libbonobo-2.*
%{_libdir}/libbonobo-activation.*
%{_libdir}/orbit-2.0/*
%{_libdir}/pkgconfig/bonobo-activation-2.0.pc
%{_libdir}/pkgconfig/libbonobo-2.0.pc
/usr/sbin/bonobo-activation-sysconf
%{_datadir}/gtk-doc/*
%{_datadir}/idl/*
%{_datadir}/locale/*
%{_mandir}/*/*

%clean
rm -rf %{buildroot}

%changelog
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
