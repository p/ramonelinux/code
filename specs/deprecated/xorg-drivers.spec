#
# Ramone Linux 1.0
#

Summary: xorg-drivers
Name: xorg-drivers
Version: 7.6.1
Release: 1%{?dist}
License: GPLv2+
Group: System Environment/Xorg
Url: http://xorg.freedesktop.org
Distribution: ramone linux
Vendor: ramone
Packager: tanggeliang <tanggeliang@gmail.com>

Requires: xf86-input-acecad
Requires: xf86-input-aiptek
Requires: xf86-input-evdev
Requires: xf86-input-joystick
Requires: xf86-input-keyboard
Requires: xf86-input-mouse
Requires: xf86-input-synaptics
Requires: xf86-input-void
Requires: xf86-video-apm
Requires: xf86-video-ark
Requires: xf86-video-ast
Requires: xf86-video-ati
Requires: xf86-video-cirrus
Requires: xf86-video-dummy
Requires: xf86-video-fbdev
Requires: xf86-video-glint
Requires: xf86-video-i128
Requires: xf86-video-i740
Requires: xf86-video-intel
Requires: xf86-video-mach64
Requires: xf86-video-mga
Requires: xf86-video-neomagic
Requires: xf86-video-nv
Requires: xf86-video-r128
Requires: xf86-video-rendition
Requires: xf86-video-s3
Requires: xf86-video-s3virge
Requires: xf86-video-savage
Requires: xf86-video-siliconmotion
Requires: xf86-video-sis
Requires: xf86-video-sisusb
Requires: xf86-video-tdfx
Requires: xf86-video-tga
Requires: xf86-video-trident
Requires: xf86-video-tseng
Requires: xf86-video-v4l
Requires: xf86-video-vesa
Requires: xf86-video-voodoo
Requires: xf86-video-xgi

%description

%prep
%build
%check
%install
rm -rf %{buildroot}
mkdir -p %{buildroot}

%files
%defattr(-,root,root,-)

%clean
rm -rf %{buildroot}

%changelog
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
