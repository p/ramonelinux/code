Name:       telepathy-glib
Version:    0.99.11
Release:    1%{?dist}
Summary:    Telepathy GLib

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.freedesktop.org
Source:     http://telepathy.freedesktop.org/releases/telepathy-glib/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  dbus-glib libxslt
BuildRequires:  gobject-introspection vala
BuildRequires:  gtk-doc intltool gettext xml-parser
AutoReq: no

%description
The Telepathy GLib package is a library for GLib based Telepathy components.
Telepathy is a D-Bus framework for unifying real time communication, including instant messaging, voice calls and video calls.
It abstracts differences between protocols to provide a unified interface for applications.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --enable-vala-bindings \
            --disable-static \
            --libexecdir=%{_libdir}/telepathy-glib \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

rm -rf %{buildroot}/%{_datadir}/glib-2.0/schemas/org.freedesktop.Telepathy.Logger.gschema.xml

%files
%defattr(-,root,root,-)
%{_includedir}/telepathy-glib-1-dbus/telepathy-glib/_gen/*.h
%{_includedir}/telepathy-glib-1-dbus/telepathy-glib/*.h
%{_includedir}/telepathy-glib-1/telepathy-glib/_gen/*.h
%{_includedir}/telepathy-glib-1/telepathy-glib/*.h
%{_includedir}/telepathy-logger-1/telepathy-logger/*.h
%{_libdir}/girepository-1.0/Telepathy*-1.typelib
%{_libdir}/libtelepathy-glib-1-dbus.la
%{_libdir}/libtelepathy-glib-1-dbus.so*
%{_libdir}/libtelepathy-glib-1.la
%{_libdir}/libtelepathy-glib-1.so*
%{_libdir}/libtelepathy-logger-1.la
%{_libdir}/libtelepathy-logger-1.so*
%{_libdir}/pkgconfig/telepathy-glib-1-dbus.pc
%{_libdir}/pkgconfig/telepathy-glib-1.pc
%{_libdir}/pkgconfig/telepathy-logger-1.pc
%{_libdir}/telepathy-glib/telepathy-logger-1
%{_datadir}/dbus-1/services/im.telepathy.v1.*.service
%{_datadir}/gir-1.0/Telepathy*-1.gir
%{_datadir}/glib-2.0/schemas/im.telepathy.v1.Logger.gschema.xml
%{_datadir}/telepathy-1/clients/Logger.client
%{_datadir}/vala/vapi/telepathy-glib-1.deps
%{_datadir}/vala/vapi/telepathy-glib-1.vapi

%files doc
%defattr(-,root,root,-)
%{_datadir}/gtk-doc/html/telepathy-glib-1/*
%{_datadir}/gtk-doc/html/telepathy-logger-1/*

%clean
rm -rf %{buildroot}

%changelog
* Sat Jul 19 2014 tanggeliang <tanggeliang@gmail.com>
- update from 0.99.10 to 0.99.11
* Sun Apr 20 2014 tanggeliang <tanggeliang@gmail.com>
- update from 0.99.9 to 0.99.10
* Sat Mar 29 2014 tanggeliang <tanggeliang@gmail.com>
- update from 0.99.8 to 0.99.9
* Thu Mar 20 2014 tanggeliang <tanggeliang@gmail.com>
- update from 0.99.7 to 0.99.8
* Sat Mar 15 2014 tanggeliang <tanggeliang@gmail.com>
- update from 0.21.2 to 0.99.7
* Wed Sep 25 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.21.1 to 0.21.2
* Sat Aug 17 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.20.2 to 0.21.1
* Fri May 10 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.20.0 to 0.20.2
* Sun Oct 14 2012 tanggeliang <tanggeliang@gmail.com>
- update from 0.19.10 to 0.20.0
* Fri Sep 28 2012 tanggeliang <tanggeliang@gmail.com>
- update from 0.18.2 to 0.19.10
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
