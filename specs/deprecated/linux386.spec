%global arch    i386

Name:       linux
Version:    3.15.6
Release:    1%{?dist}
Summary:    kernel

Group:      System Environment/Base
License:    GPLv2+
Url:        http://www.kernel.org
Source0:    http://www.kernel.org/pub/linux/kernel/v3.x/%{name}-%{version}.tar.xz
Source1:    %{name}-%{version}-%{arch}.config

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  kmod
# for hostname command
BuildRequires:  inetutils
BuildRequires:  bc
Requires(post): dracut

%description
The Linux package contains the Linux kernel.

%package        headers
Summary:        Linux API Headers
BuildArch:      noarch

%description    headers
The Linux API Headers (in linux-3.6.tar.xz) expose the kernel's API for use by Glibc.

%package        devel
Summary:        devel

%description    devel

%package        doc
Summary:        Documentation

%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
make mrproper
make headers_check

cp -v %SOURCE1 .config

make %{?_smp_mflags}

%check

%install
make INSTALL_HDR_PATH=%{buildroot}/usr headers_install
make INSTALL_MOD_PATH=%{buildroot} modules_install INSTALL_MOD_STRIP=1

mkdir -p %{buildroot}/boot
cp -v %{_builddir}/linux-%{version}/arch/x86/boot/bzImage \
    %{buildroot}/boot/vmlinux-%{version}-%{release}.%{arch}
cp -v %{_builddir}/linux-%{version}/System.map \
    %{buildroot}/boot/System.map-%{version}-%{release}.%{arch}
cp -v %{_builddir}/linux-%{version}/.config \
    %{buildroot}/boot/config-%{version}-%{release}.%{arch}

install -d %{buildroot}/usr/share/doc/linux-%{version}
cp -r Documentation/* %{buildroot}/usr/share/doc/linux-%{version}

install -v -m755 -d %{buildroot}/etc/modprobe.d
cat > %{buildroot}/etc/modprobe.d/usb.conf << "EOF"
# Begin /etc/modprobe.d/usb.conf

install ohci_hcd /sbin/modprobe ehci_hcd ; /sbin/modprobe -i ohci_hcd ; true
install uhci_hcd /sbin/modprobe ehci_hcd ; /sbin/modprobe -i uhci_hcd ; true

# End /etc/modprobe.d/usb.conf
EOF

####
# And save the headers/makefiles etc for building modules against
#
# This all looks scary, but the end result is supposed to be:
# * all arch relevant include/ files
# * all Makefile/Kconfig files
# * all script/ files

rm -f %{buildroot}/lib/modules/%{version}/build
rm -f %{buildroot}/lib/modules/%{version}/source
mkdir -p %{buildroot}/lib/modules/%{version}/build
(cd %{buildroot}/lib/modules/%{version} ; ln -s build source)
# dirs for additional modules per module-init-tools, kbuild/modules.txt
mkdir -p %{buildroot}/lib/modules/%{version}/extra
mkdir -p %{buildroot}/lib/modules/%{version}/updates
# first copy everything
cp --parents `find  -type f -name "Makefile*" -o -name "Kconfig*"` %{buildroot}/lib/modules/%{version}/build
cp Module.symvers %{buildroot}/lib/modules/%{version}/build
cp System.map %{buildroot}/lib/modules/%{version}/build
if [ -s Module.markers ]; then
  cp Module.markers %{buildroot}/lib/modules/%{version}/build
fi
# then drop all but the needed Makefiles/Kconfig files
rm -rf %{buildroot}/lib/modules/%{version}/build/Documentation
rm -rf %{buildroot}/lib/modules/%{version}/build/scripts
rm -rf %{buildroot}/lib/modules/%{version}/build/include
cp .config %{buildroot}/lib/modules/%{version}/build
cp -a scripts %{buildroot}/lib/modules/%{version}/build

rm -f %{buildroot}/lib/modules/%{version}/build/scripts/*.o
rm -f %{buildroot}/lib/modules/%{version}/build/scripts/*/*.o

if [ -d arch/x86/include ]; then
    cp -a --parents arch/x86/include %{buildroot}/lib/modules/%{version}/build/
fi

cp -a include %{buildroot}/lib/modules/%{version}/build/include

# Make sure the Makefile and version.h have a matching timestamp so that
# external modules can be built
touch -r %{buildroot}/lib/modules/%{version}/build/Makefile %{buildroot}/lib/modules/%{version}/build/include/generated/uapi/linux/version.h

# Copy .config to include/config/auto.conf so "make prepare" is unnecessary.
cp %{buildroot}/lib/modules/%{version}/build/.config %{buildroot}/lib/modules/%{version}/build/include/config/auto.conf

# Move the devel headers out of the root file system
mkdir -p %{buildroot}/usr/src/kernels
mv %{buildroot}/lib/modules/%{version}/build %{buildroot}/usr/src/kernels/%{version}

# This is going to create a broken link during the build, but we don't use
# it after this point.  We need the link to actually point to something
# when kernel-devel is installed, and a relative link doesn't work across
# the F17 UsrMove feature.
ln -sf /usr/src/kernels/%{version} %{buildroot}/lib/modules/%{version}/build

# prune junk from kernel-devel
find %{buildroot}/usr/src/kernels -name ".*.cmd" -exec rm -f {} \;

%files
%defattr(-,root,root,-)
/boot/System.map-%{version}-%{release}.%{arch}
/boot/config-%{version}-%{release}.%{arch}
/boot/vmlinux-%{version}-%{release}.%{arch}
%{_sysconfdir}/modprobe.d/*
/lib/modules/%{version}/build
/lib/modules/%{version}/kernel/*
/lib/modules/%{version}/modules.*
/lib/modules/%{version}/source
/lib/firmware/*

%files headers
%defattr(-,root,root,-)
%{_includedir}/*

%files devel
%defattr(-,root,root,-)
/usr/src/kernels/%{version}/*
/usr/src/kernels/%{version}/.config

%files doc
%defattr(-,root,root,-)
%{_docdir}/linux-%{version}/*

%post
dracut /boot/initrd.img-%{version}-%{release}.%{arch} %{version} -a dmsquash-live

%clean
rm -rf %{buildroot}

%changelog
* Sat Jul 26 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.13.6 to 3.15.6
* Sat Mar 22 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.12.2 to 3.13.6
* Sun Dec 1 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.11.3 to 3.12.2
* Wed Oct 2 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.10.9 to 3.11.3
* Sat Aug 24 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.10.1 to 3.10.9
* Mon Jul 15 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.8.6 to 3.10.1
* Sat Apr 6 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.8.5 to 3.8.6
* Fri Apr 5 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.8.2 to 3.8.5
* Sun Mar 17 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.7.9 to 3.8.2
* Sun Feb 24 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.5 to 3.7.9
* Tue Nov 6 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.2 to 3.6.5
* Sat Nov 3 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.1 to 3.6.2
* Mon Oct 29 2012 tanggeliang <tanggeliang@gmail.com>
- add PAE surport
* Fri Oct 12 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.5.2 to 3.6
- update to 3.6.1
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
