Name:       systemd
Version:    204
Release:    1%{?dist}
Summary:    Udev Extras (from systemd)

Group:      System Environment/Base
License:    GPLv2+
Url:        http://www.freedesktop.org
Source0:    http://www.freedesktop.org/wiki/Software/%{name}/%{name}-%{version}.tar.xz
%ifarch %{ix86}
Source1:    udev-lfs-%{version}-1.tar.bz2
%else %ifarch x86_64
Source1:    udev-lfs-%{version}-1-lib64-ram.tar.bz2
%endif

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build

%description
In 2012, the Udev code distribution was merged with systemd. Systemd is a set of programs that replace the SysVInit package used by LFS and is much more complex. It is not compatible with the LFS bootscripts and has many problems and few advantages for most LFS users.

The procedures below extract libraries and programs from the systemd sources that could not be built in LFS due to library dependency issues.

Unlike any other package in the BLFS book, there is no set version of systemd specified to download. Several version updates to LFS and BLFS means there are probably many different versions of Udev on the platforms that BLFS is being built upon. Therefore, you should download and use the version of systemd your computer currently uses. The BLFS team has no experience updating (or reverting to an older version) the Udev programs "on the fly." To discover the version of Udev your computer currently uses, issue /sbin/udevadm --version.

%package        udev
Summary:        udev
BuildRequires:  acl pciutils usbutils
BuildRequires:  kmod
%description    udev

%package        libudev
Summary:        libudev
%description    libudev

%package        gudev
Summary:        gudev
BuildRequires:  glib gperf gobject-introspection
%description    gudev

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
tar -xf %SOURCE1
make -f udev-lfs-%{version}-1/Makefile.lfs all -j1

%check

%install
rm -rf %{buildroot}
make -f udev-lfs-%{version}-1/Makefile.lfs install-all DESTDIR=%{buildroot}

%files udev
%defattr(-,root,root,-)
%{_sysconfdir}/udev/rules.d/55-lfs.rules
%{_sysconfdir}/udev/rules.d/81-cdrom.rules
%{_sysconfdir}/udev/rules.d/83-cdrom-symlinks.rules
/lib/udev/accelerometer
/lib/udev/ata_id
/lib/udev/cdrom_id
/lib/udev/collect
/lib/udev/findkeyboards
/lib/udev/hwdb.d/20-*.hwdb
/lib/udev/init-net-rules.sh
/lib/udev/keyboard-force-release.sh
/lib/udev/keymap
/lib/udev/keymaps/*
/lib/udev/mtd_probe
/lib/udev/rule_generator.functions
/lib/udev/rules.d/*.rules
/lib/udev/scsi_id
/lib/udev/udevd
/lib/udev/v4l_id
/lib/udev/write_cd_rules
/lib/udev/write_net_rules
/sbin/udevadm
%{_libdir}/pkgconfig/udev.pc

%files libudev
%defattr(-,root,root,-)
/%{_lib}/libudev.so.1*
%{_includedir}/libudev.h
%{_libdir}/libudev.so
%{_libdir}/pkgconfig/libudev.pc

%files gudev
%defattr(-,root,root,-)
%{_includedir}/gudev-1.0/gudev/gudev*.h
/usr/lib/girepository-1.0/GUdev-1.0.typelib
%{_libdir}/libgudev-1.0.so*
%{_libdir}/pkgconfig/gudev-1.0.pc
%{_datadir}/gir-1.0/GUdev-1.0.gir

%files doc
%defattr(-,root,root,-)
%{_datadir}/gtk-doc/html/gudev/*
%{_datadir}/gtk-doc/html/udev/*
%{_docdir}/udev/*
%{_docdir}/udev-204/lfs/*
%{_docdir}/udev-204/libudev*
%{_mandir}/man*/udev*.gz

%post
if [ ! -e /lib/udev/devices ]; then
mkdir -p /lib/udev/devices
fi
mknod -m0666 /lib/udev/devices/null c 1 3

if [ -f /etc/udev/rules.d/70-persistent-net.rules ]; then
    rm -rf /etc/udev/rules.d/70-persistent-net.rules
fi
bash /lib/udev/init-net-rules.sh

%postun
rm -rf /lib/udev/devices/null

%clean
rm -rf %{buildroot}

%changelog
* Sat Jul 6 2013 tanggeliang <tanggeliang@gmail.com>
- update from 197 to 204
* Thu Mar 14 2013 tanggeliang <tanggeliang@gmail.com>
- add subpackage gudev 
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
