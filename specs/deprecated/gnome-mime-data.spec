#
# Ramone Linux 1.0
#

Summary: gnome-mime-data
Name: gnome-mime-data
Version: 2.18.0
Release: 1%{?dist}
License: GPLv2+
Group: System Environment/libraries
Source: http://ftp.gnome.org/pub/gnome/sources/gnome-mime-data/2.18/%{name}-%{version}.tar.bz2
Url: http://ftp.gnome.org
Distribution: ramone linux
Vendor: ramone
Packager: tanggeliang <tanggeliang@gmail.com>

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires: XML-Parser
#Requires:
#AutoReqProv: no

%description

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --sysconfdir=/etc/gnome/2.30.2 \
            --mandir=/usr/share/man &&
make

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
/etc/gnome/2.30.2/gnome-vfs-mime-magic
%{_datadir}/application-registry/*
%{_datadir}/locale/*
%{_datadir}/mime-info/*
%{_datadir}/pkgconfig/gnome-mime-data-2.0.pc

%clean
rm -rf %{buildroot}

%changelog
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
