%{!?python_sitelib: %define python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}
%{!?gtk2_binary_version: %define gtk2_binary_version %(pkg-config  --variable=gtk_binary_version gtk+-2.0)}
%{!?gtk3_binary_version: %define gtk3_binary_version %(pkg-config  --variable=gtk_binary_version gtk+-3.0)}

%define glib_ver %([ -a %{_libdir}/pkgconfig/glib-2.0.pc ] && pkg-config --modversion glib-2.0 | cut -d. -f 1,2 || echo -n "999")
%define gconf2_version 2.12.0
%define dbus_python_version 0.83.0
%define im_chooser_version 1.2.5

Name:       ibus
Version:    1.5.4
Release:    2%{?dist}
Summary:    Intelligent Input Bus for Linux OS
License:    LGPLv2+
Group:      User Interface/Libraries
Url:        http://code.google.com/p/ibus/
Source0:    http://ibus.googlecode.com/files/%{name}-%{version}.tar.gz
Source1:    %{name}-xinput

BuildRoot:  %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  gettext
BuildRequires:  libtool
BuildRequires:  python
BuildRequires:  gtk2
BuildRequires:  gtk+
BuildRequires:  dbus-glib
BuildRequires:  dbus-python
BuildRequires:  desktop-file-utils
BuildRequires:  gtk-doc
BuildRequires:  dconf gconf
BuildRequires:  pygobject
BuildRequires:  intltool
BuildRequires:  iso-codes
BuildRequires:  autoconf automake m4 xml-parser libxslt docbook-xml docbook-xsl libnotify vala
Requires:   pygtk
Requires:   pyxdg
Requires:   iso-codes
Requires:   dbus-python >= %{dbus_python_version}
Requires:   librsvg
Requires:   desktop-file-utils
Requires:   gconf

%define _xinputconf %{_sysconfdir}/X11/xinit/xinput.d/ibus.conf

%description
IBus means Intelligent Input Bus. It is an input framework for Linux OS.

%prep
%setup -q

%build
%configure  --disable-static \
            --enable-gtk2 \
            --enable-gtk3 \
            --enable-xim \
            --disable-gtk-doc \
            --enable-introspection \
            --disable-python-library
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install
rm -f %{buildroot}%{_libdir}/libibus-1.0.la
rm -f %{buildroot}%{_libdir}/gtk-2.0/%{gtk2_binary_version}/immodules/im-ibus.la
rm -f %{buildroot}%{_libdir}/gtk-3.0/%{gtk3_binary_version}/immodules/im-ibus.la

# install xinput config file
install -pm 644 -D %{SOURCE1} %{buildroot}%{_xinputconf}

# install .desktop files
echo "NoDisplay=true" >> %{buildroot}%{_datadir}/applications/ibus-setup.desktop
desktop-file-install --delete-original          \
  --dir %{buildroot}%{_datadir}/applications \
  %{buildroot}%{_datadir}/applications/*

%find_lang %{name}10

%clean
rm -rf %{buildroot}

%post
# recreate icon cache
touch --no-create %{_datadir}/icons/hicolor || :
[ -x %{_bindir}/gtk-update-icon-cache ] && \
  %{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor || :

export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
gconftool-2 --makefile-install-rule %{_sysconfdir}/gconf/schemas/ibus.schemas >& /dev/null || :

%{_bindir}/gtk-query-immodules-3.0 --update-cache

%pre
if [ "$1" -gt 1 ]; then
    export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
    gconftool-2 --makefile-uninstall-rule %{_sysconfdir}/gconf/schemas/ibus.schemas >& /dev/null || :
fi

%preun
if [ "$1" -eq 0 ]; then
    export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
    gconftool-2 --makefile-uninstall-rule %{_sysconfdir}/gconf/schemas/ibus.schemas >& /dev/null || :
fi

%postun
# recreate icon cache
touch --no-create %{_datadir}/icons/hicolor || :
[ -x %{_bindir}/gtk-update-icon-cache ] && \
  %{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor || :

if [ "$1" = "0" ]; then
  %{_sbindir}/alternatives --remove xinputrc %{_xinputconf} || :
  # if alternative was set to manual, reset to auto
  [ -L %{_sysconfdir}/alternatives/xinputrc -a "`readlink %{_sysconfdir}/alternatives/xinputrc`" = "%{_xinputconf}" ] && %{_sbindir}/alternatives --auto xinputrc || :
fi

%{_bindir}/update-gtk-immodules %{_host}
%{_bindir}/gtk-query-immodules-3.0-%{__isa_bits} --update-cache

%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING README
%dir %{_datadir}/ibus/
%{_bindir}/ibus
%{_bindir}/ibus-daemon
%{_bindir}/ibus-setup
%{_datadir}/ibus/*
%{_datadir}/applications/*
%{_datadir}/icons/hicolor/*/apps/*
%{_libexecdir}/ibus-ui-gtk3
%{_libexecdir}/ibus-x11
%{_libexecdir}/ibus-engine-simple
%config %{_xinputconf}
%{_libdir}/libibus-1.0.so.*
%{_libdir}/girepository-1.0/IBus-1.0.typelib
%{_libdir}/gtk-2.0/%{gtk2_binary_version}/immodules/im-ibus.so
%{_libdir}/gtk-3.0/%{gtk3_binary_version}/immodules/im-ibus.so
%{_libdir}/lib*.so
%{_libdir}/pkgconfig/*
%{_includedir}/*
%{_datadir}/gir-1.0/IBus-1.0.gir
%{_datadir}/gtk-doc/html/*
%{_datadir}/locale/*/LC_MESSAGES/ibus10.mo
/etc/dconf/db/ibus.d/00-upstream-settings
/etc/dconf/profile/ibus
%{_libdir}/python2.7/site-packages/gi/overrides/IBus.py
%{_libdir}/python2.7/site-packages/gi/overrides/IBus.pyc
%{_libdir}/python2.7/site-packages/gi/overrides/IBus.pyo
/usr/libexec/ibus-dconf
%{_datadir}/GConf/gsettings/ibus.convert
%{_datadir}/glib-2.0/schemas/org.freedesktop.ibus.gschema.xml
%{_datadir}/bash-completion/completions/ibus.bash
%{_datadir}/vala/vapi/ibus-1.0.deps
%{_datadir}/vala/vapi/ibus-1.0.vapi
%{_mandir}/man1/ibus-daemon.1.gz
%{_mandir}/man1/ibus-setup.1.gz
%{_mandir}/man1/ibus.1.gz

%changelog
