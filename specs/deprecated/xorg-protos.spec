#
# xorg-protos for ramone
#

Summary: xorg-protos
Name: xorg-protos
Version: 7.7.1
Release: 4%{?dist}
License: GPLv2+
Group: System Environment/libraries

Source0: http://xorg.freedesktop.org/releases/individual/proto/bigreqsproto-1.1.2.tar.bz2
Source1: http://xorg.freedesktop.org/releases/individual/proto/compositeproto-0.4.2.tar.bz2
Source2: http://xorg.freedesktop.org/releases/individual/proto/damageproto-1.2.1.tar.bz2
Source3: http://xorg.freedesktop.org/releases/individual/proto/dmxproto-2.3.1.tar.bz2
Source4: http://xorg.freedesktop.org/releases/individual/proto/dri2proto-2.6.tar.bz2
Source5: http://xorg.freedesktop.org/releases/individual/proto/fixesproto-5.0.tar.bz2
Source6: http://xorg.freedesktop.org/releases/individual/proto/fontsproto-2.1.2.tar.bz2
Source7: http://xorg.freedesktop.org/releases/individual/proto/glproto-1.4.15.tar.bz2
Source8: http://xorg.freedesktop.org/releases/individual/proto/inputproto-2.2.tar.bz2
Source9: http://xorg.freedesktop.org/releases/individual/proto/kbproto-1.0.6.tar.bz2
Source10: http://xorg.freedesktop.org/releases/individual/proto/printproto-1.0.5.tar.bz2
Source11: http://xorg.freedesktop.org/releases/individual/proto/randrproto-1.3.2.tar.bz2
Source12: http://xorg.freedesktop.org/releases/individual/proto/recordproto-1.14.2.tar.bz2
Source13: http://xorg.freedesktop.org/releases/individual/proto/renderproto-0.11.1.tar.bz2
Source14: http://xorg.freedesktop.org/releases/individual/proto/resourceproto-1.2.0.tar.bz2
Source15: http://xorg.freedesktop.org/releases/individual/proto/scrnsaverproto-1.2.2.tar.bz2
Source16: http://xorg.freedesktop.org/releases/individual/proto/videoproto-2.3.1.tar.bz2
Source17: http://xorg.freedesktop.org/releases/individual/proto/xcmiscproto-1.2.2.tar.bz2
Source18: http://xorg.freedesktop.org/releases/individual/proto/xextproto-7.2.1.tar.bz2
Source19: http://xorg.freedesktop.org/releases/individual/proto/xf86bigfontproto-1.2.0.tar.bz2
Source20: http://xorg.freedesktop.org/releases/individual/proto/xf86dgaproto-2.1.tar.bz2
Source21: http://xorg.freedesktop.org/releases/individual/proto/xf86driproto-2.1.1.tar.bz2
Source22: http://xorg.freedesktop.org/releases/individual/proto/xf86vidmodeproto-2.3.1.tar.bz2
Source23: http://xorg.freedesktop.org/releases/individual/proto/xineramaproto-1.2.1.tar.bz2
Source24: http://xorg.freedesktop.org/releases/individual/proto/xproto-7.0.23.tar.bz2
Source25: http://xorg.freedesktop.org/releases/individual/proto/applewmproto-1.4.2.tar.bz2
Source26: http://xorg.freedesktop.org/releases/individual/proto/windowswmproto-1.0.4.tar.bz2

Url: http://xorg.freedesktop.org
Distribution: ramone linux
Vendor: ramone
Packager: tanggeliang <tanggeliang@gmail.com>

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires: pkg-config, util-macros
BuildRequires: libxslt

%description

%prep
%setup -q -c %{name}-%{version} -a0 -a1 -a2 -a3 -a4 -a5 -a6 -a7 -a8 -a9 -a10 -a11 -a12 -a13 -a14 -a15 -a16 -a17 -a18 -a19 -a20 -a21 -a22 -a23 -a24 -a25 -a26

%build
for dir in $(ls -1)
do
    pushd $dir
    ./configure --prefix=/usr --sysconfdir=/etc \
                --mandir=/usr/share/man --localstatedir=/var
    popd
done

%check

%install
rm -rf %{buildroot}
for dir in $(ls -1) ; do
    pushd $dir
    make install DESTDIR=%{buildroot}
    popd
done

%files
%defattr(-,root,root,-)
%{_includedir}/GL/*
%{_includedir}/X11/*
%{_libdir}/pkgconfig/bigreqsproto.pc
%{_libdir}/pkgconfig/compositeproto.pc
%{_libdir}/pkgconfig/damageproto.pc
%{_libdir}/pkgconfig/dmxproto.pc
%{_libdir}/pkgconfig/dri2proto.pc
%{_libdir}/pkgconfig/fixesproto.pc
%{_libdir}/pkgconfig/fontsproto.pc
%{_libdir}/pkgconfig/glproto.pc
%{_libdir}/pkgconfig/inputproto.pc
%{_libdir}/pkgconfig/kbproto.pc
%{_libdir}/pkgconfig/printproto.pc
%{_libdir}/pkgconfig/randrproto.pc
%{_libdir}/pkgconfig/recordproto.pc
%{_libdir}/pkgconfig/renderproto.pc
%{_libdir}/pkgconfig/resourceproto.pc
%{_libdir}/pkgconfig/scrnsaverproto.pc
%{_libdir}/pkgconfig/videoproto.pc
%{_libdir}/pkgconfig/xcmiscproto.pc
%{_libdir}/pkgconfig/xextproto.pc
%{_libdir}/pkgconfig/xf86bigfontproto.pc
%{_libdir}/pkgconfig/xf86dgaproto.pc
%{_libdir}/pkgconfig/xf86driproto.pc
%{_libdir}/pkgconfig/xf86vidmodeproto.pc
%{_libdir}/pkgconfig/xineramaproto.pc
%{_libdir}/pkgconfig/xproto.pc
%{_libdir}/pkgconfig/applewmproto.pc
%{_libdir}/pkgconfig/windowswmproto.pc
%{_docdir}/*
%{_mandir}/*

%clean
rm -rf %{buildroot}

%changelog
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
