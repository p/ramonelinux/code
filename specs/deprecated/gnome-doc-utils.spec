Name:       gnome-doc-utils
Version:    0.20.10
Release:    6%{?dist}
Summary:    GNOME Doc Utils

Group:      User Interface/GNOME
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/%{name}/0.20/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  intltool libxslt which libxml
BuildRequires:  rarian
BuildRequires:  gettext xml-parser

%description
The GNOME Doc Utils package is a collection of documentation utilities for the GNOME project.
Notably, it contains utilities for building documentation and all auxiliary files in your source tree, and it contains the DocBook XSLT stylesheets that were once distributed with Yelp.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/gnome-doc-prepare
%{_bindir}/gnome-doc-tool
%{_bindir}/xml2po
%{_datadir}/aclocal/*
%{_datadir}/gnome/*
%{_datadir}/gnome-doc-utils/*
%{_datadir}/locale/*
/usr/lib/python2.7/site-packages/xml2po/*
%{_mandir}/*/*
%{_datadir}/pkgconfig/gnome-doc-utils.pc
%{_datadir}/pkgconfig/xml2po.pc
%{_datadir}/xml/*

%clean
rm -rf %{buildroot}

%changelog
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
