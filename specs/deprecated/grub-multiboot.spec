%define _platform i386-multiboot

Name:       grub-multiboot
Version:    2.02
Release:    1%{?dist}
Summary:    GRUB

Group:      System Environment/Base
License:    GPLv2+
Url:        http://www.gnu.org
Source:     ftp://alpha.gnu.org/gnu/grub/grub-%{version}~beta2.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  bison flex

%description
The GRUB package contains the GRand Unified Bootloader.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n grub-%{version}~beta2

%build
./configure --prefix=/usr          \
            --sbindir=/sbin        \
            --sysconfdir=/etc      \
            --disable-grub-emu-usb \
            --disable-efiemu       \
            --disable-werror       \
            --with-platform=multiboot    \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

rm -f %{buildroot}/%{_infodir}/dir

%files
%defattr(-,root,root,-)
%{_sysconfdir}/bash_completion.d/grub
%{_sysconfdir}/grub.d/00_header
%{_sysconfdir}/grub.d/10_linux
%{_sysconfdir}/grub.d/20_linux_xen
%{_sysconfdir}/grub.d/30_os-prober
%{_sysconfdir}/grub.d/40_custom
%{_sysconfdir}/grub.d/41_custom
%{_sysconfdir}/grub.d/README
%{_bindir}/grub-editenv
%{_bindir}/grub-file
%{_bindir}/grub-fstest
%{_bindir}/grub-glue-efi
%{_bindir}/grub-kbdcomp
%{_bindir}/grub-menulst2cfg
%{_bindir}/grub-mkimage
%{_bindir}/grub-mklayout
%{_bindir}/grub-mknetdir
%{_bindir}/grub-mkpasswd-pbkdf2
%{_bindir}/grub-mkrelpath
%{_bindir}/grub-mkrescue
%{_bindir}/grub-mkstandalone
%{_bindir}/grub-render-label
%{_bindir}/grub-script-check
%{_bindir}/grub-syslinux2cfg
/sbin/grub-bios-setup
/sbin/grub-install
/sbin/grub-macbless
/sbin/grub-mkconfig
/sbin/grub-ofpathname
/sbin/grub-probe
/sbin/grub-reboot
/sbin/grub-set-default
/sbin/grub-sparc64-setup
%{_libdir}/grub/%{_platform}/*.img
%{_libdir}/grub/%{_platform}/*.lst
%{_libdir}/grub/%{_platform}/*.mod
%{_libdir}/grub/%{_platform}/*.module
%{_libdir}/grub/%{_platform}/config.h
%{_libdir}/grub/%{_platform}/gdb_grub
%{_libdir}/grub/%{_platform}/gmodule.pl
%{_libdir}/grub/%{_platform}/kernel.exec
%{_libdir}/grub/%{_platform}/modinfo.sh
%{_datadir}/grub/grub-mkconfig_lib
%{_datadir}/locale/*/LC_MESSAGES/grub.mo

%files doc
%defattr(-,root,root,-)
%{_infodir}/grub-dev.info.gz
%{_infodir}/grub.info.gz

%clean
rm -rf %{buildroot}

%changelog
* Tue Oct 14 2014 tanggeliang <tanggeliang@gmail.com>
- update from 2.00 to 2.02
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
