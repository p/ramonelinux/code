Name:       gnome-search-tool
Version:    3.6.0
Release:    5%{?dist}
Summary:    GNOME Search Tool

Group:      User Interface/GNOME
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/%{name}/3.6/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  gtk+ yelp-xsl
BuildRequires:  intltool gettext xml-parser libxslt docbook-xml libsm itstool

%description
The GNOME Search Tool package is an utility used for finding files on your system.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/gnome-search-tool
%{_datadir}/GConf/gsettings/gnome-search-tool.convert
%{_datadir}/applications/gnome-search-tool.desktop
%{_datadir}/glib-2.0/schemas/org.gnome.gnome-search-tool.gschema.xml
%{_datadir}/locale/*/LC_MESSAGES/gnome-search-tool.mo
%{_datadir}/pixmaps/gsearchtool/thumbnail_frame.png

%files doc
%defattr(-,root,root,-)
%{_datadir}/help/*/gnome-search-tool/*
%{_mandir}/man1/gnome-search-tool.1.gz

%post
glib-compile-schemas %{_datadir}/glib-2.0/schemas &>/dev/null || :

%clean
rm -rf %{buildroot}

%changelog
* Mon Oct 1 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.4.0 to 3.6.0
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
