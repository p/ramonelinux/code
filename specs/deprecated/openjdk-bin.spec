Name:       openjdk
Version:    1.7.0.40
Release:    1%{?dist}
Summary:    OpenJDK and IcedTea

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.icedtea.org
%ifarch %{ix86}
Source:     http://icedtea.classpath.org/download/source/OpenJDK-1.7.0.40-i686-bin.tar.xz
%else %ifarch x86_64
Source:     http://icedtea.classpath.org/download/source/OpenJDK-1.7.0.40-x86_64-bin.tar.xz
%endif
Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build

%description

%prep
%ifarch %{ix86}
%setup -q -n OpenJDK-1.7.0.40-2.4.1-i686-bin
%else %ifarch x86_64
%setup -q -n OpenJDK-1.7.0.40-2.4.1-x86_64-bin
%endif

%build

%check

%install
rm -rf %{buildroot}
mkdir -pv %{buildroot}/%{_datadir}/OpenJDK-%{version}
cp -a * %{buildroot}/%{_datadir}/OpenJDK-%{version}

ln -v -nsf OpenJDK-%{version} %{buildroot}/%{_datadir}/jdk

mkdir -pv %{buildroot}/etc/profile.d
cat > %{buildroot}/etc/profile.d/openjdk.sh << "EOF"
# Begin /etc/profile.d/openjdk.sh

# Set JAVA_HOME directory
JAVA_HOME=/usr/share/jdk

# Adjust PATH
pathappend $JAVA_HOME/bin PATH

# Auto Java CLASSPATH
# Copy jar files to, or create symlinks in this directory

AUTO_CLASSPATH_DIR=/usr/share/java

pathprepend . CLASSPATH

for dir in `find ${AUTO_CLASSPATH_DIR} -type d 2>/dev/null`; do
    pathappend $dir CLASSPATH
done

for jar in `find ${AUTO_CLASSPATH_DIR} -name "*.jar" 2>/dev/null`; do
    pathappend $jar CLASSPATH
done

export JAVA_HOME CLASSPATH
unset AUTO_CLASSPATH_DIR dir jar

# End /etc/profile.d/openjdk.sh
EOF

%files
%defattr(-,root,root,-)
/etc/profile.d/openjdk.sh
%{_datadir}/OpenJDK-%{version}/*
%{_datadir}/jdk

%clean
rm -rf %{buildroot}

%changelog
* Wed Jul 24 2013 tanggeliang <tanggeliang@gmail.com>
- create
