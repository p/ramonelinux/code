#
# xorg-app for ramone
#

Summary: xorg-apps
Name: xorg-apps
Version: 7.7.1
Release: 4%{?dist}
License: GPLv2+
Group: System Environment/libraries

Source0:  http://xorg.freedesktop.org/releases/individual/app/bdftopcf-1.0.3.tar.bz2
Source1:  http://xorg.freedesktop.org/releases/individual/app/iceauth-1.0.5.tar.bz2
Source2:  http://xorg.freedesktop.org/releases/individual/app/luit-1.1.1.tar.bz2
Source3:  http://xorg.freedesktop.org/releases/individual/app/mkfontdir-1.0.7.tar.bz2
Source4:  http://xorg.freedesktop.org/releases/individual/app/mkfontscale-1.1.0.tar.bz2
Source5:  http://xorg.freedesktop.org/releases/individual/app/sessreg-1.0.7.tar.bz2
Source6:  http://xorg.freedesktop.org/releases/individual/app/setxkbmap-1.3.0.tar.bz2
Source7:  http://xorg.freedesktop.org/releases/individual/app/smproxy-1.0.5.tar.bz2
Source8: http://xorg.freedesktop.org/releases/individual/app/x11perf-1.5.4.tar.bz2
Source9: http://xorg.freedesktop.org/releases/individual/app/xauth-1.0.7.tar.bz2
Source10: http://xorg.freedesktop.org/releases/individual/app/xbacklight-1.1.2.tar.bz2
Source11: http://xorg.freedesktop.org/releases/individual/app/xcmsdb-1.0.4.tar.bz2
Source12: http://xorg.freedesktop.org/releases/individual/app/xcursorgen-1.0.5.tar.bz2
Source13: http://xorg.freedesktop.org/releases/individual/app/xdpyinfo-1.3.0.tar.bz2
Source14: http://xorg.freedesktop.org/releases/individual/app/xdriinfo-1.0.4.tar.bz2
Source15: http://xorg.freedesktop.org/releases/individual/app/xev-1.2.0.tar.bz2
Source16: http://xorg.freedesktop.org/releases/individual/app/xgamma-1.0.5.tar.bz2
Source17: http://xorg.freedesktop.org/releases/individual/app/xhost-1.0.5.tar.bz2
Source18: http://xorg.freedesktop.org/releases/individual/app/xinput-1.6.0.tar.bz2
Source19: http://xorg.freedesktop.org/releases/individual/app/xkbcomp-1.2.4.tar.bz2
Source20: http://xorg.freedesktop.org/releases/individual/app/xkbevd-1.1.3.tar.bz2
Source21: http://xorg.freedesktop.org/releases/individual/app/xkbutils-1.0.3.tar.bz2
Source22: http://xorg.freedesktop.org/releases/individual/app/xkill-1.0.3.tar.bz2
Source23: http://xorg.freedesktop.org/releases/individual/app/xlsatoms-1.1.1.tar.bz2
Source24: http://xorg.freedesktop.org/releases/individual/app/xlsclients-1.1.2.tar.bz2
Source25: http://xorg.freedesktop.org/releases/individual/app/xmodmap-1.0.7.tar.bz2
Source26: http://xorg.freedesktop.org/releases/individual/app/xpr-1.0.4.tar.bz2
Source27: http://xorg.freedesktop.org/releases/individual/app/xprop-1.2.1.tar.bz2
Source28: http://xorg.freedesktop.org/releases/individual/app/xrandr-1.3.5.tar.bz2
Source29: http://xorg.freedesktop.org/releases/individual/app/xrdb-1.0.9.tar.bz2
Source30: http://xorg.freedesktop.org/releases/individual/app/xrefresh-1.0.4.tar.bz2
Source31: http://xorg.freedesktop.org/releases/individual/app/xset-1.2.2.tar.bz2
Source32: http://xorg.freedesktop.org/releases/individual/app/xsetroot-1.1.0.tar.bz2
Source33: http://xorg.freedesktop.org/releases/individual/app/xvinfo-1.1.1.tar.bz2
Source34: http://xorg.freedesktop.org/releases/individual/app/xwd-1.0.5.tar.bz2
Source35: http://xorg.freedesktop.org/releases/individual/app/xwininfo-1.1.2.tar.bz2
Source36: http://xorg.freedesktop.org/releases/individual/app/xwud-1.0.4.tar.bz2

Url: http://xorg.freedesktop.org
Distribution: ramone linux
Vendor: ramone
Packager: tanggeliang <tanggeliang@gmail.com>

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires: libpng, xcb-util, MesaLib, xorg-libs
BuildRequires: xbitmaps
BuildRequires: util-macros bison flex

%description

%prep
%setup -q -c %{name}-%{version} -a0 -a1 -a2 -a3 -a4 -a5 -a6 -a7 -a8 -a9 -a10 -a11 -a12 -a13 -a14 -a15 -a16 -a17 -a18 -a19 -a20 -a21 -a22 -a23 -a24 -a25 -a26 -a27 -a28 -a29 -a30 -a31 -a32 -a33 -a34 -a35 -a36

%build
{
for app in * ; do
    pushd $app
    ./configure --prefix=/usr --sysconfdir=/etc \
                --mandir=/usr/share/man --localstatedir=/var
    make %{?_smp_mflags}
    popd
done
}

%check

%install
rm -rf %{buildroot}
{
for app in * ; do
    pushd $app
    make install DESTDIR=%{buildroot}
    popd
done
}

%files
%defattr(-,root,root,-)
%{_bindir}/bdftopcf
%{_bindir}/iceauth
%{_bindir}/luit
%{_bindir}/mkfontdir
%{_bindir}/mkfontscale
%{_bindir}/sessreg
%{_bindir}/setxkbmap
%{_bindir}/smproxy
%{_bindir}/x11perf
%{_bindir}/x11perfcomp
%{_bindir}/xauth
%{_bindir}/xcmsdb
%{_bindir}/xcursorgen
%{_bindir}/xdpr
%{_bindir}/xdpyinfo
%{_bindir}/xdriinfo
%{_bindir}/xev
%{_bindir}/xgamma
%{_bindir}/xhost
%{_bindir}/xkbbell
%{_bindir}/xkbcomp
%{_bindir}/xkbevd
%{_bindir}/xkbvleds
%{_bindir}/xkbwatch
%{_bindir}/xkill
%{_bindir}/xlsatoms
%{_bindir}/xlsclients
%{_bindir}/xmodmap
%{_bindir}/xpr
%{_bindir}/xprop
%{_bindir}/xrandr
%{_bindir}/xrdb
%{_bindir}/xrefresh
%{_bindir}/xset
%{_bindir}/xsetroot
%{_bindir}/xvinfo
%{_bindir}/xwd
%{_bindir}/xwininfo
%{_bindir}/xwud
%{_bindir}/xbacklight
%{_bindir}/xinput
%{_bindir}/xkeystone
%{_libdir}/X11/x11perfcomp/*
%{_libdir}/pkgconfig/xkbcomp.pc
%{_mandir}/*/*

%clean
rm -rf %{buildroot}

%changelog
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
