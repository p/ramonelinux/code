#
# Ramone Linux 1.0
#

Summary: ORBit2
Name: ORBit2
Version: 2.14.19
Release: 1%{?dist}
License: GPLv2+
Group: System Environment/libraries
Source: http://ftp.gnome.org/pub/gnome/sources/ORBit2/2.14/%{name}-%{version}.tar.bz2
Url: http://ftp.gnome.org
Distribution: ramone linux
Vendor: ramone
Packager: tanggeliang <tanggeliang@gmail.com>

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires: libIDL, glib
Requires: libIDL, glib
AutoReqProv: no

%description

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --sysconfdir=/etc/gnome/2.30.2 &&
make

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/ior-decode-2
%{_bindir}/linc-cleanup-sockets
%{_bindir}/orbit2-config
%{_bindir}/orbit-idl-2
%{_bindir}/typelib-dump
%{_includedir}/orbit-2.0/*
%{_libdir}/libname-server-2.a
%{_libdir}/libORBit-2.*
%{_libdir}/libORBitCosNaming-2.*
%{_libdir}/libORBit-imodule-2.*
%{_libdir}/orbit-2.0/Everything_module.*
%{_libdir}/pkgconfig/ORBit-*.pc
%{_datadir}/aclocal/*
%{_datadir}/gtk-doc/*
%{_datadir}/idl/*

%clean
rm -rf %{buildroot}

%changelog
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
