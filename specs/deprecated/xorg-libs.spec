#
# Ramone Linux 1.0
#

Summary: xorg-libs
Name: xorg-libs
Version: 7.7.1
Release: 2%{?dist}
License: GPLv2+
Group: System Environment/Xorg
Url: http://xorg.freedesktop.org
Distribution: ramone linux
Vendor: ramone
Packager: tanggeliang <tanggeliang@gmail.com>

Requires: xtrans
Requires: libX11
Requires: libXext
Requires: libFS
Requires: libICE
Requires: libSM
Requires: libXScrnSaver
Requires: libXt
Requires: libXmu
Requires: libXp
Requires: libXpm
Requires: libXau
Requires: libXaw
Requires: libXfixes
Requires: libXcomposite
Requires: libXrender
Requires: libXcursor
Requires: libXdamage
Requires: libXdmcp
Requires: libfontenc
Requires: libXfont
Requires: libXft
Requires: libXi
Requires: libXinerama
Requires: libXrandr
Requires: libXres
Requires: libXtst
Requires: libXv
Requires: libXvMC
Requires: libXxf86dga
Requires: libXxf86vm
Requires: libdmx
Requires: libpciaccess
Requires: libxkbfile

%description

%prep
%build
%check
%install
rm -rf %{buildroot}
mkdir -p %{buildroot}

%files
%defattr(-,root,root,-)

%clean
rm -rf %{buildroot}

%changelog
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
