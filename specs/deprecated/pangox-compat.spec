Name:       pangox-compat
Version:    0.0.2
Release:    1%{?dist}
Summary:    Pangox Compat

Group:      System Environment/xlib
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/pangox-compat/0.0/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  pango

%description

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --sysconfdir=/etc \
            --disable-static &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
/etc/pango/pangox.aliases
%{_includedir}/pango-1.0/pango/pangox.h
%{_libdir}/libpangox-1.0.la
%{_libdir}/libpangox-1.0.so*
%{_libdir}/pkgconfig/pangox.pc

%clean
rm -rf %{buildroot}

%changelog
* Sun Mar 17 2013 tanggeliang <tanggeliang@gmail.com>
- create
