Summary: A library for handling different graphics file formats
Name: netpbm
Version: 10.59.01
Release: 3%{?dist}
# See copyright_summary for details
License: BSD and GPLv2 and IJG and MIT and Public Domain
Group: System Environment/Libraries
URL: http://netpbm.sourceforge.net/
# Source0 is prepared by
# svn checkout https://netpbm.svn.sourceforge.net/svnroot/netpbm/advanced netpbm-%{version}
# svn checkout https://netpbm.svn.sourceforge.net/svnroot/netpbm/userguide netpbm-%{version}/userguide
# and removing the .svn directories ( find -name "\.svn" -type d -print0 | xargs -0 rm -rf )
# and removing the ppmtompeg code, due to patents ( rm -rf netpbm-%{version}/converter/ppm/ppmtompeg/ )
Source0: netpbm-%{version}.tar.xz
Patch1: netpbm-time.patch
Patch2: netpbm-message.patch
Patch3: netpbm-security-scripts.patch
Patch4: netpbm-security-code.patch
Patch5: netpbm-nodoc.patch
Patch6: netpbm-gcc4.patch
Patch7: netpbm-bmptopnm.patch
Patch8: netpbm-CAN-2005-2471.patch
Patch9: netpbm-xwdfix.patch
Patch11: netpbm-multilib.patch
Patch12: netpbm-pamscale.patch
Patch13: netpbm-glibc.patch
Patch15: netpbm-docfix.patch
Patch16: netpbm-ppmfadeusage.patch
Patch17: netpbm-fiasco-overflow.patch
Patch20: netpbm-noppmtompeg.patch
Patch21: netpbm-cmuwtopbm.patch
Patch22: netpbm-pamtojpeg2k.patch
Patch23: netpbm-manfix.patch
Patch24: netpbm-ppmtopict.patch
Patch25: netpbm-pnmtopclxl.patch
BuildRequires: libjpeg-turbo, libpng, libtiff, flex, m4
BuildRequires: libx11, python, jasper, libxml

%description
The netpbm package contains a library of functions which support
programs for handling various graphics file formats, including .pbm
(portable bitmaps), .pgm (portable graymaps), .pnm (portable anymaps),
.ppm (portable pixmaps) and others.

%prep
%setup -q
%patch1 -p1 -b .time
%patch2 -p1 -b .message
%patch3 -p1 -b .security-scripts
%patch4 -p1 -b .security-code
%patch5 -p1 -b .nodoc
%patch6 -p1 -b .gcc4
%patch7 -p1 -b .bmptopnm
%patch8 -p1 -b .CAN-2005-2471
%patch9 -p1 -b .xwdfix
%patch11 -p1 -b .multilib
%patch13 -p1 -b .glibc
%patch15 -p1
%patch16 -p1 -b .ppmfadeusage
%patch17 -p1 -b .fiasco-overflow
%patch20 -p1 -b .noppmtompeg
%patch21 -p1 -b .cmuwtopbmfix
%patch22 -p1 -b .pamtojpeg2kfix
%patch23 -p1 -b .manfix
%patch24 -p1 -b .ppmtopict
%patch25 -p1 -b .pnmtopclxl

sed -i 's/STRIPFLAG = -s/STRIPFLAG =/g' config.mk.in
rm -rf converter/other/jpeg2000/libjasper/
sed -i -e 's/^SUBDIRS = libjasper/SUBDIRS =/' converter/other/jpeg2000/Makefile

%build
./configure <<EOF



















EOF

TOP=`pwd`

make \
	CC="%{__cc}" \
	LDFLAGS="-L$TOP/pbm -L$TOP/pgm -L$TOP/pnm -L$TOP/ppm" \
	CFLAGS="$RPM_OPT_FLAGS -fPIC -flax-vector-conversions -fno-strict-aliasing" \
	LADD="-lm" \
	JPEGINC_DIR=%{_includedir} \
	PNGINC_DIR=%{_includedir} \
	TIFFINC_DIR=%{_includedir} \
	JPEGLIB_DIR=%{_libdir} \
	PNGLIB_DIR=%{_libdir} \
	TIFFLIB_DIR=%{_libdir} \
	LINUXSVGALIB="NONE" \
	X11LIB=%{_libdir}/libX11.so \
	XML2LIBS="NONE" \
	JASPERLIB="" \
	JASPERDEPLIBS="-ljasper" \
	JASPERHDR_DIR="/usr/include/jasper"

# prepare man files
cd userguide
for i in *.html ; do
  ../buildtools/makeman ${i}
done
for i in 1 3 5 ; do
  mkdir -p man/man${i}
  mv *.${i} man/man${i}
done


%install
rm -rf $RPM_BUILD_ROOT

mkdir -p $RPM_BUILD_ROOT
make package pkgdir=$RPM_BUILD_ROOT/usr LINUXSVGALIB="NONE" XML2LIBS="NONE"

# Ugly hack to have libs in correct dir on 64bit archs.
mkdir -p $RPM_BUILD_ROOT%{_libdir}
if [ "%{_libdir}" != "/usr/lib" ]; then
  mv $RPM_BUILD_ROOT/usr/lib/lib* $RPM_BUILD_ROOT%{_libdir}
fi

cp -af lib/libnetpbm.a $RPM_BUILD_ROOT%{_libdir}/libnetpbm.a
cp -l $RPM_BUILD_ROOT%{_libdir}/libnetpbm.so.?? $RPM_BUILD_ROOT%{_libdir}/libnetpbm.so

mkdir -p $RPM_BUILD_ROOT%{_datadir}
mv userguide/man $RPM_BUILD_ROOT%{_mandir}

# Get rid of the useless non-ascii character in pgmminkowski.1
sed -i 's/\xa0//' $RPM_BUILD_ROOT%{_mandir}/man1/pgmminkowski.1

# Don't ship man pages for non-existent binaries and bogus ones
for i in hpcdtoppm \
	 ppmsvgalib vidtoppm picttoppm \
	 directory error extendedopacity \
	 pam pbm pgm pnm ppm index libnetpbm_dir \
	 liberror ppmtotga; do
	rm -f $RPM_BUILD_ROOT%{_mandir}/man1/${i}.1
done
rm -f $RPM_BUILD_ROOT%{_mandir}/man5/extendedopacity.5

mkdir -p $RPM_BUILD_ROOT%{_datadir}/netpbm
mv $RPM_BUILD_ROOT/usr/misc/*.map $RPM_BUILD_ROOT%{_datadir}/netpbm/
mv $RPM_BUILD_ROOT/usr/misc/rgb.txt $RPM_BUILD_ROOT%{_datadir}/netpbm/
rm -rf $RPM_BUILD_ROOT/usr/README
rm -rf $RPM_BUILD_ROOT/usr/VERSION
rm -rf $RPM_BUILD_ROOT/usr/link
rm -rf $RPM_BUILD_ROOT/usr/misc
rm -rf $RPM_BUILD_ROOT/usr/man
rm -rf $RPM_BUILD_ROOT/usr/pkginfo
rm -rf $RPM_BUILD_ROOT/usr/config_template

# Don't ship the static library
rm -f $RPM_BUILD_ROOT/%{_libdir}/lib*.a

# remove/symlink/substitute obsolete utilities
pushd $RPM_BUILD_ROOT%{_bindir}
rm -f pgmtopbm pnmcomp
ln -s pamcomp pnmcomp
echo -e '#!/bin/sh\npamditherbw $@ | pamtopnm\n' > pgmtopbm
chmod 0755 pgmtopbm
popd


%clean
rm -rf $RPM_BUILD_ROOT

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root)
%doc doc/copyright_summary doc/COPYRIGHT.PATENT doc/GPL_LICENSE.txt doc/HISTORY README
%{_libdir}/lib*.so.*

%dir %{_includedir}/netpbm
%{_includedir}/netpbm/*.h
%{_libdir}/lib*.so
%{_mandir}/man3/*

%{_bindir}/*
%{_mandir}/man1/*
%{_mandir}/man5/*
%{_datadir}/netpbm/

%doc userguide/*

%changelog
