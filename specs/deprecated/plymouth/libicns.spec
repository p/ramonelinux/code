Name:       libicns
Version:    0.8.1
Release:    6%{?dist}
Summary:    Library for manipulating Macintosh icns files

Group:      System Environment/Libraries
License:    LGPLv2+ and GPLv2+
URL:        http://icns.sourceforge.net/
Source:     http://downloads.sourceforge.net/icns/%{name}-%{version}.tar.gz

BuildRoot:  %{_tmppath}/%{name}-%{version}-%{release}-build

BuildRequires:  libpng jasper

%description
libicns is a library providing functionality for easily reading and writing Macintosh icns files.

%prep
%setup -q

%build
%configure --disable-static
# disable rpaths
sed -i 's|^hardcode_libdir_flag_spec=.*|hardcode_libdir_flag_spec=""|g' libtool
sed -i 's|^runpath_var=LD_RUN_PATH|runpath_var=DIE_RPATH_DIE|g' libtool
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}
find %{buildroot} -name '*.la' -exec rm -f {} ';'

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING COPYING.LGPL-2 COPYING.LGPL-2.1 NEWS README TODO
%{_libdir}/*.so.*
%doc src/apidocs.*
%{_includedir}/*
%{_libdir}/*.so
%{_libdir}/pkgconfig/%{name}.pc
%{_bindir}/*
%{_mandir}/man1/*
%doc README

%changelog
