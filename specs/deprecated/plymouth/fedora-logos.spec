%global codename sphericalcow

Name: fedora-logos
Summary: Fedora-related icons and pictures
Version: 17.0.2
Release: 5%{?dist}
Group: System Environment/Base
URL: http://git.fedorahosted.org/git/fedora-logos.git/
Source0: https://fedorahosted.org/releases/f/e/fedora-logos/fedora-logos-%{version}.tar.bz2
License: Licensed only for approved usage, see COPYING for details. 

Obsoletes: redhat-logos
Obsoletes: gnome-logos
Provides: redhat-logos = %{version}-%{release}
Provides: gnome-logos = %{version}-%{release}
Provides: system-logos = %{version}-%{release}
Conflicts: anaconda-images <= 10
Conflicts: redhat-artwork <= 5.0.5
# For splashtolss.sh
BuildRequires: syslinux, netpbm
Requires(post): coreutils
BuildRequires: hardlink
# For generating the EFI icon
BuildRequires: imagemagick
BuildRequires: libicns librsvg

%description
The fedora-logos package contains image files which incorporate the 
Fedora trademarks (the "Marks"). The Marks are trademarks or registered 
trademarks of Red Hat, Inc. in the United States and other countries and 
are used by permission.

This package and its content may not be distributed with anything but
unmodified packages from Fedora Project. It can be used in a Fedora Spin, 
but not in a Fedora Remix. If necessary, this package can be replaced by 
the more liberally licensed generic-logos package.

See the included COPYING file for full information on copying and 
redistribution of this package and its contents.

%prep
%setup -q

%build
make bootloader/fedora.icns

%install
# should be ifarch i386
mkdir -p $RPM_BUILD_ROOT/boot/grub
install -p -m 644 -D bootloader/splash.xpm.gz $RPM_BUILD_ROOT/boot/grub/splash.xpm.gz
mkdir -p $RPM_BUILD_ROOT/boot/grub2/themes/system/
install -p -m 644 bootloader/background.png $RPM_BUILD_ROOT/boot/grub2/themes/system/background.png
pushd $RPM_BUILD_ROOT/boot/grub2/themes/system/
ln -s background.png fireworks.png
popd

# end i386 bits

mkdir -p $RPM_BUILD_ROOT%{_datadir}/firstboot/themes/fedora-%{codename}/
for i in firstboot/* ; do
  install -p -m 644 $i $RPM_BUILD_ROOT%{_datadir}/firstboot/themes/fedora-%{codename}/
done

mkdir -p $RPM_BUILD_ROOT%{_datadir}/pixmaps/bootloader
install -p -m 644 bootloader/fedora.icns $RPM_BUILD_ROOT%{_datadir}/pixmaps/bootloader
# To regenerate these files, run:
# pngtopnm foo.png | ppmtoapplevol > foo.vol
install -p -m 644 bootloader/fedora.vol bootloader/fedora-media.vol $RPM_BUILD_ROOT%{_datadir}/pixmaps/bootloader

mkdir -p $RPM_BUILD_ROOT%{_datadir}/pixmaps/splash
for i in gnome-splash/* ; do
  install -p -m 644 $i $RPM_BUILD_ROOT%{_datadir}/pixmaps/splash
done

mkdir -p $RPM_BUILD_ROOT%{_datadir}/gnome-screensaver
for i in gnome-screensaver/* ; do
  install -p -m 644 $i $RPM_BUILD_ROOT%{_datadir}/gnome-screensaver
done

mkdir -p $RPM_BUILD_ROOT%{_datadir}/pixmaps
for i in pixmaps/* ; do
  install -p -m 644 $i $RPM_BUILD_ROOT%{_datadir}/pixmaps
done

mkdir -p $RPM_BUILD_ROOT%{_datadir}/plymouth/themes/charge
for i in plymouth/charge/* ; do
  install -p -m 644 $i $RPM_BUILD_ROOT%{_datadir}/plymouth/themes/charge
done

for size in 16x16 22x22 24x24 32x32 36x36 48x48 96x96 256x256 ; do
  mkdir -p $RPM_BUILD_ROOT%{_datadir}/icons/hicolor/$size/apps
  mkdir -p $RPM_BUILD_ROOT%{_datadir}/icons/Bluecurve/$size/apps
  for i in icons/hicolor/$size/apps/* ; do
    install -p -m 644 $i $RPM_BUILD_ROOT%{_datadir}/icons/hicolor/$size/apps
    pushd $RPM_BUILD_ROOT%{_datadir}/icons/Bluecurve/$size/apps
    ln -s ../../../hicolor/$size/apps/fedora-logo-icon.png icon-panel-menu.png
    ln -s ../../../hicolor/$size/apps/fedora-logo-icon.png gnome-main-menu.png
    ln -s ../../../hicolor/$size/apps/fedora-logo-icon.png kmenu.png
    ln -s ../../../hicolor/$size/apps/fedora-logo-icon.png start-here.png
    popd
  done
done

mkdir -p $RPM_BUILD_ROOT%{_datadir}/icons/Fedora/48x48/apps
install -p -m 644 icons/Fedora/48x48/apps/* $RPM_BUILD_ROOT%{_datadir}/icons/Fedora/48x48/apps/
mkdir -p $RPM_BUILD_ROOT%{_datadir}/icons/Fedora/scalable/apps
install -p -m 644 icons/Fedora/scalable/apps/* $RPM_BUILD_ROOT%{_datadir}/icons/Fedora/scalable/apps/

mkdir -p $RPM_BUILD_ROOT%{_sysconfdir}
pushd $RPM_BUILD_ROOT%{_sysconfdir}
ln -s %{_datadir}/icons/hicolor/16x16/apps/fedora-logo-icon.png favicon.png
popd

mkdir -p $RPM_BUILD_ROOT%{_datadir}/icons/hicolor/scalable/apps
install -p -m 644 icons/hicolor/scalable/apps/xfce4_xicon1.svg $RPM_BUILD_ROOT%{_datadir}/icons/hicolor/scalable/apps
install -p -m 644 icons/hicolor/scalable/apps/fedora-logo-icon.svg $RPM_BUILD_ROOT%{_datadir}/icons/hicolor/scalable/apps/start-here.svg

(cd anaconda; make DESTDIR=$RPM_BUILD_ROOT install)

for i in 16 22 24 32 36 48 96 256 ; do
  mkdir -p $RPM_BUILD_ROOT%{_datadir}/icons/Fedora/${i}x${i}/places
  install -p -m 644 -D $RPM_BUILD_ROOT%{_datadir}/icons/hicolor/${i}x${i}/apps/fedora-logo-icon.png $RPM_BUILD_ROOT%{_datadir}/icons/Fedora/${i}x${i}/places/start-here.png
done

mkdir -p $RPM_BUILD_ROOT%{_datadir}/icons/Fedora/scalable/places/
pushd $RPM_BUILD_ROOT%{_datadir}/icons/Fedora/scalable/places/
ln -s ../../../hicolor/scalable/apps/start-here.svg .
popd

mkdir -p $RPM_BUILD_ROOT%{_datadir}/%{name}
cp -a fedora/*.svg $RPM_BUILD_ROOT%{_datadir}/%{name}

# save some dup'd icons
/usr/sbin/hardlink -v %{buildroot}/

%post
touch --no-create %{_datadir}/icons/hicolor || :
touch --no-create %{_datadir}/icons/Bluecurve || :
touch --no-create %{_datadir}/icons/Fedora || :

%postun
if [ $1 -eq 0 ] ; then
  touch --no-create %{_datadir}/icons/hicolor || :
  touch --no-create %{_datadir}/icons/Bluecurve || :
  touch --no-create %{_datadir}/icons/Fedora || :
  gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
  gtk-update-icon-cache %{_datadir}/icons/Bluecurve &>/dev/null || :
  gtk-update-icon-cache %{_datadir}/icons/Fedora &>/dev/null || :
fi

%posttrans
gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
gtk-update-icon-cache %{_datadir}/icons/Bluecurve &>/dev/null || :
gtk-update-icon-cache %{_datadir}/icons/Fedora &>/dev/null || :

%files
%doc COPYING
%config(noreplace) %{_sysconfdir}/favicon.png
%{_datadir}/firstboot/themes/fedora-%{codename}/
%{_datadir}/plymouth/themes/charge/

%{_datadir}/pixmaps/*
%{_datadir}/anaconda/pixmaps/*
%{_datadir}/anaconda/boot/splash.lss
%{_datadir}/anaconda/boot/syslinux-splash.png
%{_datadir}/anaconda/boot/splash.png
%{_datadir}/icons/hicolor/*/apps/*
%{_datadir}/icons/Bluecurve/*/apps/*
%{_datadir}/icons/Fedora/*/apps/
%{_datadir}/icons/Fedora/*/places/*
%{_datadir}/gnome-screensaver/*
%{_datadir}/%{name}/

# we multi-own these directories, so as not to require the packages that
# provide them, thereby dragging in excess dependencies.
%dir %{_datadir}/icons/Bluecurve/
%dir %{_datadir}/icons/Bluecurve/16x16/
%dir %{_datadir}/icons/Bluecurve/16x16/apps/
%dir %{_datadir}/icons/Bluecurve/22x22/
%dir %{_datadir}/icons/Bluecurve/22x22/apps/
%dir %{_datadir}/icons/Bluecurve/24x24/
%dir %{_datadir}/icons/Bluecurve/24x24/apps/
%dir %{_datadir}/icons/Bluecurve/32x32/
%dir %{_datadir}/icons/Bluecurve/32x32/apps/
%dir %{_datadir}/icons/Bluecurve/36x36/
%dir %{_datadir}/icons/Bluecurve/36x36/apps/
%dir %{_datadir}/icons/Bluecurve/48x48/
%dir %{_datadir}/icons/Bluecurve/48x48/apps/
%dir %{_datadir}/icons/Bluecurve/96x96/
%dir %{_datadir}/icons/Bluecurve/96x96/apps/
%dir %{_datadir}/icons/Bluecurve/256x256/
%dir %{_datadir}/icons/Bluecurve/256x256/apps/
%dir %{_datadir}/icons/Fedora/
%dir %{_datadir}/icons/Fedora/16x16/
%dir %{_datadir}/icons/Fedora/16x16/places/
%dir %{_datadir}/icons/Fedora/22x22/
%dir %{_datadir}/icons/Fedora/22x22/places/
%dir %{_datadir}/icons/Fedora/24x24/
%dir %{_datadir}/icons/Fedora/24x24/places/
%dir %{_datadir}/icons/Fedora/32x32/
%dir %{_datadir}/icons/Fedora/32x32/places/
%dir %{_datadir}/icons/Fedora/36x36/
%dir %{_datadir}/icons/Fedora/36x36/places/
%dir %{_datadir}/icons/Fedora/48x48/
%dir %{_datadir}/icons/Fedora/48x48/places/
%dir %{_datadir}/icons/Fedora/96x96/
%dir %{_datadir}/icons/Fedora/96x96/places/
%dir %{_datadir}/icons/Fedora/256x256/
%dir %{_datadir}/icons/Fedora/256x256/places/
%dir %{_datadir}/icons/Fedora/scalable/
%dir %{_datadir}/icons/Fedora/scalable/places/
%dir %{_datadir}/icons/hicolor/
%dir %{_datadir}/icons/hicolor/16x16/
%dir %{_datadir}/icons/hicolor/16x16/apps/
%dir %{_datadir}/icons/hicolor/22x22/
%dir %{_datadir}/icons/hicolor/22x22/apps/
%dir %{_datadir}/icons/hicolor/24x24/
%dir %{_datadir}/icons/hicolor/24x24/apps/
%dir %{_datadir}/icons/hicolor/32x32/
%dir %{_datadir}/icons/hicolor/32x32/apps/
%dir %{_datadir}/icons/hicolor/36x36/
%dir %{_datadir}/icons/hicolor/36x36/apps/
%dir %{_datadir}/icons/hicolor/48x48/
%dir %{_datadir}/icons/hicolor/48x48/apps/
%dir %{_datadir}/icons/hicolor/96x96/
%dir %{_datadir}/icons/hicolor/96x96/apps/
%dir %{_datadir}/icons/hicolor/256x256/
%dir %{_datadir}/icons/hicolor/256x256/apps/
%dir %{_datadir}/icons/hicolor/scalable/
%dir %{_datadir}/icons/hicolor/scalable/apps/
%dir %{_datadir}/anaconda
%dir %{_datadir}/anaconda/boot/
%dir %{_datadir}/anaconda/pixmaps/
%dir %{_datadir}/firstboot/
%dir %{_datadir}/firstboot/themes/
%dir %{_datadir}/gnome-screensaver/
%dir %{_datadir}/plymouth/
%dir %{_datadir}/plymouth/themes/
/boot/grub/splash.xpm.gz
/boot/grub2/themes/system/background.png
/boot/grub2/themes/system/fireworks.png
# end i386 bits

%changelog
