Name:       gnome-screensaver
Version:    3.6.1
Release:    5%{?dist}
Summary:    GNOME Screensaver

Group:      User Interface/GNOME
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/%{name}/3.6/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  dbus-glib gnome-desktop linux-pam
BuildRequires:  libgnomekbd
BuildRequires:  intltool gettext xml-parser

%description
The GNOME Screensaver package contains a screen saver and locker designed to have simple, sane, secure defaults and be well integrated with the desktop.
It supports locking down of configuration settings, has translations into many languages and convenient user switching.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
sed -i 's|etc/pam\.d"|etc"|' data/Makefile.in &&
./configure --prefix=/usr \
            --sysconfdir=/etc \
            --libexecdir=%{_libdir}/gnome-screensaver \
            --with-pam-prefix=/etc &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

mkdir -pv %{buildroot}/etc/pam.d
cat > %{buildroot}/etc/pam.d/gnome-screensaver << "EOF"
# Begin /etc/pam.d/gnome-screensaver

auth     include    system-auth
auth     optional   pam_gnome_keyring.so

account  include    system-account
password include    system-password
session  include    system-session

# End /etc/pam.d/gnome-screensaver
EOF
chmod -v 644 %{buildroot}/etc/pam.d/gnome-screensaver

%files
%defattr(-,root,root,-)
/etc/pam.d/gnome-screensaver
/etc/xdg/autostart/gnome-screensaver.desktop
%{_bindir}/gnome-screensaver
%{_bindir}/gnome-screensaver-command
%{_libdir}/gnome-screensaver/gnome-screensaver-dialog
%{_datadir}/locale/*

%files doc
%defattr(-,root,root,-)
%{_mandir}/man1/*

%clean
rm -rf %{buildroot}

%changelog
* Sat Oct 27 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.0 to 3.6.1
* Fri Sep 28 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.4.4 to 3.6.0
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
