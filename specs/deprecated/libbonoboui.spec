#
# Ramone Linux 1.0
#

Summary: libbonoboui
Name: libbonoboui
Version: 2.24.3
Release: 1%{?dist}
License: GPLv2+
Group: System Environment/libraries
Source: http://ftp.gnome.org/pub/gnome/sources/libbonoboui/2.24/%{name}-%{version}.tar.bz2
Url: http://ftp.gnome.org
Distribution: ramone linux
Vendor: ramone
Packager: tanggeliang <tanggeliang@gmail.com>

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires: libglade, libgnome, libgnomecanvas
Requires: glib, libbonobo, libgnome, libgnomecanvas
AutoReqProv: no

%description

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr &&
make

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/bonobo-browser
%{_bindir}/test-moniker
%{_includedir}/libbonoboui-2.0/*
%{_libdir}/bonobo/*
%{_libdir}/bonobo-2.0/*
%{_libdir}/libbonoboui-2.*
%{_libdir}/libglade/*
%{_libdir}/pkgconfig/libbonoboui-2.0.pc
%{_datadir}/applications/*
%{_datadir}/gnome-2.0/*
%{_datadir}/gtk-doc/*
%{_datadir}/locale/*

%clean
rm -rf %{buildroot}

%changelog
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
