Name:       gnome-media
Version:    3.4.0
Release:    5%{?dist}
Summary:    gnome-media

Group:      Applications/Multimedia
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/gnome-media/3.4/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  desktop-file-utils, gconf, gstreamer, gst-plugins-base, gtk+, gnome-control-center, gnome-doc-utils
BuildRequires:  libgnome-media-profiles
BuildRequires:  intltool gettext xml-parser rarian docbook-xml
Requires:       gst-plugins-good

%description

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
%configure --disable-schemas-install &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
/etc/gconf/schemas/gnome-sound-recorder.schemas
%{_bindir}/gnome-sound-recorder
%{_bindir}/gstreamer-properties
%{_datadir}/applications/gnome-sound-recorder.desktop
%{_datadir}/applications/gstreamer-properties.desktop
%{_datadir}/gnome-media/sounds/gnome-sounds-default.xml
%{_datadir}/gnome-sound-recorder/ui/ui.xml
%{_datadir}/gstreamer-properties/gstreamer-properties.ui
%{_datadir}/gstreamer-properties/icons/gstreamer-properties.png
%{_datadir}/icons/hicolor/*/apps/*.png
%{_datadir}/icons/hicolor/*/apps/*.svg
%{_datadir}/locale/*/LC_MESSAGES/gnome-media-2.0.mo
%{_datadir}/omf/gnome-sound-recorder/gnome-sound-recorder-*.omf
%{_datadir}/omf/gstreamer-properties/gstreamer-properties-*.omf
%{_datadir}/sounds/gnome/default/alerts/*.ogg

%files doc
%defattr(-,root,root,-)
%{_datadir}/gnome/help/gnome-sound-recorder/*/figures/grecord_window.png
%{_datadir}/gnome/help/gnome-sound-recorder/*/gnome-sound-recorder.xml
%{_datadir}/gnome/help/gnome-sound-recorder/*/legal.xml
%{_datadir}/gnome/help/gstreamer-properties/*/figures/gstreamer_properties_window.png
%{_datadir}/gnome/help/gstreamer-properties/*/gstreamer-properties.xml
%{_datadir}/gnome/help/gstreamer-properties/*/legal.xml

%clean
rm -rf %{buildroot}

%post
export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
gconftool-2 --makefile-install-rule /etc/gconf/schemas/gnome-sound-recorder.schemas > /dev/null || :
gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
glib-compile-schemas %{_datadir}/glib-2.0/schemas &>/dev/null || :

%postun
export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
gconftool-2 --makefile-install-rule /etc/gconf/schemas/gnome-sound-recorder.schemas > /dev/null || :

%changelog
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
