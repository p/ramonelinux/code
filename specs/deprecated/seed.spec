Name:       seed
Version:    3.8.1
Release:    1%{?dist}
Summary:    Seed

Group:      User Interface/GNOME
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/%{name}/3.8/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  gnome-js-common gobject-introspection webkitgtk
BuildRequires:  gtk-doc intltool gettext xml-parser

%description
Seed is a JavaScript interpreter.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --disable-static \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/seed
%{_includedir}/seed-gtk3/seed*.h
%{_libdir}/libseed-gtk3.*
%{_libdir}/pkgconfig/seed.pc
%{_libdir}/seed-gtk3/libseed_*.la
%{_libdir}/seed-gtk3/libseed_*.so
%{_datadir}/seed-gtk3/*.js
%{_datadir}/seed-gtk3/extensions/*.js

%files doc
%defattr(-,root,root,-)
%{_datadir}/gtk-doc/html/seed/*
%{_docdir}/seed/*
%{_mandir}/man1/seed.1.gz

%clean
rm -rf %{buildroot}

%changelog
* Mon Aug 5 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.2.0 to 3.8.1
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
