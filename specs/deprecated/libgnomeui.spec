#
# Ramone Linux 1.0
#

Summary: libgnomeui
Name: libgnomeui
Version: 2.24.3
Release: 1%{?dist}
License: GPLv2+
Group: System Environment/libraries
Source: http://ftp.gnome.org/pub/gnome/sources/libgnomeui/2.24/%{name}-%{version}.tar.bz2
Url: http://ftp.gnome.org
Distribution: ramone linux
Vendor: ramone
Packager: tanggeliang <tanggeliang@gmail.com>

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires: libbonoboui, libgnome-keyring, xorg-libs
Requires: gtk+, gnome-vfs, libbonoboui, libgnome, libgnomecanvas, xorg-libs
AutoReqProv: no

%description

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --libexecdir=/usr/lib/libgnomeui &&
make

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_includedir}/libgnomeui-2.0/*
%{_libdir}/libglade/*
%{_libdir}/libgnomeui-2.*
%{_libdir}/pkgconfig/libgnomeui-2.0.pc
%{_datadir}/gtk-doc/*
%{_datadir}/locale/*
%{_datadir}/pixmaps/*

%post
cat >> /etc/profile << "EOF"
export LIBGLADE_MODULE_PATH=/usr/lib/libglade/2.0
EOF

%postun

%clean
rm -rf %{buildroot}

%changelog
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
