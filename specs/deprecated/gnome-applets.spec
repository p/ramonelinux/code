Name:       gnome-applets
Version:    3.5.92
Release:    4%{?dist}
Summary:    GNOME Applets

Group:      User Interface/GNOME
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/%{name}/3.5/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  gconf gnome-icon-theme gnome-panel
BuildRequires:  libgnomekbd libgtop libnotify networkmanager rarian
BuildRequires:  intltool gettext xml-parser
BuildRequires:  gnome-doc-utils docbook-xml docbook-xsl
Requires:       shared-mime-info

%description
The GNOME Applets package contains small applications which generally run in the background and display their output to the GNOME Panel.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --sysconfdir=/etc \
            --libexecdir=%{_libdir}/gnome-applets \
            --disable-schemas-install \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
/etc/dbus-1/system.d/org.gnome.CPUFreqSelector.conf
/etc/gconf/schemas/*.schemas
/etc/sound/events/battstat_applet.soundlist
%{_bindir}/cpufreq-selector
%{_libdir}/bonobo/servers/GNOME_MiniCommanderApplet.server
%{_libdir}/gnome-applets/*applet*
%{_datadir}/dbus-1/services/org.gnome.panel.applet.*.service
%{_datadir}/dbus-1/system-services/org.gnome.CPUFreqSelector.service
%{_datadir}/glib-2.0/schemas/org.gnome.applets.GWeatherApplet.gschema.xml
%{_datadir}/gnome-applets/builder/*.ui
%{_datadir}/gnome-applets/geyes/*/*.png
%{_datadir}/gnome-applets/geyes/*/config
%{_datadir}/gnome-applets/ui/*.xml
%{_datadir}/gnome-panel/4.0/applets/org.gnome.applets.*.panel-applet
%{_datadir}/icons/hicolor/*/apps/*-applet.png
%{_datadir}/icons/hicolor/scalable/apps/gnome-*-applet.svg
%{_datadir}/locale/*/LC_MESSAGES/gnome-applets-3.0.mo
%{_datadir}/omf/*/*-*.omf
%{_datadir}/pixmaps/accessx-status-applet/*.png
%{_datadir}/pixmaps/cpufreq-applet/cpufreq-*.png
%{_datadir}/pixmaps/stickynotes/*.png
%{_datadir}/polkit-1/actions/org.gnome.cpufreqselector.policy

%files doc
%defattr(-,root,root,-)
%{_datadir}/gnome/help/*/*/*.xml
%{_datadir}/gnome/help/*/*/figures/*.png

%post
gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
glib-compile-schemas %{_datadir}/glib-2.0/schemas &>/dev/null || :

%postun

%clean
rm -rf %{buildroot}

%changelog
* Mon Oct 1 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.4.1 to 3.5.92
* Sun Aug 26 2012 tanggeliang <tanggeliang@gmail.com>
- add "--disable-schemas-install" fix "WARNING: failed to install schema".
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
