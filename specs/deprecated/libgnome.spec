#
# Ramone Linux 1.0
#

Summary: libgnome
Name: libgnome
Version: 2.30.0
Release: 1%{?dist}
License: GPLv2+
Group: System Environment/libraries
Source: http://ftp.gnome.org/pub/gnome/sources/libgnome/2.30/%{name}-%{version}.tar.bz2
Url: http://ftp.gnome.org
Distribution: ramone linux
Vendor: ramone
Packager: tanggeliang <tanggeliang@gmail.com>

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires: libbonobo, gnome-vfs, esound
Requires: glib, libbonobo
AutoReqProv: no

%description

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --sysconfdir=/etc/gnome/2.30.2 \
            --localstatedir=/var/lib \
            --mandir=/usr/share/man &&
make

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
/etc/gnome/2.30.2/gconf/*
/etc/gnome/2.30.2/sound/*
%{_bindir}/gnome-open
%{_includedir}/libgnome-2.0/libgnome/*.h
%{_libdir}/bonobo/*
%{_libdir}/libgnome-2.*
%{_libdir}/pkgconfig/libgnome-2.0.pc
%{_datadir}/gnome-background-properties/*
%{_datadir}/gtk-doc/*
%{_datadir}/locale/*
%{_mandir}/*/*
%{_datadir}/pixmaps/*

%clean
rm -rf %{buildroot}

%post
export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
gconftool-2 --makefile-install-rule /etc/gnome/2.30.2/gconf/schemas/desktop_gnome_*.schemas > /dev/null || :

%postun

%changelog
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
