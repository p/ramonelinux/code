#
# Ramone Linux 1.0
#

Summary: libgnomecanvas
Name: libgnomecanvas
Version: 2.30.1
Release: 1%{?dist}
License: GPLv2+
Group: System Environment/libraries
Source: http://ftp.gnome.org/pub/gnome/sources/libgnomecanvas/2.30/%{name}-%{version}.tar.bz2
Url: http://ftp.gnome.org
Distribution: ramone linux
Vendor: ramone
Packager: tanggeliang <tanggeliang@gmail.com>

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires: gtk+, intltool, libart_lgpl
Requires: gtk+
AutoReqProv: no

%description

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr &&
make

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_includedir}/libgnomecanvas-2.0/libgnomecanvas/*.h
%{_libdir}/libglade/*
%{_libdir}/libgnomecanvas-2.*
%{_libdir}/pkgconfig/libgnomecanvas-2.0.pc
%{_datadir}/gtk-doc/*
%{_datadir}/locale/*

%clean
rm -rf %{buildroot}

%changelog
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
