Name:       epiphany-extensions
Version:    3.6.0
Release:    1%{?dist}
Summary:    Epiphany Extensions

Group:      User Interface/GNOME
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/%{name}/3.6/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  epiphany gnome-doc-utils
BuildRequires:  dbus-glib opensp rarian
BuildRequires:  intltool gettext xml-parser
BuildRequires:  docbook-xml docbook-xsl

%description
Epiphany Extensions is a collection of extensions for Epiphany, the GNOME Web Browser.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_libdir}/epiphany/3.6/extensions/*
%{_datadir}/epiphany-extensions/ephy-gestures.xml
%{_datadir}/epiphany-extensions/ui/*.ui
%{_datadir}/epiphany/icons/hicolor/*/status/*.png
%{_datadir}/epiphany/icons/hicolor/scalable/status/feed-presence.svg
%{_datadir}/glib-2.0/schemas/org.gnome.epiphanyextensions.gschema.xml
%{_datadir}/locale/*/LC_MESSAGES/epiphany-extensions-3.6.mo
%{_datadir}/omf/epiphany-extensions/epiphany-extensions-*.omf

%files doc
%defattr(-,root,root,-)
%{_datadir}/gnome/help/epiphany-extensions/*/*.xml
%{_datadir}/gnome/help/epiphany-extensions/*/figures/*.png

%post
glib-compile-schemas %{_datadir}/glib-2.0/schemas &>/dev/null || :

%clean
rm -rf %{buildroot}

%changelog
* Sat Oct 27 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.4.0 to 3.6.0
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
