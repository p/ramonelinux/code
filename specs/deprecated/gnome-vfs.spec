#
# Ramone Linux 1.0
#

Summary: gnome-vfs
Name: gnome-vfs
Version: 2.24.4
Release: 1%{?dist}
License: GPLv2+
Group: System Environment/libraries
Source: http://ftp.gnome.org/pub/gnome/sources/gnome-vfs/2.24/%{name}-%{version}.tar.bz2
Url: http://ftp.gnome.org
Distribution: ramone linux
Vendor: ramone
Packager: tanggeliang <tanggeliang@gmail.com>

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires: dbus-glib, GConf, gnome-mime-data
Requires: glib, GConf
AutoReqProv: no

%description

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --sysconfdir=/etc/gnome/2.30.2 \
            --libexecdir=/usr/lib/gnome-vfs-2.0 &&
make

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
/etc/gnome/2.30.2/gconf/*
/etc/gnome/2.30.2/gnome-vfs-2.0/*
%{_bindir}/gnomevfs-*
%{_includedir}/gnome-vfs-2.0/*
%{_includedir}/gnome-vfs-module-2.0/*
%{_libdir}/gnome-vfs-2.0/*
%{_libdir}/libgnomevfs-2.*
%{_libdir}/pkgconfig/gnome-vfs-*.pc
%{_datadir}/dbus-1/*
%{_datadir}/gtk-doc/*
%{_datadir}/locale/*

%clean
rm -rf %{buildroot}

%post
export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
gconftool-2 --makefile-install-rule \
    /etc/gnome/2.30.2/gconf/schemas/system_http_proxy.schemas \
    /etc/gnome/2.30.2/gconf/schemas/system_dns_sd.schemas \
    /etc/gnome/2.30.2/gconf/schemas/system_smb.schemas \
    /etc/gnome/2.30.2/gconf/schemas/desktop_gnome_url_handlers.schemas \
    /etc/gnome/2.30.2/gconf/schemas/desktop_default_applications.schemas \
      > /dev/null || :

%postun

%changelog
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
