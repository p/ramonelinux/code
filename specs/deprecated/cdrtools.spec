Name:       cdrtools
Version:    3.00
Release:    5%{?dist}
Summary:    Cdrtools

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://cdrecord.org
Source:     ftp://ftp.berlios.de/pub/cdrecord/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-build

%description
Cdrtools is a set of command line programs that allows to record CD/DVD/BluRay media.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
make INS_BASE=/usr DEFINSUSR=root DEFINSGRP=root %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make INS_BASE=%{buildroot}/usr DEFINSUSR=root DEFINSGRP=root install

chmod 644 %{buildroot}/usr/lib/*.a

%files
%defattr(-,root,root,-)
%{_bindir}/btcflash
%{_bindir}/cdda2mp3
%{_bindir}/cdda2ogg
%{_bindir}/cdda2wav
%{_bindir}/cdrecord
%{_bindir}/devdump
%{_bindir}/isodebug
%{_bindir}/isodump
%{_bindir}/isoinfo
%{_bindir}/isovfy
%{_bindir}/mkhybrid
%{_bindir}/mkisofs
%{_bindir}/readcd
%{_bindir}/scgcheck
%{_bindir}/scgskeleton
%{_includedir}/*
/usr/lib/lib*.a
/usr/lib/profiled/lib*.a
/usr/lib/siconv/cp*
/usr/lib/siconv/iso8859-*
/usr/lib/siconv/koi8-*
%{_sbindir}/rscsi

%files doc
%defattr(-,root,root,-)
%{_docdir}/*
%{_mandir}/man*/*.gz

%clean
rm -rf %{buildroot}

%changelog
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
