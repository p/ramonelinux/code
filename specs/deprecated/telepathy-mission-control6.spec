Name:       telepathy-mission-control
Version:    5.99.11
Release:    1%{?dist}
Summary:    Telepathy Mission Control

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.freedesktop.org
Source:     http://telepathy.freedesktop.org/releases/telepathy-mission-control/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  telepathy-glib
BuildRequires:  libgnome-keyring networkmanager upower
BuildRequires:  gtk-doc libxslt

%description
Telepathy Mission Control is an account manager and channel dispatcher for the Telepathy framework, allowing user interfaces and other clients to share connections to real-time communication services without conflicting.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --libexecdir=%{_libdir}/telepathy \
            --enable-gnome-keyring \
            --disable-static \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/mc6-tool
%{_bindir}/mc6-wait-for-name
%{_includedir}/mission-control-6/mission-control-plugins/*.h
%{_libdir}/libmission-control-plugins-6.la
%{_libdir}/libmission-control-plugins-6.so*
%{_libdir}/pkgconfig/mission-control-plugins-6.pc
%{_libdir}/telepathy/mission-control-6
%{_datadir}/dbus-1/services/im.telepathy.v1.*.service
%{_datadir}/glib-2.0/schemas/im.telepathy.v1.MissionControl.FromEmpathy.gschema.xml

%files doc
%defattr(-,root,root,-)
%{_datadir}/gtk-doc/html/mission-control-plugins-6/*
%{_mandir}/man1/*.1.gz
%{_mandir}/man8/*.8.gz

%post
glib-compile-schemas %{_datadir}/glib-2.0/schemas &>/dev/null || :

%clean
rm -rf %{buildroot}

%changelog
* Sat Jul 19 2014 tanggeliang <tanggeliang@gmail.com>
- update from 5.99.8 to 5.99.11
* Sat Mar 22 2014 tanggeliang <tanggeliang@gmail.com>
- update from 5.99.6 to 5.99.8
* Sun Mar 16 2014 tanggeliang <tanggeliang@gmail.com>
- update from 5.16.0 to 5.99.6
* Mon Jan 6 2014 tanggeliang <tanggeliang@gmail.com>
- update from 5.15.0 to 5.16.0
* Sat Aug 17 2013 tanggeliang <tanggeliang@gmail.com>
- update from 5.14.1 to 5.15.0
* Fri May 10 2013 tanggeliang <tanggeliang@gmail.com>
- update from 5.14.0 to 5.14.1
* Sun Oct 14 2012 tanggeliang <tanggeliang@gmail.com>
- update from 5.12.1 to 5.14.0
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
