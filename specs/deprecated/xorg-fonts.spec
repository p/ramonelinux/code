#
# xorg-fonts for ramone
#

Summary: xorg-fonts
Name: xorg-fonts
Version: 7.7.1
Release: 4%{?dist}
License: GPLv2+
Group: System Environment/libraries

Source0:  http://xorg.freedesktop.org/releases/individual/font/font-adobe-75dpi-1.0.3.tar.bz2
Source1:  http://xorg.freedesktop.org/releases/individual/font/font-adobe-100dpi-1.0.3.tar.bz2
Source2:  http://xorg.freedesktop.org/releases/individual/font/font-adobe-utopia-75dpi-1.0.4.tar.bz2
Source3:  http://xorg.freedesktop.org/releases/individual/font/font-adobe-utopia-100dpi-1.0.4.tar.bz2
Source4:  http://xorg.freedesktop.org/releases/individual/font/font-adobe-utopia-type1-1.0.4.tar.bz2
Source5:  http://xorg.freedesktop.org/releases/individual/font/font-alias-1.0.3.tar.bz2
Source6:  http://xorg.freedesktop.org/releases/individual/font/font-arabic-misc-1.0.3.tar.bz2
Source7:  http://xorg.freedesktop.org/releases/individual/font/font-bh-75dpi-1.0.3.tar.bz2
Source8: http://xorg.freedesktop.org/releases/individual/font/font-bh-100dpi-1.0.3.tar.bz2
Source9: http://xorg.freedesktop.org/releases/individual/font/font-bh-lucidatypewriter-75dpi-1.0.3.tar.bz2
Source10: http://xorg.freedesktop.org/releases/individual/font/font-bh-lucidatypewriter-100dpi-1.0.3.tar.bz2
Source11: http://xorg.freedesktop.org/releases/individual/font/font-bh-ttf-1.0.3.tar.bz2
Source12: http://xorg.freedesktop.org/releases/individual/font/font-bh-type1-1.0.3.tar.bz2
Source13: http://xorg.freedesktop.org/releases/individual/font/font-bitstream-75dpi-1.0.3.tar.bz2
Source14: http://xorg.freedesktop.org/releases/individual/font/font-bitstream-100dpi-1.0.3.tar.bz2
Source15: http://xorg.freedesktop.org/releases/individual/font/font-bitstream-type1-1.0.3.tar.bz2
Source16: http://xorg.freedesktop.org/releases/individual/font/font-cronyx-cyrillic-1.0.3.tar.bz2
Source17: http://xorg.freedesktop.org/releases/individual/font/font-cursor-misc-1.0.3.tar.bz2
Source18: http://xorg.freedesktop.org/releases/individual/font/font-daewoo-misc-1.0.3.tar.bz2
Source19: http://xorg.freedesktop.org/releases/individual/font/font-dec-misc-1.0.3.tar.bz2
Source20: http://xorg.freedesktop.org/releases/individual/font/font-ibm-type1-1.0.3.tar.bz2
Source21: http://xorg.freedesktop.org/releases/individual/font/font-isas-misc-1.0.3.tar.bz2
Source22: http://xorg.freedesktop.org/releases/individual/font/font-jis-misc-1.0.3.tar.bz2
Source23: http://xorg.freedesktop.org/releases/individual/font/font-micro-misc-1.0.3.tar.bz2
Source24: http://xorg.freedesktop.org/releases/individual/font/font-misc-cyrillic-1.0.3.tar.bz2
Source25: http://xorg.freedesktop.org/releases/individual/font/font-misc-ethiopic-1.0.3.tar.bz2
Source26: http://xorg.freedesktop.org/releases/individual/font/font-misc-meltho-1.0.3.tar.bz2
Source27: http://xorg.freedesktop.org/releases/individual/font/font-misc-misc-1.1.2.tar.bz2
Source28: http://xorg.freedesktop.org/releases/individual/font/font-mutt-misc-1.0.3.tar.bz2
Source29: http://xorg.freedesktop.org/releases/individual/font/font-schumacher-misc-1.1.2.tar.bz2
Source30: http://xorg.freedesktop.org/releases/individual/font/font-screen-cyrillic-1.0.4.tar.bz2
Source31: http://xorg.freedesktop.org/releases/individual/font/font-sony-misc-1.0.3.tar.bz2
Source32: http://xorg.freedesktop.org/releases/individual/font/font-sun-misc-1.0.3.tar.bz2
Source33: http://xorg.freedesktop.org/releases/individual/font/font-winitzki-cyrillic-1.0.3.tar.bz2
Source34: http://xorg.freedesktop.org/releases/individual/font/font-xfree86-type1-1.0.4.tar.bz2

Url: http://xorg.freedesktop.org
Distribution: ramone linux
Vendor: ramone
Packager: tanggeliang <tanggeliang@gmail.com>

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires: xorg-apps, xcursor-themes, fontconfig, libfontenc, libXfont, font-util, encodings
BuildRequires: util-macros

%description

%prep
%setup -q -c %{name}-%{version} -a0 -a1 -a2 -a3 -a4 -a5 -a6 -a7 -a8 -a9 -a10 -a11 -a12 -a13 -a14 -a15 -a16 -a17 -a18 -a19 -a20 -a21 -a22 -a23 -a24 -a25 -a26 -a27 -a28 -a29 -a30 -a31 -a32 -a33 -a34

%build
for dir in font-*; do
    pushd $dir
    ./configure --prefix=/usr --sysconfdir=/etc \
                --mandir=/usr/share/man --localstatedir=/var
    make %{?_smp_mflags}
    popd
done

%check

%install
rm -rf %{buildroot}
for dir in font-*; do
    make -C $dir install DESTDIR=%{buildroot}
done

%files
%defattr(-,root,root,-)
/etc/fonts/conf.avail/*
/etc/fonts/conf.d/*
%{_datadir}/fonts/X11/100dpi/*
%{_datadir}/fonts/X11/75dpi/*
%{_datadir}/fonts/X11/cyrillic/*
%{_datadir}/fonts/X11/misc/*
%{_datadir}/fonts/X11/OTF/*
%{_datadir}/fonts/X11/TTF/*
%{_datadir}/fonts/X11/Type1/*

%clean
rm -rf %{buildroot}

%changelog
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
