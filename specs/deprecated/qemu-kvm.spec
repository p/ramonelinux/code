Name:       qemu-kvm
Version:    1.2.0
Release:    5%{?dist}
Summary:    qemu-kvm

Group:      Applications/Emulators
License:    GPLv2+
Url:        http://kvm.sourceforge.net
Source:     http://sourceforge.net/projects/kvm/files/qemu-kvm/1.2.0-rc2/%{name}-%{version}-rc2.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  glib python sdl libx11
BuildRequires:  alsa-lib attr check curl esound mesa cyrus-sasl
BuildRequires:  libtool

%description
qemu-kvm is a full virtualization solution for Linux on x86 hardware containing virtualization extensions (Intel VT or AMD-V).

%prep
%setup -q -n %{name}-%{version}-rc2

%build
./configure --prefix=/usr --sysconfdir=/etc &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
/etc/qemu/target-x86_64.conf
%{_bindir}/qemu-ga
%{_bindir}/qemu-img
%{_bindir}/qemu-io
%{_bindir}/qemu-nbd
%{_bindir}/qemu-system-x86_64
%{_bindir}/vscclient
%{_libdir}exec/qemu-bridge-helper
%{_datadir}/qemu/*
%{_docdir}/qemu/*
%{_mandir}/man*/qemu*.*.gz

%clean
rm -rf %{buildroot}

%changelog
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
