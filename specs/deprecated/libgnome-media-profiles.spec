Name:       libgnome-media-profiles
Version:    3.0.0
Release:    5%{?dist}
Summary:    GNOME Media Profiles library

Group:      System Environment/Libraries
License:    LGPLv2+
Url:        http://www.gnome.org
Source:     http://download.gnome.org/sources/%{name}/3.0/%{name}-%{version}.tar.bz2

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  gtk+
BuildRequires:  gconf glib
Buildrequires:  gstreamer gst-plugins-base
BuildRequires:  intltool
BuildRequires:  gnome-doc-utils
BuildRequires:  gettext xml-parser libxslt docbook-xml

%description

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
%configure --disable-static --disable-schemas-install --disable-scrollkeeper
make %{?_smp_mflags}

%install
make install DESTDIR=%{buildroot}
find %{buildroot} -name '*.la' -exec rm -f {} ';'

%files
%defattr(-,root,root,-)
%{_bindir}/gnome-audio-profiles-properties
%{_sysconfdir}/gconf/schemas/gnome-media-profiles.schemas
%{_libdir}/*.so.*
%{_includedir}/*
%{_libdir}/*.so
%{_libdir}/pkgconfig/libgnome-media-profiles-3.0.pc
%{_datadir}/libgnome-media-profiles
%{_datadir}/locale/*/LC_MESSAGES/libgnome-media-profiles.mo
%{_datadir}/omf/gnome-audio-profiles/gnome-audio-profiles-*.omf

%files doc
%defattr(-,root,root,-)
%doc COPYING README
%{_datadir}/gnome/help/gnome-audio-profiles/*/figures/gnome-audio-profiles-profile-window.png
%{_datadir}/gnome/help/gnome-audio-profiles/*/figures/gnome-audio-profiles-profiles-window.png
%{_datadir}/gnome/help/gnome-audio-profiles/*/gnome-audio-profiles.xml
%{_datadir}/gnome/help/gnome-audio-profiles/*/legal.xml

%clean
rm -rf %{buildroot}

%post
export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
gconftool-2 --makefile-install-rule /etc/gconf/schemas/gnome-media-profiles.schemas > /dev/null || :

%postun
export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
gconftool-2 --makefile-install-rule /etc/gconf/schemas/gnome-media-profiles.schemas > /dev/null || :

%changelog
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
