#!/bin/bash

ROOT=/mnt
source /usr/share/ram-utils/ram-utils-functions

function ch_in()
{
    chroot "$ROOT" /usr/bin/env -i  \
        HOME="/root"                \
        TERM="$TERM"                \
        PS1='\u:\w\$ '              \
        PATH=/bin:/usr/bin:/sbin:/usr/sbin \
        /bin/bash -x -c "su - $1" --login
}

if [ $1 == "in" ]; then
    vfs_mount $ROOT
    ch_in $2
elif [ $1 == "out" ]; then
    vfs_umount $ROOT
fi
