#!/bin/bash

if [ ! -e index.html ]; then
    wget http://www.linuxfromscratch.org/lfs/view/development/
    mv index.html lfs-index.html
    wget http://www.linuxfromscratch.org/blfs/view/svn/
    mv index.html blfs-index.html
    cat lfs-index.html blfs-index.html > index.html
    rm -rf lfs-index.html blfs-index.html
fi

for pkg in `cat all.lst`; do
    ./chk_version.py $pkg
done
