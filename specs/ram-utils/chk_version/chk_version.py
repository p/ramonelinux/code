#!/usr/bin/env python

import sys
import os
import re
from HTMLParser import HTMLParser

class MyParser(HTMLParser):
    def __init__(self, name):
        HTMLParser.__init__(self)
        self.name = name
        ret = os.popen("rpm -q " + self.name).read().strip()
        if ret == "package " + self.name + " is not installed":
            self.version = 0
            print ret
        else:
            self.version = ret[len(self.name) + 1:]
        self.patt = re.compile(name.replace('+', '\+') + '[0-9]*' + '-', re.IGNORECASE)

    def handle_data(self, data):
        HTMLParser.handle_data(self, data)
        m = re.match(self.patt, data)
        if m is not None:
            self.new_version = data[len(self.name) + 1:]
            if re.match('[0-9]', self.new_version) is not None:
                if self.new_version > self.version:
                    print "package " + self.name + " has new version: " + self.new_version

if __name__ == '__main__':
    parser = MyParser(sys.argv[1])
    parser.feed(open("index.html").read())
    parser.close()
