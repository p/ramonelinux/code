#!/bin/bash

for x in proto-7.7.md5 lib-7.7.md5 app-7.7.md5 font-7.7.md5; do
    for line in `awk '{print $2}' $x | sed 's/.tar.bz2//'`; do
        nversion=`echo $line | awk -F '-' '{print $NF}'`
        name=`echo $line | sed 's:-'$nversion'::' | tr A-Z a-z`
        ret=`rpm -q $name`
        if [ "$ret" = "package ${name} is not installed" ]; then
            echo $ret
            version="0"
        else
            version=`echo $ret | cut -c$((${#name}+2))-`
        fi
        if [ "$nversion" '>' "$version" ]; then
            echo "package $name has new version: $nversion"
        fi
    done
done
