#!/bin/bash

workdir=`pwd`

cd ../../../
find -name *.spec > $workdir/list
cd $workdir

sed -i '/deprecated/d' list
sed -i '/dist/d' list
sed -i '/kde/d' list
sed -i '/lxde/d' list
sed -i '/xfce/d' list

cat list | awk -F '/' '{print $NF}' | sed -s 's/.spec$//' | sort > all.lst

rm -rf list
