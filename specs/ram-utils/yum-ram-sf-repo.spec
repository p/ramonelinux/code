Name:       yum-ram-sf-repo
Version:    0.99
Release:    2%{?dist}
Summary:    yum-ram-sf-repo

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://ramonelinux.sourceforge.net
Source0:    ram-sf-i686.repo
Source1:    ram-sf-x86_64.repo

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
Requires:       yum

%description

%prep

%build

%check

%install
mkdir -pv %{buildroot}/etc/yum.repos.d
%ifarch %{ix86}
cp %SOURCE0 %{buildroot}/etc/yum.repos.d/ram-%{version}-sf.repo
%else %ifarch x86_64
cp %SOURCE1 %{buildroot}/etc/yum.repos.d/ram-%{version}-sf.repo
%endif

%files
%defattr(-,root,root,-)
/etc/yum.repos.d/ram-%{version}-sf.repo

%clean
rm -rf %{buildroot}

%changelog
* Sun Jul 27 2014 tanggeliang <tanggeliang@gmail.com>
- update from 0.98 to 0.99
* Thu Oct 24 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.97 to 0.98
* Mon Jun 10 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.96 to 0.97
* Sun Oct 21 2012 tanggeliang <tanggeliang@gmail.com>
- create
