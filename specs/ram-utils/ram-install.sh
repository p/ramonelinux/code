#!/bin/bash

source /usr/share/ram-utils/ram-utils-functions
root=/mnt
USER=ram

if [ $# != 2 ]; then
    echo "ram-install.sh list arch"
    exit
fi

check_root_uid
mkroot $root
pkgs_install $1 $root $2
chroot_exec $root "passwd"
chroot_exec $root "useradd $USER"
chroot_exec $root "passwd $USER"
