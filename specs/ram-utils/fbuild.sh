#!/bin/bash

cd ~/ramone/specs

for arg in "$@"; do
    ram-pkg.sh -a x86_64 -s `find -name $arg.spec`
    ram-pkg.sh -a i686 -s `find -name $arg.spec`
done
