#!/bin/bash

LFS=/mnt

umount $LFS/dev/shm
umount $LFS/dev
umount $LFS/proc
umount $LFS/sys
