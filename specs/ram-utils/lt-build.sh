#!/bin/bash

if [ $# == 2 ]; then
    ram-pkg.sh -a x86_64 `find -name $1.spec` 1>& ~/lt-$1-x86_64-log
    ram-pkg.sh -a i686   `find -name $1.spec` 1>& ~/lt-$1-i686-log
    power-off.expect $2
else
    echo "Usage: $0 pkg passwd."
fi
