#!/bin/bash

source /usr/share/ram-utils/ram-utils-functions
home=/home/ftp/ramone
pool=$home/Rel_0.99/updates
retdir=$pool/resultdir.$$

arch=`rpm -q glibc| awk -F '.' '{print $NF}'`
silence=0

showopts () {
    while getopts ":sa:" optname
    do
        case "$optname" in
            "s")
                silence=1
                ;;
            "a")
                arch=$OPTARG
                ;;
            "?")
                echo "Unknown option $OPTARG"
                ;;
            ":")
                echo "No argument value for option $OPTARG"
                ;;
            *)
                # Should not occur
                echo "Unknown error while processing options"
                ;;
        esac
    done
    return $OPTIND
}

showargs () {
    for p in "$@"
    do
        specfile=$p
    done
}

showopts "$@"
argstart=$?
showargs "${@:$argstart}"

srpm=`rpmbuild -bs $specfile | cut -c8-`

if [ "$silence" == 1 ]; then
    mock --arch $arch -r ramone-1-$arch --resultdir=$retdir rebuild $srpm
else
    mock -v --arch $arch -r ramone-1-$arch --resultdir=$retdir rebuild $srpm
fi

rm -rf $srpm

arch_num=$(ls $retdir | grep ".$arch.rpm" | wc -l)
if [ "$arch_num" -gt 0 ]; then
    mv $retdir/*.$arch.rpm $pool/$arch/packages
    cd $pool/$arch
    $home/genrepo.sh
    cd $pool
fi
noarch_num=$(ls $retdir | grep ".noarch.rpm" | wc -l)
if [ "$noarch_num" -gt 0 ]; then
    mv $retdir/*.noarch.rpm $pool/noarch/packages
    cd $pool/noarch
    $home/genrepo.sh
    cd $pool
fi
mv $retdir/*.src.rpm $pool/source
rm -rf $retdir
