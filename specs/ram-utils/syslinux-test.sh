#!/bin/bash

WORKDIR=`pwd`
DISK_LABEL=ramone

function mk_syslinux()
{
    mkdir -p $WORKDIR/iso/isolinux

    echo -n "\
default linux0
prompt 1
timeout 10

label linux0
  kernel vmlinuz0
  append initrd=initrd0.img root=live:CDLABEL=${DISK_LABEL} rootfstype=auto ro rd.live.image quiet  rhgb rd.luks=0 rd.md=0 rd.dm=0
" > $WORKDIR/iso/isolinux/isolinux.cfg

    cp /usr/share/syslinux/isolinux.bin $WORKDIR/iso/isolinux
    cp -r /usr/share/syslinux/*.c32 $WORKDIR/iso/isolinux/
    cp /boot/vmlinux-3.15.6-1.ram0.99.x86_64 $WORKDIR/iso/isolinux/vmlinuz0
    cp /boot/initrd.img-3.15.6-1.ram0.99.x86_64 $WORKDIR/iso/isolinux/initrd0.img
}

function mk_iso()
{
    mkisofs -R -l -D -b isolinux/isolinux.bin -c isolinux/boot.cat \
        -no-emul-boot -boot-load-size 4 -boot-info-table -V $DISK_LABEL \
        "$1" > "$2"
}

mk_syslinux
mk_iso "$WORKDIR/iso" "$WORKDIR/test.iso"
