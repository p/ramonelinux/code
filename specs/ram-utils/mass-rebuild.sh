#!/bin/bash

if [ $# != 2 ]; then
    echo "mass-rebuild.sh list passwd"
    exit
else
    fbuild.sh `paste $1`
    power-off.expect $2
fi
