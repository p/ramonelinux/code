Name:       bind
Version:    9.9.1
Release:    5%{?dist}
Summary:    BIND

Group:      Applications/Internet
License:    GPLv2+
Url:        http://www.bind.com
Source:     http://gd.tuwien.ac.at/infosys/servers/isc/bind9/9.9.1-P2/%{name}-%{version}-P2.tar.gz
Patch:      bind-9.9.1-P2-use_iproute2-1.patch

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  openssl

%description
The BIND package provides a DNS server and client utilities.
If you are only interested in the utilities, refer to the BIND Utilities-9.9.1-P2.

%prep
%setup -q -n %{name}-%{version}-P2
%patch -p1

%build
./configure --prefix=/usr           \
            --sysconfdir=/etc       \
            --localstatedir=/var    \
            --mandir=/usr/share/man \
            --enable-threads        \
            --with-libtool          &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install &&
chmod -v 0755 %{buildroot}/usr/lib/lib{bind9,isc{,cc,cfg},lwres,dns}.so.*.?.? &&

cd doc &&
install -v -d -m755 %{buildroot}/usr/share/doc/bind-9.9.1-P2/{arm,misc} &&
install -v -m644 arm/*.html \
    %{buildroot}/usr/share/doc/bind-9.9.1-P2/arm &&
install -v -m644 \
    misc/{dnssec,ipv6,migrat*,options,rfc-compliance,roadmap,sdb} \
    %{buildroot}/usr/share/doc/bind-9.9.1-P2/misc

%files
%defattr(-,root,root,-)
/etc/bind.keys
%{_bindir}/dig
%{_bindir}/host
%{_bindir}/isc-config.sh
%{_bindir}/nslookup
%{_bindir}/nsupdate
%{_includedir}/bind9/*.h
%{_includedir}/dns/*.h
%{_includedir}/dst/*.h
%{_includedir}/isc/*.h
%{_includedir}/isccc/*.h
%{_includedir}/isccfg/*.h
%{_includedir}/lwres/*.h
%{_libdir}/libbind9.*
%{_libdir}/libdns.*
%{_libdir}/libisc.*
%{_libdir}/libisccc.*
%{_libdir}/libisccfg.*
%{_libdir}/liblwres.*
%{_sbindir}/arpaname
%{_sbindir}/ddns-confgen
%{_sbindir}/dnssec-dsfromkey
%{_sbindir}/dnssec-keyfromlabel
%{_sbindir}/dnssec-keygen
%{_sbindir}/dnssec-revoke
%{_sbindir}/dnssec-settime
%{_sbindir}/dnssec-signzone
%{_sbindir}/genrandom
%{_sbindir}/isc-hmac-fixup
%{_sbindir}/lwresd
%{_sbindir}/named
%{_sbindir}/named-checkconf
%{_sbindir}/named-checkzone
%{_sbindir}/named-compilezone
%{_sbindir}/named-journalprint
%{_sbindir}/nsec3hash
%{_sbindir}/rndc
%{_sbindir}/rndc-confgen
%{_docdir}/bind-9.9.1-P2/*
%{_mandir}/man*/*.gz

%clean
rm -rf %{buildroot}

%changelog
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
