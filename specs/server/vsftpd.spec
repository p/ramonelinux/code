Name:       vsftpd
Version:    3.0.0
Release:    1%{?dist}
Summary:    vsftpd

Group:      System Environment/libraries
License:    GPLv2+
Url:        https://security.appspot.com
Source:     https://security.appspot.com/downloads/%{name}-%{version}.tgz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  linux-pam openssl libcap

%description
The vsftpd package contains a very secure and very small FTP daemon. This is useful for serving files over a network.

%prep
%setup -q -n %{name}-%{version}

%build
make %{?_smp_mflags}

%check

%install
install -v -m 755 vsftpd        %{buildroot}/usr/sbin/vsftpd    &&
install -v -m 644 vsftpd.8      %{buildroot}/usr/share/man/man8 &&
install -v -m 644 vsftpd.conf.5 %{buildroot}/usr/share/man/man5 &&
install -v -m 644 vsftpd.conf   %{buildroot}/etc

cat >> %{buildroot}/etc/vsftpd.conf << "EOF"
background=YES
listen=YES
nopriv_user=vsftpd
secure_chroot_dir=/var/ftp/empty
EOF

%post
install -v -d -m 0755 /var/ftp/empty &&
install -v -d -m 0755 /home/ftp      &&
groupadd -g 47 vsftpd                &&
groupadd -g 45 ftp                   &&
useradd -c "vsftpd User"  -d /dev/null -g vsftpd -s /bin/false -u 47 vsftpd &&
useradd -c anonymous_user -d /home/ftp -g ftp    -s /bin/false -u 45 ftp

%files
%defattr(-,root,root,-)

%clean
rm -rf %{buildroot}

%changelog
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
