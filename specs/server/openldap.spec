Name:       openldap
Version:    2.4.39
Release:    3%{?dist}
Summary:    OpenLDAP

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.openldap.org
Source:     ftp://ftp.openldap.org/pub/OpenLDAP/openldap-release/%{name}-%{version}.tgz
Patch0:     openldap-%{version}-blfs_paths-ram-1.patch
Patch1:     openldap-%{version}-symbol_versions-1.patch

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  db cyrus-sasl openssl icu
BuildRequires:  autoconf automake m4 libtool groff

%description
The OpenLDAP package provides an open source implementation of the Lightweight Directory Access Protocol.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}
%patch0 -p1
%patch1 -p1

%build
autoconf &&
./configure --prefix=/usr         \
            --sysconfdir=/etc     \
            --localstatedir=/var  \
            --libexecdir=%{_libdir} \
            --disable-static      \
            --disable-debug       \
            --enable-dynamic      \
            --enable-crypt        \
            --enable-spasswd      \
            --enable-modules      \
            --enable-rlookups     \
            --enable-backends=mod \
            --enable-overlays=mod \
            --disable-ndb         \
            --disable-sql         \
            --libdir=%{_libdir} &&
make depend &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install &&

install -v -dm 755 %{buildroot}/usr/share/doc/openldap-%{version} &&
cp -vfr doc/drafts %{buildroot}/usr/share/doc/openldap-%{version} &&
cp -vfr doc/rfc    %{buildroot}/usr/share/doc/openldap-%{version} &&
cp -vfr doc/guide  %{buildroot}/usr/share/doc/openldap-%{version}

%files
%defattr(-,root,root,-)
/etc/openldap/schema/README
/etc/openldap/schema/*.ldif
/etc/openldap/schema/*.schema
/etc/openldap/*.*
%{_bindir}/ldapadd
%{_bindir}/ldapcompare
%{_bindir}/ldapdelete
%{_bindir}/ldapexop
%{_bindir}/ldapmodify
%{_bindir}/ldapmodrdn
%{_bindir}/ldappasswd
%{_bindir}/ldapsearch
%{_bindir}/ldapurl
%{_bindir}/ldapwhoami
%{_includedir}/lber*.h
%{_includedir}/ldap*.h
%{_includedir}/ldif.h
%{_includedir}/slapi-plugin.h
%{_libdir}/liblber*.*
%{_libdir}/libldap*.*
%{_libdir}/openldap/accesslog*.*
%{_libdir}/openldap/auditlog*.*
%{_libdir}/openldap/back_*.*
%{_libdir}/openldap/collect*.*
%{_libdir}/openldap/constraint*.*
%{_libdir}/openldap/dds*.*
%{_libdir}/openldap/deref*.*
%{_libdir}/openldap/dyngroup*.*
%{_libdir}/openldap/dynlist*.*
%{_libdir}/openldap/memberof*.*
%{_libdir}/openldap/pcache*.*
%{_libdir}/openldap/ppolicy*.*
%{_libdir}/openldap/refint*.*
%{_libdir}/openldap/retcode*.*
%{_libdir}/openldap/rwm*.*
%{_libdir}/openldap/seqmod*.*
%{_libdir}/openldap/sssvlv*.*
%{_libdir}/openldap/syncprov*.*
%{_libdir}/openldap/translucent*.*
%{_libdir}/openldap/unique*.*
%{_libdir}/openldap/valsort*.*
%{_sbindir}/slapacl
%{_sbindir}/slapadd
%{_sbindir}/slapauth
%{_sbindir}/slapcat
%{_sbindir}/slapd
%{_sbindir}/slapdn
%{_sbindir}/slapindex
%{_sbindir}/slappasswd
%{_sbindir}/slapschema
%{_sbindir}/slaptest
/var/lib/openldap/DB_CONFIG.example

%files doc
%defattr(-,root,root,-)
%{_docdir}/openldap-%{version}/*
%{_mandir}/man*/*.gz

%clean
rm -rf %{buildroot}

%changelog
* Tue Mar 18 2014 tanggeliang <tanggeliang@gmail.com>
- update from 2.4.38 to 2.4.39
* Thu Dec 5 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.4.36 to 2.4.38
* Sat Aug 24 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.4.35 to 2.4.36
* Sun Apr 14 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.4.34 to 2.4.35
* Tue Mar 19 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.4.33 to 2.4.34
* Fri Nov 2 2012 tanggeliang <tanggeliang@gmail.com>
- update from 2.4.32 to 2.4.33
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
