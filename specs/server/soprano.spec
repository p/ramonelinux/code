Name:       soprano
Version:    2.9.4
Release:    2%{?dist}
Summary:    Soprano

Group:      Applications/Internet
License:    GPLv2+
Url:        http://soprano.sourceforge.net
Source:     http://downloads.sourceforge.net/soprano/%{name}-%{version}.tar.bz2
Patch:      %{name}-%{version}-dbus-1.patch

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  cmake qt4 redland clucene
BuildRequires:  dbus libiodbc doxygen
Requires:       virtuoso

%description
Soprano (formally known as QRDF) is a library which provides a nice Qt interface to RDF storage solutions.
It has a modular structure which allows to replace the actual RDF storage implementation used.

%prep
%setup -q -n %{name}-%{version}
%patch -p1

%build
mkdir build &&
cd build &&
cmake -DCMAKE_INSTALL_PREFIX=/usr \
      -DCMAKE_BUILD_TYPE=Release  \
%ifarch x86_64
      -DLIB_SUFFIX=64 \
%endif
      .. &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
cd build &&
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/onto2vocabularyclass
%{_bindir}/sopranocmd
%{_bindir}/sopranod
%{_includedir}/Soprano/*
%{_includedir}/soprano/*.h
%{_libdir}/libsoprano.so*
%{_libdir}/libsopranoclient.so*
%{_libdir}/libsopranoindex.so*
%{_libdir}/libsopranoserver.so*
%{_libdir}/pkgconfig/soprano.pc
%{_libdir}/pkgconfig/sopranoclient.pc
%{_libdir}/pkgconfig/sopranoindex.pc
%{_libdir}/pkgconfig/sopranoserver.pc
%{_libdir}/soprano/libsoprano_*.so
%{_datadir}/dbus-1/interfaces/org.soprano.*.xml
%{_datadir}/soprano/cmake/SopranoAddOntology.cmake
%{_datadir}/soprano/plugins/*.desktop
%{_datadir}/soprano/rules/*.rules

%clean
rm -rf %{buildroot}

%changelog
* Tue Oct 22 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.9.3 to 2.9.4
* Mon Jul 29 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.9.2 to 2.9.3
* Thu Jul 25 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.9.0 to 2.9.2
* Wed Feb 27 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.8.0 to 2.9.0
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
