Name:       hadoop
Version:    2.2.0
Release:    1%{?dist}
Summary:    The Apache™ Hadoop®

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://hadoop.apache.org
Source:     http://mirror.bit.edu.cn/apache/hadoop/common/%{name}-%{version}/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  maven

%description
The Apache™ Hadoop® project develops open-source software for reliable, scalable, distributed computing.

%prep
%setup -q -n %{name}-%{version}

%build
mvn package -Pdist,native -DskipTests -Dtar

%check

%install

%files
%defattr(-,root,root,-)

%clean
rm -rf %{buildroot}

%changelog
* Mon Feb 17 2014 tanggeliang <tanggeliang@gmail.com>
- create
