Name:       virtuoso
Version:    7.1.0
Release:    1%{?dist}
Summary:    Virtuoso

Group:      Applications/Internet
License:    GPLv2+
Url:        http://virtuoso.sourceforge.net
Source:     http://downloads.sourceforge.net/virtuoso/%{name}-opensource-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  libiodbc libxml openssl openldap
BuildRequires:  imagemagick krb

%description
Virtuoso is a cross-platform server that implements multiple server-side protocols as part of a single-server product offering.
There is one server product that offers WebDAV/HTTP, Application, and Database-server functionality alongside Native XML Storage, Universal Data-Access Middleware, Business Process Integration and a Web-Services Platform.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-opensource-%{version}

%build
sed -i "s|virt_iodbc_dir/include|&/iodbc|" configure  &&
./configure --prefix=/usr                             \
            --sysconfdir=/etc                         \
            --localstatedir=/var                      \
            --with-iodbc=/usr                         \
            --with-readline                           \
            --without-internal-zlib                   \
            --program-transform-name="s/isql/isql-v/" \
            --disable-all-vads                        \
            --disable-static                          \
            --libdir=%{_libdir}                       &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install &&
install -v -m755 -d %{buildroot}/usr/share/doc/virtuoso-%{version} &&
ln -s   -v          ../../virtuoso/doc \
                    %{buildroot}/usr/share/doc/virtuoso-%{version}

mkdir -p %{buildroot}/lib/systemd/system
cat > %{buildroot}/lib/systemd/system/virtuoso.service << "EOF"
[Unit]
Description=Virtuoso Server
After=network.target

[Service]
Type=forking
ExecStart=/usr/bin/virtuoso-t -c /var/lib/virtuoso/db/virtuoso.ini +wait
Restart=always

[Install]
WantedBy=multi-user.target
EOF

%files
%defattr(-,root,root,-)
%{_bindir}/inifile
%{_bindir}/isql-v
%{_bindir}/isql-v-iodbc
%{_bindir}/isql-vw
%{_bindir}/isql-vw-iodbc
%{_bindir}/odbc_mail
%{_bindir}/virt_mail
%{_bindir}/virtuoso-iodbc-t
%{_bindir}/virtuoso-t
%{_libdir}/hibernate/virt_dialect.jar
%{_libdir}/jdbc-*.0/virtjdbc*.jar
%{_libdir}/jdbc-4.1/virtjdbc4_1.jar
%{_libdir}/jdbc-4.1/virtjdbc4ssl.jar
%{_libdir}/jena/virt_jena.jar
%{_libdir}/jena2/virt_jena2.jar
%{_libdir}/libvirtuoso-iodbc-t.*a
%{_libdir}/libvirtuoso-t.*a
%{_libdir}/sesame/create-virtuoso.xsl
%{_libdir}/sesame/create.xsl
%{_libdir}/sesame/virt_sesame*.jar
%{_libdir}/virtodbc.la
%{_libdir}/virtodbc.so
%{_libdir}/virtodbc_r.la
%{_libdir}/virtodbc_r.so
%{_libdir}/virtodbcu.la
%{_libdir}/virtodbcu.so
%{_libdir}/virtodbcu_r.la
%{_libdir}/virtodbcu_r.so
%{_libdir}/virtuoso/hosting/im.la
%{_libdir}/virtuoso/hosting/im.so
/var/lib/virtuoso/db/virtuoso.ini
/var/lib/virtuoso/vsp/admin/index_left.vsp
/var/lib/virtuoso/vsp/*.css
/var/lib/virtuoso/vsp/images/*.gif
/var/lib/virtuoso/vsp/images/*.jpg
/var/lib/virtuoso/vsp/images/*.png
/var/lib/virtuoso/vsp/*.html
/var/lib/virtuoso/vsp/robots.txt
/var/lib/virtuoso/vsp/vsmx/*.gif
/var/lib/virtuoso/vsp/vsmx/default.css
/var/lib/virtuoso/vsp/vsmx/*.vspx
/var/lib/virtuoso/vsp/vsmx/slvnav.jpg
/var/lib/virtuoso/vsp/vsmx/vsmx_*.xsl
/lib/systemd/system/virtuoso.service

%post
systemctl enable virtuoso

%postun
systemctl disable virtuoso

%files doc
%defattr(-,root,root,-)
%{_datadir}/virtuoso/doc/*
%{_docdir}/virtuoso-%{version}/*

%clean
rm -rf %{buildroot}

%changelog
* Thu Mar 20 2014 tanggeliang <tanggeliang@gmail.com>
- update from 7.0.0 to 7.1.0
* Mon Sep 30 2013 tanggeliang <tanggeliang@gmail.com>
- update from 6.1.7 to 7.0.0
* Sat Aug 24 2013 tanggeliang <tanggeliang@gmail.com>
- update from 6.1.6 to 6.1.7
* Tue Mar 19 2013 tanggeliang <tanggeliang@gmail.com>
- create
