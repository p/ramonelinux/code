Name:       apache-httpd
Version:    2.4.7
Release:    1%{?dist}
Summary:    Apache HTTPD 

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.apache.org
Source:     http://archive.apache.org/dist/httpd/httpd-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  apr-util openssl
BuildRequires:  db openldap pcre rsync
Requires:       systemd

%description
The Apache HTTPD package contains an open-source HTTP server.
It is useful for creating local intranet web sites or running huge web serving operations.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n httpd-%{version}

%build
./configure --enable-layout=RPM \
            --enable-mods-shared="all cgi" \
            --enable-mpms-shared=all \
            --with-apr=/usr/bin/apr-1-config \
            --with-apr-util=/usr/bin/apu-1-config \
            --enable-suexec=shared \
            --with-suexec-bin=%{_libdir}/httpd/suexec \
            --with-suexec-docroot=/srv/www \
            --with-suexec-caller=apache \
            --with-suexec-userdir=public_html \
            --with-suexec-logfile=/var/log/httpd/suexec.log \
            --with-suexec-uidmin=100 \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install &&

mv -v %{buildroot}/usr/sbin/suexec %{buildroot}/%{_libdir}/httpd/suexec

mkdir -p %{buildroot}/lib/systemd/system
cat > %{buildroot}/lib/systemd/system/httpd.service << "EOF"
[Unit]
Description=The Apache HTTP Server
After=syslog.target network.target

[Service]
Type=forking
ExecStart=/usr/sbin/httpd $OPTIONS -k start
ExecReload=/usr/sbin/httpd $OPTIONS -t
ExecReload=/bin/kill -HUP $MAINPID
ExecStop=/usr/sbin/httpd $OPTIONS -k stop
PrivateTmp=true

[Install]
WantedBy=multi-user.target
EOF

%post
groupadd -g 25 apache &&
useradd -c "Apache Server" -d /srv/www -g apache \
        -s /bin/false -u 25 apache

systemctl enable httpd

mkdir -p /srv/www &&
chown -v -R apache:apache /srv/www

chgrp apache %{_libdir}/httpd/suexec &&
chmod 4754 %{_libdir}/httpd/suexec

useradd -g daemon daemon &&
mkdir -p /var/log/httpd

%postun
systemctl disable httpd

%files
%defattr(-,root,root,-)
/etc/httpd/conf/extra/httpd-*.conf
/etc/httpd/conf/extra/proxy-html.conf
/etc/httpd/conf/httpd.conf
/etc/httpd/conf/magic
/etc/httpd/conf/mime.types
/etc/httpd/conf/original/extra/httpd-*.conf
/etc/httpd/conf/original/extra/proxy-html.conf
/etc/httpd/conf/original/httpd.conf
%{_bindir}/ab
%{_bindir}/apxs
%{_bindir}/dbmmanage
%{_bindir}/htdbm
%{_bindir}/htdigest
%{_bindir}/htpasswd
%{_bindir}/httxt2dbm
%{_bindir}/logresolve
%{_includedir}/httpd/*.h
%{_libdir}/httpd/build/*
%{_libdir}/httpd/modules/httpd.exp
%{_libdir}/httpd/modules/mod_*.so
%{_libdir}/httpd/suexec
%{_sbindir}/apachectl
%{_sbindir}/checkgid
%{_sbindir}/envvars
%{_sbindir}/envvars-std
%{_sbindir}/fcgistarter
%{_sbindir}/htcacheclean
%{_sbindir}/httpd
%{_sbindir}/rotatelogs
/var/www/cgi-bin/printenv
/var/www/cgi-bin/printenv.vbs
/var/www/cgi-bin/printenv.wsf
/var/www/cgi-bin/test-cgi
/var/www/error/HTTP_BAD_GATEWAY.html.var
/var/www/error/HTTP_BAD_REQUEST.html.var
/var/www/error/HTTP_FORBIDDEN.html.var
/var/www/error/HTTP_GONE.html.var
/var/www/error/HTTP_INTERNAL_SERVER_ERROR.html.var
/var/www/error/HTTP_LENGTH_REQUIRED.html.var
/var/www/error/HTTP_METHOD_NOT_ALLOWED.html.var
/var/www/error/HTTP_NOT_FOUND.html.var
/var/www/error/HTTP_NOT_IMPLEMENTED.html.var
/var/www/error/HTTP_PRECONDITION_FAILED.html.var
/var/www/error/HTTP_REQUEST_ENTITY_TOO_LARGE.html.var
/var/www/error/HTTP_REQUEST_TIME_OUT.html.var
/var/www/error/HTTP_REQUEST_URI_TOO_LARGE.html.var
/var/www/error/HTTP_SERVICE_UNAVAILABLE.html.var
/var/www/error/HTTP_UNAUTHORIZED.html.var
/var/www/error/HTTP_UNSUPPORTED_MEDIA_TYPE.html.var
/var/www/error/HTTP_VARIANT_ALSO_VARIES.html.var
/var/www/error/README
/var/www/error/contact.html.var
/var/www/error/include/bottom.html
/var/www/error/include/spacer.html
/var/www/error/include/top.html
/var/www/html/index.html
/var/www/icons/*
/var/www/manual/*
/lib/systemd/system/httpd.service

%files doc
%defattr(-,root,root,-)
%{_mandir}/man1/*
%{_mandir}/man8/*

%clean
rm -rf %{buildroot}

%changelog
* Mon Feb 17 2014 tanggeliang <tanggeliang@gmail.com>
- update from 2.4.6 to 2.4.7
* Thu Aug 15 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.4.4 to 2.4.6
* Fri Jul 12 2013 tanggeliang <tanggeliang@gmail.com>
- create
