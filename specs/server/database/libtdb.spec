Name:       libtdb
Version:    1.2.12
Release:    1%{?dist}
Summary:    The tdb library

Group:      System Environment/Daemons
License:    LGPLv3+
URL:        http://tdb.samba.org/
Source:     http://samba.org/ftp/tdb/tdb-%{version}.tar.gz

BuildRoot:      %(mktemp -ud %{_tmppath}/%{name}-%{version}-%{release}-XXXXXX)
BuildRequires:  autoconf libxslt docbook-xsl python
Provides:       bundled(libreplace)

%description
A library that implements a trivial database.

%prep
%setup -q -n tdb-%{version}

%build
%configure --disable-rpath \
           --bundled-libraries=NONE \
           --builtin-libraries=replace
make %{?_smp_mflags} V=1

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%{_libdir}/libtdb.so.*
%{_includedir}/tdb.h
%{_libdir}/libtdb.so
%{_libdir}/pkgconfig/tdb.pc
%{_libdir}/python2.7/site-packages/tdb.so
%{_bindir}/tdbbackup
%{_bindir}/tdbdump
%{_bindir}/tdbtool
%{_bindir}/tdbrestore
%{_mandir}/man8/*

%changelog
* Wed Jun 19 2013 tanggeliang <tanggeliang@gmail.com>
- create
