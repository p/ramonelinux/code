Name:       db
Version:    6.1.19
Release:    1%{?dist}
Summary:    Berkeley DB

Group:      System Environment/libraries
License:    GPLv2+
Url:        www.oracle.com
Source:     http://download.oracle.com/berkeley-db/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  tcl
Requires:       db-libdb db-libdb_cxx

%description
The Berkeley DB package contains programs and utilities used by many other applications for database related functions.

%package        libdb
Summary:        libdb
%description    libdb

%package        libdb_cxx
Summary:        libdb_cxx
%description    libdb_cxx

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
cd build_unix                        &&
../dist/configure --prefix=/usr      \
                  --enable-compat185 \
                  --enable-dbm       \
                  --disable-static   \
                  --enable-cxx       \
                  --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
cd build_unix &&
make DESTDIR=%{buildroot} docdir=/usr/share/doc/%{name}-%{version} install

# Avoid Permission denied. strip when building as non-root.
chmod u+w %{buildroot}/%{_bindir} %{buildroot}/%{_bindir}/*

#chown -v -R root:root                        \
#      %{buildroot}/usr/bin/db_*                          \
#      %{buildroot}/usr/include/db{,_185,_cxx}.h          \
#      %{buildroot}/%{_libdir}/libdb*.{so,la}                \
#      %{buildroot}/usr/share/doc/db-%{version}

%files
%defattr(-,root,root,-)
%{_bindir}/db_archive
%{_bindir}/db_checkpoint
%{_bindir}/db_deadlock
%{_bindir}/db_dump
%{_bindir}/db_hotbackup
%{_bindir}/db_load
%{_bindir}/db_log_verify
%{_bindir}/db_printlog
%{_bindir}/db_recover
%{_bindir}/db_replicate
%{_bindir}/db_stat
%{_bindir}/db_tuner
%{_bindir}/db_upgrade
%{_bindir}/db_verify
%{_includedir}/db*.h
%{_libdir}/libdb*-6.1.la

%files libdb
%defattr(-,root,root,-)
%{_libdir}/libdb*.so

%files libdb_cxx
%defattr(-,root,root,-)
%{_libdir}/libdb_cxx*.so

%files doc
%defattr(-,root,root,-)
%{_docdir}/db-%{version}/*

%clean
rm -rf %{buildroot}

%changelog
* Thu Jul 24 2014 tanggeliang <tanggeliang@gmail.com>
- update from 6.0.20 to 6.1.19
* Sun Aug 4 2013 tanggeliang <tanggeliang@gmail.com>
- update from 5.3.21 to 6.0.20
* Sun Apr 22 2012 tanggeliang <tanggeliang@gmail.com>
- update to 5.3.21
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
