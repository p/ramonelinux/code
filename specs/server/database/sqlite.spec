%define _version 3081101

Name:       sqlite
Version:    3.8.11.1
Release:    1%{?dist}
Summary:    SQLite

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.sqlite.org
Source0:    http://sqlite.org/%{name}-autoconf-%{_version}.tar.gz
Source1:    sqlite-doc-%{_version}.zip

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  unzip

%description
The SQLite package is a software library that implements a self-contained, serverless, zero-configuration, transactional SQL database engine.

%package        doc
Summary:        SQLite Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-autoconf-%{_version}

%build
unzip -q %SOURCE1
./configure --prefix=/usr --disable-static	  \
            --libdir=%{_libdir}			  \
            CFLAGS="-g -O2 -DSQLITE_ENABLE_FTS3=1 \
            -DSQLITE_ENABLE_COLUMN_METADATA=1     \
            -DSQLITE_ENABLE_UNLOCK_NOTIFY=1       \
            -DSQLITE_SECURE_DELETE=1 		  \
	    -DSQLITE_ENABLE_DBSTAT_VTAB=1" &&
make %{?_smp_mflags}

%check

%install
make DESTDIR=%{buildroot} install

install -v -m755 -d %{buildroot}/usr/share/doc/sqlite-%{version} &&
cp -v -R sqlite-doc-%{_version}/* %{buildroot}/usr/share/doc/sqlite-%{version}

%files
%defattr(-,root,root,-)
%{_bindir}/sqlite3
%{_includedir}/sqlite3*.h
%{_libdir}/libsqlite3.la
%{_libdir}/libsqlite3.so*
%{_libdir}/pkgconfig/sqlite3.pc

%files doc
%defattr(-,root,root,-)
%{_docdir}/sqlite-%{version}/*
%{_mandir}/man1/sqlite3.1.gz

%clean
rm -rf %{buildroot}

%changelog
* Sat Sep 26 2015 tanggeliang <tanggeliang@gmail.com>
- update from 3.8.5 to 3.8.11.1
* Sun Jul 20 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.8.4.1 to 3.8.5
* Thu Mar 20 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.8.2 to 3.8.4.1
* Sun Dec 15 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.8.1 to 3.8.2
* Tue Oct 22 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.8.0.2 to 3.8.1
* Thu Oct 3 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.7.17 to 3.8.0.2
* Thu Jul 25 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.7.16.1 to 3.7.17
* Fri Apr 5 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.7.14.1 to 3.7.16.1
* Fri Nov 2 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.7.14 to 3.7.14.1
* Fri Sep 28 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.7.13 to 3.7.14
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
