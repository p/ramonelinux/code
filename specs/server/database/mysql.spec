Name:       mysql
Version:    5.6.14
Release:    1%{?dist}
Summary:    MySQL

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.mysql.com
Source:     http://cdn.mysql.com/Downloads/MySQL-5.6/%{name}-%{version}.tar.gz
Patch:      mysql-%{version}-embedded_library_shared-1.patch

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  cmake libevent openssl
AutoReq:        no

%description
MySQL is a widely used and fast SQL database server.
It is a client/server implementation that consists of a server daemon and many different client programs and libraries.

%package        test
Summary:        Test
%description    test

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}
%patch -p1

%build
sed -i "/ADD_SUBDIRECTORY(sql\/share)/d" CMakeLists.txt &&
sed -i "s/ADD_SUBDIRECTORY(libmysql)/&\\nADD_SUBDIRECTORY(sql\/share)/" CMakeLists.txt &&
sed -i "s@data/test@\${INSTALL_MYSQLSHAREDIR}@g" sql/CMakeLists.txt &&
sed -i "s@data/mysql@\${INSTALL_MYSQLTESTDIR}@g" sql/CMakeLists.txt &&

sed -i "s/srv_buf_size/srv_sort_buf_size/" storage/innobase/row/row0log.cc &&

mkdir build &&
cd build &&

cmake -DCMAKE_BUILD_TYPE=Release                    \
      -DCMAKE_INSTALL_PREFIX=/usr                   \
      -DINSTALL_DOCDIR=share/doc/mysql              \
      -DINSTALL_DOCREADMEDIR=share/doc/mysql        \
      -DINSTALL_INCLUDEDIR=include/mysql            \
      -DINSTALL_INFODIR=share/info                  \
      -DINSTALL_MANDIR=share/man                    \
      -DINSTALL_MYSQLDATADIR=/srv/mysql             \
      -DINSTALL_MYSQLSHAREDIR=share/mysql           \
      -DINSTALL_MYSQLTESTDIR=share/mysql/test       \
      -DINSTALL_PLUGINDIR=%{_lib}/mysql/plugin      \
      -DINSTALL_SBINDIR=sbin                        \
      -DINSTALL_SCRIPTDIR=bin                       \
      -DINSTALL_SQLBENCHDIR=share/mysql/bench       \
      -DINSTALL_SUPPORTFILESDIR=share/mysql         \
      -DMYSQL_DATADIR=/srv/mysql                    \
      -DMYSQL_UNIX_ADDR=/run/mysqld/mysqld.sock     \
      -DSYSCONFDIR=/etc/mysql                       \
      -DWITH_PARTITION_STORAGE_ENGINE=OFF           \
      -DWITH_PERFSCHEMA_STORAGE_ENGINE=OFF          \
      -DWITH_EXTRA_CHARSETS=complex                 \
      -DWITH_LIBEVENT=system                        \
      -DWITH_SSL=system                             \
%ifarch x86_64
      -DINSTALL_LIBDIR=%{_lib}                      \
%endif
      .. &&
make %{?_smp_mflags}

%check

%install
cd build &&
make install DESTDIR=%{buildroot}

%files
%defattr(-,root,root,-)
%{_bindir}/innochecksum
%{_bindir}/msql2mysql
%{_bindir}/my_print_defaults
%{_bindir}/myisam_ftdump
%{_bindir}/myisamchk
%{_bindir}/myisamlog
%{_bindir}/myisampack
%{_bindir}/mysql
%{_bindir}/mysql_client_test
%{_bindir}/mysql_client_test_embedded
%{_bindir}/mysql_config
%{_bindir}/mysql_config_editor
%{_bindir}/mysql_convert_table_format
%{_bindir}/mysql_embedded
%{_bindir}/mysql_find_rows
%{_bindir}/mysql_fix_extensions
%{_bindir}/mysql_install_db
%{_bindir}/mysql_plugin
%{_bindir}/mysql_secure_installation
%{_bindir}/mysql_setpermission
%{_bindir}/mysql_tzinfo_to_sql
%{_bindir}/mysql_upgrade
%{_bindir}/mysql_waitpid
%{_bindir}/mysql_zap
%{_bindir}/mysqlaccess
%{_bindir}/mysqlaccess.conf
%{_bindir}/mysqladmin
%{_bindir}/mysqlbinlog
%{_bindir}/mysqlbug
%{_bindir}/mysqlcheck
%{_bindir}/mysqld_multi
%{_bindir}/mysqld_safe
%{_bindir}/mysqldump
%{_bindir}/mysqldumpslow
%{_bindir}/mysqlhotcopy
%{_bindir}/mysqlimport
%{_bindir}/mysqlshow
%{_bindir}/mysqlslap
%{_bindir}/mysqltest
%{_bindir}/mysqltest_embedded
%{_bindir}/perror
%{_bindir}/replace
%{_bindir}/resolve_stack_dump
%{_bindir}/resolveip
%{_includedir}/mysql/*.h
%{_includedir}/mysql/mysql/*
%{_libdir}/libmysqlclient.a
%{_libdir}/libmysqlclient.so*
%{_libdir}/libmysqlclient_r.a
%{_libdir}/libmysqlclient_r.so*
%{_libdir}/libmysqld.a
%{_libdir}/libmysqld.so*
%{_libdir}/libmysqlservices.a
%{_libdir}/mysql/plugin/adt_null.so
%{_libdir}/mysql/plugin/auth.so
%{_libdir}/mysql/plugin/auth_socket.so
%{_libdir}/mysql/plugin/auth_test_plugin.so
%{_libdir}/mysql/plugin/daemon_example.ini
%{_libdir}/mysql/plugin/libdaemon_example.so
%{_libdir}/mysql/plugin/mypluglib.so
%{_libdir}/mysql/plugin/qa_auth_client.so
%{_libdir}/mysql/plugin/qa_auth_interface.so
%{_libdir}/mysql/plugin/qa_auth_server.so
%{_libdir}/mysql/plugin/semisync_master.so
%{_libdir}/mysql/plugin/semisync_slave.so
%{_libdir}/mysql/plugin/validate_password.so
%{_sbindir}/mysqld
%{_datadir}/aclocal/mysql.m4
%{_datadir}/mysql/bench/sql-bench/Data/ATIS/*.txt
%{_datadir}/mysql/bench/sql-bench/Data/Wisconsin/onek.data
%{_datadir}/mysql/bench/sql-bench/Data/Wisconsin/tenk.data
%{_datadir}/mysql/bench/sql-bench/README
%{_datadir}/mysql/bench/sql-bench/bench-count-distinct
%{_datadir}/mysql/bench/sql-bench/bench-init.pl
%{_datadir}/mysql/bench/sql-bench/compare-results
%{_datadir}/mysql/bench/sql-bench/copy-db
%{_datadir}/mysql/bench/sql-bench/crash-me
%{_datadir}/mysql/bench/sql-bench/graph-compare-results
%{_datadir}/mysql/bench/sql-bench/innotest*
%{_datadir}/mysql/bench/sql-bench/limits/*.cfg
%{_datadir}/mysql/bench/sql-bench/run-all-tests
%{_datadir}/mysql/bench/sql-bench/server-cfg
%{_datadir}/mysql/bench/sql-bench/test-*
%{_datadir}/mysql/binary-configure
%{_datadir}/mysql/*/errmsg.sys
%{_datadir}/mysql/charsets/*.xml
%{_datadir}/mysql/charsets/README
%{_datadir}/mysql/db.opt
%{_datadir}/mysql/dictionary.txt
%{_datadir}/mysql/errmsg-utf8.txt
%{_datadir}/mysql/fill_help_tables.sql
%{_datadir}/mysql/innodb_memcached_config.sql
%{_datadir}/mysql/magic
%{_datadir}/mysql/my-default.cnf
%{_datadir}/mysql/mysql-log-rotate
%{_datadir}/mysql/mysql.server
%{_datadir}/mysql/mysql_*.sql
%{_datadir}/mysql/mysqld_multi.server
%{_datadir}/mysql/solaris/postinstall-solaris

%files test
%defattr(-,root,root,-)
%{_datadir}/mysql/test/*

%files doc
%defattr(-,root,root,-)
%{_docdir}/mysql/*
%{_infodir}/mysql.info.gz
%{_mandir}/man1/*.1.gz
%{_mandir}/man8/mysqld.8.gz

%post
groupadd -g 40 mysql &&
useradd -c "MySQL Server" -d /dev/null -g mysql -s /bin/false -u 40 mysql

mysql_install_db --basedir=/usr --datadir=/srv/mysql --user=mysql &&
chown -R mysql:mysql /srv/mysql

install -v -m755 -o mysql -g mysql -d /var/run/mysql &&
mysqld_safe --user=mysql 2>&1 >/dev/null &

%clean
rm -rf %{buildroot}

%changelog
* Tue Oct 22 2013 tanggeliang <tanggeliang@gmail.com>
- update from 5.6.13 to 5.6.14
* Mon Oct 7 2013 tanggeliang <tanggeliang@gmail.com>
- update from 5.6.12 to 5.6.13
* Mon Jul 22 2013 tanggeliang <tanggeliang@gmail.com>
- update from 5.6.11 to 5.6.12
* Tue Jun 18 2013 tanggeliang <tanggeliang@gmail.com>
- update from 5.5.28 to 5.6.11
* Sat Dec 1 2012 tanggeliang <tanggeliang@gmail.com>
- create
