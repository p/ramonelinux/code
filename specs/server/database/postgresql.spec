Name:       postgresql
Version:    9.1.5
Release:    5%{?dist}
Summary:    PostgreSQL

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.postgresql.org
Source:     ftp://ftp5.us.postgresql.org/pub/PostgreSQL/source/v9.1.5/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  python tcl openssl libxml libxslt openldap linux-pam krb bonjour

%description
PostgreSQL is an advanced object-relational database management system (ORDBMS), derived from the Berkeley Postgres database management system.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
  --docdir=/usr/share/doc/postgresql-9.1.5 \
  --enable-thread-safety &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot} &&
make install-docs DESTDIR=%{buildroot} &&
chown -v root:root %{buildroot}/usr/share/doc/postgresql-9.1.5/html/*

%files
%defattr(-,root,root,-)

%post
install -v -m700 -d /srv/pgsql/data &&
groupadd -g 41 postgres &&
useradd -c "PostgreSQL Server" -g postgres -d /srv/pgsql/data \
        -u 41 postgres &&
chown -v postgres /srv/pgsql/data &&
su - postgres -c '/usr/bin/initdb -D /srv/pgsql/data'

%clean
rm -rf %{buildroot}

%changelog
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
