Name:       lxappearance
Version:    0.5.3
Release:    2%{?dist}
Summary:    LXAppearance

Group:      User Interface/LXDE
License:    GPLv2+
Url:        http://lxde.org
Source:     http://sourceforge.net/lxde/files/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  gtk2 dbus-glib pkg-config
BuildRequires:  intltool gettext xml-parser
BuildRequires:  libxslt docbook-xml docbook-xsl

%description
The LXAppearance package contains a desktop-independent theme switcher for GTK+.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --sysconfdir=/etc \
            --disable-static  \
            --enable-dbus     \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/lxappearance
%{_includedir}/lxappearance/lxappearance.h
%{_libdir}/pkgconfig/lxappearance.pc
%{_datadir}/applications/lxappearance.desktop
%{_datadir}/locale/*/LC_MESSAGES/lxappearance.mo
%{_datadir}/lxappearance/ui/about.ui
%{_datadir}/lxappearance/ui/lxappearance.ui
%{_mandir}/man1/lxappearance.1.gz

%clean
rm -rf %{buildroot}

%changelog
* Fri Nov 8 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.5.2 to 0.5.3
* Wed Jun 19 2013 tanggeliang <tanggeliang@gmail.com>
- create
