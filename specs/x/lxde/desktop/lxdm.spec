Name:       lxdm
Version:    0.4.1
Release:    2%{?dist}
Summary:    LXDM - GUI login manager for LXDE

Group:      User Interface/LXDE
License:    GPLv2+
Url:        http://lxde.org
Source:     http://sourceforge.net/lxde/files/%{name}-%{version}.tar.gz
Patch:      lxdm-0.4.1-tcp-listen.patch

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  gtk2 iso-codes consolekit
BuildRequires:  intltool gettext xml-parser
BuildRequires:  systemd
Requires:       systemd

%description
A lightweight dropped-in replacement for GDM or KDM.

%prep
%setup -q -n %{name}-%{version}
%patch -p1

%build
./configure --prefix=/usr \
            --sysconfdir=/etc \
            --libexecdir=%{_libdir}/lxdm &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

cat > %{buildroot}/etc/pam.d/lxdm <<"EOF"
# Begin /etc/pam.d/lxdm

auth     requisite      pam_nologin.so
auth     required       pam_env.so

auth     required       pam_succeed_if.so uid >= 1000 quiet
auth     include        system-auth
auth     optional       pam_gnome_keyring.so

account  include        system-account
password include        system-password

session  required       pam_limits.so
session  include        system-session
session  optional       pam_gnome_keyring.so auto_start

# End /etc/pam.d/lxdm
EOF

mkdir -p %{buildroot}/lib/systemd/system
cat > %{buildroot}/lib/systemd/system/lxdm.service << "EOF"
[Unit]
Description=LXDM (Lightweight X11 Display Manager)
#Documentation=man:lxdm(8)
Conflicts=getty@tty1.service
After=systemd-user-sessions.service getty@tty1.service plymouth-quit.service livesys-late.service
#Conflicts=plymouth-quit.service

[Service]
ExecStart=/usr/sbin/lxdm
Restart=always
IgnoreSIGPIPE=no
#BusName=org.freedesktop.lxdm

[Install]
Alias=display-manager.service
EOF

%files
%defattr(-,root,root,-)
/etc/lxdm/LoginReady
/etc/lxdm/PostLogin
/etc/lxdm/PostLogout
/etc/lxdm/PreLogin
/etc/lxdm/PreReboot
/etc/lxdm/PreShutdown
/etc/lxdm/Xsession
/etc/lxdm/lxdm.conf
/etc/pam.d/lxdm
%{_bindir}/lxdm-config
%{_libdir}/lxdm/lxdm-greeter-gdk
%{_libdir}/lxdm/lxdm-greeter-gtk
%{_libdir}/lxdm/lxdm-numlock
%{_sbindir}/lxdm
%{_sbindir}/lxdm-binary
%{_datadir}/locale/*/LC_MESSAGES/lxdm.mo
%{_datadir}/lxdm/config.ui
%{_datadir}/lxdm/lxdm.glade
%{_datadir}/lxdm/themes/Industrial/greeter*.ui
%{_datadir}/lxdm/themes/Industrial/gtk.css
%{_datadir}/lxdm/themes/Industrial/gtkrc
%{_datadir}/lxdm/themes/Industrial/index.theme
%{_datadir}/lxdm/themes/Industrial/*.png
%{_datadir}/lxdm/themes/Industrial/wave.svg
/lib/systemd/system/lxdm.service

%post
systemctl enable lxdm

%postun
systemctl disable lxdm

%clean
rm -rf %{buildroot}

%changelog
* Thu Jun 20 2013 tanggeliang <tanggeliang@gmail.com>
- create
