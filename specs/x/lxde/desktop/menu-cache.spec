Name:       menu-cache
Version:    0.5.1
Release:    2%{?dist}
Summary:    Menu-cache

Group:      User Interface/LXDE
License:    GPLv2+
Url:        http://lxde.org
Source:     http://sourceforge.net/lxde/files/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  glib pkg-config

%description
The Menu Cache package contains a library for creating and utilizing caches to speed up the manipulation for freedesktop.org defined application menus.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --libexecdir=%{_libdir} \
            --disable-static \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_includedir}/menu-cache/menu-cache.h
%{_libdir}/libmenu-cache.la
%{_libdir}/libmenu-cache.so*
%{_libdir}/menu-cache/menu-cache-gen
%{_libdir}/menu-cache/menu-cached
%{_libdir}/pkgconfig/libmenu-cache.pc

%clean
rm -rf %{buildroot}

%changelog
* Fri Nov 8 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.4.1 to 0.5.1
* Wed Jun 19 2013 tanggeliang <tanggeliang@gmail.com>
- create
