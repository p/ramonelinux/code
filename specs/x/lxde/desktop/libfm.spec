Name:       libfm
Version:    1.1.4
Release:    1%{?dist}
Summary:    Library of PCMan file manager

Group:      User Interface/LXDE
License:    GPLv2+
Url:        http://lxde.org
Source:     http://sourceforge.net/lxde/files/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  gtk2 menu-cache
BuildRequires:  intltool gettext xml-parser
BuildRequires:  libexif vala lxmenu-data
BuildRequires:  dbus-glib udisks

%description
The libfm package contains a library used to develop file managers providing some file management utilities.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --sysconfdir=/etc \
            --disable-static \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
/etc/xdg/libfm/libfm.conf
/etc/xdg/libfm/pref-apps.conf
%{_bindir}/libfm-pref-apps
%{_includedir}/libfm
%{_includedir}/libfm-1.0/fm-*.h
%{_includedir}/libfm-1.0/fm.h
%{_libdir}/libfm-gtk.la
%{_libdir}/libfm-gtk.so*
%{_libdir}/libfm.la
%{_libdir}/libfm.so*
%{_libdir}/pkgconfig/libfm-gtk.pc
%{_libdir}/pkgconfig/libfm-gtk3.pc
%{_libdir}/pkgconfig/libfm.pc
%{_datadir}/applications/libfm-pref-apps.desktop
%{_datadir}/libfm/archivers.list
%{_datadir}/libfm/ui/*.ui
%{_datadir}/locale/*/LC_MESSAGES/libfm.mo
%{_datadir}/mime/packages/libfm.xml
%{_mandir}/man1/libfm-pref-apps.1.gz

%clean
rm -rf %{buildroot}

%changelog
* Sun Dec 15 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.1.2.2 to 1.1.4
* Fri Nov 8 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.1.0 to 1.1.2.2
* Wed Jun 19 2013 tanggeliang <tanggeliang@gmail.com>
- create
