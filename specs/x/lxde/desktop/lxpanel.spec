Name:       lxpanel
Version:    0.6.1
Release:    2%{?dist}
Summary:    LXPanel (desktop panel)

Group:      User Interface/LXDE
License:    GPLv2+
Url:        http://lxde.org
Source:     http://sourceforge.net/lxde/files/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  gtk2 pkg-config menu-cache lxmenu-data
BuildRequires:  alsa-lib libwnck2 wireless-tools
BuildRequires:  intltool gettext xml-parser
BuildRequires:  libxslt docbook-xml docbook-xsl

%description
The LXPanel package contains a lightweight X11 desktop panel.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/lxpanel
%{_bindir}/lxpanelctl
%{_includedir}/lxpanel/plugin.h
%{_libdir}/lxpanel/plugins/batt.so
%{_libdir}/lxpanel/plugins/cpu.so
%{_libdir}/lxpanel/plugins/cpufreq.so
%{_libdir}/lxpanel/plugins/deskno.so
%{_libdir}/lxpanel/plugins/kbled.so
%{_libdir}/lxpanel/plugins/monitors.so
%{_libdir}/lxpanel/plugins/netstat.so
%{_libdir}/lxpanel/plugins/netstatus.so
%{_libdir}/lxpanel/plugins/thermal.so
%{_libdir}/lxpanel/plugins/volumealsa.so
%{_libdir}/lxpanel/plugins/wnckpager.so
%{_libdir}/lxpanel/plugins/xkb.so
%{_libdir}/pkgconfig/lxpanel.pc
%{_datadir}/locale/*/LC_MESSAGES/lxpanel.mo
%{_datadir}/lxpanel/images/*.png
%{_datadir}/lxpanel/images/xkb-flags/*.png
%{_datadir}/lxpanel/profile/default/config
%{_datadir}/lxpanel/profile/default/panels/panel
%{_datadir}/lxpanel/profile/two_panels/config
%{_datadir}/lxpanel/profile/two_panels/panels/bottom
%{_datadir}/lxpanel/profile/two_panels/panels/top
%{_datadir}/lxpanel/ui/launchbar.ui
%{_datadir}/lxpanel/ui/netstatus.ui
%{_datadir}/lxpanel/ui/panel-pref.ui
%{_datadir}/lxpanel/xkeyboardconfig/layouts.cfg
%{_datadir}/lxpanel/xkeyboardconfig/models.cfg
%{_datadir}/lxpanel/xkeyboardconfig/toggle.cfg
%{_mandir}/man1/lxpanel*.1.gz

%clean
rm -rf %{buildroot}

%changelog
* Fri Nov 8 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.5.12 to 0.6.1
* Wed Jun 19 2013 tanggeliang <tanggeliang@gmail.com>
- create
