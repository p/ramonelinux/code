Name:       lxpolkit
Version:    0.1.0
Release:    1%{?dist}
Summary:    LXPolkit

Group:      User Interface/LXDE
License:    GPLv2+
Url:        http://lxde.org
Source:     http://sourceforge.net/lxde/files/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  gtk2 pkg-config menu-cache polkit
BuildRequires:  intltool gettext xml-parser

%description
The LXPolkit package contains a simple PolicyKit authentication agent.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --sysconfdir=/etc \
            --libexecdir=%{_libdir}/lxpolkit &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
/etc/xdg/autostart/lxpolkit.desktop
%{_libdir}/lxpolkit/lxpolkit
%{_datadir}/locale/*/LC_MESSAGES/lxpolkit.mo
%{_datadir}/lxpolkit/ui/lxpolkit.ui

%clean
rm -rf %{buildroot}

%changelog
* Sat Nov 9 2013 tanggeliang <tanggeliang@gmail.com>
- create
