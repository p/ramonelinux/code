Name:       pcmanfm
Version:    1.1.2
Release:    2%{?dist}
Summary:    Pcman file manager

Group:      User Interface/LXDE
License:    GPLv2+
Url:        http://lxde.org
Source:     http://sourceforge.net/lxde/files/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  gtk2 shared-mime-info
BuildRequires:  lxde-icon-theme startup-notification pkg-config
BuildRequires:  libfm gamin
BuildRequires:  intltool gettext xml-parser

%description
The PCManFM package contains an extremly fast, lightweight, yet feature-rich file manager with tabbed browsing.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --sysconfdir=/etc &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
/etc/xdg/pcmanfm/default/pcmanfm.conf
%{_bindir}/pcmanfm
%{_datadir}/applications/pcmanfm-desktop-pref.desktop
%{_datadir}/applications/pcmanfm.desktop
%{_datadir}/locale/*/LC_MESSAGES/pcmanfm.mo
%{_datadir}/pcmanfm/ui/*.ui
%{_mandir}/man1/pcmanfm.1.gz

%clean
rm -rf %{buildroot}

%changelog
* Fri Nov 8 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.1.0 to 1.1.2
* Wed Jun 19 2013 tanggeliang <tanggeliang@gmail.com>
- create
