Name:       lxsession
Version:    0.4.9.2
Release:    3%{?dist}
Summary:    LXSession (session manager)

Group:      User Interface/LXDE
License:    GPLv2+
Url:        http://lxde.org
Source:     http://sourceforge.net/lxde/files/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  dbus-glib gtk2 libgee0 vala pkg-config polkit
BuildRequires:  intltool gettext xml-parser
BuildRequires:  libxslt docbook-xml docbook-xsl

%description
The LXSession package contains the default session manager for LXDE.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --disable-man \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/lxclipboard
%{_bindir}/lxlock
%{_bindir}/lxpolkit
%{_bindir}/lxsession
%{_bindir}/lxsession-default
%{_bindir}/lxsession-default-apps
%{_bindir}/lxsession-default-terminal
%{_bindir}/lxsession-edit
%{_bindir}/lxsession-logout
%{_datadir}/applications/lxsession-default-apps.desktop
%{_datadir}/applications/lxsession-edit.desktop
%{_datadir}/locale/*/LC_MESSAGES/lxsession.mo
%{_datadir}/lxsession/images/gnome-session-hibernate.png
%{_datadir}/lxsession/images/gnome-session-reboot.png
%{_datadir}/lxsession/images/gnome-session-suspend.png
%{_datadir}/lxsession/images/gnome-session-switch.png
%{_datadir}/lxsession/images/system-log-out.png
%{_datadir}/lxsession/images/system-shutdown.png
%{_datadir}/lxsession/ui/lxpolkit.ui
%{_datadir}/lxsession/ui/lxsession-default-apps.ui
%{_datadir}/lxsession/ui/lxsession-edit.ui
%{_mandir}/man1/lxsession*.1.gz

%clean
rm -rf %{buildroot}

%changelog
* Wed Jun 19 2013 tanggeliang <tanggeliang@gmail.com>
- create
