Name:       lxde-icon-theme
Version:    0.5.0
Release:    2%{?dist}
Summary:    LXDE Icon Theme

Group:      User Interface/LXDE
License:    GPLv2+
Url:        http://lxde.org
Source:     http://sourceforge.net/lxde/files/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
Requires:       gtk2-update-icon-cache

%description
The LXDE Icon Theme package contains nuoveXT 2.2 Icon Theme for LXDE.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_datadir}/icons/nuoveXT2/*x*/actions/*.png
%{_datadir}/icons/nuoveXT2/*x*/apps/*.png
%{_datadir}/icons/nuoveXT2/*x*/categories/*.png
%{_datadir}/icons/nuoveXT2/*x*/devices/*.png
%{_datadir}/icons/nuoveXT2/*x*/filesystems/*.png
%{_datadir}/icons/nuoveXT2/*x*/mimetypes/*.png
%{_datadir}/icons/nuoveXT2/*x*/places/*.png
%{_datadir}/icons/nuoveXT2/*x*/status/*.png
%{_datadir}/icons/nuoveXT2/*x*/emblems/placeholder
%{_datadir}/icons/nuoveXT2/*x*/emotes/placeholder
%{_datadir}/icons/nuoveXT2/*x*/filesystems/placeholder
%{_datadir}/icons/nuoveXT2/extra/*-HD.png
%{_datadir}/icons/nuoveXT2/index.theme
%{_datadir}/icons/nuoveXT2/72x72/filesystems/.gitignore
%{_datadir}/icons/nuoveXT2/96x96/filesystems/.gitignore

%post
gtk-update-icon-cache -qf /usr/share/icons/nuoveXT2

%clean
rm -rf %{buildroot}

%changelog
* Wed Jun 19 2013 tanggeliang <tanggeliang@gmail.com>
- create
