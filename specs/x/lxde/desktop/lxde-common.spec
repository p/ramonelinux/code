Name:       lxde-common
Version:    0.5.5
Release:    2%{?dist}
Summary:    Lxde-common (default config)

Group:      User Interface/LXDE
License:    GPLv2+
Url:        http://lxde.org
Source:     http://sourceforge.net/lxde/files/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  consolekit lxde-icon-theme lxpanel lxsession openbox pcmanfm
BuildRequires:  desktop-file-utils hicolor-icon-theme shared-mime-info
Requires:       openbox lxpolkit
Requires:       shared-mime-info gtk2-update-icon-cache desktop-file-utils

%description
The LXDE Common package provides a set of default configuration for LXDE.

%prep
%setup -q -n %{name}-%{version}

%build
sed -e "s:@prefix@/share/lxde/pcmanfm:@sysconfdir@/xdg/pcmanfm/LXDE:" \
    -i startlxde.in &&
./configure --prefix=/usr --sysconfdir=/etc \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

install -Dm644 lxde-logout.desktop %{buildroot}/usr/share/applications/lxde-logout.desktop

%files
%defattr(-,root,root,-)
/etc/xdg/lxsession/LXDE/autostart
/etc/xdg/lxsession/LXDE/desktop.conf
/etc/xdg/pcmanfm/LXDE/pcmanfm.conf
%{_bindir}/lxde-logout
%{_bindir}/openbox-lxde
%{_bindir}/startlxde
%{_datadir}/applications/lxde-logout.desktop
%{_datadir}/lxde/images/logout-banner.png
%{_datadir}/lxde/images/lxde-icon.png
%{_datadir}/lxde/openbox/menu.xml
%{_datadir}/lxde/openbox/rc.xml
%{_datadir}/lxde/wallpapers/lxde_blue.jpg
%{_datadir}/lxde/wallpapers/lxde_green.jpg
%{_datadir}/lxde/wallpapers/lxde_red.jpg
%{_datadir}/lxpanel/profile/LXDE/config
%{_datadir}/lxpanel/profile/LXDE/panels/panel
%{_datadir}/xsessions/LXDE.desktop
%{_mandir}/man1/*.1.gz

%post
update-mime-database /usr/share/mime &&
gtk-update-icon-cache -qf /usr/share/icons/hicolor &&
update-desktop-database -q

%clean
rm -rf %{buildroot}

%changelog
* Wed Jun 19 2013 tanggeliang <tanggeliang@gmail.com>
- create
