Name:       lxterminal
Version:    0.1.11
Release:    2%{?dist}
Summary:    LXTerminal (terminal emulator)

Group:      User Interface/LXDE
License:    GPLv2+
Url:        http://lxde.org
Source:     http://sourceforge.net/lxde/files/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  gtk2 vte2 pkg-config
BuildRequires:  intltool gettext xml-parser
BuildRequires:  libxslt docbook-xml docbook-xsl

%description
The LXTerminal package contains a VTE-based terminal emulator for LXDE with support for multiple tabs.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/lxterminal
%{_datadir}/applications/lxterminal.desktop
%{_datadir}/locale/*/LC_MESSAGES/lxterminal.mo
%{_datadir}/lxterminal/lxterminal-preferences.ui
%{_datadir}/lxterminal/lxterminal.conf
%{_datadir}/pixmaps/lxterminal.png
%{_mandir}/man1/lxterminal.1.gz

%clean
rm -rf %{buildroot}

%changelog
* Wed Jun 19 2013 tanggeliang <tanggeliang@gmail.com>
- create
