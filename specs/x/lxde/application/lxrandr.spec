Name:       lxrandr
Version:    0.1.2
Release:    2%{?dist}
Summary:    LXRandR (monitor config tool)

Group:      User Interface/LXDE
License:    GPLv2+
Url:        http://lxde.org
Source:     http://sourceforge.net/lxde/files/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  gtk2 libx11 pkg-config
BuildRequires:  intltool gettext xml-parser
BuildRequires:  libxslt docbook-xml docbook-xsl

%description
The LXRandR package contains a monitor configuration tool for LXDE.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/lxrandr
%{_datadir}/applications/lxrandr.desktop
%{_datadir}/locale/*/LC_MESSAGES/lxrandr.mo
%{_mandir}/man1/lxrandr.1.gz

%clean
rm -rf %{buildroot}

%changelog
* Wed Jun 19 2013 tanggeliang <tanggeliang@gmail.com>
- create
