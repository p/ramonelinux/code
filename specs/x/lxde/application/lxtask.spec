Name:       lxtask
Version:    0.1.4
Release:    2%{?dist}
Summary:    LXTask (task manager)

Group:      User Interface/LXDE
License:    GPLv2+
Url:        http://lxde.org
Source:     http://sourceforge.net/lxde/files/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  gtk2 pkg-config
BuildRequires:  intltool gettext xml-parser

%description
The LXTask package contains a lightweight and desktop-independent task manager.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/lxtask
%{_datadir}/applications/lxtask.desktop
%{_datadir}/locale/*/LC_MESSAGES/lxtask.mo

%clean
rm -rf %{buildroot}

%changelog
* Wed Jun 19 2013 tanggeliang <tanggeliang@gmail.com>
- create
