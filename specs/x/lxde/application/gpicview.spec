Name:       gpicview
Version:    0.2.4
Release:    2%{?dist}
Summary:    GPicView (image Viewer)

Group:      User Interface/LXDE
License:    GPLv2+
Url:        http://lxde.org
Source:     http://sourceforge.net/lxde/files/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  gtk2 pkg-config
BuildRequires:  intltool gettext xml-parser

%description
The GPicView package contains a lightweight image viewer.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/gpicview
%{_datadir}/applications/gpicview.desktop
%{_datadir}/gpicview/pixmaps/object-*.png
%{_datadir}/gpicview/ui/pref-dlg.ui
%{_datadir}/locale/*/LC_MESSAGES/gpicview.mo
%{_datadir}/icons/hicolor/48x48/apps/gpicview.png

%clean
rm -rf %{buildroot}

%changelog
* Fri Nov 8 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.2.3 to 0.2.4
* Wed Jun 19 2013 tanggeliang <tanggeliang@gmail.com>
- create
