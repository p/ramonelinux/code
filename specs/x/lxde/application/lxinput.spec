Name:       lxinput
Version:    0.3.2
Release:    2%{?dist}
Summary:    LXInput (Kbd and amp_ mouse config)

Group:      User Interface/LXDE
License:    GPLv2+
Url:        http://lxde.org
Source:     http://sourceforge.net/lxde/files/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  gtk2 pkg-config
BuildRequires:  intltool gettext xml-parser

%description
The LXInput package contains a small program used to configure keyboard and mouse for LXDE.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/lxinput
%{_datadir}/applications/lxinput.desktop
%{_datadir}/locale/*/LC_MESSAGES/lxinput.mo
%{_datadir}/lxinput/input-keyboard.png
%{_datadir}/lxinput/input-mouse.png
%{_datadir}/lxinput/lxinput.ui
%{_mandir}/man1/lxinput.1.gz

%clean
rm -rf %{buildroot}

%changelog
* Fri Nov 8 2013 tanggeliang <tanggeliang@gmail.com>
- create
