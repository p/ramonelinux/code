Name:       lxappearance-obconf
Version:    0.2.1
Release:    2%{?dist}
Summary:    LXAppearance Obconf

Group:      User Interface/LXDE
License:    GPLv2+
Url:        http://lxde.org
Source:     http://sourceforge.net/lxde/files/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  lxappearance openbox
BuildRequires:  gtk2 pkg-config
BuildRequires:  intltool gettext xml-parser

%description
The LXAppearance OBconf package contains a plugin for LXAppearance to configure OpenBox.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_libdir}/lxappearance/plugins/obconf.a
%{_libdir}/lxappearance/plugins/obconf.la
%{_libdir}/lxappearance/plugins/obconf.so
%{_datadir}/locale/*/LC_MESSAGES/lxappearance-obconf.mo
%{_datadir}/lxappearance/obconf/obconf.glade

%clean
rm -rf %{buildroot}

%changelog
* Fri Nov 8 2013 tanggeliang <tanggeliang@gmail.com>
- create
