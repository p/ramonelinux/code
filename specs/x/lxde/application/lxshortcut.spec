Name:       lxshortcut
Version:    0.1.2
Release:    1%{?dist}
Summary:    LXShortcut

Group:      User Interface/LXDE
License:    GPLv2+
Url:        http://lxde.org
Source:     http://sourceforge.net/lxde/files/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  gtk2 pkg-config
BuildRequires:  intltool gettext xml-parser

%description
The LXShortcut package contains a small program used to edit application shortcuts created with freedesktop.org Desktop Entry spec.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/lxshortcut
%{_datadir}/locale/*/LC_MESSAGES/lxshortcut.mo
%{_datadir}/lxshortcut/choose-icon.ui
%{_datadir}/lxshortcut/lxshortcut.ui

%clean
rm -rf %{buildroot}

%changelog
* Sat Nov 9 2013 tanggeliang <tanggeliang@gmail.com>
- create
