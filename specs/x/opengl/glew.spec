Name:       glew
Version:    1.10.0
Release:    1%{?dist}
Summary:    The OpenGL Extension Wrangler Library

Group:      System Environment/xlib
License:    GPLv2+
Url:        http://glew.sourceforge.net
Source:     http://downloads.sourceforge.net/glew/%{name}-%{version}.tgz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  mesa libxmu libxi libxext libx11

%description
The OpenGL Extension Wrangler Library is a simple tool that helps C/C++ developers initialize extensions and write portable applications.
GLEW currently supports a variety of operating systems, including Windows, Linux, Darwin, Irix, and Solaris.

%prep
%setup -q -n %{name}-%{version}

%build
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make install GLEW_DEST=%{buildroot}/usr LIBDIR=%{buildroot}/%{_libdir}

%files
%defattr(-,root,root,-)
%{_includedir}/GL/glew.h
%{_includedir}/GL/glxew.h
%{_includedir}/GL/wglew.h
%{_libdir}/libGLEW.a
%{_libdir}/libGLEW.so*
%{_libdir}/pkgconfig/glew.pc

%clean
rm -rf %{buildroot}

%changelog
* Fri Jul 26 2013 tanggeliang <tanggeliang@gmail.com>
- create
