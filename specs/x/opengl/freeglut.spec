Name:       freeglut
Version:    3.0.0
Release:    1%{?dist}
Summary:    freeglut

Group:      System Environment/xlib
License:    GPLv2+
Url:        http://freeglut.sourceforge.net
Source:     http://downloads.sourceforge.net/freeglut/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  cmake mesa glu
BuildRequires:  libxi libxrandr libice

%description
freeglut is intended to be a 100% compatible, completely opensourced clone of the GLUT library.
GLUT is a window system independent toolkit for writing OpenGL programs, implementing a simple windowing API, which makes learning about and exploring OpenGL programming very easy.

%prep
%setup -q -n %{name}-%{version}

%build
mkdir build &&
cd    build &&

cmake -DCMAKE_INSTALL_PREFIX=/usr      \
      -DCMAKE_BUILD_TYPE=Release       \
      -DFREEGLUT_BUILD_DEMOS=OFF       \
      -DFREEGLUT_BUILD_STATIC_LIBS=OFF \
%ifarch x86_64
      -DLIB_SUFFIX=64 		       \
%endif
      ..

make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
cd    build &&
make DESTDIR=%{buildroot} install &&

%files
%defattr(-,root,root,-)
%{_includedir}/GL/freeglut*.h
%{_includedir}/GL/glut.h
%{_libdir}/libglut.so*
%{_libdir}/pkgconfig/freeglut.pc

%clean
rm -rf %{buildroot}

%changelog
* Sat Sep 26 2015 tanggeliang <tanggeliang@gmail.com>
- update from 2.8.1 to 3.0.0
* Sat Jul 6 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.8.0 to 2.8.1
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
