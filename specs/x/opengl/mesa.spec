Name:       mesa
Version:    10.6.6
Release:    1%{?dist}
Summary:    Mesa

Group:      User Interface/Xorg
License:    GPLv2+
Url:        http://xorg.freedesktop.org
Source:     ftp://ftp.freedesktop.org/pub/%{name}/%{version}/%{name}-%{version}.tar.xz
Patch0:      %{name}-%{version}-llvm_3_7-1.patch
Patch1:      %{name}-%{version}-add_xdemos-1.patch
Patch2:      %{name}-%{version}-add_xdemos-lib64-1.patch

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  expat libdrm libxml llvm makedepend
BuildRequires:  libsm libice libx11 libxext libxmu libxt
BuildRequires:  libxxf86vm libxi libxdamage libxfixes libxshmfence
BuildRequires:  bison flex libva
BuildRequires:  dri2proto dri3proto glproto presentproto elfutils
BuildRequires:  autoconf automake libtool systemd systemd-libudev gettext

%description
Mesa is an OpenGL compatible 3D graphics library.

%package        wayland
Summary:        Mesa for Wayland
BuildRequires:  wayland
%description    wayland

%package        demos
Summary:        Demo programs
%description    demos

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}
%ifarch %{ix86}
%patch0 -p1
%else %ifarch x86_64
%patch1 -p1
%endif

%build
GLL_DRV="nouveau,r300,r600,radeonsi,svga,swrast" &&

./configure CFLAGS="-O2" CXXFLAGS="-O2"     \
            --prefix=/usr                   \
            --sysconfdir=/etc               \
            --enable-texture-float          \
            --enable-gles1                  \
            --enable-gles2                  \
	    --enable-osmesa		    \
            --enable-xa                     \
	    --enable-gbm 		    \
            --enable-glx-tls                \
            --with-egl-platforms="drm,x11,wayland" \
            --with-gallium-drivers=$GLL_DRV \
            --libdir=%{_libdir} 	    &&

unset GLL_DRV &&

make %{?_smp_mflags}
make -C xdemos DEMOS_PREFIX=/usr %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}
make -C xdemos DEMOS_PREFIX=/usr install DESTDIR=%{buildroot}

install -v -dm755 %{buildroot}/usr/share/doc/%{name}-%{version} && 
cp -rfv docs/* %{buildroot}/usr/share/doc/%{name}-%{version}

%files
%defattr(-,root,root,-)
%{_sysconfdir}/drirc
%{_includedir}/EGL/*
%{_includedir}/GL/*
%{_includedir}/GLES/*.h
%{_includedir}/GLES2/gl2*.h
%{_includedir}/GLES3/gl3*.h
%{_includedir}/KHR/*
%{_includedir}/gbm.h
%{_includedir}/xa_*.h
%{_libdir}/dri/*_dri.so
%{_libdir}/dri/gallium_drv_video.*
%{_libdir}/libEGL.*
%{_libdir}/libGL.*
%{_libdir}/libGLESv1_CM.*
%{_libdir}/libGLESv2.*
%{_libdir}/libgbm.*
%{_libdir}/libglapi.*
%{_libdir}/libOSMesa.*
%{_libdir}/libxatracker.*
%{_libdir}/pkgconfig/dri.pc
%{_libdir}/pkgconfig/egl.pc
%{_libdir}/pkgconfig/gl.pc
%{_libdir}/pkgconfig/glesv1_cm.pc
%{_libdir}/pkgconfig/glesv2.pc
%{_libdir}/pkgconfig/gbm.pc
%{_libdir}/pkgconfig/osmesa.pc
%{_libdir}/pkgconfig/xatracker.pc

%files wayland
%defattr(-,root,root,-)
%{_libdir}/libwayland-egl.la
%{_libdir}/libwayland-egl.so*
%{_libdir}/pkgconfig/wayland-egl.pc

%files demos
%defattr(-,root,root,-)
%{_bindir}/glxgears
%{_bindir}/glxinfo

%files doc
%defattr(-,root,root,-)
%{_docdir}/%{name}-%{version}/*
%{_mandir}/man1/glxgears.1.gz
%{_mandir}/man1/glxinfo.1.gz

%clean
rm -rf %{buildroot}

%changelog
* Mon Sep 14 2015 tanggeliang <tanggeliang@gmail.com>
- update from 10.6.0 to 10.6.6
* Sat Jun 27 2015 tanggeliang <tanggeliang@gmail.com>
- update from 10.2.4 to 10.6.0
* Wed Jul 23 2014 tanggeliang <tanggeliang@gmail.com>
- update from 9.2.4 to 10.2.4
* Tue Dec 3 2013 tanggeliang <tanggeliang@gmail.com>
- update from 9.2.2 to 9.2.4
* Sun Oct 20 2013 tanggeliang <tanggeliang@gmail.com>
- update from 9.2.0 to 9.2.2
* Thu Oct 3 2013 tanggeliang <tanggeliang@gmail.com>
- update from 9.1.6 to 9.2.0
* Sun Aug 18 2013 tanggeliang <tanggeliang@gmail.com>
- update from 9.1.5 to 9.1.6
* Mon Jul 29 2013 tanggeliang <tanggeliang@gmail.com>
- update from 9.1.4 to 9.1.5
* Sat Jul 6 2013 tanggeliang <tanggeliang@gmail.com>
- update from 9.1.3 to 9.1.4
* Thu May 23 2013 tanggeliang <tanggeliang@gmail.com>
- update from 9.1.1 to 9.1.3
- add "--with-egl-platforms wayland"
* Sun Mar 31 2013 tanggeliang <tanggeliang@gmail.com>
- update from 9.1 to 9.1.1
* Thu Mar 14 2013 tanggeliang <tanggeliang@gmail.com>
- update form 9.0.2 to 9.1
- add "-march=i686" in CFLAGS and CXXFLAGS to fix "can build in rpmbuild, cann't build in mock"
* Fri Feb 22 2013 tanggeliang <tanggeliang@gmail.com>
- update from 9.0 to 9.0.2
* Wed Oct 10 2012 tanggeliang <tanggeliang@gmail.com>
- update from 8.0.4 to 9.0
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
