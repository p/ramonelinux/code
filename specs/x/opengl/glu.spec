Name:       glu
Version:    9.0.0
Release:    2%{?dist}
Summary:    GLU

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://xorg.freedesktop.org
Source:     ftp://ftp.freedesktop.org/pub/mesa/glu/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  mesa

%description

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --disable-static \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_includedir}/GL/glu.h
%{_includedir}/GL/glu_mangle.h
%{_libdir}/libGLU.la
%{_libdir}/libGLU.so*
%{_libdir}/pkgconfig/glu.pc

%clean
rm -rf %{buildroot}

%changelog
* Wed Oct 10 2012 tanggeliang <tanggeliang@gmail.com>
- create
