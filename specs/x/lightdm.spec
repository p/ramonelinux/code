Name:       lightdm
Version:    1.9.5
Release:    1%{?dist}
Summary:    The Light Display Manager (LightDM)

Group:      User Interface/LXDE
License:    GPLv2+
Url:        http://www.freedesktop.org
Source:     https://launchpad.net/lightdm/1.9/%{version}/+download/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  glib libx11 libxdmcp libxcb vala libgcrypt
BuildRequires:  itstool libxml gobject-introspection libxklavier
BuildRequires:  intltool gettext xml-parser gtk-doc
BuildRequires:  libxslt docbook-xml docbook-xsl
BuildRequires:  systemd

%description
LightDM is a cross-desktop display manager that aims is to be the standard display manager for the X.org X server.
The motivation for this project is there have been many new display managers written since XDM (often based on the XDM source).
The main difference between these projects is in the GUIs (e.g. different toolkits) and performance - this could be better accomplished with a common display manager that allows these differences.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --sysconfdir=/etc \
            --disable-static \
            --enable-gtk-doc \
            --enable-introspection \
            --enable-liblightdm-gobject \
            --enable-tests \
            --with-greeter-user=lightdm \
            --with-greeter-session=lightdm-greeter \
            --libexecdir=%{_libdir} \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
/etc/apparmor.d/abstractions/lightdm
/etc/apparmor.d/abstractions/lightdm_chromium-browser
/etc/apparmor.d/lightdm-guest-session
/etc/dbus-1/system.d/org.freedesktop.DisplayManager.conf
/etc/init/lightdm.conf
/etc/lightdm/keys.conf
/etc/lightdm/lightdm.conf
/etc/lightdm/users.conf
/etc/pam.d/lightdm
/etc/pam.d/lightdm-autologin
/etc/pam.d/lightdm-greeter
%{_bindir}/dm-tool
%{_includedir}/lightdm-gobject-1/lightdm.h
%{_includedir}/lightdm-gobject-1/lightdm/*.h
%{_libdir}/girepository-1.0/LightDM-1.typelib
%{_libdir}/liblightdm-gobject-1.la
%{_libdir}/liblightdm-gobject-1.so*
%{_libdir}/lightdm-guest-session
%{_libdir}/pkgconfig/liblightdm-gobject-1.pc
%{_sbindir}/lightdm
%{_datadir}/gir-1.0/LightDM-1.gir
%{_datadir}/gtk-doc/html/lightdm-gobject-1/*
%{_datadir}/help/C/lightdm/*
%{_datadir}/locale/*/LC_MESSAGES/lightdm.mo
%{_mandir}/man1/lightdm*.1.gz
%{_mandir}/man1/dm-tool.1.gz
%{_datadir}/vala/vapi/liblightdm-gobject-1.vapi

%clean
rm -rf %{buildroot}

%changelog
* Tue Jan 7 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.9.3 to 1.9.5
* Sat Nov 9 2013 tanggeliang <tanggeliang@gmail.com>
- create
