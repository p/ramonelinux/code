%define subversion 2

Name:       libreoffice
Version:    4.1.4
Release:    1%{?dist}
Summary:    LibreOffice

Group:      System Environment/xlib
License:    GPLv2+
Url:        http://www.documentfoundation.org
Source0:    http://download.documentfoundation.org/%{name}/src/%{version}/%{name}-%{version}.%{subversion}.tar.xz
Source1:    http://download.documentfoundation.org/%{name}/src/%{version}/%{name}-dictionaries-%{version}.%{subversion}.tar.xz
Source2:    http://download.documentfoundation.org/%{name}/src/%{version}/%{name}-help-%{version}.%{subversion}.tar.xz
Source3:    http://download.documentfoundation.org/%{name}/src/%{version}/%{name}-translations-%{version}.%{subversion}.tar.xz
Source4:    %{name}-%{version}.%{subversion}-download-src.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  gperf gtk2 archive-zip xml-parser unzip which zip
BuildRequires:  boost clucene cups curl dbus expat gstreamer gst-plugins-base icu
BuildRequires:  lcms2 librsvg libxml libxslt mesalib glu neon npapi-sdk nss openldap
BuildRequires:  openssl poppler python3 redland unixodbc libjpeg-turbo libgsf libpng
BuildRequires:  bluez gtk+ autoconf automake m4
BuildRequires:  libx11 libxt libice libxcomposite libxrender libxrandr libxinerama
BuildRequires:  bison flex gconf gvfs dbus-glib gnutls gstreamer0 gst-plugins-base0
BuildRequires:  font-alias

%description
LibreOffice is a full-featured office suite.
It is largely compatible with Microsoft Office and is descended from OpenOffice.org.

%prep
%setup -q -n %{name}-%{version}.%{subversion}

%build
install -dm755 src &&
tar -xf %{SOURCE1} --no-overwrite-dir --strip-components=1 &&
ln -sv %{SOURCE1} src/ &&
ln -sv %{SOURCE2} src/ &&
ln -sv %{SOURCE3} src/ &&
tar -xf %{SOURCE4}

sed -e "/gzip -f/d"   \
    -e "s|.1.gz|.1|g" \
    -i bin/distro-install-desktop-integration &&
sed -e "/distro-install-file-lists/d" -i Makefile.in &&

chmod -v +x bin/unpack-sources &&
sed -e "s/target\.mk/langlist\.mk/"                \
    -e "s/tar -xf/tar -x --strip-components=1 -f/" \
    -e "/tar -x/s/lo_src_dir/start_dir/"           \
    -i bin/unpack-sources &&

./autogen.sh --prefix=/usr              \
             --sysconfdir=/etc          \
             --libdir=%{_libdir}        \
             --with-vendor="BLFS"       \
             --with-lang="en-US pt-BR"  \
             --with-help                \
             --with-alloc=system        \
             --without-java             \
             --disable-gconf            \
             --disable-odk              \
             --disable-postgresql-sdbc  \
             --enable-python=system     \
             --with-system-boost        \
             --with-system-clucene      \
             --with-system-cairo        \
             --with-system-curl         \
             --with-system-expat        \
             --with-system-harfbuzz     \
             --with-system-icu          \
             --with-system-jpeg         \
             --with-system-lcms2        \
             --with-system-libpng       \
             --with-system-libxml       \
             --with-system-mesa-headers \
             --with-system-neon         \
             --with-system-npapi-headers \
             --with-system-nss          \
             --with-system-odbc         \
             --with-system-openldap     \
             --with-system-openssl      \
             --with-system-poppler      \
             --with-system-redland      \
             --with-system-zlib         \
             --with-parallelism=$(getconf _NPROCESSORS_ONLN)
make build %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make distro-pack-install DESTDIR=%{buildroot}

#chown -cR 0:0 dictionaries/
mkdir -pv %{buildroot}/usr/lib/libreoffice/share/extensions/dict-en                      &&
cp -vR dictionaries/en/*    %{buildroot}/usr/lib/libreoffice/share/extensions/dict-en    &&
mkdir -pv %{buildroot}/usr/lib/libreoffice/share/extensions/dict-pt-BR                   &&
cp -vR dictionaries/pt_BR/* %{buildroot}/usr/lib/libreoffice/share/extensions/dict-pt-BR

%post
gtk-update-icon-cache /usr/share/icons/hicolor

%files
%defattr(-,root,root,-)
/etc/bash_completion.d/libreoffice.sh
%{_bindir}/libreoffice
%{_bindir}/lobase
%{_bindir}/localc
%{_bindir}/lodraw
%{_bindir}/loffice
%{_bindir}/lofromtemplate
%{_bindir}/loimpress
%{_bindir}/lomath
%{_bindir}/loweb
%{_bindir}/lowriter
%{_bindir}/soffice
%{_bindir}/unopkg
%{_libdir}/libreoffice/CREDITS.odt
%{_libdir}/libreoffice/help/*
%{_libdir}/libreoffice/LICENSE
%{_libdir}/libreoffice/LICENSE.html
%{_libdir}/libreoffice/LICENSE.odt
%{_libdir}/libreoffice/NOTICE
%{_libdir}/libreoffice/presets/*
%{_libdir}/libreoffice/program/*
%{_libdir}/libreoffice/readmes/*
%{_libdir}/libreoffice/share/*
%{_libdir}/libreoffice/ure/*
%{_libdir}/libreoffice/ure-link
%{_datadir}/applications/libreoffice-*.desktop
%{_datadir}/application-registry/libreoffice.applications
%{_datadir}/icons/gnome/*/mimetypes/*
%{_datadir}/icons/gnome/*/apps/*
%{_datadir}/icons/locolor/*/mimetypes/*
%{_datadir}/icons/locolor/*/apps/*
%{_datadir}/icons/hicolor/*/mimetypes/*
%{_datadir}/icons/hicolor/*/apps/*
%{_datadir}/mime-info/libreoffice.mime
%{_datadir}/mime-info/libreoffice.keys
%{_datadir}/mimelnk/application/libreoffice-*.desktop
%{_datadir}/mime/packages/libreoffice.xml
%{_mandir}/man1/*.1.gz
/gid_Module_*
/usr/lib/libreoffice/share/extensions/dict-en/*
/usr/lib/libreoffice/share/extensions/dict-pt-BR/*

%clean
rm -rf %{buildroot}

%changelog
* Wed Dec 18 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.1.3 to 4.1.4
* Tue Dec 17 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.0 to 4.1.3
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
