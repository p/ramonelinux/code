Name:       transmission
Version:    2.82
Release:    1%{?dist}
Summary:    transmission

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://transmissionbt.com
Source:     http://download.transmissionbt.com/files/%{name}-%{version}.tar.xz
Patch:      transmission-%{version}-qt4-1.patch

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires: 	openssl curl libevent intltool
BuildRequires: 	gtk+ qt4
BuildRequires: 	gettext gdb

%description
Transmission is a cross-platform, open source BitTorrent client.
This is useful for downloading large files (such as Linux ISOs) and reduces the need for the distributors to provide server bandwidth.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}
%patch -p1

%build
./configure --prefix=/usr &&
make %{?_smp_mflags}

pushd qt                &&
  qmake qtr.pro         &&
  make %{?_smp_mflags}  &&
popd

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/transmission-cli
%{_bindir}/transmission-create
%{_bindir}/transmission-daemon
%{_bindir}/transmission-edit
%{_bindir}/transmission-gtk
%{_bindir}/transmission-remote
%{_bindir}/transmission-show
%{_datadir}/applications/transmission-gtk.desktop
%{_datadir}/icons/hicolor/*/apps/transmission.png
%{_datadir}/icons/hicolor/scalable/apps/transmission.svg
%{_datadir}/locale/*/LC_MESSAGES/transmission-gtk.mo
%{_datadir}/pixmaps/transmission.png
%{_datadir}/transmission/web/LICENSE
%{_datadir}/transmission/web/images/favicon.ico
%{_datadir}/transmission/web/images/favicon.png
%{_datadir}/transmission/web/images/webclip-icon.png
%{_datadir}/transmission/web/index.html
%{_datadir}/transmission/web/javascript/*.js
%{_datadir}/transmission/web/javascript/jquery/*.js
%{_datadir}/transmission/web/style/jqueryui/images/ui-*.png
%{_datadir}/transmission/web/style/jqueryui/jqueryui-1.8.16.css
%{_datadir}/transmission/web/style/transmission/common.css
%{_datadir}/transmission/web/style/transmission/images/*.png
%{_datadir}/transmission/web/style/transmission/images/buttons/*.png
%{_datadir}/transmission/web/style/transmission/mobile.css

%files doc
%defattr(-,root,root,-)
%{_mandir}/man1/transmission-*.1.gz

%post
gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :

%clean
rm -rf %{buildroot}

%changelog
* Mon Oct 7 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.73 to 2.82
* Fri Nov 2 2012 tanggeliang <tanggeliang@gmail.com>
- update from 2.71 to 2.73
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
