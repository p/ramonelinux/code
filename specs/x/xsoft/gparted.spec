Name:       gparted
Version:    0.18.0
Release:    1%{?dist}
Summary:    Gparted

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://gparted.sourceforge.net
Source:     http://downloads.sourceforge.net/gparted/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires: 	gtkmm2 intltool parted
BuildRequires: 	rarian
BuildRequires: 	gettext

%description
Gparted is the Gnome Partition Editor, a Gtk 2 GUI for other command line tools that can create, reorganise or delete disk partitions.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr    \
            --disable-doc    \
            --disable-static \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_sbindir}/gparted
%{_sbindir}/gpartedbin
%{_datadir}/appdata/gparted.appdata.xml
%{_datadir}/applications/gparted.desktop
%ifarch %{ix86}
%{_datadir}/icons/hicolor/*x*/apps/gparted.png
%{_datadir}/icons/hicolor/scalable/apps/gparted.svg
%else %ifarch x86_64
%{_datadir}/icons/hicolor/hicolor_apps_*x*_gparted.png
%{_datadir}/icons/hicolor/hicolor_apps_scalable_gparted.svg
%endif
%{_datadir}/locale/*/LC_MESSAGES/gparted.mo
%{_mandir}/man8/gparted.8.gz

%clean
rm -rf %{buildroot}

%changelog
* Thu Mar 20 2014 tanggeliang <tanggeliang@gmail.com>
- update from 0.17.0 to 0.18.0
* Sun Dec 15 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.16.2 to 0.17.0
* Wed Oct 2 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.14.0 to 0.16.2
* Tue Oct 23 2012 tanggeliang <tanggeliang@gmail.com>
- create
