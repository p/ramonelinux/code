Name:       gimp
Version:    2.8.6
Release:    1%{?dist}
Summary:    Gimp

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.gimp.org
Source:     ftp://ftp.gimp.org/pub/gimp/v2.8/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires: 	gegl gtk2 intltool
BuildRequires:	pygtk
BuildRequires:	aalib alsa-lib curl dbus-glib ghostscript gvfs
BuildRequires:  iso-codes jasper lcms libexif libmng librsvg
BuildRequires:	poppler mta systemd-gudev webkitgtk gtk-doc

%description
The Gimp package contains the GNU Image Manipulation Program which is useful for photo retouching, image composition and image authoring.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --sysconfdir=/etc --without-gvfs \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)

%clean
rm -rf %{buildroot}

%changelog
* Fri Nov 1 2013 tanggeliang <tanggeliang@gmail.com>
- create
