Name:       gimp-help
Version:    2.8.0
Release:    1%{?dist}
Summary:    Gimp-help

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.gimp.org
Source0:    ftp://ftp.gimp.org/pub/gimp/v2.8/%{name}-%{version}.tar.bz2
Source1:    %{name}-%{version}-build_fixes-1.patch.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
#BuildRequires: 	

%description
The Gimp package contains the GNU Image Manipulation Program which is useful for photo retouching, image composition and image authoring.

%prep
%setup -q -n %{name}-%{version}

%build
xzcat ${SOURCE1} \
 | patch -p1 &&
./autogen.sh --prefix=/usr &&
             --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

chown -R root:root %{buildroot}/usr/share/gimp/2.0/help

%files
%defattr(-,root,root,-)

%clean
rm -rf %{buildroot}

%changelog
* Fri Nov 1 2013 tanggeliang <tanggeliang@gmail.com>
- create
