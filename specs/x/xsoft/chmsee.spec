Name:       chmsee
Version:    2.0.2
Release:    1%{?dist}
Summary:    chmsee

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://chmsee.googlecode.com
Source:     http://chmsee.googlecode.com/files/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
#BuildRequires: 	

%description
HTML Help viewer for Unix/Linux.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr    \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)

%clean
rm -rf %{buildroot}

%changelog
* Wed Jan 29 2014 tanggeliang <tanggeliang@gmail.com>
- create
