Name:       firefox
Version:    41.0
Release:    1%{?dist}
Summary:    Firefox

Group:      Applications/Internet
License:    GPLv2+
Url:        http://www.mozilla.org
Source:     ftp://ftp.mozilla.org/pub/mozilla.org/firefox/releases/%{version}/source/%{name}-%{version}.source.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  alsa-lib gtk2 zip unzip
BuildRequires:  libevent libvpx nspr nss sqlite yasm
BuildRequires:  dbus-glib startup-notification mesa libjpeg-turbo
BuildRequires:  libice libsm libxaw libxfont libxi libxmu libxpm libxt libxtst libxv libxxf86dga libxxf86vm libdmx libfontenc libxkbfile
BuildRequires:  pulseaudio gstreamer gst-plugins-base libpng

%description
Firefox is a stand-alone browser based on the Mozilla codebase.

%prep
%setup -q -n mozilla-release

%build
cat > mozconfig << "EOF"
# If you have a multicore machine, firefox will now use all the cores by
# default. Exceptionally, you can reduce the number of cores, e.g. to 1,
# by uncommenting the next line and setting a valid number of CPU cores.
mk_add_options MOZ_MAKE_FLAGS="-j4"

# If you have installed DBus-Glib comment out this line:
ac_add_options --disable-dbus

# If you have installed dbus-glib, and you have installed (or will install)
# wireless-tools, and you wish to use geolocation web services, comment out
# this line
ac_add_options --disable-necko-wifi

# If you have installed libnotify comment out this line:
ac_add_options --disable-libnotify

# GStreamer is necessary for H.264 video playback in HTML5 Video Player;
# to be enabled, also remember to set "media.gstreamer.enabled" to "true"
# in about:config. If you have GStreamer 0.x.y, uncomment this line:
ac_add_options --enable-gstreamer
# or uncomment this line, if you have GStreamer 1.x.y
ac_add_options --enable-gstreamer=1.0

# Uncomment these lines if you have installed optional dependencies:
#ac_add_options --enable-system-hunspell
#ac_add_options --enable-startup-notification

# Comment out following option if you have PulseAudio installed
ac_add_options --enable-pulseaudio

# If you have not installed Yasm then uncomment this line:
#ac_add_options --disable-webm

# If you have installed xulrunner uncomment the next two ac_add_options lines
# and check that the sdk will be set by running pkg-config in a subshell
# and has not become hardcoded or empty when you created this file
#ac_add_options --with-system-libxul
#ac_add_options --with-libxul-sdk=$(pkg-config --variable=sdkdir libxul)

# Comment out following options if you have not installed
# recommended dependencies:
ac_add_options --enable-system-sqlite
ac_add_options --with-system-libevent
ac_add_options --with-system-libvpx
ac_add_options --with-system-nspr
ac_add_options --with-system-nss
ac_add_options --with-system-icu

# The BLFS editors recommend not changing anything below this line:
ac_add_options --prefix=/usr
ac_add_options --libdir=%{_libdir}
ac_add_options --enable-application=browser

ac_add_options --disable-crashreporter
ac_add_options --disable-updater
ac_add_options --disable-tests

ac_add_options --enable-optimize
ac_add_options --enable-strip
ac_add_options --enable-install-strip

ac_add_options --enable-gio
ac_add_options --enable-official-branding
ac_add_options --enable-safe-browsing
ac_add_options --enable-url-classifier

# From firefox-40, using system cairo causes firefox to crash
# frequently when it is doing background rendering in a tab.
ac_add_options --disable-system-cairo
ac_add_options --enable-system-ffi
ac_add_options --enable-system-pixman

ac_add_options --with-pthreads

ac_add_options --with-system-bz2
ac_add_options --with-system-jpeg
ac_add_options --with-system-png
ac_add_options --with-system-zlib

mk_add_options MOZ_OBJDIR=@TOPSRCDIR@/firefox-build-dir
EOF

make -f client.mk

%check

%install
rm -rf %{buildroot}
make -f client.mk install INSTALL_SDK= DESTDIR=%{buildroot} &&

rm -rf %{buildroot}/usr/bin/firefox &&
cat > %{buildroot}/usr/bin/firefox << "EOF" &&
#!/bin/sh
export GTK_IM_MODULE_FILE=/etc/gtk-2.0/gtk.immodules
%{_libdir}/firefox-%{version}/firefox
EOF
chmod +x %{buildroot}/usr/bin/firefox

mkdir -pv %{buildroot}/%{_libdir}/mozilla/plugins &&
ln -sfv ../mozilla/plugins %{buildroot}/%{_libdir}/firefox-%{version} &&

mkdir -pv %{buildroot}/usr/share/applications &&
mkdir -pv %{buildroot}/usr/share/pixmaps &&

cat > %{buildroot}/usr/share/applications/firefox.desktop << "EOF" &&
[Desktop Entry]
Encoding=UTF-8
Name=Firefox Web Browser
Comment=Browse the World Wide Web
GenericName=Web Browser
Exec=firefox %u
Terminal=false
Type=Application
Icon=firefox
Categories=GNOME;GTK;Network;WebBrowser;
MimeType=application/xhtml+xml;text/xml;application/xhtml+xml;application/vnd.mozilla.xul+xml;text/mml;x-scheme-handler/http;x-scheme-handler/https;
StartupNotify=true
EOF

ln -sfv %{_libdir}/firefox-%{version}/browser/icons/mozicon128.png \
        %{buildroot}/usr/share/pixmaps/firefox.png

%files
%defattr(-,root,root,-)
%{_bindir}/firefox
%{_libdir}/firefox-%{version}/*
%{_datadir}/applications/firefox.desktop
%{_datadir}/pixmaps/firefox.png

%clean
rm -rf %{buildroot}

%changelog
* Sat Sep 26 2015 tanggeliang <tanggeliang@gmail.com>
- update from 31.0 to 41.0
* Fri Jul 25 2014 tanggeliang <tanggeliang@gmail.com>
- update from 28.0 to 31.0
* Thu Mar 20 2014 tanggeliang <tanggeliang@gmail.com>
- update from 26.0 to 28.0
* Sat Dec 14 2013 tanggeliang <tanggeliang@gmail.com>
- update from 25.0.1 to 26.0
* Thu Dec 5 2013 tanggeliang <tanggeliang@gmail.com>
- update from 24.0 to 25.0.1
* Wed Oct 2 2013 tanggeliang <tanggeliang@gmail.com>
- update from 23.0.1 to 24.0
* Sun Aug 18 2013 tanggeliang <tanggeliang@gmail.com>
- update from 22.0 to 23.0.1
* Sat Jul 6 2013 tanggeliang <tanggeliang@gmail.com>
- update from 21.0 to 22.0
* Thu May 30 2013 tanggeliang <tanggeliang@gmail.com>
- update from 20.0 to 21.0
* Fri Apr 5 2013 tanggeliang <tanggeliang@gmail.com>
- update from 18.0.1 to 20.0
* Sun Feb 24 2013 tanggeliang <tanggeliang@gmail.com>
- update from 16.0.1 to 18.0.1
* Fri Nov 2 2012 tanggeliang <tanggeliang@gmail.com>
- update from 15.0.1 to 16.0.1
* Thu Oct 11 2012 tanggeliang <tanggeliang@gmail.com>
- update from 14.0.1 to 15.0.1
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
