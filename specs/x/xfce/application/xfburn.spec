Name:       xfburn
Version:    0.4.3
Release:    5%{?dist}
Summary:    Xfburn

Group:      User Interface/Xfce
License:    GPLv2+
Url:        http://www.xfce.org
Source:     http://archive.xfce.org/src/apps/xfburn/0.4/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  exo libxfcegui4 libisoburn
BuildRequires:  intltool gettext
BuildRequires:  libburn libisofs

%description
Xfburn is a Gtk 2 GUI frontend for Libisoburn.
This is useful for creating CDs and DVDs from files on your computer or ISO images downloaded from elsewhere.

%prep
%setup -q -n %{name}-%{version}

%build
sed -i '/<glib.h>/a#include <glib-object.h>' xfburn/xfburn-settings.h &&
./configure --prefix=/usr --disable-static &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/xfburn
%{_datadir}/Thunar/sendto/thunar-sendto-xfburn.desktop
%{_datadir}/applications/xfburn.desktop
%{_datadir}/icons/hicolor/*x*/stock/media/stock_xfburn-*.png
%{_datadir}/icons/hicolor/scalable/stock/media/stock_xfburn-*.svg
%{_datadir}/locale/*/LC_MESSAGES/xfburn.mo
%{_datadir}/xfburn/xfburn*.ui
%{_mandir}/man1/xfburn.1.gz

%clean
rm -rf %{buildroot}

%changelog
* Mon Apr 8 2013 tanggeliang <tanggeliang@gmail.com>
- create
