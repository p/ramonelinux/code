Name:       gtksourceview2
Version:    2.10.5
Release:    2%{?dist}
Summary:    GtkSourceView

Group:      User Interface/GNOME
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/%{name}/2.10/gtksourceview-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  gtk2 intltool
BuildRequires:  gtk-doc libxml gettext xml-parser

%description
The GtkSourceView package contains libraries used for extending the GTK+ 2 text functions to include syntax highlighting. 

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n gtksourceview-%{version}

%build
./configure --prefix=/usr \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_includedir}/gtksourceview-2.0/gtksourceview/completion-providers/words/gtksourcecompletionwords.h
%{_includedir}/gtksourceview-2.0/gtksourceview/gtksource*.h
%{_libdir}/libgtksourceview-2.0.la
%{_libdir}/libgtksourceview-2.0.so*
%{_libdir}/pkgconfig/gtksourceview-2.0.pc
%{_datadir}/gtk-doc/html/gtksourceview-2.0/*
%{_datadir}/gtksourceview-2.0/language-specs/*
%{_datadir}/gtksourceview-2.0/styles/*
%{_datadir}/locale/*/LC_MESSAGES/gtksourceview-2.0.mo

%files doc
%defattr(-,root,root,-)
%{_datadir}/gtk-doc/html/gtksourceview-2.0/*.html
%{_datadir}/gtk-doc/html/gtksourceview-2.0/gtksourceview-2.0.devhelp*

%clean
rm -rf %{buildroot}

%changelog
* Mon Apr 8 2013 tanggeliang <tanggeliang@gmail.com>
- create
