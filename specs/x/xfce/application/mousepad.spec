Name:       mousepad
Version:    0.3.0
Release:    1%{?dist}
Summary:    Mousepad

Group:      User Interface/Xfce
License:    GPLv2+
Url:        http://www.xfce.org
Source:     http://archive.xfce.org/src/apps/%{name}/0.3/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  dbus-glib gtksourceview2
BuildRequires:  intltool gettext xml-parser

%description
Mousepad is a simple GTK+ 2 text editor for the Xfce desktop environment. 

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/mousepad
%{_datadir}/applications/mousepad.desktop
%{_datadir}/locale/*/LC_MESSAGES/mousepad.mo

%clean
rm -rf %{buildroot}

%changelog
* Mon Apr 8 2013 tanggeliang <tanggeliang@gmail.com>
- create
