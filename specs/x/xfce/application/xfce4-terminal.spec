Name:       xfce4-terminal
Version:    0.6.2
Release:    1%{?dist}
Summary:    Xfce4 Terminal

Group:      User Interface/Xfce
License:    GPLv2+
Url:        http://www.xfce.org
Source:     http://archive.xfce.org/src/apps/%{name}/0.6/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  libxfce4ui vte2
BuildRequires:  intltool gettext xml-parser

%description
Xfce4 Terminal is a GTK+ 2 terminal emulator.
This is useful for running commands or programs in the comfort of an Xorg window; you can drag and drop files into the Xfce4 Terminal or copy and paste text with your mouse. 

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/xfce4-terminal
%{_datadir}/applications/xfce4-terminal.desktop
%{_datadir}/gnome-control-center/default-apps/xfce4-terminal-default-apps.xml
%{_datadir}/locale/*/LC_MESSAGES/xfce4-terminal.mo
%{_datadir}/xfce4/terminal/colorschemes/*.theme
%{_datadir}/xfce4/terminal/terminal-preferences.ui
%{_mandir}/*/man1/xfce4-terminal.1.gz
%{_mandir}/man1/xfce4-terminal.1.gz

%clean
rm -rf %{buildroot}

%changelog
* Thu Jun 20 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.6.1 to 0.6.2
* Mon Apr 8 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.4.8 to 0.6.1
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
