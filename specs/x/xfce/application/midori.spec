Name:       midori
Version:    0.5.6
Release:    1%{?dist}
Summary:    Midori

Group:      User Interface/Xfce
License:    GPLv2+
Url:        http://www.xfce.org
Source:     http://www.midori-browser.org/downloads/%{name}_%{version}_all_.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  cmake libzeitgeist webkitgtk vala
BuildRequires:  libnotify librsvg libunique
BuildRequires:  intltool gettext libsoup-gnome xml-parser

%description
Midori is a lightweight web browser that uses WebKitGTK.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --enable-gtk3 &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
/etc/xdg/midori/extensions/adblock/config
/etc/xdg/midori/search
%{_bindir}/midori
%{_libdir}/libmidori-core.so*
%{_libdir}/midori/libadblock.so
%{_libdir}/midori/libaddons.so
%{_libdir}/midori/libapps.so
%{_libdir}/midori/libcolorful-tabs.so
%{_libdir}/midori/libcookie-manager.so
%{_libdir}/midori/libcookie-permissions.so
%{_libdir}/midori/libcopy-tabs.so
%{_libdir}/midori/libdelayed-load.so
%{_libdir}/midori/libdevpet.so
%{_libdir}/midori/libexternal-download-manager.so
%{_libdir}/midori/libfeed-panel.so
%{_libdir}/midori/libformhistory.so
%{_libdir}/midori/libhistory-list.so
%{_libdir}/midori/libmouse-gestures.so
%{_libdir}/midori/libnojs.so
%{_libdir}/midori/libnsplugin-manager.so
%{_libdir}/midori/libshortcuts.so
%{_libdir}/midori/libstatus-clock.so
%{_libdir}/midori/libstatusbar-features.so
%{_libdir}/midori/libtab-panel.so
%{_libdir}/midori/libtabby.so
%{_libdir}/midori/libtabs-minimized.so
%{_libdir}/midori/libtransfers.so
%{_libdir}/midori/libtoolbar-editor.so
%{_datadir}/applications/midori-private.desktop
%{_datadir}/applications/midori.desktop
%{_datadir}/icons/hicolor/*x*/apps/midori.png
%{_datadir}/icons/hicolor/*x*/categories/extension.png
%{_datadir}/icons/hicolor/*x*/status/internet-news-reader.png
%{_datadir}/icons/hicolor/scalable/apps/midori.svg
%{_datadir}/icons/hicolor/scalable/categories/extension.svg
%{_datadir}/icons/hicolor/scalable/status/internet-news-reader.svg
%{_datadir}/locale/*/LC_MESSAGES/midori.mo
%{_datadir}/midori/res/*
%{_datadir}/appdata/midori.appdata.xml
%{_docdir}/midori/*

%clean
rm -rf %{buildroot}

%changelog
* Thu Dec 5 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.5.5 to 0.5.6
* Thu Aug 22 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.5.2 to 0.5.5
* Thu Jun 20 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.4.9 to 0.5.2
* Mon Apr 8 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.4.5 to 0.4.9
- add "--enable-gtk3" fix "No package 'webkit-1.0' found"
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
