Name:       xfce4-notifyd
Version:    0.2.4
Release:    1%{?dist}
Summary:    Xfce4 Notification Daemon

Group:      User Interface/Xfce
License:    GPLv2+
Url:        http://www.xfce.org
Source:     http://archive.xfce.org/src/apps/xfce4-notifyd/0.2/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  libxfce4ui
BuildRequires:  intltool gettext xml-parser
BuildRequires:  libnotify

%description
The Xfce4 Notification Daemon is a small program that implements the "server-side" portion of the Freedesktop desktop notifications specification.
Applications that wish to pop up a notification bubble in a standard way can use Xfce4-Notifyd to do so by sending standard messages over D-Bus using the org.freedesktop.Notifications interface.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/xfce4-notifyd-config
%{_libdir}/xfce4/notifyd/xfce4-notifyd
%{_datadir}/applications/xfce4-notifyd-config.desktop
%{_datadir}/dbus-1/services/org.xfce.xfce4-notifyd.Notifications.service
%{_datadir}/icons/hicolor/48x48/apps/xfce4-notifyd.png
%{_datadir}/locale/*/LC_MESSAGES/xfce4-notifyd.mo
%{_datadir}/themes/Default/xfce-notify-4.0/gtkrc
%{_datadir}/themes/Smoke/xfce-notify-4.0/gtkrc
%{_datadir}/themes/ZOMG-PONIES!/xfce-notify-4.0/gtkrc
%{_datadir}/man/man1/xfce4-notifyd-config.1.gz

%clean
rm -rf %{buildroot}

%changelog
* Thu Jun 20 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.2.2 to 0.2.4
* Mon Apr 8 2013 tanggeliang <tanggeliang@gmail.com>
- create
