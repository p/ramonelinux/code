Name:       vte2
Version:    0.28.2
Release:    6%{?dist}
Summary:    VTE

Group:      User Interface/GNOME
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/vte/0.28/vte-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  intltool gtk2
BuildRequires:  gobject-introspection
BuildRequires:  gtk-doc gettext xml-parser

%description
Vte is a library (libvte) implementing a terminal emulator widget for Gtk 2, and a minimal demonstration application (vte) that uses libvte.

%prep
%setup -q -n vte-%{version}

%build
./configure --prefix=/usr \
            --libexecdir=%{_libdir}/vte \
            --disable-static \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/vte
%{_includedir}/vte-0.0/vte/*.h
%{_libdir}/libvte.*
%{_libdir}/pkgconfig/vte.pc
%{_libdir}/vte/gnome-pty-helper
%{_datadir}/gtk-doc/html/vte-0.0/*
%{_datadir}/locale/*/LC_MESSAGES/vte-0.0.mo
%{_datadir}/vte/termcap-0.0/xterm

%clean
rm -rf %{buildroot}

%changelog
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
