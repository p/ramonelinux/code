Name:       parole
Version:    0.5.4
Release:    1%{?dist}
Summary:    Parole

Group:      User Interface/Xfce
License:    GPLv2+
Url:        http://www.xfce.org
Source:     http://archive.xfce.org/src/apps/parole/0.5/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  gst-plugins-base libxfcegui4
BuildRequires:  libnotify taglib
BuildRequires:  dbus-glib libxfce4ui
BuildRequires:  gst-plugins-ugly
BuildRequires:  intltool gettext

%description
Parole is a DVD/CD/music player that uses Gstreamer.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --with-gstreamer=1.0 \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/parole
%{_includedir}/parole/parole*.h
%{_libdir}/parole-0/parole-notify.la
%{_libdir}/parole-0/parole-notify.so
%{_libdir}/parole-0/tray-icon.la
%{_libdir}/parole-0/tray-icon.so
%{_datadir}/applications/parole.desktop
%{_datadir}/icons/hicolor/*x*/apps/parole*.png
%{_datadir}/icons/hicolor/scalable/apps/parole.svg
%{_datadir}/locale/*/LC_MESSAGES/parole.mo
%{_datadir}/parole/gtk-2.0/parole.gtkrc
%{_datadir}/parole/parole-plugins-0/*.desktop
%{_datadir}/parole/pixmaps/parole.png

%clean
rm -rf %{buildroot}

%changelog
* Sun Dec 15 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.5.3 to 0.5.4
* Sat Nov 9 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.5.2 to 0.5.3
* Thu Aug 22 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.5.0 to 0.5.2
* Mon Apr 8 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.2.0.6 to 0.5.0
* Sun Mar 24 2013 tanggeliang <tanggeliang@gmail.com>
- create
