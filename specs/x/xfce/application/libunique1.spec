Name:       libunique1
Version:    1.1.6
Release:    2%{?dist}
Summary:    libunique

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/libunique/1.1/libunique-%{version}.tar.bz2
Patch:      libunique-1.1.6-upstream_fixes-1.patch

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  gtk2 gobject-introspection gtk-doc
BuildRequires:  autoconf automake m4 libtool

%description
The libunique package contains a library for writing single instance applications.

%prep
%setup -q -n libunique-%{version}
%patch -p1

%build
autoreconf -fi &&
./configure --prefix=/usr  \
            --disable-dbus \
            --disable-static \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_includedir}/unique-1.0/unique/unique*.h
%{_libdir}/girepository-1.0/Unique-1.0.typelib
%{_libdir}/libunique-1.0.la
%{_libdir}/libunique-1.0.so*
%{_libdir}/pkgconfig/unique-1.0.pc
%{_datadir}/gir-1.0/Unique-1.0.gir
%{_datadir}/gtk-doc/html/unique/*

%clean
rm -rf %{buildroot}

%changelog
* Mon Apr 8 2013 tanggeliang <tanggeliang@gmail.com>
- create
