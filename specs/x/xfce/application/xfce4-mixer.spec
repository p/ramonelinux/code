Name:       xfce4-mixer
Version:    4.10.0
Release:    3%{?dist}
Summary:    Xfce4 Mixer

Group:      User Interface/Xfce
License:    GPLv2+
Url:        http://www.xfce.org
Source:     http://archive.xfce.org/src/apps/%{name}/4.10/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  gst-plugins-base0 libunique1 xfce4-panel
BuildRequires:  intltool gettext xml-parser

%description
Xfce4 Mixer is a volume control application for the Xfce desktop based on GStreamer. 

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/xfce4-mixer
%{_libdir}/xfce4/panel/plugins/libmixer.la
%{_libdir}/xfce4/panel/plugins/libmixer.so
%{_datadir}/applications/xfce4-mixer.desktop
%{_datadir}/locale/*/LC_MESSAGES/xfce4-mixer.mo
%{_datadir}/pixmaps/xfce4-mixer/chain-broken.png
%{_datadir}/pixmaps/xfce4-mixer/chain.png
%{_datadir}/xfce4-mixer/icons/hicolor/16x16/status/audio-input-microphone-muted.png
%{_datadir}/xfce4-mixer/icons/hicolor/scalable/status/audio-input-microphone-muted.svg
%{_datadir}/xfce4/panel/plugins/mixer.desktop
%{_datadir}/man/man1/xfce4-mixer.1.gz

%clean
rm -rf %{buildroot}

%changelog
* Mon Apr 8 2013 tanggeliang <tanggeliang@gmail.com>
- create
