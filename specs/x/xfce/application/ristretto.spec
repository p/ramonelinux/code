Name:       ristretto
Version:    0.6.3
Release:    1%{?dist}
Summary:    Ristretto

Group:      User Interface/Xfce
License:    GPLv2+
Url:        http://www.xfce.org
Source:     http://archive.xfce.org/src/apps/%{name}/0.6/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  libexif libxfce4ui
BuildRequires:  intltool gettext

%description
Ristretto is a fast and lightweight image viewer for the Xfce desktop. 

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/ristretto
%{_datadir}/applications/ristretto.desktop
%{_datadir}/icons/hicolor/*x*/apps/ristretto.png
%{_datadir}/icons/hicolor/scalable/apps/ristretto.svg
%{_datadir}/locale/*/LC_MESSAGES/ristretto.mo

%clean
rm -rf %{buildroot}

%changelog
* Mon Apr 8 2013 tanggeliang <tanggeliang@gmail.com>
- create
