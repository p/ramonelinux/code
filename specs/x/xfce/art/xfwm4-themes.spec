Name:       xfwm4-themes
Version:    4.10.0
Release:    1%{?dist}
Summary:    xfwm4-themes

Group:      User Interface/Xfce
License:    GPLv2+
Url:        http://www.xfce.org
Source:     http://archive.xfce.org/src/art/%{name}/4.10/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  intltool gettext

%description

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_datadir}/themes/*/xfwm4/*.xpm
%{_datadir}/themes/*/xfwm4/*.png
%{_datadir}/themes/*/xfwm4/README
%{_datadir}/themes/*/xfwm4/themerc

%clean
rm -rf %{buildroot}

%changelog
* Tue Apr 9 2013 tanggeliang <tanggeliang@gmail.com>
- create
