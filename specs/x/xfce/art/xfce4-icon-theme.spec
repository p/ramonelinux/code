Name:       xfce4-icon-theme
Version:    4.4.3
Release:    1%{?dist}
Summary:    xfce4-icon-theme

Group:      User Interface/Xfce
License:    GPLv2+
Url:        http://www.xfce.org
Source:     http://archive.xfce.org/src/art/%{name}/4.4/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  intltool gettext

%description

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_libdir}/pkgconfig/xfce4-icon-theme-1.0.pc
%{_datadir}/icons/Rodent/48x48/apps/*.png
%{_datadir}/icons/Rodent/48x48/devices/*.png
%{_datadir}/icons/Rodent/48x48/emblems/*.png
%{_datadir}/icons/Rodent/48x48/filesystems/*.png
%{_datadir}/icons/Rodent/48x48/mimetypes/*.png
%{_datadir}/icons/Rodent/48x48/misc/*.png
%{_datadir}/icons/Rodent/48x48/stock/*.png
%{_datadir}/icons/Rodent/iconrc
%{_datadir}/icons/Rodent/iconrc-png
%{_datadir}/icons/Rodent/index.theme
%{_datadir}/icons/Rodent/scalable/apps/*.svg
%{_datadir}/icons/Rodent/scalable/devices/*.svg
%{_datadir}/icons/Rodent/scalable/emblems/*.svg
%{_datadir}/icons/Rodent/scalable/filesystems/*.svg
%{_datadir}/icons/Rodent/scalable/mimetypes/*.svg
%{_datadir}/icons/Rodent/scalable/misc/*.svg
%{_datadir}/icons/Rodent/scalable/stock/*.svg
%{_datadir}/xfce4/mime/Rodent.mime.xml

%clean
rm -rf %{buildroot}

%changelog
* Tue Apr 9 2013 tanggeliang <tanggeliang@gmail.com>
- create
