Name:       xfce4-panel
Version:    4.10.1
Release:    2%{?dist}
Summary:    Xfce4-Panel

Group:      User Interface/Xfce
License:    GPLv2+
Url:        http://www.xfce.org
Source:     http://archive.xfce.org/src/xfce/xfce4-panel/4.10/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  exo garcon libwnck2 libxfce4ui
BuildRequires:  intltool gettext xml-parser

%description
The Xfce4-Panel package contains the Xfce4 Panel.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --sysconfdir=/etc \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
/etc/xdg/xfce4/panel/default.xml
%{_bindir}/xfce4-panel
%{_bindir}/xfce4-popup-applicationsmenu
%{_bindir}/xfce4-popup-directorymenu
%{_bindir}/xfce4-popup-windowmenu
%{_includedir}/xfce4/libxfce4panel-1.0/libxfce4panel/*xfce*.h
%{_libdir}/libxfce4panel-1.0.*
%{_libdir}/pkgconfig/libxfce4panel-1.0.pc
%{_libdir}/xfce4/panel/migrate
%{_libdir}/xfce4/panel/plugins/libactions.*
%{_libdir}/xfce4/panel/plugins/libapplicationsmenu.*
%{_libdir}/xfce4/panel/plugins/libclock.*
%{_libdir}/xfce4/panel/plugins/libdirectorymenu.*
%{_libdir}/xfce4/panel/plugins/liblauncher.*
%{_libdir}/xfce4/panel/plugins/libpager.*
%{_libdir}/xfce4/panel/plugins/libseparator.*
%{_libdir}/xfce4/panel/plugins/libshowdesktop.*
%{_libdir}/xfce4/panel/plugins/libsystray.*
%{_libdir}/xfce4/panel/plugins/libtasklist.*
%{_libdir}/xfce4/panel/plugins/libwindowmenu.*
%{_libdir}/xfce4/panel/wrapper
%{_datadir}/applications/panel-*.desktop
%{_datadir}/gtk-doc/html/libxfce4panel-1.0/*
%{_datadir}/icons/hicolor/*/apps/xfce4-panel*.png
%{_datadir}/icons/hicolor/scalable/apps/xfce4-panel.svg
%{_datadir}/locale/*/LC_MESSAGES/xfce4-panel.mo
%{_datadir}/xfce4/panel/plugins/*.desktop

%clean
rm -rf %{buildroot}

%changelog
* Thu Jun 20 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.10.0 to 4.10.1
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
