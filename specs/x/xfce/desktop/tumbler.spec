Name:       tumbler
Version:    0.1.29
Release:    2%{?dist}
Summary:    Tumbler

Group:      User Interface/Xfce
License:    GPLv2+
Url:        http://www.xfce.org
Source:     http://archive.xfce.org/src/xfce/tumbler/0.1/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  dbus-glib intltool
BuildRequires:  freetype gdk-pixbuf gtk-doc libjpeg-turbo libpng
BuildRequires:  gettext

%description
The Tumbler package contains a D-Bus thumbnailing service based on the thumbnail management D-Bus specification.
This is useful for generating thumbnail images of files.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --sysconfdir=/etc \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
/etc/xdg/tumbler/tumbler.rc
%{_includedir}/tumbler-1/tumbler/tumbler*.h
%{_libdir}/libtumbler-1.*
%{_libdir}/pkgconfig/tumbler-1.pc
%{_libdir}/tumbler-1/plugins/cache/tumbler-cache-plugin.so
%{_libdir}/tumbler-1/plugins/cache/tumbler-xdg-cache.*
%{_libdir}/tumbler-1/plugins/tumbler-font-thumbnailer.*
%{_libdir}/tumbler-1/plugins/tumbler-jpeg-thumbnailer.*
%{_libdir}/tumbler-1/plugins/tumbler-pixbuf-thumbnailer.*
%{_libdir}/tumbler-1/tumblerd
%{_datadir}/dbus-1/services/org.xfce.Tumbler.*.service
%{_datadir}/gtk-doc/html/tumbler/*
%{_datadir}/locale/*/LC_MESSAGES/tumbler.mo

%clean
rm -rf %{buildroot}

%changelog
* Thu May 30 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.1.27 to 0.1.29
* Mon Apr 8 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.1.25 to 0.1.27
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
