Name:       gtk-xfce-engine
Version:    3.0.1
Release:    2%{?dist}
Summary:    GTK-Xfce-Engine

Group:      User Interface/Xfce
License:    GPLv2+
Url:        http://www.xfce.org
Source:     http://archive.xfce.org/src/xfce/gtk-xfce-engine/3.0/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  gtk2 gtk+

%description
The GTK-Xfce-Engine package contains several GTK+-2 themes and the applications and libraries needed to display them.
This is useful for customising the appearance of your Xfce desktop.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_libdir}/gtk-2.0/2.10.0/engines/libxfce.*
%{_libdir}/gtk-3.0/3.0.0/theming-engines/libxfce.*
%{_datadir}/themes/Xfce*/gtk-2.0/gtkrc
%{_datadir}/themes/Xfce*/gtk-3.0/gtk.css

%clean
rm -rf %{buildroot}

%changelog
* Mon Apr 8 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.0.0 to 3.0.1
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
