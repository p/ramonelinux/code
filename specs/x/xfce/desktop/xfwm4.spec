Name:       xfwm4
Version:    4.11.0
Release:    1%{?dist}
Summary:    Xfwm4

Group:      User Interface/Xfce
License:    GPLv2+
Url:        http://www.xfce.org
Source:     http://archive.xfce.org/src/xfce/xfwm4/4.11/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  libwnck2 libxfce4ui libxfce4util
BuildRequires:  startup-notification
BuildRequires:  intltool gettext

%description
Xfwm4 is the window manager for Xfce.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/xfwm4
%{_bindir}/xfwm4-settings
%{_bindir}/xfwm4-tweaks-settings
%{_bindir}/xfwm4-workspace-settings
%{_libdir}/xfce4/xfwm4/helper-dialog
%{_datadir}/applications/xfce-*-settings.desktop
%{_datadir}/icons/hicolor/*/actions/xfce-wm-*.png
%{_datadir}/icons/hicolor/*/apps/*.png
%{_datadir}/icons/hicolor/scalable/actions/xfce-wm-*.svg
%{_datadir}/icons/hicolor/scalable/apps/*.svg
%{_datadir}/locale/*/LC_MESSAGES/xfwm4.mo
%{_datadir}/themes/Daloa/xfwm4/*
%{_datadir}/themes/Default/xfwm4/*
%{_datadir}/themes/Kokodi/xfwm4/*
%{_datadir}/themes/Moheli/xfwm4/*
%{_datadir}/xfwm4/defaults

%clean
rm -rf %{buildroot}

%changelog
* Sat Nov 9 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.10.1 to 4.11.0
* Thu Jun 20 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.10.0 to 4.10.1
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
