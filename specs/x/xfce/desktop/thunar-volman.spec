Name:       thunar-volman
Version:    0.8.0
Release:    5%{?dist}
Summary:    Thunar Volume Manager

Group:      User Interface/Xfce
License:    GPLv2+
Url:        http://www.xfce.org
Source:     http://archive.xfce.org/src/xfce/thunar-volman/0.8/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  exo libxfce4ui systemd-gudev
BuildRequires:  libnotify startup-notification
BuildRequires:  intltool gettext xml-parser

%description
The Thunar Volume Manager is an extension for the Thunar file manager, which enables automatic management of removable drives and media.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/thunar-volman
%{_bindir}/thunar-volman-settings
%{_datadir}/applications/thunar-volman-settings.desktop
%{_datadir}/icons/hicolor/48x48/apps/tvm-*.png
%{_datadir}/icons/hicolor/scalable/apps/tvm-burn-cd.svg
%{_datadir}/locale/*/LC_MESSAGES/thunar-volman.mo

%clean
rm -rf %{buildroot}

%changelog
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
