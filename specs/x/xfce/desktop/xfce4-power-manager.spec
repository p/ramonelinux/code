Name:       xfce4-power-manager
Version:    1.2.0
Release:    7%{?dist}
Summary:    Xfce4 Power Manager

Group:      User Interface/Xfce
License:    GPLv2+
Url:        http://www.xfce.org
Source:     http://archive.xfce.org/src/xfce/xfce4-power-manager/1.2/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  libnotify upower xfce4-panel
BuildRequires:  intltool gettext xml-parser

%description
The Xfce4 Power Manager is a power manager for the Xfce desktop, Xfce power manager manages the power sources on the computer and the devices that can be controlled to reduce their power consumption (such as LCD brightness level, monitor sleep, CPU frequency scaling).
In addition, Xfce4 Power Manager provides a set of freedesktop-compliant DBus interfaces to inform other applications about current power level so that they can adjust their power consumption.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --sysconfdir=/etc \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make docdir=/usr/share/doc/xfce4-power-manager-1.2.0 \
  imagesdir=/usr/share/doc/xfce4-power-manager-1.2.0/images install DESTDIR=%{buildroot}

%files
%defattr(-,root,root,-)
/etc/xdg/autostart/xfce4-power-manager.desktop
%{_bindir}/xfce4-power-information
%{_bindir}/xfce4-power-manager
%{_bindir}/xfce4-power-manager-settings
%{_libdir}/xfce4/panel-plugins/xfce4-brightness-plugin
%{_sbindir}/xfpm-power-backlight-helper
%{_datadir}/applications/xfce4-power-manager-settings.desktop
%{_datadir}/doc/xfce4-power-manager-1.2.0/images/xfpm-*.png
%{_datadir}/doc/xfce4-power-manager-1.2.0/xfce4-power-manager.html
%{_datadir}/icons/hicolor/128x128/devices/processor.png
%{_datadir}/icons/hicolor/*/actions/xfpm-*.*
%{_datadir}/icons/hicolor/*/status/xfpm-*.*
%{_datadir}/locale/*/LC_MESSAGES/xfce4-power-manager.mo
%{_datadir}/polkit-1/actions/org.xfce.power.policy
%{_datadir}/xfce4/panel-plugins/xfce4-brightness-plugin.desktop
%{_mandir}/man1/xfce4-power-manager*.1.gz

%clean
rm -rf %{buildroot}

%changelog
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
