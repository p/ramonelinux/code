Name:       libxfcegui4
Version:    4.10.0
Release:    6%{?dist}
Summary:    LibXfceGUI4

Group:      User Interface/Xfce
License:    GPLv2+
Url:        http://www.xfce.org
Source:     http://archive.xfce.org/src/xfce/libxfcegui4/4.10/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  gtk2 libglade libxfce4util
BuildRequires:  intltool gettext

%description
The LibXfceGUI4 package provides the basic GUI functions used by Xfce.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --disable-static \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_includedir}/xfce4/libxfcegui4/*.h
%{_libdir}/libglade/2.0/libxfce4.*
%{_libdir}/libxfcegui4.*
%{_libdir}/pkgconfig/libxfcegui4-1.0.pc
%{_datadir}/gtk-doc/html/libxfcegui4/*
%{_datadir}/icons/hicolor/48x48/apps/xfce-*.png
%{_datadir}/icons/hicolor/scalable/apps/xfce-filemanager.svg
%{_datadir}/locale/*/LC_MESSAGES/libxfcegui4.mo

%clean
rm -rf %{buildroot}

%changelog
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
