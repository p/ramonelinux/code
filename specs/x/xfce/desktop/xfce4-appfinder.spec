Name:       xfce4-appfinder
Version:    4.10.1
Release:    1%{?dist}
Summary:    Xfce4-Appfinder

License:    GPLv2+
Group:      User Interface/Xfce
Url:        http://www.xfce.org
Source:     http://archive.xfce.org/src/xfce/xfce4-appfinder/4.10/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  garcon libxfce4ui
BuildRequires:  intltool gettext xml-parser

%description
Xfce4-Appfinder is a tool to find and launch installed applications by searching the .desktop files installed on your system.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/xfce4-appfinder
%{_bindir}/xfrun4
%{_datadir}/applications/xfce4-appfinder.desktop
%{_datadir}/applications/xfce4-run.desktop
%{_datadir}/locale/*/LC_MESSAGES/xfce4-appfinder.mo

%clean
rm -rf %{buildroot}

%changelog
* Thu Jun 20 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.10.0 to 4.10.1
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
