Name:       garcon
Version:    0.2.1
Release:    1%{?dist}
Summary:    Garcon

Group:      User Interface/Xfce
License:    GPLv2+
Url:        http://www.xfce.org
Source:     http://archive.xfce.org/src/xfce/garcon/0.2/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  libxfce4util
BuildRequires:  intltool gettext

%description
The Garcon package contains a freedesktop.org compliant menu implementation based on GLib and GIO.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --sysconfdir=/etc \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
/etc/xdg/menus/xfce-applications.menu
%{_includedir}/garcon-1/garcon/garcon*.h
%{_libdir}/libgarcon-1.*
%{_libdir}/pkgconfig/garcon-1.pc
%{_datadir}/desktop-directories/xfce-*.directory
%{_datadir}/gtk-doc/html/garcon/*
%{_datadir}/locale/*/LC_MESSAGES/garcon.mo

%clean
rm -rf %{buildroot}

%changelog
* Thu Jun 20 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.2.0 to 0.2.1
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
