Name:       exo
Version:    0.10.2
Release:    2%{?dist}
Summary:    exo

Group:      User Interface/Xfce
License:    GPLv2+
Url:        http://www.xfce.org
Source:     http://archive.xfce.org/src/xfce/exo/0.10/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  libxfce4ui libxfce4util perl-uri
BuildRequires:  intltool gettext

%description
Exo is a support library used in the Xfce desktop.
It also has some helper applications that are used throughout Xfce.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --sysconfdir=/etc \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
/etc/xdg/xfce4/helpers.rc
%{_bindir}/exo-csource
%{_bindir}/exo-desktop-item-edit
%{_bindir}/exo-open
%{_bindir}/exo-preferred-applications
%{_includedir}/exo-1/exo/exo*.h
%{_libdir}/libexo-1.*
%{_libdir}/pkgconfig/exo-1.pc
%{_libdir}/xfce4/exo-1/exo-compose-mail-1
%{_libdir}/xfce4/exo-1/exo-helper-1
%{_datadir}/applications/exo-*.desktop
%{_datadir}/gtk-doc/html/exo-1/*
%{_datadir}/icons/hicolor/*/apps/*applications*.png
%{_datadir}/locale/*/LC_MESSAGES/exo-1.mo
%{_datadir}/pixmaps/exo-1/exo-thumbnail-frame.png
%{_datadir}/xfce4/helpers/*.desktop
%{_mandir}/man1/exo-*.1.gz

%clean
rm -rf %{buildroot}

%changelog
* Mon Apr 8 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.8.0 to 0.10.2
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
