Name:       thunar
Version:    1.6.3
Release:    3%{?dist}
Summary:    Thunar

Group:      User Interface/Xfce
License:    GPLv2+
Url:        http://www.xfce.org
Source:     http://archive.xfce.org/src/xfce/thunar/1.6/Thunar-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  exo libxfce4ui
BuildRequires:  libnotify startup-notification systemd-gudev xfce4-panel
BuildRequires:  intltool gettext xml-parser

%description
Thunar is the Xfce file manager, a Gtk 2 GUI to organise the files on your computer.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n Thunar-%{version}

%build
./configure --prefix=/usr \
            --sysconfdir=/etc \
            --docdir=/usr/share/doc/Thunar-%{version} \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
/etc/xdg/Thunar/uca.xml
%{_bindir}/Thunar
%{_bindir}/thunar
%{_bindir}/thunar-settings
%{_includedir}/thunarx-2/thunarx/thunarx*.h
%{_libdir}/Thunar/ThunarBulkRename
%{_libdir}/Thunar/thunar-sendto-email
%{_libdir}/libthunarx-2.*
%{_libdir}/pkgconfig/thunarx-2.pc
%{_libdir}/thunarx-2/thunar-apr.*
%{_libdir}/thunarx-2/thunar-sbr.*
%{_libdir}/thunarx-2/thunar-uca.*
%{_libdir}/thunarx-2/thunar-wallpaper-plugin.*
%{_libdir}/xfce4/panel/plugins/libthunar-tpa.*
%{_datadir}/Thunar/sendto/thunar-sendto-email.desktop
%{_datadir}/applications/*hunar*.desktop
%{_datadir}/dbus-1/services/org.xfce.*.service
%{_datadir}/icons/hicolor/*/apps/*.png
%{_datadir}/icons/hicolor/*/stock/navigation/*.png
%{_datadir}/icons/hicolor/scalable/apps/Thunar.svg
%{_datadir}/locale/*/LC_MESSAGES/Thunar.mo
%{_datadir}/pixmaps/Thunar/Thunar-about-logo.png
%{_datadir}/xfce4/panel-plugins/thunar-tpa.desktop

%files doc
%defattr(-,root,root,-)
%{_docdir}/Thunar-%{version}/README.*rc
%{_datadir}/gtk-doc/html/thunarx/*
%{_mandir}/man1/Thunar.1.gz

%clean
rm -rf %{buildroot}

%changelog
* Thu Jun 20 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.6.2 to 1.6.3
* Mon Apr 8 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.4.0 to 1.6.2
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
