Name:       libxfce4ui
Version:    4.11.0
Release:    1%{?dist}
Summary:    LibXfce4UI

Group:      User Interface/Xfce
License:    GPLv2+
Url:        http://www.xfce.org
Source:     http://archive.xfce.org/src/xfce/libxfce4ui/4.11/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  gtk2 xfconf
BuildRequires:  startup-notification
BuildRequires:  intltool gettext xml-parser

%description
The LibXfce4UI package contains Gtk 2 widgets that are used by other Xfce applications.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --sysconfdir=/etc \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
/etc/xdg/xfce4/xfconf/xfce-perchannel-xml/xfce4-keyboard-shortcuts.xml
%{_bindir}/xfce4-about
%{_includedir}/xfce4/libxfce4kbd-private-2/libxfce4kbd-private/xfce-shortcut*.h
%{_includedir}/xfce4/libxfce4ui-1/libxfce4ui/libxfce4ui*.h
%{_includedir}/xfce4/libxfce4ui-1/libxfce4ui/xfce-*.h
%{_libdir}/libxfce4kbd-private-2.*
%{_libdir}/libxfce4ui-1.*
%{_libdir}/pkgconfig/libxfce4kbd-private-2.pc
%{_libdir}/pkgconfig/libxfce4ui-1.pc
%{_datadir}/applications/xfce4-about.desktop
%{_datadir}/gtk-doc/html/libxfce4ui/*
%{_datadir}/icons/hicolor/48x48/apps/xfce4-logo.png
%{_datadir}/locale/*/LC_MESSAGES/libxfce4ui.mo

%clean
rm -rf %{buildroot}

%changelog
* Sat Nov 9 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.10.0 to 4.11.0
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
