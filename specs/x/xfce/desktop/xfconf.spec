Name:       xfconf
Version:    4.10.0
Release:    5%{?dist}
Summary:    Xfconf

Group:      User Interface/Xfce
License:    GPLv2+
Url:        http://www.xfce.org
Source:     http://archive.xfce.org/src/xfce/xfconf/4.10/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  dbus-glib glib libxfce4util
BuildRequires:  intltool gettext xml-parser

%description
Xfconf is the configuration storage system for Xfce.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --disable-static \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/xfconf-query
%{_includedir}/xfce4/xfconf-0/xfconf/xfconf*.h
%{_libdir}/libxfconf-0.*
%{_libdir}/pkgconfig/libxfconf-0.pc
%{_libdir}/xfce4/xfconf/xfconfd
%{_datadir}/dbus-1/services/org.xfce.Xfconf.service
%{_datadir}/gtk-doc/html/xfconf/*
%{_datadir}/locale/*/LC_MESSAGES/xfconf.mo

%clean
rm -rf %{buildroot}

%changelog
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
