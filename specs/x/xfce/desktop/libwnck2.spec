Name:       libwnck2
Version:    2.30.7
Release:    7%{?dist}
Summary:    Libwnck

Group:      User Interface/GNOME
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/libwnck/2.30/libwnck-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  gtk2 intltool
BuildRequires:  gobject-introspection startup-notification
BuildRequires:  gettext xml-parser gtk-doc
BuildRequires:  resourceproto libxres

%description
The Libwnck package contains a Window Navigator Construction Kit.

%prep
%setup -q -n libwnck-%{version}

%build
./configure --prefix=/usr \
            --disable-static \
            --program-suffix=-1 \
            --libdir=%{_libdir} &&
make GETTEXT_PACKAGE=libwnck-1 %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make GETTEXT_PACKAGE=libwnck-1 install DESTDIR=%{buildroot}

%files
%defattr(-,root,root,-)
%{_bindir}/wnck-urgency-monitor-1
%{_bindir}/wnckprop-1
%{_includedir}/libwnck-1.0/libwnck/*.h
%{_libdir}/girepository-1.0/Wnck-1.0.typelib
%{_libdir}/libwnck-1.*
%{_libdir}/pkgconfig/libwnck-1.0.pc
%{_datadir}/gir-1.0/Wnck-1.0.gir
%{_datadir}/gtk-doc/html/libwnck/*
%{_datadir}/locale/*/LC_MESSAGES/libwnck-1.mo

%clean
rm -rf %{buildroot}

%changelog
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
