Name:       libxfce4util
Version:    4.10.1
Release:    2%{?dist}
Summary:    LibXfce4Util

Group:      User Interface/Xfce
License:    GPLv2+
Url:        http://www.xfce.org
Source:     http://archive.xfce.org/src/xfce/libxfce4util/4.10/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  intltool pkg-config
BuildRequires:  gettext glib

%description
The LibXfce4Util package is a basic utility library for the Xfce desktop environment.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --disable-static \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_includedir}/xfce4/libxfce4util/libxfce4util*.h
%{_includedir}/xfce4/libxfce4util/xfce-*.h
%{_libdir}/libxfce4util.*
%{_libdir}/pkgconfig/libxfce4util-1.0.pc
%{_sbindir}/xfce4-kiosk-query
%{_datadir}/gtk-doc/html/libxfce4util/*
%{_datadir}/locale/*/LC_MESSAGES/libxfce4util.mo

%clean
rm -rf %{buildroot}

%changelog
* Thu Jun 20 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.10.0 to 4.10.1
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
