Name:       xfce4-settings
Version:    4.10.1
Release:    1%{?dist}
Summary:    Xfce4 Settings

Group:      User Interface/Xfce
License:    GPLv2+
Url:        http://www.xfce.org
Source:     http://archive.xfce.org/src/xfce/xfce4-settings/4.10/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  exo libxfce4ui
BuildRequires:  libnotify libxklavier
BuildRequires:  intltool gettext
BuildRequires:  garcon libxi

%description
The Xfce4 Settings package contains a collection of programs that are useful for adjusting your Xfce preferences.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --sysconfdir=/etc \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
/etc/xdg/autostart/xfsettingsd.desktop
/etc/xdg/menus/xfce-settings-manager.menu
/etc/xdg/xfce4/xfconf/xfce-perchannel-xml/xsettings.xml
%{_bindir}/xfce4-accessibility-settings
%{_bindir}/xfce4-appearance-settings
%{_bindir}/xfce4-display-settings
%{_bindir}/xfce4-keyboard-settings
%{_bindir}/xfce4-mime-settings
%{_bindir}/xfce4-mouse-settings
%{_bindir}/xfce4-settings-editor
%{_bindir}/xfce4-settings-manager
%{_bindir}/xfsettingsd
%{_libdir}/xfce4/settings/appearance-install-theme
%{_datadir}/applications/xfce*-settings*.desktop
%{_datadir}/locale/*/LC_MESSAGES/xfce4-settings.mo

%clean
rm -rf %{buildroot}

%changelog
* Thu Jun 20 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.10.0 to 4.10.1
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
