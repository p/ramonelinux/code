Name:       xfdesktop
Version:    4.11.1
Release:    1%{?dist}
Summary:    Xfdesktop

Group:      User Interface/Xfce
License:    GPLv2+
Url:        http://www.xfce.org
Source:     http://archive.xfce.org/src/xfce/xfdesktop/4.11/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  exo libwnck2 libxfce4ui
BuildRequires:  libnotify startup-notification thunar
BuildRequires:  intltool gettext garcon

%description
Xfdesktop is a desktop manager for the Xfce Desktop Environment.
Xfdesktop sets the background image / color, creates the right click menu and window list and displays the file icons on the desktop using Thunar libraries.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/xfdesktop
%{_bindir}/xfdesktop-settings
%{_datadir}/applications/xfce-backdrop-settings.desktop
%{_datadir}/backgrounds/xfce/xfce-blue.jpg
%{_datadir}/icons/hicolor/*/apps/xfce4-*.png
%{_datadir}/icons/hicolor/*/apps/xfce4-*.svg
%{_datadir}/locale/*/LC_MESSAGES/xfdesktop.mo
%{_datadir}/pixmaps/xfce4_xicon*.png
%{_datadir}/pixmaps/xfdesktop/xfdesktop-fallback-icon.png
%{_mandir}/man1/xfdesktop.1.gz

%post
gtk-update-icon-cache -f -t /usr/share/icons/hicolor

%postun
gtk-update-icon-cache -f -t /usr/share/icons/hicolor

%clean
rm -rf %{buildroot}

%changelog
* Sat Nov 9 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.10.2 to 4.11.1
* Mon Apr 8 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.10.0 to 4.10.2
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
