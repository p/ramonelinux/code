Name:       xfce4-session
Version:    4.10.1
Release:    2%{?dist}
Summary:    Xfce4 Session

Group:      User Interface/Xfce
License:    GPLv2+
Url:        http://www.xfce.org
Source:     http://archive.xfce.org/src/xfce/xfce4-session/4.10/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  libwnck2 libxfce4ui
BuildRequires:  intltool gettext xml-parser iceauth libsm
Requires:       consolekit gnupg hicolor-icon-theme

%description
Xfce4 Session is a session manager for Xfce.
Its task is to save the state of your desktop (opened applications and their location) and restore it during a next startup.
You can create several different sessions and choose one of them on startup.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --sysconfdir=/etc \
            --disable-legacy-sm \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%post
gtk-update-icon-cache -f -t /usr/share/icons/hicolor

%postun

%files
%defattr(-,root,root,-)
/etc/xdg/autostart/xscreensaver.desktop
/etc/xdg/xfce4/Xft.xrdb
/etc/xdg/xfce4/xfconf/xfce-perchannel-xml/xfce4-session.xml
/etc/xdg/xfce4/xinitrc
%{_bindir}/startxfce4
%{_bindir}/xfce4-session
%{_bindir}/xfce4-session-logout
%{_bindir}/xfce4-session-settings
%{_bindir}/xflock4
%{_includedir}/xfce4/xfce4-session-4.6/libxfsm/xfsm-splash-*.h
%{_libdir}/libxfsm-4.6.*
%{_libdir}/pkgconfig/xfce4-session-2.0.pc
%{_libdir}/xfce4/session/balou-export-theme
%{_libdir}/xfce4/session/balou-install-theme
%{_libdir}/xfce4/session/splash-engines/libbalou.*
%{_libdir}/xfce4/session/splash-engines/libmice.*
%{_libdir}/xfce4/session/splash-engines/libsimple.*
%{_libdir}/xfce4/session/xfsm-shutdown-helper
%{_datadir}/applications/xfce*-session-*.desktop
%{_datadir}/icons/hicolor/*/apps/*.png
%{_datadir}/icons/hicolor/*/apps/*.svg
%{_datadir}/locale/*/LC_MESSAGES/xfce4-session.mo
%{_datadir}/themes/Default/balou/logo.png
%{_datadir}/themes/Default/balou/themerc
%{_datadir}/xsessions/xfce.desktop
%{_mandir}/man1/xfce4-session*.1.gz

%clean
rm -rf %{buildroot}

%changelog
* Thu Jun 20 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.10.0 to 4.10.1
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
