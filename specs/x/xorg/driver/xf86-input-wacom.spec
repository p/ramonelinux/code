Name:       xf86-input-wacom
Version:    0.31.0
Release:    1%{?dist}
Summary:    Xorg Wacom Driver

Group:      User Interface/Xorg
License:    GPLv2+
Url:        http://xorg.freedesktop.org
Source:     http://xorg.freedesktop.org/releases/individual/driver/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  xserver libxrandr libxinerama util-macros
BuildRequires:  resourceproto scrnsaverproto systemd

%description
The Xorg Wacom Driver package contains the X.Org X11 driver and SDK for Wacom and Wacom-like tablets.
It is not required to use a Wacom tablet, the xf86-input-evdev driver can handle these devices without problems.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --sysconfdir=/etc \
            --mandir=/usr/share/man --localstatedir=/var \
            --with-udev-rules-dir=/lib/udev/rules.d \
            --with-systemd-unit-dir=/lib/systemd/system \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/isdv4-serial-debugger
%{_bindir}/isdv4-serial-inputattach
%{_bindir}/xsetwacom
%{_includedir}/xorg/*.h
%{_libdir}/xorg/modules/input/wacom_drv.*
%{_libdir}/pkgconfig/xorg-wacom.pc
%{_datadir}/X11/xorg.conf.d/50-wacom.conf
/lib/udev/rules.d/wacom.rules
/lib/systemd/system/wacom-inputattach@.service

%files doc
%defattr(-,root,root,-)
%{_mandir}/*/*

%clean
rm -rf %{buildroot}

%changelog
* Mon Sep 14 2015 tanggeliang <tanggeliang@gmail.com>
- update from 0.30.0 to 0.31.0
* Sat Jun 27 2015 tanggeliang <tanggeliang@gmail.com>
- update from 0.25.0 to 0.30.0
* Wed Jul 23 2014 tanggeliang <tanggeliang@gmail.com>
- update from 0.23.0 to 0.25.0
* Fri Mar 14 2014 tanggeliang <tanggeliang@gmail.com>
- update from 0.22.1 to 0.23.0
* Thu Oct 3 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.20.0 to 0.22.1
* Sat Mar 16 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.19.0 to 0.20.0
* Sat Feb 23 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.17.0 to 0.19.0
* Mon Oct 15 2012 tanggeliang <tanggeliang@gmail.com>
- update from 0.14.0 to 0.17.0
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
