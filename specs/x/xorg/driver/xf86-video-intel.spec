Name:       xf86-video-intel
Version:    2.99.917
Release:    3%{?dist}
Summary:    Xorg Intel Driver

Group:      User Interface/Xorg
License:    GPLv2+
Url:        http://xorg.freedesktop.org
Source:     http://xorg.freedesktop.org/releases/individual/driver/%{name}-%{version}.tar.bz2
Patch0:	    %{name}-%{version}-ramone-stat.diff
Patch1:	    %{name}-20150729.diff

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  util-macros xserver libxshmfence
BuildRequires:	libxrandr libxdamage libxfixes libxcursor libxtst libxrender libxext libx11 libxinerama
BuildRequires:  pixman libxvmc xcb-util libxcb mesa
BuildRequires:  resourceproto scrnsaverproto dri2proto dri3proto
BuildRequires:	autoconf automake m4

%description
The Xorg Intel Driver package contains the X.Org Video Driver for Intel integrated video cards including 8xx, 9xx, Gxx, Qxx and HD graphics processors.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}
%patch0 -p1
%patch1 -p1

%build

./configure --prefix=/usr --sysconfdir=/etc \
            --mandir=/usr/share/man --localstatedir=/var \
            --enable-kms-only \
	    --enable-uxa \
	    --enable-tools \
            --libexecdir=%{_libdir}/xorg \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/intel-virtual-output
%{_libdir}/libIntelXvMC.*
%{_libdir}/xorg/modules/drivers/intel_drv.*
%{_libdir}/xorg/xf86-video-intel-backlight-helper
%{_datadir}/polkit-1/actions/org.x.xf86-video-intel.backlight-helper.policy

%files doc
%defattr(-,root,root,-)
%{_mandir}/*/*

%clean
rm -rf %{buildroot}

%changelog
* Sat Jun 27 2015 tanggeliang <tanggeliang@gmail.com>
- update from 2.99.911 to 2.99.917
* Wed Jul 23 2014 tanggeliang <tanggeliang@gmail.com>
- update from 2.21.15 to 2.99.911
* Thu Oct 3 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.21.8 to 2.21.15
* Wed Jun 5 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.21.3 to 2.21.8
* Sat Mar 16 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.20.19 to 2.21.3
* Sat Feb 23 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.20.9 to 2.20.19
* Sun Oct 14 2012 tanggeliang <tanggeliang@gmail.com>
- update from 2.19.0 to 2.20.9
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
