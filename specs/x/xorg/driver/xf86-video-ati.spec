Name:       xf86-video-ati
Version:    7.5.0
Release:    1%{?dist}
Summary:    Xorg ATI Driver

Group:      User Interface/Xorg
License:    GPLv2+
Url:        http://xorg.freedesktop.org
Source:     http://xorg.freedesktop.org/releases/individual/driver/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  pixman libdrm mesa util-macros xserver
BuildRequires:  xf86driproto glproto libxcb
BuildRequires:  resourceproto scrnsaverproto
BuildRequires:  glamor-egl

%description
The Xorg ATI Driver package contains the X.Org Video Driver for ATI Radeon video cards including all chipsets ranging from R100 to R700 including newer Radeon HD ones.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --sysconfdir=/etc \
            --mandir=/usr/share/man --localstatedir=/var \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_libdir}/xorg/modules/drivers/ati_drv.*
%{_libdir}/xorg/modules/drivers/radeon_drv.*

%files doc
%defattr(-,root,root,-)
%{_mandir}/*/*

%clean
rm -rf %{buildroot}

%changelog
* Sat Jun 27 2015 tanggeliang <tanggeliang@gmail.com>
- update from 7.4.0 to 7.5.0
* Wed Jul 23 2014 tanggeliang <tanggeliang@gmail.com>
- update from 7.3.0 to 7.4.0
* Fri Mar 14 2014 tanggeliang <tanggeliang@gmail.com>
- update from 7.2.0 to 7.3.0
* Thu Oct 3 2013 tanggeliang <tanggeliang@gmail.com>
- update from 7.1.0 to 7.2.0
* Sat Mar 16 2013 tanggeliang <tanggeliang@gmail.com>
- update from 7.0.0 to 7.1.0
* Sat Feb 23 2013 tanggeliang <tanggeliang@gmail.com>
- update from 6.14.6 to 7.0.0
* Mon Oct 15 2012 tanggeliang <tanggeliang@gmail.com>
- update from 6.14.4 to 6.14.6
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
