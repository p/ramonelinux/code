Name:       xf86-video-openchrome
Version:    0.3.3
Release:    2%{?dist}
Summary:    Xorg OpenChrome Driver

Group:      User Interface/Xorg
License:    GPLv2+
Url:        http://xorg.freedesktop.org
Source:     http://xorg.freedesktop.org/releases/individual/driver/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  util-macros xserver
BuildRequires:  xf86driproto libxvmc mesa glproto
BuildRequires:  resourceproto scrnsaverproto

%description
The Xorg OpenChrome Driver package contains the X.Org Video Driver for Via integrated video cards including Unichrome, Unichrome Pro and Chrome9 series.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --sysconfdir=/etc \
            --mandir=/usr/share/man --localstatedir=/var \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_libdir}/libchromeXvMC.*
%{_libdir}/libchromeXvMCPro.*
%{_libdir}/xorg/modules/drivers/openchrome_drv.*

%files doc
%defattr(-,root,root,-)
%{_mandir}/*/*

%clean
rm -rf %{buildroot}

%changelog
* Thu Oct 3 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.3.1 to 0.3.3
* Mon Oct 15 2012 tanggeliang <tanggeliang@gmail.com>
- update from 0.2.906 to 0.3.1
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
