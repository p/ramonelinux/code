Name:       xf86-video-s3virge
Version:    1.10.6
Release:    3%{?dist}
Summary:    xf86-video-s3virge

Group:      User Interface/Xorg
License:    GPLv2+
Url:        http://xorg.freedesktop.org
Source:     http://xorg.freedesktop.org/releases/individual/driver/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  pixman xserver
BuildRequires:  resourceproto scrnsaverproto

%description

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --sysconfdir=/etc \
            --mandir=/usr/share/man --localstatedir=/var \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_libdir}/xorg/modules/drivers/s3virge_drv.*
%{_mandir}/*/*

%clean
rm -rf %{buildroot}

%changelog
* Wed Oct 17 2012 tanggeliang <tanggeliang@gmail.com>
- update from 1.10.4 to 1.10.6
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
