Name:       xf86-video-r128
Version:    6.9.2
Release:    2%{?dist}
Summary:    Xorg R128 Driver

Group:      User Interface/Xorg
License:    GPLv2+
Url:        http://xorg.freedesktop.org
Source:     http://xorg.freedesktop.org/releases/individual/driver/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  pixman libdrm mesa xserver
BuildRequires:  xf86driproto glproto
BuildRequires:  resourceproto scrnsaverproto

%description
The Xorg R128 Driver package contains the X.Org Video Driver for ATI Rage 128 based video cards.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --sysconfdir=/etc \
            --mandir=/usr/share/man --localstatedir=/var \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_libdir}/xorg/modules/drivers/r128_drv.*

%files doc
%defattr(-,root,root,-)
%{_mandir}/*/*

%clean
rm -rf %{buildroot}

%changelog
* Fri Mar 14 2014 tanggeliang <tanggeliang@gmail.com>
- update from 6.9.1 to 6.9.2
* Mon Oct 15 2012 tanggeliang <tanggeliang@gmail.com>
- update from 6.8.2 to 6.9.1
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
