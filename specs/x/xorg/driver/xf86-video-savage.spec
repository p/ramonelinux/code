Name:       xf86-video-savage
Version:    2.3.7
Release:    2%{?dist}
Summary:    Xorg Savage Driver

Group:      User Interface/Xorg
License:    GPLv2+
Url:        http://xorg.freedesktop.org
Source:     http://xorg.freedesktop.org/releases/individual/driver/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  pixman libdrm mesa xserver
BuildRequires:  xf86driproto glproto
BuildRequires:  resourceproto scrnsaverproto

%description
The Xorg Savage Driver package contains the X.Org Video Driver for the S3 Savage family video accelerator chips including Savage3D, Savage4, Savage/MX, Savage/IX, SuperSavage/MX, SuperSavage/IX, ProSavage PM133, ProSavage KM133, Twister, TwisterK, ProSavage DDR and ProSavage DDR-K series.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --sysconfdir=/etc \
            --mandir=/usr/share/man --localstatedir=/var \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_libdir}/xorg/modules/drivers/savage_drv.*

%files doc
%defattr(-,root,root,-)
%{_mandir}/*/*

%clean
rm -rf %{buildroot}

%changelog
* Thu Oct 3 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.3.6 to 2.3.7
* Mon Oct 15 2012 tanggeliang <tanggeliang@gmail.com>
- update from 2.3.4 to 2.3.6
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
