Name:       xf86-input-vmmouse
Version:    13.0.0
Release:    4%{?dist}
Summary:    Xorg VMMouse Driver

Group:      User Interface/Xorg
License:    GPLv2+
Url:        http://xorg.freedesktop.org
Source:     http://xorg.freedesktop.org/releases/individual/driver/%{name}-%{version}.tar.bz2
Patch:	    %{name}-%{version}-build_fix-1.patch

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  util-macros xserver
BuildRequires:  resourceproto scrnsaverproto

%description
The Xorg VMMouse Driver package contains VMMouse input driver for the Xorg X server.
The VMMouse driver enables support for the special VMMouse protocol that is provided by VMware virtual machines to give absolute pointer positioning.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}
%patch -p1

%build
sed -i -e '/__i386__/a iopl(3);' tools/vmmouse_detect.c      &&

./configure --prefix=/usr --sysconfdir=/etc \
            --mandir=/usr/share/man --localstatedir=/var \
            --without-hal-callouts-dir 	    \
            --without-hal-fdi-dir 	    \
            --with-udev-rules-dir=/lib/udev/rules.d \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
/lib/udev/rules.d/69-xorg-vmmouse.rules
%{_bindir}/vmmouse_detect
%{_libdir}/xorg/modules/input/vmmouse_drv.*
%{_datadir}/X11/xorg.conf.d/50-vmmouse.conf

%files doc
%defattr(-,root,root,-)
%{_mandir}/*/*

%clean
rm -rf %{buildroot}

%changelog
* Sat Feb 23 2013 tanggeliang <tanggeliang@gmail.com>
- update from 12.9.0 to 13.0.0
* Mon Oct 15 2012 tanggeliang <tanggeliang@gmail.com>
- update from 12.8.0 to 12.9.0
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
