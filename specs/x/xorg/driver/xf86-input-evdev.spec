Name:       xf86-input-evdev
Version:    2.9.2
Release:    1%{?dist}
Summary:    Xorg Evdev Driver

Group:      User Interface/Xorg
License:    GPLv2+
Url:        http://xorg.freedesktop.org
Source:     http://xorg.freedesktop.org/releases/individual/driver/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  libevdev xserver util-macros mtdev
BuildRequires:  resourceproto scrnsaverproto

%description
The Xorg Drivers page contains the instructions for building Xorg drivers that are necesary in order for Xorg Server to take the advantage of the hardware that it is running on. At least one input and one video driver is required for Xorg Server to start.

The Xorg Evdev Driver package contains Generic Linux input driver for the Xorg X server. It handles keyboard, mouse, touchpads and wacom devices, even tough for touchpad and wacom advanced handling, additional drives are required.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --sysconfdir=/etc \
            --mandir=/usr/share/man --localstatedir=/var \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_includedir}/xorg/evdev-properties.h
%{_libdir}/pkgconfig/xorg-evdev.pc
%{_libdir}/xorg/modules/input/evdev_drv.*

%files doc
%defattr(-,root,root,-)
%{_mandir}/*/*

%clean
rm -rf %{buildroot}

%changelog
* Sat Jun 27 2015 tanggeliang <tanggeliang@gmail.com>
- update from 2.9.0 to 2.9.2
* Wed Jul 23 2014 tanggeliang <tanggeliang@gmail.com>
- update from 2.8.2 to 2.9.0
* Fri Mar 14 2014 tanggeliang <tanggeliang@gmail.com>
- update from 2.8.1 to 2.8.2
* Thu Oct 3 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.8.0 to 2.8.1
* Wed Jun 5 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.7.3 to 2.8.0
* Mon Oct 15 2012 tanggeliang <tanggeliang@gmail.com>
- update from 2.7.0 to 2.7.3
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
