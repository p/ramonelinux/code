Name:       xf86-input-acecad
Version:    1.5.0
Release:    4%{?dist}
Summary:    Acecad Flair input driver

Group:      User Interface/Xorg
License:    GPLv2+
Url:        http://xorg.freedesktop.org
Source:     http://xorg.freedesktop.org/releases/individual/driver/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  pixman xserver util-macros
BuildRequires:  resourceproto scrnsaverproto

%description
Acecad Flair input driver for the Xorg X server

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --sysconfdir=/etc \
            --mandir=/usr/share/man --localstatedir=/var \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_libdir}/xorg/modules/input/acecad_drv.*
%{_mandir}/*/*

%clean
rm -rf %{buildroot}

%changelog
* Wed Oct 17 2012 tanggeliang <tanggeliang@gmail.com>
- update from 1.4.0 to 1.5.0
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
