Name:       xf86-video-nouveau
Version:    1.0.11
Release:    1%{?dist}
Summary:    Xorg Nouveau Driver

Group:      User Interface/Xorg
License:    GPLv2+
Url:        http://xorg.freedesktop.org
Source:     http://xorg.freedesktop.org/releases/individual/driver/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  util-macros xserver
BuildRequires:  xf86driproto
BuildRequires:  resourceproto scrnsaverproto

%description
The Xorg Nouveau Driver package contains the X.Org Video Driver for NVidia Cards including RIVA TNT, RIVA TNT2, GeForce 256, QUADRO, GeForce2, QUADRO2, GeForce3, QUADRO DDC, nForce, nForce2, GeForce4, QUADRO4, GeForce FX, QUADRO FX, GeForce 6XXX and GeForce 7xxx chipsets.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --sysconfdir=/etc \
            --mandir=/usr/share/man --localstatedir=/var \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_libdir}/xorg/modules/drivers/nouveau_drv.*

%files doc
%defattr(-,root,root,-)
%{_mandir}/*/*

%clean
rm -rf %{buildroot}

%changelog
* Sat Jun 27 2015 tanggeliang <tanggeliang@gmail.com>
- update from 1.0.10 to 1.0.11
* Fri Mar 14 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.0.9 to 1.0.10
* Thu Oct 3 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.0.6 to 1.0.9
* Sat Mar 16 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.0.4 to 1.0.6
* Sat Feb 23 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.0.2 to 1.0.4
* Wed Oct 10 2012 tanggeliang <tanggeliang@gmail.com>
- update from 0.0.16 to 1.0.2
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
