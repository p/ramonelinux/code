Name:       xf86-video-dummy
Version:    0.3.6
Release:    3%{?dist}
Summary:    xf86-video-dummy

Group:      User Interface/Xorg
License:    GPLv2+
Url:        http://xorg.freedesktop.org
Source:     http://xorg.freedesktop.org/releases/individual/driver/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  pixman, util-macros, xserver
BuildRequires:  xf86dgaproto
BuildRequires:  resourceproto scrnsaverproto

%description

%prep
%setup -q -n %{name}-%{version}

%build
sed -e "/mibstore.h/d" -e "/miInitializeBackingStore/d" \
    -i src/dummy_driver.c &&
./configure --prefix=/usr --sysconfdir=/etc \
            --mandir=/usr/share/man --localstatedir=/var \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_libdir}/xorg/modules/drivers/dummy_drv.*

%clean
rm -rf %{buildroot}

%changelog
* Wed Oct 17 2012 tanggeliang <tanggeliang@gmail.com>
- update from 0.3.5 to 0.3.6
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
