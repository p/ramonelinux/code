Name:       xf86-video-cirrus
Version:    1.5.2
Release:    3%{?dist}
Summary:    Xorg Cirrus Driver

Group:      User Interface/Xorg
License:    GPLv2+
Url:        http://xorg.freedesktop.org
Source:     http://xorg.freedesktop.org/releases/individual/driver/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  pixman xserver util-macros
BuildRequires:  resourceproto scrnsaverproto

%description
The Xorg Cirrus Driver package contains the X.Org Video Driver for Cirrus Logic video chips. Qemu uses this driver for its virtual GPU.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --sysconfdir=/etc \
            --mandir=/usr/share/man --localstatedir=/var \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_libdir}/xorg/modules/drivers/cirrus_*

%files doc
%defattr(-,root,root,-)
%{_mandir}/*/*

%clean
rm -rf %{buildroot}

%changelog
* Sun Mar 17 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.5.1 to 1.5.2
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
