Name:       xf86-video-ast
Version:    0.99.9
Release:    1%{?dist}
Summary:    xf86-video-ast

Group:      User Interface/Xorg
License:    GPLv2+
Url:        http://xorg.freedesktop.org
Source:     http://xorg.freedesktop.org/releases/individual/driver/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  pixman util-macros xserver
BuildRequires:  resourceproto scrnsaverproto

%description

%prep
%setup -q -n %{name}-%{version}

%build
sed -e "/mibstore.h/d" \
    -i src/*.c &&
./configure --prefix=/usr --sysconfdir=/etc \
            --mandir=/usr/share/man --localstatedir=/var \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_libdir}/xorg/modules/drivers/ast_drv.*

%clean
rm -rf %{buildroot}

%changelog
* Thu Jul 24 2014 tanggeliang <tanggeliang@gmail.com>
- update from 0.97.0 to 0.99.9
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
