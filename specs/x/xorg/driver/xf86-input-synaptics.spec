Name:       xf86-input-synaptics
Version:    1.8.2
Release:    1%{?dist}
Summary:    Xorg Synaptics Driver

Group:      User Interface/Xorg
License:    GPLv2+
Url:        http://xorg.freedesktop.org
Source:     http://xorg.freedesktop.org/releases/individual/driver/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  mtdev xserver libevdev
BuildRequires:  util-macros libxtst
BuildRequires:  resourceproto scrnsaverproto

%description
The Xorg Synaptics Driver package contains the X.Org Input Driver, support programs and SDK for Synaptics touchpads.
Even tough evdev driver can handle touchpads very well, this driver is required if you want to use advanced features like multi tapping, scrolling with touchpad, turning the touchpad off while typing, etc.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --sysconfdir=/etc \
            --mandir=/usr/share/man --localstatedir=/var \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/synclient
%{_bindir}/syndaemon
%{_includedir}/*
%{_libdir}/pkgconfig/xorg-synaptics.pc
%{_libdir}/xorg/modules/input/synaptics_drv.*
%{_datadir}/X11/xorg.conf.d/50-synaptics.conf

%files doc
%defattr(-,root,root,-)
%{_mandir}/*/*

%clean
rm -rf %{buildroot}

%changelog
* Sat Jun 27 2015 tanggeliang <tanggeliang@gmail.com>
- update from 1.8.0 to 1.8.2
* Wed Jul 23 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.7.3 to 1.8.0
* Fri Mar 14 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.7.1 to 1.7.3
* Thu Oct 3 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.6.3 to 1.7.1
* Sat Mar 16 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.6.2 to 1.6.3
* Mon Oct 15 2012 tanggeliang <tanggeliang@gmail.com>
- update from 1.6.1 to 1.6.2
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
