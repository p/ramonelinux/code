Name:       xf86-video-sis
Version:    0.10.7
Release:    5%{?dist}
Summary:    Xorg SiS Driver

Group:      User Interface/Xorg
License:    GPLv2+
Url:        http://xorg.freedesktop.org
Source:     http://xorg.freedesktop.org/releases/individual/driver/%{name}-%{version}.tar.bz2
Patch:      xf86-video-sis-0.10.7-upstream_fixes-1.patch

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  pixman libdrm mesa util-macros xserver
BuildRequires:  xf86dgaproto xf86driproto glproto
BuildRequires:  resourceproto scrnsaverproto

%description
The Xorg SiS Driver package contains the X.Org Video Driver for SiS (Silicon Integrated Systems) and XGI video cards including SiS5597/5598, SiS530/620, SiS6326/AGP/DVD, SiS300/305, SiS540, SiS630/730, SiS315/E/H/PRO, SiS550/551/552, SiS650/651/661/741, SiS330 (Xabre), SiS760/761, XGI Volari V3/V5/V8 and XGI Volari Z7 chipsets.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}
%patch -p1

%build
./configure --prefix=/usr --sysconfdir=/etc \
            --mandir=/usr/share/man --localstatedir=/var \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_libdir}/xorg/modules/drivers/sis_drv.*

%files doc
%defattr(-,root,root,-)
%{_mandir}/*/*

%clean
rm -rf %{buildroot}

%changelog
* Mon Oct 15 2012 tanggeliang <tanggeliang@gmail.com>
- update from 0.10.4 to 0.10.7
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
