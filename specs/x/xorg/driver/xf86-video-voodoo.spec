Name:       xf86-video-voodoo
Version:    1.2.5
Release:    3%{?dist}
Summary:    xf86-video-voodoo

Group:      User Interface/Xorg
License:    GPLv2+
Url:        http://xorg.freedesktop.org
Source:     http://xorg.freedesktop.org/releases/individual/driver/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  pixman util-macros xserver
BuildRequires:  xf86dgaproto
BuildRequires:  resourceproto scrnsaverproto

%description

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --sysconfdir=/etc \
            --mandir=/usr/share/man --localstatedir=/var \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_libdir}/xorg/modules/drivers/voodoo_drv.*
%{_mandir}/*/*

%clean
rm -rf %{buildroot}

%changelog
* Wed Oct 17 2012 tanggeliang <tanggeliang@gmail.com>
- update from 1.2.4 to 1.2.5
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
