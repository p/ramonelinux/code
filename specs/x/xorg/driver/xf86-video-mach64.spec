Name:       xf86-video-mach64
Version:    6.9.4
Release:    3%{?dist}
Summary:    Xorg Mach64 Driver

Group:      User Interface/Xorg
License:    GPLv2+
Url:        http://xorg.freedesktop.org
Source:     http://xorg.freedesktop.org/releases/individual/driver/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  pixman libdrm mesa xserver
BuildRequires:  xf86driproto glproto
BuildRequires:  resourceproto scrnsaverproto

%description
The Xorg Mach64 Driver package contains the X.Org Video Driver for ATI video adapters based on the Mach64 chipsets.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --sysconfdir=/etc \
            --mandir=/usr/share/man --localstatedir=/var \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_libdir}/xorg/modules/drivers/mach64_drv.*

%clean
rm -rf %{buildroot}

%changelog
* Sat Feb 23 2013 tanggeliang <tanggeliang@gmail.com>
- update from 6.9.3 to 6.9.4
* Mon Oct 15 2012 tanggeliang <tanggeliang@gmail.com>
- update from 6.9.1 to 6.9.3
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
