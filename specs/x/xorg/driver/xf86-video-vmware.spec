Name:       xf86-video-vmware
Version:    13.1.0
Release:    1%{?dist}
Summary:    Xorg VMware Driver

Group:      User Interface/Xorg
License:    GPLv2+
Url:        http://xorg.freedesktop.org
Source:     http://xorg.freedesktop.org/releases/individual/driver/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  util-macros xserver
BuildRequires:  resourceproto scrnsaverproto
BuildRequires:  autoconf automake m4 libtool

%description
The Xorg VMware Driver package contains the X.Org Video Driver for VMware SVGA virtual video cards.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --sysconfdir=/etc \
            --mandir=/usr/share/man --localstatedir=/var \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_libdir}/xorg/modules/drivers/vmware_drv.*

%files doc
%defattr(-,root,root,-)
%{_mandir}/*/*

%clean
rm -rf %{buildroot}

%changelog
* Sat Jun 27 2015 tanggeliang <tanggeliang@gmail.com>
- update from 13.0.2 to 13.1.0
* Wed Jul 23 2014 tanggeliang <tanggeliang@gmail.com>
- update from 13.0.1 to 13.0.2
* Thu Oct 3 2013 tanggeliang <tanggeliang@gmail.com>
- update from 13.0.0 to 13.0.1
* Sat Feb 23 2013 tanggeliang <tanggeliang@gmail.com>
- update from 12.0.2.20120718gite5ac80d8f to 13.0.0
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
