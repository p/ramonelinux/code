Name:       xf86-video-tdfx
Version:    1.4.5
Release:    5%{?dist}
Summary:    Xorg 3Dfx Driver

Group:      User Interface/Xorg
License:    GPLv2+
Url:        http://xorg.freedesktop.org
Source:     http://xorg.freedesktop.org/releases/individual/driver/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  pixman libdrm mesa xserver
BuildRequires:  xf86driproto glproto
BuildRequires:  resourceproto scrnsaverproto

%description
The Xorg 3Dfx Driver package contains the X.Org Video Driver for 3Dfx video cards including Voodoo Banshee, Voodoo3, Voodoo4 and Voodoo5 chipsets.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
sed -i -e "/mibstore.h/d" -e "/miInitializeBackingStore/d" src/tdfx_driver.c &&
./configure --prefix=/usr --sysconfdir=/etc \
            --mandir=/usr/share/man --localstatedir=/var \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_libdir}/xorg/modules/drivers/tdfx_drv.*

%files doc
%defattr(-,root,root,-)
%{_mandir}/*/*

%clean
rm -rf %{buildroot}

%changelog
* Mon Oct 15 2012 tanggeliang <tanggeliang@gmail.com>
- update from 1.4.4 to 1.4.5
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
