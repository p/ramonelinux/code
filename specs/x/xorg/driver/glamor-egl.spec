Name:       glamor-egl
Version:    0.6.0
Release:    1%{?dist}
Summary:    Glamor EGL

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.freedesktop.org
Source:     http://www.freedesktop.org/wiki/Software/Glamor/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  xserver util-macros
BuildRequires:  resourceproto scrnsaverproto
BuildRequires:  libtool autoconf automake m4

%description
The Glamor EGL package contains a GL-based rendering acceleration library for X server.
It is only useful if you are using newer AMD Radeon cards with Xorg ATI Driver.

%prep
%setup -q -n %{name}-%{version}

%build
autoreconf -fi &&
./configure --prefix=/usr --sysconfdir=/etc \
            --mandir=/usr/share/man --localstatedir=/var \
            --enable-glx-tls \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_includedir}/xorg/glamor.h
%{_libdir}/libglamor.la
%{_libdir}/libglamor.so*
%{_libdir}/pkgconfig/glamor-egl.pc
%{_libdir}/pkgconfig/glamor.pc
%{_libdir}/xorg/modules/libglamoregl.la
%{_libdir}/xorg/modules/libglamoregl.so
%{_datadir}/X11/xorg.conf.d/glamor.conf

%clean
rm -rf %{buildroot}

%changelog
* Fri Mar 14 2014 tanggeliang <tanggeliang@gmail.com>
- update from 0.5.0 to 0.6.0
* Fri Oct 4 2013 tanggeliang <tanggeliang@gmail.com>
- create
