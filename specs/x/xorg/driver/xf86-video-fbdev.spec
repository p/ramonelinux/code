Name:       xf86-video-fbdev
Version:    0.4.4
Release:    3%{?dist}
Summary:    Xorg Fbdev Driver

Group:      User Interface/Xorg
License:    GPLv2+
Url:        http://xorg.freedesktop.org
Source:     http://xorg.freedesktop.org/releases/individual/driver/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  util-macros xserver
BuildRequires:  resourceproto scrnsaverproto

%description
The Xorg Fbdev Driver package contains the X.Org Video Driver for framebuffer devices.
This driver is often used as fallback driver if the hardware specific and VESA drivers fail to load or are not present.
If this driver is not installed, Xorg Server will print a warning on startup, but it can be safely ignored if hardware specific driver works well.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --sysconfdir=/etc \
            --mandir=/usr/share/man --localstatedir=/var \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_libdir}/xorg/modules/drivers/fbdev_drv.*

%files doc
%defattr(-,root,root,-)
%{_mandir}/*/*

%clean
rm -rf %{buildroot}

%changelog
* Thu Oct 3 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.4.3 to 0.4.4
* Mon Oct 15 2012 tanggeliang <tanggeliang@gmail.com>
- update from 0.4.2 to 0.4.3
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
