Name:       xf86-input-joystick
Version:    1.6.2
Release:    3%{?dist}
Summary:    Joystick input driver

Group:      User Interface/Xorg
License:    GPLv2+
Url:        http://xorg.freedesktop.org
Source:     http://xorg.freedesktop.org/releases/individual/driver/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  pixman xserver
BuildRequires:  resourceproto scrnsaverproto

%description
joystick is an Xorg input driver for Joysticks. There are 3 backends available that are used in the following order, if support was found:
- Linux’s evdev interface 
- Linux’s joystick interface 
- BSD’s usbhid interface
The driver reports cursor movement as well as raw axis values through valuators.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --sysconfdir=/etc \
            --mandir=/usr/share/man --localstatedir=/var \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_includedir}/xorg/joystick-properties.h
%{_libdir}/pkgconfig/xorg-joystick.pc
%{_libdir}/xorg/modules/input/joystick_drv.*

%files doc
%defattr(-,root,root,-)
%{_mandir}/*/*

%clean
rm -rf %{buildroot}

%changelog
* Sun Mar 17 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.6.1 to 1.6.2
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
