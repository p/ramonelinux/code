Name:       xserver
Version:    1.17.2
Release:    1%{?dist}
Summary:    Xorg Server

Group:      User Interface/Xorg
License:    GPLv2+
Url:        http://xorg.freedesktop.org
Source:     http://xorg.freedesktop.org/releases/individual/xserver/xorg-server-%{version}.tar.bz2
Patch:	    xorg-server-%{version}-add_prime_support-1.patch

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  mesa nettle libgcrypt openssl libpciaccess pixman xkeyboard-config
BuildRequires:  encodings font-alias font-util
BuildRequires:  xf86driproto fixesproto damageproto xcmiscproto bigreqsproto xproto randrproto renderproto xextproto inputproto kbproto fontsproto videoproto compositeproto recordproto scrnsaverproto resourceproto xineramaproto glproto dri2proto presentproto
BuildRequires:  libx11 xtrans libxkbfile libxfont libdmx libxkbfile libxres libxtst libxdmcp libxinerama libxi libxt libxmu libxrender libxpm libxaw libxv
BuildRequires:  systemd-udev libxslt xcb-util-keysyms
BuildRequires:  fontconfig bison flex util-macros libdrm
BuildRequires:  xf86dgaproto libxxf86dga
BuildRequires:	systemd dbus

%description
The Xorg Server is the core of the X Window system.

%package        wayland
Summary:        wayland
BuildRequires:  wayland libepoxy
%description    wayland

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n xorg-server-%{version}
%patch -p1

%build
./configure --prefix=/usr --sysconfdir=/etc  \
            --mandir=/usr/share/man --localstatedir=/var \
	    --disable-glamor       	     \
            --enable-suid-wrapper            \
	    --enable-systemd-logind	     \
            --with-xkb-output=/var/lib/xkb   \
            --enable-dmx                     \
            --enable-xwayland                \
            --libexecdir=%{_libdir}/%{name}  \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

mkdir -pv %{buildroot}/etc/X11/xorg.conf.d

%files
%{_var}/*
%defattr(-,root,root,-)
%dir /etc/X11/xorg.conf.d
%{_bindir}/cvt
%{_bindir}/dmxaddinput
%{_bindir}/dmxaddscreen
%{_bindir}/dmxinfo
%{_bindir}/dmxreconfig
%{_bindir}/dmxresize
%{_bindir}/dmxrminput
%{_bindir}/dmxrmscreen
%{_bindir}/dmxtodmx
%{_bindir}/dmxwininfo
%{_bindir}/gtf
%{_bindir}/vdltodmx
%{_bindir}/X
%{_bindir}/Xdmx
%{_bindir}/xdmxconfig
%{_bindir}/Xnest
%attr(4755,root,root) %{_bindir}/Xorg
%{_bindir}/Xvfb
%{_includedir}/xorg/*
%{_libdir}/pkgconfig/xorg-server.pc
%{_libdir}/xorg/modules/extensions/libglx.*
%{_libdir}/xorg/modules/libexa.*
%{_libdir}/xorg/modules/libfb.*
%{_libdir}/xorg/modules/libfbdevhw.*
%{_libdir}/xorg/modules/libint10.*
%{_libdir}/xorg/modules/libshadow.*
%{_libdir}/xorg/modules/libshadowfb.*
%{_libdir}/xorg/modules/libvbe.*
%{_libdir}/xorg/modules/libvgahw.*
%{_libdir}/xorg/modules/libwfb.*
%{_libdir}/xorg/modules/drivers/modesetting_drv.*
%{_libdir}/xorg/protocol.txt
%{_libdir}/%{name}/Xorg
%{_libdir}/%{name}/Xorg.wrap
%{_datadir}/aclocal/*
%{_datadir}/X11/xorg.conf.d/10-evdev.conf
%{_datadir}/X11/xorg.conf.d/10-quirks.conf

%files wayland
%defattr(-,root,root,-)
%{_bindir}/Xwayland

%files doc
%defattr(-,root,root,-)
%{_mandir}/*/*

%post
if [ $1 -eq 0 ] ; then
cat >> /etc/sysconfig/createfiles << "EOF"
/tmp/.ICE-unix dir 1777 root root
/tmp/.X11-unix dir 1777 root root
EOF
fi

%clean
rm -rf %{buildroot}

%changelog
* Sat Jun 27 2015 tanggeliang <tanggeliang@gmail.com>
- update from 1.16.0 to 1.17.2
* Wed Jul 23 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.15.0 to 1.16.0
* Thu Mar 13 2014 tanggeliang <tanggeliang@mail.com>
- update from 1.14.4 to 1.15.0
* Sun Dec 1 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.14.3 to 1.14.4
* Wed Oct 2 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.14.2 to 1.14.3
- remove add_prime_support-1.patch
* Mon Jul 29 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.14.1 to 1.14.2
* Wed Jun 5 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.14.0 to 1.14.1
* Fri Mar 15 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.13.2 to 1.14.0
* Sat Feb 23 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.13.0 to 1.13.2
* Mon Nov 5 2012 tanggeliang <tanggeliang@gmail.com>
- update from 1.12.4 to 1.13.0
* Mon Oct 15 2012 tanggeliang <tanggeliang@gmail.com>
- update from 1.12.2 to 1.12.4
- remove "--enable-install-setuid" fix "chown: changing ownership of 'Xorg': Operation not permitted".
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
