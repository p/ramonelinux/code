%define _fontdir %{_datadir}/fonts/X11

Name:       font-misc-ethiopic
Version:    1.0.3
Release:    5%{?dist}
Summary:    font-misc-ethiopic

Group:      User Interface/Xorg
License:    GPLv2+
Url:        http://xorg.freedesktop.org
Source:     http://xorg.freedesktop.org/releases/individual/font/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildArch:      noarch
BuildRequires:  xcursor-themes fontconfig
BuildRequires:  mkfontdir bdftopcf mkfontscale
BuildRequires:  font-util encodings util-macros
Requires:       mkfontdir mkfontscale fontconfig

%description

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --sysconfdir=/etc \
            --mandir=/usr/share/man --localstatedir=/var &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

rm -rf %{buildroot}/%{_fontdir}/OTF/fonts.dir
rm -rf %{buildroot}/%{_fontdir}/OTF/fonts.scale

rm -rf %{buildroot}/%{_fontdir}/TTF/fonts.dir
rm -rf %{buildroot}/%{_fontdir}/TTF/fonts.scale

%files
%defattr(-,root,root,-)
%{_fontdir}/OTF/*.otf
%{_fontdir}/TTF/*.ttf

%post
mkfontdir %{_fontdir}/OTF
mkfontscale %{_fontdir}/OTF
fc-cache %{_fontdir}/OTF

mkfontdir %{_fontdir}/TTF
mkfontscale %{_fontdir}/TTF
fc-cache %{_fontdir}/TTF

%postun
if [ "$1" = "0" -a -d %{_fontdir}/OTF ]; then
  mkfontdir %{_fontdir}/OTF
  mkfontscale %{_fontdir}/OTF
  fc-cache %{_fontdir}/OTF
fi

if [ "$1" = "0" -a -d %{_fontdir}/TTF ]; then
  mkfontdir %{_fontdir}/TTF
  mkfontscale %{_fontdir}/TTF
  fc-cache %{_fontdir}/TTF
fi

%clean
rm -rf %{buildroot}

%changelog
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
