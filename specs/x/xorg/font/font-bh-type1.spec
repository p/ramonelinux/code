%define _fontdir %{_datadir}/fonts/X11/Type1

Name:       font-bh-type1
Version:    1.0.3
Release:    5%{?dist}
Summary:    font-bh-type1

Group:      User Interface/Xorg
License:    GPLv2+
Url:        http://xorg.freedesktop.org
Source:     http://xorg.freedesktop.org/releases/individual/font/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildArch:      noarch
BuildRequires:  xcursor-themes fontconfig
BuildRequires:  mkfontdir bdftopcf mkfontscale
BuildRequires:  font-util encodings util-macros
Requires:       mkfontdir mkfontscale fontconfig

%description

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --sysconfdir=/etc \
            --mandir=/usr/share/man --localstatedir=/var &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

rm -rf %{buildroot}/%{_fontdir}/fonts.dir
rm -rf %{buildroot}/%{_fontdir}/fonts.scale

%files
%defattr(-,root,root,-)
%{_fontdir}/*.afm
%{_fontdir}/*.pfa

%post
mkfontdir %{_fontdir}
mkfontscale %{_fontdir}
fc-cache %{_fontdir}

%postun
if [ "$1" = "0" -a -d %{_fontdir} ]; then
  mkfontdir %{_fontdir}
  mkfontscale %{_fontdir}
  fc-cache %{_fontdir}
fi

%clean
rm -rf %{buildroot}

%changelog
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
