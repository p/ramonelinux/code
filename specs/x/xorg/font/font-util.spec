Name:       font-util
Version:    1.3.1
Release:    1%{?dist}
Summary:    font-util

Group:      User Interface/Xorg
License:    GPLv2+
Url:        http://xorg.freedesktop.org
Source:     http://xorg.freedesktop.org/releases/individual/font/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  util-macros

%description

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --sysconfdir=/etc \
            --mandir=/usr/share/man --localstatedir=/var \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/bdftruncate
%{_bindir}/ucs2any
%{_libdir}/pkgconfig/fontutil.pc
%{_datadir}/aclocal/*
%{_datadir}/fonts/*

%files doc
%defattr(-,root,root,-)
%{_mandir}/*/*

%clean
rm -rf %{buildroot}

%changelog
* Mon Sep 14 2015 tanggeliang <tanggeliang@gmail.com>
- update from 1.3.0 to 1.3.1
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
