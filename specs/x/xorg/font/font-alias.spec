%define _fontdir %{_datadir}/fonts/X11

Name:       font-alias
Version:    1.0.3
Release:    6%{?dist}
Summary:    font-alias

Group:      User Interface/Xorg
License:    GPLv2+
Url:        http://xorg.freedesktop.org
Source:     http://xorg.freedesktop.org/releases/individual/font/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildArch:      noarch
BuildRequires:  util-macros
Requires:       font-adobe-100dpi font-adobe-75dpi font-adobe-utopia-100dpi font-adobe-utopia-75dpi font-adobe-utopia-type1 font-arabic-misc
Requires:       font-bh-100dpi font-bh-75dpi font-bh-lucidatypewriter-100dpi font-bh-lucidatypewriter-75dpi font-bh-ttf font-bh-type1
Requires:       font-bitstream-100dpi font-bitstream-75dpi font-bitstream-type1
Requires:       font-cronyx-cyrillic font-cursor-misc font-daewoo-misc font-dec-misc font-ibm-type1 font-isas-misc font-jis-misc
Requires:       font-micro-misc font-misc-cyrillic font-misc-ethiopic font-misc-meltho font-misc-misc font-mutt-misc
Requires:       font-schumacher-misc font-screen-cyrillic font-sony-misc font-sun-misc
Requires:       font-winitzki-cyrillic font-xfree86-type1

%description

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --sysconfdir=/etc \
            --mandir=/usr/share/man --localstatedir=/var &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

%files
%defattr(-,root,root,-)
%{_fontdir}/100dpi/fonts.alias
%{_fontdir}/75dpi/fonts.alias
%{_fontdir}/cyrillic/fonts.alias
%{_fontdir}/misc/fonts.alias

%clean
rm -rf %{buildroot}

%changelog
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
