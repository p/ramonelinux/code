Name:       xtrans
Version:    1.3.5
Release:    1%{?dist}
Summary:    xtrans

Group:      User Interface/Xorg
License:    GPLv2+
Url:        http://xorg.freedesktop.org
Source:     http://xorg.freedesktop.org/releases/individual/lib/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  util-macros

%description

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --sysconfdir=/etc \
            --mandir=/usr/share/man --localstatedir=/var &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_includedir}/*
%{_datadir}/aclocal/*
%{_docdir}/*/*
%{_datadir}/pkgconfig/xtrans.pc

%clean
rm -rf %{buildroot}

%changelog
* Sat Jun 27 2015 tanggeliang <tanggeliang@gmail.com>
- update from 1.3.4 to 1.3.5
* Mon Jul 21 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.3.3 to 1.3.4
* Thu Mar 20 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.3.2 to 1.3.3
* Wed Dec 4 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.2.7 to 1.3.2
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
