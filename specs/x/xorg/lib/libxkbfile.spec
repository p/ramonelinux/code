Name:       libxkbfile
Version:    1.0.9
Release:    1%{?dist}
Summary:    libxkbfile

Group:      User Interface/Xorg
License:    GPLv2+
Url:        http://xorg.freedesktop.org
Source:     http://xorg.freedesktop.org/releases/individual/lib/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  libx11 libxau
BuildRequires:  util-macros xproto

%description

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --sysconfdir=/etc \
            --mandir=/usr/share/man --localstatedir=/var \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_includedir}/*
%{_libdir}/libxkbfile.*
%{_libdir}/pkgconfig/xkbfile.pc

%clean
rm -rf %{buildroot}

%changelog
* Mon Sep 14 2015 tanggeliang <tanggeliang@gmail.com>
- update from 1.0.8 to 1.0.9
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
