Name:       libxi
Version:    1.7.4
Release:    1%{?dist}
Summary:    libXi

Group:      User Interface/Xorg
License:    GPLv2+
Url:        http://xorg.freedesktop.org
Source:     http://xorg.freedesktop.org/releases/individual/lib/libXi-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  libxau libxdmcp libx11 libxext libpthread-stubs libxfixes
BuildRequires:  util-macros inputproto xextproto xproto kbproto libxslt

%description

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n libXi-%{version}

%build
./configure --prefix=/usr --sysconfdir=/etc \
            --mandir=/usr/share/man --localstatedir=/var \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_includedir}/*
%{_libdir}/libXi.*
%{_libdir}/pkgconfig/xi.pc

%files doc
%defattr(-,root,root,-)
%{_docdir}/*/*
%{_mandir}/*/*

%clean
rm -rf %{buildroot}

%changelog
* Mon Jul 21 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.7.2 to 1.7.4
* Thu Oct 3 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.6.2 to 1.7.2
* Fri Feb 22 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.6.1 to 1.6.2
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
