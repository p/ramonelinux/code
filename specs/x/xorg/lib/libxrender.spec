Name:       libxrender
Version:    0.9.9
Release:    1%{?dist}
Summary:    libXrender

Group:      User Interface/Xorg
License:    GPLv2+
Url:        http://xorg.freedesktop.org
Source:     http://xorg.freedesktop.org/releases/individual/lib/libXrender-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  libx11
BuildRequires:  util-macros renderproto xproto

%description

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n libXrender-%{version}

%build
./configure --prefix=/usr --sysconfdir=/etc \
            --mandir=/usr/share/man --localstatedir=/var \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_includedir}/X11/extensions/Xrender.h
%{_libdir}/libXrender.*a
%{_libdir}/libXrender.so*
%{_libdir}/pkgconfig/xrender.pc

%files doc
%defattr(-,root,root,-)
%{_docdir}/libXrender/libXrender.txt

%clean
rm -rf %{buildroot}

%changelog
* Mon Sep 14 2015 tanggeliang <tanggeliang@gmail.com>
- update from 0.9.8 to 0.9.9
* Thu Oct 3 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.9.7 to 0.9.8
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
