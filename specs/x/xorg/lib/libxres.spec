Name:       libxres
Version:    1.0.7
Release:    1%{?dist}
Summary:    libXres

Group:      User Interface/Xorg
License:    GPLv2+
Url:        http://xorg.freedesktop.org
Source:     http://xorg.freedesktop.org/releases/individual/lib/libXres-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  libx11 libxext
BuildRequires:  util-macros xextproto resourceproto xproto

%description

%prep
%setup -q -n libXres-%{version}

%build
./configure --prefix=/usr --sysconfdir=/etc \
            --mandir=/usr/share/man --localstatedir=/var \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_includedir}/*
%{_libdir}/libXRes.*
%{_libdir}/pkgconfig/xres.pc
%{_mandir}/*/*

%clean
rm -rf %{buildroot}

%changelog
* Thu Oct 3 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.0.6 to 1.0.7
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
