Name:       libxaw
Version:    1.0.13
Release:    1%{?dist}
Summary:    libXaw

Group:      User Interface/Xorg
License:    GPLv2+
Url:        http://xorg.freedesktop.org
Source:     http://xorg.freedesktop.org/releases/individual/lib/libXaw-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  libice libsm libx11 libxau libxdmcp libxext libxmu libxpm libxt xtrans
BuildRequires:  util-macros xproto libxslt

%description

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n libXaw-%{version}

%build
./configure --prefix=/usr --sysconfdir=/etc \
            --mandir=/usr/share/man --localstatedir=/var \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_includedir}/*
%{_libdir}/libXaw*.*
%{_libdir}/pkgconfig/xaw*.pc

%files doc
%defattr(-,root,root,-)
%{_docdir}/*/*
%{_mandir}/*/*

%clean
rm -rf %{buildroot}

%changelog
* Mon Sep 14 2015 tanggeliang <tanggeliang@gmail.com>
- update from 1.0.12 to 1.0.13
* Thu Oct 3 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.0.11 to 1.0.12
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
