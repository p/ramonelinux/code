Name:       libfs
Version:    1.0.7
Release:    1%{?dist}
Summary:    libFS

Group:      User Interface/Xorg
License:    GPLv2+
Url:        http://xorg.freedesktop.org
Source:     http://xorg.freedesktop.org/releases/individual/lib/libFS-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  xtrans
BuildRequires:  util-macros fontsproto xproto

%description

%prep
%setup -q -n libFS-%{version}

%build
./configure --prefix=/usr --sysconfdir=/etc \
            --mandir=/usr/share/man --localstatedir=/var \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_includedir}/*
%{_libdir}/libFS.*
%{_libdir}/pkgconfig/libfs.pc
%{_docdir}/*/*

%clean
rm -rf %{buildroot}

%changelog
* Mon Sep 14 2015 tanggeliang <tanggeliang@gmail.com>
- update from 1.0.6 to 1.0.7
* Thu Mar 20 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.0.5 to 1.0.6
* Thu Oct 3 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.0.4 to 1.0.5
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
