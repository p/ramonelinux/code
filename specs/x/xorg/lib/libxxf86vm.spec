Name:       libxxf86vm
Version:    1.1.4
Release:    1%{?dist}
Summary:    libXxf86vm

Group:      User Interface/Xorg
License:    GPLv2+
Url:        http://xorg.freedesktop.org
Source:     http://xorg.freedesktop.org/releases/individual/lib/libXxf86vm-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  libxau, libxdmcp, libx11, libxext
BuildRequires:  util-macros xextproto xf86vidmodeproto xproto

%description

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n libXxf86vm-%{version}

%build
./configure --prefix=/usr --sysconfdir=/etc \
            --mandir=/usr/share/man --localstatedir=/var \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_includedir}/*
%{_libdir}/libXxf86vm.*
%{_libdir}/pkgconfig/xxf86vm.pc

%files doc
%defattr(-,root,root,-)
%{_mandir}/*/*

%clean
rm -rf %{buildroot}

%changelog
* Mon Sep 14 2015 tanggeliang <tanggeliang@gmail.com>
- update from 1.1.3 to 1.1.4
* Thu Oct 3 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.1.2 to 1.1.3
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
