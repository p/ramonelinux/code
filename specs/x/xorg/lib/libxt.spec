Name:       libxt
Version:    1.1.5
Release:    1%{?dist}
Summary:    libXt

Group:      User Interface/Xorg
License:    GPLv2+
Url:        http://xorg.freedesktop.org
Source:     http://xorg.freedesktop.org/releases/individual/lib/libXt-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  libsm libice libx11 libxau
BuildRequires:  util-macros xproto libxslt

%description

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n libXt-%{version}

%build
./configure --prefix=/usr --sysconfdir=/etc \
            --mandir=/usr/share/man --localstatedir=/var \
            --with-appdefaultdir=/etc/X11/app-defaults \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_includedir}/*
%{_libdir}/libXt.*
%{_libdir}/pkgconfig/xt.pc

%files doc
%defattr(-,root,root,-)
%{_docdir}/libXt/*.xml
%{_mandir}/*/*

%clean
rm -rf %{buildroot}

%changelog
* Mon Sep 14 2015 tanggeliang <tanggeliang@gmail.com>
- update from 1.1.4 to 1.1.5
* Thu Oct 3 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.1.3 to 1.1.4
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
