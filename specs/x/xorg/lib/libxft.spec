Name:       libxft
Version:    2.3.2
Release:    1%{?dist}
Summary:    libXft

Group:      User Interface/Xorg
License:    GPLv2+
Url:        http://xorg.freedesktop.org
Source:     http://xorg.freedesktop.org/releases/individual/lib/libXft-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  fontconfig, libx11, libxrender
BuildRequires:  util-macros xproto

%description

%prep
%setup -q -n libXft-%{version}

%build
./configure --prefix=/usr --sysconfdir=/etc \
            --mandir=/usr/share/man --localstatedir=/var \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_includedir}/*
%{_libdir}/libXft.*
%{_libdir}/pkgconfig/xft.pc
%{_mandir}/*/*

%clean
rm -rf %{buildroot}

%changelog
* Mon Jul 21 2014 tanggeliang <tanggeliang@gmail.com>
- update from 2.3.1 to 2.3.2
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
