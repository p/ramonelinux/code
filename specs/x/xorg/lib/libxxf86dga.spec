Name:       libxxf86dga
Version:    1.1.4
Release:    1%{?dist}
Summary:    libXxf86dga

Group:      User Interface/Xorg
License:    GPLv2+
Url:        http://xorg.freedesktop.org
Source:     http://xorg.freedesktop.org/releases/individual/lib/libXxf86dga-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  libx11 libxext
BuildRequires:  util-macros xf86dgaproto xproto

%description

%prep
%setup -q -n libXxf86dga-%{version}

%build
./configure --prefix=/usr --sysconfdir=/etc \
            --mandir=/usr/share/man --localstatedir=/var \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_includedir}/*
%{_libdir}/libXxf86dga.*
%{_libdir}/pkgconfig/xxf86dga.pc
%{_mandir}/*/*

%clean
rm -rf %{buildroot}

%changelog
* Thu Oct 3 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.1.3 to 1.1.4
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
