Name:       libxdmcp
Version:    1.1.2
Release:    1%{?dist}
Summary:    libXdmcp

Group:      User Interface/Xorg
License:    GPLv2+
Url:        http://xorg.freedesktop.org
Source:     http://xorg.freedesktop.org/releases/individual/lib/libXdmcp-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  xproto libxslt util-macros

%description
The libXdmcp package contains a library implementing the X Display Manager Control Protocol.
This is useful for allowing clients to interact with the X Display Manager.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n libXdmcp-%{version}

%build
./configure --prefix=/usr --sysconfdir=/etc \
            --mandir=/usr/share/man --localstatedir=/var \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_includedir}/X11/Xdmcp.h
%{_libdir}/libXdmcp.*
%{_libdir}/pkgconfig/xdmcp.pc

%files doc
%defattr(-,root,root,-)
%{_docdir}/libXdmcp/xdmcp.xml

%clean
rm -rf %{buildroot}

%changelog
* Mon Sep 14 2015 tanggeliang <tanggeliang@gmail.com>
- update from 1.1.1 to 1.1.2
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
