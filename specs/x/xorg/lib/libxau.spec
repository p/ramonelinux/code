Name:       libxau
Version:    1.0.8
Release:    1%{?dist}
Summary:    libXau

Group:      User Interface/Xorg
License:    GPLv2+
Url:        http://xorg.freedesktop.org
Source:     http://xorg.freedesktop.org/releases/individual/lib/libXau-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  util-macros xproto

%description
The libXau package contains a library implementing the X11 Authorization Protocol. This is useful for restricting client access to the display.

%prep
%setup -q -n libXau-%{version}

%build
./configure --prefix=/usr --sysconfdir=/etc \
            --mandir=/usr/share/man --localstatedir=/var \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_includedir}/X11/Xauth.h
%{_libdir}/libXau.*
%{_libdir}/pkgconfig/xau.pc
%{_mandir}/man3/Xau*.3.gz

%clean
rm -rf %{buildroot}

%changelog
* Mon Jul 29 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.0.7 to 1.0.8
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
