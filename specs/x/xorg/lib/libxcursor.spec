Name:       libxcursor
Version:    1.1.14
Release:    1%{?dist}
Summary:    libXcursor

Group:      User Interface/Xorg
License:    GPLv2+
Url:        http://xorg.freedesktop.org
Source:     http://xorg.freedesktop.org/releases/individual/lib/libXcursor-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  libxau, libxdmcp, libx11, libxfixes, libxrender
BuildRequires:  util-macros xextproto xproto

%description

%prep
%setup -q -n libXcursor-%{version}

%build
./configure --prefix=/usr --sysconfdir=/etc \
            --mandir=/usr/share/man --localstatedir=/var \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_includedir}/X11/Xcursor/Xcursor.h
%{_libdir}/libXcursor.*a
%{_libdir}/libXcursor.so*
%{_libdir}/pkgconfig/xcursor.pc
%{_mandir}/man3/Xcursor*.3.gz

%clean
rm -rf %{buildroot}

%changelog
* Thu Oct 3 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.1.13 to 1.1.14
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
