Name:       libxinerama
Version:    1.1.3
Release:    1%{?dist}
Summary:    libXinerama

Group:      User Interface/Xorg
License:    GPLv2+
Url:        http://xorg.freedesktop.org
Source:     http://xorg.freedesktop.org/releases/individual/lib/libXinerama-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  libx11, libxext
BuildRequires:  util-macros xextproto xineramaproto xproto

%description

%prep
%setup -q -n libXinerama-%{version}

%build
./configure --prefix=/usr --sysconfdir=/etc \
            --mandir=/usr/share/man --localstatedir=/var \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_includedir}/X11/extensions/Xinerama.h
%{_includedir}/X11/extensions/panoramiXext.h
%{_libdir}/libXinerama.*a
%{_libdir}/libXinerama.so*
%{_libdir}/pkgconfig/xinerama.pc
%{_mandir}/man3/Xinerama*.3.gz

%clean
rm -rf %{buildroot}

%changelog
* Thu Oct 3 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.1.2 to 1.1.3
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
