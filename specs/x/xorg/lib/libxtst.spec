Name:       libxtst
Version:    1.2.2
Release:    1%{?dist}
Summary:    libXtst

Group:      User Interface/Xorg
License:    GPLv2+
Url:        http://xorg.freedesktop.org
Source:     http://xorg.freedesktop.org/releases/individual/lib/libXtst-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  libx11 libxext libxi libxslt
BuildRequires:  util-macros recordproto xextproto inputproto xproto

%description

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n libXtst-%{version}

%build
./configure --prefix=/usr --sysconfdir=/etc \
            --mandir=/usr/share/man --localstatedir=/var \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_includedir}/X11/extensions/XTest.h
%{_includedir}/X11/extensions/record.h
%{_libdir}/libXtst.*a
%{_libdir}/libXtst.so*
%{_libdir}/pkgconfig/xtst.pc

%files doc
%defattr(-,root,root,-)
%{_docdir}/libXtst/recordlib.xml
%{_docdir}/libXtst/xtestlib.xml
%{_mandir}/man3/XTest*.3.gz

%clean
rm -rf %{buildroot}

%changelog
* Thu Oct 3 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.2.1 to 1.2.2
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
