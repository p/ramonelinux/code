Name:       libxext
Version:    1.3.3
Release:    1%{?dist}
Summary:    libXext

Group:      User Interface/Xorg
License:    GPLv2+
Url:        http://xorg.freedesktop.org
Source:     http://xorg.freedesktop.org/releases/individual/lib/libXext-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  libxau libx11
BuildRequires:  libxslt util-macros
BuildRequires:  xextproto xproto

%description

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n libXext-%{version}

%build
./configure --prefix=/usr --sysconfdir=/etc \
            --mandir=/usr/share/man --localstatedir=/var \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_includedir}/X11/extensions/*.h
%{_libdir}/libXext.a
%{_libdir}/libXext.la
%{_libdir}/libXext.so*
%{_libdir}/pkgconfig/xext.pc

%files doc
%defattr(-,root,root,-)
%{_docdir}/libXext/*.xml
%{_mandir}/*/*

%clean
rm -rf %{buildroot}

%changelog
* Mon Sep 14 2015 tanggeliang <tanggeliang@gmail.com>
- update from 1.3.2 to 1.3.3
* Thu Oct 3 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.3.1 to 1.3.2
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
