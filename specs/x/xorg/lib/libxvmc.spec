Name:       libxvmc
Version:    1.0.9
Release:    1%{?dist}
Summary:    libXvMC

Group:      User Interface/Xorg
License:    GPLv2+
Url:        http://xorg.freedesktop.org
Source:     http://xorg.freedesktop.org/releases/individual/lib/libXvMC-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  libx11 libxext libxv
BuildRequires:  util-macros xproto

%description

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n libXvMC-%{version}

%build
./configure --prefix=/usr --sysconfdir=/etc \
            --mandir=/usr/share/man --localstatedir=/var \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_includedir}/*
%{_libdir}/libXvMC.*
%{_libdir}/libXvMCW.*
%{_libdir}/pkgconfig/xvmc.pc

%files doc
%defattr(-,root,root,-)
%{_docdir}/*/*

%clean
rm -rf %{buildroot}

%changelog
* Mon Sep 14 2015 tanggeliang <tanggeliang@gmail.com>
- update from 1.0.8 to 1.0.9
* Thu Oct 3 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.0.7 to 1.0.8
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
