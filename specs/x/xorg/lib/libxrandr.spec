Name:       libxrandr
Version:    1.5.0
Release:    1%{?dist}
Summary:    libXrandr

Group:      User Interface/Xorg
License:    GPLv2+
Url:        http://xorg.freedesktop.org
Source:     http://xorg.freedesktop.org/releases/individual/lib/libXrandr-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  libx11, libxau, libxext, libxrender
BuildRequires:  util-macros randrproto xextproto renderproto xproto

%description

%prep
%setup -q -n libXrandr-%{version}

%build
./configure --prefix=/usr --sysconfdir=/etc \
            --mandir=/usr/share/man --localstatedir=/var \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_includedir}/X11/extensions/Xrandr.h
%{_libdir}/libXrandr.*a
%{_libdir}/libXrandr.so*
%{_libdir}/pkgconfig/xrandr.pc
%{_mandir}/man3/*.3.gz

%clean
rm -rf %{buildroot}

%changelog
* Mon Sep 14 2015 tanggeliang <tanggeliang@gmail.com>
- update from 1.4.2 to 1.5.0
* Thu Oct 3 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.4.0 to 1.4.2
* Fri Feb 22 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.3.2 to 1.4.0
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
