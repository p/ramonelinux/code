Name:       libxcomposite
Version:    0.4.4
Release:    1%{?dist}
Summary:    libXcomposite

Group:      User Interface/Xorg
License:    GPLv2+
Url:        http://xorg.freedesktop.org
Source:     http://xorg.freedesktop.org/releases/individual/lib/libXcomposite-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  libx11, libxfixes, libxext
BuildRequires:  util-macros compositeproto xproto

%description

%prep
%setup -q -n libXcomposite-%{version}

%build
./configure --prefix=/usr --sysconfdir=/etc \
            --mandir=/usr/share/man --localstatedir=/var \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_includedir}/X11/extensions/Xcomposite.h
%{_libdir}/libXcomposite.*a
%{_libdir}/libXcomposite.so*
%{_libdir}/pkgconfig/xcomposite.pc
%{_mandir}/man3/XComposite*.3.gz
%{_mandir}/man3/Xcomposite*.3.gz

%clean
rm -rf %{buildroot}

%changelog
* Thu Oct 3 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.4.3 to 0.4.4
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
