Name:       libxp
Version:    1.0.2
Release:    1%{?dist}
Summary:    libXp

Group:      User Interface/Xorg
License:    GPLv2+
Url:        http://xorg.freedesktop.org
Source:     http://xorg.freedesktop.org/releases/individual/lib/libXp-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  libx11 libxau libxext
BuildRequires:  util-macros xextproto printproto xproto

%description
The libXp package contains a library implementing the X Print Protocol.

%prep
%setup -q -n libXp-%{version}

%build
./configure --prefix=/usr --sysconfdir=/etc \
            --mandir=/usr/share/man --localstatedir=/var \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_libdir}/libXp.*
%{_libdir}/pkgconfig/xp.pc
%{_mandir}/*/*

%clean
rm -rf %{buildroot}

%changelog
* Mon Jul 29 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.0.1 to 1.0.2
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
