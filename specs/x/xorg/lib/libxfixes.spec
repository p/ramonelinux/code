Name:       libxfixes
Version:    5.0.1
Release:    1%{?dist}
Summary:    libXfixes

Group:      User Interface/Xorg
License:    GPLv2+
Url:        http://xorg.freedesktop.org
Source:     http://xorg.freedesktop.org/releases/individual/lib/libXfixes-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  libx11
BuildRequires:  util-macros fixesproto xextproto xproto

%description

%prep
%setup -q -n libXfixes-%{version}

%build
./configure --prefix=/usr --sysconfdir=/etc \
            --mandir=/usr/share/man --localstatedir=/var \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_includedir}/X11/extensions/Xfixes.h
%{_libdir}/libXfixes.*a
%{_libdir}/libXfixes.so*
%{_libdir}/pkgconfig/xfixes.pc
%{_mandir}/man3/Xfixes.3.gz

%clean
rm -rf %{buildroot}

%changelog
* Thu Oct 3 2013 tanggeliang <tanggeliang@gmail.com>
- update from 5.0 to 5.0.1
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
