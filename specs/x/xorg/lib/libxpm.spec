Name:       libxpm
Version:    3.5.11
Release:    1%{?dist}
Summary:    libXpm

Group:      User Interface/Xorg
License:    GPLv2+
Url:        http://xorg.freedesktop.org
Source:     http://xorg.freedesktop.org/releases/individual/lib/libXpm-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  gettext
BuildRequires:  libx11
BuildRequires:  libxt libsm libxext
BuildRequires:  util-macros xproto

%description

%prep
%setup -q -n libXpm-%{version}

%build
./configure --prefix=/usr --sysconfdir=/etc \
            --mandir=/usr/share/man --localstatedir=/var \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/cxpm
%{_bindir}/sxpm
%{_includedir}/*
%{_libdir}/libXpm.*
%{_libdir}/pkgconfig/xpm.pc
%{_mandir}/*/*

%clean
rm -rf %{buildroot}

%changelog
* Thu Oct 3 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.5.10 to 3.5.11
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
