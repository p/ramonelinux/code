Name:       libxfont
Version:    1.5.1
Release:    1%{?dist}
Summary:    libXfont

Group:      User Interface/Xorg
License:    GPLv2+
Url:        http://xorg.freedesktop.org
Source:     http://xorg.freedesktop.org/releases/individual/lib/libXfont-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  freetype libfontenc xtrans
BuildRequires:  util-macros fontsproto xproto

%description

%prep
%setup -q -n libXfont-%{version}

%build
./configure --prefix=/usr --sysconfdir=/etc \
            --mandir=/usr/share/man --localstatedir=/var \
            --disable-devel-docs \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_includedir}/*
%{_libdir}/libXfont.*
%{_libdir}/pkgconfig/xfont.pc

%clean
rm -rf %{buildroot}

%changelog
* Mon Sep 14 2015 tanggeliang <tanggeliang@gmail.com>
- update from 1.5.0 to 1.5.1
* Mon Jul 21 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.4.7 to 1.5.0
* Thu Mar 20 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.4.6 to 1.4.7
* Thu Oct 3 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.4.5 to 1.4.6
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
