Name:       libx11
Version:    1.6.3
Release:    1%{?dist}
Summary:    libX11

Group:      User Interface/Xorg
License:    GPLv2+
Url:        http://xorg.freedesktop.org
Source:     http://xorg.freedesktop.org/releases/individual/lib/libX11-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  libxau xtrans libxcb libxslt
BuildRequires:  util-macros xextproto kbproto inputproto xproto
BuildRequires:  libpthread-stubs xf86bigfontproto

%description

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n libX11-%{version}

%build
./configure --prefix=/usr --sysconfdir=/etc \
            --mandir=/usr/share/man --localstatedir=/var \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_includedir}/X11/*.h
%{_libdir}/libX11.*a
%{_libdir}/libX11.so*
%{_libdir}/libX11-xcb.*a
%{_libdir}/libX11-xcb.so*
%{_libdir}/pkgconfig/x11.pc
%{_libdir}/pkgconfig/x11-xcb.pc
%{_datadir}/X11/Xcms.txt
%{_datadir}/X11/XErrorDB
%{_datadir}/X11/locale/*/Compose
%{_datadir}/X11/locale/*/XI18N_OBJS
%{_datadir}/X11/locale/*/XLC_LOCALE
%{_datadir}/X11/locale/compose.dir
%{_datadir}/X11/locale/locale.alias
%{_datadir}/X11/locale/locale.dir

%files doc
%defattr(-,root,root,-)
%{_docdir}/libX11/*
%{_mandir}/man3/*.3.gz
%{_mandir}/man5/*.5.gz

%clean
rm -rf %{buildroot}

%changelog
* Mon Sep 14 2015 tanggeliang <tanggeliang@gmail.com>
- update from 1.6.2 to 1.6.3
* Thu Oct 3 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.5.0 to 1.6.2
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
