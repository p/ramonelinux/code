Name:       libxdamage
Version:    1.1.4
Release:    1%{?dist}
Summary:    libXdamage

Group:      User Interface/Xorg
License:    GPLv2+
Url:        http://xorg.freedesktop.org
Source:     http://xorg.freedesktop.org/releases/individual/lib/libXdamage-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  libx11 libxfixes
BuildRequires:  util-macros damageproto fixesproto xextproto xproto

%description

%prep
%setup -q -n libXdamage-%{version}

%build
./configure --prefix=/usr --sysconfdir=/etc \
            --mandir=/usr/share/man --localstatedir=/var \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_includedir}/X11/extensions/Xdamage.h
%{_libdir}/libXdamage.*a
%{_libdir}/libXdamage.so*
%{_libdir}/pkgconfig/xdamage.pc

%clean
rm -rf %{buildroot}

%changelog
* Thu Oct 3 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.1.3 to 1.1.4
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
