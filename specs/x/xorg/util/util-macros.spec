Name:       util-macros
Version:    1.19.0
Release:    1%{?dist}
Summary:    util-macros

Group:      User Interface/Xorg
License:    GPLv2+
Url:        http://xorg.freedesktop.org
Source:     http://xorg.freedesktop.org/releases/individual/util/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildArch:      noarch

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build

%description
The util-macros package contains the m4 macros used by all of the Xorg packages.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --sysconfdir=/etc \
            --mandir=%{_datadir}/man --localstatedir=/var \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_datadir}/aclocal/xorg-macros.m4
%{_datadir}/pkgconfig/xorg-macros.pc
%{_datadir}/util-macros/INSTALL

%clean
rm -rf %{buildroot}

%changelog
* Mon Jul 21 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.18.0 to 1.19.0
* Thu Dec 5 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.17.1 to 1.18.0
* Thu Oct 3 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.17 to 1.17.1
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
