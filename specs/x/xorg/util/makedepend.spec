Name:       makedepend
Version:    1.0.5
Release:    1%{?dist}
Summary:    makedepend

Group:      User Interface/Xorg
License:    GPLv2+
Url:        http://xorg.freedesktop.org
Source:     http://xorg.freedesktop.org/releases/individual/util/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  util-macros xproto

%description
The makedepend package contains a C-preprocessor like utility to determine build-time dependencies.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --sysconfdir=/etc \
            --mandir=/usr/share/man --localstatedir=/var
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/makedepend

%files doc
%defattr(-,root,root,-)
%{_mandir}/man1/makedepend.1.gz

%clean
rm -rf %{buildroot}

%changelog
* Sat Aug 24 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.0.4 to 1.0.5
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
