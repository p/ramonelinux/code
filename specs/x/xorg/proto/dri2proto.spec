Name:       dri2proto
Version:    2.8
Release:    3%{?dist}
Summary:    dri2proto

Group:      User Interface/Xorg
License:    GPLv2+
Url:        http://xorg.freedesktop.org
Source:     http://xorg.freedesktop.org/releases/individual/proto/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  util-macros libxslt

%description

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --sysconfdir=/etc \
            --mandir=/usr/share/man --localstatedir=/var \
            --libdir=%{_libdir} &&

%check

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

%files
%defattr(-,root,root,-)
%{_includedir}/X11/extensions/dri2proto.h
%{_includedir}/X11/extensions/dri2tokens.h
%{_libdir}/pkgconfig/dri2proto.pc

%files doc
%defattr(-,root,root,-)
%{_docdir}/dri2proto/dri2proto.txt

%clean
rm -rf %{buildroot}

%changelog
* Sun Jul 14 2013 tanggeliang <tanggeliang@gmail.com>
- remove "BuildArch: noarch"
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
