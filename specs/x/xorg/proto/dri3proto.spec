Name:       dri3proto
Version:    1.0
Release:    1%{?dist}
Summary:    dri3proto

Group:      User Interface/Xorg
License:    GPLv2+
Url:        http://xorg.freedesktop.org
Source:     http://xorg.freedesktop.org/releases/individual/proto/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  util-macros libxslt

%description

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --sysconfdir=/etc \
            --mandir=/usr/share/man --localstatedir=/var \
            --libdir=%{_libdir} &&

%check

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

%files
%defattr(-,root,root,-)
%{_includedir}/X11/extensions/dri3proto.h
%{_libdir}/pkgconfig/dri3proto.pc

%files doc
%defattr(-,root,root,-)
%{_docdir}/dri3proto/dri3proto.txt

%clean
rm -rf %{buildroot}

%changelog
* Thu Dec 5 2013 tanggeliang <tanggeliang@gmail.com>
- create
