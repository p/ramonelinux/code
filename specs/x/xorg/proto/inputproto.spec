Name:       inputproto
Version:    2.3.1
Release:    1%{?dist}
Summary:    inputproto

Group:      User Interface/Xorg
License:    GPLv2+
Url:        http://xorg.freedesktop.org
Source:     http://xorg.freedesktop.org/releases/individual/proto/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  util-macros libxslt

%description

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --sysconfdir=/etc \
            --mandir=/usr/share/man --localstatedir=/var \
            --libdir=%{_libdir} &&

%check

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

%files
%defattr(-,root,root,-)
%{_includedir}/X11/extensions/XI.h
%{_includedir}/X11/extensions/XI2.h
%{_includedir}/X11/extensions/XI2proto.h
%{_includedir}/X11/extensions/XIproto.h
%{_libdir}/pkgconfig/inputproto.pc

%clean
rm -rf %{buildroot}

%changelog
* Mon Jul 21 2014 tanggeliang <tanggeliang@gmail.com>
- update from 2.3 to 2.3.1
* Fri Mar 15 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.2 to 2.3
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
