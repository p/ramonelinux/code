Name:       videoproto
Version:    2.3.2
Release:    1%{?dist}
Summary:    videoproto

Group:      User Interface/Xorg
License:    GPLv2+
Url:        http://xorg.freedesktop.org
Source:     http://xorg.freedesktop.org/releases/individual/proto/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  util-macros libxslt

%description

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --sysconfdir=/etc \
            --mandir=/usr/share/man --localstatedir=/var \
            --libdir=%{_libdir} &&

%check

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

%files
%defattr(-,root,root,-)
%{_includedir}/X11/extensions/Xv.h
%{_includedir}/X11/extensions/XvMC.h
%{_includedir}/X11/extensions/XvMCproto.h
%{_includedir}/X11/extensions/Xvproto.h
%{_includedir}/X11/extensions/vldXvMC.h
%{_libdir}/pkgconfig/videoproto.pc

%files doc
%defattr(-,root,root,-)
%{_docdir}/videoproto/xv-protocol-v2.txt

%clean
rm -rf %{buildroot}

%changelog
* Wed Dec 4 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.3.1 to 2.3.2
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
