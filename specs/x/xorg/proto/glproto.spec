Name:       glproto
Version:    1.4.17
Release:    1%{?dist}
Summary:    glproto

Group:      User Interface/Xorg
License:    GPLv2+
Url:        http://xorg.freedesktop.org
Source:     http://xorg.freedesktop.org/releases/individual/proto/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  util-macros libxslt

%description

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --sysconfdir=/etc \
            --mandir=/usr/share/man --localstatedir=/var \
            --libdir=%{_libdir} &&

%check

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

%files
%defattr(-,root,root,-)
%{_includedir}/GL/glxint.h
%{_includedir}/GL/glxmd.h
%{_includedir}/GL/glxproto.h
%{_includedir}/GL/glxtokens.h
%{_includedir}/GL/internal/glcore.h
%{_libdir}/pkgconfig/glproto.pc

%clean
rm -rf %{buildroot}

%changelog
* Thu Mar 13 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.4.16 to 1.4.17
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
