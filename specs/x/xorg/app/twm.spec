Name:       twm
Version:    1.0.9
Release:    1%{?dist}
Summary:    twm

Group:      User Interface/Xorg
License:    GPLv2+
Url:        http://xorg.freedesktop.org
Source:     http://dri.freedesktop.org/libdrm/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  xserver
BuildRequires:  util-macros bison flex

%description
The twm package contains a very minimal window manager.
This package is not a part of the Xorg katamari and is provided only as a dependency to other packages or for testing the completed Xorg installation.

%prep
%setup -q -n %{name}-%{version}

%build
sed -i -e '/^rcdir =/s,^\(rcdir = \).*,\1/etc/X11/app-defaults,' src/Makefile.in &&
./configure --prefix=/usr --sysconfdir=/etc \
            --mandir=/usr/share/man --localstatedir=/var &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
/etc/X11/app-defaults/system.twmrc
%{_bindir}/twm
%{_mandir}/man1/twm.1.gz

%clean
rm -rf %{buildroot}

%changelog
* Mon Sep 14 2015 tanggeliang <tanggeliang@gmail.com>
- update from 1.0.8 to 1.0.9
* Thu Oct 3 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.0.7 to 1.0.8
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
