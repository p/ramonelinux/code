Name:       xrandr
Version:    1.4.3
Release:    1%{?dist}
Summary:    xrandr

Group:      User Interface/Xorg
License:    GPLv2+
Url:        http://xorg.freedesktop.org
Source:     http://xorg.freedesktop.org/releases/individual/app/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  util-macros libxrandr libxrender libx11

%description

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --sysconfdir=/etc \
            --mandir=/usr/share/man --localstatedir=/var &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

%files
%defattr(-,root,root,-)
%{_bindir}/xkeystone
%{_bindir}/xrandr
%{_mandir}/*/*

%clean
rm -rf %{buildroot}

%changelog
* Mon Sep 14 2015 tanggeliang <tanggeliang@gmail.com>
- update from 1.4.2 to 1.4.3
* Mon Jul 21 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.4.1 to 1.4.2
* Thu Oct 3 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.3.5 to 1.4.1
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
