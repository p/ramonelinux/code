Name:       xdm
Version:    1.1.11
Release:    4%{?dist}
Summary:    xdm

Group:      User Interface/Xorg
License:    GPLv2+
Url:        http://xorg.freedesktop.org
Source:     http://xorg.freedesktop.org/releases/individual/app/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  util-macros libx11 libxmu libxaw xtrans
Requires:       xconsole xsm

%description

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --sysconfdir=/etc \
            --mandir=/usr/share/man --localstatedir=/var &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

%files
%defattr(-,root,root,-)
/etc/X11/app-defaults/Chooser
%{_bindir}/xdm
%{_libdir}/X11/xdm/GiveConsole
%{_libdir}/X11/xdm/TakeConsole
%{_libdir}/X11/xdm/Xaccess
%{_libdir}/X11/xdm/Xreset
%{_libdir}/X11/xdm/Xresources
%{_libdir}/X11/xdm/Xservers
%{_libdir}/X11/xdm/Xsession
%{_libdir}/X11/xdm/Xsetup_0
%{_libdir}/X11/xdm/Xstartup
%{_libdir}/X11/xdm/Xwilling
%{_libdir}/X11/xdm/chooser
%{_libdir}/X11/xdm/libXdmGreet.*
%{_libdir}/X11/xdm/pixmaps/xorg-bw.xpm
%{_libdir}/X11/xdm/pixmaps/xorg.xpm
%{_libdir}/X11/xdm/xdm-config
%{_mandir}/man1/xdm.1.gz

%clean
rm -rf %{buildroot}

%changelog
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
