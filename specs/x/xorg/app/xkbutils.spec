Name:       xkbutils
Version:    1.0.4
Release:    1%{?dist}
Summary:    xkbutils

Group:      User Interface/Xorg
License:    GPLv2+
Url:        http://xorg.freedesktop.org
Source:     http://xorg.freedesktop.org/releases/individual/app/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  util-macros libx11 libxaw libxt inputproto

%description

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --sysconfdir=/etc \
            --mandir=/usr/share/man --localstatedir=/var &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

%files
%defattr(-,root,root,-)
%{_bindir}/xkbbell
%{_bindir}/xkbvleds
%{_bindir}/xkbwatch
%{_mandir}/*/*

%clean
rm -rf %{buildroot}

%changelog
* Thu Oct 3 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.0.3 to 1.0.4
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
