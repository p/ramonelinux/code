Name:       intel-gpu-tools
Version:    1.12
Release:    1%{?dist}
Summary:    intel-gpu-tools

Group:      User Interface/Xorg
License:    GPLv2+
Url:        http://xorg.freedesktop.org
Source:     http://xorg.freedesktop.org/releases/individual/app/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:	flex bison gtk-doc xserver
BuildRequires:  util-macros libdrm libxv libx11 libxext libxrandr dri2proto
BuildRequires:	systemd-libudev libunwind glib cairo libpciaccess

%description

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --sysconfdir=/etc \
            --mandir=/usr/share/man --localstatedir=/var \
	    --libexecdir=%{_libdir} \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/igt_stats
%{_bindir}/intel-gen4asm
%{_bindir}/intel-gen4disasm
%{_bindir}/intel-gpu-overlay
%{_bindir}/intel_aubdump
%{_bindir}/intel_audio_dump
%{_bindir}/intel_backlight
%{_bindir}/intel_bios_dumper
%{_bindir}/intel_bios_reader
%{_bindir}/intel_display_crc
%{_bindir}/intel_display_poller
%{_bindir}/intel_dump_decode
%{_bindir}/intel_error_decode
%{_bindir}/intel_firmware_decode
%{_bindir}/intel_forcewaked
%{_bindir}/intel_framebuffer_dump
%{_bindir}/intel_gpu_abrt
%{_bindir}/intel_gpu_frequency
%{_bindir}/intel_gpu_time
%{_bindir}/intel_gpu_top
%{_bindir}/intel_gtt
%{_bindir}/intel_infoframes
%{_bindir}/intel_l3_parity
%{_bindir}/intel_lid
%{_bindir}/intel_opregion_decode
%{_bindir}/intel_panel_fitter
%{_bindir}/intel_perf_counters
%{_bindir}/intel_reg
%{_bindir}/intel_reg_checker
%{_bindir}/intel_sprite_on
%{_bindir}/intel_stepping
%{_bindir}/intel_watermark
%{_libdir}/intel_aubdump.la
%{_libdir}/intel_aubdump.so
%{_libdir}/pkgconfig/intel-gen4asm.pc
%{_libdir}/intel-gpu-tools/benchmarks/*
%{_libdir}/intel-gpu-tools/check_drm_clients
%{_libdir}/intel-gpu-tools/core_*
%{_libdir}/intel-gpu-tools/ddx_intel_after_fbdev
%{_libdir}/intel-gpu-tools/debugfs_*
%{_libdir}/intel-gpu-tools/drm_*
%{_libdir}/intel-gpu-tools/drv_*
%{_libdir}/intel-gpu-tools/gem_*
%{_libdir}/intel-gpu-tools/gen3_*
%{_libdir}/intel-gpu-tools/gen7_forcewake_mt
%{_libdir}/intel-gpu-tools/kms_*
%{_libdir}/intel-gpu-tools/pm_*
%{_libdir}/intel-gpu-tools/prime_*
%{_libdir}/intel-gpu-tools/sysfs_l3_parity
%{_libdir}/intel-gpu-tools/template
%{_libdir}/intel-gpu-tools/test_rte_check
%{_libdir}/intel-gpu-tools/testdisplay
%{_libdir}/intel-gpu-tools/tools_test
%{_datadir}/gtk-doc/html/intel-gpu-tools/*
%{_datadir}/intel-gpu-tools/*.png
%{_datadir}/intel-gpu-tools/*.txt
%{_datadir}/intel-gpu-tools/registers/*
%{_mandir}/man1/intel_*.1.gz

%clean
rm -rf %{buildroot}

%changelog
* Sun Sep 20 2015 tanggeliang <tanggeliang@gmail.com>
- create
