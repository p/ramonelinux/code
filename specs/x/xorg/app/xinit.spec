Name:       xinit
Version:    1.3.4
Release:    1%{?dist}
Summary:    xinit

Group:      User Interface/Xorg
License:    GPLv2+
Url:        http://xorg.freedesktop.org
Source:     http://dri.freedesktop.org/libdrm/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  util-macros libx11
Requires:       setup
Requires:       font-alias xserver
Requires:       xf86-input-acecad xf86-input-aiptek xf86-input-evdev xf86-input-joystick xf86-input-keyboard xf86-input-mouse xf86-input-synaptics xf86-input-vmmouse xf86-input-void xf86-input-wacom
Requires:       xf86-video-apm xf86-video-ark xf86-video-ast xf86-video-ati xf86-video-cirrus xf86-video-dummy xf86-video-fbdev xf86-video-glint xf86-video-i128 xf86-video-i740 xf86-video-intel xf86-video-mach64 xf86-video-mga xf86-video-neomagic xf86-video-nouveau xf86-video-nv xf86-video-openchrome xf86-video-r128 xf86-video-rendition xf86-video-s3 xf86-video-s3virge xf86-video-savage xf86-video-siliconmotion xf86-video-sis xf86-video-sisusb xf86-video-tdfx xf86-video-tga xf86-video-trident xf86-video-tseng xf86-video-v4l xf86-video-vesa xf86-video-vmware xf86-video-voodoo
# xf86-video-xgi
# xf86-video-geode
Requires:       xauth xkbcomp
Requires:       xkeyboard-config xbitmaps xcursor-themes

%description
The xinit package contains a usable script to start the xserver.
This package is not a part of the Xorg katamari and is provided only as a dependency to other packages or for testing the completed Xorg installation.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --sysconfdir=/etc \
            --mandir=/usr/share/man --localstatedir=/var \
            --with-xinitdir=/etc/X11/app-defaults &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
/etc/X11/app-defaults/xinitrc
%{_bindir}/startx
%{_bindir}/xinit
%{_mandir}/man1/startx.1.gz
%{_mandir}/man1/xinit.1.gz

%clean
rm -rf %{buildroot}

%changelog
* Mon Sep 14 2015 tanggeliang <tanggeliang@gmail.com>
- update from 1.3.3 to 1.3.4
* Thu Oct 3 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.3.2 to 1.3.3
* Sat Sep 22 2012 tanggeliang <tanggeliang@gmail.com>
- add "font-alias" require.
- remove "xclock xterm twm" requires.
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
