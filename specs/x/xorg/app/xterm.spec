Name:       xterm
Version:    320
Release:    1%{?dist}
Summary:    xterm

Group:      User Interface/Xorg
License:    GPLv2+
Url:        http://invisible-island.net/xterm/xterm.html
Source:     ftp://invisible-island.net/xterm/%{name}-%{version}.tgz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  libx11 libxt libxaw libxext libxmu libice 
BuildRequires:  desktop-file-utils
Requires:       xserver

%description
xterm is a terminal emulator for the X Window System.
This package is not a part of the Xorg katamari and is provided only as a dependency to other packages or for testing the completed Xorg installation.

%prep
%setup -q -n %{name}-%{version}

%build
sed -i '/v0/,+1s/new:/new:kb=^?:/' termcap &&
echo -e '\tkbs=\\177,' >>terminfo &&

TERMINFO=%{_libdir}/terminfo \
./configure --prefix=/usr --sysconfdir=/etc \
            --mandir=/usr/share/man --localstatedir=/var \
            --with-app-defaults=/etc/X11/app-defaults \
            --enable-luit \
            --enable-wide-chars &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install &&
make DESTDIR=%{buildroot} install-ti

desktop-file-install --dir=%{buildroot}%{_datadir}/applications xterm.desktop

cat >> %{buildroot}/etc/X11/app-defaults/XTerm << "EOF"
*VT100*locale: true
*VT100*faceName: Monospace
*VT100*faceSize: 10
*backarrowKeyIsErase: true
*ptyInitialErase: true
EOF

%files
%defattr(-,root,root,-)
/etc/X11/app-defaults/*
%{_bindir}/koi8rxterm
%{_bindir}/resize
%{_bindir}/uxterm
%{_bindir}/xterm
%{_libdir}/terminfo/v/vs100
%{_libdir}/terminfo/x/xterm*
%{_datadir}/pixmaps/*xterm*.xpm
%{_datadir}/applications/xterm.desktop
%{_mandir}/*/*

%clean
rm -rf %{buildroot}

%changelog
* Mon Sep 14 2015 tanggeliang <tanggeliang@gmail.com>
- update from 309 to 320
* Mon Jul 21 2014 tanggeliang <tanggeliang@gmail.com>
- update from 303 to 309
* Thu Mar 20 2014 tanggeliang <tanggeliang@gmail.com>
- update from 300 to 303
* Thu Dec 5 2013 tanggeliang <tanggeliang@gmail.com>
- update from 299 to 300
* Tue Dec 3 2013 tanggeliang <tanggeliang@gmail.com>
- update from 298 to 299
* Sun Dec 1 2013 tanggeliang <tanggeliang@gmail.com>
- update from 297 to 298
* Wed Oct 2 2013 tanggeliang <tanggeliang@gmail.com>
- update from 296 to 297
* Sat Aug 24 2013 tanggeliang <tanggeliang@gmail.com>
- remove BuildRequires luit
* Sat Aug 24 2013 tanggeliang <tanggeliang@gmail.com>
- update from 291 to 296
* Thu May 30 2013 tanggeliang <tanggeliang@gmail.com>
- update from 279 to 291
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
