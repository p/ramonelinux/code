Name:       xclock
Version:    1.0.7
Release:    1%{?dist}
Summary:    xclock

Group:      User Interface/Xorg
License:    GPLv2+
Url:        http://xorg.freedesktop.org
Source:     http://dri.freedesktop.org/libdrm/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  util-macros libxaw libxmu libx11 libxrender libxft libxkbfile

%description
The xclock package contains a simple clock application which is used in the default xinit configuration.
This package is not a part of the Xorg katamari and is provided only as a dependency to other packages or for testing the completed Xorg installation.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --sysconfdir=/etc \
            --mandir=/usr/share/man --localstatedir=/var &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
/etc/X11/app-defaults/XClock
/etc/X11/app-defaults/XClock-color
%{_bindir}/xclock
%{_mandir}/man1/xclock.1.gz

%clean
rm -rf %{buildroot}

%changelog
* Thu Oct 3 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.0.6 to 1.0.7
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
