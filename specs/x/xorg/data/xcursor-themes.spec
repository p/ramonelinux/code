Name:       xcursor-themes
Version:    1.0.4
Release:    1%{?dist}
Summary:    xcursor-themes

Group:      User Interface/Xorg
License:    GPLv2+
Url:        http://xorg.freedesktop.org
Source:     http://xorg.freedesktop.org/releases/individual/data/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildArch:      noarch
BuildRequires:  libpng libxcursor libxau libxdmcp libxfixes libxrender libx11
BuildRequires:  util-macros xcursorgen xextproto

%description
The xcursor-themes package contains the redglass and whiteglass animated cursor themes.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --sysconfdir=/etc \
            --mandir=/usr/share/man --localstatedir=/var &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_datadir}/icons/handhelds/*
%{_datadir}/icons/redglass/*
%{_datadir}/icons/whiteglass/*

%clean
rm -rf %{buildroot}

%changelog
* Sun Aug 18 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.0.3 to 1.0.4
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
