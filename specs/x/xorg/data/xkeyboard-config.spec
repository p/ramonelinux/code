Name:       xkeyboard-config
Version:    2.15
Release:    1%{?dist}
Summary:    XKeyboardConfig

Group:      User Interface/Xorg
License:    GPLv2+
Url:        http://xorg.freedesktop.org
Source:     http://xorg.freedesktop.org/releases/individual/data/xkeyboard-config/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  expat xml-parser intltool xkbcomp
BuildRequires:  gettext libxslt

%description
The XKeyboardConfig package contains the keyboard configuration database for the X Window System.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --sysconfdir=/etc \
            --mandir=/usr/share/man --localstatedir=/var \
            --with-xkb-rules-symlink=xorg &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_datadir}/locale/*
%{_datadir}/pkgconfig/xkeyboard-config.pc
%{_datadir}/X11/xkb/*

%files doc
%defattr(-,root,root,-)
%{_mandir}/man7/xkeyboard-config.7.gz

%clean
rm -rf %{buildroot}

%changelog
* Mon Sep 14 2015 tanggeliang <tanggeliang@gmail.com>
- update from 2.12 to 2.15
* Mon Jul 21 2014 tanggeliang <tanggeliang@gmail.com>
- update from 2.10.1 to 2.12
* Thu Oct 10 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.9 to 2.10.1
* Thu Aug 8 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.7 to 2.9
* Sat Feb 23 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.5.1 to 2.7
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
