Name:       libxcb
Version:    1.11.1
Release:    1%{?dist}
Summary:    libxcb

Group:      User Interface/Xorg
License:    GPLv2+
Url:        http://www.freedesktop.org
Source:     http://xcb.freedesktop.org/dist/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  libxau libxdmcp libpthread-stubs libxslt xcb-proto xproto
BuildRequires:  autoconf automake m4 libtool doxygen util-macros

%description
The libxcb package provides an interface to the X Window System protocol, which replaces the current Xlib interface.
Xlib can also use XCB as a transport layer, allowing software to make requests and receive responses with both.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
sed -e "s/pthread-stubs//" -i configure.ac &&
autoreconf -fi &&
./configure --prefix=/usr --sysconfdir=/etc \
            --mandir=/usr/share/man --localstatedir=/var \
            --docdir=/usr/share/doc/libxcb-%{version} \
            --enable-xinput --enable-xkb \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_includedir}/xcb/*.h
%{_libdir}/libxcb*.*
%{_libdir}/pkgconfig/xcb-composite.pc
%{_libdir}/pkgconfig/xcb-damage.pc
%{_libdir}/pkgconfig/xcb-dpms.pc
%{_libdir}/pkgconfig/xcb-dri2.pc
%{_libdir}/pkgconfig/xcb-dri3.pc
%{_libdir}/pkgconfig/xcb-glx.pc
%{_libdir}/pkgconfig/xcb-present.pc
%{_libdir}/pkgconfig/xcb-randr.pc
%{_libdir}/pkgconfig/xcb-record.pc
%{_libdir}/pkgconfig/xcb-render.pc
%{_libdir}/pkgconfig/xcb-res.pc
%{_libdir}/pkgconfig/xcb-screensaver.pc
%{_libdir}/pkgconfig/xcb-shape.pc
%{_libdir}/pkgconfig/xcb-shm.pc
%{_libdir}/pkgconfig/xcb-sync.pc
%{_libdir}/pkgconfig/xcb-xevie.pc
%{_libdir}/pkgconfig/xcb-xf86dri.pc
%{_libdir}/pkgconfig/xcb-xfixes.pc
%{_libdir}/pkgconfig/xcb-xinerama.pc
%{_libdir}/pkgconfig/xcb-xinput.pc
%{_libdir}/pkgconfig/xcb-xkb.pc
%{_libdir}/pkgconfig/xcb-xprint.pc
%{_libdir}/pkgconfig/xcb-xtest.pc
%{_libdir}/pkgconfig/xcb-xv.pc
%{_libdir}/pkgconfig/xcb-xvmc.pc
%{_libdir}/pkgconfig/xcb.pc

%files doc
%defattr(-,root,root,-)
%{_docdir}/libxcb-%{version}/tutorial/index.html
%{_docdir}/libxcb-%{version}/tutorial/xcb.css
%{_mandir}/man3/*

%clean
rm -rf %{buildroot}

%changelog
* Mon Sep 14 2015 tanggeliang <tanggeliang@gmail.com>
- update from 1.10 to 1.11.1
* Fri Mar 14 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.9.3 to 1.10
* Tue Dec 3 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.9.1 to 1.9.3
* Mon Jul 29 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.9 to 1.9.1
* Mon Nov 5 2012 tanggeliang <tanggeliang@gmail.com>
- update from 1.8.1 to 1.9
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
