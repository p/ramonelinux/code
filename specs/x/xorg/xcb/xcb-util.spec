Name:       xcb-util
Version:    0.4.0
Release:    1%{?dist}
Summary:    xcb-util

Group:      User Interface/Xorg
License:    GPLv2+
Url:        http://www.freedesktop.org
Source:     http://xorg.freedesktop.org/releases/individual/lib/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  libxcb gperf
BuildRequires:  m4

%description
The xcb-util package provides additional extensions to the XCB library, many that were previously found in Xlib, but are not part of core X protocol.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --sysconfdir=/etc \
            --mandir=/usr/share/man --localstatedir=/var \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_includedir}/xcb/xcb_*.h
%{_libdir}/libxcb-util.*a
%{_libdir}/libxcb-util.so*
%{_libdir}/pkgconfig/xcb-atom.pc
%{_libdir}/pkgconfig/xcb-aux.pc
%{_libdir}/pkgconfig/xcb-event.pc
%{_libdir}/pkgconfig/xcb-util.pc

%clean
rm -rf %{buildroot}

%changelog
* Mon Sep 14 2015 tanggeliang <tanggeliang@gmail.com>
- update from 0.3.9 to 0.4.0
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
