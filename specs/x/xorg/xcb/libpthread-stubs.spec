Name:       libpthread-stubs
Version:    0.3
Release:    7%{?dist}
Summary:    libpthread-stubs

Group:      User Interface/Xorg
License:    GPLv2+
Url:        http://www.freedesktop.org
Source:     http://xcb.freedesktop.org/dist/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build

%description
The libpthread-stubs package provides weak aliases for pthread functions not provided in libc or otherwise available by default.
This is useful for libraries that rely on pthread stubs to use pthreads optionally.
On Linux, all necessary pthread functions are available, so this package is simply a placeholder.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_libdir}/pkgconfig/pthread-stubs.pc

%clean
rm -rf %{buildroot}

%changelog
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
