Name:       xcb-util-renderutil
Version:    0.3.9
Release:    1%{?dist}
Summary:    xcb-util-renderutil

Group:      User Interface/Xorg
License:    GPLv2+
Url:        http://www.freedesktop.org
Source:     http://xorg.freedesktop.org/releases/individual/lib/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  libxcb util-macros

%description
The xcb-util-renderutil package provides additional extensions to the XCB library. 

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --sysconfdir=/etc \
            --mandir=/usr/share/man --localstatedir=/var \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_includedir}/xcb/xcb_renderutil.h
%{_libdir}/libxcb-render-util.*a
%{_libdir}/libxcb-render-util.so*
%{_libdir}/pkgconfig/xcb-renderutil.pc

%clean
rm -rf %{buildroot}

%changelog
* Mon Jul 21 2014 tanggeliang <tanggeliang@gmail.com>
- update from 0.3.8 to 0.3.9
* Fri Feb 22 2013 tanggeliang <tanggeliang@gmail.com>
- create
