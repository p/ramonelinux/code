Name:       xcb-util-image
Version:    0.4.0
Release:    1%{?dist}
Summary:    xcb-util-image

Group:      User Interface/Xorg
License:    GPLv2+
Url:        http://www.freedesktop.org
Source:     http://xorg.freedesktop.org/releases/individual/lib/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  xcb-util util-macros

%description
The xcb-util-image package provides additional extensions to the XCB library. 

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --sysconfdir=/etc \
            --mandir=/usr/share/man --localstatedir=/var \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_includedir}/xcb/xcb_*.h
%{_libdir}/libxcb-image.*a
%{_libdir}/libxcb-image.so*
%{_libdir}/pkgconfig/xcb-image.pc

%clean
rm -rf %{buildroot}

%changelog
* Mon Sep 14 2015 tanggeliang <tanggeliang@gmail.com>
- update from 0.3.9 to 0.4.0
* Fri Feb 22 2013 tanggeliang <tanggeliang@gmail.com>
- create
