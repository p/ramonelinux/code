Name:       xcb-util-keysyms
Version:    0.4.0
Release:    1%{?dist}
Summary:    xcb-util-keysyms

Group:      User Interface/Xorg
License:    GPLv2+
Url:        http://www.freedesktop.org
Source:     http://xorg.freedesktop.org/releases/individual/lib/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  libxcb xcb-util util-macros

%description
The xcb-util-image package provides additional extensions to the XCB library. 

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --sysconfdir=/etc \
            --mandir=/usr/share/man --localstatedir=/var \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_includedir}/xcb/xcb_keysyms.h
%{_libdir}/libxcb-keysyms.*a
%{_libdir}/libxcb-keysyms.so*
%{_libdir}/pkgconfig/xcb-keysyms.pc

%clean
rm -rf %{buildroot}

%changelog
* Sat Jun 27 2015 tanggeliang <tanggeliang@gmail.com>
- create
