Name:       xcb-proto
Version:    1.11
Release:    1%{?dist}
Summary:    xcb-proto

Group:      User Interface/Xorg
License:    GPLv2+
Url:        http://www.freedesktop.org
Source:     http://xcb.freedesktop.org/dist/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  libxml

%description
The xcb-proto package provides the XML-XCB protocol descriptions that libxcb uses to generate the majority of its code and API.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --sysconfdir=/etc \
            --mandir=/usr/share/man --localstatedir=/var \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_libdir}/pkgconfig/xcb-proto.pc
/usr/lib/python2.7/site-packages/xcbgen/*.py*
%{_datadir}/xcb/*.xml
%{_datadir}/xcb/xcb.xsd

%clean
rm -rf %{buildroot}

%changelog
* Mon Sep 14 2015 tanggeliang <tanggeliang@gmail.com>
- update from 1.10 to 1.11
* Fri Mar 14 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.9 to 1.10
* Tue Dec 3 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.8 to 1.9
* Mon Nov 5 2012 tanggeliang <tanggeliang@gmail.com>
- update from 1.7.1 to 1.8
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
