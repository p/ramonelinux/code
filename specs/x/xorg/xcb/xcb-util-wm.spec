Name:       xcb-util-wm
Version:    0.4.1
Release:    1%{?dist}
Summary:    xcb-util-wm

Group:      User Interface/Xorg
License:    GPLv2+
Url:        http://www.freedesktop.org
Source:     http://xorg.freedesktop.org/releases/individual/lib/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  xcb-util m4 util-macros

%description
The xcb-util-wm package contains libraries which provide client and window-manager helpers for EWMH and ICCCM.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --sysconfdir=/etc \
            --mandir=/usr/share/man --localstatedir=/var \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_includedir}/xcb/xcb_*.h
%{_libdir}/libxcb-ewmh.*a
%{_libdir}/libxcb-ewmh.so*
%{_libdir}/libxcb-icccm.*a
%{_libdir}/libxcb-icccm.so*
%{_libdir}/pkgconfig/xcb-ewmh.pc
%{_libdir}/pkgconfig/xcb-icccm.pc

%clean
rm -rf %{buildroot}

%changelog
* Thu Mar 20 2014 tanggeliang <tanggeliang@gmail.com>
- update from 0.3.9 to 0.4.1
* Fri Aug 2 2013 tanggeliang <tanggeliang@gmail.com>
- create
