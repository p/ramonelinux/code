Name:       libva
Version:    1.5.1
Release:    1%{?dist}
Summary:    Video Acceleration

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.freedesktop.org
Source:     http://www.freedesktop.org/software/vaapi/releases/libva/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  libdrm libx11 mesa wayland

%description
The libva package contains a library which provides access to hardware accelerated video processing, using hardware to accelerate video processing in order to offload the central processing unit (CPU) to decode and encode compressed digital video.
VA API video decode/encode interface is platform and window system independent targeted at Direct Rendering Infrastructure (DRI) in the X Window System however it can potentially also be used with direct framebuffer and graphics sub-systems for video output.
Accelerated processing includes support for video decoding, video encoding, subpicture blending, and rendering.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --disable-static \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/avcenc
%{_bindir}/h264encode
%{_bindir}/jpegenc
%{_bindir}/loadjpeg
%{_bindir}/mpeg2vaenc
%{_bindir}/mpeg2vldemo
%{_bindir}/putsurface
%{_bindir}/putsurface_wayland
%{_bindir}/vainfo
%{_includedir}/va/va.h
%{_includedir}/va/va_*.h
%{_libdir}/dri/dummy_drv_video.*
%{_libdir}/libva-drm.*
%{_libdir}/libva-egl.*
%{_libdir}/libva-glx.*
%{_libdir}/libva-tpi.*
%{_libdir}/libva-wayland.*
%{_libdir}/libva-x11.*
%{_libdir}/libva.*
%{_libdir}/pkgconfig/libva-drm.pc
%{_libdir}/pkgconfig/libva-egl.pc
%{_libdir}/pkgconfig/libva-glx.pc
%{_libdir}/pkgconfig/libva-tpi.pc
%{_libdir}/pkgconfig/libva-wayland.pc
%{_libdir}/pkgconfig/libva-x11.pc
%{_libdir}/pkgconfig/libva.pc

%clean
rm -rf %{buildroot}

%changelog
* Sat Jun 27 2015 tanggeliang <tanggeliang@gmail.com>
- update from 1.3.0 to 1.5.1
* Thu Jul 24 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.2.1 to 1.3.1
* Sat Aug 24 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.1.1 to 1.2.1
* Sun Jun 23 2013 tanggeliang <tanggeliang@gmail.com>
- create
