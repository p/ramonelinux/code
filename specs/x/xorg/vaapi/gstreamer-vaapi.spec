Name:       gstreamer-vaapi
Version:    0.5.5
Release:    1%{?dist}
Summary:    gstreamer-vaapi

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.freedesktop.org
Source:     http://www.freedesktop.org/software/vaapi/releases/%{name}/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  libva
BuildRequires:  gstreamer gst-plugins-base gstreamer-basevideo

%description
The main motivation for VA-API (Video Acceleration API) is to enable hardware accelerated video decode/encode at various entry-points (VLD, IDCT, Motion Compensation etc.) for the prevailing coding standards today (MPEG-2, MPEG-4 ASP/H.263, MPEG-4 AVC/H.264, and VC-1/VMW3).
Extending XvMC was considered, but due to its original design for MPEG-2 ?MotionComp only, it made more sense to design an interface from scratch that can fully expose the video decode capabilities in today's GPUs.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)

%clean
rm -rf %{buildroot}

%changelog
* Sat Aug 24 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.5.4 to 0.5.5
* Sun Jun 23 2013 tanggeliang <tanggeliang@gmail.com>
- create
