Name:       libvdpau
Version:    1.1
Release:    1%{?dist}
Summary:    libvdpau

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.freedesktop.org
Source:     http://people.freedesktop.org/~aplattner/vdpau/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  libx11 libxext dri2proto
BuildRequires:  libtool autoconf automake m4
Requires:       mesa

%description
The libvdpau package contains a library which implements the VDPAU library.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --sysconfdir=/etc \
            --mandir=/usr/share/man --localstatedir=/var \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
/etc/vdpau_wrapper.cfg
%{_includedir}/vdpau/vdpau.h
%{_includedir}/vdpau/vdpau_x11.h
%{_libdir}/libvdpau.*
%{_libdir}/pkgconfig/vdpau.pc
%{_libdir}/vdpau/libvdpau_trace.*

%clean
rm -rf %{buildroot}

%changelog
* Sat Sep 26 2015 tanggeliang <tanggeliang@gmail.com>
- update from 0.8 to 1.1
* Thu Jul 24 2014 tanggeliang <tanggeliang@gmail.com>
- update from 0.7 to 0.8
* Fri Oct 4 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.6 to 0.7
* Sun Jun 23 2013 tanggeliang <tanggeliang@gmail.com>
- create
