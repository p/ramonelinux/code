Name:       libva-intel-driver
Version:    1.3.2
Release:    1%{?dist}
Summary:    Video Acceleration for Intel Driver (i965 chipsets only)

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.freedesktop.org
Source:     http://www.freedesktop.org/software/vaapi/releases/libva-intel-driver/%{name}/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  libva

%description
The libva-intel-driver is designed specifically for video cards based on an Intel GPU.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --sysconfdir=/etc \
            --mandir=/usr/share/man --localstatedir=/var \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_libdir}/dri/i965_drv_video.la
%{_libdir}/dri/i965_drv_video.so

%clean
rm -rf %{buildroot}

%changelog
* Thu Jul 24 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.2.1 to 1.3.2
* Fri Oct 4 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.2.0 to 1.2.1
* Sat Aug 24 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.0.20 to 1.2.0
* Sun Jun 23 2013 tanggeliang <tanggeliang@gmail.com>
- create
