Name:       libva-vdpau-driver
Version:    0.7.4
Release:    1%{?dist}
Summary:    VDPAU Driver for VDPAU capable Cards

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.freedesktop.org
Source:     http://www.freedesktop.org/software/vaapi/releases/%{name}/%{name}-%{version}.tar.bz2
Patch1:     libva-vdpau-driver-0.7.4_1.patch
Patch2:     libva-vdpau-driver-0.7.4_2.patch

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  libva libvdpau

%description
A VDPAU-based backend for VA-API.

%prep
%setup -q -n %{name}-%{version}
%patch1 -p0
%patch2 -p0

%build
./configure --prefix=/usr --sysconfdir=/etc \
            --mandir=/usr/share/man --localstatedir=/var \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_libdir}/dri/nvidia_drv_video.so
%{_libdir}/dri/s3g_drv_video.so
%{_libdir}/dri/vdpau_drv_video.la
%{_libdir}/dri/vdpau_drv_video.so

%clean
rm -rf %{buildroot}

%changelog
* Sun Jun 23 2013 tanggeliang <tanggeliang@gmail.com>
- create
