Name:       libepoxy
Version:    1.3.1
Release:    1%{?dist}
Summary:    Epoxy

Group:      User Interface/Xorg
License:    GPLv2+
Url:        http://www.freedesktop.org
Source:     https://github.com/anholt/libepoxy/releases/download/v%{version}/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  mesa util-macros xtrans
BuildRequires:  autoconf automake m4 libtool gettext

%description
libepoxy is a library for handling OpenGL function pointer management.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_includedir}/epoxy/egl*.h
%{_includedir}/epoxy/gl*.h
%{_libdir}/libepoxy.la
%{_libdir}/libepoxy.so*
%{_libdir}/pkgconfig/epoxy.pc

%clean
rm -rf %{buildroot}

%changelog
* Tue Sep 15 2015 tanggeliang <tanggeliang@gmail.com>
- update from 1.2 to 1.3.1
* Sat Sep 27 2014 tanggeliang <tanggeliang@gmail.com>
- create
