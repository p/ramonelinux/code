Name:       wayland
Version:    1.9.0
Release:    1%{?dist}
Summary:    Wayland

Group:      User Interface/Xorg
License:    GPLv2+
Url:        http://www.freedesktop.org
Source:     http://wayland.freedesktop.org/releases/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:	libffi doxygen

%description
Wayland is intended as a simpler replacement for X, easier to develop and maintain. GNOME and KDE are expected to be ported to it.

Wayland is a protocol for a compositor to talk to its clients as well as a C library implementation of that protocol. The compositor can be a standalone display server running on Linux kernel modesetting and evdev input devices, an X application, or a wayland client itself. The clients can be traditional applications, X servers (rootless or fullscreen) or other display servers.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr 	    \
	    --disable-static 	    \
            --disable-documentation \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/wayland-scanner
%{_includedir}/wayland-*.h
%{_libdir}/libwayland-client.*a
%{_libdir}/libwayland-client.so*
%{_libdir}/libwayland-cursor.*a
%{_libdir}/libwayland-cursor.so*
%{_libdir}/libwayland-server.*a
%{_libdir}/libwayland-server.so*
%{_libdir}/pkgconfig/wayland-client.pc
%{_libdir}/pkgconfig/wayland-cursor.pc
%{_libdir}/pkgconfig/wayland-scanner.pc
%{_libdir}/pkgconfig/wayland-server.pc
%{_datadir}/aclocal/wayland-scanner.m*
%{_datadir}/wayland/wayland.dtd
%{_datadir}/wayland/wayland.xml
%{_datadir}/wayland/wayland-scanner.mk

%clean
rm -rf %{buildroot}

%changelog
* Tue Sep 22 2015 tanggeliang <tanggeliang@gmail.com>
- update from 1.8.92 to 1.9.0
* Tue Sep 15 2015 tanggeliang <tanggeliang@gmail.com>
- update from 1.8.1 to 1.8.92
* Sat Jun 27 2015 tanggeliang <tanggeliang@gmail.com>
- update from 1.7.0 to 1.8.1
* Sun Mar 29 2015 tanggeliang <tanggeliang@gmail.com>
- update from 1.6.0 to 1.7.0
* Tue Sep 23 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.5.0 to 1.6.0
* Sat Jul 26 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.4.0 to 1.5.0
* Thu Mar 13 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.3.91 to 1.4.0
* Sun Jan 5 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.3.0 to 1.3.91
* Sun Dec 1 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.2.92 to 1.3.0
* Mon Oct 7 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.2.91 to 1.2.92
* Sat Sep 28 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.2.0 to 1.2.91
* Fri Aug 2 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.1.0 to 1.2.0
* Thu May 23 2013 tanggeliang <tanggeliang@gmail.com>
- create
