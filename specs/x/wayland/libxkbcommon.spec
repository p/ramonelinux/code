Name:       libxkbcommon
Version:    0.5.0
Release:    2%{?dist}
Summary:    libxkbcommon

Group:      User Interface/Xorg
License:    GPLv2+
Url:        http://www.xkbcommon.org
Source:     http://www.xkbcommon.org/download/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:	xkeyboard-config libxcb util-macros
BuildRequires:	bison flex xproto kbproto libx11

%description
libxkbcommon is a keymap compiler and support library which processes a reduced subset of keymaps as defined by the XKB specification.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr       \
            --disable-static    \
            --libdir=%{_libdir} \
            --docdir=/usr/share/doc/%{name}-%{version} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_includedir}/xkbcommon/xkbcommon*.h
%{_libdir}/libxkbcommon.*a
%{_libdir}/libxkbcommon.so*
%{_libdir}/libxkbcommon-x11.*a
%{_libdir}/libxkbcommon-x11.so*
%{_libdir}/pkgconfig/xkbcommon.pc
%{_libdir}/pkgconfig/xkbcommon-x11.pc

%clean
rm -rf %{buildroot}

%changelog
* Sat Jun 27 2015 tanggeliang <tanggeliang@gmail.com>
- update from 0.4.3 to 0.5.0
* Tue Sep 23 2014 tanggeliang <tanggeliang@gmail.com>
- update from 0.3.0 to 0.4.3
* Thu May 23 2013 tanggeliang <tanggeliang@gmail.com>
- create
