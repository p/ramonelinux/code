Name:       weston
Version:    1.8.92
Release:    1%{?dist}
Summary:    Weston

Group:      User Interface/Xorg
License:    GPLv2+
Url:        http://www.freedesktop.org
Source:     http://wayland.freedesktop.org/releases/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  mesa-wayland cairo-gl
BuildRequires:  wayland pixman libxkbcommon libunwind
BuildRequires:  libxcursor mtdev libjpeg-turbo
BuildRequires:  libdrm libxcb linux-pam poppler pango glu librsvg libinput

%description
Part of the Wayland project is also the Weston reference implementation of a Wayland compositor.
Weston can run as an X client or under Linux KMS and ships with a few demo clients.
The Weston compositor is a minimal and fast compositor and is suitable for many embedded and mobile use cases.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --disable-setuid-install \
            --enable-xwayland \
            --libdir=%{_libdir} \
            --libexecdir=%{_libdir}/weston &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/wcap-decode
%{_bindir}/weston
%{_bindir}/weston-info
%{_bindir}/weston-launch
%{_bindir}/weston-terminal
%{_includedir}/weston/*.h
%{_libdir}/pkgconfig/weston.pc
%{_libdir}/weston/cms-static.*
%{_libdir}/weston/desktop-shell.*
%{_libdir}/weston/drm-backend.*
%{_libdir}/weston/fbdev-backend.*
%{_libdir}/weston/fullscreen-shell.*
%{_libdir}/weston/headless-backend.*
%{_libdir}/weston/hmi-controller.*
%{_libdir}/weston/gl-renderer.*
%{_libdir}/weston/ivi-shell.*
%{_libdir}/weston/wayland-backend.*
%{_libdir}/weston/weston-desktop-shell
%{_libdir}/weston/weston-ivi-shell-user-interface
%{_libdir}/weston/weston-keyboard
%{_libdir}/weston/weston-screenshooter
%{_libdir}/weston/weston-simple-im
%{_libdir}/weston/x11-backend.*
%{_libdir}/weston/xwayland.*
%{_datadir}/wayland-sessions/weston.desktop
%{_datadir}/weston/*.png
%{_datadir}/weston/wayland.svg
%{_mandir}/man*/weston*.gz

%post
if test -z "${XDG_RUNTIME_DIR}"; then
    echo "XDG_RUNTIME_DIR=/tmp/wayland" >> /etc/environment
    if ! test -d "${XDG_RUNTIME_DIR}"; then
        mkdir "${XDG_RUNTIME_DIR}"
        chmod 0700 "${XDG_RUNTIME_DIR}"
    fi
fi

groupadd weston-launch
usermod -a -G weston-launch $USER
chown root %{_bindir}/weston-launch
chmod +s %{_bindir}/weston-launch

%clean
rm -rf %{buildroot}

%changelog
* Tue Sep 15 2015 tanggeliang <tanggeliang@gmail.com>
- update from 1.7.0 to 1.8.92
* Sun Mar 29 2015 tanggeliang <tanggeliang@gmail.com>
- update from 1.6.0 to 1.7.0
* Tue Sep 23 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.5.0 to 1.6.0
* Sat Jul 26 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.4.0 to 1.5.0
* Thu Mar 13 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.3.91 to 1.4.0
* Sun Jan 5 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.3.1 to 1.3.91
* Sun Dec 1 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.2.92 to 1.3.1
* Mon Oct 7 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.2.91 to 1.2.92
* Sat Sep 28 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.2.0 to 1.2.91
* Fri Aug 2 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.1.1 to 1.2.0
* Tue Jul 2 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.1.0 to 1.1.1
* Thu May 23 2013 tanggeliang <tanggeliang@gmail.com>
- create
- add "--disable-setuid-install" to fix "chown root weston-launch Operation not permitted"
