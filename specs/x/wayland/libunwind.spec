Name:       libunwind
Version:    1.1
Release:    1%{?dist}
Summary:    libunwind

Group:      User Interface/Xorg
License:    GPLv2+
Url:        http://www.nongnu.org/libunwind
Source:     http://download.savannah.nongnu.org/releases/libunwind/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build

%description
The primary goal of this project is to define a portable and efficient C programming interface (API) to determine the call-chain of a program.
The API additionally provides the means to manipulate the preserved (callee-saved) state of each call-frame and to resume execution at any point in the call-chain (non-local goto).
The API supports both local (same-process) and remote (across-process) operation.
As such, the API is useful in a number of applications.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_includedir}/libunwind-*.h
%{_includedir}/libunwind.h
%{_includedir}/unwind.h
%{_libdir}/libunwind-coredump.*
%{_libdir}/libunwind-generic.*
%{_libdir}/libunwind-ptrace.*
%{_libdir}/libunwind-setjmp.*
%ifarch %{ix86}
%{_libdir}/libunwind-x86.*
%else %ifarch x86_64
%{_libdir}/libunwind-x86_64.*
%endif
%{_libdir}/libunwind.*
%{_libdir}/pkgconfig/libunwind-coredump.pc
%{_libdir}/pkgconfig/libunwind-generic.pc
%{_libdir}/pkgconfig/libunwind-ptrace.pc
%{_libdir}/pkgconfig/libunwind-setjmp.pc
%{_libdir}/pkgconfig/libunwind.pc
%{_mandir}/man3/*.3.gz

%clean
rm -rf %{buildroot}

%changelog
* Thu May 23 2013 tanggeliang <tanggeliang@gmail.com>
- create
