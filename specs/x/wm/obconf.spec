Name:       obconf
Version:    2.0.3
Release:    1%{?dist}
Summary:    Openbox configuration tool

Group:      User Interface/Window Manager
License:    GPLv2+
Url:        http://openbox.org
Source:     http://openbox.org/dist/%{name}/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires: 	gtk2 openbox
BuildRequires:  autoconf automake m4

%description
Openbox configuration tool.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
sed -i 's/obrender-3.0/obrender-3.5/' configure.ac 
sed -i 's/obparser-3.0/obt-3.5/'      configure.ac
autoreconf -i &&
./configure --prefix=/usr --sysconfdir=/etc --disable-static \
  --docdir=%{_datadir}/doc/openbox-%{version} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

%files
%defattr(-,root,root,-)

%files doc
%defattr(-,root,root,-)

%post
update-mime-database /usr/share/mime
update-desktop-database /usr/share/applications

%clean
rm -rf %{buildroot}

%changelog
* Wed Jun 19 2013 tanggeliang <tanggeliang@gmail.com>
- create
