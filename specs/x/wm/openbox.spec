Name:       openbox
Version:    3.5.2
Release:    1%{?dist}
Summary:    openbox

Group:      User Interface/Window Manager
License:    GPLv2+
Url:        http://openbox.org
Source:     http://openbox.org/dist/%{name}/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires: 	libx11 libxcursor libice libsm libxrandr libxinerama
BuildRequires:  pango imlib2

%description
Openbox is a highly configurable desktop window manager with extensive standards support. It allows you to control almost every aspect of how you interact with your desktop.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr                            \
            --libexecdir=%{_libdir}/openbox          \
            --sysconfdir=/etc                        \
            --docdir=%{_docdir}/openbox-%{version}   \
            --disable-static                         \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

%files
%defattr(-,root,root,-)
/etc/xdg/openbox/autostart
/etc/xdg/openbox/environment
/etc/xdg/openbox/menu.xml
/etc/xdg/openbox/rc.xml
%{_bindir}/gdm-control
%{_bindir}/gnome-panel-control
%{_bindir}/obxprop
%{_bindir}/openbox
%{_bindir}/openbox-gnome-session
%{_bindir}/openbox-kde-session
%{_bindir}/openbox-session
%{_includedir}/openbox/3.5/obrender/*.h
%{_includedir}/openbox/3.5/obt/*.h
%{_libdir}/libobrender.la
%{_libdir}/libobrender.so*
%{_libdir}/libobt.la
%{_libdir}/libobt.so*
%{_libdir}/pkgconfig/obrender-3.5.pc
%{_libdir}/pkgconfig/obt-3.5.pc
%{_libdir}/openbox/openbox-autostart
%{_libdir}/openbox/openbox-xdg-autostart
%{_datadir}/applications/openbox.desktop
%{_datadir}/gnome/wm-properties/openbox.desktop
%{_datadir}/gnome-session/sessions/openbox-gnome*.session
%{_datadir}/locale/*/LC_MESSAGES/openbox.mo
%{_datadir}/pixmaps/openbox.png
%{_datadir}/themes/*/openbox-3/themerc
%{_datadir}/themes/Bear2/openbox-3/*.xbm
%{_datadir}/themes/Mikachu/openbox-3/*.xbm
%{_datadir}/themes/Natura/openbox-3/*.xbm
%{_datadir}/themes/Syscrash/openbox-3/*.xbm
%{_datadir}/xsessions/openbox*.desktop

%files doc
%defattr(-,root,root,-)
%{_docdir}/openbox-%{version}/*
%{_mandir}/man1/obxprop.1.gz
%{_mandir}/man1/openbox*.1.gz

%clean
rm -rf %{buildroot}

%changelog
* Thu Aug 22 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.5.0 to 3.5.2
* Thu Mar 14 2013 tanggeliang <tanggeliang@gmail.com>
- create
