Name:       libmatchbox
Version:    1.11
Release:    1%{?dist}
Summary:    libmatchbox

Group:      User Interface/Window Manager
License:    GPLv2+
Url:        http://matchbox-project.org
Source:     http://downloads.yoctoproject.org/releases/matchbox/%{name}/%{version}/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires: 	libxft libx11
BuildRequires:	pango libpng libjpeg-turbo

%description
Matchbox is an Open Source base environment for the X Window System running on non-desktop embedded platforms such as handhelds, set-top boxes, kiosks and anything else for which screen space, input mechanisms or system resources are limited.

%prep
%setup -q -n %{name}-%{version}

%build
export LDFLAGS+=-lX11
./configure --prefix=/usr \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

%files
%defattr(-,root,root,-)
%{_includedir}/libmb/hash.h
%{_includedir}/libmb/mb*.h
%{_libdir}/libmb.*a
%{_libdir}/libmb.so*
%{_libdir}/pkgconfig/libmb.pc

%clean
rm -rf %{buildroot}

%changelog
* Mon Nov 11 2013 tanggeliang <tanggeliang@gmail.com>
- create
