Name:       matchbox-window-manager
Version:    1.2
Release:    1%{?dist}
Summary:    matchbox-window-manager

Group:      User Interface/Window Manager
License:    GPLv2+
Url:        http://matchbox-project.org
Source:     http://downloads.yoctoproject.org/releases/matchbox/%{name}/%{version}/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires: 	libmatchbox

%description
Matchbox is an Open Source base environment for the X Window System running on non-desktop embedded platforms such as handhelds, set-top boxes, kiosks and anything else for which screen space, input mechanisms or system resources are limited.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --sysconfdir=/etc &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

%files
%defattr(-,root,root,-)
/etc/matchbox/kbdconfig
%{_bindir}/matchbox-remote
%{_bindir}/matchbox-window-manager
%{_datadir}/matchbox/mbnoapp.xpm
%{_datadir}/themes/Default/matchbox/*.xpm
%{_datadir}/themes/Default/matchbox/theme.desktop
%{_datadir}/themes/Default/matchbox/theme.xml
%{_datadir}/themes/MBOpus/matchbox/*.png
%{_datadir}/themes/MBOpus/matchbox/theme.desktop
%{_datadir}/themes/MBOpus/matchbox/theme.xml
%{_datadir}/themes/blondie/matchbox/*.png
%{_datadir}/themes/blondie/matchbox/theme.desktop
%{_datadir}/themes/blondie/matchbox/theme.xml

%clean
rm -rf %{buildroot}

%changelog
* Mon Nov 11 2013 tanggeliang <tanggeliang@gmail.com>
- create
