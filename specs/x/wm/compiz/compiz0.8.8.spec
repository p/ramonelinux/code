%global    core_plugins    blur clone cube decoration fade ini inotify minimize move place png regex resize rotate scale screenshot switcher video water wobbly zoom fs obs commands wall annotate svg matecompat
 
# List of plugins passed to ./configure.  The order is important
%global    plugins         core,png,svg,video,screenshot,decoration,clone,place,fade,minimize,move,resize,switcher,scale,wall,obs

Name:           compiz
URL:            http://www.compiz.org
License:        GPLv2+ and LGPLv2+ and MIT
Group:          User Interface/Desktops
Version:        0.8.8
Release:        1%{?dist}
Epoch:          1
Summary:        OpenGL window and compositing manager
 
Requires:       system-logos
Requires:       glx-utils
# this is an inverse require which is needed for build without gtk-windows-decorator
Requires:       emerald
Requires:       hicolor-icon-theme

BuildRequires: libx11 libxfixes libxrandr libxrender libxcomposite libxdamage libxext libxt libsm libice libxmu
BuildRequires: libdrm
BuildRequires: desktop-file-utils
#BuildRequires: libmatewnck
BuildRequires: intltool
BuildRequires: gettext
BuildRequires: librsvg
BuildRequires: mesalib
#BuildRequires: fuse
BuildRequires: cairo
BuildRequires: libtool
BuildRequires: libxslt
#BuildRequires: mate-window-manager
 
Source0:       http://releases.compiz.org/%{version}/%{name}-%{version}.tar.bz2
Source1:       compiz-mate-gtk
Source2:       compiz-mate-gtk.desktop
Source3:       compiz-mate-emerald
Source4:       compiz-mate-emerald.desktop
Source5:       compiz-lxde-emerald
Source6:       compiz-lxde-emerald.desktop
Source7:       compiz-xfce-emerald
Source8:       compiz-xfce-emerald.desktop
Source9:       compiz-decorator-gtk
Source10:      gtk-decorator.desktop
Source11:      compiz-decorator-emerald
Source12:      emerald-decorator.desktop
Source13:      compiz-plugins-main_plugin-matecompat.svg
Source14:      emerald-decorator.svg
Source15:      gtk-decorator.svg

%description
Compiz is one of the first OpenGL-accelerated compositing window
managers for the X Window System. The integration allows it to perform
compositing effects in window management, such as a minimization
effect and a cube work space. Compiz is an OpenGL compositing manager
that use Compiz use EXT_texture_from_pixmap OpenGL extension for
binding redirected top-level windows to texture objects.
 
%package devel
Summary: Development packages for compiz
Group: Development/Libraries
Requires: %{name}%{?_isa} = %{epoch}:%{version}-%{release}
Requires: pkgconfig
Requires: libXcomposite-devel libXfixes-devel libXdamage-devel libXrandr-devel
Requires: libXinerama-devel libICE-devel libSM-devel libxml2-devel
Requires: libxslt-devel startup-notification-devel
 
%description devel
The compiz-devel package includes the header files,
and developer docs for the compiz package.
Install compiz-devel if you want to develop plugins for the compiz
windows and compositing manager.

%package mate
Summary: Compiz mate integration bits
Group: User Interface/Desktops
Requires: %{name}%{?_isa} = %{epoch}:%{version}-%{release}
 
%description mate
The compiz-mate package contains the matecompat plugin
and start scripts to start Compiz with emerald and
gtk-windows-decorator.

%package xfce
Summary: Compiz xfce integration bits
Group: User Interface/Desktops
Requires: %{name}%{?_isa} = %{epoch}:%{version}-%{release}
 
%description xfce
The compiz-xfce package contains a start script to start
Compiz with emerald.

%package lxde
Summary: Compiz lxde integration bits
Group: User Interface/Desktops
Requires: %{name}%{?_isa} = %{epoch}:%{version}-%{release}
 
%description lxde
The compiz-lxde package contains a start script to start
Compiz with emerald.

 
%prep
%setup -q
 
%build
autoreconf -f -i

%configure \
    --enable-librsvg \
    --enable-gtk \
    --enable-marco \
    --enable-mate \
    --with-default-plugins=%{plugins}
 
make %{?_smp_mflags} imagedir=%{_datadir}/pixmaps
 
%install
make DESTDIR=$RPM_BUILD_ROOT install || exit 1

install %SOURCE1 $RPM_BUILD_ROOT%{_bindir}
install %SOURCE3 $RPM_BUILD_ROOT%{_bindir}
install %SOURCE5 $RPM_BUILD_ROOT%{_bindir}
install %SOURCE7 $RPM_BUILD_ROOT%{_bindir}
install %SOURCE9 $RPM_BUILD_ROOT%{_bindir}
install %SOURCE11 $RPM_BUILD_ROOT%{_bindir}

desktop-file-install --vendor="" \
  --dir $RPM_BUILD_ROOT%{_datadir}/applications \
  %SOURCE2
desktop-file-install --vendor="" \
  --dir $RPM_BUILD_ROOT%{_datadir}/applications \
  %SOURCE4
desktop-file-install --vendor="" \
  --dir $RPM_BUILD_ROOT%{_datadir}/applications \
  %SOURCE6
desktop-file-install --vendor="" \
  --dir $RPM_BUILD_ROOT%{_datadir}/applications \
  %SOURCE8
desktop-file-install --vendor="" \
  --dir $RPM_BUILD_ROOT%{_datadir}/applications \
  %SOURCE10
desktop-file-install --vendor="" \
  --dir $RPM_BUILD_ROOT%{_datadir}/applications \
  %SOURCE12

# matecompat icon
mkdir -p $RPM_BUILD_ROOT%{_datadir}/icons/hicolor/scalable/apps
cp -f %SOURCE13 $RPM_BUILD_ROOT%{_datadir}/icons/hicolor/scalable/apps/plugin-matecompat.svg
# emerald-decorator icon
cp -f %SOURCE14 $RPM_BUILD_ROOT%{_datadir}/icons/hicolor/scalable/apps/emerald-decorator.svg
# gtk-decorator icon
cp -f %SOURCE15 $RPM_BUILD_ROOT%{_datadir}/icons/hicolor/scalable/apps/gtk-decorator.svg

%find_lang %{name}
 
cat %{name}.lang > core-files.txt
 
for f in %{core_plugins}; do
  echo %{_libdir}/compiz/lib$f.so
  echo %{_datadir}/compiz/$f.xml
done >> core-files.txt
 
%post
/sbin/ldconfig
/bin/touch --no-create %{_datadir}/compiz &>/dev/null || :
/bin/touch --no-create %{_datadir}/icons/hicolor &>/dev/null || :
 
%postun
/sbin/ldconfig
if [ $1 -eq 0 ] ; then
    /bin/touch --no-create %{_datadir}/compiz &>/dev/null
    /usr/bin/gtk-update-icon-cache %{_datadir}/compiz &>/dev/null || :
    /bin/touch --no-create %{_datadir}/icons/hicolor &>/dev/null
    /usr/bin/gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
fi

%posttrans
/usr/bin/gtk-update-icon-cache %{_datadir}/compiz &>/dev/null || :
/usr/bin/gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :

%post mate -p /sbin/ldconfig

%postun mate -p /sbin/ldconfig

%files -f core-files.txt
%doc AUTHORS ChangeLog COPYING.GPL COPYING.LGPL README TODO NEWS
%{_bindir}/compiz
%{_libdir}/libdecoration.so.*
%dir %{_libdir}/compiz
%dir %{_datadir}/compiz
%{_datadir}/compiz/*.png
%{_datadir}/compiz/core.xml
%{_datadir}/icons/hicolor/scalable/apps/*.svg

%files mate
%{_bindir}/gtk-window-decorator
%{_bindir}/compiz-mate-emerald
%{_bindir}/compiz-mate-gtk
%{_bindir}/compiz-decorator-gtk
%{_bindir}/compiz-decorator-emerald
%{_datadir}/applications/compiz-mate-emerald.desktop
%{_datadir}/applications/compiz-mate-gtk.desktop
%{_datadir}/applications/gtk-decorator.desktop
%{_datadir}/applications/emerald-decorator.desktop

%files xfce
%{_bindir}/compiz-xfce-emerald
%{_datadir}/applications/compiz-xfce-emerald.desktop

%files lxde
%{_bindir}/compiz-lxde-emerald
%{_datadir}/applications/compiz-lxde-emerald.desktop
 
%files devel
%{_libdir}/pkgconfig/compiz.pc
%{_libdir}/pkgconfig/libdecoration.pc
%{_libdir}/pkgconfig/compiz-cube.pc
%{_libdir}/pkgconfig/compiz-scale.pc
%{_includedir}/compiz/
%{_libdir}/libdecoration.so

%changelog
