Name:       compiz
Version:    0.8.8
Release:    1%{?dist}
Summary:    OpenGL window and compositing manager

Group:      User Interface/Desktops
License:    GPLv2+ and LGPLv2+ and MIT
URL:        http://www.compiz.org
Source:     http://releases.compiz.org/%{version}/%{name}-%{version}.tar.bz2
 
BuildRequires:  libx11 libxfixes libxrandr libxrender libxcomposite
BuildRequires:  libxdamage libxext libxt libsm libice libxmu
BuildRequires:  libdrm mesalib cairo
BuildRequires:  desktop-file-utils
BuildRequires:  intltool gettext librsvg libtool libxslt
#BuildRequires: libmatewnck
#BuildRequires: fuse
#BuildRequires: mate-window-manager
Requires:       glx-utils
Requires:       emerald
Requires:       hicolor-icon-theme

%description
Compiz is one of the first OpenGL-accelerated compositing window managers for the X Window System.
The integration allows it to perform compositing effects in window management, such as a minimization effect and a cube work space.
Compiz is an OpenGL compositing manager that use Compiz use EXT_texture_from_pixmap OpenGL extension for binding redirected top-level windows to texture objects.
 
%prep
%setup -q
 
%build
autoreconf -f -i

%configure \
    --enable-librsvg \
    --enable-gtk \
    --enable-marco \
    --enable-mate \
    --with-default-plugins=%{plugins}
 
make %{?_smp_mflags} imagedir=%{_datadir}/pixmaps
 
%install
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
/etc/gconf/schemas/compiz-*.schemas
/etc/gconf/schemas/gwd.schemas
%{_bindir}/compiz
%{_bindir}/compiz-decorator-emerald
%{_bindir}/compiz-decorator-gtk
%{_bindir}/compiz-lxde-emerald
%{_bindir}/compiz-mate-emerald
%{_bindir}/compiz-mate-gtk
%{_bindir}/compiz-xfce-emerald
%{_bindir}/gtk-window-decorator
%{_includedir}/compiz/compiz-*.h
%{_includedir}/compiz/compiz.h
%{_includedir}/compiz/decoration.h
%{_libdir}/compiz/libannotate.*
%{_libdir}/compiz/libblur.*
%{_libdir}/compiz/libclone.*
%{_libdir}/compiz/libcommands.*
%{_libdir}/compiz/libcube.*
%{_libdir}/compiz/libdbus.*
%{_libdir}/compiz/libdecoration.*
%{_libdir}/compiz/libfade.*
%{_libdir}/compiz/libgconf.*
%{_libdir}/compiz/libglib.*
%{_libdir}/compiz/libgnomecompat.*
%{_libdir}/compiz/libini.*
%{_libdir}/compiz/libinotify.*
%{_libdir}/compiz/libminimize.*
%{_libdir}/compiz/libmove.*
%{_libdir}/compiz/libobs.*
%{_libdir}/compiz/libplace.*
%{_libdir}/compiz/libpng.*
%{_libdir}/compiz/libregex.*
%{_libdir}/compiz/libresize.*
%{_libdir}/compiz/librotate.*
%{_libdir}/compiz/libscale.*
%{_libdir}/compiz/libscreenshot.*
%{_libdir}/compiz/libsvg.*
%{_libdir}/compiz/libswitcher.*
%{_libdir}/compiz/libvideo.*
%{_libdir}/compiz/libwater.*
%{_libdir}/compiz/libwobbly.*
%{_libdir}/compiz/libzoom.*
%{_libdir}/libdecoration.*
%{_libdir}/pkgconfig/compiz-cube.pc
%{_libdir}/pkgconfig/compiz-gconf.pc
%{_libdir}/pkgconfig/compiz-scale.pc
%{_libdir}/pkgconfig/compiz.pc
%{_libdir}/pkgconfig/libdecoration.pc
%{_datadir}/applications/compiz-lxde-emerald.desktop
%{_datadir}/applications/compiz-mate-emerald.desktop
%{_datadir}/applications/compiz-mate-gtk.desktop
%{_datadir}/applications/compiz-xfce-emerald.desktop
%{_datadir}/applications/emerald-decorator.desktop
%{_datadir}/applications/gtk-decorator.desktop
%{_datadir}/compiz/*.xml
%{_datadir}/compiz/freedesktop.png
%{_datadir}/compiz/icon.png
%{_datadir}/compiz/schemas.xslt
%{_datadir}/icons/hicolor/scalable/apps/emerald-decorator.svg
%{_datadir}/icons/hicolor/scalable/apps/gtk-decorator.svg
%{_datadir}/icons/hicolor/scalable/apps/plugin-matecompat.svg
%{_datadir}/locale/*/LC_MESSAGES/compiz.mo
%{_docdir}/compiz-%{version}/*

%changelog
* Thu Jun 20 2013 tanggeliang <tanggeliang@gmail.com>
- create
