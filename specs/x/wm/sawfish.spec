Name:       sawfish
Version:    1.9.1
Release:    1%{?dist}
Summary:    sawfish

Group:      User Interface/Window Manager
License:    GPLv2+
Url:        http://sawfish.tuxfamily.org
Source:     http://download.tuxfamily.org/%{name}/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires: 	rep-gtk which
BuildRequires:  gtk2 pango

%description
The sawfish package contains a window manager. This is useful for organizing and displaying windows where all window decorations are configurable and all user-interface policy is controlled through the extension language.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --with-pango  &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

%files
%defattr(-,root,root,-)

%files doc
%defattr(-,root,root,-)

%clean
rm -rf %{buildroot}

%changelog
* Thu Mar 14 2013 tanggeliang <tanggeliang@gmail.com>
- create
