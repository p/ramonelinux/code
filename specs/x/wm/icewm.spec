Name:       icewm
Version:    1.3.7
Release:    1%{?dist}
Summary:    IceWM

Group:      User Interface/Window Manager
License:    GPLv2+
Url:        http://icewm.sourceforge.net
Source:     http://downloads.sourceforge.net/%{name}/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires: 	libx11 gdk-pixbuf esound

%description
IceWM is a window manager with the goals of speed, simplicity, and not getting in the user's way.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
sed -i '/^LIBS/s/\(.*\)/\1 -lfontconfig/' src/Makefile.in &&
sed -i 's/define deprecated/define ICEWM_deprecated/' src/base.h &&
./configure --prefix=/usr &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot} &&
make install-docs DESTDIR=%{buildroot} &&
make install-man DESTDIR=%{buildroot} &&
make install-desktop DESTDIR=%{buildroot}

%files
%defattr(-,root,root,-)
/usr/X11R6/bin/icehelp
/usr/X11R6/bin/icesh
/usr/X11R6/bin/icewm
/usr/X11R6/bin/icewm-session
/usr/X11R6/bin/icewm-set-gnomewm
/usr/X11R6/bin/icewmbg
/usr/X11R6/bin/icewmhint
/usr/X11R6/bin/icewmtray
/usr/share/icewm/icons/*.xpm
/usr/share/icewm/keys
/usr/share/icewm/ledclock/*.xpm
/usr/share/icewm/mailbox/*.xpm
/usr/share/icewm/menu
/usr/share/icewm/preferences
/usr/share/icewm/taskbar/*.xpm
/usr/share/icewm/themes/Infadel2/*
/usr/share/icewm/themes/gtk2/*
/usr/share/icewm/themes/icedesert/*
/usr/share/icewm/themes/metal2/*
/usr/share/icewm/themes/motif/*
/usr/share/icewm/themes/nice/*
/usr/share/icewm/themes/nice2/*
/usr/share/icewm/themes/warp3/*
/usr/share/icewm/themes/warp4/*
/usr/share/icewm/themes/win95/*
/usr/share/icewm/themes/yellowmotif/*
/usr/share/icewm/toolbar
/usr/share/icewm/winoptions

%files doc
%defattr(-,root,root,-)

%clean
rm -rf %{buildroot}

%changelog
* Thu Mar 14 2013 tanggeliang <tanggeliang@gmail.com>
- create
