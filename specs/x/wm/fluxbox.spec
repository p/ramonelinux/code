Name:       fluxbox
Version:    1.3.2
Release:    1%{?dist}
Summary:    Fluxbox

Group:      User Interface/Window Manager
License:    GPLv2+
Url:        http://fluxbox.sourceforge.net
Source:     http://downloads.sourceforge.net/%{name}/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires: 	libx11

%description
The Fluxbox package contains a window manager.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

%files
%defattr(-,root,root,-)
%{_bindir}/fbrun
%{_bindir}/fbsetbg
%{_bindir}/fbsetroot
%{_bindir}/fluxbox
%{_bindir}/fluxbox-generate_menu
%{_bindir}/fluxbox-remote
%{_bindir}/fluxbox-update_configs
%{_bindir}/startfluxbox
%{_datadir}/fluxbox/apps
%{_datadir}/fluxbox/init
%{_datadir}/fluxbox/keys
%{_datadir}/fluxbox/menu
%{_datadir}/fluxbox/overlay
%{_datadir}/fluxbox/styles/*
%{_datadir}/fluxbox/windowmenu

%files doc
%defattr(-,root,root,-)
%{_mandir}/man*/*.gz

%clean
rm -rf %{buildroot}

%changelog
* Thu Mar 14 2013 tanggeliang <tanggeliang@gmail.com>
- create
