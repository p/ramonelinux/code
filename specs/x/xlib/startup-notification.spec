Name:       startup-notification
Version:    0.12
Release:    6%{?dist}
Summary:    startup-notification

Group:      System Environment/xlib
License:    GPLv2+
Url:        http://www.freedesktop.org
Source:     http://www.freedesktop.org/software/startup-notification/releases/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  libx11
BuildRequires:  xcb-util

%description
The startup-notification package contains startup-notification libraries.
These are useful for building a consistent manner to notify the user through the cursor that the application is loading.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_includedir}/startup-notification-1.0/libsn/sn*.h
%{_libdir}/libstartup-notification-1.*
%{_libdir}/pkgconfig/libstartup-notification-1.0.pc

%clean
rm -rf %{buildroot}

%changelog
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
