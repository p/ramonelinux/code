Name:       shared-mime-info
Version:    1.3
Release:    1%{?dist}
Summary:    shared-mime-info

Group:      System Environment/xlib
License:    GPLv2+
Url:        http://ftp.gnome.org
Source:     http://freedesktop.org/~hadess/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  glib intltool libxml
BuildRequires:  gettext xml-parser

%description
The shared-mime-info package contains a MIME database. This allows central updates of MIME information for all supporting applications.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr &&
make -j1

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/update-mime-database
%{_datadir}/locale/*
%{_datadir}/mime/*
%{_datadir}/pkgconfig/shared-mime-info.pc
%{_mandir}/*/*

%clean
rm -rf %{buildroot}

%changelog
* Wed Jul 23 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.2 to 1.3
* Mon Oct 7 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.1 to 1.2
* Sun Mar 17 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.0 to 1.1
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
