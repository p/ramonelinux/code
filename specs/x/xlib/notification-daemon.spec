Name:       notification-daemon
Version:    3.16.0
Release:    1%{?dist}
Summary:    Notification Daemon

Group:      System Environment/xlib
License:    GPLv2+
Url:        http://ftp.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/%{name}/3.16/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  gtk+ intltool libcanberra
BuildRequires:  gettext xml-parser

%description
The Notification Daemon package contains a daemon that displays passive pop-up notifications.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --sysconfdir=/etc \
            --libexecdir=%{_libdir}/notification-daemon &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
/etc/xdg/autostart/notification-daemon-autostart.desktop
%{_libdir}/notification-daemon/notification-daemon
%{_datadir}/applications/notification-daemon.desktop
%{_datadir}/locale/*

%clean
rm -rf %{buildroot}

%changelog
* Sun Mar 29 2015 tanggeliang <tanggeliang@gmail.com>
- update from 0.7.6 to 3.16.0
* Sun Oct 14 2012 tanggeliang <tanggeliang@gmail.com>
- update from 0.7.5 to 0.7.6
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
