Name:       clutter-gtk
Version:    1.6.4
Release:    1%{?dist}
Summary:    Clutter Gtk

Group:      System Environment/xlib
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/%{name}/1.6/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  clutter gtk+
BuildRequires:  gobject-introspection
BuildRequires:  mesa gettext clutter-wayland

%description
The Clutter Gtk package is a library providing facilities to integrate Clutter into GTK+ applications.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_includedir}/clutter-gtk-1.0/clutter-gtk/*.h
%{_libdir}/girepository-1.0/GtkClutter-1.0.typelib
%{_libdir}/libclutter-gtk-1.0.*
%{_libdir}/pkgconfig/clutter-gtk-1.0.pc
%{_datadir}/gir-1.0/GtkClutter-1.0.gir
%{_datadir}/locale/*/LC_MESSAGES/cluttergtk-1.0.mo

%files doc
%defattr(-,root,root,-)
%{_datadir}/gtk-doc/html/clutter-gtk-1.0/*

%clean
rm -rf %{buildroot}

%changelog
* Wed Sep 23 2015 tanggeliang <tanggeliang@gmail.com>
- update from 1.6.2 to 1.6.4
* Wed Sep 24 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.5.2 to 1.6.0
* Sun Mar 16 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.4.4 to 1.5.2
* Fri May 10 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.4.2 to 1.4.4
* Tue Mar 19 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.4.0 to 1.4.2
* Mon Oct 22 2012 tanggeliang <tanggeliang@gmail.com>
- update from 1.3.2 to 1.4.0
* Fri Sep 28 2012 tanggeliang <tanggeliang@gmail.com>
- update from 1.2.0 to 1.3.2
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
