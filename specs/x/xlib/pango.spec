Name:       pango
Version:    1.38.0
Release:    1%{?dist}
Summary:    Pango

Group:      System Environment/xlib
License:    GPLv2+
Url:        http://ftp.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/%{name}/1.38/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  cairo harfbuzz fontconfig
BuildRequires:  libx11 libxft
BuildRequires:  gobject-introspection gtk-doc

%description
Pango is a library for laying out and rendering of text, with an emphasis on internationalization.
It can be used anywhere that text layout is needed, though most of the work on Pango so far has been done in the context of the GTK+ widget toolkit.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --sysconfdir=/etc \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_includedir}/pango-1.0/pango/pango*.h
%{_bindir}/pango-view
%{_libdir}/girepository-1.0/Pango*-1.0.typelib
%{_libdir}/libpango-1.0.la
%{_libdir}/libpango-1.0.so*
%{_libdir}/libpangocairo-1.0.la
%{_libdir}/libpangocairo-1.0.so*
%{_libdir}/libpangoft2-1.0.la
%{_libdir}/libpangoft2-1.0.so*
%{_libdir}/libpangoxft-1.0.la
%{_libdir}/libpangoxft-1.0.so*
%{_libdir}/pkgconfig/pango.pc
%{_libdir}/pkgconfig/pangocairo.pc
%{_libdir}/pkgconfig/pangoft2.pc
%{_libdir}/pkgconfig/pangoxft.pc
%{_datadir}/gir-1.0/Pango*-1.0.gir

%files doc
%defattr(-,root,root,-)
%{_datadir}/gtk-doc/html/pango/*
%{_mandir}/*/*

%clean
rm -rf %{buildroot}

%changelog
* Tue Sep 22 2015 tanggeliang <tanggeliang@gmail.com>
- update from 1.37.5 to 1.38.0
* Tue Sep 15 2015 tanggeliang <tanggeliang@gmail.com>
- update from 1.36.8 to 1.37.5
- no more pango-querymodules in '/usr/bin',
- remove 'pango-querymodules --update-cache' from '%post'.
* Tue Sep 23 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.36.5 to 1.36.8
* Sun Jul 27 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.36.3 to 1.36.5
* Thu Mar 20 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.36.1 to 1.36.3
* Thu Dec 5 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.36.0 to 1.36.1
* Tue Sep 24 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.34.1 to 1.36.0
* Thu Jul 25 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.34.0 to 1.34.1
* Thu Apr 11 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.32.5 to 1.34.0
* Sun Mar 17 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.32.1 to 1.32.5
* Fri Sep 28 2012 tanggeliang <tanggeliang@gmail.com>
- update from 1.30.1 to 1.32.1
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
