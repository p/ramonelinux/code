Name:       qt
Version:    5.1.1
Release:    1%{?dist}
Summary:    Qt

Group:      System Environment/xlib
License:    GPLv2+
Url:        http://www.qt.org
Source:     http://download.qt-project.org/official_releases/qt/5.1/%{version}/single/%{name}-everywhere-opensource-src-%{version}.tar.xz
Patch:      %{name}-%{version}-xcb-193.patch

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  alsa-lib mesalib qtchooser xcb-util-image xcb-util-keysyms xcb-util-renderutil xcb-util-wm
BuildRequires:  ca-certificates cups dbus glib gst-plugins-base icu libjpeg-turbo libmng libpng libtiff openssl pcre sqlite
BuildRequires:  libx11 libxinerama libxcursor libxrandr
BuildRequires:  pulseaudio

%description
Qt is a cross-platform application framework that is widely used for developing application software with a graphical user interface (GUI) (in which cases Qt is classified as a widget toolkit), and also used for developing non-GUI programs such as command-line tools and consoles for servers.
One of the major users of Qt is KDE.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%package        examples
Summary:        examples
%description    examples

%prep
%setup -q -n %{name}-everywhere-opensource-src-%{version}
%patch -p1

%build
sed -i "s:Context\* context}:&\n%lex-param {YYLEX_PARAM}:" \
     qtwebkit/Source/ThirdParty/ANGLE/src/compiler/glslang.y &&

sed -i -e "/#if/d" -e "/#error/d" -e "/#endif/d" \
     qtimageformats/config.tests/libmng/libmng.cpp &&

./configure -prefix /usr                \
            -sysconfdir /etc/xdg        \
            -bindir %{_libdir}/qt5/bin  \
            -libdir %{_libdir}          \
            -headerdir /usr/include/qt5 \
            -archdatadir %{_libdir}/qt5 \
            -datadir %{_datadir}/qt5     \
            -docdir %{_datadir}/doc/qt5  \
            -translationdir %{_datadir}/qt5/translations \
            -examplesdir %{_datadir}/doc/qt5/examples    \
            -confirm-license   \
            -opensource        \
            -dbus-linked       \
            -openssl-linked    \
            -system-sqlite     \
            -plugin-sql-sqlite \
            -no-nis            \
            -opengl es2        \
            -optimized-qmake   &&
make %{?_smp_mflags}
find . -name "*.pc" -exec perl -pi -e "s, -L$PWD/?\S+,,g" {} \;

%check

%install
rm -rf %{buildroot}
make install INSTALL_ROOT=%{buildroot} &&

mkdir -p %{buildroot}%{_datadir}/pixmaps &&
install -v -Dm644 qttools/src/assistant/assistant/images/assistant-128.png \
                  %{buildroot}%{_datadir}/pixmaps/assistant-qt5.png &&
install -v -Dm644 qttools/src/designer/src/designer/images/designer.png \
                  %{buildroot}%{_datadir}/pixmaps/designer-qt5.png &&
install -v -Dm644 qttools/src/linguist/linguist/images/icons/linguist-128-32.png \
                  %{buildroot}%{_datadir}/pixmaps/linguist-qt5.png &&
install -v -Dm644 qttools/src/qdbus/qdbusviewer/images/qdbusviewer-128.png \
                  %{buildroot}%{_datadir}/pixmaps/qdbusviewer-qt5.png

sed -i -e "s:$PWD/qtbase:/usr/lib/qt5:g" \
    %{buildroot}/%{_libdir}/qt5/mkspecs/modules/qt_lib_bootstrap.pri &&
find %{buildroot}/%{_libdir}/*.prl -exec sed -i -e \
     '/^QMAKE_PRL_BUILD_DIR/d;s/\(QMAKE_PRL_LIBS =\).*/\1/' {} \;

mkdir -p %{buildroot}/usr/bin &&
for file in %{buildroot}/%{_libdir}/qt5/bin/*
do
  ln -sfv ../%{_lib}/qt5/bin/$(basename $file) %{buildroot}/usr/bin/$(basename $file)-qt5
done

install -dm755 %{buildroot}%{_datadir}/applications
cat > %{buildroot}%{_datadir}/applications/assistant-qt5.desktop << "EOF"
[Desktop Entry]
Name=Qt5 Assistant 
Comment=Shows Qt5 documentation and examples
Exec=assistant-qt5
Icon=assistant-qt5
Terminal=false
Encoding=UTF-8
Type=Application
Categories=Qt;Development;Documentation;
EOF
cat > %{buildroot}%{_datadir}/applications/designer-qt5.desktop << "EOF"
[Desktop Entry]
Name=Qt5 Designer
GenericName=Interface Designer
Comment=Design GUIs for Qt5 applications
Exec=designer-qt5
Icon=designer-qt5
MimeType=application/x-designer;
Terminal=false
Encoding=UTF-8
Type=Application
Categories=Qt;Development;
EOF
cat > %{buildroot}%{_datadir}/applications/linguist-qt5.desktop << "EOF"
[Desktop Entry]
Name=Qt5 Linguist
Comment=Add translations to Qt5 applications
Exec=linguist-qt5
Icon=linguist-qt5
MimeType=text/vnd.trolltech.linguist;application/x-linguist;
Terminal=false
Encoding=UTF-8
Type=Application
Categories=Qt;Development;
EOF
cat > %{buildroot}%{_datadir}/applications/qdbusviewer-qt5.desktop << "EOF"
[Desktop Entry]
Name=Qt5 QDbusViewer 
GenericName=D-Bus Debugger
Comment=Debug D-Bus applications
Exec=qdbusviewer-qt5
Icon=qdbusviewer-qt5
Terminal=false
Encoding=UTF-8
Type=Application
Categories=Qt;Development;Debugger;
EOF

%files
%defattr(-,root,root,-)
%{_bindir}/*
%{_libdir}/qt5/bin/assistant
%{_libdir}/qt5/bin/designer
%{_libdir}/qt5/bin/lconvert
%{_libdir}/qt5/bin/linguist
%{_libdir}/qt5/bin/lrelease
%{_libdir}/qt5/bin/lupdate
%{_libdir}/qt5/bin/moc
%{_libdir}/qt5/bin/pixeltool
%{_libdir}/qt5/bin/qcollectiongenerator
%{_libdir}/qt5/bin/qdbus
%{_libdir}/qt5/bin/qdbuscpp2xml
%{_libdir}/qt5/bin/qdbusviewer
%{_libdir}/qt5/bin/qdbusxml2cpp
%{_libdir}/qt5/bin/qhelpconverter
%{_libdir}/qt5/bin/qhelpgenerator
%{_libdir}/qt5/bin/qmake
%{_libdir}/qt5/bin/qmlplugindump
%{_libdir}/qt5/bin/qmlviewer
%{_libdir}/qt5/bin/rcc
%{_libdir}/qt5/bin/uic
%{_libdir}/qt5/bin/xmlpatterns
%{_libdir}/qt5/bin/xmlpatternsvalidator
%{_libdir}/qt5/bin/qdoc
%{_libdir}/qt5/bin/qml1plugindump
%{_libdir}/qt5/bin/qmlbundle
%{_libdir}/qt5/bin/qmlmin
%{_libdir}/qt5/bin/qmlprofiler
%{_libdir}/qt5/bin/qmlscene
%{_libdir}/qt5/bin/qmltestrunner
%{_libdir}/qt5/bin/syncqt.pl
%{_includedir}/qt5/Qt*/*
%{_libdir}/qt5/imports/Qt*/*
%{_libdir}/libQt*.*
%{_libdir}/pkgconfig/Qt*.pc
%{_libdir}/cmake/*
%{_libdir}/qt5/mkspecs/*
%{_libdir}/qt5/imports/builtins.qmltypes
%{_libdir}/qt5/plugins/*
%{_libdir}/qt5/qml/*
%{_datadir}/applications/*-qt5.desktop
%{_datadir}/pixmaps/*.png
%{_datadir}/qt5/phrasebooks/*
%{_datadir}/qt5/translations/*.qm

%files doc
%defattr(-,root,root,-)
%{_docdir}/qt5/global/*

%files examples
%defattr(-,root,root,-)
%{_docdir}/qt5/examples/*

%clean
rm -rf %{buildroot}

%changelog
* Fri Dec 6 2013 tanggeliang <tanggeliang@gmail.com>
- update from 5.1.0 to 5.1.1
* Mon Aug 19 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.8.5 to 5.1.0
* Fri Jul 26 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.8.4 to 4.8.5
* Wed Mar 13 2013 tanggeliang <tanggeliang@gmail.com>
- remove BuildRequires - glib gtk2
* Wed Feb 27 2013 tanggeliang <tanggeliang@gmail.com>
- remove "-nomake examples -nomake demos" to build Qt examples and demos
* Sat Feb 23 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.8.2 to 4.8.4
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
