Name:       libnotify
Version:    0.7.6
Release:    1%{?dist}
Summary:    libnotify

Group:      System Environment/xlib
License:    GPLv2+
Url:        http://ftp.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/%{name}/0.7/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  gtk+ gobject-introspection

%description
The libnotify library is used to send desktop notifications to a notification daemon, as defined in the Desktop Notifications spec.
These notifications can be used to inform the user about an event or display some form of information without getting in the user's way.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --disable-static \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/notify-send
%{_includedir}/libnotify
%{_libdir}/girepository-1.0/Notify-0.7.typelib
%{_libdir}/libnotify.*
%{_libdir}/pkgconfig/libnotify.pc
%{_datadir}/gir-1.0/Notify-0.7.gir

%files doc
%defattr(-,root,root,-)
%{_datadir}/gtk-doc/html/libnotify/*

%clean
rm -rf %{buildroot}

%changelog
* Wed Oct 2 2013 tanggeliang <tanggeliang@gamil.com>
- update from 0.7.5 to 0.7.6
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
