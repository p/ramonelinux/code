Name:       gtkmm
Version:    3.17.90
Release:    1%{?dist}
Summary:    Gtkmm

Group:      System Environment/xlib
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/%{name}/3.17/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  atkmm, gtk+, pangomm
Requires:       %{name}-libgdkmm %{name}-libgtkmm

%description
The Gtkmm package provides a C++ interface to GTK+ 3.

%package        libgdkmm
Summary:        libgdkmm
%description    libgdkmm

%package        libgtkmm
Summary:        libgtkmm
%description    libgtkmm

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_includedir}/gdkmm-3.0/gdkmm.h
%{_includedir}/gtkmm-3.0/gtkmm.h
%{_includedir}/gdkmm-3.0/gdkmm/*.h
%{_includedir}/gtkmm-3.0/gtkmm/*.h
%{_includedir}/gdkmm-3.0/gdkmm/private/*.h
%{_includedir}/gtkmm-3.0/gtkmm/private/*.h
%{_libdir}/gdkmm-3.0/include/gdkmmconfig.h
%{_libdir}/gtkmm-3.0/include/gtkmmconfig.h
%{_libdir}/gtkmm-3.0/proc/m4/*.m4
%{_libdir}/libgdkmm-3.0.la
%{_libdir}/libgtkmm-3.0.la
%{_libdir}/pkgconfig/gdkmm-3.0.pc
%{_libdir}/pkgconfig/gtkmm-3.0.pc

%files libgdkmm
%defattr(-,root,root,-)
%{_libdir}/libgdkmm-3.0.so*

%files libgtkmm
%defattr(-,root,root,-)
%{_libdir}/libgtkmm-3.0.so*

%files doc
%defattr(-,root,root,-)
%{_docdir}/gtkmm-3.0/*
%{_datadir}/devhelp/books/gtkmm-3.0/gtkmm-3.0.devhelp2

%clean
rm -rf %{buildroot}

%changelog
* Sun Sep 20 2015 tanggeliang <tanggeliang@gmail.com>
- update from 3.10.1 to 3.17.90
* Thu Dec 5 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.10.0 to 3.10.1
* Tue Oct 22 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.9.16 to 3.10.0
* Tue Oct 8 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.8.1 to 3.9.16
* Mon Jul 29 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.7.12 to 3.8.1
* Sun Apr 14 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.0 to 3.7.12
* Sat Nov 3 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.4.0 to 3.6.0
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
