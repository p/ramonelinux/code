Name:       atk
Version:    2.18.0
Release:    1%{?dist}
Summary:    ATK

Group:      System Environment/xlib
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/%{name}/2.18/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  glib gettext gtk-doc
BuildRequires:  gobject-introspection

%description
ATK provides the set of accessibility interfaces that are implemented by other toolkits and applications.
Using the ATK interfaces, accessibility tools have full access to view and control running applications.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_includedir}/atk-1.0/atk/atk*.h
%{_libdir}/libatk-1.0.*
%{_libdir}/pkgconfig/atk.pc
%{_libdir}/girepository-1.0/Atk-1.0.typelib
%{_datadir}/locale/*
%{_datadir}/gir-1.0/Atk-1.0.gir

%files doc
%defattr(-,root,root,-)
%{_datadir}/gtk-doc/html/atk/*

%clean
rm -rf %{buildroot}

%changelog
* Tue Sep 22 2015 tanggeliang <tanggeliang@gmail.com>
- update from 2.17.90 to 2.18.0
* Tue Sep 15 2015 tanggeliang <tanggeliang@gmail.com>
- update from 2.16.0 to 2.17.90
* Sat Mar 28 2015 tanggeliang <tanggeliang@gmail.com>
- update from 2.12.0 to 2.16.0
* Thu Mar 27 2014 tanggeliang <tanggeliang@gmail.com>
- update from 2.10.0 to 2.12.0
* Tue Sep 24 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.8.0 to 2.10.0
* Thu Apr 11 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.6.0 to 2.8.0
* Fri Sep 28 2012 tanggeliang <tanggeliang@gmail.com>
- update from 2.4.0 to 2.6.0
- add "BuildRequires gobject-introspection", build libunique need.
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
