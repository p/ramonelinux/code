Name:       at-spi2-atk
Version:    2.18.0
Release:    1%{?dist}
Summary:    At-Spi2 Atk

Group:      User Interface/GNOME
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/%{name}/2.18/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  at-spi2-core atk intltool
BuildRequires:  gettext xml-parser dbus glib libxml

%description
The At-Spi2 Atk package is a GTK+ module that bridges ATK to D-Bus At-Spi2.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_includedir}/at-spi2-atk/2.0/atk-bridge.h
%{_libdir}/libatk-bridge-2.0.la
%{_libdir}/libatk-bridge-2.0.so*
%{_libdir}/pkgconfig/atk-bridge-2.0.pc
%{_libdir}/gnome-settings-daemon-3.0/gtk-modules/at-spi2-atk.desktop
%{_libdir}/gtk-2.0/modules/libatk-bridge.*

%post
glib-compile-schemas %{_datadir}/glib-2.0/schemas &>/dev/null || :

%clean
rm -rf %{buildroot}

%changelog
* Tue Sep 22 2015 tanggeliang <tanggeliang@gmail.com>
- update from 2.17.90 to 2.18.0
* Tue Sep 15 2015 tanggeliang <tanggeliang@gmail.com>
- update from 2.16.0 to 2.17.90
* Sat Mar 28 2015 tanggeliang <tanggeliang@gmail.com>
- update from 2.14.0 to 2.16.0
* Wed Sep 24 2014 tanggeliang <tanggeliang@gmail.com>
- update from 2.12.1 to 2.14.0
* Sun Apr 20 2014 tanggeliang <tanggeliang@gmail.com>
- update from 2.10.2 to 2.12.1
* Tue Dec 3 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.10.0 to 2.10.2
* Tue Sep 24 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.8.1 to 2.10.0
* Sun Jul 7 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.8.0 to 2.8.1
* Thu Apr 11 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.6.2 to 2.8.0
* Tue Apr 9 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.6.1 to 2.6.2
* Mon Oct 22 2012 tanggeliang <tanggeliang@gmail.com>
- update from 2.6.0 to 2.6.1
* Fri Sep 28 2012 tanggeliang <tanggeliang@gmail.com>
- update from 2.4.0 to 2.6.0
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
