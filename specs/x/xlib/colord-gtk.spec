Name:       colord-gtk
Version:    0.1.26
Release:    1%{?dist}
Summary:    Colord GTK

Group:      System Environment/xlib
License:    GPLv2+
Url:        http://www.freedesktop.org
Source:     http://www.freedesktop.org/software/colord/releases/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  colord gtk+
BuildRequires:  gobject-introspection vala
BuildRequires:  gtk-doc intltool gettext xml-parser

%description
The Colord GTK package contains GTK+ bindings for Colord.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --enable-vala \
            --disable-static \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/cd-convert
%{_includedir}/colord-1/colord-gtk.h
%{_includedir}/colord-1/colord-gtk/cd-*.h
%{_libdir}/girepository-1.0/ColordGtk-1.0.typelib
%{_libdir}/libcolord-gtk.la
%{_libdir}/libcolord-gtk.so*
%{_libdir}/pkgconfig/colord-gtk.pc
%{_datadir}/gir-1.0/ColordGtk-1.0.gir
%{_datadir}/locale/en_GB/LC_MESSAGES/colord-gtk.mo
%{_datadir}/vala/vapi/colord-gtk.vapi

%clean
rm -rf %{buildroot}

%changelog
* Wed Sep 23 2015 tanggeliang <tanggeliang@gmail.com>
- update from 0.1.25 to 0.1.26
* Sun Apr 14 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.1.24 to 0.1.25
* Tue Mar 19 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.1.23 to 0.1.24
* Mon Oct 1 2012 tanggeliang <tanggeliang@gmail.com>
- create
