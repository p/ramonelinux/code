Name:       gnash
Version:    0.8.10
Release:    1%{?dist}
Summary:    gnash

Group:      System Environment/xlib
License:    GPLv2+
Url:        http://www.gnu.org
Source:     http://ftp.gnu.org/pub/gnu/gnash/0.8.10/%{name}-%{version}.tar.bz2
Patch:      gnash-0.8.10-CVE-2012-1175-1.patch

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  agg boost gst-ffmpeg firefox gconf giflib
BuildRequires:  libtool

%description
Gnash is the GNU Flash movie player and browser plugin. This is useful for watching YouTube videos or simple flash animations.

%prep
%setup -q -n %{name}-%{version}
%patch -p1

%build
sed -i '/^LIBS/s/\(.*\)/\1 -lboost_system/' \
  gui/Makefile.in utilities/Makefile.in &&
./configure --prefix=/usr --sysconfdir=/etc               \
  --with-npapi-incl=/usr/include/npapi --enable-media=gst \
  --with-npapi-plugindir=/usr/lib/mozilla/plugins         &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot} &&
make install-plugin DESTDIR=%{buildroot}

%files
%defattr(-,root,root,-)
/etc/gnashpluginrc
/etc/gnashrc
/etc/gnashthumbnailrc
%{_bindir}/dump-gnash
%{_bindir}/fb-gnash
%{_bindir}/findmicrophones
%{_bindir}/findwebcams
%{_bindir}/gnash
%{_bindir}/gnash-gtk-launcher
%{_bindir}/gnash-thumbnailer
%{_bindir}/gprocessor
%{_bindir}/gtk-gnash
%{_bindir}/rtmpget
%{_bindir}/sdl-gnash
%{_includedir}/gnash/*.h*
%{_includedir}/gnash/asobj/*.h
%{_includedir}/gnash/parser/*.h
%{_includedir}/gnash/vm/*.h
%{_libdir}/gnash/libgnashbase*.so
%{_libdir}/gnash/libgnashcore*.so
%{_libdir}/gnash/libgnashdevice.la
%{_libdir}/gnash/libgnashdevice*.so
%{_libdir}/gnash/libgnashmedia*.so
%{_libdir}/gnash/libgnashrender*.so
%{_libdir}/gnash/libgnashsound*.so
%{_libdir}/mozilla/plugins/libgnashplugin.so
%{_libdir}/pkgconfig/gnash.pc
%{_datadir}/applications/gnash.desktop
%{_datadir}/applications/gnash.schemas
%{_datadir}/gnash/GnashG.png
%{_datadir}/gnash/gnash-splash.swf
%{_datadir}/gnash/gnash_128_96.ico
%{_datadir}/icons/hicolor/32x32/apps/gnash.xpm
%{_docdir}/gnash/*
%{_mandir}/man1/*.1.gz

%clean
rm -rf %{buildroot}

%changelog
* Sun Mar 3 2013  tanggeliang <tanggeliang@gmail.com>
- create
