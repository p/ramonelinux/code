Name:       webkitgtk
Version:    2.9.90
Release:    2%{?dist}
Summary:    WebKitGTK+

Group:      System Environment/xlib
License:    GPLv2+
Url:        http://www.webkit.org
Source:     http://webkitgtk.org/releases/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  gperf gst-plugins-base gtk+ icu libsecret libsoup libwebp mesa ruby sqlite systemd-gudev which
BuildRequires:  enchant geoclue0 geoclue gobject-introspection gtk-doc
BuildRequires:  bison flex libx11 libxt dbus-glib gettext libxslt curl
BuildRequires:  gstreamer libjpeg-turbo libpng pcre fontconfig freetype js 
BuildRequires:  pango cairo cairo-gl gtk2 gsettings-desktop-schemas cmake libnotify hyphen

%description
The WebKitGTK+ package is the port of the portable web rendering engine WebKit to the GTK+ platform.

%package        javascriptcore
Summary:        javascriptcore
%description    javascriptcore

%package        bin
Summary:        bin
%description    bin

%prep
%setup -q -n %{name}-%{version}

%build
sed -i 's/â€/\"/g' Source/WebCore/xml/XMLViewer.{css,js} &&

mkdir -vp build &&
cd build        &&

cmake -DCMAKE_BUILD_TYPE=Release    \
      -DCMAKE_INSTALL_PREFIX=/usr   \
      -DCMAKE_SKIP_RPATH=ON         \
      -DPORT=GTK                    \
      -DLIB_INSTALL_DIR=%{_libdir}  \
      -DLIBEXEC_INSTALL_DIR=%{_libdir}/webkit2gtk-4.0 \
      -Wno-dev .. &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
cd build
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_includedir}/webkitgtk-4.0/webkit2/*
%{_includedir}/webkitgtk-4.0/webkitdom/*.h
%{_libdir}/girepository-1.0/WebKit2-4.0.typelib
%{_libdir}/girepository-1.0/WebKit2WebExtension-4.0.typelib
%{_libdir}/webkit2gtk-4.0/injected-bundle/libwebkit2gtkinjectedbundle.*
%{_libdir}/libwebkit2gtk-4.0.so*
%{_libdir}/pkgconfig/webkit2gtk-4.0.pc
%{_libdir}/pkgconfig/webkit2gtk-web-extension-4.0.pc
%{_datadir}/gir-1.0/WebKit2-4.0.gir
%{_datadir}/gir-1.0/WebKit2WebExtension-4.0.gir
%{_datadir}/locale/*/LC_MESSAGES/WebKit2GTK-4.0.mo

%files javascriptcore
%defattr(-,root,root,-)
%{_bindir}/jsc
%{_includedir}/webkitgtk-4.0/JavaScriptCore/*
%{_libdir}/girepository-1.0/JavaScriptCore-4.0.typelib
%{_libdir}/libjavascriptcoregtk-4.0.so*
%{_libdir}/pkgconfig/javascriptcoregtk-4.0.pc
%{_datadir}/gir-1.0/JavaScriptCore-4.0.gir

%files bin
%defattr(-,root,root,-)
%{_libdir}/webkit2gtk-4.0/WebKitDatabaseProcess
%{_libdir}/webkit2gtk-4.0/WebKitNetworkProcess
%{_libdir}/webkit2gtk-4.0/WebKitPluginProcess
%{_libdir}/webkit2gtk-4.0/WebKitPluginProcess2
%{_libdir}/webkit2gtk-4.0/WebKitWebProcess

%clean
rm -rf %{buildroot}

%changelog
* Mon Sep 14 2015 tanggeliang <tanggeliang@gmail.com>
- update from 2.8.0 to 2.9.90
* Sat Mar 28 2015 tanggeliang <tanggeliang@gmail.com>
- update from 2.4.4 to 2.8.0
* Sun Jul 20 2014 tanggeliang <tanggeliang@gmail.com>
- update from 2.4.0 to 2.4.4
* Thu Mar 27 2014 tanggeliang <tanggeliang@gmail.com>
- update from 2.3.91 to 2.4.0
* Sun Mar 16 2014 tanggeliang <tanggeliang@gmail.com>
- update from 2.3.2 to 2.3.91
* Tue Dec 3 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.2.1 to 2.3.2
* Sun Oct 20 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.1.92 to 2.2.1
* Tue Sep 24 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.1.2 to 2.1.92
* Sun Jul 7 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.0.0 to 2.1.2
* Fri Jun 7 2013 tanggeliang <tanggeliang@gmail.com>
- add "#include <sys/types.h> #include <sys/socket.h>"
- in Source/WebKit2/UIProcess/Launcher/gtk/ProcessLauncherGtk.cpp
* Thu Apr 11 2013 tanggeliang <tanggeliang@gmil.com>
- update from 1.10.2 to 2.0.0
* Sun Mar 31 2013 tanggeliang <tanggeliang@gmail.com>
- add LDFLAGS+="-lrt" to fix "undefined reference to symbol 'shm_unlink@@GLIBC_2.2.5'"
* Thu Feb 28 2013 tanggeliang <tanggeliang@gmail.com>
- Split into three packages: webkit webkit2 javascriptcore
* Sat Feb 23 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.10.1 to 1.10.2
* Sun Oct 21 2012 tanggeliang <tanggeliang@gmail.com>
- update from 1.10.0 to 1.10.1
* Sat Sep 29 2012 tanggeliang <tanggeliang@gmail.com>
- update from 1.18.2 to 1.10.0
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
