Name:       gtkmm2
Version:    2.24.4
Release:    1%{?dist}
Summary:    Gtkmm

Group:      System Environment/xlib
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/%{name}/2.24/gtkmm-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  atkmm gtk2 pangomm

%description
The Gtkmm package provides a C++ interface to GTK+-2. It can be installed alongside Gtkmm-3.4.0 (the GTK+-3 version) with no namespace conflicts.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n gtkmm-%{version}

%build
./configure --prefix=/usr \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_includedir}/gdkmm-2.4/gdkmm.h
%{_includedir}/gdkmm-2.4/gdkmm/*
%{_includedir}/gtkmm-2.4/gtkmm.h
%{_includedir}/gtkmm-2.4/gtkmm/*
%{_libdir}/gdkmm-2.4/include/g*kmmconfig.h
%{_libdir}/gtkmm-2.4/include/gtkmmconfig.h
%{_libdir}/gtkmm-2.4/proc/m4/*.m4
%{_libdir}/libgdkmm-2.4.la
%{_libdir}/libgdkmm-2.4.so*
%{_libdir}/libgtkmm-2.4.la
%{_libdir}/libgtkmm-2.4.so*
%{_libdir}/pkgconfig/gdkmm-2.4.pc
%{_libdir}/pkgconfig/gtkmm-2.4.pc
%{_datadir}/devhelp/books/gtkmm-2.4/gtkmm-2.4.devhelp2
%{_docdir}/gtkmm-2.4/*

%clean
rm -rf %{buildroot}

%changelog
* Wed Oct 2 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.24.2 to 2.24.4
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
