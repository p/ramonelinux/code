Name:       libxklavier
Version:    5.3
Release:    2%{?dist}
Summary:    libxklavier

Group:      System Environment/xlib
License:    GPLv2+
Url:        http://ftp.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/%{name}/5.3/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  glib iso-codes libxml
BuildRequires:  libx11 xkbcomp libxkbfile libxi
BuildRequires:  gobject-introspection gtk-doc gettext
Requires:       iso-codes

%description
The libxklavier package contains an utility library for X keyboard.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --disable-static \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_includedir}/libxklavier/xkl*
%{_libdir}/libxklavier.*
%{_libdir}/girepository-1.0/Xkl-1.0.typelib
%{_libdir}/pkgconfig/libxklavier.pc
%{_datadir}/gir-1.0/Xkl-1.0.gir

%files doc
%defattr(-,root,root,-)
%{_datadir}/gtk-doc/html/libxklavier/*

%clean
rm -rf %{buildroot}

%changelog
* Sat Sep 29 2012 tanggeliang <tanggeliang@gmail.com>
- update from 5.2.1 to 5.3
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
