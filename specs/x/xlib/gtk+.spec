Name:       gtk+
Version:    3.18.0
Release:    1%{?dist}
Summary:    GTK+ 3

Group:      System Environment/xlib
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/%{name}/3.18/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  at-spi2-atk gdk-pixbuf atk glib cairo pango
BuildRequires:  gettext gobject-introspection
BuildRequires:  libx11 libxinerama libxrandr libxext libxcursor libxcomposite libxdamage libxfixes libxi
BuildRequires:  libxkbcommon gtk-doc libxslt
BuildRequires:	mesa wayland mesa-wayland libepoxy

%description
The GTK+ 3 package contains the libraries used for creating graphical user interfaces for applications.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr             \
            --sysconfdir=/etc         \
            --enable-broadway-backend \
            --enable-x11-backend      \
            --enable-wayland-backend  \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

cat > %{buildroot}/etc/gtk-3.0/settings.ini << "EOF"
[Settings]
gtk-theme-name = Clearwaita
gtk-fallback-icon-theme = elementary
EOF

%files
%defattr(-,root,root,-)
/etc/gtk-3.0/*
%{_bindir}/broadwayd
%{_bindir}/gtk-builder-tool
%{_bindir}/gtk-encode-symbolic-svg
%{_bindir}/gtk-launch
%{_bindir}/gtk-query-immodules-3.0
%{_bindir}/gtk-update-icon-cache
%{_bindir}/gtk3-demo
%{_bindir}/gtk3-demo-application
%{_bindir}/gtk3-icon-browser
%{_bindir}/gtk3-widget-factory
%{_includedir}/gail-3.0/*
%{_includedir}/gtk-3.0/*
%{_libdir}/girepository-1.0/*
%{_libdir}/libgailutil-3.*
%{_libdir}/libgdk-3.*
%{_libdir}/libgtk-3.*
%{_libdir}/gtk-3.0/*
%{_libdir}/pkgconfig/gail-3.0.pc
%{_libdir}/pkgconfig/gdk-broadway-3.0.pc
%{_libdir}/pkgconfig/gdk-x11-3.0.pc
%{_libdir}/pkgconfig/gdk-3.0.pc
%{_libdir}/pkgconfig/gtk+-3.0.pc
%{_libdir}/pkgconfig/gtk+-broadway-3.0.pc
%{_libdir}/pkgconfig/gtk+-unix-print-3.0.pc
%{_libdir}/pkgconfig/gtk+-x11-3.0.pc
%{_datadir}/aclocal/*
%{_datadir}/applications/gtk3-*.desktop
%{_datadir}/glib-2.0/schemas/*
%{_datadir}/icons/hicolor/*x*/apps/gtk3-*.png
%{_datadir}/locale/*
%{_datadir}/themes/*
%{_datadir}/gtk-3.0/*
%{_datadir}/gir-1.0/*
%{_libdir}/pkgconfig/gdk-wayland-3.0.pc
%{_libdir}/pkgconfig/gtk+-wayland-3.0.pc

%files doc
%defattr(-,root,root,-)
%{_datadir}/gtk-doc/*
%{_mandir}/*

%post
gtk-query-immodules-3.0 --update-cache &>/dev/null || :

%postun
gtk-query-immodules-3.0 --update-cache &>/dev/null || :

%clean
rm -rf %{buildroot}

%changelog
* Tue Sep 22 2015 tanggeliang <tanggeliang@gmail.com>
- update from 3.17.9 to 3.18.0
* Tue Sep 15 2015 tanggeliang <tanggeliang@gmail.com>
- update from 3.16.4 to 3.17.9
* Sat Jun 27 2015 tanggeliang <tanggeliang@gmail.com>
- update from 3.16.0 to 3.16.4
* Sat Mar 28 2015 tanggeliang <tanggeliang@gmail.com>
- update from 3.14.0 to 3.16.0
* Tue Sep 23 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.12.2 to 3.14.0
* Sat Jul 19 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.12.1 to 3.12.2
* Sun Apr 20 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.12.0 to 3.12.1
* Thu Mar 27 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.11.8 to 3.12.0
* Sat Mar 15 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.10.6 to 3.11.8
* Sat Dec 7 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.10.5 to 3.10.6
* Tue Dec 3 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.10.2 to 3.10.5
* Sun Oct 20 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.10.0 to 3.10.2
* Tue Sep 24 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.8.2 to 3.10.0
* Tue Jul 2 2013 tanggeliang <tanggeliang@gmail.com>
- add --enable-wayland-backend
- export GDK_BACKEND=wayland
* Tue Jul 2 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.8.1 to 3.8.2
* Sat May 11 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.8.0 to 3.8.1
* Thu Apr 11 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.4 to 3.8.0
* Sat Feb 23 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.2 to 3.6.4
* Mon Nov 26 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.1 to 3.6.2
* Mon Oct 22 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.0 to 3.6.1
* Fri Sep 28 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.4.4 to 3.6.0
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
