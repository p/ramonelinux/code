Name:       gdk-pixbuf
Version:    2.32.0
Release:    1%{?dist}
Summary:    Gdk Pixbuf

Group:      System Environment/xlib
License:    GPLv2+
Url:        http://ftp.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/%{name}/2.32/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  glib libjpeg-turbo libpng libtiff
BuildRequires:  libx11
BuildRequires:  gobject-introspection
BuildRequires:  gettext gtk-doc

%description
The Gdk Pixbuf is a toolkit for image loading and pixel buffer manipulation.
It is used by GTK+ 2 and GTK+ 3 to load and manipulate images.
In the past it was distributed as part of GTK+ 2 but it was split off into a separate package in preparation for the change to GTK+ 3.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --with-x11 \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/gdk-pixbuf-csource
%{_bindir}/gdk-pixbuf-pixdata
%{_bindir}/gdk-pixbuf-query-loaders
%{_includedir}/gdk-pixbuf-2.0/*
%{_libdir}/gdk-pixbuf-*.*
%{_libdir}/libgdk*pixbuf*.*
%{_libdir}/girepository-1.0/GdkPixbuf-2.0.typelib
%{_libdir}/pkgconfig/gdk-pixbuf-2.0.pc
%{_libdir}/pkgconfig/gdk-pixbuf-xlib-2.0.pc
%{_datadir}/gir-1.0/GdkPixbuf-2.0.gir
%{_datadir}/locale/*

%files doc
%defattr(-,root,root,-)
%{_datadir}/gtk-doc/html/gdk-pixbuf/*
%{_mandir}/*

%clean
rm -rf %{buildroot}

%post
gdk-pixbuf-query-loaders --update-cache

%postun
gdk-pixbuf-query-loaders --update-cache

%changelog
* Tue Sep 22 2015 tanggeliang <tanggeliang@gmail.com>
- update from 2.31.7 to 2.32.0
* Tue Sep 15 2015 tanggeliang <tanggeliang@gmail.com>
- update from 2.31.3 to 2.31.7
* Sat Mar 28 2015 tanggeliang <tanggeliang@gmail.com>
- update from 2.31.0 to 2.31.3
* Tue Jul 22 2014 tanggeliang <tanggeliang@gmail.com>
- update from 2.30.8 to 2.31.0
* Sat Jul 19 2014 tanggeliang <tanggeliang@gmail.com>
- update from 2.30.7 to 2.30.8
* Thu Mar 27 2014 tanggeliang <tanggeliang@gmail.com>
- update from 2.30.6 to 2.30.7
* Sat Mar 15 2014 tanggeliang <tanggeliang@gmail.com>
- update from 2.30.2 to 2.30.6
* Sun Jan 5 2014 tanggeliang <tanggeliang@gmail.com>
- update from 2.30.1 to 2.30.2
* Tue Dec 3 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.30.0 to 2.30.1
* Tue Sep 24 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.28.2 to 2.30.0
* Sat Jul 6 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.28.0 to 2.28.2
* Thu Apr 11 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.26.5 to 2.28.0
* Mon Nov 26 2012 tanggeliang <tanggeliang@gmail.com>
- update from 2.26.4 to 2.26.5
* Fri Sep 28 2012 tanggeliang <tanggeliang@gmail.com>
- update from 2.26.3 to 2.26.4
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
