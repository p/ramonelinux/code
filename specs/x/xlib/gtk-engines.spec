Name:       gtk-engines
Version:    2.20.2
Release:    7%{?dist}
Summary:    GTK Engines

Group:      System Environment/xlib
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/%{name}/2.20/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  gtk2 intltool which
BuildRequires:  gettext xml-parser

%description
The GTK Engines package contains eight themes/engines and two additional engines for GTK2.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_libdir}/gtk-2.0/2.10.0/engines/libclearlooks.*
%{_libdir}/gtk-2.0/2.10.0/engines/libcrux-engine.*
%{_libdir}/gtk-2.0/2.10.0/engines/libglide.*
%{_libdir}/gtk-2.0/2.10.0/engines/libhcengine.*
%{_libdir}/gtk-2.0/2.10.0/engines/libindustrial.*
%{_libdir}/gtk-2.0/2.10.0/engines/libmist.*
%{_libdir}/gtk-2.0/2.10.0/engines/libredmond95.*
%{_libdir}/gtk-2.0/2.10.0/engines/libthinice.*
%{_libdir}/pkgconfig/gtk-engines-2.pc
%{_datadir}/gtk-engines/*.xml
%{_datadir}/locale/*/LC_MESSAGES/gtk-engines.mo
%{_datadir}/themes/Clearlooks/gtk-2.0/gtkrc
%{_datadir}/themes/Crux/gtk-2.0/gtkrc
%{_datadir}/themes/Industrial/gtk-2.0/gtkrc
%{_datadir}/themes/Mist/gtk-2.0/gtkrc
%{_datadir}/themes/Redmond/gtk-2.0/gtkrc
%{_datadir}/themes/ThinIce/gtk-2.0/gtkrc

%clean
rm -rf %{buildroot}

%changelog
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
