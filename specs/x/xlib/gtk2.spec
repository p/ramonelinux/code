Name:       gtk2
Version:    2.24.28
Release:    2%{?dist}
Summary:    GTK+ 2

Group:      System Environment/xlib
License:    GPLv2+
Url:        http://ftp.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/gtk+/2.24/gtk+-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  atk cairo gdk-pixbuf pango
BuildRequires:  libx11 libxinerama libxrandr libxext libxcursor libxcomposite libxdamage libxfixes
BuildRequires:  hicolor-icon-theme gobject-introspection
BuildRequires:  gettext

%description
The GTK+ 2 package contains libraries used for creating graphical user interfaces for applications.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%package        update-icon-cache
Summary:        gtk2-update-icon-cache
%description    update-icon-cache

%prep
%setup -q -n gtk+-%{version}

%build
sed -i "s#l \(gtk-.*\).sgml#& -o \1#" docs/{faq,tutorial}/Makefile.in &&
./configure --prefix=/usr --sysconfdir=/etc \
            --libdir=%{_libdir}                                       &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
/etc/gtk-2.0/im-multipress.conf
%{_bindir}/gtk-builder-convert
%{_bindir}/gtk-demo
%{_bindir}/gtk-query-immodules-2.0
%{_includedir}/gail-1.0/*
%{_includedir}/gtk-2.0/*
%{_includedir}/gtk-unix-print-2.0/*
%{_libdir}/gtk-2.0/*
%{_libdir}/libgailutil.la
%{_libdir}/libgailutil.so*
%{_libdir}/libgdk-x11-2.0.la
%{_libdir}/libgdk-x11-2.0.so*
%{_libdir}/libgtk-x11-2.0.la
%{_libdir}/libgtk-x11-2.0.so*
%{_libdir}/pkgconfig/gail.pc
%{_libdir}/pkgconfig/gdk-2.0.pc
%{_libdir}/pkgconfig/gdk-x11-2.0.pc
%{_libdir}/pkgconfig/gtk+-2.0.pc
%{_libdir}/pkgconfig/gtk+-unix-print-2.0.pc
%{_libdir}/pkgconfig/gtk+-x11-2.0.pc
%{_datadir}/aclocal/*
%{_datadir}/gtk-2.0/*
%{_datadir}/locale/*
%{_datadir}/themes/*
%{_libdir}/girepository-1.0/*
%{_datadir}/gir-1.0/*

%files update-icon-cache
%defattr(-,root,root,-)
# Conflicts gtk+
%{_bindir}/gtk-update-icon-cache

%files doc
%defattr(-,root,root,-)
%{_datadir}/gtk-doc/*

%post
gtk-query-immodules-2.0 > /etc/gtk-2.0/gtk.immodules

%postun
rm -rf /etc/gtk-2.0/gtk.immodules

%clean
rm -rf %{buildroot}

%changelog
* Sat Jun 27 2015 tanggeliang <tanggeliang@gmail.com>
- update from 2.24.23 to 2.24.28
* Thu Mar 20 2014 tanggeliang <tanggeliang@gmail.com>
- update from 2.24.22 to 2.24.23
* Sat Nov 9 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.24.21 to 2.24.22
* Tue Sep 24 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.24.19 to 2.24.21
* Tue Jul 2 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.24.17 to 2.24.19
* Sat Mar 30 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.24.16 to 2.24.17
* Sun Mar 17 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.24.14 to 2.24.16
* Sat Feb 23 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.24.13 to 2.24.14
* Fri Sep 28 2012 tanggeliang <tanggeliang@gmail.com>
- update from 2.24.12 to 2.24.13
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
