Name:       qt4
Version:    4.8.5
Release:    1%{?dist}
Summary:    Qt 4

Group:      System Environment/xlib
License:    GPLv2+
Url:        http://www.qt.org
Source:     http://releases.qt-project.org/qt4/source/qt-everywhere-opensource-src-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  libx11 libxinerama libxcursor libxrandr mesalib
BuildRequires:  dbus libjpeg-turbo libmng libtiff libpng
BuildRequires:  gst-plugins-base pulseaudio openssl cups

%description
Qt is a cross-platform application framework that is widely used for developing application software with a graphical user interface (GUI) (in which cases Qt is classified as a widget toolkit), and also used for developing non-GUI programs such as command-line tools and consoles for servers.
One of the major users of Qt is KDE.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%package        demos
Summary:        demos
Requires:       %{name}-examples
%description    demos

%package        examples
Summary:        examples
%description    examples

%prep
%setup -q -n qt-everywhere-opensource-src-%{version}

%build
./configure -confirm-license \
            -opensource \
            -release \
            -prefix /usr \
            -bindir %{_libdir}/qt4/bin \
            -libdir %{_libdir} \
            -headerdir /usr/include/qt4 \
            -datadir /usr/share/qt4 \
            -plugindir %{_libdir}/qt4/plugins \
            -importdir %{_libdir}/qt4/imports \
            -translationdir /usr/share/qt4/translations \
            -sysconfdir /etc/xdg \
            -docdir /usr/share/doc/qt4 \
            -demosdir /usr/share/doc/qt4/demos \
            -examplesdir /usr/share/doc/qt4/examples \
            -dbus-linked \
            -openssl-linked \
            -system-sqlite \
            -plugin-sql-sqlite \
            -no-phonon \
            -no-phonon-backend \
            -no-nis \
            -no-openvg \
            -optimized-qmake &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make install INSTALL_ROOT=%{buildroot} &&

rm -rf %{buildroot}/usr/tests &&

mkdir -p %{buildroot}/usr/share/pixmaps &&
install -v -Dm644 src/gui/dialogs/images/qtlogo-64.png \
                  %{buildroot}/usr/share/pixmaps/qt4logo.png &&
install -v -Dm644 tools/assistant/tools/assistant/images/assistant-128.png \
                  %{buildroot}/usr/share/pixmaps/assistant-qt4.png &&
install -v -Dm644 tools/designer/src/designer/images/designer.png \
                  %{buildroot}/usr/share/pixmaps/designer-qt4.png &&
install -v -Dm644 tools/linguist/linguist/images/icons/linguist-128-32.png \
                  %{buildroot}/usr/share/pixmaps/linguist-qt4.png &&
install -v -Dm644 tools/qdbus/qdbusviewer/images/qdbusviewer-128.png \
                  %{buildroot}/usr/share/pixmaps/qdbusviewer-qt4.png

find %{buildroot}/%{_libdir}/libQt*.prl -exec sed -i -e \
     '/^QMAKE_PRL_BUILD_DIR/d;s/\(QMAKE_PRL_LIBS =\).*/\1/' {} \;
find %{buildroot}/%{_libdir}/pkgconfig/Qt*.pc -exec perl -pi -e \
     "s, -L$(basename $PWD)/?\S+,,g" {} \;

find %{buildroot}/%{_libdir}/pkgconfig/Qt*.pc -exec sed -i -e \
     "s@/usr/bin/@/%{_libdir}/qt4/bin/@g" {} \;

mkdir -p %{buildroot}/usr/bin &&
for file in %{buildroot}/%{_libdir}/qt4/bin/*
do
  ln -sfv ../%{_lib}/qt4/bin/$(basename $file) %{buildroot}/usr/bin/$(basename $file)
  ln -sfv ../%{_lib}/qt4/bin/$(basename $file) %{buildroot}/usr/bin/$(basename $file)-qt4
done

mkdir -p %{buildroot}/etc/profile.d &&
cat > %{buildroot}/etc/profile.d/qt.sh << EOF
# Begin /etc/profile.d/qt.sh

QTDIR=/usr

export QTDIR

# End /etc/profile.d/qt.sh
EOF

install -dm755 %{buildroot}/usr/share/applications
cat > %{buildroot}/usr/share/applications/qtconfig-qt4.desktop << "EOF"
[Desktop Entry]
Name=Qt4 Config 
Comment=Configure Qt4 behavior, styles, fonts
Exec=qtconfig-qt4
Icon=qt4logo
Terminal=false
Encoding=UTF-8
Type=Application
Categories=Qt;Settings;
EOF
cat > %{buildroot}/usr/share/applications/assistant-qt4.desktop << "EOF"
[Desktop Entry]
Name=Qt4 Assistant 
Comment=Shows Qt4 documentation and examples
Exec=assistant-qt4
Icon=assistant-qt4
Terminal=false
Encoding=UTF-8
Type=Application
Categories=Qt;Development;Documentation;
EOF
cat > %{buildroot}/usr/share/applications/designer-qt4.desktop << "EOF"
[Desktop Entry]
Name=Qt4 Designer
Comment=Design GUIs for Qt4 applications
Exec=designer-qt4
Icon=designer-qt4
MimeType=application/x-designer;
Terminal=false
Encoding=UTF-8
Type=Application
Categories=Qt;Development;
EOF
cat > %{buildroot}/usr/share/applications/linguist-qt4.desktop << "EOF"
[Desktop Entry]
Name=Qt4 Linguist 
Comment=Add translations to Qt4 applications
Exec=linguist-qt4
Icon=linguist-qt4
MimeType=text/vnd.trolltech.linguist;application/x-linguist;
Terminal=false
Encoding=UTF-8
Type=Application
Categories=Qt;Development;
EOF

cat > %{buildroot}/usr/share/applications/qdbusviewer-qt4.desktop << "EOF"
[Desktop Entry]
Name=Qt4 QDbusViewer 
GenericName=D-Bus Debugger
Comment=Debug D-Bus applications
Exec=qdbusviewer-qt4
Icon=qdbusviewer-qt4
Terminal=false
Encoding=UTF-8
Type=Application
Categories=Qt;Development;Debugger;
EOF

%files
%defattr(-,root,root,-)
/etc/profile.d/qt.sh
%{_bindir}/*
%{_libdir}/qt4/bin/assistant
%{_libdir}/qt4/bin/designer
%{_libdir}/qt4/bin/lconvert
%{_libdir}/qt4/bin/linguist
%{_libdir}/qt4/bin/lrelease
%{_libdir}/qt4/bin/lupdate
%{_libdir}/qt4/bin/moc
%{_libdir}/qt4/bin/pixeltool
%{_libdir}/qt4/bin/qcollectiongenerator
%{_libdir}/qt4/bin/qdbus
%{_libdir}/qt4/bin/qdbuscpp2xml
%{_libdir}/qt4/bin/qdbusviewer
%{_libdir}/qt4/bin/qdbusxml2cpp
%{_libdir}/qt4/bin/qdoc3
%{_libdir}/qt4/bin/qhelpconverter
%{_libdir}/qt4/bin/qhelpgenerator
%{_libdir}/qt4/bin/qmake
%{_libdir}/qt4/bin/qmlplugindump
%{_libdir}/qt4/bin/qmlviewer
%{_libdir}/qt4/bin/qt3to4
%{_libdir}/qt4/bin/qtconfig
%{_libdir}/qt4/bin/qttracereplay
%{_libdir}/qt4/bin/rcc
%{_libdir}/qt4/bin/uic
%{_libdir}/qt4/bin/uic3
%{_libdir}/qt4/bin/xmlpatterns
%{_libdir}/qt4/bin/xmlpatternsvalidator
%{_includedir}/qt4/Qt*/*
%{_libdir}/qt4/imports/Qt*/*
%{_libdir}/libQt*.*
%{_libdir}/pkgconfig/Qt*.pc
%{_libdir}/qt4/plugins/accessible/libqtaccessiblecompatwidgets.so
%{_libdir}/qt4/plugins/accessible/libqtaccessiblewidgets.so
%{_libdir}/qt4/plugins/bearer/libqconnmanbearer.so
%{_libdir}/qt4/plugins/bearer/libqgenericbearer.so
%{_libdir}/qt4/plugins/bearer/libqnmbearer.so
%{_libdir}/qt4/plugins/codecs/libqcncodecs.so
%{_libdir}/qt4/plugins/codecs/libqjpcodecs.so
%{_libdir}/qt4/plugins/codecs/libqkrcodecs.so
%{_libdir}/qt4/plugins/codecs/libqtwcodecs.so
%{_libdir}/qt4/plugins/designer/libarthurplugin.so
%{_libdir}/qt4/plugins/designer/libcontainerextension.so
%{_libdir}/qt4/plugins/designer/libcustomwidgetplugin.so
%{_libdir}/qt4/plugins/designer/libqdeclarativeview.so
%{_libdir}/qt4/plugins/designer/libqt3supportwidgets.so
%{_libdir}/qt4/plugins/designer/libqwebview.so
%{_libdir}/qt4/plugins/designer/libtaskmenuextension.so
%{_libdir}/qt4/plugins/designer/libworldtimeclockplugin.so
%{_libdir}/qt4/plugins/graphicssystems/libqglgraphicssystem.so
%{_libdir}/qt4/plugins/graphicssystems/libqtracegraphicssystem.so
%{_libdir}/qt4/plugins/iconengines/libqsvgicon.so
%{_libdir}/qt4/plugins/imageformats/libqgif.so
%{_libdir}/qt4/plugins/imageformats/libqico.so
%{_libdir}/qt4/plugins/imageformats/libqjpeg.so
%{_libdir}/qt4/plugins/imageformats/libqmng.so
%{_libdir}/qt4/plugins/imageformats/libqsvg.so
%{_libdir}/qt4/plugins/imageformats/libqtga.so
%{_libdir}/qt4/plugins/imageformats/libqtiff.so
%{_libdir}/qt4/plugins/inputmethods/libqimsw-multi.so
%{_libdir}/qt4/plugins/qmltooling/libqmldbg_inspector.so
%{_libdir}/qt4/plugins/qmltooling/libqmldbg_tcp.so
%{_libdir}/qt4/plugins/script/libqtscriptdbus.so
%{_libdir}/qt4/plugins/sqldrivers/libqsqlite.so
%{_datadir}/applications/*-qt4.desktop
%{_datadir}/pixmaps/*.png
%{_datadir}/qt4/mkspecs/*
%{_datadir}/qt4/phrasebooks/*
%{_datadir}/qt4/q3porting.xml
%{_datadir}/qt4/translations/*.qm

%files doc
%defattr(-,root,root,-)
%{_docdir}/qt4/html/*
%{_docdir}/qt4/qch/*.qch
%{_docdir}/qt4/src/images/*

%files demos
%defattr(-,root,root,-)
%{_libdir}/qt4/bin/qtdemo
%{_docdir}/qt4/demos/*

%files examples
%defattr(-,root,root,-)
%{_docdir}/qt4/examples/*

%clean
rm -rf %{buildroot}

%changelog
* Fri Jul 26 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.8.4 to 4.8.5
* Wed Mar 13 2013 tanggeliang <tanggeliang@gmail.com>
- remove BuildRequires - glib gtk2
* Wed Feb 27 2013 tanggeliang <tanggeliang@gmail.com>
- remove "-nomake examples -nomake demos" to build Qt examples and demos
* Sat Feb 23 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.8.2 to 4.8.4
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
