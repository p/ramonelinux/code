Name:       pangomm
Version:    2.37.2
Release:    1%{?dist}
Summary:    Pangomm

Group:      System Environment/xlib
License:    GPLv2+
Url:        http://ftp.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/%{name}/2.37/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  cairomm glibmm pango

%description
The Pangomm package provides a C++ interface to Pango.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_includedir}/pangomm-1.4/pangomm/*
%{_includedir}/pangomm-1.4/pangomm.h
%{_libdir}/libpangomm-1.4.*
%{_libdir}/pangomm-1.4/include/pangommconfig.h
%{_libdir}/pangomm-1.4/proc/m4/convert*.m4
%{_libdir}/pkgconfig/pangomm-1.4.pc
%{_datadir}/devhelp/books/pangomm-1.4/pangomm-1.4.devhelp2

%files doc
%defattr(-,root,root,-)
%{_docdir}/*/*

%clean
rm -rf %{buildroot}

%changelog
* Sun Sep 20 2015 tanggeliang <tanggeliang@gmail.com>
- update from 2.34.0 to 2.37.2
* Thu Jul 25 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.28.4 to 2.34.0
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
