Name:       mx
Version:    1.4.6
Release:    5%{?dist}
Summary:    Mx

Group:      System Environment/xlib
License:    GPLv2+
Url:        http://www.clutter-project.org
Source:     http://source.clutter-project.org/sources/mx/1.4/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  clutter dbus gtk2 intltool startup-notification
BuildRequires:  gobject-introspection
BuildRequires:  gtk-doc gettext xml-parser mesalib

%description
Mx is a traditional GUI toolkit built upon Clutter technology.
It enables smooth animations and special effects through hardware accelerated graphics (OpenGL).

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/mx-create-image-cache
%{_includedir}/mx-1.0/mx-gtk/mx-gtk*.h
%{_includedir}/mx-1.0/mx/mx*.h
%{_libdir}/girepository-1.0/Mx-1.0.typelib
%{_libdir}/girepository-1.0/MxGtk-1.0.typelib
%{_libdir}/libmx-1.0.la
%{_libdir}/libmx-1.0.so*
%{_libdir}/libmx-gtk-1.0.la
%{_libdir}/libmx-gtk-1.0.so*
%{_libdir}/pkgconfig/mx-1.0.pc
%{_libdir}/pkgconfig/mx-gtk-1.0.pc
%{_datadir}/gir-1.0/Mx-1.0.gir
%{_datadir}/gir-1.0/MxGtk-1.0.gir
%{_datadir}/gtk-doc/html/mx-gtk/*
%{_datadir}/gtk-doc/html/mx/*
%{_datadir}/locale/*/LC_MESSAGES/mx-1.0.mo
%{_datadir}/mx/style/*

%clean
rm -rf %{buildroot}

%changelog
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
