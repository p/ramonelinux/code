Name:       clutter
Version:    1.24.0
Release:    1%{?dist}
Summary:    Clutter

Group:      System Environment/xlib
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/%{name}/1.24/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  atk cogl json-glib cairo
BuildRequires:  gobject-introspection gtk+
BuildRequires:  gtk-doc systemd-gudev mesa gettext
BuildRequires:  libevdev libinput

%description
The Clutter package is an open source software library for creating fast, visually rich and animated graphical user interfaces.

%package        wayland
Summary:        Clutter for Wayland
BuildRequires:  wayland libxkbcommon
%description    wayland

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr 		\
	    --sysconfdir=/etc 		\
	    --enable-egl-backend 	\
            --enable-evdev-input 	\
            --enable-wayland-backend 	\
	    --enable-wayland-compositor \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_includedir}/clutter-1.0/cally/cally*.h
%{_includedir}/clutter-1.0/clutter/clutter*.h
%{_includedir}/clutter-1.0/clutter/deprecated/clutter-*.h
%{_includedir}/clutter-1.0/clutter/egl/clutter-egl*.h
%{_includedir}/clutter-1.0/clutter/evdev/clutter-evdev.h
%{_includedir}/clutter-1.0/clutter/gdk/clutter-*.h
%{_includedir}/clutter-1.0/clutter/glx/clutter-*.h
%{_includedir}/clutter-1.0/clutter/x11/clutter-*.h
%{_libdir}/girepository-1.0/Cally-1.0.typelib
%{_libdir}/girepository-1.0/Clutter-1.0.typelib
%{_libdir}/girepository-1.0/ClutterGdk-1.0.typelib
%{_libdir}/girepository-1.0/ClutterX11-1.0.typelib
%{_libdir}/libclutter-1.0.*
%{_libdir}/libclutter-glx-1.0.*
%{_libdir}/pkgconfig/cally-1.0.pc
%{_libdir}/pkgconfig/clutter-1.0.pc
%{_libdir}/pkgconfig/clutter-cogl-1.0.pc
%{_libdir}/pkgconfig/clutter-egl-1.0.pc
%{_libdir}/pkgconfig/clutter-gdk-1.0.pc
%{_libdir}/pkgconfig/clutter-glx-1.0.pc
%{_libdir}/pkgconfig/clutter-x11-1.0.pc
%{_datadir}/gir-1.0/Cally-1.0.gir
%{_datadir}/gir-1.0/Clutter-1.0.gir
%{_datadir}/gir-1.0/ClutterGdk-1.0.gir
%{_datadir}/gir-1.0/ClutterX11-1.0.gir
%{_datadir}/locale/*

%files wayland
%defattr(-,root,root,-)
%{_includedir}/clutter-1.0/clutter/wayland/clutter-wayland*.h
%{_libdir}/pkgconfig/clutter-wayland-1.0.pc
%{_libdir}/pkgconfig/clutter-wayland-compositor-1.0.pc

%files doc
%defattr(-,root,root,-)
#%{_datadir}/gtk-doc/html/cally/*
%{_datadir}/gtk-doc/html/clutter/*

%clean
rm -rf %{buildroot}

%changelog
* Wed Sep 23 2015 tanggeliang <tanggeliang@gmail.com>
- update from 1.23.4 to 1.24.0
* Sat Jun 27 2015 tanggeliang <tanggeliang@gmail.com>
- update from 1.22.0 to 1.22.2
* Sun Mar 29 2015 tanggeliang <tanggeliang@gmail.com>
- update from 1.20.0 to 1.22.0
* Tue Sep 23 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.19.4 to 1.20.0
* Thu Jul 24 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.18.2 to 1.19.4
* Sun Apr 20 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.18.0 to 1.18.2
* Fri Mar 21 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.17.6 to 1.18.0
* Sat Mar 15 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.16.2 to 1.17.6
* Tue Dec 3 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.16.0 to 1.16.2
* Wed Sep 25 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.14.4 to 1.16.0
* Thu Jul 18 2013 tanggeliang <tanggeliang@gmail.com> 
- update from 1.14.0 to 1.14.4
* Sun Apr 14 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.12.2 to 1.14.0
* Mon Oct 22 2012 tanggeliang <tanggeliang@gmail.com>
- update from 1.12.0 to 1.12.2
* Fri Sep 28 2012 tanggeliang <tanggeliang@gmail.com>
- update from 1.10.8 to 1.12.0
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
