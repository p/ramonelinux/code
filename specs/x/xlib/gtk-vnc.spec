Name:       gtk-vnc
Version:    0.5.4
Release:    1%{?dist}
Summary:    Gtk VNC 

Group:      System Environment/xlib
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/%{name}/0.5/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  gtk+ gnutls intltool
BuildRequires:  gobject-introspection vala
BuildRequires:  cyrus-sasl nspr pulseaudio
BuildRequires:  gettext xml-parser libgcrypt

%description
The Gtk VNC package contains a VNC viewer widget for GTK+. It is built using coroutines allowing it to be completely asynchronous while remaining single threaded.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --with-gtk=3.0 \
            --enable-vala \
            --without-sasl &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/gvnccapture
%{_includedir}/gtk-vnc-2.0/*.h
%{_includedir}/gvnc-1.0/*.h
%{_includedir}/gvncpulse-1.0/*.h
%{_libdir}/girepository-1.0/*.typelib
%{_libdir}/libgtk-vnc-2.0.*
%{_libdir}/libgvnc-1.0.*
%{_libdir}/libgvncpulse-1.0.*
%{_libdir}/pkgconfig/gtk-vnc-2.0.pc
%{_libdir}/pkgconfig/gvnc-1.0.pc
%{_libdir}/pkgconfig/gvncpulse-1.0.pc
%{_datadir}/gir-1.0/*.gir
%{_datadir}/locale/*/LC_MESSAGES/gtk-vnc.mo
%{_datadir}/vala/vapi/gtk-vnc-2.0.deps
%{_datadir}/vala/vapi/*.vapi
%{_mandir}/man1/gvnccapture.1.gz

%clean
rm -rf %{buildroot}

%changelog
* Thu Mar 26 2015 tanggeliang <tanggeliang@gmail.com>
- update from 0.5.2 to 0.5.4
* Wed Aug 7 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.5.1 to 0.5.2
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
