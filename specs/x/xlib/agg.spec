Name:       agg
Version:    2.5
Release:    1%{?dist}
Summary:    agg

Group:      System Environment/xlib
License:    GPLv2+
Url:        http://www.antigrain.com
Source:     http://www.antigrain.com/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  pkg-config sdl libx11
BuildRequires:  libtool autoconf automake m4

%description
The Anti-Grain Geometry (AGG) package contains a general purpose C++ graphical toolkit.
It can be used in many areas of computer programming where high quality 2D graphics is an essential part of the project.

%prep
%setup -q -n %{name}-%{version}

%build
sed -i 's:  -L@x_libraries@::' src/platform/X11/Makefile.am &&
sed -i '/^AM_C_PROTOTYPES/d' configure.in &&
bash autogen.sh --prefix=/usr --disable-static &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

%files
%defattr(-,root,root,-)
%{_includedir}/agg2/agg_*.h
%{_includedir}/agg2/ctrl/agg_*.h
%{_includedir}/agg2/platform/agg_platform_support.h
%{_includedir}/agg2/util/agg_color_conv*.h
%{_libdir}/libagg.la
%{_libdir}/libagg.so*
%{_libdir}/libaggplatformX11.la
%{_libdir}/libaggplatformX11.so*
%{_libdir}/libaggplatformsdl.la
%{_libdir}/libaggplatformsdl.so*
%{_libdir}/pkgconfig/libagg.pc
%{_datadir}/aclocal/libagg.m4

%clean
rm -rf %{buildroot}

%changelog
* Sun Mar 3 2013  tanggeliang <tanggeliang@gmail.com>
- create
