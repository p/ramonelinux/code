Name:       cairo
Version:    1.14.2
Release:    3%{?dist}
Summary:    Cairo

Group:      System Environment/xlib
License:    GPLv2+
Url:        http://www.cairographics.org
Source:     http://cairographics.org/releases/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  libpng glib pixman
BuildRequires:  fontconfig
BuildRequires:  libx11 libxrender libxi

%description
Cairo is a 2D graphics library with support for multiple output devices.
Currently supported output targets include the X Window System, win32, image buffers, PostScript, PDF and SVG.
Experimental backends include OpenGL, Quartz and XCB file output.
Cairo is designed to produce consistent output on all output media while taking advantage of display hardware acceleration when available (e.g., through the X Render Extension).
The Cairo API provides operations similar to the drawing operators of PostScript and PDF.
Operations in Cairo include stroking and filling cubic Bezier splines, transforming and compositing translucent images, and antialiased text rendering.
All drawing operations can be transformed by any affine transformation (scale, rotation, shear, etc.).

%package        gl
Summary:        Cairo's OpenGL
BuildRequires:  mesa
%description    gl
Cairo's experimental OpenGL surface which is required for Wayland compositor and some other packages that are not part of BLFS.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
	    --disable-static \
            --enable-tee \
            --enable-gl \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/cairo-trace
%{_bindir}/cairo-sphinx
%{_includedir}/cairo/cairo-deprecated.h
%{_includedir}/cairo/cairo-features.h
%{_includedir}/cairo/cairo-ft.h
%{_includedir}/cairo/cairo-gobject.h
%{_includedir}/cairo/cairo-pdf.h
%{_includedir}/cairo/cairo-ps.h
%{_includedir}/cairo/cairo-script-interpreter.h
%{_includedir}/cairo/cairo-script.h
%{_includedir}/cairo/cairo-svg.h
%{_includedir}/cairo/cairo-tee.h
%{_includedir}/cairo/cairo-version.h
%{_includedir}/cairo/cairo-xcb.h
%{_includedir}/cairo/cairo-xlib-xrender.h
%{_includedir}/cairo/cairo-xlib.h
%{_includedir}/cairo/cairo.h
%{_libdir}/cairo/cairo-fdr.*
%{_libdir}/cairo/cairo-sphinx.*
%{_libdir}/cairo/libcairo-trace.*
%{_libdir}/libcairo-gobject.*
%{_libdir}/libcairo-script-interpreter.*
%{_libdir}/libcairo.*
%{_libdir}/pkgconfig/cairo-fc.pc
%{_libdir}/pkgconfig/cairo-ft.pc
%{_libdir}/pkgconfig/cairo-gobject.pc
%{_libdir}/pkgconfig/cairo-pdf.pc
%{_libdir}/pkgconfig/cairo-png.pc
%{_libdir}/pkgconfig/cairo-ps.pc
%{_libdir}/pkgconfig/cairo-script.pc
%{_libdir}/pkgconfig/cairo-svg.pc
%{_libdir}/pkgconfig/cairo-tee.pc
%{_libdir}/pkgconfig/cairo-xcb-shm.pc
%{_libdir}/pkgconfig/cairo-xcb.pc
%{_libdir}/pkgconfig/cairo-xlib-xrender.pc
%{_libdir}/pkgconfig/cairo-xlib.pc
%{_libdir}/pkgconfig/cairo.pc

%files gl
%defattr(-,root,root,-)
%{_includedir}/cairo/cairo-gl.h
%{_libdir}/pkgconfig/cairo-egl.pc
%{_libdir}/pkgconfig/cairo-gl.pc
%{_libdir}/pkgconfig/cairo-glx.pc

%files doc
%defattr(-,root,root,-)
%{_datadir}/gtk-doc/html/cairo/*

%clean
rm -rf %{buildroot}

%changelog
* Sat Mar 28 2015 tanggeliang <tanggeliang@gmail.com>
- update from 1.12.16 to 1.14.2
* Tue Sep 24 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.12.14 to 1.12.16
* Thu May 23 2013 tanggeliang <tanggeliang@gmail.com>
- add "--enable-gl" switch for Wayland.
* Sun Mar 17 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.12.8 to 1.12.14
* Tue Nov 6 2012 tanggeliang <tanggeliang@gmail.com>
- update from 1.12.6 to 1.12.8
* Sat Oct 27 2012 tanggeliang <tanggeliang@gmail.com>
- update from 1.12.4 to 1.12.6
* Sun Oct 14 2012 tanggeliang <tanggeliang@gmail.com>
- update from 1.12.2 to 1.12.4
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
