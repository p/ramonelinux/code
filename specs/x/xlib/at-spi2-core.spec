Name:       at-spi2-core
Version:    2.18.0
Release:    1%{?dist}
Summary:    At-Spi2 Core

Group:      User Interface/GNOME
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/%{name}/2.18/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  dbus glib libx11 gobject-introspection gtk-doc
BuildRequires:  intltool gettext xml-parser libxtst

%description
The At-Spi2 Core package is a part of the GNOME Accessibility Project.
It provides a Service Provider Interface for the Assisstive Technologies available on the GNOME platform and a library against which applications can be linked.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --sysconfdir=/etc \
            --libexecdir=%{_libdir}/at-spi2-core \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
/etc/at-spi2/accessibility.conf
/etc/xdg/autostart/at-spi-dbus-bus.desktop
%{_includedir}/at-spi-2.0/atspi/atspi*.h
%{_includedir}/at-spi-2.0/atspi/atspi-gmain.c
%{_libdir}/at-spi2-core/at-spi-bus-launcher
%{_libdir}/at-spi2-core/at-spi2-registryd
%{_libdir}/girepository-1.0/Atspi-2.0.typelib
%{_libdir}/libatspi.la
%{_libdir}/libatspi.so
%{_libdir}/libatspi.so.0
%{_libdir}/libatspi.so.0.0.1
%{_libdir}/pkgconfig/atspi-2.pc
%{_datadir}/dbus-1/services/org.a11y.Bus.service
%{_datadir}/dbus-1/accessibility-services/org.a11y.atspi.Registry.service
%{_datadir}/gir-1.0/Atspi-2.0.gir
%{_datadir}/locale/*

%files doc
%defattr(-,root,root,-)
%{_datadir}/gtk-doc/html/libatspi/*

%clean
rm -rf %{buildroot}

%changelog
* Tue Sep 22 2015 tanggeliang <tanggeliang@gmail.com>
- update from 2.17.90 to 2.18.0
* Tue Sep 15 2015 tanggeliang <tanggeliang@gmail.com>
- update from 2.16.0 to 2.17.90
* Sat Mar 28 2015 tanggeliang <tanggeliang@gmail.com>
- update from 2.14.0 to 2.16.0
* Thu Sep 25 2014 tanggeliang <tanggeliang@gmail.com>
- update from 2.12.0 to 2.14.0
* Sun Apr 20 2014 tanggeliang <tanggeliang@gmail.com>
- update from 2.10.2 to 2.12.0
* Tue Dec 3 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.10.0 to 2.10.2
* Tue Sep 24 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.8.0 to 2.10.0
* Thu Apr 11 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.6.3 to 2.8.0
* Tue Apr 9 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.6.1 to 2.6.3
* Mon Oct 22 2012 tanggeliang <tanggeliang@gmail.com>
- update from 2.6.0 to 2.6.1
* Fri Sep 28 2012 tanggeliang <tanggeliang@gmail.com>
- update from 2.4.2 to 2.6.0
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
