Name:       atkmm
Version:    2.23.3
Release:    1%{?dist}
Summary:    Atkmm

Group:      System Environment/xlib
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/%{name}/2.23/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  atk glibmm

%description
Atkmm is the official C++ interface for the ATK accessibility toolkit library.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_libdir}/libatkmm*
%{_libdir}/pkgconfig/atkmm-1.6.pc
%{_includedir}/atkmm-1.6/*
%{_libdir}/atkmm-1.6/*

%files doc
%defattr(-,root,root,-)
%{_docdir}/atkmm-1.6/*
%{_datadir}/devhelp/books/atkmm-1.6/*

%clean
rm -rf %{buildroot}

%changelog
* Sun Sep 20 2015 tanggeliang <tanggeliang@gmail.com>
- update from 2.22.7 to 2.23.3
* Sun Jul 7 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.22.6 to 2.22.7
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
