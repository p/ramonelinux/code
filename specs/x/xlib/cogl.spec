Name:       cogl
Version:    1.22.0
Release:    1%{?dist}
Summary:    Cogl

Group:      System Environment/xlib
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/%{name}/1.22/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  gdk-pixbuf mesa pango gobject-introspection
BuildRequires:  libxcomposite libxrandr
BuildRequires:  gtk-doc sdl
BuildRequires:  mesa-wayland

%description
Cogl is a modern 3D graphics API with associated utility APIs designed to expose the features of 3D graphics hardware using a direct state access API design, as opposed to the state-machine style of OpenGL.

%package        gst
Summary:        Gstreamer
BuildRequires:  gstreamer gst-plugins-base
%description    gst

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr 		  \
	    --enable-gles1 		  \
	    --enable-gles2 		  \
            --enable-kms-egl-platform 	  \
            --enable-wayland-egl-platform \
	    --enable-xlib-egl-platform    \
	    --enable-wayland-egl-server	  \
            --enable-cogl-gst 		  \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_includedir}/cogl/cogl-gles2/GLES2/gl2*.h
%{_includedir}/cogl/cogl-pango/cogl-pango.h
%{_includedir}/cogl/cogl-path/cogl*-path*.h
%{_includedir}/cogl/cogl/gl-prototypes/cogl-*-functions.h
%{_includedir}/cogl/cogl/cogl*.h
%{_includedir}/cogl/cogl/deprecated/cogl-*.h
%{_libdir}/girepository-1.0/Cogl-1.0.typelib
%{_libdir}/girepository-1.0/CoglPango-1.0.typelib
%{_libdir}/libcogl-gles2.*
%{_libdir}/libcogl-pango.*
%{_libdir}/libcogl-path.*
%{_libdir}/libcogl.*
%{_libdir}/girepository-1.0/Cogl-2.0.typelib
%{_libdir}/girepository-1.0/CoglPango-2.0.typelib
%{_libdir}/pkgconfig/cogl-1.0.pc
%{_libdir}/pkgconfig/cogl-2.0-experimental.pc
%{_libdir}/pkgconfig/cogl-gl-1.0.pc
%{_libdir}/pkgconfig/cogl-gles2-1.0.pc
%{_libdir}/pkgconfig/cogl-gles2-2.0-experimental.pc
%{_libdir}/pkgconfig/cogl-pango-1.0.pc
%{_libdir}/pkgconfig/cogl-pango-2.0-experimental.pc
%{_libdir}/pkgconfig/cogl-path-1.0.pc
%{_libdir}/pkgconfig/cogl-path-2.0-experimental.pc
%{_datadir}/cogl/examples-data/crate.jpg
%{_datadir}/gir-1.0/Cogl-1.0.gir
%{_datadir}/gir-1.0/Cogl-2.0.gir
%{_datadir}/gir-1.0/CoglPango-1.0.gir
%{_datadir}/gir-1.0/CoglPango-2.0.gir
%{_datadir}/locale/*

%files gst
%defattr(-,root,root,-)
%{_includedir}/cogl/cogl-gst/cogl-gst-video-sink.h
%{_includedir}/cogl/cogl-gst/cogl-gst.h
%{_libdir}/girepository-1.0/CoglGst-2.0.typelib
%{_libdir}/gstreamer-1.0/libgstcogl.la
%{_libdir}/gstreamer-1.0/libgstcogl.so
%{_libdir}/libcogl-gst.la
%{_libdir}/libcogl-gst.so*
%{_libdir}/pkgconfig/cogl-gst-1.0.pc
%{_libdir}/pkgconfig/cogl-gst-2.0-experimental.pc
%{_datadir}/gir-1.0/CoglGst-2.0.gir

%clean
rm -rf %{buildroot}

%changelog
* Wed Sep 23 2015 tanggeliang <tanggeliang@gmail.com>
- update from 1.21.2 to 1.22.0
* Sat Mar 28 2015 tanggeliang <tanggeliang@gmail.com>
- update from 1.18.2 to 1.20.0
* Sun Jul 20 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.18.0 to 1.18.2
* Sat Mar 22 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.17.4 to 1.18.0
* Sat Mar 15 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.16.0 to 1.17.4
* Wed Sep 25 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.14.0 to 1.16.0
* Sun Apr 14 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.12.2 to 1.14.0
* Mon Mar 18 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.12.0 to 1.12.2
* Fri Sep 28 2012 tanggeliang <tanggeliang@gmail.com>
- update from 1.10.4 to 1.12.0
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
