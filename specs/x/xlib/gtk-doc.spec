Name:       gtk-doc
Version:    1.24
Release:    2%{?dist}
Summary:    GTK-Doc

Group:      System Environment/xlib
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/%{name}/1.24/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  docbook-xml docbook-xsl itstool libxslt
BuildRequires:  pkg-config gettext rarian
# perl(gtkdoc-common.pl)
AutoReq:        no

%description
The GTK-Doc package contains a code documenter.
This is useful for extracting specially formatted comments from the code to create API documentation.
This package is optional; if it is not installed, packages will not build the documentation.
This does not mean that you will not have any documentation.
If GTK-Doc is not available, the install process will copy any pre-built documentation to your system.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/gtkdoc-check
%{_bindir}/gtkdoc-depscan
%{_bindir}/gtkdoc-fixxref
%{_bindir}/gtkdoc-mkdb
%{_bindir}/gtkdoc-mkhtml
%{_bindir}/gtkdoc-mkman
%{_bindir}/gtkdoc-mkpdf
%{_bindir}/gtkdoc-mktmpl
%{_bindir}/gtkdoc-rebase
%{_bindir}/gtkdoc-scan
%{_bindir}/gtkdoc-scangobj
%{_bindir}/gtkdocize
%{_datadir}/aclocal/gtk-doc.m4
%{_datadir}/gtk-doc/data/*
%{_datadir}/help/*/gtk-doc-manual/*
%{_datadir}/pkgconfig/gtk-doc.pc
%{_datadir}/sgml/gtk-doc/gtk-doc.cat

%clean
rm -rf %{buildroot}

%changelog
* Tue Sep 15 2015 tanggeliang <tanggeliang@gmail.com>
- update from 1.20 to 1.24
* Sat Mar 29 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.19 to 1.20
* Wed Aug 7 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.18 to 1.19
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
