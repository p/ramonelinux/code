Name:       qtchooser
Version:    31
Release:    1%{?dist}
Summary:    qtchooser

Group:      System Environment/xlib
License:    GPLv2+
Url:        http://www.qt.org
Source:     http://macieira.org/qtchooser/%{name}-%{version}-g980c64c.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build

%description
The qtchooser package contains a wrapper used to select between Qt binary versions.

%prep
%setup -q -n %{name}-%{version}-g980c64c

%build
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make INSTALL_ROOT=%{buildroot} install

mkdir -p %{buildroot}/usr/share/man/man1
install -m644 doc/qtchooser.1 %{buildroot}/usr/share/man/man1

%files
%defattr(-,root,root,-)
%{_bindir}/assistant
%{_bindir}/designer
%{_bindir}/lconvert
%{_bindir}/linguist
%{_bindir}/lrelease
%{_bindir}/lupdate
%{_bindir}/moc
%{_bindir}/pixeltool
%{_bindir}/qcollectiongenerator
%{_bindir}/qdbus
%{_bindir}/qdbuscpp2xml
%{_bindir}/qdbusviewer
%{_bindir}/qdbusxml2cpp
%{_bindir}/qdoc
%{_bindir}/qdoc3
%{_bindir}/qglinfo
%{_bindir}/qhelpconverter
%{_bindir}/qhelpgenerator
%{_bindir}/qmake
%{_bindir}/qml1plugindump
%{_bindir}/qmlbundle
%{_bindir}/qmlmin
%{_bindir}/qmlplugindump
%{_bindir}/qmlprofiler
%{_bindir}/qmlscene
%{_bindir}/qmltestrunner
%{_bindir}/qmlviewer
%{_bindir}/qtchooser
%{_bindir}/qtconfig
%{_bindir}/rcc
%{_bindir}/uic
%{_bindir}/uic3
%{_bindir}/xmlpatterns
%{_bindir}/xmlpatternsvalidator
%{_mandir}/man1/qtchooser.1.gz

%clean
rm -rf %{buildroot}

%changelog
* Wed Aug 28 2013 tanggeliang <tanggeliang@gmail.com>
- create
