Name:       cairomm
Version:    1.10.0
Release:    6%{?dist}
Summary:    Cairomm

Group:      System Environment/xlib
License:    GPLv2+
Url:        http://www.cairographics.org
Source:     http://cairographics.org/releases/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  cairo libsigc++

%description
The Cairomm package provides a C++ interface to Cairo.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_includedir}/cairomm-1.0/cairomm/*
%{_libdir}/cairomm-1.0/include/cairommconfig.h
%{_libdir}/libcairomm-1.0.*
%{_libdir}/pkgconfig/cairomm-*.pc

%files doc
%defattr(-,root,root,-)
%{_docdir}/cairomm-1.0/*
%{_datadir}/devhelp/books/cairomm-1.0/cairomm-1.0.devhelp2

%clean
rm -rf %{buildroot}

%changelog
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
