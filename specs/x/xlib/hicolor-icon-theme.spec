Name:       hicolor-icon-theme
Version:    0.15
Release:    1%{?dist}
Summary:    hicolor-icon-theme

Group:      System Environment/xlib
License:    GPLv2+
Url:        http://icon-theme.freedesktop.org
Source:     http://icon-theme.freedesktop.org/releases/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildArch:      noarch

%description
The hicolor-icon-theme package contains a default fallback theme for implementations of the icon theme specification.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_datadir}/icons/hicolor/index.theme

%clean
rm -rf %{buildroot}

%changelog
* Sat Mar 28 2015 tanggeliang <tanggeliang@gmail.com>
- update from 0.13 to 0.15
* Tue Mar 18 2014 tanggeliang <tanggeliang@gmail.com>
- update from 0.12 to 0.13
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
