Name:       syslinux
Version:    6.02
Release:    2%{?dist}
Summary:    Simple kernel loader which boots from a FAT filesystem

Group:      Applications/System
License:    GPLv2+
URL:        http://www.syslinux.com
Source:     http://www.kernel.org/pub/linux/utils/boot/syslinux/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  nasm
AutoReq:        no

%description
SYSLINUX is a boot loader for the Linux operating system which runs on an MS-DOS/Windows FAT filesystem.
It is intended to simplify first-time installation of Linux, and for creation of rescue and other special purpose boot disks.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
make bios %{?_smp_mflags}

%install
rm -rf %{buildroot}

mkdir -p %{buildroot}%{_bindir}
mkdir -p %{buildroot}%{_sbindir}
mkdir -p %{buildroot}%{_prefix}/lib/syslinux
mkdir -p %{buildroot}%{_includedir}
make bios install \
	INSTALLROOT=%{buildroot} BINDIR=%{_bindir} SBINDIR=%{_sbindir} \
    LIBDIR=%{_prefix}/lib DATADIR=%{_datadir} \
	MANDIR=%{_mandir} INCDIR=%{_includedir} \
	TFTPBOOT=/tftpboot EXTLINUXDIR=/boot/extlinux \
    LDLINUX=ldlinux.c32

mkdir -p %{buildroot}/%{_docdir}/%{name}-%{version}/sample
install -m 644 sample/sample.* %{buildroot}/%{_docdir}/%{name}-%{version}/sample/
mkdir -p %{buildroot}/etc
( cd %{buildroot}/etc && ln -s ../boot/extlinux/extlinux.conf . )

%files
%defattr(-,root,root)
%config /etc/extlinux.conf
%{_bindir}/gethostip
%{_bindir}/isohybrid
%{_bindir}/memdiskfind
%{_bindir}/syslinux
%{_datadir}/syslinux/*.com
%{_datadir}/syslinux/*.c32
%{_datadir}/syslinux/*.bin
%{_datadir}/syslinux/*.0
%{_datadir}/syslinux/memdisk
%{_datadir}/syslinux/dosutil/*
%{_datadir}/syslinux/diag/*
%{_datadir}/syslinux/com32/*
%{_bindir}/isohybrid.pl
%{_bindir}/keytab-lilo
%{_bindir}/lss16toppm
%{_bindir}/md5pass
%{_bindir}/mkdiskimage
%{_bindir}/ppmtolss16
%{_bindir}/pxelinux-options
%{_bindir}/sha1pass
%{_bindir}/syslinux2ansi
%{_sbindir}/extlinux

%files doc
%defattr(-,root,root)
%{_docdir}/syslinux-%{version}/sample/sample.msg
%{_mandir}/man1/*.gz

%clean
rm -rf %{buildroot}

%changelog
* Fri Jul 25 2014 tanggeliang <tanggeliang@gmail.com>
- update from 4.05 to 6.02
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
