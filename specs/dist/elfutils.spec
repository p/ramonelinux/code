Name:       elfutils
Version:    0.160
Release:    1%{?dist}
Summary:    A collection of utilities and DSOs to handle compiled objects

Group:      Development/Tools
License:    GPLv2+
Url:        https://fedorahosted.org/elfutils/
Source:     %{?source_url}%{name}-%{version}.tar.bz2

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  gettext bison flex
Requires:       elfutils-libelf

%description

%package        libelf
Summary:        libelf
%description    libelf

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q

: 'compat=%compat'
: 'portability=%portability'
: 'separate_devel_static=%separate_devel_static'
: 'scanf_has_m=%scanf_has_m'

%build
sed -i '192s/-Werror//' lib/Makefile.in
sed -i '323s/-Werror//' src/Makefile.in

# Remove -Wall from default flags.  The makefiles enable enough warnings
# themselves, and they use -Werror.  Appending -Wall defeats the cases where
# the makefiles disable some specific warnings for specific code.
RPM_OPT_FLAGS=${RPM_OPT_FLAGS/-Wall/}

%configure CFLAGS="$RPM_OPT_FLAGS -fexceptions" || {
  cat config.log
  exit 2
}
make -s %{?_smp_mflags}

%install
rm -rf ${RPM_BUILD_ROOT}
make -s install DESTDIR=${RPM_BUILD_ROOT}

chmod +x ${RPM_BUILD_ROOT}%{_prefix}/%{_lib}/lib*.so*
chmod +x ${RPM_BUILD_ROOT}%{_prefix}/%{_lib}/elfutils/lib*.so*

# XXX Nuke unpackaged files
(cd ${RPM_BUILD_ROOT}
 rm -f .%{_bindir}/eu-ld
)

rm -rf  %{buildroot}/usr/bin

%find_lang %{name}

%check

%clean
rm -rf ${RPM_BUILD_ROOT}

%files
%defattr(-,root,root)
%{_includedir}/dwarf.h
%dir %{_includedir}/elfutils
%{_includedir}/elfutils/elf-knowledge.h
%{_includedir}/elfutils/libasm.h
%{_includedir}/elfutils/libebl.h
%{_includedir}/elfutils/libdw.h
%{_includedir}/elfutils/libdwelf.h
%{_includedir}/elfutils/libdwfl.h
%{_includedir}/elfutils/version.h
%{_includedir}/libelf.h
%{_includedir}/gelf.h
%{_includedir}/nlist.h
%{_libdir}/libasm-%{version}.so
%{_libdir}/libasm.so.*
%{_libdir}/libdw-%{version}.so
%{_libdir}/libdw.so.*
%dir %{_libdir}/elfutils
%{_libdir}/elfutils/lib*.so
%{_libdir}/libebl.a
%{_libdir}/libasm.so
%{_libdir}/libdw.so
%{_libdir}/libasm.a
%{_libdir}/libdw.a
%{_libdir}/libelf.so
%{_libdir}/libelf.a
%{_datadir}/locale/*

%files libelf
%defattr(-,root,root)
%{_libdir}/libelf-%{version}.so
%{_libdir}/libelf.so.*

%files doc
%defattr(-,root,root)
%doc COPYING README TODO

%changelog
* Fri Sep 19 2014 tanggeliang <tanggeliang@gmail.com>
- update from 0.152 to 0.160
* Sun May 1 2011 tanggeliang <tanggeliang@gmail.com>
- porting from Fedora
