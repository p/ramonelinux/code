%global mockgid  135

Name:       mock
Version:    1.1.41
Release:    1%{?dist}
Summary:    Builds packages inside chroots

Group:      Development/Tools
License:    GPLv2+
Url:        http://fedoraproject.org/wiki/Projects/Mock
Source0:    https://fedorahosted.org/mock/attachment/wiki/MockTarballs/%{name}-%{version}.tar.xz
Source1:    ramone-1-i686.cfg
Source2:    ramone-1-x86_64.cfg

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildArch:      noarch
Requires:       python yum yum-utils tar pigz decoratortools usermode
Requires:       createrepo
Requires(pre):  shadow
Requires(post): coreutils

%description
Mock takes an SRPM and builds it in a chroot

%prep
%setup -q

%build
%configure
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

mkdir -p %{buildroot}/var/lib/mock
mkdir -p %{buildroot}/var/cache/mock

ln -s consolehelper %{buildroot}/usr/bin/mock
install -m 644 %{SOURCE1} %{buildroot}%{_sysconfdir}/mock/
install -m 644 %{SOURCE2} %{buildroot}%{_sysconfdir}/mock/

echo "%defattr(0644, root, mock)" > %{name}.cfgs
find %{buildroot}%{_sysconfdir}/mock -name "*.cfg" \
    | sed -e "s|^%{buildroot}|%%config(noreplace) |" >> %{name}.cfgs

ln -s ramone-1-i686.cfg %{buildroot}%{_sysconfdir}/mock/default.cfg

%clean
rm -rf %{buildroot}

%files
%defattr(-, root, root)
/etc/pki/mock/RPM-GPG-KEY-*
%{_bindir}/mock
%{_bindir}/mockchain
%attr(0755, root, root) %{_sbindir}/mock
/usr/lib/python2.7/site-packages/mockbuild/*
%dir  %{_sysconfdir}/%{name}
/etc/mock/*.cfg
%ghost %config(noreplace,missingok) %{_sysconfdir}/%{name}/default.cfg
%config(noreplace) %{_sysconfdir}/%{name}/*.ini
%config(noreplace) %{_sysconfdir}/pam.d/%{name}
%config(noreplace) %{_sysconfdir}/security/console.apps/%{name}
%{_sysconfdir}/bash_completion.d
%defattr(0775, root, mock, 02775)
%dir /var/cache/mock
%dir /var/lib/mock
%{_mandir}/man1/mock.1.gz
%{_mandir}/man1/mockchain.1.gz

%pre
# check for existence of mock group, create it if not found
getent group mock > /dev/null || groupadd -f -g %mockgid -r mock
exit 0

%post
# fix cache permissions from old installs
chmod 2775 /var/cache/mock

# TODO: use dist and version of install system, not build one
if [ ! -e %{_sysconfdir}/%{name}/default.cfg ] ; then
    ln -s ramone-1-i686.cfg %{_sysconfdir}/mock/default.cfg
fi
usermod -a -G mock `whoami` && newgrp mock
:

%postun
groupdel mock

%changelog
* Sun Jul 27 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.1.33 to 1.1.41
* Thu Sep 19 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.1.29 to 1.1.33
* Thu Mar 14 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.1.22 to 1.1.29
* Fri Sep 28 2012 tanggeliang <tanggeliang@gmail.com>
- chenge "users" to "whoami".
* Sun May 1 2011 tanggeliang <tanggeliang@gmail.com>
- porting from Fedora
