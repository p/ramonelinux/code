Name:       dracut
Version:    034
Release:    1%{?dist}
Summary:    dracut

Group:      System Environment/libraries
License:    GPLv2+
Url:        https://dracut.wiki.kernel.org
Source:     https://www.kernel.org/pub/linux/utils/boot/dracut/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  dash systemd pkg-config
Requires:       dash cpio findutils grep
Requires:       kmod systemd sysvinit-utils plymouth
Requires:       e2fsprogs jfsutils lvm ntfs-3g reiserfsprogs xfsprogs
Requires:       gzip setup iproute gawk binutils-strip

%description
Dracut contains tools to create a bootable initramfs for Linux kernels.
Unlike existing implementations, dracut does hard-code as little as possible into the initramfs.
Dracut contains various modules which are driven by the event-based udev.
Having root on MD, DM, LVM2, LUKS is supported as well as NFS, iSCSI, NBD, FCoE with the dracut-network package.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --sysconfdir=/etc \
            --systemdsystemunitdir=%{_unitdir} \
            --bashcompletiondir=$(pkg-config --variable=completionsdir bash-completion) \
            --libdir=/usr/lib &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make install \
    DESTDIR=%{buildroot} \
    sysconfdir=/etc \
    libdir=/usr/lib

%files
%defattr(-,root,root,-)
/etc/dracut.conf
%{_bindir}/dracut
%{_bindir}/dracut-catimages
%{_bindir}/lsinitrd
%{_bindir}/mkinitrd
/lib/systemd/system/dracut-cmdline.service
/lib/systemd/system/dracut-initqueue.service
/lib/systemd/system/dracut-mount.service
/lib/systemd/system/dracut-pre-mount.service
/lib/systemd/system/dracut-pre-pivot.service
/lib/systemd/system/dracut-pre-trigger.service
/lib/systemd/system/dracut-pre-udev.service
/lib/systemd/system/dracut-shutdown.service
/lib/systemd/system/initrd.target.wants/dracut-cmdline.service
/lib/systemd/system/initrd.target.wants/dracut-initqueue.service
/lib/systemd/system/initrd.target.wants/dracut-mount.service
/lib/systemd/system/initrd.target.wants/dracut-pre-mount.service
/lib/systemd/system/initrd.target.wants/dracut-pre-pivot.service
/lib/systemd/system/initrd.target.wants/dracut-pre-trigger.service
/lib/systemd/system/initrd.target.wants/dracut-pre-udev.service
/lib/systemd/system/shutdown.target.wants/dracut-shutdown.service
/usr/lib/dracut/dracut-functions
/usr/lib/dracut/dracut-functions.sh
/usr/lib/dracut/dracut-initramfs-restore
/usr/lib/dracut/dracut-install
/usr/lib/dracut/dracut-logger.sh
/usr/lib/dracut/dracut-version.sh
/usr/lib/dracut/modules.d/*
/usr/lib/kernel/install.d/50-dracut.install
/usr/lib/kernel/install.d/51-dracut-rescue.install
%{_datadir}/bash-completion/completions/dracut
%{_datadir}/bash-completion/completions/lsinitrd
%{_mandir}/man*/*.gz

%clean
rm -rf %{buildroot}

%changelog
* Sun Dec 1 2013 tanggeliang <tanggeliang@gmail.com>
- update from 031 to 034
* Tue Aug 13 2013 tanggeliang <tanggeliang@gmail.com>
- update from 030 to 031
* Tue Aug 13 2013 tanggeliang <tanggeliang@gmail.com>
- update from 019 to 030
* Sun May 1 2011 tanggeliang <tanggeliang@gmail.com>
- porting from Fedora
