Name:       opencc
Version:    0.4.3
Release:    1%{?dist}
Summary:    Libraries for Simplified-Traditional Chinese Conversion

Group:      System Environment/Libraries
License:    ASL 2.0
Url:        http://code.google.com/p/opencc/
Source:     http://opencc.googlecode.com/files/%{name}-%{version}.tar.gz
Patch:      opencc-0.3.0-fixes-cmake.patch

BuildRequires:  gettext
BuildRequires:  cmake

%description
OpenCC is a library for converting characters and phrases between Traditional Chinese and Simplified Chinese.

%prep
%setup -q
%patch -p1 -b .cmake

%build
CMAKE_PREFIX_PATH=/usr \
    cmake . -DENABLE_GETTEXT:BOOL=ON -DCMAKE_INSTALL_PREFIX=/usr -DLIB_INSTALL_DIR=%{_libdir}
make VERBOSE=1 %{?_smp_mflags}

%install
make install DESTDIR=$RPM_BUILD_ROOT

%check
ctest

%find_lang %{name}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%{_bindir}/opencc
%{_bindir}/opencc_dict
%{_includedir}/opencc/opencc*.h
%{_libdir}/libopencc.*
%{_libdir}/pkgconfig/opencc.pc
%{_datadir}/locale/zh_*/LC_MESSAGES/opencc.mo
%{_datadir}/opencc/*
%{_mandir}/man1/opencc.1.gz
%{_mandir}/man1/opencc_dict.1.gz

%changelog
* Thu Aug 15 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.3.0 to 0.4.3
* Sun May 1 2011 tanggeliang <tanggeliang@gmail.com>
- porting from Fedora
