Name:       createrepo
Version:    0.9.9
Release:    7%{?dist}
Summary:    Creates a common metadata repository

Group:      System Environment/Base
License:    GPLv2
Url:        http://createrepo.baseurl.org
Source:     %{name}-%{version}.tar.gz
Patch0:     ten-changelog-limit.patch
Patch1:     createrepo-head.patch

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
Requires:       rpm yum-metadata-parser yum deltarpm pyliblzma

%description
This utility will generate a common metadata repository from a directory of rpm packages.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q
%patch0 -p0
%patch1 -p1

%build

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} sysconfdir=%{_sysconfdir} install

%clean
rm -rf %{buildroot}

%files
%defattr(-, root, root,-)
%{_sysconfdir}/bash_completion.d/
%{_datadir}/%{name}/
%{_bindir}/createrepo
%{_bindir}/modifyrepo
%{_bindir}/mergerepo
%{python_sitelib}/createrepo

%files doc
%defattr(-, root, root,-)
%doc ChangeLog README COPYING COPYING.lib
%{_mandir}/*/*

%changelog
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
