%{!?python_sitelib: %define python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}

Name:       yum-utils
Version:    1.1.31
Release:    7%{?dist}
Summary:    Utilities based around the yum package manager

Group:      Development/Tools
License:    GPLv2+
Url:        http://yum.baseurl.org
Source:     http://yum.baseurl.org/download/yum-utils/%{name}-%{version}.tar.gz
Patch0:     yum-utils-HEAD.patch

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  gettext intltool
Requires:       yum kitchen pyyaml

%description
yum-utils is a collection of utilities and examples for the yum package manager.
It includes utilities by different authors that make yum easier and more powerful to use.
These tools include: debuginfo-install, find-repos-of-install, needs-restarting, package-cleanup, repoclosure, repodiff, repo-graph, repomanage, repoquery, repo-rss, reposync, repotrack, show-installed, show-changed-rco, verifytree, yumdownloader, yum-builddep, yum-complete-transaction, yum-config-manager, yum-debug-dump, yum-debug-restore and yum-groups-manager.

%prep
%setup -q
%patch0 -p1

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install
make -C updateonboot DESTDIR=%{buildroot} install

%find_lang %name

# Plugins to install
plugins="\
 changelog \
 fastestmirror \
 protectbase \
 versionlock \
 tsflags \
 downloadonly \
 priorities \
 refresh-updatesd \
 merge-conf \
 security \
 upgrade-helper \
 aliases \
 list-data \
 filter-data \
 tmprepo \
 verify \
 keys \
 remove-with-leaves \
 post-transaction-actions \
 rpm-warm-cache \
 auto-update-debuginfo \
 show-leaves \
 local \
 fs-snapshot \
 ps \
 puppetverify \
"

mkdir -p %{buildroot}/%{_sysconfdir}/yum/pluginconf.d/ %{buildroot}/usr/lib/yum-plugins/
mkdir -p %{buildroot}/%{_sysconfdir}/yum/post-actions

cd plugins
for plug in $plugins; do
    install -m 644 $plug/*.conf %{buildroot}/%{_sysconfdir}/yum/pluginconf.d/
    install -m 644 $plug/*.py %{buildroot}/usr/lib/yum-plugins/
    %{__python} -c "import compileall; compileall.compile_dir('%{buildroot}/usr/lib/yum-plugins', 1)"
done
install -m 644 aliases/aliases %{buildroot}/%{_sysconfdir}/yum/aliases.conf
install -m 644 versionlock/versionlock.list %{buildroot}/%{_sysconfdir}/yum/pluginconf.d/
# need for for the ghost in files section of yum-plugin-local
mkdir -p %{buildroot}/%{_sysconfdir}/yum.repos.d
touch %{buildroot}%{_sysconfdir}/yum.repos.d/_local.repo

%files -f %{name}.lang
%defattr(-, root, root)
%{_sysconfdir}/bash_completion.d
%{_bindir}/debuginfo-install
%{_bindir}/find-repos-of-install
%{_bindir}/needs-restarting
%{_bindir}/package-cleanup
%{_bindir}/repoclosure
%{_bindir}/repodiff
%{_bindir}/repomanage
%{_bindir}/repoquery
%{_bindir}/repotrack
%{_bindir}/reposync
%{_bindir}/repo-graph
%{_bindir}/repo-rss
%{_bindir}/verifytree
%{_bindir}/yumdownloader
%{_bindir}/yum-builddep
%{_bindir}/yum-config-manager
%{_bindir}/yum-debug-dump
%{_bindir}/yum-debug-restore
%{_bindir}/yum-groups-manager
%{_bindir}/show-installed
%{_bindir}/show-changed-rco
%{_sbindir}/yum-complete-transaction
%{_sbindir}/yumdb
%{python_sitelib}/yumutils/
/etc/NetworkManager/dispatcher.d/*
%config(noreplace) %{_sysconfdir}/sysconfig/yum-updateonboot
%config(noreplace) %{_sysconfdir}/yum/pluginconf.d/changelog.conf
%config(noreplace) %{_sysconfdir}/yum/pluginconf.d/fastestmirror.conf
%config(noreplace) %{_sysconfdir}/yum/pluginconf.d/protectbase.conf
%config(noreplace) %{_sysconfdir}/yum/pluginconf.d/versionlock.conf
%config(noreplace) %{_sysconfdir}/yum/pluginconf.d/versionlock.list
%config(noreplace) %{_sysconfdir}/yum/pluginconf.d/tsflags.conf
%config(noreplace) %{_sysconfdir}/yum/pluginconf.d/downloadonly.conf
%config(noreplace) %{_sysconfdir}/yum/pluginconf.d/priorities.conf
%config(noreplace) %{_sysconfdir}/yum/pluginconf.d/refresh-updatesd.conf
%config(noreplace) %{_sysconfdir}/yum/pluginconf.d/merge-conf.conf
%config(noreplace) %{_sysconfdir}/yum/pluginconf.d/security.conf
%config(noreplace) %{_sysconfdir}/yum/pluginconf.d/upgrade-helper.conf
%config(noreplace) %{_sysconfdir}/yum/pluginconf.d/aliases.conf
%config(noreplace) %{_sysconfdir}/yum/aliases.conf
%config(noreplace) %{_sysconfdir}/yum/pluginconf.d/list-data.conf
%config(noreplace) %{_sysconfdir}/yum/pluginconf.d/filter-data.conf
%config(noreplace) %{_sysconfdir}/yum/pluginconf.d/tmprepo.conf
%config(noreplace) %{_sysconfdir}/yum/pluginconf.d/verify.conf
%config(noreplace) %{_sysconfdir}/yum/pluginconf.d/keys.conf
%config(noreplace) %{_sysconfdir}/yum/pluginconf.d/remove-with-leaves.conf
%config(noreplace) %{_sysconfdir}/yum/pluginconf.d/post-transaction-actions.conf
%dir %{_sysconfdir}/yum/post-actions
%config(noreplace) %{_sysconfdir}/yum/pluginconf.d/rpm-warm-cache.conf
%config(noreplace) %{_sysconfdir}/yum/pluginconf.d/auto-update-debuginfo.conf
%config(noreplace) %{_sysconfdir}/yum/pluginconf.d/show-leaves.conf
%ghost %{_sysconfdir}/yum.repos.d/_local.repo
%config(noreplace) %{_sysconfdir}/yum/pluginconf.d/local.conf
%config(noreplace) %{_sysconfdir}/yum/pluginconf.d/fs-snapshot.conf
%config(noreplace) %{_sysconfdir}/yum/pluginconf.d/ps.conf
%config(noreplace) %{_sysconfdir}/yum/pluginconf.d/puppetverify.conf
%{_initrddir}/yum-updateonboot
/usr/lib/yum-plugins/changelog.*
/usr/lib/yum-plugins/fastestmirror*.*
/usr/lib/yum-plugins/protectbase.*
/usr/lib/yum-plugins/versionlock.*
/usr/lib/yum-plugins/tsflags.*
/usr/lib/yum-plugins/downloadonly.*
/usr/lib/yum-plugins/priorities.*
/usr/lib/yum-plugins/refresh-updatesd.*
/usr/lib/yum-plugins/merge-conf.*
/usr/lib/yum-plugins/security.*
/usr/lib/yum-plugins/upgrade-helper.*
/usr/lib/yum-plugins/aliases.*
/usr/lib/yum-plugins/list-data.*
/usr/lib/yum-plugins/filter-data.*
/usr/lib/yum-plugins/tmprepo.*
/usr/lib/yum-plugins/verify.*
/usr/lib/yum-plugins/keys.*
/usr/lib/yum-plugins/remove-with-leaves.*
/usr/lib/yum-plugins/post-transaction-actions.*
/usr/lib/yum-plugins/rpm-warm-cache.*
/usr/lib/yum-plugins/auto-update-debuginfo.*
/usr/lib/yum-plugins/show-leaves.*
/usr/lib/yum-plugins/local.*
/usr/lib/yum-plugins/fs-snapshot.*
/usr/lib/yum-plugins/ps.*
/usr/lib/yum-plugins/puppetverify.*
%doc
%{_mandir}/man*/*.gz

%clean
rm -rf %{buildroot}

%changelog
* Sun May 1 2011 tanggeliang <tanggeliang@gmail.com>
- porting from Fedora
