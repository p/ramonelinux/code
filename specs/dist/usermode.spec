Name:       usermode
Version:    1.111
Release:    2%{?dist}
Summary:    Tools for certain user account management tasks

Group:      Applications/System
License:    GPLv2+
Url:        https://fedorahosted.org/usermode/
Source:     https://fedorahosted.org/releases/u/s/usermode/usermode-%{version}.tar.xz
Source1:    config-util

BuildRequires:  desktop-file-utils gettext glib gtk2 intltool
BuildRequires:  libsm libuser
BuildRequires:  linux-pam xml-parser startup-notification
BuildRequires:  util-linux
Requires:       linux-pam util-linux

%description
The usermode package contains the userhelper program, which can be used to allow configured programs to be run with superuser privileges by ordinary users.

%package        gtk
Summary:        Graphical tools for certain user account management tasks
Group:          Applications/System
Requires:       %{name} = %{version}-%{release}
%description    gtk
The usermode-gtk package contains several graphical tools for users: userinfo, usermount and userpasswd.  Userinfo allows users to change their finger information.  Usermount lets users mount, unmount, and format file systems.  Userpasswd allows users to change their passwords.
Install the usermode-gtk package if you would like to provide users with graphical tools for certain account management tasks.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q

%build
%configure

make %{?_smp_mflags}

%install
make install DESTDIR=%{buildroot} INSTALL='install -p'

# make userformat symlink to usermount
ln -sf usermount %{buildroot}/%{_bindir}/userformat
ln -s usermount.1 %{buildroot}/%{_mandir}/man1/userformat.1

mkdir -p %{buildroot}/etc/security/console.apps
install -p -m 644 %{SOURCE1} \
    %{buildroot}/etc/security/console.apps/config-util

for i in redhat-userinfo.desktop redhat-userpasswd.desktop \
    redhat-usermount.desktop; do
    echo 'NotShowIn=GNOME;KDE;' >>%{buildroot}/%{_datadir}/applications/$i
    desktop-file-install --vendor redhat --delete-original \
        --dir %{buildroot}/%{_datadir}/applications \
        %{buildroot}/%{_datadir}/applications/$i
done

%find_lang %{name}

%clean
rm -rf %{buildroot}

%files -f %{name}.lang
%doc COPYING ChangeLog NEWS README
%attr(4711,root,root) /usr/sbin/userhelper
%{_bindir}/consolehelper
%config(noreplace) /etc/security/console.apps/config-util

%files gtk
%{_bindir}/usermount
%{_bindir}/userformat
%{_bindir}/userinfo
%{_bindir}/userpasswd
%{_bindir}/consolehelper-gtk
%{_bindir}/pam-panel-icon
%{_datadir}/%{name}
%{_datadir}/pixmaps/*
%{_datadir}/applications/*

%files doc
%defattr(-,root,root,-)
%{_mandir}/man1/pam-panel-icon.1*
%{_mandir}/man1/usermount.1*
%{_mandir}/man1/userformat.1*
%{_mandir}/man1/userinfo.1*
%{_mandir}/man1/userpasswd.1*
%{_mandir}/man8/consolehelper-gtk.8*
%{_mandir}/man8/consolehelper.8*
%{_mandir}/man8/userhelper.8*

%changelog
* Wed Mar 13 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.109 to 1.111
* Sun May 1 2011 tanggeliang <tanggeliang@gmail.com>
- porting from Fedora
