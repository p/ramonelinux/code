Name:       livecd-tools
Version:    21.2
Release:    1%{?dist}
Summary:    Tools for building live CDs

Group:      System Environment/Base
License:    GPLv2
URL:        http://git.fedorahosted.org/git/livecd
Source:     http://fedorahosted.org/releases/l/i/livecd/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  /usr/bin/pod2man
Requires:       cdrkit isomd5sum parted pyparted util-linux dosfstools
Requires:       e2fsprogs mtools yum squashfs pykickstart
Requires:       system-config-keyboard urlgrabber dbus-python

%description 
Tools for generating live CDs on Fedora based systems including derived distributions such as RHEL, CentOS and others.
See http://fedoraproject.org/wiki/FedoraLiveCD for more details.

%prep
%setup -q

%build
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%{_bindir}/livecd-creator
%{_bindir}/livecd-iso-to-disk
%{_bindir}/livecd-iso-to-pxeboot
%{_bindir}/image-creator
%{_bindir}/liveimage-mount
%{_bindir}/edit-livecd
%{_bindir}/mkbiarch
/usr/lib/python2.7/site-packages/imgcreate/*.py*
%{_docdir}/livecd-tools/*
%{_mandir}/man*/*

%changelog
* Sun Jul 27 2014 tanggeliang <tanggeliang@gmail.com>
- update from 20.0 to 21.2
* Wed Oct 2 2013 tanggeliang <tanggeliang@gmail.com>
- update from 19.6 to 20.0
* Tue Jul 23 2013 tanggeliang <tanggeliang@gmail.com>
- update from 18.11 to 19.6
