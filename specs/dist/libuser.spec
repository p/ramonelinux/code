Name:       libuser
Version:    0.58
Release:    2%{?dist}
Summary:    A user and group account administration library

Group:      System Environment/Base
License:    LGPLv2+
Url:        https://fedorahosted.org/libuser/
Source:     https://fedorahosted.org/releases/l/i/libuser/libuser-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  glib linux-pam popt openssl linuxdoc-tools opensp groff
BuildRequires:  autoconf automake m4 libtool gettext

%description
The libuser library implements a standardized interface for manipulating and administering user and group accounts.
The library uses pluggable back-ends to interface to its data sources.

Sample applications modeled after those included with the shadow password suite are included.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
libtoolize --force
autopoint
aclocal -I m4
autoconf -Wall
autoheader -Wall
automake -Wall --add-missing
%configure --with-html-dir=%{_datadir}/gtk-doc/html
make

%clean
rm -fr %{buildroot}

%install
make install DESTDIR=%{buildroot} INSTALL='install -p'

%check

%files
%defattr(-,root,root)
/etc/libuser.conf
%attr(0755,root,root) %{_bindir}/lchfn
%attr(0755,root,root) %{_bindir}/lchsh
%{_includedir}/libuser/*.h
%{_libdir}/libuser.*
%{_libdir}/libuser/libuser_files.*
%{_libdir}/libuser/libuser_shadow.*
%{_libdir}/pkgconfig/libuser.pc
%{_libdir}/python2.7/site-packages/libusermodule.*
%attr(0755,root,root) %{_sbindir}/lchage
%attr(0755,root,root) %{_sbindir}/lgroupadd
%attr(0755,root,root) %{_sbindir}/lgroupdel
%attr(0755,root,root) %{_sbindir}/lgroupmod
%attr(0755,root,root) %{_sbindir}/lid
%attr(0755,root,root) %{_sbindir}/lnewusers
%attr(0755,root,root) %{_sbindir}/lpasswd
%attr(0755,root,root) %{_sbindir}/luseradd
%attr(0755,root,root) %{_sbindir}/luserdel
%attr(0755,root,root) %{_sbindir}/lusermod
%{_datadir}/locale/*/LC_MESSAGES/libuser.mo

%files doc
%defattr(-,root,root)
%{_datadir}/gtk-doc/html/libuser/*
%{_mandir}/man*/*.gz

%changelog
* Fri Mar 15 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.57.6 to 0.58
* Sun May 1 2011 tanggeliang <tanggeliang@gmail.com>
- porting from Fedora
