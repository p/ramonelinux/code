Name:       yum
Version:    3.4.3
Release:    10%{?dist}
Summary:    RPM package installer/updater/manager

Group:      System Environment/Base
License:    GPLv2+
Url:        http://yum.baseurl.org/
Source0:    http://yum.baseurl.org/download/3.4/%{name}-%{version}.tar.gz
Source1:    yum.conf.fedora

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildArch:      noarch
BuildRequires:  gettext intltool
BuildRequires:  urlgrabber yum-metadata-parser
Requires:       urlgrabber yum-metadata-parser rpm pycurl

%description
Yum is a utility that can check for and automatically download and install updated RPM packages.
Dependencies are obtained and downloaded automatically, prompting the user for permission as necessary.

%prep
%setup -q

%build
make

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

install -m 644 %{SOURCE1} %{buildroot}/%{_sysconfdir}/yum.conf
mkdir -p %{buildroot}/%{_sysconfdir}/yum/pluginconf.d %{buildroot}/usr/lib/yum-plugins
mkdir -p %{buildroot}/usr/share/yum-plugins

# for now, move repodir/yum.conf back
mv %{buildroot}/%{_sysconfdir}/yum/repos.d %{buildroot}/%{_sysconfdir}/yum.repos.d
rm -f %{buildroot}/%{_sysconfdir}/yum/yum.conf

# yum-updatesd has moved to the separate source version
rm -f %{buildroot}/%{_sysconfdir}/yum/yum-updatesd.conf 
rm -f %{buildroot}/%{_sysconfdir}/rc.d/init.d/yum-updatesd
rm -f %{buildroot}/%{_sysconfdir}/dbus-1/system.d/yum-updatesd.conf
rm -f %{buildroot}/%{_sbindir}/yum-updatesd
rm -f %{buildroot}/%{_mandir}/man*/yum-updatesd*
rm -f %{buildroot}/%{_datadir}/yum-cli/yumupd.py*

# Ghost files:
mkdir -p %{buildroot}/var/lib/yum/history
mkdir -p %{buildroot}/var/lib/yum/plugins
mkdir -p %{buildroot}/var/lib/yum/yumdb
touch %{buildroot}/var/lib/yum/uuid

# rpmlint bogus stuff...
chmod +x %{buildroot}/%{_datadir}/yum-cli/*.py
chmod +x %{buildroot}/%{python_sitelib}/yum/*.py
chmod +x %{buildroot}/%{python_sitelib}/rpmUtils/*.py

%find_lang %name

%clean
rm -rf %{buildroot}

%post
if [ "$1" -ge "1" ]; then
 if [ -f /etc/init.d/yum ]; then 
  RETVAL=$?
  if [ $RETVAL = 0 ]; then
   /sbin/service yum stop 1> /dev/null 2>&1
   /sbin/service yum-cron start 1> /dev/null 2>&1
  fi
 fi
fi 
exit 0

%preun
if [ $1 = 0 ]; then
 /sbin/service yum-cron stop 1> /dev/null 2>&1
fi
exit 0

%postun
if [ "$1" -ge "1" ]; then
 /sbin/service yum-cron condrestart 1> /dev/null 2>&1
fi
exit 0

%files -f %{name}.lang
%defattr(-, root, root, -)
%{_sysconfdir}/bash_completion.d
%{_sysconfdir}/cron.daily/0yum.cron
%{_sysconfdir}/rc.d/init.d/yum-cron
%config(noreplace) %{_sysconfdir}/yum.conf
%config(noreplace) %{_sysconfdir}/yum/version-groups.conf
%config(noreplace) %{_sysconfdir}/logrotate.d/yum
%config(noreplace) %{_sysconfdir}/yum/yum-daily.yum
%config(noreplace) %{_sysconfdir}/yum/yum-weekly.yum
%config(noreplace) %{_sysconfdir}/sysconfig/yum-cron
%dir %{_sysconfdir}/yum
%dir %{_sysconfdir}/yum/protected.d
%dir %{_sysconfdir}/yum.repos.d
%dir %{_sysconfdir}/yum/vars
%dir %{_sysconfdir}/yum/pluginconf.d 
%dir %{_datadir}/yum-cli
%{_bindir}/yum
%{python_sitelib}/yum
%{python_sitelib}/rpmUtils
%dir /var/cache/yum
%dir /var/lib/yum
%ghost /var/lib/yum/uuid
%ghost /var/lib/yum/history
%ghost /var/lib/yum/plugins
%ghost /var/lib/yum/yumdb
%dir /usr/lib/yum-plugins
%dir %{_datadir}/yum-plugins
%{_datadir}/yum-cli/*
%doc
%{_mandir}/man*/yum.*
%{_mandir}/man*/yum-shell*

%changelog
* Sun May 1 2011 tanggeliang <tanggeliang@gmail.com>
- porting from Fedora
