%define abs2rel() perl -MFile::Spec -e 'print File::Spec->abs2rel(@ARGV)' %1 %2
%global relccache %(%abs2rel %{_bindir}/ccache %{_libdir}/ccache)

Name:           ccache
Version:        3.1.9
Release:        1%{?dist}
Summary:        C/C++ compiler cache

Group:          Development/Tools
License:        GPLv3+
URL:            http://ccache.samba.org/
Source0:        http://samba.org/ftp/ccache/%{name}-%{version}.tar.xz
Source1:        %{name}.sh.in
Source2:        %{name}.csh.in
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  perl zlib
# coreutils for triggerin, triggerpostun
Requires: coreutils

%description
ccache is a compiler cache.
It speeds up recompilation of C/C++ code by caching previous compiles and detecting when the same compile is being done again.
The main focus is to handle the GNU C/C++ compiler(GCC), but it may also work with compilers that mimic GCC good enough.

%prep
%setup -q
sed -e 's|@LIBDIR@|%{_libdir}|g' -e 's|@CACHEDIR@|%{_var}/cache/ccache|g' \
    %{SOURCE1} > %{name}.sh
sed -e 's|@LIBDIR@|%{_libdir}|g' -e 's|@CACHEDIR@|%{_var}/cache/ccache|g' \
    %{SOURCE2} > %{name}.csh
# Make sure system zlib is used
rm -r zlib

%build
%configure
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

install -dm 755 %{buildroot}%{_sysconfdir}/profile.d
install -pm 644 %{name}.sh %{name}.csh %{buildroot}%{_sysconfdir}/profile.d

install -dm 770 %{buildroot}%{_var}/cache/ccache

# %%ghost files for ownership, keep in sync with triggers
install -dm 755 %{buildroot}%{_libdir}/ccache
for n in cc gcc g++ c++ ; do
    ln -s %{relccache} %{buildroot}%{_libdir}/ccache/$n
    for p in avr- arm-gp2x-linux- msp430- ; do
        ln -s %{relccache} %{buildroot}%{_libdir}/ccache/$p$n
    done
    for s in 32 34 4 44 ; do
        ln -s %{relccache} %{buildroot}%{_libdir}/ccache/$n$s
    done
    for a in %{archs} ; do
        ln -s %{relccache} \
            %{buildroot}%{_libdir}/ccache/$a-%{_vendor}-%{_target_os}-$n
    done
done
find %{buildroot}%{_libdir}/ccache -type l | \
    sed -e "s|^%{buildroot}|%%ghost |" > %{name}-%{version}.compilers

%check

%clean
rm -fr %{buildroot}

%define ccache_trigger(p:) \
%triggerin -- %{-p*}\
for n in %* ; do\
    [ ! -x %{_bindir}/$n ] || ln -sf %{relccache} %{_libdir}/ccache/$n\
    for a in %{archs} ; do\
        [ ! -x %{_bindir}/$a-%{_vendor}-%{_target_os}-$n ] || \\\
          ln -sf %{relccache} %{_libdir}/ccache/$a-%{_vendor}-%{_target_os}-$n\
    done\
done\
:\
%triggerpostun -- %{-p*}\
for n in %* ; do\
    [ -x %{_bindir}/$n ] || rm -f %{_libdir}/ccache/$n\
    for a in %{archs} ; do\
        [ -x %{_bindir}/$a-%{_vendor}-%{_target_os}-$n ] || \\\
            rm -f %{_libdir}/ccache/$a-%{_vendor}-%{_target_os}-$n\
    done\
done\
:\
%{nil}

%ccache_trigger -p arm-gp2x-linux-gcc arm-gp2x-linux-cc arm-gp2x-linux-gcc
%ccache_trigger -p arm-gp2x-linux-gcc-c++ arm-gp2x-linux-c++ arm-gp2x-linux-g++
%ccache_trigger -p avr-gcc avr-cc avr-gcc
%ccache_trigger -p avr-gcc-c++ avr-c++ avr-g++
%ccache_trigger -p compat-gcc-32 cc32 gcc32
%ccache_trigger -p compat-gcc-32-c++ c++32 g++32
%ccache_trigger -p compat-gcc-34 cc34 gcc34
%ccache_trigger -p compat-gcc-34-c++ c++34 g++34
%ccache_trigger -p gcc cc gcc
%ccache_trigger -p gcc-c++ c++ g++
%ccache_trigger -p gcc4 cc4 gcc4
%ccache_trigger -p gcc4-c++ c++4 g++4
%ccache_trigger -p gcc44 cc4 gcc44
%ccache_trigger -p gcc44-c++ c++44 g++44
%ccache_trigger -p mingw32-gcc i686-pc-mingw32-cc i686-pc-mingw32-gcc i686-w64-mingw32-gcc
%ccache_trigger -p mingw32-gcc-c++ i686-pc-mingw32-c++ i686-pc-mingw32-g++ i686-w64-mingw32-c++ i686-w64-mingw32-g++
%ccache_trigger -p mingw64-gcc i686-w64-mingw32-gcc x86_64-w64-mingw32-gcc
%ccache_trigger -p mingw64-gcc-c++ i686-w64-mingw32-c++ i686-w64-mingw32-g++ x86_64-w64-mingw32-c++ x86_64-w64-mingw32-g++
%ccache_trigger -p msp430-gcc msp430-cc msp430-gcc

%pre
getent group ccache >/dev/null || groupadd -r ccache || :

%files -f %{name}-%{version}.compilers
%defattr(-,root,root,-)
%doc AUTHORS.* GPL-3.0.txt LICENSE.* MANUAL.* NEWS.* README.*
%config(noreplace) %{_sysconfdir}/profile.d/%{name}.*sh
%{_bindir}/ccache
%dir %{_libdir}/ccache/
%attr(2770,root,ccache) %dir %{_var}/cache/ccache/
%{_mandir}/man1/ccache.1*

%changelog
* Mon Apr 8 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.1.8 to 3.1.9
* Sat Sep 29 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.1.6 to 3.1.8
* Sun May 1 2011 tanggeliang <tanggeliang@gmail.com>
- porting from Fedora
