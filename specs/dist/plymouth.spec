Name:       plymouth
Version:    0.8.8
Release:    4%{?dist}
Summary:    Graphical Boot Animation and Logger

Group:      System Environment/Base
License:    GPLv2+
Url:        http://plymouth.freedesktop.org
Source0:    http://freedesktop.org/software/plymouth/releases/%{name}-%{version}.tar.bz2
Source1:    boot-duration
Source2:    charge.plymouth

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  libdrm
BuildRequires:  pcre gtk+ gtk2 libpng pango cairo systemd

%description
Plymouth provides an attractive graphical boot animation in place of the text messages that normally get shown.
Text messages are instead redirected to a log file for viewing after boot.

%prep
%setup -q

# Change the default theme
sed -i -e 's/fade-in/charge/g' src/plymouthd.defaults

%build
./configure --prefix=/usr --exec-prefix=/usr --bindir=/bin --sbindir=/sbin --sysconfdir=/etc --datadir=/usr/share --includedir=/usr/include --libdir=%{_libdir} --libexecdir=/usr/libexec --localstatedir=/var --sharedstatedir=/var/lib --mandir=/usr/share/man --infodir=/usr/share/info \
           --enable-tracing --disable-tests                      \
           --with-logo=%{_datadir}/pixmaps/system-logo-white.png \
           --with-background-start-color-stop=0x0073B3           \
           --with-background-end-color-stop=0x00457E             \
           --with-background-color=0x3391cd                      \
           --disable-gdm-transition                              \
           --enable-systemd-integration                          \
           --without-system-root-install                         \
           --with-rhgb-compat-link                               \
           --without-log-viewer                                  \
           --disable-libkms
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make install DESTDIR=%{buildroot}

# Glow isn't quite ready for primetime
rm -rf %{buildroot}%{_datadir}/plymouth/glow/
rm -f %{buildroot}%{_libdir}/plymouth/glow.so

find %{buildroot} -name '*.a' -exec rm -f {} \;
find %{buildroot} -name '*.la' -exec rm -f {} \;

mkdir -p %{buildroot}%{_localstatedir}/lib/plymouth
cp $RPM_SOURCE_DIR/boot-duration %{buildroot}%{_datadir}/plymouth/default-boot-duration
cp $RPM_SOURCE_DIR/boot-duration %{buildroot}%{_localstatedir}/lib/plymouth

# Add charge, our new default
mkdir -p %{buildroot}%{_datadir}/plymouth/themes/charge
cp %{SOURCE2} %{buildroot}%{_datadir}/plymouth/themes/charge
cp %{buildroot}%{_datadir}/plymouth/themes/glow/{box,bullet,entry,lock}.png %{buildroot}%{_datadir}/plymouth/themes/charge

# Drop glow, it's not very Fedora-y
rm -rf %{buildroot}%{_datadir}/plymouth/themes/glow

%clean
rm -rf %{buildroot}

%post
[ -f %{_localstatedir}/lib/plymouth/boot-duration ] || cp -f %{_datadir}/plymouth/default-boot-duration %{_localstatedir}/lib/plymouth/boot-duration

%postun
if [ $1 -eq 0 ]; then
    rm -f %{_libdir}/plymouth/default.so
    rm -f /boot/initrd-plymouth.img
fi

%files
%defattr(-, root, root)
%doc AUTHORS NEWS README
%dir %{_datadir}/plymouth
%dir %{_datadir}/plymouth/themes
%dir %{_datadir}/plymouth/themes/details
%dir %{_datadir}/plymouth/themes/text
%dir %{_libexecdir}/plymouth
%dir %{_localstatedir}/lib/plymouth
%dir %{_libdir}/plymouth/renderers
%dir %{_sysconfdir}/plymouth
%config(noreplace) %{_sysconfdir}/plymouth/plymouthd.conf
/bin/plymouth
/bin/rhgb-client
/sbin/plymouthd
/sbin/plymouth-set-default-theme
%{_libdir}/plymouth/details.so
%{_libdir}/plymouth/text.so
%{_libdir}/plymouth/renderers/drm*
%{_libdir}/plymouth/renderers/frame-buffer*
%{_datadir}/plymouth/default-boot-duration
%{_datadir}/plymouth/themes/details/details.plymouth
%{_datadir}/plymouth/themes/text/text.plymouth
%{_datadir}/plymouth/plymouthd.defaults
%{_localstatedir}/run/plymouth
%{_localstatedir}/spool/plymouth
%ghost %{_localstatedir}/lib/plymouth/boot-duration
%{_includedir}/plymouth-1
%{_libdir}/libply.so
%{_libdir}/libply-splash-core.so
%{_libdir}/libply-boot-client.so
%{_libdir}/libply-splash-graphics.so
%{_libdir}/pkgconfig/ply-splash-core.pc
%{_libdir}/pkgconfig/ply-splash-graphics.pc
%{_libdir}/pkgconfig/ply-boot-client.pc
%{_libdir}/plymouth/renderers/x11*
%{_libdir}/libply.so.*
%{_libdir}/libply-splash-core.so.*
%{_libdir}/libply-boot-client.so.*
%{_libdir}/libply-splash-graphics.so.*
%{_libdir}/plymouth/label.so
%{_libdir}/plymouth/fade-throbber.so
%{_libdir}/plymouth/throbgress.so
%{_libdir}/plymouth/space-flares.so
%{_libdir}/plymouth/two-step.so
%{_libdir}/plymouth/script.so
%{_libexecdir}/plymouth/plymouth-update-initrd
%{_libexecdir}/plymouth/plymouth-generate-initrd
%{_libexecdir}/plymouth/plymouth-populate-initrd
%{_datadir}/plymouth/themes/fade-in/*.png
%{_datadir}/plymouth/themes/fade-in/fade-in.plymouth
%{_datadir}/plymouth/themes/spinner/*.png
%{_datadir}/plymouth/themes/spinner/spinner.plymouth
%{_datadir}/plymouth/themes/spinfinity/*.png
%{_datadir}/plymouth/themes/spinfinity/spinfinity.plymouth
%{_datadir}/plymouth/themes/solar/*.png
%{_datadir}/plymouth/themes/solar/solar.plymouth
%{_datadir}/plymouth/themes/charge/*.png
%{_datadir}/plymouth/themes/charge/charge.plymouth
%{_datadir}/plymouth/themes/script/*.png
%{_datadir}/plymouth/themes/script/script.script
%{_datadir}/plymouth/themes/script/script.plymouth
/lib/systemd/system/*.target.wants/plymouth-*.service
/lib/systemd/system/plymouth-*.service
/lib/systemd/system/systemd-ask-password-plymouth.path
/lib/systemd/system/systemd-ask-password-plymouth.service
%{_mandir}/man?/*

%changelog
* Wed Jun 12 2013 tanggeliang <tanggeliang@gmail.com>
- remove system-logos requires
* Sun Jun 9 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.8.7 to 0.8.8
* Sun May 1 2011 tanggeliang <tanggeliang@gmail.com>
- porting from Fedora
