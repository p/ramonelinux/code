Name:       pv
Version:    1.3.4
Release:    6%{?dist}
Summary:    A tool for monitoring the progress of data through a pipeline

Group:      Development/Tools
License:    Artistic 2.0
URL:        http://www.ivarch.com/programs/pv.shtml
Source:     http://www.ivarch.com/programs/sources/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  gettext

%description
PV ("Pipe Viewer") is a tool for monitoring the progress of data through a pipeline.
It can be inserted into any normal pipeline between two processes to give a visual indication of how quickly data is passing through, how long it has taken, how near to completion it is, and an estimate of how long it will be until completion.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --mandir=/usr/share/man &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}

mkdir -p ${buildroot}/%{_bindir}
mkdir -p ${buildroot}/%{_mandir}/man1

make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/pv
%{_datadir}/locale/*/LC_MESSAGES/pv.mo
%doc
%{_mandir}/man1/%{name}.1.gz

%clean
rm -rf %{buildroot}

%changelog
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
