Name:       rpm
Version:    4.12.0.1
Release:    1%{?dist}
Summary:    The RPM Package Manager

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://rpm.org
Source:     http://rpm.org/releases/rpm-4.12.x/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  gawk elfutils readline zlib nss popt file gettext ncurses bzip python acl db xz libarchive
Requires:       bzip cpio diffutils gzip unzip xz

%description
The RPM Package Manager (RPM) is a powerful command line driven package management system capable of installing, uninstalling, verifying, querying, and updating computer software packages.
Each software package consists of an archive of files along with information about the package like its version, a description, and the like.
There is also a library API, permitting advanced developers to manage such transactions from programming languages such as C or Python.

%prep
%setup -q -n %{name}-%{version}

%build
CPPFLAGS="$CPPFLAGS `pkg-config --cflags nspr nss`"
export CPPFLAGS
./configure \
    --prefix=%{_usr} \
    --sysconfdir=%{_sysconfdir} \
    --localstatedir=%{_var} \
    --sharedstatedir=%{_var}/lib \
    --libdir=%{_libdir} \
    --with-vendor=ramone \
    --with-external-db \
    --without-lua \
    --without-cap \
    --with-acl \
    --enable-python &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

chmod 1777 %{buildroot}/var/tmp

mkdir -p %{buildroot}/etc/rpm
echo "%%dist .ram1.0" > %{buildroot}/etc/rpm/macros.dist

# avoid dragging in tonne of perl libs for an unused script
# perl(Module::ScanDeps::DataFeed)
chmod 0644 %{buildroot}/usr/lib/rpm/perldeps.pl

%files
%defattr(-,root,root,-)
%{_sysconfdir}/rpm/macros.dist
/bin/rpm
%{_bindir}/gendiff
%{_bindir}/rpm2archive
%{_bindir}/rpm2cpio
%{_bindir}/rpmbuild
%{_bindir}/rpmdb
%{_bindir}/rpmgraph
%{_bindir}/rpmkeys
%{_bindir}/rpmquery
%{_bindir}/rpmsign
%{_bindir}/rpmspec
%{_bindir}/rpmverify
%{_includedir}/rpm/*.h
%{_libdir}/librpm.*
%{_libdir}/librpmbuild.*
%{_libdir}/librpmio.*
%{_libdir}/librpmsign.*
%{_libdir}/pkgconfig/rpm.pc
%{_libdir}/python2.7/site-packages/rpm/*
%{_libdir}/rpm-plugins/syslog.*
/usr/lib/rpm/appdata.prov
/usr/lib/rpm/brp-compress
/usr/lib/rpm/brp-java-gcjcompile
/usr/lib/rpm/brp-python-bytecompile
/usr/lib/rpm/brp-python-hardlink
/usr/lib/rpm/brp-strip*
/usr/lib/rpm/check-*
/usr/lib/rpm/config.*
/usr/lib/rpm/debugedit
/usr/lib/rpm/desktop-file.prov
/usr/lib/rpm/elfdeps
/usr/lib/rpm/fileattrs/*.attr
/usr/lib/rpm/find-debuginfo.sh
/usr/lib/rpm/find-lang.sh
/usr/lib/rpm/find-provides
/usr/lib/rpm/find-requires
/usr/lib/rpm/fontconfig.prov
/usr/lib/rpm/libtooldeps.sh
/usr/lib/rpm/macros*
/usr/lib/rpm/mkinstalldirs
/usr/lib/rpm/mono-find-provides
/usr/lib/rpm/mono-find-requires
/usr/lib/rpm/ocaml-find-provides.sh
/usr/lib/rpm/ocaml-find-requires.sh
/usr/lib/rpm/osgideps.pl
/usr/lib/rpm/perl.prov
/usr/lib/rpm/perl.req
/usr/lib/rpm/perldeps.pl
/usr/lib/rpm/pkgconfigdeps.sh
/usr/lib/rpm/platform/*
/usr/lib/rpm/pythondeps.sh
/usr/lib/rpm/rpm.daily
/usr/lib/rpm/rpm.log
/usr/lib/rpm/rpm.supp
/usr/lib/rpm/rpm2cpio.sh
/usr/lib/rpm/rpmdb_loadcvt
/usr/lib/rpm/rpmdeps
/usr/lib/rpm/rpmpopt-%{version}
/usr/lib/rpm/rpmrc
/usr/lib/rpm/script.req
/usr/lib/rpm/tcl.req
/usr/lib/rpm/tgpg
%{_datadir}/locale/*
%{_mandir}/*
%dir %{_var}/tmp

%clean
rm -rf %{buildroot}

%changelog
* Fri Sep 19 2014 tanggeliang <tanggeliang@gmail.com>
- update from 4.11.2 to 4.12.0.1
* Thu Jul 24 2014 tanggeliang <tanggeliang@gmail.com>
- update from 4.11.1 to 4.11.2
* Mon Sep 23 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.11.0.1 to 4.11.1
* Tue Mar 26 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.10.1 to 4.11.0.1
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
