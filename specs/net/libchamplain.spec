Name:       libchamplain
Version:    0.12.11
Release:    1%{?dist}
Summary:    libchamplain

Group:      Applications/Internet
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/libchamplain/0.12/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  clutter-gtk libsoup sqlite
BuildRequires:  gobject-introspection vala
BuildRequires:  gtk-doc mesa

%description
The libchamplain is a Clutter based widget used to display rich, eye-candy and interactive maps.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --enable-vala \
            --disable-static \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_includedir}/libchamplain-0.12/champlain/champlain*.h
%{_includedir}/libchamplain-gtk-0.12/champlain-gtk/*.h
%{_libdir}/girepository-1.0/Champlain-0.12.typelib
%{_libdir}/girepository-1.0/GtkChamplain-0.12.typelib
%{_libdir}/libchamplain-0.12.*
%{_libdir}/libchamplain-gtk-0.12.*
%{_libdir}/pkgconfig/champlain-0.12.pc
%{_libdir}/pkgconfig/champlain-gtk-0.12.pc
%{_datadir}/gir-1.0/Champlain-0.12.gir
%{_datadir}/gir-1.0/GtkChamplain-0.12.gir
%{_datadir}/gtk-doc/html/libchamplain-gtk-0.12/*
%{_datadir}/gtk-doc/html/libchamplain-0.12/*
%{_datadir}/vala/vapi/champlain*-0.12.vapi

%clean
rm -rf %{buildroot}

%changelog
* Sun Sep 20 2015 tanggeliang <tanggeliang@gmail.com>
- update from 0.12.10 to 0.12.11
* Sun Mar 29 2015 tanggeliang <tanggeliang@gmail.com>
- update from 0.12.7 to 0.12.10
* Thu Mar 20 2014 tanggeliang <tanggeliang@gmail.com>
- update from 0.12.5 to 0.12.7
* Thu Sep 26 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.12.4 to 0.12.5
* Wed Aug 7 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.12.3 to 0.12.4
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
