Name:       geocode-glib
Version:    3.18.0
Release:    1%{?dist}
Summary:    Geocode GLib

Group:      Applications/Internet
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/geocode-glib/3.18/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  json-glib libsoup libsoup-gnome
BuildRequires:  gobject-introspection
BuildRequires:  gtk-doc intltool gettext xml-parser

%description
The Geocode GLib is a convenience library for the Yahoo! Place Finder APIs.
The Place Finder web service allows to do geocoding (finding longitude and latitude from an address), and reverse geocoding (finding an address from coordinates).

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_includedir}/geocode-glib-1.0/geocode-glib/geocode-*.h
%{_libdir}/girepository-1.0/GeocodeGlib-1.0.typelib
%{_libdir}/libgeocode-glib.*
%{_libdir}/pkgconfig/geocode-glib-1.0.pc
%{_datadir}/icons/gnome/scalable/places/poi-*.svg
%{_datadir}/gir-1.0/GeocodeGlib-1.0.gir
%{_datadir}/gtk-doc/html/geocode-glib-1.0/*

%clean
rm -rf %{buildroot}

%changelog
* Tue Sep 22 2015 tanggeliang <tanggeliang@gmail.com>
- update from 3.17.92 to 3.18.0
* Wed Sep 16 2015 tanggeliang <tanggeliang@gmail.com>
- update from 3.16.0 to 3.17.92
* Sun Mar 29 2015 tanggeliang <tanggeliang@gmail.com>
- update from 3.14.0 to 3.16.0
* Wed Sep 24 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.12.2 to 3.14.0
* Thu Jul 24 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.10.0 to 3.12.2
* Tue Sep 24 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.99.1 to 3.10.0
* Wed Aug 7 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.99.0 to 0.99.1
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
