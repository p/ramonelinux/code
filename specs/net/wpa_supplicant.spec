Name:       wpa_supplicant
Version:    2.2
Release:    1%{?dist}
Summary:    WPA Supplicant

Group:      Applications/Internet
License:    GPLv2+
Url:        http://hostap.epitest.fi
Source:     http://hostap.epitest.fi/releases/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  libnl openssl
BuildRequires:  dbus libxml systemd

%description
WPA Supplicant is a Wi-Fi Protected Access (WPA) client and IEEE 802.1X supplicant.
It implements WPA key negotiation with a WPA Authenticator and Extensible Authentication Protocol (EAP) authentication with an Authentication Server.
In addition, it controls the roaming and IEEE 802.11 authentication/association of the wireless LAN driver.
This is useful for connecting to a password protected wireless access point. 

%prep
%setup -q -n %{name}-%{version}

%build
cat > wpa_supplicant/.config << "EOF"
CONFIG_BACKEND=file
CONFIG_CTRL_IFACE=y
CONFIG_DEBUG_FILE=y
CONFIG_DEBUG_SYSLOG=y
CONFIG_DEBUG_SYSLOG_FACILITY=LOG_DAEMON
CONFIG_DRIVER_NL80211=y
CONFIG_DRIVER_WEXT=y
CONFIG_DRIVER_WIRED=y
CONFIG_EAP_GTC=y
CONFIG_EAP_LEAP=y
CONFIG_EAP_MD5=y
CONFIG_EAP_MSCHAPV2=y
CONFIG_EAP_OTP=y
CONFIG_EAP_PEAP=y
CONFIG_EAP_TLS=y
CONFIG_EAP_TTLS=y
CONFIG_IEEE8021X_EAPOL=y
CONFIG_IPV6=y
CONFIG_LIBNL32=y
CONFIG_PEERKEY=y
CONFIG_PKCS12=y
CONFIG_READLINE=y
CONFIG_SMARTCARD=y
CONFIG_WPS=y
CFLAGS += -I/usr/include/libnl3
EOF
cat >> wpa_supplicant/.config << "EOF"
CONFIG_CTRL_IFACE_DBUS=y
CONFIG_CTRL_IFACE_DBUS_NEW=y
CONFIG_CTRL_IFACE_DBUS_INTRO=y
EOF

cd wpa_supplicant &&
make BINDIR=/sbin LIBDIR=/lib %{?_smp_mflags}

%check

%install
mkdir -p %{buildroot}/{sbin,usr/share/dbus-1/system-services,etc/dbus-1/system.d,/lib/systemd/system}
mkdir -p %{buildroot}/{usr/share/man/man{5,8}}

cd wpa_supplicant &&
install -v -m755 wpa_{cli,passphrase,supplicant} %{buildroot}/sbin &&
install -v -m644 doc/docbook/wpa_supplicant.conf.5 %{buildroot}/usr/share/man/man5 &&
install -v -m644 doc/docbook/wpa_{cli,passphrase,supplicant}.8 %{buildroot}/usr/share/man/man8

install -v -m644 dbus/fi.{epitest.hostap.WPASupplicant,w1.wpa_supplicant1}.service \
                 %{buildroot}/usr/share/dbus-1/system-services &&
install -v -m644 dbus/dbus-wpa_supplicant.conf %{buildroot}/etc/dbus-1/system.d/wpa_supplicant.conf

install -v -m644 systemd/wpa_supplicant*.service %{buildroot}/lib/systemd/system

%files
%defattr(-,root,root,-)
/etc/dbus-1/system.d/wpa_supplicant.conf
/sbin/wpa_cli
/sbin/wpa_passphrase
/sbin/wpa_supplicant
%{_datadir}/dbus-1/system-services/fi.epitest.hostap.WPASupplicant.service
%{_datadir}/dbus-1/system-services/fi.w1.wpa_supplicant1.service
/lib/systemd/system/wpa_supplicant-nl80211@.service
/lib/systemd/system/wpa_supplicant-wired@.service
/lib/systemd/system/wpa_supplicant.service
/lib/systemd/system/wpa_supplicant@.service

%clean
rm -rf %{buildroot}

%changelog
* Sat Jul 26 2014 tanggeliang <tanggeliang@gmail.com>
- update from 2.1 to 2.2
* Thu Mar 20 2014 tanggeliang <tanggeliang@gmail.com>
- update from 2.0 to 2.1
* Thu May 30 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.0 to 2.0
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
