Name:       serf
Version:    1.3.6
Release:    1%{?dist}
Summary:    Serf

Group:      Applications/Internet
License:    GPLv2+
Url:        https://serf.googlecode.com
Source:     https://serf.googlecode.com/files/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  apr-util openssl krb scons

%description
The Serf package contains a C-based HTTP client library built upon the Apache Portable Runtime (APR) library.
It multiplexes connections, running the read/write communication asynchronously.
Memory copies and transformations are kept to a minimum to provide high performance operation.

%prep
%setup -q -n %{name}-%{version}

%build
sed -i "/Append/s:RPATH=libdir,::"  SConstruct &&
sed -i "/Default/s:lib_static,::"   SConstruct &&
sed -i "/Alias/s:install_static,::" SConstruct &&
scons PREFIX=/usr LIBDIR=%{_libdir}

%check

%install
rm -rf %{buildroot}
scons PREFIX=/usr LIBDIR=%{_libdir} install --install-sandbox=%{buildroot}

%files
%defattr(-,root,root,-)
%{_includedir}/serf-1/serf*.h
%{_libdir}/libserf-1.so*
%{_libdir}/pkgconfig/serf-1.pc

%clean
rm -rf %{buildroot}

%changelog
* Sun Jul 20 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.3.4 to 1.3.6
* Tue Mar 18 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.3.2 to 1.3.4
* Tue Oct 22 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.3.1 to 1.3.2
* Thu Aug 22 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.2.1 to 1.3.1
* Sat Jul 27 2013 tanggeliang <tanggeliang@gmail.com>
- create
