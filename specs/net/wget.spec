Name:       wget
Version:    1.16.3
Release:    1%{?dist}
Summary:    Wget

Group:      Applications/Internet
License:    GPLv2+
Url:        http://www.gnu.org
Source:     http://ftp.gnu.org/gnu/wget/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  gnutls openssl

%description
The Wget package contains a utility useful for non-interactive downloading of files from the Web.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --sysconfdir=/etc \
            --with-ssl=openssl \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

rm -f %{buildroot}/%{_infodir}/dir

%files
%defattr(-,root,root,-)
/etc/wgetrc
%{_bindir}/wget
%{_datadir}/locale/*/LC_MESSAGES/wget.mo

%files doc
%defattr(-,root,root,-)
%{_infodir}/wget.info.gz
%{_mandir}/man1/wget.1.gz

%clean
rm -rf %{buildroot}

%changelog
* Sat Mar 28 2015 tanggeliang <tanggeliang@gmail.com>
- update from 1.15 to 1.16.3
* Thu Mar 20 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.14 to 1.15
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
