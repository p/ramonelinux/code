Name:       traceroute
Version:    2.0.18
Release:    5%{?dist}
Summary:    Traceroute

Group:      Applications/Internet
License:    GPLv2+
Url:        http://traceroute.sourceforge.net
Source:     http://downloads.sourceforge.net/traceroute/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build

%description
The Traceroute package contains a program which is used to display the network route that packets take to reach a specified host.
This is a standard network troubleshooting tool.
If you find yourself unable to connect to another system, traceroute can help pinpoint the problem.

%prep
%setup -q -n %{name}-%{version}

%build
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make prefix=%{buildroot}/usr install

%files
%defattr(-,root,root,-)
%{_bindir}/traceroute
%{_mandir}/man8/traceroute.8.gz

%clean
rm -rf %{buildroot}

%changelog
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
