Name:       samba
Version:    4.2.3
Release:    1%{?dist}
Summary:    Samba

Group:      Applications/Internet
License:    GPLv2+
Url:        http://www.samba.org
Source:     https://download.samba.org/pub/samba/stable/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  libxslt openldap docbook-xsl docbook-xml gnutls

%description
The Samba package provides file and print services to SMB/CIFS clients and Windows networking to Linux clients.
Samba can also be configured as a Windows Domain Controller replacement, a file/print server acting as a member of a Windows Active Directory domain and a NetBIOS (rfc1001/1002) nameserver (which among other things provides LAN browsing support).

%prep
%setup -q -n %{name}-%{version}

%build
./configure                             \
    --prefix=/usr                       \
    --sysconfdir=/etc                   \
    --localstatedir=/var                \
    --with-piddir=/run/samba            \
    --with-pammodulesdir=/lib/security  \
    --without-systemd                   \
    --enable-fhs                        \
    --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
/lib/security/pam_smbpass.so
/lib/security/pam_winbind.so
%{_bindir}/cifsdd
%{_bindir}/dbwrap_tool
%{_bindir}/eventlogadm
%{_bindir}/gentest
%{_bindir}/ldbadd
%{_bindir}/ldbdel
%{_bindir}/ldbedit
%{_bindir}/ldbmodify
%{_bindir}/ldbrename
%{_bindir}/ldbsearch
%{_bindir}/locktest
%{_bindir}/masktest
%{_bindir}/ndrdump
%{_bindir}/net
%{_bindir}/nmblookup
%{_bindir}/ntdbbackup
%{_bindir}/ntdbdump
%{_bindir}/ntdbrestore
%{_bindir}/ntdbtool
%{_bindir}/ntlm_auth
%{_bindir}/oLschema2ldif
%{_bindir}/pdbedit
%{_bindir}/pidl
%{_bindir}/profiles
%{_bindir}/regdiff
%{_bindir}/regpatch
%{_bindir}/regshell
%{_bindir}/regtree
%{_bindir}/rpcclient
%{_bindir}/samba-regedit
%{_bindir}/samba-tool
%{_bindir}/sharesec
%{_bindir}/smbcacls
%{_bindir}/smbclient
%{_bindir}/smbcontrol
%{_bindir}/smbcquotas
%{_bindir}/smbget
%{_bindir}/smbpasswd
%{_bindir}/smbspool
%{_bindir}/smbstatus
%{_bindir}/smbta-util
%{_bindir}/smbtar
%{_bindir}/smbtorture
%{_bindir}/smbtree
%{_bindir}/tdbbackup
%{_bindir}/tdbdump
%{_bindir}/tdbrestore
%{_bindir}/tdbtool
%{_bindir}/testparm
%{_bindir}/wbinfo
%{_includedir}/samba-4.0/*
%{_libdir}/libdcerpc*.so*
%{_libdir}/libgensec.so*
%{_libdir}/libndr*.so*
%{_libdir}/libnetapi.so*
%{_libdir}/libnss_*.so*
%{_libdir}/libregistry.so*
%{_libdir}/libsamba-*.so*
%{_libdir}/libsamdb.so*
%{_libdir}/libsmbclient-raw.so*
%{_libdir}/libsmbclient.so*
%{_libdir}/libsmbconf.so*
%{_libdir}/libsmbldap.so*
%{_libdir}/libtevent-util.so*
%{_libdir}/libtorture.so*
%{_libdir}/libwbclient.so*
%{_libdir}/mit_samba.so
%{_libdir}/perl5/vendor_perl/*/*
%{_libdir}/pkgconfig/*.pc
%{_libdir}/python2.7/site-packages/*
%{_libdir}/samba/auth/script.so
%{_libdir}/samba/bind9/*.so
%{_libdir}/samba/gensec/krb5.so
%{_libdir}/samba/idmap/*.so
%{_libdir}/samba/ldb/*.so
%{_libdir}/samba/*.so*
%{_libdir}/samba/nss_info/*.so
%{_libdir}/samba/process_model/*.so
%{_libdir}/samba/service/*.so
%{_libdir}/samba/vfs/*.so
%{_libdir}/winbind_krb5_locator.so
%{_sbindir}/nmbd
%{_sbindir}/samba
%{_sbindir}/samba_*
%{_sbindir}/smbd
%{_sbindir}/winbindd
%{_datadir}/samba/codepages/*.dat
%{_datadir}/samba/setup/*
%{_mandir}/man*/*.gz

%clean
rm -rf %{buildroot}

%changelog
* Wed Sep 16 2015 tanggeliang <tanggeliang@gmail.com>
- create
