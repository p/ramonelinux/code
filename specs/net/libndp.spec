Name:       libndp
Version:    1.5
Release:    1%{?dist}
Summary:    libndp

Group:      Applications/Internet
License:    GPLv2+
Url:        http://libndp.org
Source:     http://libndp.org/files/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build

%description
The libndp package provides a wrapper for IPv6 Neighbor Discovery Protocol. It also provides a tool named ndptool for sending and receiving NDP messages.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr        \
            --sysconfdir=/etc    \
            --localstatedir=/var \
            --disable-static     \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/ndptool
%{_includedir}/ndp.h
%{_libdir}/libndp.la
%{_libdir}/libndp.so*
%{_libdir}/pkgconfig/libndp.pc
%{_mandir}/man8/ndptool.8.gz

%clean
rm -rf %{buildroot}

%changelog
* Wed Sep 23 2015 tanggeliang <tanggeliang@gmail.com>
- update from 1.3 to 1.5
* Tue Jul 22 2014 tanggeliang <tanggeliang@gmail.com>
- create
