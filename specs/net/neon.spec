Name:       neon
Version:    0.30.0
Release:    1%{?dist}
Summary:    neon

Group:      Applications/Internet
License:    GPLv2+
Url:        http://www.webdav.org/neon
Source:     http://www.webdav.org/neon/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  libxml expat
BuildRequires:  openssl gnutls

%description
The neon package is an HTTP and WebDAV client library, with a C interface.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --enable-shared --with-ssl --disable-static \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/neon-config
%{_includedir}/neon/ne_*.h
%{_libdir}/libneon.*
%{_libdir}/pkgconfig/neon.pc
%{_datadir}/locale/*/LC_MESSAGES/neon.mo

%files doc
%defattr(-,root,root,-)
%{_docdir}/neon-%{version}/html/*.html
%{_mandir}/man1/*.1.gz
%{_mandir}/man3/*.3.gz

%clean
rm -rf %{buildroot}

%changelog
* Sun Aug 18 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.29.6 to 0.30.0
* Tue Aug 14 2012 tanggeliang <tanggeliang@gmail.com>
- add "--with-ssl=openssl" for subversion surport "https".
- "neon-config --libs" changed from "-lneon -lz -lexpat" to "-lneon -lz -lssl -lcrypto -lexpat".

* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
