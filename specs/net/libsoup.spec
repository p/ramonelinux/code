Name:       libsoup
Version:    2.52.0
Release:    1%{?dist}
Summary:    libsoup

Group:      Applications/Internet
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/libsoup/2.52/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  libxml sqlite gobject-introspection
BuildRequires:  intltool gettext xml-parser vala

%description
The libsoup is HTTP client/server library for GNOME.
It uses GObject and the GLib main loop to integrate with GNOME applications and it also has asynchronous API for use in threaded applications.

%package        gnome
Summary:        GNOME
BuildRequires:  glib-networking
BuildRequires:  gobject-introspection libgnome-keyring
Requires:       glib-networking
%description    gnome

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --disable-static \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_includedir}/libsoup-2.4/libsoup/soup*.h
%{_libdir}/libsoup-2.4.la
%{_libdir}/libsoup-2.4.so*
%{_libdir}/girepository-1.0/Soup-2.4.typelib
%{_libdir}/pkgconfig/libsoup-2.4.pc
%{_datadir}/gir-1.0/Soup-2.4.gir
%{_datadir}/locale/*/LC_MESSAGES/libsoup.mo

%files gnome
%defattr(-,root,root,-)
%{_includedir}/libsoup-gnome-2.4/libsoup/soup-*.h
%{_libdir}/libsoup-gnome-2.4.la
%{_libdir}/libsoup-gnome-2.4.so*
%{_libdir}/girepository-1.0/SoupGNOME-2.4.typelib
%{_libdir}/pkgconfig/libsoup-gnome-2.4.pc
%{_datadir}/gir-1.0/SoupGNOME-2.4.gir
%{_datadir}/vala/vapi/libsoup-2.4.vapi

%files doc
%defattr(-,root,root,-)
%{_datadir}/gtk-doc/html/libsoup-2.4/*

%clean
rm -rf %{buildroot}

%changelog
* Tue Sep 22 2015 tanggeliang <tanggeliang@gmail.com>
- update from 2.51.92 to 2.52.0
* Tue Sep 15 2015 tanggeliang <tanggeliang@gmail.com>
- update from 2.48.0 to 2.51.92
* Wed Sep 24 2014 tanggeliang <tanggeliang@gmail.com>
- update from 2.47.4 to 2.48.0
* Tue Jul 22 2014 tanggeliang <tanggeliang@gmail.com>
- update from 2.46.0 to 2.47.4
* Sun Jul 20 2014 tanggeliang <tanggeliang@gmail.com>
- update from 2.44.2 to 2.46.0
* Tue Dec 3 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.44.1 to 2.44.2
* Wed Oct 23 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.44.0 to 2.44.1
* Thu Sep 26 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.43.1 to 2.44.0
* Sat May 11 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.42.0 to 2.43.1
* Thu Apr 11 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.40.3 to 2.42.0
* Thu Mar 14 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.40.1 to 2.40.3
* Sun Oct 28 2012 tanggeliang <tanggeliang@gmail.com>
- update from 2.40.0 to 2.40.1
* Fri Sep 28 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.38.1 to 2.40.0
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
