Name:       geoclue0
Version:    0.12.0
Release:    1%{?dist}
Summary:    GeoClue 

Group:      Applications/Internet
License:    GPLv2+
Url:        https://launchpad.net/geoclue
Source:     https://launchpad.net/geoclue/trunk/0.12/+download/geoclue-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  dbus-glib, libxslt, gconf, libsoup, networkmanager, libxml
BuildRequires:  systemd-udev

%description
GeoClue is a modular geoinformation service built on top of the D-Bus messaging system.
The goal of the GeoClue project is to make creating location-aware applications as simple as possible.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n geoclue-%{version}

%build
sed -i "s@ -Werror@@" configure &&
sed -i "s@libnm_glib@libnm-glib@g" configure &&
sed -i "s@geoclue/libgeoclue.la@& -lgthread-2.0@g" \
       providers/skyhook/Makefile.in &&
./configure --prefix=/usr --libexecdir=%{_libdir}/geoclue \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_includedir}/geoclue/*.h
%{_libdir}/geoclue/geoclue-*
%{_libdir}/libgeoclue.*
%{_libdir}/pkgconfig/geoclue.pc
%{_datadir}/dbus-1/services/org.freedesktop.Geoclue.*.service
%{_datadir}/geoclue-providers/geoclue-*.provider

%files doc
%defattr(-,root,root,-)
%{_datadir}/gtk-doc/html/geoclue/*

%clean
rm -rf %{buildroot}

%changelog
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
