Name:       libnice
Version:    0.1.13
Release:    1%{?dist}
Summary:    libnice

Group:      Applications/Internet
License:    GPLv2+
Url:        http://www.freedesktop.org
Source:     http://nice.freedesktop.org/releases/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  glib
BuildRequires:  gst-plugins-base
BuildRequires:  gtk-doc

%description
The libnice package is an implementation of the IETF's draft Interactice Connectivity Establishment standard (ICE).
It provides GLib-based library, libnice and GStreamer elements.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --disable-static \
            --without-gstreamer-0.10 \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/sdp-example
%{_bindir}/simple-example
%{_bindir}/stunbdc
%{_bindir}/stund
%{_bindir}/threaded-example
%{_includedir}/nice/*.h
%{_includedir}/stun/*
%{_libdir}/gstreamer-1.0/libgstnice.*
%{_libdir}/libnice.*
%{_libdir}/pkgconfig/nice.pc
%{_datadir}/gtk-doc/html/libnice/*

%clean
rm -rf %{buildroot}

%changelog
* Wed Sep 23 2015 tanggeliang <tanggeliang@gmail.com>
- update from 0.1.4 to 0.1.13
* Sat Aug 24 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.1.3 to 0.1.4
* Sun Sep 30 2012 tanggeliang <tanggeliang@gmail.com>
- update from 0.1.2 to 0.1.3
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
