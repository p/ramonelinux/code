Name:       whois
Version:    5.0.18
Release:    6%{?dist}
Summary:    Whois

Group:      Applications/Internet
License:    GPLv2+
Url:        http://www.linux.it/~md/software
Source:     http://ftp.debian.org/debian/pool/main/w/whois/%{name}_%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  gettext

%description
Whois is a client-side application which queries the whois directory service for information pertaining to a particular domain name.

%prep
%setup -q -n %{name}-%{version}

%build
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make prefix=%{buildroot}/usr install-whois
make prefix=%{buildroot}/usr install-pos

%files
%defattr(-,root,root,-)
%{_bindir}/whois
%{_datadir}/locale/*/LC_MESSAGES/whois.mo
%{_mandir}/man1/whois.1.gz

%clean
rm -rf %{buildroot}

%changelog
* Sun Sep 16 2012 tanggeliang <tanggeliang@gmail.com>
- remove "mkpasswd" command which is installed by the "expect" package.
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
