Name:       curl
Version:    7.44.0
Release:    1%{?dist}
Summary:    cURL

Group:      Applications/Internet
License:    GPLv2+
Url:        http://curl.haxx.se
Source:     http://curl.haxx.se/download/%{name}-%{version}.tar.lzma

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  ca-certificates openssl gnutls

%description
The cURL package contains curl and its support library libcurl.
This is useful for transferring files with URL syntax to any of the following protocols: FTP, FTPS, HTTP, HTTPS, SCP, SFTP, TFTP, TELNET, DICT, LDAP, LDAPS and FILE.
This ability to both download and upload files can be incorporated into other programs to support functions like streaming media.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr              \
            --disable-static           \
            --enable-threaded-resolver \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

find docs \( -name "Makefile*" -o -name "*.1" -o -name "*.3" \) -exec rm {} \; &&
install -v -d -m755 %{buildroot}/usr/share/doc/curl-%{version} &&
cp -v -R docs/*     %{buildroot}/usr/share/doc/curl-%{version}

%files
%defattr(-,root,root,-)
%{_bindir}/curl
%{_bindir}/curl-config
%{_includedir}/curl/*.h
%{_libdir}/libcurl.la
%{_libdir}/libcurl.so*
%{_libdir}/pkgconfig/libcurl.pc
%{_datadir}/aclocal/libcurl.m4

%files doc
%defattr(-,root,root,-)
%{_docdir}/curl-%{version}/*
%{_mandir}/man*/*.gz

%clean
rm -rf %{buildroot}

%changelog
* Mon Sep 14 2015 tanggeliang <tanggeliang@gmail.com>
- update from 7.41.0 to 7.44.0
* Sat Mar 28 2015 tanggeliang <tanggeliang@gmail.com>
- update from 7.37.1 to 7.41.0
* Sun Jul 20 2014 tanggeliang <tanggeliang@gmail.com>
- update from 7.36.0 to 7.37.1
* Sun Apr 20 2014 tanggeliang <tanggeliang@gmail.com>
- update from 7.35.0 to 7.36.0
* Tue Mar 18 2014 tanggeliang <tanggeliang@gmail.com>
- update from 7.34.0 to 7.35.0
* Sun Jan 5 2014 tanggeliang <tanggeliang@gmail.com>
- update from 7.33.0 to 7.34.0
* Sun Oct 20 2013 tanggeliang <tanggeliang@gmail.com>
- update from 7.32.0 to 7.33.0
* Wed Aug 14 2013 tanggeliang <tanggeliang@gmail.com>
- update from 7.31.0 to 7.32.0
* Fri Jul 5 2013 tanggeliang <tanggeliang@gmail.com>
- update from 7.30.0 to 7.31.0
* Wed May 29 2013 tanggeliang <tanggeliang@gmail.com>
- update from 7.28.0 to 7.30.0
* Mon Oct 22 2012 tanggeliang <tanggeliang@gmail.com>
- update from 7.27.0 to 7.28.0
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
