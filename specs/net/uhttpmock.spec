Name:       uhttpmock
Version:    0.5.0
Release:    1%{?dist}
Summary:    HTTP web service mocking library

Group:      Development/Tools
License:    LGPL-2.1+
URL:        http://gitorious.org/uhttpmock
Source0:    http://tecnocode.co.uk/downloads/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  gobject-introspection vala
BuildRequires:  glib libsoup

%description
uhttpmock is a project for mocking web service APIs which use HTTP or HTTPS.
It provides a library, libuhttpmock, which implements recording and playback of HTTP request/response traces.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root)
%{_includedir}/libuhttpmock-0.0/
%{_libdir}/libuhttpmock-0.0.*
%{_libdir}/girepository-1.0/Uhm-0.0.typelib
%{_libdir}/pkgconfig/libuhttpmock-0.0.pc
%{_datadir}/gir-1.0/Uhm-0.0.gir
%{_datadir}/gtk-doc/html/libuhttpmock-0.0/*
%{_datadir}/vala/vapi/libuhttpmock-0.0.*

%changelog
* Sat Sep 26 2015 tanggeliang <tanggeliang@gmail.com>
- update from 0.2.0 to 0.5.0
* Mon Jan 6 2014 tanggeliang <tanggeliang@gmail.com>
- create
