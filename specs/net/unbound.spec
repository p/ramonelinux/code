Name:       unbound
Version:    1.4.21
Release:    1%{?dist}
Summary:    Unbound

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://unbound.net
Source:     http://unbound.net/downloads/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  openssl ldns

%description
Unbound is a validating, recursive, and caching DNS resolver.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr     \
            --sysconfdir=/etc \
            --disable-static  \
            --with-pidfile=/run/unbound.pid \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

mkdir -p %{buildroot}/usr/bin/
mv -v %{buildroot}/usr/sbin/unbound-host %{buildroot}/usr/bin/

%files
%defattr(-,root,root,-)
/etc/unbound/unbound.conf
%{_bindir}/unbound-host
%{_includedir}/unbound.h
%{_libdir}/libunbound.*a
%{_libdir}/libunbound.so*
%{_sbindir}/unbound
%{_sbindir}/unbound-anchor
%{_sbindir}/unbound-checkconf
%{_sbindir}/unbound-control
%{_sbindir}/unbound-control-setup

%files doc
%defattr(-,root,root,-)
%{_mandir}/man*/*.gz

%post
groupadd -g 88 unbound &&
useradd -c "Unbound DNS resolver" -d /var/lib/unbound -u 88 \
        -g unbound -s /bin/false unbound

%clean
rm -rf %{buildroot}

%changelog
* Thu Oct 3 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.4.20 to 1.4.21
* Sat Jul 6 2013 tanggeliang <tanggeliang@gmail.com>
- create
