Name:       wireless-tools
Version:    29
Release:    7%{?dist}
Summary:    Wireless Tools

Group:      Applications/Internet
License:    GPLv2+
Url:        http://www.hpl.hp.com
Source:     http://www.hpl.hp.com/personal/Jean_Tourrilhes/Linux/wireless_tools.%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
Requires:       linux-firmware wpa_supplicant

%description
The Wireless Extension (WE) is a generic API in the Linux kernel allowing a driver to expose configuration and statistics specific to common Wireless LANs to user space.
A single set of tools can support all the variations of Wireless LANs, regardless of their type as long as the driver supports Wireless Extensions.
WE parameters may also be changed on the fly without restarting the driver (or Linux). 

%prep
%setup -q -n wireless_tools.%{version}

%build
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make PREFIX=%{buildroot}/usr INSTALL_MAN=%{buildroot}/usr/share/man install \
     INSTALL_LIB=%{buildroot}/%{_libdir}

%files
%defattr(-,root,root,-)
%{_includedir}/iwlib.h
%{_includedir}/wireless.h
%{_libdir}/libiw.so
%{_libdir}/libiw.so.29
%{_sbindir}/ifrename
%{_sbindir}/iwconfig
%{_sbindir}/iwevent
%{_sbindir}/iwgetid
%{_sbindir}/iwlist
%{_sbindir}/iwpriv
%{_sbindir}/iwspy
%{_mandir}/*

%clean
rm -rf %{buildroot}

%changelog
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
