Name:       networkmanager
Version:    1.0.6
Release:    1%{?dist}
Summary:    NetworkManager

Group:      System Environment/Base
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/NetworkManager/1.0/NetworkManager-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  dbus-glib intltool libnl nss systemd systemd-gudev wireless-tools
BuildRequires:  consolekit dhcpcd gobject-introspection iptables libsoup polkit upower vala
BuildRequires:  gettext xml-parser lsb_release libndp libgcrypt polkit
Requires:       dhcpcd wireless-tools systemd

%description
NetworkManager is a set of co-operative tools that make networking simple and straightforward. Whether WiFi, wired, 3G, or Bluetooth, NetworkManager allows you to quickly move from one network to another: once a network has been configured and joined once, it can be detected and re-joined automatically the next time its available. 

%package        glib
Summary:        libnm-glib
%description    glib

%package        util
Summary:        libnm-util
%description    util

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n NetworkManager-%{version}

%build
./configure --prefix=/usr \
            --sysconfdir=/etc \
            --localstatedir=/var \
            --libexecdir=%{_libdir}/NetworkManager \
            --with-systemdsystemunitdir=/lib/systemd/system \
            --with-session-tracking=systemd \
            --disable-ppp \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

rm -rf %{buildroot}/etc/rc.d/init.d/NetworkManager
mkdir -p %{buildroot}/etc/NetworkManager
cat >> %{buildroot}/etc/NetworkManager/NetworkManager.conf << "EOF"
[main]
plugins=keyfile
EOF

%files
%defattr(-,root,root,-)
/etc/dbus-1/system.d/nm-*.conf
/etc/dbus-1/system.d/org.freedesktop.NetworkManager.conf
/etc/NetworkManager/NetworkManager.conf
/lib/udev/rules.d/85-nm-unmanaged.rules
%{_bindir}/nm-online
%{_bindir}/nmcli
%{_includedir}/NetworkManager/*.h
%{_includedir}/libnm/*.h
%{_libdir}/NetworkManager/libnm-device-plugin-adsl.*
%{_libdir}/NetworkManager/libnm-settings-plugin-ibft.*
%{_libdir}/NetworkManager/libnm-device-plugin-wifi.*
%{_libdir}/NetworkManager/nm-*
%{_libdir}/libnm.la
%{_libdir}/libnm.so*
%{_libdir}/girepository-1.0/NMClient-1.0.typelib
%{_libdir}/girepository-1.0/NetworkManager-1.0.typelib
%{_libdir}/girepository-1.0/NM-1.0.typelib
%{_libdir}/pkgconfig/NetworkManager.pc
%{_libdir}/pkgconfig/libnm.pc
%{_sbindir}/NetworkManager
%{_datadir}/bash-completion/completions/nmcli
%{_datadir}/dbus-1/system-services/org.freedesktop.NetworkManager.service
%{_datadir}/dbus-1/system-services/org.freedesktop.nm_dispatcher.service
%{_datadir}/gir-1.0/NMClient-1.0.gir
%{_datadir}/gir-1.0/NetworkManager-1.0.gir
%{_datadir}/gir-1.0/NM-1.0.gir
%{_datadir}/locale/*/LC_MESSAGES/NetworkManager.mo
%{_datadir}/polkit-1/actions/org.freedesktop.NetworkManager.policy
/lib/systemd/system/NetworkManager-dispatcher.service
/lib/systemd/system/NetworkManager-wait-online.service
/lib/systemd/system/NetworkManager.service
/lib/systemd/system/network-online.target.wants/NetworkManager-wait-online.service

%files glib
%defattr(-,root,root,-)
%{_includedir}/libnm-glib/*.h
%{_libdir}/libnm-glib-vpn.*
%{_libdir}/libnm-glib.*
%{_libdir}/pkgconfig/libnm-glib-vpn.pc
%{_libdir}/pkgconfig/libnm-glib.pc
%{_datadir}/vala/vapi/libnm-glib.*

%files util
%defattr(-,root,root,-)
%{_libdir}/libnm-util.*
%{_libdir}/pkgconfig/libnm-util.pc
%{_datadir}/vala/vapi/libnm-util.*

%files doc
%defattr(-,root,root,-)
%{_datadir}/gtk-doc/html/*
%{_docdir}/NetworkManager/examples/server.conf
%{_mandir}/man*/*.gz

%post
systemctl enable NetworkManager
mkdir -p /etc/NetworkManager/system-connections

%postun
systemctl disable NetworkManager
rm -rf /etc/NetworkManager/system-connections

%clean
rm -rf %{buildroot}

%changelog
* Wed Sep 23 2015 tanggeliang <tanggeliang@gmail.com>
- update from 0.9.10.0 to 1.0.6
* Tue Jul 22 2014 tanggeliang <tanggeliang@gmail.com>
- update from 0.9.8.8 to 0.9.10.0
* Tue Dec 3 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.9.8.6 to 0.9.8.8
* Mon Oct 7 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.9.8.4 to 0.9.8.6
* Wed Sep 25 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.9.8.2 to 0.9.8.4
* Fri Aug 16 2013 tanggeliang <tanggeliang@gmail.com>
- fix dhcpcd6 support.
* Thu Aug 15 2013 tanggeliang <tanggeliang@gmail.com>
- add systemd support.
* Sat Jun 8 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.9.8.0 to 0.9.8.2
- remove "sed -i '22661s:-Werror::' configure"
* Tue Jun 4 2013 tanggeliang <tanggeliang@gmail.com>
- remove '-Werror' to fix "nm-dns-manager.c:564:28: error: argument to 'sizeof' in 'memset' call is the same expression as the destination; did you mean to provide an explicit length? [-Werror=sizeof-pointer-memaccess] memset (buffer, 0, sizeof (buffer));"

* Fri Mar 15 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.9.6.4 to 0.9.8.0
* Sat Nov 3 2012 tanggeliang <tanggeliang@gmail.com>
- update from 0.9.6.0 to 0.9.6.4
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
