Name:       libevent
Version:    2.0.21
Release:    1%{?dist}
Summary:    Libevent

Group:      Applications/Internet
License:    GPLv2+
Url:        https://libevent.com
Source:     https://github.com/downloads/libevent/libevent/%{name}-%{version}-stable.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  openssl

%description
Libevent is an asynchronous event notification software library.
The Libevent API provides a mechanism to execute a callback function when a specific event occurs on a file descriptor or after a timeout has been reached.
Furthermore, Libevent also supports callbacks due to signals or regular timeouts.

%prep
%setup -q -n %{name}-%{version}-stable

%build
./configure --prefix=/usr --disable-static \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

rm -f %{buildroot}/%{_infodir}/dir

%files
%defattr(-,root,root,-)
%{_bindir}/event_rpcgen.py
%{_includedir}/ev*.h
%{_includedir}/event2/*.h
%{_libdir}/libevent.*
%{_libdir}/libevent-2.0.so.5*
%{_libdir}/libevent_core*.*
%{_libdir}/libevent_extra*.*
%{_libdir}/libevent_openssl*.*
%{_libdir}/libevent_pthreads*.*
%{_libdir}/pkgconfig/libevent.pc
%{_libdir}/pkgconfig/libevent_openssl.pc
%{_libdir}/pkgconfig/libevent_pthreads.pc

%clean
rm -rf %{buildroot}

%changelog
* Fri Apr 5 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.0.20 to 2.0.21
* Tue Oct 23 2012 tanggeliang <tanggeliang@gmail.com>
- create
