Name:       glib-networking
Version:    2.46.0
Release:    1%{?dist}
Summary:    GLib Networking

Group:      Applications/Internet
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/glib-networking/2.46/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  gnutls gsettings-desktop-schemas
BuildRequires:  ca-certificates p11-kit
BuildRequires:  libgcrypt libproxy
BuildRequires:  intltool gettext xml-parser glib nettle
BuildRequires:  autoconf automake m4

%description
The GLib Networking package contains Network related gio modules for GLib.

%prep
%setup -q -n %{name}-%{version}

%build
autoreconf -fi &&
./configure --prefix=/usr \
            --libexecdir=%{_libdir}/glib-networking \
            --with-ca-certificates=/etc/ssl/ca-bundle.crt \
            --disable-static \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_libdir}/gio/modules/libgiognomeproxy.la
%{_libdir}/gio/modules/libgiognomeproxy.so
%{_libdir}/gio/modules/libgiolibproxy.la
%{_libdir}/gio/modules/libgiolibproxy.so
%{_libdir}/gio/modules/libgiognutls.la
%{_libdir}/gio/modules/libgiognutls.so
%{_datadir}/dbus-1/services/org.gtk.GLib.PACRunner.service
%{_datadir}/locale/*
%{_libdir}/glib-networking/glib-pacrunner

%clean
rm -rf %{buildroot}

%changelog
* Tue Sep 22 2015 tanggeliang <tanggeliang@gmail.com>
- update from 2.45.1 to 2.46.0
* Tue Sep 15 2015 tanggeliang <tanggeliang@gmail.com>
- update from 2.42.0 to 2.45.1
* Wed Sep 24 2014 tanggeliang <tanggeliang@gmail.com>
- update from 2.41.4 to 2.42.0
* Tue Jul 22 2014 tanggeliang <tanggeliang@gmail.com>
- update from 2.38.2 to 2.41.4
* Tue Dec 3 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.38.1 to 2.38.2
* Tue Oct 22 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.38.0 to 2.38.1
* Thu Sep 26 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.36.2 to 2.38.0
* Sun Jul 7 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.34.2 to 2.36.2
* Sun Mar 31 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.34.0 to 2.34.2
* Fri Sep 28 2012 tanggeliang <tanggeliang@gmail.com>
- update from 2.32.3 to 2.34.0
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
