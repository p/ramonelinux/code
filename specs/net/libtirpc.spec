Name:       libtirpc
Version:    0.2.4
Release:    1%{?dist}
Summary:    libtirpc

Group:      Applications/Internet
License:    GPLv2+
Url:        http://libtirpc.sourceforge.net
Source:     http://downloads.sourceforge.net/project/libtirpc/%{version}/%{name}-%{version}.tar.bz2
Patch:      libtirpc-%{version}-remove_nis-1.patch

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  pkg-config autoconf automake m4 libtool

%description
The libtirpc package contains libraries that support programs that use the Remote Procedure Call (RPC) API.
It replaces the RPC, but not the NIS library entries that used to be in glibc.

%prep
%setup -q -n %{name}-%{version}
%patch -p1

%build
autoreconf -fi &&

./configure --prefix=/usr     \
            --sysconfdir=/etc \
            --disable-static  \
            --disable-gssapi  \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install &&
mkdir -pv %{buildroot}/%{_lib}
mv -v %{buildroot}/%{_libdir}/libtirpc.so.* %{buildroot}/%{_lib} &&
ln -sfv ../../%{_lib}/libtirpc.so.1.0.10 %{buildroot}/%{_libdir}/libtirpc.so

%files
%defattr(-,root,root,-)
/etc/netconfig
/%{_lib}/libtirpc.so.1*
%{_includedir}/tirpc/netconfig.h
%{_includedir}/tirpc/rpc/*.h
%{_includedir}/tirpc/rpc/rpcb_prot.x
%{_includedir}/tirpc/rpcsvc/crypt.*
%{_libdir}/libtirpc.*a
%{_libdir}/libtirpc.so
%{_libdir}/pkgconfig/libtirpc.pc
%{_mandir}/man*/*.gz

%clean
rm -rf %{buildroot}

%changelog
* Sun Jan 5 2014 tanggeliang <tanggeliang@gmail.com>
- update from 0.2.3 to 0.2.4
* Mon Apr 1 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.2.2 to 0.2.3
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
