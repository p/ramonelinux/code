Name:       ppp
Version:    2.4.7
Release:    1%{?dist}
Summary:    PPP

Group:      Applications/Internet
License:    GPLv2+
Url:        http://www.ppp.org
Source:     http://samba.org/ftp/ppp/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build

%description
The PPP package contains the pppd daemon and the chat program.
This is used for connecting to other machines; often for connecting to the Internet via a dial-up or PPPoE connection to an ISP.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot}/usr install

pushd %{buildroot}
mkdir -pv etc/ppp
install -d etc/ppp/peers
popd

%files
%defattr(-,root,root,-)
/etc/ppp/peers
%{_includedir}/pppd/*.h
/usr/lib/pppd/%{version}/minconn.so
/usr/lib/pppd/%{version}/openl2tp.so
/usr/lib/pppd/%{version}/passprompt.so
/usr/lib/pppd/%{version}/passwordfd.so
/usr/lib/pppd/%{version}/pppoatm.so
/usr/lib/pppd/%{version}/pppol2tp.so
/usr/lib/pppd/%{version}/radattr.so
/usr/lib/pppd/%{version}/radius.so
/usr/lib/pppd/%{version}/radrealms.so
/usr/lib/pppd/%{version}/rp-pppoe.so
/usr/lib/pppd/%{version}/winbind.so
%{_sbindir}/chat
%{_sbindir}/pppd
%{_sbindir}/pppdump
%{_sbindir}/pppoe-discovery
%{_sbindir}/pppstats
%{_mandir}/man8/*

%post
groupadd -g 52 pppusers

%postun

%clean
rm -rf %{buildroot}

%changelog
* Wed Sep 16 2015 tanggeliang <tanggeliang@gmail.com>
- update from 2.4.5 to 2.4.7
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
