Name:       rsync
Version:    3.1.1
Release:    1%{?dist}
Summary:    rsync

Group:      Applications/Internet
License:    GPLv2+
Url:        http://www.samba.org
Source:     http://samba.org/ftp/rsync/src/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  popt attr acl
Requires:       openssh

%description
The rsync package contains the rsync utility.
This is useful for synchronizing large file archives over a network.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --without-included-zlib &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

mkdir -p %{buildroot}/etc
cat > %{buildroot}/etc/rsyncd.conf << "EOF"
# This is a basic rsync configuration file
# It exports a single module without user authentication.

motd file = /home/rsync/welcome.msg
use chroot = yes

[localhost]
    path = /home/rsync
    comment = Default rsync module
    read only = yes
    list = yes
    uid = rsyncd
    gid = rsyncd

EOF

mkdir -p %{buildroot}/lib/systemd/system
cat > %{buildroot}/lib/systemd/system/rsync.service << "EOF"
[Unit]
Description=fast remote file copy program daemon
ConditionPathExists=/etc/rsyncd.conf

[Service]
ExecStart=/usr/bin/rsync --daemon --no-detach

[Install]
WantedBy=multi-user.target
EOF

%files
%defattr(-,root,root,-)
/etc/rsyncd.conf
%{_bindir}/rsync
/lib/systemd/system/rsync.service

%files doc
%defattr(-,root,root,-)
%{_mandir}/man1/rsync.1.gz
%{_mandir}/man5/rsyncd.conf.5.gz

%post
groupadd -g 48 rsyncd &&
useradd -c "rsyncd Daemon" -d /home/rsync -g rsyncd \
    -s /bin/false -u 48 rsyncd
systemctl enable rsync

%postun
/usr/sbin/userdel rsyncd
systemctl disable rsync

%clean
rm -rf %{buildroot}

%changelog
* Sun Jul 27 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.1.0 to 3.1.1
* Mon Sep 30 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.0.9 to 3.1.0
* Sun Oct 21 2012 tanggeliang <tanggeliang@gmail.com>
- create
