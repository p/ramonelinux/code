Name:       geoclue
Version:    2.3.0
Release:    1%{?dist}
Summary:    GeoClue 

Group:      Applications/Internet
License:    GPLv2+
Url:        http://www.freedesktop.org
Source:     http://www.freedesktop.org/software/geoclue/releases/2.3/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  glib dbus-glib libxslt gconf libsoup networkmanager libxml
BuildRequires:  systemd-udev intltool gettext xml-parser json-glib
BuildRequires:	modemmanager avahi

%description
GeoClue is a modular geoinformation service built on top of the D-Bus messaging system.
The goal of the GeoClue project is to make creating location-aware applications as simple as possible.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --libexecdir=%{_libdir}/geoclue \
            --sysconfdir=/etc \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
/etc/dbus-1/system.d/org.freedesktop.GeoClue2.Agent.conf
/etc/dbus-1/system.d/org.freedesktop.GeoClue2.conf
/etc/geoclue/geoclue.conf
%{_libdir}/geoclue/geoclue
%{_libdir}/geoclue/geoclue-2.0/demos/where-am-i
%{_libdir}/pkgconfig/geoclue-2.0.pc
%{_datadir}/applications/geoclue-*.desktop
%{_datadir}/dbus-1/system-services/org.freedesktop.GeoClue2.service
%{_datadir}/dbus-1/interfaces/org.freedesktop.GeoClue2.Agent.xml
%{_datadir}/dbus-1/interfaces/org.freedesktop.GeoClue2.xml
/lib/systemd/system/geoclue.service

%clean
rm -rf %{buildroot}

%changelog
* Tue Sep 22 2015 tanggeliang <tanggeliang@gmail.com>
- update from 2.2.0 to 2.3.0
* Wed Sep 16 2015 tanggeliang <tanggeliang@gmail.com>
- update from 2.1.10 to 2.2.0
* Sat Mar 15 2014 tanggeliang <tanggeliang@gmail.com>
- update from 2.0.0 to 2.1.6
* Tue Sep 24 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.12.99 to 2.0.0
* Wed Aug 7 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.12.0 to 0.12.99
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
