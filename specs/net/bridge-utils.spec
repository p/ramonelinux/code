Name:       bridge-utils
Version:    1.5
Release:    1%{?dist}
Summary:    bridge-utils

Group:      Applications/Internet
License:    GPLv2+
Url:        http://bridge.sourceforge.net
Source:     http://sourceforge.net/projects/bridge/files/bridge/%{name}-%{version}.tar.gz
Patch:      %{name}-%{version}-linux_3.8_fix-1.patch

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  autoconf automake m4 systemd
Requires:       net-tools

%description
The bridge-utils package contains a utility needed to create and manage bridge devices.
This is useful in setting up networks for a hosted virtual machine (VM).

%prep
%setup -q -n %{name}-%{version}
%patch -p1

%build
autoconf -o configure configure.in                      &&
./configure --prefix=/usr                               \
            --libdir=%{_libdir}                         &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_sbindir}/brctl
%{_mandir}/man8/brctl.8.gz

%clean
rm -rf %{buildroot}

%changelog
* Tue Feb 25 2014 tanggeliang <tanggeliang@gmail.com>
- create
