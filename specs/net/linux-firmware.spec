%global checkout a4f3bc03

Name:		linux-firmware
Version:	20140605
Release:	1%{?dist}
Summary:	Firmware files used by the Linux kernel

Group:		System Environment/Kernel
License:	GPL+ and GPLv2+ and MIT and Redistributable, no modification permitted
Url:		http://www.kernel.org
Source:     ftp://ftp.kernel.org/pub/linux/kernel/people/dwmw2/firmware/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildArch:      noarch
Requires:       systemd

%description
Kernel-firmware includes firmware files required for some devices to operate.

%prep
%setup -q -n %{name}-%{checkout}

%build

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}/lib/firmware
cp -r * %{buildroot}/lib/firmware
rm %{buildroot}/lib/firmware/{WHENCE,LICENCE.*,LICENSE.*}

# Create file list but exclude firmwares that we place in subpackages
FILEDIR=`pwd`
pushd %{buildroot}/lib/firmware
find . \! -type d > $FILEDIR/linux-firmware.files
find . -type d | sed -e '/^.$/d' > $FILEDIR/linux-firmware.dirs
popd
sed -i -e 's:^./::' linux-firmware.{files,dirs}
sed -i -e '/^iwlwifi/d' \
    -i -e '/^libertas\/sd8686/d' \
    -i -e '/^libertas\/usb8388/d' \
    -i -e '/^mrvl\/sd8787/d' \
    linux-firmware.files
sed -i -e 's/^/\/lib\/firmware\//' linux-firmware.{files,dirs}
sed -e 's/^/%%dir /' linux-firmware.dirs >> linux-firmware.files

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
/lib/firmware/3com/3C359.bin
/lib/firmware/3com/typhoon.bin
/lib/firmware/GPL-3
/lib/firmware/Makefile
/lib/firmware/README
/lib/firmware/ar5523.bin
/lib/firmware/carl9170-1.fw
/lib/firmware/configure
/lib/firmware/RTL8192E/boot.img
/lib/firmware/RTL8192E/data.img
/lib/firmware/RTL8192E/main.img
/lib/firmware/TDA7706_OM_v2.5.1_boot.txt
/lib/firmware/TDA7706_OM_v3.0.2_boot.txt
/lib/firmware/acenic/tg1.bin
/lib/firmware/acenic/tg2.bin
/lib/firmware/adaptec/starfire_rx.bin
/lib/firmware/adaptec/starfire_tx.bin
/lib/firmware/advansys/3550.bin
/lib/firmware/advansys/38C0800.bin
/lib/firmware/advansys/38C1600.bin
/lib/firmware/advansys/mcode.bin
/lib/firmware/agere_ap_fw.bin
/lib/firmware/agere_sta_fw.bin
/lib/firmware/ar3k/*
/lib/firmware/ar7010.fw
/lib/firmware/ar7010_1_1.fw
/lib/firmware/ar9170-1.fw
/lib/firmware/ar9170-2.fw
/lib/firmware/ar9271.fw
/lib/firmware/ath3k-1.fw
/lib/firmware/ath6k/AR600*/*
/lib/firmware/atmsar11.fw
/lib/firmware/av7110/Boot.S
/lib/firmware/av7110/Makefile
/lib/firmware/av7110/bootcode.bin
/lib/firmware/bnx2/bnx2-mips-*.fw
/lib/firmware/bnx2/bnx2-rv2p-*.fw
/lib/firmware/bnx2x-e1*-*.fw
/lib/firmware/bnx2x/bnx2x-e*-*.fw
/lib/firmware/brcm/*
/lib/firmware/carl9170fw/*
/lib/firmware/cis/*.cis
/lib/firmware/cis/Makefile
/lib/firmware/cis/src/*.cis
/lib/firmware/cpia2/stv0672_vp4.bin
/lib/firmware/cxgb*/*.bin
/lib/firmware/dabusb/bitstream.bin
/lib/firmware/dabusb/firmware.fw
/lib/firmware/dsp56k/Makefile
/lib/firmware/dsp56k/bootstrap.asm
/lib/firmware/dsp56k/bootstrap.bin
/lib/firmware/dsp56k/concat-bootstrap.pl
/lib/firmware/dvb-fe-xc5000-1.6.114.fw
/lib/firmware/dvb-usb-dib0700-1.20.fw
/lib/firmware/dvb-usb-terratec-h5-drxk.fw
/lib/firmware/e100/d101m_ucode.bin
/lib/firmware/e100/d101s_ucode.bin
/lib/firmware/e100/d102e_ucode.bin
/lib/firmware/edgeport/boot.fw
/lib/firmware/edgeport/boot2.fw
/lib/firmware/edgeport/down.fw
/lib/firmware/edgeport/down2.fw
/lib/firmware/edgeport/down3.bin
/lib/firmware/emi26/bitstream.fw
/lib/firmware/emi26/firmware.fw
/lib/firmware/emi26/loader.fw
/lib/firmware/emi62/bitstream.fw
/lib/firmware/emi62/loader.fw
/lib/firmware/emi62/midi.fw
/lib/firmware/emi62/spdif.fw
/lib/firmware/ene-ub6250/ms_init.bin
/lib/firmware/ene-ub6250/ms_rdwr.bin
/lib/firmware/ene-ub6250/msp_rdwr.bin
/lib/firmware/ene-ub6250/sd_init1.bin
/lib/firmware/ene-ub6250/sd_init2.bin
/lib/firmware/ene-ub6250/sd_rdwr.bin
/lib/firmware/ess/maestro3_assp_kernel.fw
/lib/firmware/ess/maestro3_assp_minisrc.fw
/lib/firmware/f2255usb.bin
/lib/firmware/htc_7010.fw
/lib/firmware/htc_9271.fw
/lib/firmware/i2400m-fw-usb-1.4.sbcf
/lib/firmware/i2400m-fw-usb-1.5.sbcf
/lib/firmware/i6050-fw-usb-1.5.sbcf
/lib/firmware/intelliport2.bin
/lib/firmware/isci/Makefile
/lib/firmware/isci/README
/lib/firmware/isci/create_fw.c
/lib/firmware/isci/create_fw.h
/lib/firmware/isci/isci_firmware.bin
/lib/firmware/isci/probe_roms.h
/lib/firmware/iwlwifi-*.ucode
/lib/firmware/kaweth/*code*.bin
/lib/firmware/keyspan/mpr.fw
/lib/firmware/keyspan/usa*.fw
/lib/firmware/keyspan_pda/Makefile
/lib/firmware/keyspan_pda/keyspan_pda.S
/lib/firmware/keyspan_pda/keyspan_pda.fw
/lib/firmware/keyspan_pda/xircom_pgs.S
/lib/firmware/keyspan_pda/xircom_pgs.fw
/lib/firmware/korg/k1212.dsp
/lib/firmware/lbtf_usb.bin
/lib/firmware/lgs8g75.fw
/lib/firmware/libertas/cf838*.bin
/lib/firmware/libertas/gspi868*.bin
/lib/firmware/libertas/lbtf_sdio.bin
/lib/firmware/libertas/sd8*.bin
/lib/firmware/libertas/usb8388_olpc.bin
/lib/firmware/libertas/usb8388_v5.bin
/lib/firmware/libertas/usb8388_v9.bin
/lib/firmware/libertas/usb8682.bin
/lib/firmware/matrox/g200_warp.fw
/lib/firmware/matrox/g400_warp.fw
/lib/firmware/mrvl/*_uapsta.bin
/lib/firmware/mts_cdma.fw
/lib/firmware/mts_edge.fw
/lib/firmware/mts_gsm.fw
/lib/firmware/mts_mt9234mu.fw
/lib/firmware/mts_mt9234zba.fw
/lib/firmware/mwl8k/fmimage_8366.fw
/lib/firmware/mwl8k/fmimage_8366_ap-1.fw
/lib/firmware/mwl8k/fmimage_8366_ap-2.fw
/lib/firmware/mwl8k/fmimage_8687.fw
/lib/firmware/mwl8k/helper_8366.fw
/lib/firmware/mwl8k/helper_8687.fw
/lib/firmware/myri10ge_eth_z8e.dat
/lib/firmware/myri10ge_ethp_z8e.dat
/lib/firmware/myri10ge_rss_eth_z8e.dat
/lib/firmware/myri10ge_rss_ethp_z8e.dat
/lib/firmware/myricom/lanai.bin
/lib/firmware/ositech/Xilinx7OD.bin
/lib/firmware/phanfw.bin
/lib/firmware/ql2*_fw.bin
/lib/firmware/qlogic/*.bin
/lib/firmware/qlogic/*.fw
/lib/firmware/r128/r128_cce.bin
/lib/firmware/radeon/*_*.bin
/lib/firmware/rt*.bin
/lib/firmware/rtl_nic/rtl8*.fw
/lib/firmware/rtlwifi/rtl8*.bin
/lib/firmware/s2250.fw
/lib/firmware/s2250_loader.fw
/lib/firmware/s5p-mfc/s5p-mfc*.fw
/lib/firmware/sb16/alaw_main.csp
/lib/firmware/sb16/ima_adpcm_capture.csp
/lib/firmware/sb16/ima_adpcm_init.csp
/lib/firmware/sb16/ima_adpcm_playback.csp
/lib/firmware/sb16/mulaw_main.csp
/lib/firmware/slicoss/gbdownload.sys
/lib/firmware/slicoss/gbrcvucode.sys
/lib/firmware/slicoss/oasisdbgdownload.sys
/lib/firmware/slicoss/oasisdownload.sys
/lib/firmware/slicoss/oasisrcvucode.sys
/lib/firmware/sun/cassini.bin
/lib/firmware/sxg/saharadbgdownloadB.sys
/lib/firmware/sxg/saharadownloadB.sys
/lib/firmware/tehuti/bdx.bin
/lib/firmware/ti-connectivity/TIInit_7.2.31.bts
/lib/firmware/ti-connectivity/wl1*.bin
/lib/firmware/ti_*.fw
/lib/firmware/tigon/tg3*.bin
/lib/firmware/tlg2300_firmware.bin
/lib/firmware/tr_smctr.bin
/lib/firmware/ttusb-budget/dspbootcode.bin
/lib/firmware/ueagle-atm/*.bin*
/lib/firmware/ueagle-atm/*.fw
/lib/firmware/usbdux/*dux
/lib/firmware/usbdux/*.asm
/lib/firmware/usbdux*_firmware.bin
/lib/firmware/v4l-cx*.fw
/lib/firmware/vicam/firmware.fw
/lib/firmware/vntwusb.fw
/lib/firmware/vxge/X3fw*.ncf
/lib/firmware/whiteheat*.fw
/lib/firmware/yam/*00.bin
/lib/firmware/yamaha/ds1_ctrl.fw
/lib/firmware/yamaha/ds1_dsp.fw
/lib/firmware/yamaha/ds1e_ctrl.fw
/lib/firmware/yamaha/yss225_registers.bin
/lib/firmware/amd-ucode/microcode_amd.bin
/lib/firmware/amd-ucode/microcode_amd.bin.asc
/lib/firmware/amd-ucode/microcode_amd_fam15h.bin
/lib/firmware/amd-ucode/microcode_amd_fam15h.bin.asc
/lib/firmware/cbfw-3.2.1.1.bin
/lib/firmware/ct2fw-3.2.1.1.bin
/lib/firmware/ctefx.bin
/lib/firmware/ctfw-3.2.1.1.bin
/lib/firmware/ctspeq.bin
/lib/firmware/go7007/go7007fw.bin
/lib/firmware/go7007/go7007tv.bin
/lib/firmware/go7007/lr192.fw
/lib/firmware/go7007/px-m402u.fw
/lib/firmware/go7007/px-tv402u.fw
/lib/firmware/go7007/s2250-1.fw
/lib/firmware/go7007/s2250-2.fw
/lib/firmware/go7007/wis-startrek.fw
/lib/firmware/intel/ibt-hw-37.7*.bseq
/lib/firmware/moxa/moxa-*.fw
/lib/firmware/mrvl/sd8688.bin
/lib/firmware/mrvl/sd8688_helper.bin
/lib/firmware/mt7650.bin
/lib/firmware/mwl8k/fmimage_8366_ap-3.fw
/lib/firmware/mwl8k/fmimage_8764_ap-1.fw
/lib/firmware/rp2.fw
/lib/firmware/sdd_sagrad_1091_1098.bin
/lib/firmware/cbfw-3.2.3.0.bin
/lib/firmware/ct2fw-3.2.3.0.bin
/lib/firmware/ctfw-3.2.3.0.bin
/lib/firmware/intel/ibt-hw-37.8.10-fw-1.10.2.27.d.bseq
/lib/firmware/intel/ibt-hw-37.8.bseq
/lib/firmware/myri10ge_eth_big_z8e.dat
/lib/firmware/myri10ge_ethp_big_z8e.dat
/lib/firmware/myri10ge_rss_eth_big_z8e.dat
/lib/firmware/myri10ge_rss_ethp_big_z8e.dat
/lib/firmware/qat_895xcc.bin
/lib/firmware/rsi_91x.fw
/lib/firmware/wsm_22.bin

%changelog
* Sat Jul 26 2014 tanggeliang <tanggeliang@gmail.com>
- update from 20131001 to 20140605
* Mon Oct 14 2013 tanggeliang <tanggeliang@gmail.com>
- update from 20130201 to 20131001
* Mon Mar 18 2013 tanggeliang <tanggeliang@gmail.com>
- update from 20120510 to 20130201
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
