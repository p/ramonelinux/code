Name:       librest
Version:    0.7.93
Release:    1%{?dist}
Summary:    librest

Group:      Applications/Internet
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/rest/0.7/rest-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  ca-certificates libsoup-gnome
BuildRequires:  gobject-introspection

%description
librest was designed to make it easier to access web services that claim to be "RESTful".
It includes convenience wrappers for libsoup and libxml to ease remote use of the RESTful API.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n rest-%{version}

%build
./configure --prefix=/usr \
            --with-ca-certificates=/etc/ssl/ca-bundle.crt \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_includedir}/rest-0.7/rest-extras/*.h
%{_includedir}/rest-0.7/rest/*.h
%{_libdir}/girepository-1.0/Rest-0.7.typelib
%{_libdir}/girepository-1.0/RestExtras-0.7.typelib
%{_libdir}/librest-0.7.*
%{_libdir}/librest-extras-0.7.*
%{_libdir}/pkgconfig/rest-0.7.pc
%{_libdir}/pkgconfig/rest-extras-0.7.pc
%{_datadir}/gir-1.0/Rest-0.7.gir
%{_datadir}/gir-1.0/RestExtras-0.7.gir

%files doc
%defattr(-,root,root,-)
%{_datadir}/gtk-doc/html/rest-0.7/*

%clean
rm -rf %{buildroot}

%changelog
* Wed Sep 23 2015 tanggeliang <tanggeliang@gmail.com>
- update from 0.7.91 to 0.7.93
* Thu Mar 20 2014 tanggeliang <tanggeliang@gmail.com>
- update from 0.7.90 to 0.7.91
* Sun Oct 14 2012 tanggeliang <tanggeliang@gmail.com>
- update from 0.7.12 to 0.7.90
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
