Name:       net-tools
Version:    2.0
Release:    1%{?dist}
Summary:    Net-tools

Group:      Applications/Internet
License:    GPLv2+
Url:        http://net-tools.sourceforge.net
Source:     git://git.code.sf.net/p/net-tools/code/%{name}-code-git20130713.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build

%description
The Net-tools package is a collection of programs for controlling the network subsystem of the Linux kernel.

%prep
%setup -q -n %{name}-code

%build
sed -i -e '/Token/s/y$/n/' config.in &&
yes "" | make config                 &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make update DESTDIR=%{buildroot}

%files
%defattr(-,root,root,-)
/bin/dnsdomainname
/bin/domainname
/bin/hostname
/bin/ifconfig
/bin/netstat
/bin/nisdomainname
/bin/route
/bin/ypdomainname
/sbin/arp
/sbin/ipmaddr
/sbin/iptunnel
/sbin/mii-tool
/sbin/nameif
/sbin/plipconfig
/sbin/rarp
/sbin/slattach
%{_mandir}/man*/*.gz

%clean
rm -rf %{buildroot}

%changelog
* Sat Jul 13 2013 tanggeliang <tanggeliang@gmail.com>
- update from CVS_20101030 to git20130713
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
