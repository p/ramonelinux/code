Name:       modemmanager
Version:    1.4.10
Release:    1%{?dist}
Summary:    ModemManager

Group:      Applications/Internet
License:    GPLv2+
Url:        http://www.freedesktop.org
Source:     http://www.freedesktop.org/software/ModemManager/ModemManager-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  glib dbus-glib gtk-doc systemd-gudev systemd
BuildRequires:  intltool gettext xml-parser
BuildRequires:  polkit ppp libmbim libqmi

%description
ModemManager provides a unified high level API for communicating with (mobile broadband) modems.
While the basic commands are standardized, the more advanced operations (like signal quality monitoring while connected) varies a lot.

%prep
%setup -q -n ModemManager-%{version}

%build
./configure --prefix=/usr \
            --sysconfdir=/etc \
            --without-qmi \
            --without-mbim \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
/etc/dbus-1/system.d/org.freedesktop.ModemManager1.conf
/lib/udev/rules.d/77-mm-*.rules
/lib/udev/rules.d/80-mm-candidate.rules
%{_bindir}/mmcli
%{_includedir}/ModemManager/*.h
%{_includedir}/libmm-glib/*.h
%{_libdir}/ModemManager/libmm-plugin-*.*a
%{_libdir}/ModemManager/libmm-plugin-*.so
%{_libdir}/libmm-glib.*a
%{_libdir}/libmm-glib.so*
%{_libdir}/pkgconfig/ModemManager.pc
%{_libdir}/pkgconfig/mm-glib.pc
%{_sbindir}/ModemManager
%{_datadir}/dbus-1/interfaces/org.freedesktop.ModemManager1.*.xml
%{_datadir}/dbus-1/interfaces/org.freedesktop.ModemManager1.xml
%{_datadir}/dbus-1/interfaces/wip-org.freedesktop.ModemManager1.Modem.Contacts.xml
%{_datadir}/dbus-1/system-services/org.freedesktop.ModemManager1.service
%{_datadir}/icons/hicolor/22x22/apps/ModemManager.png
%{_datadir}/locale/*/LC_MESSAGES/ModemManager.mo
%{_datadir}/polkit-1/actions/org.freedesktop.ModemManager1.policy
%{_mandir}/man8/ModemManager.8.gz
%{_mandir}/man8/mmcli.8.gz
/lib/systemd/system/ModemManager.service

%clean
rm -rf %{buildroot}

%changelog
* Wed Sep 16 2015 tanggeliang <tanggeliang@gmail.com>
- update from 1.2.0 to 1.4.10
* Sat Mar 15 2014 tanggeliang <tanggeliang@gmail.com>
- update from 0.7.991 to 1.2.0
* Wed Sep 25 2013 tanggeliang <tanggeliang@gmail.com>
- create
