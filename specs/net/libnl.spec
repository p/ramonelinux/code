Name:       libnl
Version:    3.2.25
Release:    1%{?dist}
Summary:    libnl

Group:      Applications/Internet
License:    GPLv2+
Url:        http://www.infradead.org/libnl
Source:     http://www.infradead.org/~tgr/libnl/files/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  bison flex

%description
The libnl suite is a collection of libraries providing APIs to netlink protocol based Linux kernel interfaces.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --sysconfdir=/etc \
            --disable-static \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
/etc/libnl/classid
/etc/libnl/pktloc
%{_includedir}/libnl3/netlink/*
%{_libdir}/libnl-3.*
%{_libdir}/libnl-cli-3.*
%{_libdir}/libnl-genl-3.*
%{_libdir}/libnl-idiag-3.*
%{_libdir}/libnl-nf-3.*
%{_libdir}/libnl-route-3.*
%{_libdir}/libnl/cli/cls/basic.*
%{_libdir}/libnl/cli/cls/cgroup.*
%{_libdir}/libnl/cli/qdisc/bfifo.*
%{_libdir}/libnl/cli/qdisc/blackhole.*
%{_libdir}/libnl/cli/qdisc/fq_codel.*
%{_libdir}/libnl/cli/qdisc/htb.*
%{_libdir}/libnl/cli/qdisc/ingress.*
%{_libdir}/libnl/cli/qdisc/plug.*
%{_libdir}/libnl/cli/qdisc/pfifo.*
%{_libdir}/pkgconfig/libnl-3.0.pc
%{_libdir}/pkgconfig/libnl-cli-3.0.pc
%{_libdir}/pkgconfig/libnl-genl-3.0.pc
%{_libdir}/pkgconfig/libnl-nf-3.0.pc
%{_libdir}/pkgconfig/libnl-route-3.0.pc
%{_sbindir}/genl-ctrl-list
%{_sbindir}/nl-class-add
%{_sbindir}/nl-class-delete
%{_sbindir}/nl-class-list
%{_sbindir}/nl-classid-lookup
%{_sbindir}/nl-cls-add
%{_sbindir}/nl-cls-delete
%{_sbindir}/nl-cls-list
%{_sbindir}/nl-link-list
%{_sbindir}/nl-pktloc-lookup
%{_sbindir}/nl-qdisc-add
%{_sbindir}/nl-qdisc-delete
%{_sbindir}/nl-qdisc-list

%files doc
%defattr(-,root,root,-)
%{_mandir}/*

%clean
rm -rf %{buildroot}

%changelog
* Sat Jul 26 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.2.24 to 3.2.25
* Tue Mar 18 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.2.23 to 3.2.24
* Thu Dec 5 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.2.22 to 3.2.23
* Sun Aug 18 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.2.21 to 3.2.22
* Thu Apr 11 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.2.14 to 3.2.21
* Sat Nov 3 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.2.12 to 3.2.14
* Sun Oct 14 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.2.11 to 3.2.12
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
