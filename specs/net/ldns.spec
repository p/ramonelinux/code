Name:       ldns
Version:    1.6.17
Release:    1%{?dist}
Summary:    ldns

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.nlnetlabs.nl
Source:     http://www.nlnetlabs.nl/downloads/ldns/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  openssl

%description
The goal of ldns is to simplify DNS programming, it supports recent RFCs like the DNSSEC documents, and allows developers to easily create software conforming to current RFCs, and experimental software for current Internet Drafts.
A secondary benefit of using ldns is speed; ldns is written in C it should be a lot faster than Perl.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr     \
            --sysconfdir=/etc \
            --disable-static  \
            --with-drill      \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/drill
%{_bindir}/ldns-config
%{_includedir}/ldns/*.h
%{_libdir}/libldns.*a
%{_libdir}/libldns.so*

%files doc
%defattr(-,root,root,-)
%{_mandir}/man1/ldns-config.1.gz
%{_mandir}/man3/ldns_*.3.gz
%{_mandir}/man1/drill.1.gz

%clean
rm -rf %{buildroot}

%changelog
* Fri Mar 21 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.6.16 to 1.6.17
* Sat Jul 6 2013 tanggeliang <tanggeliang@gmail.com>
- create
