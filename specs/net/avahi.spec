Name:       avahi
Version:    0.6.31
Release:    3%{?dist}
Summary:    Avahi

Group:      Applications/Internet
License:    GPLv2+
Url:        http://avahi.org
Source:     http://avahi.org/download/%{name}-%{version}.tar.gz
Patch:      avahi-0.6.31-gtkstock_deprecated.patch

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  glib intltool
BuildRequires:  dbus-python gobject-introspection gtk2 gtk+ libdaemon libglade
BuildRequires:  gettext xml-parser gdbm

%description
The Avahi package is a system which facilitates service discovery on a local network.

%prep
%setup -q -n %{name}-%{version}
%patch -p1

%build
./configure --prefix=/usr        \
            --sysconfdir=/etc    \
            --localstatedir=/var \
            --disable-static     \
            --disable-mono       \
            --disable-monodoc    \
            --disable-python     \
            --disable-qt3        \
            --disable-qt4        \
            --enable-core-docs   \
            --with-distro=none   \
            --with-systemdsystemunitdir=/lib/systemd/system \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
/etc/avahi/avahi-autoipd.action
/etc/avahi/avahi-daemon.conf
/etc/avahi/avahi-dnsconfd.action
/etc/avahi/hosts
/etc/avahi/services/sftp-ssh.service
/etc/avahi/services/ssh.service
/etc/dbus-1/system.d/avahi-dbus.conf
%{_bindir}/avahi-browse
%{_bindir}/avahi-browse-domains
%{_bindir}/avahi-discover-standalone
%{_bindir}/avahi-publish
%{_bindir}/avahi-publish-address
%{_bindir}/avahi-publish-service
%{_bindir}/avahi-resolve
%{_bindir}/avahi-resolve-address
%{_bindir}/avahi-resolve-host-name
%{_bindir}/avahi-set-host-name
%{_bindir}/bshell
%{_bindir}/bssh
%{_bindir}/bvnc
%{_includedir}/avahi-client/*.h
%{_includedir}/avahi-common/*.h
%{_includedir}/avahi-core/*.h
%{_includedir}/avahi-glib/glib-*.h
%{_includedir}/avahi-gobject/ga-*.h
%{_includedir}/avahi-ui/avahi-ui.h
%{_libdir}/girepository-1.0/Avahi*-0.6.typelib
%{_libdir}/libavahi-client.*
%{_libdir}/libavahi-common.*
%{_libdir}/libavahi-core.*
%{_libdir}/libavahi-glib.*
%{_libdir}/libavahi-gobject.*
%{_libdir}/libavahi-ui-gtk3.*
%{_libdir}/libavahi-ui.*
%{_libdir}/pkgconfig/avahi-client.pc
%{_libdir}/pkgconfig/avahi-core.pc
%{_libdir}/pkgconfig/avahi-glib.pc
%{_libdir}/pkgconfig/avahi-gobject.pc
%{_libdir}/pkgconfig/avahi-ui-gtk3.pc
%{_libdir}/pkgconfig/avahi-ui.pc
%{_sbindir}/avahi-autoipd
%{_sbindir}/avahi-daemon
%{_sbindir}/avahi-dnsconfd
%{_datadir}/applications/bssh.desktop
%{_datadir}/applications/bvnc.desktop
%{_datadir}/avahi/avahi-service.dtd
%{_datadir}/avahi/interfaces/avahi-discover.ui
%{_datadir}/avahi/service-types
%{_datadir}/dbus-1/interfaces/org.freedesktop.Avahi.*.xml
%{_datadir}/dbus-1/system-services/org.freedesktop.Avahi.service
%{_datadir}/gir-1.0/Avahi*-0.6.gir
%{_datadir}/locale/*/LC_MESSAGES/avahi.mo
/lib/systemd/system/avahi-daemon.service
/lib/systemd/system/avahi-daemon.socket
/lib/systemd/system/avahi-dnsconfd.service
%{_mandir}/man*/*.gz

%post
groupadd -fg 84 avahi &&
useradd -c "Avahi Daemon Owner" -d /var/run/avahi-daemon -u 84 \
        -g avahi -s /bin/false avahi
groupadd -fg 86 netdev
systemctl enable avahi-daemon avahi-dnsconfd

%postun
systemctl disable avahi-daemon avahi-dnsconfd

%clean
rm -rf %{buildroot}

%changelog
* Sun Sep 30 2012 tanggeliang <tanggeliang@gmail.com>
- create
