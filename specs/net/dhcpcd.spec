Name:       dhcpcd
Version:    6.4.2
Release:    1%{?dist}
Summary:    dhcpcd

Group:      Applications/Internet
License:    GPLv2+
Url:        http://roy.marples.name
Source:     http://roy.marples.name/downloads/dhcpcd/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  systemd

%description
dhcpcd is an implementation of the DHCP client specified in RFC2131.
A DHCP client is useful for connecting your computer to a network which uses DHCP to assign network addresses.
dhcpcd strives to be a fully featured, yet very lightweight DHCP client.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --libexecdir=/%{_lib}/dhcpcd \
            --dbdir=/var/tmp \
            --sysconfdir=/etc &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

mkdir -pv %{buildroot}/%{_lib}/dhcpcd/dhcpcd-hooks/
sed -i "s;/var/lib;/run;g" dhcpcd-hooks/50-dhcpcd-compat &&
install -v -m 644 dhcpcd-hooks/50-dhcpcd-compat %{buildroot}/%{_lib}/dhcpcd/dhcpcd-hooks/

%files
%defattr(-,root,root,-)
/etc/dhcpcd.conf
/lib/dhcpcd/dev/udev.so
/%{_lib}/dhcpcd/dhcpcd-hooks/01-test
/%{_lib}/dhcpcd/dhcpcd-hooks/02-dump
/%{_lib}/dhcpcd/dhcpcd-hooks/10-mtu
/%{_lib}/dhcpcd/dhcpcd-hooks/10-wpa_supplicant
/%{_lib}/dhcpcd/dhcpcd-hooks/15-timezone
/%{_lib}/dhcpcd/dhcpcd-hooks/20-resolv.conf
/%{_lib}/dhcpcd/dhcpcd-hooks/29-lookup-hostname
/%{_lib}/dhcpcd/dhcpcd-hooks/30-hostname
/%{_lib}/dhcpcd/dhcpcd-hooks/50-dhcpcd-compat
/%{_lib}/dhcpcd/dhcpcd-run-hooks
/sbin/dhcpcd

%files doc
%defattr(-,root,root,-)
%{_mandir}/*

%clean
rm -rf %{buildroot}

%changelog
* Sat Jul 26 2014 tanggeliang <tanggeliang@gmail.com>
- update from 6.3.2 to 6.4.2
* Tue Mar 18 2014 tanggeliang <tanggeliang@gmail.com>
- update from 6.1.0 to 6.3.2
* Mon Oct 7 2013 tanggeliang <tanggeliang@gmail.com>
- update from 6.0.5 to 6.1.0
* Wed Aug 14 2013 tanggeliang <tanggeliang@gmail.com>
- update from 5.6.7 to 6.0.5
* Thu May 30 2013 tanggeliang <tanggeliang@gmail.com>
- update from 5.6.2 to 5.6.7
* Fri Nov 2 2012 tanggeliang <tanggeliang@gmail.com>
- update from 5.6.1 to 5.6.2
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
