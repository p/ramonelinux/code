%global icedtea_version 2.4.3

Name:       openjdk
Version:    1.7.0.45
Release:    1%{?dist}
Summary:    OpenJDK and IcedTea

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.icedtea.org
Source0:    http://icedtea.classpath.org/download/source/icedtea-%{icedtea_version}.tar.xz
Source1:    corba.tar.gz
Source2:    hotspot.tar.gz
Source3:    openjdk.tar.gz
Source4:    jaxp.tar.gz
Source5:    jaxws.tar.gz
Source6:    langtools.tar.gz
Source7:    jdk.tar.gz
Patch0:     icedtea-%{icedtea_version}-add_cacerts-1.patch
Patch1:     icedtea-%{icedtea_version}-fixed_paths-1.patch
Patch2:     icedtea-%{icedtea_version}-fix_tests-1.patch

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  openjdk
BuildRequires:  ca-certificates cups gtk+ giflib pulseaudio libx11 libxp libxtst libxi libxt nspr 
BuildRequires:  apache-ant cpio unzip wget which zip
BuildRequires:  autoconf automake m4 rhino libxslt attr
BuildRequires:  procps-ng krb lcms2

%description
IcedTea provides a build harness for the OpenJDK package, Oracle's open-sourced Java development environment.
In order to provide a completely free runtime environment, similar to Oracle's closed distribution, the IcedTea build harness also provides free, and arguably better versions of parts of the JDK which have not been open-sourced to date.
OpenJDK is useful for developing Java programs and provides a complete runtime environment to run Java programs.

%package        jre
Summary:        Java Runtime Environment

%description    jre
JRE contains everything required to run Java applications on your system.

%package        demo
Summary:        Demo
%description    demo

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n icedtea-%{icedtea_version}
%patch0 -p1
%patch1 -p1
%patch2 -p1

%build
cp -v %SOURCE1 . &&
cp -v %SOURCE2 . &&
cp -v %SOURCE3 . &&
cp -v %SOURCE4 . &&
cp -v %SOURCE5 . &&
cp -v %SOURCE6 . &&
cp -v %SOURCE7 .

unset JAVA_HOME                                               &&
./autogen.sh                                                  &&
./configure --with-jdk-home=/usr/share/jdk                    \
            --with-version-suffix=blfs                        \
            --enable-nss                                      \
            --enable-pulse-java                               \
            --disable-system-kerberos                         \
            --with-parallel-jobs=$(getconf _NPROCESSORS_ONLN) &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
mkdir -pv %{buildroot}/%{_datadir}
chmod 0644 openjdk.build/j2sdk-image/lib/sa-jdi.jar  &&
cp -R openjdk.build/j2sdk-image %{buildroot}/%{_datadir}/OpenJDK-%{version} &&
#chown -R root:root %{buildroot}/%{_datadir}/OpenJDK-%{version}

ln -v -nsf OpenJDK-%{version} %{buildroot}/%{_datadir}/jdk

mkdir -pv %{buildroot}/etc/profile.d
cat > %{buildroot}/etc/profile.d/openjdk.sh << "EOF"
# Begin /etc/profile.d/openjdk.sh

# Set JAVA_HOME directory
JAVA_HOME=/usr/share/jdk

# Adjust PATH
pathappend $JAVA_HOME/bin PATH

# Auto Java CLASSPATH
# Copy jar files to, or create symlinks in this directory

AUTO_CLASSPATH_DIR=/usr/share/java

pathprepend . CLASSPATH

for dir in `find ${AUTO_CLASSPATH_DIR} -type d 2>/dev/null`; do
    pathappend $dir CLASSPATH
done

for jar in `find ${AUTO_CLASSPATH_DIR} -name "*.jar" 2>/dev/null`; do
    pathappend $jar CLASSPATH
done

export JAVA_HOME CLASSPATH
unset AUTO_CLASSPATH_DIR dir jar

# End /etc/profile.d/openjdk.sh
EOF

%files
%defattr(-,root,root,-)
/etc/profile.d/openjdk.sh
%{_datadir}/OpenJDK-%{version}/ASSEMBLY_EXCEPTION
%{_datadir}/OpenJDK-%{version}/bin/*
%{_datadir}/OpenJDK-%{version}/include/*
%{_datadir}/OpenJDK-%{version}/lib/*
%{_datadir}/OpenJDK-%{version}/LICENSE
%{_datadir}/OpenJDK-%{version}/release
%{_datadir}/OpenJDK-%{version}/src.zip
%{_datadir}/OpenJDK-%{version}/THIRD_PARTY_README
%{_datadir}/jdk

%files jre
%defattr(-,root,root,-)
%{_datadir}/OpenJDK-%{version}/jre/ASSEMBLY_EXCEPTION
%{_datadir}/OpenJDK-%{version}/jre/bin/*
%{_datadir}/OpenJDK-%{version}/jre/lib/*
%{_datadir}/OpenJDK-%{version}/jre/LICENSE
%{_datadir}/OpenJDK-%{version}/jre/THIRD_PARTY_README

%files demo
%defattr(-,root,root,-)
%{_datadir}/OpenJDK-%{version}/demo/*
%{_datadir}/OpenJDK-%{version}/sample/*

%files doc
%defattr(-,root,root,-)
%{_datadir}/OpenJDK-%{version}/man/*

#%post
#cat >> /etc/man_db.conf << "EOF" &&
#MANDATORY_MANPATH     /opt/jdk/man
#MANPATH_MAP           /opt/jdk/bin     /opt/jdk/man
#MANDB_MAP             /opt/jdk/man     /var/cache/man/jdk
#EOF

#mandb -c /opt/jdk/man

%clean
rm -rf %{buildroot}

%changelog
* Sat Dec 7 2013 tanggeliang <tanggeliang@gmail.com>
- update openjdk from 1.7.0.40 to 1.7.0.45
- update icedtea from 2.4.2 to 2.4.3
* Fri Oct 4 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.4.1 to 2.4.2
* Thu Jul 18 2013 tanggeliang <tanggeliang@gmail.com>
- update openjdk from 1.7.0.9 to 1.7.0.40
- update icedtea from 2.3.3 to 2.4.1
* Fri Oct 26 2012 tanggeliang <tanggeliang@gmail.com>
- create
