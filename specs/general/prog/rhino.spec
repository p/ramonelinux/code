Name:       rhino
Version:    1.7
Release:    1%{?dist}
Summary:    Rhino

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.mozilla.org
Source:     ftp://ftp.mozilla.org/pub/mozilla.org/js/%{name}1_7R3.zip

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build

%description
Rhino Java Script.

%prep
%setup -q -n %{name}1_7R3

%build

%check

%install
rm -rf %{buildroot}
install -v -d -m755 %{buildroot}/usr/share/java &&
install -v -m755 *.jar %{buildroot}/usr/share/java

%files
%defattr(-,root,root,-)
%{_datadir}/java/js-14.jar
%{_datadir}/java/js.jar

%clean
rm -rf %{buildroot}

%changelog
* Fri Oct 26 2012 tanggeliang <tanggeliang@gmail.com>
- create
