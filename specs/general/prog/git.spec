Name:       git
Version:    2.5.0
Release:    1%{?dist}
Summary:    Git

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://git-scm.com
Source:     https://www.kernel.org/pub/software/scm/git/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  curl expat openssl python
BuildRequires:  pcre tk
BuildRequires:  gettext
AutoReq:        no

%description
Git is a free and open source, distributed version control system designed to handle everything from small to very large projects with speed and efficiency.
Every Git clone is a full-fledged repository with complete history and full revision tracking capabilities, not dependent on network access or a central server.
Branching and merging are fast and easy to do.
Git is used for version control of files, much like tools such as Mercurial, Bazaar, Subversion, CVS, Perforce, and Team Foundation Server.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --with-gitconfig=/etc/gitconfig \
            --libexecdir=%{_libdir} \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

rm -rf %{buildroot}/%{perl_archlib}/perllocal.pod

%files
%defattr(-,root,root,-)
%{_bindir}/git
%{_bindir}/git-cvsserver
%{_bindir}/git-receive-pack
%{_bindir}/git-shell
%{_bindir}/git-upload-archive
%{_bindir}/git-upload-pack
%{_bindir}/gitk
%{_libdir}/git-core/git
%{_libdir}/git-core/git-*
%{_libdir}/git-core/mergetools/*
%{perl_sitearch}/auto/Git/.packlist
%{perl_sitelib}/*.pm
%{perl_sitelib}/Git/*.pm
%{perl_sitelib}/Git/SVN/*.pm
%{perl_sitelib}/Git/SVN/Memoize/*.pm
%{_datadir}/git-core/templates/*
%{_datadir}/git-gui/lib/*
%{_datadir}/gitk/lib/msgs/*.msg
%{_datadir}/gitweb/gitweb.cgi
%{_datadir}/gitweb/static/*
%{_datadir}/locale/*/LC_MESSAGES/git.mo

%files doc
%defattr(-,root,root,-)
%{_mandir}/man3/Git*.3.gz
%{_mandir}/man3/private-Error.3.gz

%clean
rm -rf %{buildroot}

%changelog
* Mon Sep 14 2015 tanggeliang <tanggeliang@gmail.com>
- update from 2.3.4 to 2.5.0
* Sat Mar 28 2015 tanggeliang <tanggeliang@gmail.com>
- update from 2.2.2 to 2.3.4
* Sat Jan 24 2015 tanggeliang <tanggeliang@gmail.com>
- update from 1.9.1 to 2.2.2
* Thu Mar 20 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.9.0 to 1.9.1
* Tue Mar 18 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.8.5.1 to 1.9.0
* Thu Dec 5 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.8.4.1 to 1.8.5.1
* Mon Oct 7 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.8.4 to 1.8.4.1
* Thu Oct 3 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.8.3.4 to 1.8.4
* Wed Aug 21 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.8.3 to 1.8.3.4
* Sat Jul 6 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.8.2.3 to 1.8.3
* Thu May 30 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.8.2.1 to 1.8.2.3
* Sun Apr 14 2013 tanggeliang <tanggeliang@gmail.com>
- create
