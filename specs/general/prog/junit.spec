Name:       junit
Version:    4.11
Release:    1%{?dist}
Summary:    JUnit

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://junit.sourceforge.net
Source0:    http://downloads.sourceforge.net/junit/%{name}4-%{version}.tar.xz
Source1:    search.maven.org/remotecontent?filepath=junit/junit/4.11/junit-4.11.jar
Source2:    http://hamcrest.googlecode.com/files/hamcrest-1.3.tgz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  apache-ant unzip openjdk

%description
The JUnit package contains a simple, open source framework to write and run repeatable tests. It is an instance of the xUnit architecture for unit testing frameworks.
JUnit features include assertions for testing expected results, test fixtures for sharing common test data, and test runners for running tests.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}4-%{version}

%build
tar -xf %{SOURCE2}                              &&
cp -v hamcrest-1.3/hamcrest-core-1.3{,-sources}.jar lib/ &&
ant dist

%check

%install
rm -rf %{buildroot}

install -v -m755 -d %{buildroot}/usr/share/java &&
cp -v %{SOURCE1} %{buildroot}/usr/share/java

install -v -m755 -d %{buildroot}/usr/share/{doc,java}/junit-%{version}
#chown -R root:root .
cp -v -R junit*/javadoc/*             %{buildroot}/usr/share/doc/junit-%{version}  &&
cp -v junit*/junit*.jar               %{buildroot}/usr/share/java/junit-%{version} &&
cp -v hamcrest-1.3/hamcrest-core*.jar %{buildroot}/usr/share/java/junit-%{version}

export CLASSPATH=$CLASSPATH:/usr/share/java/junit-%{version}

%files
%defattr(-,root,root,-)
%{_datadir}/java/junit-%{version}.jar
%{_datadir}/java/junit-%{version}/*

%files doc
%defattr(-,root,root,-)
%{_docdir}/junit-%{version}/*

%clean
rm -rf %{buildroot}

%changelog
* Thu Jul 25 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.10 to 4.11
* Fri Oct 26 2012 tanggeliang <tanggeliang@gmail.com>
- create
