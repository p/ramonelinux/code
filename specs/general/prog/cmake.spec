Name:       cmake
Version:    3.2.1
Release:    1%{?dist}
Summary:    CMake

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.cmake.org
Source:     http://www.cmake.org/files/v3.2/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  curl libarchive expat

%description
The CMake package contains a modern toolset used for generating Makefiles.
It is a successor of the auto-generated configure script and aims to be platform- and compiler-independent.
A significant user of CMake is KDE since version 4.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./bootstrap --prefix=/usr \
            --system-libs \
            --mandir=/share/man \
            --no-system-jsoncpp \
            --docdir=/share/doc/cmake-%{version} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/ccmake
%{_bindir}/cmake
%{_bindir}/cpack
%{_bindir}/ctest
%{_datadir}/aclocal/cmake.m4
%{_datadir}/cmake-3.2/Help/*
%{_datadir}/cmake-3.2/Modules/.NoDartCoverage
%{_datadir}/cmake-3.2/Modules/*
%{_datadir}/cmake-3.2/Templates/*
%{_datadir}/cmake-3.2/completions/*
%{_datadir}/cmake-3.2/editors/*
%{_datadir}/cmake-3.2/include/cmCPluginAPI.h

%files doc
%defattr(-,root,root,-)
%{_docdir}/cmake-%{version}/*

%clean
rm -rf %{buildroot}

%changelog
* Sat Mar 28 2015 tanggeliang <tanggeliang@gmail.com>
- update from 2.8.12.2 to 3.2.1
* Thu Mar 20 2014 tanggeliang <tanggeliang@gmail.com>
- update from 2.8.12.1 to 2.8.12.2
* Sun Dec 1 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.8.12 to 2.8.12.1
* Sun Oct 20 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.8.11.2 to 2.8.12
* Mon Jul 29 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.8.11 to 2.8.11.2
* Thu May 30 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.8.10.2 to 2.8.11
* Sat Mar 2 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.8.9 to 2.8.10.2
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
