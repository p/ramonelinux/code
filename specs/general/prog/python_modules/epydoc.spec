%define python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")

Name:       epydoc
Version:    3.0.1
Release:    12%{?dist}
Summary:    Automatic API documentation generation tool for Python

Group:      Development/Tools
License:    MIT
URL:        http://epydoc.sourceforge.net/
Source0:    http://dl.sf.net/epydoc/epydoc-%{version}.tar.gz
Source1:    epydocgui.desktop
Patch0:     epydoc-3.0.1-nohashbang.patch
Patch1:     epydoc-3.0.1-giftopng.patch
Patch2:     epydoc-3.0.1-new-docutils.patch

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root
BuildRequires:  python
BuildRequires:  desktop-file-utils
Requires:       tk

%description
Epydoc  is a tool for generating API documentation for Python modules,
based  on their docstrings. For an example of epydoc's output, see the
API  documentation for epydoc itself (html, pdf). A lightweight markup
language  called  epytext can be used to format docstrings, and to add
information  about  specific  fields,  such as parameters and instance
variables.    Epydoc    also   understands   docstrings   written   in
ReStructuredText, Javadoc, and plaintext.

%prep
%setup -q
%patch0 -p1 -b .nohashbang
%patch1 -p1 -b .giftopng
%patch2 -p1 -b .new-docutils

%build
%{__python} setup.py build

%install
%{__rm} -rf %{buildroot}
%{__python} setup.py install -O1 --skip-build --root=%{buildroot}

desktop-file-install \
    --vendor="" \
    --dir=%{buildroot}%{_datadir}/applications \
    --mode=0644 \
    %{SOURCE1}

# Also install the man pages
%{__mkdir_p} %{buildroot}%{_mandir}/man1
%{__install} -p -m 0644 man/*.1 %{buildroot}%{_mandir}/man1/

# Prevent having *.pyc and *.pyo in _bindir
%{__mv} %{buildroot}%{_bindir}/apirst2html.py %{buildroot}%{_bindir}/apirst2html

%clean
%{__rm} -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc LICENSE.txt README.txt doc/
%{_bindir}/apirst2html
%{_bindir}/epydoc
%{_bindir}/epydocgui
%{python_sitelib}/epydoc/
%{python_sitelib}/epydoc-*.egg-info
%{_datadir}/applications/epydocgui.desktop
%{_mandir}/man1/*.1*

%changelog
