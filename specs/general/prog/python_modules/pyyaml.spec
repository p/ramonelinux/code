%{!?python_sitearch: %global python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")}

Name:       pyyaml
Version:    3.10
Release:    8%{?dist}
Summary:    YAML parser and emitter for Python

Group:      Development/Libraries
License:    MIT
Url:        http://pyyaml.org
Source:     http://pyyaml.org/download/pyyaml/PyYAML-%{version}.tar.gz

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  python-setuptools yaml

%description
PyYAML is a YAML parser and emitter for Python.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n PyYAML-%{version}
chmod a-x examples/yaml-highlight/yaml_hl.py

%build
CFLAGS="${RPM_OPT_FLAGS}" %{__python} setup.py --with-libyaml build

%install
rm -rf %{buildroot}
%{__python} setup.py install -O1 --skip-build --root %{buildroot}

%clean
rm -rf %{buildroot}

%files
%defattr(644,root,root,755)
%{python_sitearch}/*

%files doc
%defattr(644,root,root,755)
%doc CHANGES LICENSE PKG-INFO README examples

%changelog
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
