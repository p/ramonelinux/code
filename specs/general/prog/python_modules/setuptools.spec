Name:       setuptools
Version:    2.0
Release:    1%{?dist}
Summary:    Easily build and distribute Python packages

Group:      Applications/System
License:    Python or ZPLv2.0
Url:        http://pypi.python.org/pypi/%{name}
Source0:    http://pypi.python.org/packages/source/d/%{name}/%{name}-%{version}.tar.gz
Source1:    psfl.txt
Source2:    zpl.txt
Patch0:     setuptools-sdist.patch

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
Setuptools is a collection of enhancements to the Python distutils that allow you to more easily build and distribute Python packages, especially ones that have dependencies on other packages.
This package contains the runtime components of setuptools, necessary to execute the software that requires pkg_resources.py.
This package contains the distribute fork of setuptools.

%prep
%setup -q -n %{name}-%{version}
%patch0 -p1

%build
CFLAGS="$RPM_OPT_FLAGS" %{__python} setup.py build

%install
rm -rf %{buildroot}
%{__python} setup.py install --skip-build --root %{buildroot}

install -p -m 0644 %{SOURCE1} %{SOURCE2} .

%check

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc *.txt docs
%{_bindir}/easy_install
%{_bindir}/easy_install-2.*
%{_libdir}/python2.7/site-packages/_markerlib/*.py*
%{_libdir}/python2.7/site-packages/*.py*
%{_libdir}/python2.7/site-packages/setuptools-2.0-py2.7.egg-info/*
%{_libdir}/python2.7/site-packages/setuptools/*.py*
%{_libdir}/python2.7/site-packages/setuptools/command/*.py*
%{_libdir}/python2.7/site-packages/setuptools/tests/*.py*

%changelog
* Sun Jul 27 2014 tanggeliang <tanggeliang@gmail.com>
- update from 0.6.27 to 2.0
- change name from python-setuptools to setuptools.
