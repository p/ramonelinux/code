Name:       yum-metadata-parser
Version:    1.1.4
Release:    9%{?dist}
Summary:    A fast metadata parser for yum

Group:      Development/Libraries
License:    GPLv2
Url:        http://linux.duke.edu/projects/yum/
Source:     http://linux.duke.edu/projects/yum/download/%{name}/%{name}-%{version}.tar.gz
Patch:      BZ-612409-handle-2GB-rpms.patch

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  python glib libxml sqlite

%description
Fast metadata parser for yum implemented in C.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q
%patch -p1

%build
python setup.py build

%install
rm -rf %{buildroot}
python setup.py install -O1 --root=%{buildroot}

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%{_libdir}/python2.7/site-packages/_sqlitecache.so
%{_libdir}/python2.7/site-packages/sqlitecachec.py*
%{_libdir}/python2.7/site-packages/yum_metadata_parser-1.1.4-py2.7.egg-info

%files doc
%defattr(-,root,root,-)
%doc README AUTHORS ChangeLog

%changelog
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
