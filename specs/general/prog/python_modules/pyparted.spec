Name:       pyparted
Version:    3.8
Release:    5%{?dist}
Summary:    Python module for GNU parted

Group:      System Environment/Libraries
License:    GPLv2+
URL:        http://fedorahosted.org/pyparted
Source:     http://fedorahosted.org/releases/p/y/%{name}/%{name}-%{version}.tar.gz
Patch:      0001-Add-support-for-new-disk-flag-PED_DISK_GPT_PMBR_BOOT.patch

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(id -u -n)
BuildRequires:  python parted pkg-config

%description
Python module for the parted library.  It is used for manipulating partition tables.

%prep
%setup -q
%patch -p1

%build
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc AUTHORS BUGS COPYING ChangeLog NEWS README TODO
%{python_sitearch}/_pedmodule.so
%{python_sitearch}/parted
%{python_sitearch}/%{name}-%{version}-*.egg-info

%changelog
