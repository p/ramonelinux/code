Name:       nose
Version:    1.3.2
Release:    1%{?dist}
Summary:    Discovery-based unittest extension for Python

Group:      Development/Languages
License:    LGPLv2+ and Public Domain
Url:        http://somethingaboutorange.com/mrl/projects/nose/
Source:     http://pypi.python.org/packages/source/n/nose/%{name}-%{version}.tar.gz

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  setuptools
BuildRequires:  dos2unix
BuildRequires:  coverage >= 3.4-1
Requires:       setuptools

%description
nose extends the test loading and running features of unittest, making it easier to write, find and run tests.
By default, nose will run tests in files or directories under the current working directory whose names include "test" or "Test" at a word boundary (like "test_this" or "functional_test" or "TestClass" but not "libtest").
Test output is similar to that of unittest, but also includes captured stdout output from failing tests, for easy print-style debugging.
These features, and many more, are customizable through the use of plugins.
Plugins included with nose provide support for doctest, code coverage and profiling, flexible attribute-based test selection, output capture and more.

%prep
%setup -q -n %{name}-%{version}

dos2unix examples/attrib_plugin.py

%build
%{__python} setup.py build

%install
rm -rf %{buildroot}
%{__python} setup.py install --skip-build --root %{buildroot} \
           --install-data=%{_datadir}

cp -a doc reST
rm -rf reST/.static reST/.templates

%check

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%{_bindir}/nosetests
%{_bindir}/nosetests-%{python_version}
%{_libdir}/python2.7/site-packages/nose-1.3.2-py2.7.egg-info/*
%{_libdir}/python2.7/site-packages/nose/*.py*
%{_libdir}/python2.7/site-packages/nose/ext/*.py*
%{_libdir}/python2.7/site-packages/nose/plugins/*.py*
%{_libdir}/python2.7/site-packages/nose/sphinx/*.py*
%{_libdir}/python2.7/site-packages/nose/tools/*.py*
%{_libdir}/python2.7/site-packages/nose/usage.txt
%{_mandir}/man1/nosetests.1.gz

%changelog
* Sun Jul 27 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.1.2 to 1.3.2
- change name from python-nose to nose.
