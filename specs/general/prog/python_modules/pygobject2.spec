Name:       pygobject2
Version:    2.28.6
Release:    6%{?dist}
Summary:    PyGObject Module

Group:      System Environment/xlib
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/pygobject/2.28/pygobject-%{version}.tar.xz
Patch:      pygobject-2.28.6-introspection-1.patch

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  glib py2cairo gobject-introspection libxslt

%description
PyGObject-2.28.6 provides Python 2 bindings to the GObject class from GLib.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n pygobject-%{version}
%patch -p1

%build
./configure --prefix=/usr \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/pygobject-codegen-2.0
%{_includedir}/pygtk-2.0/pyglib.h
%{_includedir}/pygtk-2.0/pygobject.h
%{_libdir}/libpyglib-2.0-python.la
%{_libdir}/libpyglib-2.0-python.so
%{_libdir}/libpyglib-2.0-python.so.0
%{_libdir}/libpyglib-2.0-python.so.0.0.0
%{_libdir}/python2.7/site-packages/gi/*
%{_libdir}/python2.7/site-packages/glib/*
%{_libdir}/python2.7/site-packages/gobject/*
%{_libdir}/python2.7/site-packages/gtk-2.0/*
%{_libdir}/python2.7/site-packages/pygtk.*
%{_libdir}/pkgconfig/pygobject-2.0.pc
%{_datadir}/pygobject/2.0/*
%{_datadir}/pygobject/xsl/*

%files doc
%defattr(-,root,root,-)
%{_datadir}/gtk-doc/html/pygobject/*

%clean
rm -rf %{buildroot}

%changelog
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
