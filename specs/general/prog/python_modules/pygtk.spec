Name:       pygtk
Version:    2.24.0
Release:    6%{?dist}
Summary:    PyGTK Module 

Group:      System Environment/xlib
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/%{name}/2.24/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  pygobject2 atk pango py2cairo gtk+ libglade libxslt

%description
PyGTK lets you to easily create programs with a graphical user interface using the Python programming language.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/pygtk-codegen-2.0
%{_bindir}/pygtk-demo
%{_includedir}/pygtk-2.0/pygtk/pygtk.h
%{_libdir}/pkgconfig/pygtk-2.0.pc
%{_libdir}/python2.7/site-packages/gtk-2.0/*
%{_libdir}/pygtk/2.0/*
%{_datadir}/pygtk/*

%files doc
%defattr(-,root,root,-)
%{_datadir}/gtk-doc/*

%clean
rm -rf %{buildroot}

%changelog
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
