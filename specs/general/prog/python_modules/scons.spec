Name:       scons
Version:    2.3.1
Release:    1%{?dist}
Summary:    SCons

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://scons.sourceforge.net
Source:     http://downloads.sourceforge.net/scons/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  python

%description
SCons is a tool for building software (and other files) implemented in Python.

%prep
%setup -q -n %{name}-%{version}

%build

%check

%install
rm -rf %{buildroot}
python setup.py install --prefix=/usr \
                        --standard-lib \
                        --optimize=1 \
                        --install-data=/usr/share \
                        --root=%{buildroot}

%files
%defattr(-,root,root,-)
%{_bindir}/scons
%{_bindir}/scons-%{version}
%{_bindir}/scons-time
%{_bindir}/scons-time-%{version}
%{_bindir}/sconsign
%{_bindir}/sconsign-%{version}
/usr/lib/python2.7/site-packages/SCons/*
/usr/lib/python2.7/site-packages/scons-%{version}-py2.7.egg-info
%{_mandir}/man1/scons*.1.gz

%clean
rm -rf %{buildroot}

%changelog
* Thu Mar 20 2014 tanggeliang <tanggeliang@gmail.com>
- update from 2.3.0 to 2.3.1
* Thu Aug 22 2013 tanggeliang <tanggeliang@gmail.com>
- create
