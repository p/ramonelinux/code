Name:       pygobject
Version:    3.18.0
Release:    1%{?dist}
Summary:    pygobject

Group:      System Environment/xlib
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/%{name}/3.18/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  gobject-introspection py2cairo
BuildRequires:  libxslt gnome-common

%description

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_includedir}/pygobject-3.0/pygobject.h
%{_libdir}/python2.7/site-packages/pygobject-3.18.0-py2.7-linux-x86_64.egg-info
%{_libdir}/python2.7/site-packages/pygtkcompat/*.py*
%{_libdir}/pkgconfig/pygobject-3.0.pc
%{_libdir}/python2.7/site-packages/gi/*

%clean
rm -rf %{buildroot}

%changelog
* Wed Sep 23 2015 tanggeliang <tanggeliang@gmail.com>
- update from 3.4.0 to 3.18.0
* Fri Sep 28 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.2.2 to 3.4.0
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
