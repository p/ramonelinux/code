Name:           pyparsing
Version:        1.5.6
Release:        6%{?dist}
Summary:        An object-oriented approach to text processing
Group:          Development/Libraries
License:        MIT
URL:            http://pyparsing.wikispaces.com/
Source:         http://downloads.sourceforge.net/pyparsing/pyparsing-%{version}.tar.gz

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  dos2unix

%description
pyparsing is a module that can be used to easily and directly configure syntax
definitions for any number of text parsing applications.

%prep
%setup -q
mv docs/pyparsingClassDiagram.PNG docs/pyparsingClassDiagram.png
rm docs/pyparsingClassDiagram.JPG
dos2unix -k CHANGES LICENSE
dos2unix -k docs/examples/*
dos2unix -k docs/htmldoc/epydoc*
for f in CHANGES docs/examples/{holaMundo.py,mozillaCalendarParser.py} ; do
    mv $f $f.iso88591
    iconv -f ISO-8859-1 -t UTF-8 -o $f $f.iso88591
    touch -r $f.iso88591 $f
    rm -f $f.iso88591
done

%build
%{__python} setup.py build

%install
rm -rf %{buildroot}
%{__python} setup.py install -O1 --skip-build --root %{buildroot}

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc
%{_libdir}/python2.7/site-packages/pyparsing-1.5.6-py2.7.egg-info
%{_libdir}/python2.7/site-packages/pyparsing.py*

%changelog
