Name:       pyxdg
Version:    0.25
Release:    1%{?dist}
Summary:    PyXDG Python library to access freedesktop.org standards

Group:      Development/Libraries
License:    LGPLv2
Url:        http://www.freedesktop.org
Source:     http://people.freedesktop.org/~takluyver/%{name}-%{version}.tar.gz

BuildRoot:  %{_tmppath}/%{name}-%{version}-%{release}-build

%description
PyXDG is a Python library to access freedesktop.org standards.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q

%build
python setup.py build

%install
rm -rf %{buildroot}
python setup.py install --skip-build --root=%{buildroot}

%clean
rm -rf %{buildroot} 

%files
%defattr(-,root,root)
%{python_sitelib}/xdg
%{python_sitelib}/pyxdg-*.egg-info

%files doc
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog README TODO

%changelog
* Thu Aug 15 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.22 to 0.25
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
