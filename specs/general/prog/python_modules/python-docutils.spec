# Just a reminder to remove this when these conditions can no longer occur.
%if 0%{?rhel} && 0%{?rhel} <= 5
%{!?python_sitelib: %global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}
%endif

%if 0%{?fedora}
%global with_python3 1
%endif

%global srcname docutils

Name:           python-%{srcname}
Version:        0.10
Release:        5%{?dist}
Summary:        System for processing plaintext documentation

Group:          Development/Languages
# See COPYING.txt for information
License:        Public Domain and BSD and Python and GPLv3+
URL:            http://docutils.sourceforge.net
#Source0:        http://downloads.sourceforge.net/docutils/%{srcname}-%{version}.tar.gz
# Sometimes we need snapshots.  Instructions below:
# svn co -r 7502 https://docutils.svn.sourceforge.net/svnroot/docutils/trunk/docutils
# cd docutils
# python setup.py sdist
# The tarball is in dist/docutils-VERSION.tar.gz
Source0:        %{srcname}-%{version}.tar.gz
# Submitted upstream: https://sourceforge.net/tracker/index.php?func=detail&aid=3560841&group_id=38414&atid=422030
Patch0: docutils-__import__-tests.patch
Patch1: docutils-__import__-fixes2.patch

# Disable some tests known to fail with Python 3.3
# Bug reports filed upstream as:
#   https://sourceforge.net/tracker/?func=detail&aid=3555164&group_id=38414&atid=422030
# and:
#   http://sourceforge.net/tracker/?func=detail&aid=3561133&group_id=38414&atid=422030
# Unicode test is failing because of a python3.3b2 bug:
# ImportError(b'str').__str__() returns bytes rather than str
# http://bugs.python.org/issue15778
Patch100: disable-failing-tests.patch


BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  python
BuildRequires:  python-setuptools

Requires: python-imaging
Provides: docutils = %{version}-%{release}

%description
The Docutils project specifies a plaintext markup language, reStructuredText,
which is easy to read and quick to write.  The project includes a python
library to parse rST files and transform them into other useful formats such
as HTML, XML, and TeX as well as commandline tools that give the enduser
access to this functionality.

Currently, the library supports parsing rST that is in standalone files and
PEPs (Python Enhancement Proposals).  Work is underway to parse rST from
Python inline documentation modules and packages.

%prep
%setup -q -n %{srcname}-%{version}
%patch0 -p0
%patch1 -p0
%patch100 -p1 -b .disable-failing-tests

# Remove shebang from library files
for file in docutils/utils/{code_analyzer.py,punctuation_chars.py,error_reporting.py} docutils/utils/math/{latex2mathml.py,math2html.py} docutils/writers/xetex/__init__.py; do
sed -i -e '/#! *\/usr\/bin\/.*/{1D}' $file
done

iconv -f ISO88592 -t UTF8 tools/editors/emacs/IDEAS.rst > tmp
mv tmp tools/editors/emacs/IDEAS.rst

%build
CFLAGS="$RPM_OPT_FLAGS" %{__python} setup.py build

%install
rm -rf %{buildroot}

%{__python} setup.py install --skip-build --root %{buildroot}

for file in %{buildroot}/%{_bindir}/*.py; do
    mv $file `dirname $file`/`basename $file .py`
done

# We want the licenses but don't need this build file
rm -f licenses/docutils.conf

%check

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc BUGS.txt COPYING.txt FAQ.txt HISTORY.txt README.txt RELEASE-NOTES.txt 
%doc THANKS.txt licenses docs tools/editors
%{_bindir}/*
%{python_sitelib}/*

%changelog
