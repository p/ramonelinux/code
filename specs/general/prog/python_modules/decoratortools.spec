Name:           decoratortools
Version:        1.8
Release:        1%{?dist}
Summary:        Use class and function decorators -- even in Python 2.3

Group:          Development/Languages
License:        Python or ZPLv2.1
Url:            http://pypi.python.org/pypi/DecoratorTools
Source:         http://pypi.python.org/packages/source/D/DecoratorTools/DecoratorTools-%{version}.zip

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  setuptools nose

%description
Want to use decorators, but still need to support Python 2.3? Wish you could
have class decorators, decorate arbitrary assignments, or match decorated
function signatures to their original functions? Then you need "DecoratorTools"

%prep
%setup -q -n DecoratorTools-%{version}

%build
%{__python} setup.py build

%install
rm -rf %{buildroot}
%{__python} setup.py install --skip-build --root %{buildroot}

%check
PYTHONPATH=$(pwd) nosetests -q

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%{_libdir}/python2.7/site-packages/DecoratorTools-1.8-py2.7-nspkg.pth
%{_libdir}/python2.7/site-packages/DecoratorTools-1.8-py2.7.egg-info/*
%{_libdir}/python2.7/site-packages/peak/util/decorators.py
%{_libdir}/python2.7/site-packages/peak/util/decorators.pyc

%changelog
* Sun Jul 27 2014 tanggeliang <tanggeliang@gmail.com>
- change name from python-decoratortools to decoratortools.
