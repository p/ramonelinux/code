
%if 0%{?fedora} > 12
%global with_python3 1
%endif

Name: 	 pyqt
Version: 4.9.4
Release: 5%{?dist}
Summary: Python bindings for Qt4

# GPLv2 exceptions(see GPL_EXCEPTIONS*.txt)
License: GPLv3 or GPLv2 with exceptions
Group: 	 Development/Languages
Url:     http://www.riverbankcomputing.com/software/pyqt/
Source0: http://www.riverbankcomputing.com/static/Downloads/PyQt4/PyQt-x11-gpl%{?snap:-snapshot}-%{version}%{?snap:-%{snap}}.tar.gz
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

## upstreamable patches
# fix multilib conflict because of timestamp
Patch50:  PyQt-x11-gpl-4.6.2-timestamp-multilib.patch
# multilib-safe(r)  opengl_types.sip (hopefully)
# simpler approach may be to not try to change these at buildime (only verify),
# if /usr/include/GL/gl.h ever changes these types, we have bigger problems
Patch51:  PyQt-x11-gpl-4.9.1-opengl_types.patch
Patch52:  PyQt-x11-gpl-4.9.2-pyuic_shbang.patch

## upstream patches
# fix FTBFS on ARM
Patch60:  qreal_float_support.diff

# rhel patches
Patch300: PyQt-x11-gpl-4.9-webkit.patch

BuildRequires: findutils
BuildRequires: dbus dbus-python
BuildRequires: phonon
BuildRequires: python
BuildRequires: qt
BuildRequires: sip
Requires: dbus-python

%description
These are Python bindings for Qt4.

%prep
%setup -q -n PyQt-x11-gpl%{?snap:-snapshot}-%{version}%{?snap:-%{snap}} 

%patch50 -p1 -b .timestamp
%patch51 -p1 -b .opengl_types
%patch52 -p1 -b .pyuic_shbang
# save orig for comparison later
cp -a ./sip/QtOpenGL/opengl_types.sip ./sip/QtOpenGL/opengl_types.sip.orig
%patch60 -p1 -b .arm
%if 0%{?rhel}
%patch300 -p1 -b .webkit
%endif

# permissions, mark examples non-executable
find examples/ -name "*.py" | xargs chmod a-x
chmod a+rx pyuic/uic/pyuic.py

%build

QT4DIR=%{_qt4_prefix}
PATH=%{_qt4_bindir}:$PATH ; export PATH

# Python 2 build:
%{__python} configure.py \
  --assume-shared \
  --confirm-license \
  --no-timestamp \
  --qmake=/usr/bin/qmake \
  --verbose 

make %{?_smp_mflags}

%install
rm -rf %{buildroot}

InstallPyQt4() {
  PySiteArch=$1

  make install DESTDIR=%{buildroot} INSTALL_ROOT=%{buildroot}

  # fix/remove rpaths
  chrpath --list   %{buildroot}$PySiteArch/PyQt4/QtCore.so && \
  chrpath --delete %{buildroot}$PySiteArch/PyQt4/QtCore.so ||:

  chrpath --list   %{buildroot}$PySiteArch/PyQt4/QtGui.so && \
  chrpath --delete %{buildroot}$PySiteArch/PyQt4/QtGui.so ||:

  chrpath --list   %{buildroot}$PySiteArch/PyQt4/QtDeclarative.so && \
  chrpath --delete %{buildroot}$PySiteArch/PyQt4/QtDeclarative.so ||:

  chrpath --list   %{buildroot}$PySiteArch/PyQt4/QtDesigner.so && \
  chrpath --delete %{buildroot}$PySiteArch/PyQt4/QtDesigner.so ||:

  chrpath --list   %{buildroot}$PySiteArch/PyQt4/QtOpenGL.so && \
  chrpath --delete %{buildroot}$PySiteArch/PyQt4/QtOpenGL.so ||:
}

InstallPyQt4 %{python_sitearch}

# DBus bindings only work for Python 2 so far:
chrpath --list   %{buildroot}%{python_sitelib}/dbus/mainloop/qt.so && \
chrpath --delete %{buildroot}%{python_sitelib}/dbus/mainloop/qt.so ||:

# HACK: fix multilb conflict, http://bugzilla.redhat.com/509415
rm -fv %{buildroot}%{_bindir}/pyuic4
mv %{buildroot}%{python_sitearch}/PyQt4/uic/pyuic.py \
   %{buildroot}%{_bindir}/pyuic4
ln -s %{_bindir}/pyuic4 \
      %{buildroot}%{python_sitearch}/PyQt4/uic/pyuic.py

# remove Python 3 code from Python 2.6 directory, fixes FTBFS (#564633)
rm -rf %{buildroot}%{python_sitearch}/PyQt4/uic/port_v3/

# likewise, remove Python 2 code from the Python 3.1 directory:
rm -rf %{buildroot}%{python3_sitearch}/PyQt4/uic/port_v2/

%check

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc
%{python_sitearch}/PyQt4/
%exclude %{python_sitearch}/PyQt4/uic/pyuic.py*
%{python_sitelib}/dbus/mainloop/qt.so
%{_bindir}/pylupdate4
%{_bindir}/pyrcc4
%{_bindir}/pyuic4
%{python_sitearch}/PyQt4/uic/pyuic.py*
%{_libdir}/qt4/plugins/designer/libpythonplugin.so
%{_datadir}/sip/PyQt4/

%changelog
