%if ! (0%{?fedora} > 12 || 0%{?rhel} > 5)
%{!?python_sitelib: %global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib())")}
%{!?python_sitearch: %global python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib(1))")}
%endif


Summary:    Python bindings for lzma
Name:       pyliblzma
Version:    0.5.3
Release:    8%{?dist}
License:    LGPLv3+
Url:        https://launchpad.net/pyliblzma
Source0:    http://pypi.python.org/packages/source/p/pyliblzma/%{name}-%{version}.tar.bz2
Patch0:     no-script-liblzma.patch

BuildRequires:    xz python-setuptools python
BuildRoot:    %{_tmppath}/%{name}-%{version}-%{release}-buildroot

%description
PylibLZMA provides a python interface for the liblzma library
to read and write data that has been compressed or can be decompressed
by Lasse Collin's lzma utils.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -qn %{name}-%{version}

%patch0 -p1 

%build
%{__python} setup.py build

%check

%install
rm -rf %{buildroot}
%{__python} setup.py install --root=%{buildroot}

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%attr(0755,-,-) %{python_sitearch}/lzma.so
%{python_sitearch}/liblzma.py*
%{python_sitearch}/%{name}*.egg-info

%files doc
%defattr(-,root,root)
%doc README THANKS ChangeLog NEWS

%changelog
