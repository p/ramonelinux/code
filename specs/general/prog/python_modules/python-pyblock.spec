%global realname pyblock
%{!?python_sitearch: %global python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")}
%global dmrver 1.0.0.rc15
%global dmver 1.02.54-3

Summary: Python modules for dealing with block devices
Name: python-%{realname}
Version: 0.53
Release: 2%{?dist}
# We are upstream, to generate Source0 do:
# git clone http://git.fedorahosted.org/git/pyblock.git
# git checkout -b archive-branch pyblock-%{version}-%{release}
# make archive-no-tag
Source0: %{realname}-%{version}.tar.bz2
License: GPLv2 or GPLv3
Group: System Environment/Libraries
URL: http://fedoraproject.org/wiki/Anaconda
BuildRoot: %{_tmppath}/%{name}-%{version}-root
BuildRequires: python gettext lvm
#BuildRequires: device-mapper-libs >= %{dmver}, device-mapper-devel >= %{dmver}
BuildRequires: dmraid >= %{dmrver}, libsepol libselinux
#Requires: device-mapper-libs >= %{dmver}, pyparted
Requires: pyparted lvm

%description
The pyblock contains Python modules for dealing with block devices.

%prep
%setup -q -n %{realname}-%{version}

%build
make %{?_smp_mflags} OPTFLAGS="$RPM_OPT_FLAGS"

%install
rm -rf $RPM_BUILD_ROOT
make DESTDIR=${RPM_BUILD_ROOT} SITELIB=%{python_sitearch} install

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc %{_docdir}/pyblock-%{version}
%{python_sitearch}/block

%changelog
