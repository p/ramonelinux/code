%{!?python_sitelib: %global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}

Name:       pykickstart
Version:    1.99.7
Release:    5%{?dist}
Summary:    A python library for manipulating kickstart files

Group:      System Environment/Libraries
License:    GPLv2
Url:        http://fedoraproject.org/wiki/pykickstart
Source:     %{name}-%{version}.tar.gz

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  python gettext setuptools
Requires:       python urlgrabber

%description
The pykickstart package is a python library for manipulating kickstart files.

%prep
%setup -q

%build
make

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install
%find_lang %{name}

%clean
rm -rf %{buildroot}

%files -f %{name}.lang
%defattr(-,root,root,-)
%{_bindir}/ksvalidator
%{_bindir}/ksflatten
%{_bindir}/ksverdiff
%{_libdir}/python2.7/site-packages/pykickstart-1.99.7-py2.7.egg-info
%{_libdir}/python2.7/site-packages/pykickstart/*.py*
%{_libdir}/python2.7/site-packages/pykickstart/commands/*.py*
%{_libdir}/python2.7/site-packages/pykickstart/handlers/*.py*

%changelog
