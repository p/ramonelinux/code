Name:       coverage
Version:    3.7
Release:    1%{?dist}
Summary:    Code coverage testing module for Python

Group:      System Environment/Libraries
License:    BSD and (MIT or GPLv2)
Url:        http://nedbatchelder.com/code/modules/coverage.html
Source0:    http://pypi.python.org/packages/source/c/coverage/%{name}-%{version}.tar.gz
BuildRequires:  setuptools
Requires:       setuptools

%description
Coverage.py is a Python module that measures code coverage during Python execution.
It uses the code analysis tools and tracing hooks provided in the Python standard library to determine which lines are executable, and which have been executed.

%prep
%setup -q -n %{name}-%{version}

find . -type f -exec chmod 0644 \{\} \;
sed -i 's/\r//g' README.txt

%build
%{__python} setup.py build

%install
%{__python} setup.py install -O1 --skip-build --root %{buildroot}

%files
%doc README.txt
%{_bindir}/coverage
%{_bindir}/coverage-2.7
%{_bindir}/coverage2
%{python_sitearch}/coverage/
%{python_sitearch}/coverage*.egg-info/

%changelog
* Sun Jul 27 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.5.1 to 3.7
- change name from python-coverage to coverage.
