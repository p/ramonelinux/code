Name:           kitchen
Version:        1.1.1
Release:        1%{?dist}
Summary:        Small, useful pieces of code to make python coding easier

Group:          Development/Languages
License:        LGPLv2+
Url:            https://pypi.python.org/pypi/kitchen/
Source:         https://fedorahosted.org/releases/k/i/kitchen/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  python-nose

%description
kitchen includes functions to make gettext easier to use, handling unicode text easier (conversion with bytes, outputting xml, and calculating how many columns a string takes), and compatibility modules for writing code that uses python-2.7 modules but needs to run on python-2.3.

%prep
%setup -q -n kitchen-%{version}

%build
%{__python} setup.py build

%install
rm -rf %{buildroot}
%{__python} setup.py install --skip-build --root %{buildroot}

%check
 
%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%{_libdir}/python2.7/site-packages/kitchen-1.1.1-py2.7.egg-info/*
%{_libdir}/python2.7/site-packages/kitchen/*

%changelog
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
