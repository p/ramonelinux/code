Name:       py2cairo
Version:    1.10.0
Release:    5%{?dist}
Summary:    py2cairo

Group:      System Environment/xlib
License:    GPLv2+
Url:        http://cairographics.org
Source:     http://cairographics.org/releases/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  python, cairo

%description

%prep
%setup -q -n %{name}-%{version}

%build
./waf configure --prefix=/usr \
                --libdir=%{_libdir} &&
./waf build %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
./waf install --destdir=%{buildroot}

%files
%defattr(-,root,root,-)
%{_includedir}/pycairo/pycairo.h
%{_libdir}/pkgconfig/pycairo.pc
%{_libdir}/python2.7/site-packages/cairo/__init__.py*
%{_libdir}/python2.7/site-packages/cairo/_cairo.so

%clean
rm -rf %{buildroot}

%changelog
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
