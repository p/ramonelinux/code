Name:       pycurl
Version:    7.19.0
Release:    9%{?dist}
Summary:    pycurl

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://pycurl.sourceforge.net
Source:     http://pycurl.sourceforge.net/%{name}-%{version}.tar.gz
Patch0:     python-pycurl-no-static-libs.patch
Patch1:     python-pycurl-fix-do_curl_reset-refcount.patch

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  curl openssl

%description
PycURL is a Python interface to libcurl.
PycURL can be used to fetch objects identified by a URL from a Python program, similar to the urllib Python module.
PycURL is mature, very fast, and supports a lot of features.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}
%patch0 -p0
%patch1 -p1
chmod a-x examples/*

%build
CFLAGS="$RPM_OPT_FLAGS -DHAVE_CURL_OPENSSL" %{__python} setup.py build

%check

%install
rm -rf %{buildroot}
%{__python} setup.py install -O1 --skip-build --root %{buildroot}
rm -rf %{buildroot}%{_datadir}/doc/pycurl

%files
%defattr(-,root,root,-)
%{python_sitearch}/*

%files doc
%defattr(-,root,root,-)
%doc COPYING ChangeLog README TODO examples doc tests

%clean
rm -rf %{buildroot}

%changelog
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
