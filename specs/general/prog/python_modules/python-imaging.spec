%{!?python_sitearch: %define python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")}
%define pyver %(%{__python} -c "import sys ; print sys.version[:3]")
%define py_incdir %{_includedir}/python%{pyver}

Name:       python-imaging
Version:    1.1.7
Release:    7%{?dist}
Summary:    Python's own image processing library

Group:      System Environment/Libraries
License:    MIT
URL:        http://www.pythonware.com/products/pil/
Source:     http://effbot.org/downloads/Imaging-%{version}.tar.gz
Patch1:     %{name}-lib64.patch
Patch2:     %{name}-giftrans.patch
Patch3:     %{name}-1.1.6-sane-types.patch
Patch4:     %{name}-shebang.patch
# buffer overflow patch, bz 703212
Patch5:     %{name}-buffer.patch

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  python, libjpeg, zlib, freetype
BuildRequires:  tk, lcms

%description
Python Imaging Library

The Python Imaging Library (PIL) adds image processing capabilities
to your Python interpreter.

This library provides extensive file format support, an efficient
internal representation, and powerful image processing capabilities.

Notice that in order to reduce the package dependencies there are
three subpackages: devel (for development); tk (to interact with the
tk interface) and sane (scanning devices interface).

%prep
%setup -q -n Imaging-%{version}
%patch1 -p1
%patch2 -p1
%patch3 -p1 -b .sane-types
%patch4 -p1 -b .shebang
%patch5 -p1 -b .buffer

# fix the interpreter path for Scripts/*.py
cd Scripts
for scr in *.py
do
  sed -e "s|/usr/local/bin/python|%{_bindir}/python|"  $scr > tmp.py
  mv tmp.py $scr
  chmod 755 $scr
done

%build
CFLAGS="$RPM_OPT_FLAGS" %{__python} setup.py build

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}/%{py_incdir}/Imaging
install -m 644 libImaging/*.h %{buildroot}/%{py_incdir}/Imaging
%{__python} setup.py install -O1 --skip-build --root %{buildroot}

# There is no need to ship the binaries since they are already packaged
# in %doc
rm -rf %{buildroot}%{_bindir}

# Separate files that need Tk and files that don't
echo '%%defattr (0644,root,root,755)' > files.main
echo '%%defattr (0644,root,root,755)' > files.tk
p="$PWD"

pushd %{buildroot}%{python_sitearch}/PIL
for file in *; do
    case "$file" in
    ImageTk*|SpiderImagePlugin*|_imagingtk.so)
        what=files.tk
        ;;
    *)
        what=files.main
        ;;
    esac
    echo %{python_sitearch}/PIL/$file >> "$p/$what"
done
popd

%check

%clean
rm -rf %{buildroot}

%files
%defattr (-,root,root,-)
%{_includedir}/python2.7/Imaging/*.h
%{_libdir}/python2.7/site-packages/PIL.pth
%{_libdir}/python2.7/site-packages/PIL/*

%changelog
