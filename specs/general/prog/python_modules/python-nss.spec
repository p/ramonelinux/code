%{!?python_sitelib: %global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}
%{!?python_sitearch: %global python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")}

%global build_api_doc 1

Name:       python-nss
Version:    0.12
Release:    4%{?dist}
Summary:    Python bindings for Network Security Services (NSS)

Group:      Development/Languages
License:    MPLv1.1 or GPLv2+ or LGPLv2+
URL:        ftp://www.mozilla.org
Source:     ftp://ftp.mozilla.org/pub/mozilla.org/security/python-nss/releases/PYNSS_RELEASE_0_12_0/src/python-nss-%{version}.tar.bz2
Patch:      python-nss-0.12-rsapssparams.patch

BuildRoot:  %{_tmppath}/%{name}-%{version}-%{release}-build

%global docdir %{_docdir}/%{name}-%{version}

# We don't want to provide private python extension libs
%{?filter_setup:
%filter_provides_in %{python_sitearch}/.*\.so$
%filter_setup
}

BuildRequires: python-setuptools
BuildRequires: python-docutils
BuildRequires: nspr
BuildRequires: nss
BuildRequires: epydoc

%description
This package provides Python bindings for Network Security Services
(NSS) and the Netscape Portable Runtime (NSPR).

NSS is a set of libraries supporting security-enabled client and
server applications. Applications built with NSS can support SSL v2
and v3, TLS, PKCS #5, PKCS #7, PKCS #11, PKCS #12, S/MIME, X.509 v3
certificates, and other security standards. Specific NSS
implementations have been FIPS-140 certified.

%prep
%setup -q
%patch -b .rsapssparams

%build
CFLAGS="$RPM_OPT_FLAGS -fno-strict-aliasing" %{__python} setup.py build
%if %build_api_doc
%{__python} setup.py build_doc
%endif

%install
rm -rf %{buildroot}
%{__python} setup.py install  -O1 --install-platlib %{python_sitearch} --skip-build --root %{buildroot}
%{__python} setup.py install_doc --docdir %{docdir} --skip-build --root %{buildroot}

# Remove execution permission from any example/test files in docdir
find %{buildroot}/%{docdir} -type f | xargs chmod a-x

# Set correct permissions on .so files
chmod 0755 %{buildroot}/%{python_sitearch}/nss/*.so

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%{python_sitearch}/*
%doc %{docdir}/ChangeLog
%doc %{docdir}/LICENSE.gpl
%doc %{docdir}/LICENSE.lgpl
%doc %{docdir}/LICENSE.mpl
%doc %{docdir}/README
%doc %{docdir}/examples
%doc %{docdir}/test
%if %build_api_doc
%doc %{docdir}/api
%endif

%changelog
