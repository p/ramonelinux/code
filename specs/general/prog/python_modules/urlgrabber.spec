Name:       urlgrabber
Version:    3.10.1
Release:    1%{?dist}
Summary:    A high-level cross-protocol url-grabber

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://urlgrabber.baseurl.org
Source:     http://urlgrabber.baseurl.org/download/%{name}-%{version}.tar.gz
Patch:      urlgrabber-HEAD.patch

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  pycurl

%description
A high-level cross-protocol url-grabber for python supporting HTTP, FTP and file locations.
Features include keepalive, byte ranges, throttling, authentication, proxies and more.

%prep
%setup -q -n %{name}-%{version}
%patch -p1

%build
python setup.py build

%check

%install
rm -rf %{buildroot}
python setup.py install -O1 --root=%{buildroot}

%files
%defattr(-,root,root,-)
%{_bindir}/urlgrabber
/usr/libexec/urlgrabber-ext-down
%{_libdir}/python2.7/site-packages/urlgrabber-%{version}-py2.7.egg-info
%{_libdir}/python2.7/site-packages/urlgrabber/*.py*
%{_docdir}/urlgrabber-%{version}/*

%clean
rm -rf %{buildroot}

%changelog
* Sun Jul 27 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.9.1 to 3.10.1
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
