Name:       guile
Version:    2.0.11
Release:    1%{?dist}
Summary:    Guile

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.gnu.org
Source:     http://ftp.gnu.org/pub/gnu/guile/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  gc libffi libunistring
BuildRequires:  libtool gettext

%description
The Guile package contains the Project GNU's extension language library.
Guile also contains a stand alone Scheme interpreter.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --disable-static \
            --docdir=/usr/share/doc/guile-%{version} \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot} &&

rm -rf %{buildroot}/%{_infodir}/dir

%files
%defattr(-,root,root,-)
%{_bindir}/guild
%{_bindir}/guile
%{_bindir}/guile-config
%{_bindir}/guile-snarf
%{_bindir}/guile-tools
%{_includedir}/guile/2.0/*.h
%{_includedir}/guile/2.0/libguile/*.h
%{_libdir}/guile/2.0/ccache/*
%{_libdir}/libguile-2.0.la
%{_libdir}/libguile-2.0.so*
%{_libdir}/libguilereadline-v-18.la
%{_libdir}/libguilereadline-v-18.so*
%{_libdir}/pkgconfig/guile-2.0.pc
%{_datadir}/aclocal/guile.m4
%{_datadir}/guile/2.0/*

%files doc
%defattr(-,root,root,-)
%{_infodir}/guile.info*.gz
%{_infodir}/r5rs.info.gz
%{_mandir}/man1/guile.1.gz

%clean
rm -rf %{buildroot}

%changelog
* Sun Mar 30 2014 tanggeliang <tanggeliang@gmail.com>
- update from 2.0.10 to 2.0.11
* Tue Mar 18 2014 tanggeliang <tanggeliang@gmail.com>
- update from 2.0.9 to 2.0.10
* Sat Jul 6 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.0.7 to 2.0.9
* Sun Mar 31 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.0.6 to 2.0.7
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
