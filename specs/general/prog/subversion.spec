Name:       subversion
Version:    1.8.9
Release:    1%{?dist}
Summary:    Subversion

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://subversion.apache.org
Source:     http://archive.apache.org/dist/subversion/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  sqlite apr-util serf
BuildRequires:  db cyrus-sasl openssl dbus-glib gnome-keyring gettext

%description
Subversion is a version control system that is designed to be a compelling replacement for CVS in the open source community.
It extends and enhances CVS' feature set, while maintaining a similar interface for those already familiar with CVS.
These instructions install the client and server software used to manipulate a Subversion repository.
Creation of a repository is covered at Running a Subversion Server.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr    \
            --disable-static \
            --with-apache-libexecdir \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install &&
install -v -m755 -d %{buildroot}/usr/share/doc/subversion-%{version} &&
cp      -v -R       doc/* \
                    %{buildroot}/usr/share/doc/subversion-%{version}

%files
%defattr(-,root,root,-)
%{_bindir}/svn
%{_bindir}/svnadmin
%{_bindir}/svndumpfilter
%{_bindir}/svnlook
%{_bindir}/svnmucc
%{_bindir}/svnrdump
%{_bindir}/svnserve
%{_bindir}/svnsync
%{_bindir}/svnversion
%{_includedir}/subversion-1/svn-revision.txt
%{_includedir}/subversion-1/*svn*.h
%{_libdir}/libsvn_client-1.*
%{_libdir}/libsvn_delta-1.*
%{_libdir}/libsvn_diff-1.*
%{_libdir}/libsvn_fs-1.*
%{_libdir}/libsvn_fs_fs-1.*
%{_libdir}/libsvn_fs_util-1.*
%{_libdir}/libsvn_ra-1.*
%{_libdir}/libsvn_ra_local-1.*
%{_libdir}/libsvn_ra_serf-1.*
%{_libdir}/libsvn_ra_svn-1.*
%{_libdir}/libsvn_repos-1.*
%{_libdir}/libsvn_subr-1.*
%{_libdir}/libsvn_wc-1.*
%{_datadir}/locale/*/LC_MESSAGES/subversion.mo

%files doc
%defattr(-,root,root,-)
%{_docdir}/subversion-%{version}/*
%{_mandir}/man1/svn*.1.gz
%{_mandir}/man5/svnserve.conf.5.gz
%{_mandir}/man8/svnserve.8.gz

%clean
rm -rf %{buildroot}

%changelog
* Sat Jul 26 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.8.8 to 1.8.9
* Tue Mar 18 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.8.5 to 1.8.8
* Tue Dec 3 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.8.3 to 1.8.5
* Thu Oct 3 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.8.1 to 1.8.3
* Sat Jul 27 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.8.0 to 1.8.1
* Thu Jul 25 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.7.8 to 1.8.0
* Tue Apr 2 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.7.7 to 1.7.8
* Fri Nov 2 2012 tanggeliang <tanggeliang@gmail.com>
- update from 1.7.6 to 1.7.7
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
