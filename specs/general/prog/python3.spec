Name:       python3
Version:    3.3.3
Release:    1%{?dist}
Summary:    Python 3

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.python.org
Source:     http://www.python.org/ftp/python/%{version}/Python-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  expat libffi pkg-config
BuildRequires:  libxml sqlite

%description
The Python 3 package contains the Python development environment. This is useful for object-oriented programming, writing scripts, prototyping large programs or developing entire applications. 

%package        test
Summary:        The test modules from the main python package.
%description    test

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n Python-%{version}

%build
./configure --prefix=/usr       \
            --enable-shared     \
            --with-system-expat \
            --with-system-ffi   \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

chmod -v 755 %{buildroot}/%{_libdir}/libpython3.3m.so &&
chmod -v 755 %{buildroot}/%{_libdir}/libpython3.so
mv %{buildroot}%{_bindir}/2to3 %{buildroot}%{_bindir}/2to3-3

%files
%defattr(-,root,root,-)
%{_bindir}/2to3*
%{_bindir}/idle3*
%{_bindir}/pydoc3*
%{_bindir}/python3
%{_bindir}/python3-config
%{_bindir}/python3.3
%{_bindir}/python3.3-config
%{_bindir}/python3.3m
%{_bindir}/python3.3m-config
%{_bindir}/pyvenv*
%{_includedir}/python3.3m/*.h
/usr/lib/python3.3/LICENSE.txt
/usr/lib/python3.3/*.py
/usr/lib/python3.3/__pycache__/*
/usr/lib/python3.3/collections/*
/usr/lib/python3.3/concurrent/*
/usr/lib/python3.3/config-3.3m/*
/usr/lib/python3.3/ctypes/*
/usr/lib/python3.3/curses/*
/usr/lib/python3.3/dbm/*
/usr/lib/python3.3/distutils/*
/usr/lib/python3.3/email/*
/usr/lib/python3.3/encodings/*
/usr/lib/python3.3/html/*
/usr/lib/python3.3/http/*
/usr/lib/python3.3/idlelib/*
/usr/lib/python3.3/importlib/*
/usr/lib/python3.3/json/*
/usr/lib/python3.3/lib2to3/*
/usr/lib/python3.3/logging/*
/usr/lib/python3.3/multiprocessing/*
/usr/lib/python3.3/plat-linux/*
/usr/lib/python3.3/pydoc_data/*
/usr/lib/python3.3/site-packages/README
/usr/lib/python3.3/sqlite3/*
/usr/lib/python3.3/tkinter/*
/usr/lib/python3.3/turtledemo/*
/usr/lib/python3.3/urllib/*
/usr/lib/python3.3/venv/*
/usr/lib/python3.3/wsgiref/*
/usr/lib/python3.3/xml/*
/usr/lib/python3.3/xmlrpc/*
%{_libdir}/libpython3.3m.so*
%{_libdir}/libpython3.so
%{_libdir}/pkgconfig/python-3.3.pc
%{_libdir}/pkgconfig/python-3.3m.pc
%{_libdir}/pkgconfig/python3.pc
%{_libdir}/python3.3/lib-dynload/*.so

%files test
%defattr(-,root,root,-)
/usr/lib/python3.3/ctypes/test/*
/usr/lib/python3.3/distutils/tests/*
/usr/lib/python3.3/lib2to3/tests/*
/usr/lib/python3.3/sqlite3/test/*
/usr/lib/python3.3/test/*
/usr/lib/python3.3/tkinter/test/*
/usr/lib/python3.3/unittest/*

%files doc
%defattr(-,root,root,-)
%{_mandir}/man1/python3.1.gz
%{_mandir}/man1/python3.3.1.gz

%clean
rm -rf %{buildroot}

%changelog
* Thu Dec 5 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.3.1 to 3.3.3
* Fri May 10 2013 tanggeliang <tanggeliang@gmail.com>
- create
