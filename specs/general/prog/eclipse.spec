Name:       eclipse
Version:    4.3
Release:    1%{?dist}
Summary:    Eclipse Standard 4.3

Group:      System Environment/tools
License:    GPLv2+
Url:        http://www.eclipse.org
Source0:    http://www.eclipse.org/downloads/eclipse-standard-kepler-R-linux-gtk.tar.gz
Source1:    http://www.eclipse.org/downloads/eclipse-standard-kepler-R-linux-gtk-x86_64.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
Requires:       openjdk

%description
Eclipse is a community for individuals and organizations who wish to collaborate on commercially-friendly open source software.
Its projects are focused on building an open development platform comprised of extensible frameworks, tools and runtimes for building, deploying and managing software across the lifecycle.
The Eclipse Foundation is a not-for-profit, member supported corporation that hosts the Eclipse projects and helps cultivate both an open source community and an ecosystem of complementary products and services.

%prep
%setup -c -T
%ifarch %{ix86}
tar xvf %{SOURCE0}
%else %ifarch x86_64
tar xvf %{SOURCE1}
%endif

%build

%check

%install
rm -rf %{buildroot}

cd %{name}

mkdir -p %{buildroot}/%{_datadir}/eclipse
cp -a * %{buildroot}/%{_datadir}/eclipse

mkdir -p %{buildroot}/%{_bindir}
ln -s %{_datadir}/eclipse/eclipse %{buildroot}/%{_bindir}/eclipse

install -D plugins/org.eclipse.platform_4.3.0.v20130605-2000/eclipse32.png \
    %{buildroot}/usr/share/icons/hicolor/32x32/apps/eclipse.png
install -D plugins/org.eclipse.platform_4.3.0.v20130605-2000/eclipse48.png \
    %{buildroot}/usr/share/icons/hicolor/48x48/apps/eclipse.png
install -D plugins/org.eclipse.platform_4.3.0.v20130605-2000/eclipse256.png \
    %{buildroot}/usr/share/icons/hicolor/256x256/apps/eclipse.png

mkdir -p %{buildroot}/usr/share/pixmaps
ln -s /usr/share/icons/hicolor/256x256/apps/eclipse.png \
    %{buildroot}/usr/share/pixmaps/eclipse.png

mkdir -p %{buildroot}/%{_datadir}/applications
cat > %{buildroot}/%{_datadir}/applications/eclipse.desktop << "EOF"
[Desktop Entry]
Type=Application
Name=eclipse
Comment=Eclipse Integrated Development Environment
Icon=eclipse
Exec=eclipse
Terminal=false
Encoding=UTF-8
StartupNotify=true
Categories=Development;IDE;Java;
EOF

%files
%defattr(-,root,root,-)
%{_bindir}/eclipse
%{_datadir}/applications/eclipse.desktop
%{_datadir}/eclipse/about_files/*
%{_datadir}/eclipse/about.html
%{_datadir}/eclipse/artifacts.xml
%{_datadir}/eclipse/configuration/*
%dir %{_datadir}/eclipse/dropins
%{_datadir}/eclipse/eclipse
%{_datadir}/eclipse/eclipse.ini
%{_datadir}/eclipse/epl-v10.html
%{_datadir}/eclipse/features/*
%{_datadir}/eclipse/icon.xpm
%{_datadir}/eclipse/notice.html
%{_datadir}/eclipse/p2/*
%{_datadir}/eclipse/plugins/*
%{_datadir}/eclipse/readme/*
%{_datadir}/icons/hicolor/*x*/apps/eclipse.png
%{_datadir}/pixmaps/eclipse.png

%clean
rm -rf %{buildroot}

%changelog
* Tue Jul 23 2013 tanggeliang <tanggeliang@gmail.com>
- create
