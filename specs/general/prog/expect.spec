Name:       expect
Version:    5.45
Release:    9%{?dist}
Summary:    Expect

Group:      System Environment/Base
License:    GPLv2+
Url:        http://expect.sourceforge.net
Source:     http://prdownloads.sourceforge.net/expect/%{name}%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  tcl

%description
The Expect package contains a program for carrying out scripted dialogues with other interactive programs.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}%{version}

%build
./configure --prefix=/usr \
            --with-tcl=%{_libdir} \
            --with-tclinclude=/usr/include \
            --enable-shared \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
make install DESTDIR=%{buildroot} &&
ln -svf expect5.45/libexpect5.45.so %{buildroot}/%{_libdir}

%files
%defattr(-,root,root,-)
%{_bindir}/autoexpect
%{_bindir}/autopasswd
%{_bindir}/cryptdir
%{_bindir}/decryptdir
%{_bindir}/dislocate
%{_bindir}/expect
%{_bindir}/ftp-rfc
%{_bindir}/kibitz
%{_bindir}/lpunlock
%{_bindir}/mkpasswd
%{_bindir}/multixterm
%{_bindir}/passmass
%{_bindir}/rftp
%{_bindir}/rlogin-cwd
%{_bindir}/timed-read
%{_bindir}/timed-run
%{_bindir}/tknewsbiff
%{_bindir}/tkpasswd
%{_bindir}/unbuffer
%{_bindir}/weather
%{_bindir}/xkibitz
%{_bindir}/xpstat
%{_includedir}/expect*.h
%{_includedir}/tcldbg.h
%{_libdir}/libexpect5.45.so
%{_libdir}/expect5.45/libexpect5.45.so
%{_libdir}/expect5.45/pkgIndex.tcl

%files doc
%defattr(-,root,root,-)
/usr/man/man*/*.gz

%clean
rm -rf %{buildroot}

%changelog
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
