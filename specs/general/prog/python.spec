Name:       python
Version:    2.7.9
Release:    1%{?dist}
Summary:    Python 2

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.python.org
Source0:    http://www.python.org/ftp/%{name}/%{version}/Python-%{version}.tar.xz
Source1:    python-%{version}-docs-html.tar.bz2
Patch0:     Python-%{version}-skip_test_gdb-1.patch
Patch1:     python-%{version}-lib64-ram1.patch
Patch2:     python-2.7-lib64-sysconfig.patch

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  expat libffi pkg-config
BuildRequires:  libxml sqlite openssl gdbm tk
Provides:       /usr/local/bin/python

%description
The Python 2 package contains the Python development environment. This is useful for object-oriented programming, writing scripts, prototyping large programs or developing entire applications. This version is for backward compatibility with other dependent packages.

%package        test
Summary:        The test modules from the main python package.
%description    test

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n Python-%{version}
%patch0 -p1
%ifarch x86_64
%patch1 -p1
%patch2 -p1
%endif

%build
./configure --prefix=/usr       \
            --enable-shared     \
            --with-system-expat \
            --with-system-ffi   \
            --enable-unicode=ucs4 \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

chmod -v 755 %{buildroot}/%{_libdir}/libpython2.7.so.1.0

install -v -dm755 %{buildroot}/%{_docdir}/python-%{version} &&
tar --strip-components=1 -C %{buildroot}/%{_docdir}/python-%{version} \
    -xvf %SOURCE1                      &&
find %{buildroot}/%{_docdir}/python-%{version} -type d -exec chmod 0755 {} \; &&
find %{buildroot}/%{_docdir}/python-%{version} -type f -exec chmod 0644 {} \;

%files
%defattr(-,root,root,-)
%{_bindir}/2to3
%{_bindir}/idle
%{_bindir}/pydoc
%{_bindir}/python
%{_bindir}/python-config
%{_bindir}/python2
%{_bindir}/python2-config
%{_bindir}/python2.7
%{_bindir}/python2.7-config
%{_bindir}/smtpd.py
%{_includedir}/python2.7/*.h
%{_libdir}/libpython2.7.so*
%{_libdir}/pkgconfig/python-2.7.pc
%{_libdir}/pkgconfig/python.pc
%{_libdir}/pkgconfig/python2.pc
%{_libdir}/python2.7/config/*
%{_libdir}/python2.7/ensurepip/*.py*
%{_libdir}/python2.7/ensurepip/_bundled/pip-1.5.6-py2.py3-none-any.whl
%{_libdir}/python2.7/ensurepip/_bundled/setuptools-7.0-py2.py3-none-any.whl
%{_libdir}/python2.7/lib-dynload/Python-%{version}-py2.7.egg-info
%{_libdir}/python2.7/lib-dynload/_bisect.so
%{_libdir}/python2.7/lib-dynload/_codecs_cn.so
%{_libdir}/python2.7/lib-dynload/_codecs_hk.so
%{_libdir}/python2.7/lib-dynload/_codecs_iso2022.so
%{_libdir}/python2.7/lib-dynload/_codecs_jp.so
%{_libdir}/python2.7/lib-dynload/_codecs_kr.so
%{_libdir}/python2.7/lib-dynload/_codecs_tw.so
%{_libdir}/python2.7/lib-dynload/_collections.so
%{_libdir}/python2.7/lib-dynload/_csv.so
%{_libdir}/python2.7/lib-dynload/_ctypes.so
%{_libdir}/python2.7/lib-dynload/_ctypes_test.so
%{_libdir}/python2.7/lib-dynload/_curses.so
%{_libdir}/python2.7/lib-dynload/_curses_panel.so
%{_libdir}/python2.7/lib-dynload/_elementtree.so
%{_libdir}/python2.7/lib-dynload/_functools.so
%{_libdir}/python2.7/lib-dynload/_hashlib.so
%{_libdir}/python2.7/lib-dynload/_heapq.so
%{_libdir}/python2.7/lib-dynload/_hotshot.so
%{_libdir}/python2.7/lib-dynload/_io.so
%{_libdir}/python2.7/lib-dynload/_json.so
%{_libdir}/python2.7/lib-dynload/_locale.so
%{_libdir}/python2.7/lib-dynload/_lsprof.so
%{_libdir}/python2.7/lib-dynload/_multibytecodec.so
%{_libdir}/python2.7/lib-dynload/_multiprocessing.so
%{_libdir}/python2.7/lib-dynload/_random.so
%{_libdir}/python2.7/lib-dynload/_socket.so
%{_libdir}/python2.7/lib-dynload/_sqlite3.so
%{_libdir}/python2.7/lib-dynload/_ssl.so
%{_libdir}/python2.7/lib-dynload/_struct.so
%{_libdir}/python2.7/lib-dynload/_testcapi.so
%{_libdir}/python2.7/lib-dynload/_tkinter.so
%{_libdir}/python2.7/lib-dynload/array.so
%{_libdir}/python2.7/lib-dynload/audioop.so
%{_libdir}/python2.7/lib-dynload/binascii.so
%{_libdir}/python2.7/lib-dynload/bz2.so
%{_libdir}/python2.7/lib-dynload/cPickle.so
%{_libdir}/python2.7/lib-dynload/cStringIO.so
%{_libdir}/python2.7/lib-dynload/cmath.so
%{_libdir}/python2.7/lib-dynload/crypt.so
%{_libdir}/python2.7/lib-dynload/datetime.so
%{_libdir}/python2.7/lib-dynload/dbm.so
%ifarch %{ix86}
%{_libdir}/python2.7/lib-dynload/dl.so
%endif
%{_libdir}/python2.7/lib-dynload/fcntl.so
%{_libdir}/python2.7/lib-dynload/future_builtins.so
%{_libdir}/python2.7/lib-dynload/gdbm.so
%{_libdir}/python2.7/lib-dynload/grp.so
%ifarch %{ix86}
%{_libdir}/python2.7/lib-dynload/imageop.so
%endif
%{_libdir}/python2.7/lib-dynload/itertools.so
%{_libdir}/python2.7/lib-dynload/linuxaudiodev.so
%{_libdir}/python2.7/lib-dynload/math.so
%{_libdir}/python2.7/lib-dynload/mmap.so
%{_libdir}/python2.7/lib-dynload/nis.so
%{_libdir}/python2.7/lib-dynload/operator.so
%{_libdir}/python2.7/lib-dynload/ossaudiodev.so
%{_libdir}/python2.7/lib-dynload/parser.so
%{_libdir}/python2.7/lib-dynload/pyexpat.so
%{_libdir}/python2.7/lib-dynload/readline.so
%{_libdir}/python2.7/lib-dynload/resource.so
%{_libdir}/python2.7/lib-dynload/select.so
%{_libdir}/python2.7/lib-dynload/spwd.so
%{_libdir}/python2.7/lib-dynload/strop.so
%{_libdir}/python2.7/lib-dynload/syslog.so
%{_libdir}/python2.7/lib-dynload/termios.so
%{_libdir}/python2.7/lib-dynload/time.so
%{_libdir}/python2.7/lib-dynload/unicodedata.so
%{_libdir}/python2.7/lib-dynload/zlib.so
%{_libdir}/python2.7/LICENSE.txt
%{_libdir}/python2.7/pdb.doc
%{_libdir}/python2.7/wsgiref.egg-info
%{_libdir}/python2.7/*.py*
%{_libdir}/python2.7/bsddb/*.py*
%{_libdir}/python2.7/compiler/*.py*
%{_libdir}/python2.7/ctypes/*.py*
%{_libdir}/python2.7/ctypes/macholib/*
%{_libdir}/python2.7/curses/*.py*
%{_libdir}/python2.7/distutils/README
%{_libdir}/python2.7/distutils/*.py*
%{_libdir}/python2.7/distutils/command/*.py*
%{_libdir}/python2.7/distutils/command/command_template
%{_libdir}/python2.7/distutils/command/wininst-*.exe
%{_libdir}/python2.7/email/*.py*
%{_libdir}/python2.7/email/mime/*.py*
%{_libdir}/python2.7/encodings/*.py*
%{_libdir}/python2.7/hotshot/*.py*
%{_libdir}/python2.7/idlelib/*
%{_libdir}/python2.7/importlib/__init__.py*
%{_libdir}/python2.7/json/*.py*
%{_libdir}/python2.7/lib-tk/*.py*
%{_libdir}/python2.7/lib2to3/Grammar.txt
%{_libdir}/python2.7/lib2to3/Grammar%{version}.final.0.pickle
%{_libdir}/python2.7/lib2to3/PatternGrammar.txt
%{_libdir}/python2.7/lib2to3/PatternGrammar%{version}.final.0.pickle
%{_libdir}/python2.7/lib2to3/*.py*
%{_libdir}/python2.7/lib2to3/fixes/*.py*
%{_libdir}/python2.7/lib2to3/pgen2/*.py*
%{_libdir}/python2.7/logging/*.py*
%{_libdir}/python2.7/multiprocessing/*.py*
%{_libdir}/python2.7/multiprocessing/dummy/*.py*
%{_libdir}/python2.7/plat-linux2/*.py*
%{_libdir}/python2.7/plat-linux2/regen
%{_libdir}/python2.7/pydoc_data/*.py*
%{_libdir}/python2.7/site-packages/README
%{_libdir}/python2.7/sqlite3/*.py*
%{_libdir}/python2.7/unittest/*.py*
%{_libdir}/python2.7/wsgiref/*.py*
%{_libdir}/python2.7/xml/*.py*
%{_libdir}/python2.7/xml/dom/*.py*
%{_libdir}/python2.7/xml/etree/*.py*
%{_libdir}/python2.7/xml/parsers/*.py*
%{_libdir}/python2.7/xml/sax/*.py*

%files test
%defattr(-,root,root,-)
%{_libdir}/python2.7/bsddb/test/*
%{_libdir}/python2.7/ctypes/test/*
%{_libdir}/python2.7/distutils/tests/*
%{_libdir}/python2.7/email/test/*
%{_libdir}/python2.7/json/tests/*
%{_libdir}/python2.7/lib2to3/tests/*
%{_libdir}/python2.7/lib-tk/test/*
%{_libdir}/python2.7/sqlite3/test/*
%{_libdir}/python2.7/test/*
%{_libdir}/python2.7/unittest/test/*

%files doc
%defattr(-,root,root,-)
%{_docdir}/python-%{version}/*
%{_docdir}/python-%{version}/.buildinfo
%{_mandir}/man1/python*.1.gz

%clean
rm -rf %{buildroot}

%changelog
* Tue Mar 31 2015 tanggeliang <tanggeliang@gmail.com>
- update from 2.7.8 to 2.7.9
* Sun Jul 27 2014 tanggeliang <tanggeliang@gmail.com>
- update from 2.7.6 to 2.7.8
* Sun Apr 20 2014 tanggeliang <tanggeliang@gmail.com>
- update from 2.7.5 to 2.7.6
* Sun Jul 7 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.7.3 to 2.7.5
* Wed Mar 27 2013 tanggeliang <tanggeliang@gmail.com>
- add x86_64
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
