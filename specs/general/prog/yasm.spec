Name:       yasm
Version:    1.2.0
Release:    5%{?dist}
Summary:    yasm

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.tortall.net/projects/yasm
Source:     http://www.tortall.net/projects/yasm/releases/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build

%description
Yasm is a complete rewrite of the NASM-2.10.01 assembler. It supports the x86 and AMD64 instruction sets, accepts NASM and GAS assembler syntaxes and outputs binary, ELF32 and ELF64 object formats.

%prep
%setup -q -n %{name}-%{version}

%build
sed -i 's#) ytasm.*#)#' Makefile.in &&
./configure --prefix=/usr \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/yasm
%{_includedir}/libyasm*.h
%{_includedir}/libyasm/*.h
%{_libdir}/libyasm.a
%{_mandir}/man1/*
%{_mandir}/man7/*

%clean
rm -rf %{buildroot}

%changelog
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
