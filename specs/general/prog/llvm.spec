Name:       llvm
Version:    3.4.2
Release:    2%{?dist}
Summary:    LLVM

Group:      System Environment/core
License:    GPLv2+
Url:        http://www.llvm.org
Source0:    http://llvm.org/releases/%{version}/%{name}-%{version}.src.tar.gz
Source1:    cfe-%{version}.src.tar.gz
Source2:    compiler-rt-3.4.src.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  libffi libxml zip groff tcl

%description
The llvm package contains a collection of modular and reusable compiler and toolchain technologies.
The Low Level Virtual MAchine (LLVM) Core libraries provide a modern source- and target-independent optimizer, along with code generation support for many popular CPUs (as well as some less common ones!) These libraries are built around a well specified code representation known as the LLVM intermediate representation ("LLVM IR").

The optional clang package provides a new C, C++, Objective C and Objective C++ front-end for the LLVM compiler. 

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}.src
%ifarch x86_64
sed -i 's:/lib:/lib64:' Makefile.config.in
sed -i '253s:/lib:/lib64:' tools/llvm-config/llvm-config.cpp
%endif

%build
tar -xf %SOURCE1 -C tools &&
tar -xf %SOURCE2 -C projects &&

mv tools/cfe-%{version}.src tools/clang
mv projects/compiler-rt-3.4 projects/compiler-rt &&

sed -e 's:\$(PROJ_prefix)/docs/llvm:$(PROJ_prefix)/share/doc/llvm-%{version}:' \
    -i Makefile.config.in &&
CC=gcc CXX=g++                         \
./configure --prefix=/usr              \
            --sysconfdir=/etc          \
            --enable-libffi            \
            --enable-optimized         \
            --enable-shared            \
            --disable-assertions       \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
make DESTDIR=%{buildroot} install &&
for file in %{buildroot}/%{_libdir}/lib{clang,LLVM,LTO}*.a
do
  test -f $file && chmod -v 644 $file
done

install -v -dm755 %{buildroot}/%{_libdir}/clang-analyzer &&
for prog in scan-build scan-view
do
  cp -rfv tools/clang/tools/$prog %{buildroot}/%{_libdir}/clang-analyzer/
  ln -sfv ../lib/clang-analyzer/$prog/$prog %{buildroot}/usr/bin/
done &&
ln -sfv /usr/bin/clang %{buildroot}/%{_libdir}/clang-analyzer/scan-build/ &&
mv -v %{buildroot}/%{_libdir}/clang-analyzer/scan-build/scan-build.1 %{buildroot}/%{_mandir}/man1/

%files
%defattr(-,root,root,-)
%{_bindir}/bugpoint
%{_bindir}/c-index-test
%{_bindir}/clang
%{_bindir}/clang++
%{_bindir}/clang-check
%{_bindir}/clang-format
%{_bindir}/clang-tblgen
%{_bindir}/llc
%{_bindir}/lli
%{_bindir}/lli-child-target
%{_bindir}/llvm-ar
%{_bindir}/llvm-as
%{_bindir}/llvm-bcanalyzer
%{_bindir}/llvm-config
%{_bindir}/llvm-cov
%{_bindir}/llvm-diff
%{_bindir}/llvm-dis
%{_bindir}/llvm-dwarfdump
%{_bindir}/llvm-extract
%{_bindir}/llvm-link
%{_bindir}/llvm-mc
%{_bindir}/llvm-mcmarkup
%{_bindir}/llvm-nm
%{_bindir}/llvm-objdump
%{_bindir}/llvm-ranlib
%{_bindir}/llvm-readobj
%{_bindir}/llvm-rtdyld
%{_bindir}/llvm-size
%{_bindir}/llvm-stress
%{_bindir}/llvm-symbolizer
%{_bindir}/llvm-tblgen
%{_bindir}/macho-dump
%{_bindir}/opt
%{_bindir}/scan-build
%{_bindir}/scan-view
%{_includedir}/clang-c/*.h
%{_includedir}/clang/*
%{_includedir}/llvm-c/*
%{_includedir}/llvm/*
/usr/lib/clang/%{version}/include/*.h
/usr/lib/clang/%{version}/include/sanitizer/*.h
/usr/lib/clang/%{version}/include/module.map
/usr/lib/clang/%{version}/lib/linux/libclang_rt.*.a
%{_libdir}/clang-analyzer/scan-build/*
%{_libdir}/clang-analyzer/scan-view/*
%{_libdir}/BugpointPasses.so
%{_libdir}/LLVMHello.so
%{_libdir}/libLLVM-*.so
%{_libdir}/libLLVM*.a
%{_libdir}/libLTO.*
%{_libdir}/libclang.so
%{_libdir}/libclang*.a

%files doc
%defattr(-,root,root,-)
%{_docdir}/llvm-%{version}/*
%{_mandir}/man1/*

%clean
rm -rf %{buildroot}

%changelog
* Fri Jul 25 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.4 to 3.4.2
* Tue Mar 18 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.3 to 3.4
* Sun Jul 7 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.2 to 3.3
* Fri Feb 22 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.1 to 3.2
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
