Name:       tcl
Version:    8.6.2
Release:    1%{?dist}
Summary:    Tcl

Group:      System Environment/Base
License:    GPLv2+
Url:        http://tcl.sourceforge.net
Source0:    http://downloads.sourceforge.net/tcl/%{name}%{version}-src.tar.gz
Source1:    %{name}%{version}-html.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build

%description
The Tcl package contains the Tool Command Language, a robust general-purpose scripting language.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}%{version}

%build
tar -xf %SOURCE1 --strip-components=1

export SRCDIR=`pwd` &&

cd unix &&

./configure --prefix=/usr           \
            --without-tzdata        \
            --mandir=/usr/share/man \
            $([ $(uname -m) = x86_64 ] && echo --enable-64bit) \
            --libdir=%{_libdir} &&
make %{?_smp_mflags} &&

sed -e "s#$SRCDIR/unix#%{_libdir}#" \
    -e "s#$SRCDIR#/usr/include#"  \
    -i tclConfig.sh               &&

sed -e "s#$SRCDIR/unix/pkgs/tdbc1.0.1#%{_libdir}/tdbc1.0.0#" \
    -e "s#$SRCDIR/pkgs/tdbc1.0.1/generic#/usr/include#"    \
    -e "s#$SRCDIR/pkgs/tdbc1.0.1/library#%{_libdir}/tcl8.6#" \
    -e "s#$SRCDIR/pkgs/tdbc1.0.1#/usr/include#"            \
    -i pkgs/tdbc1.0.1/tdbcConfig.sh                        &&

sed -e "s#$SRCDIR/unix/pkgs/itcl4.0.1#%{_libdir}/itcl4.0.0#" \
    -e "s#$SRCDIR/pkgs/itcl4.0.1/generic#/usr/include#"    \
    -e "s#$SRCDIR/pkgs/itcl4.0.1#/usr/include#"            \
    -i pkgs/itcl4.0.1/itclConfig.sh                        &&

unset SRCDIR

%check

%install
cd unix
make install DESTDIR=%{buildroot} &&
make install-private-headers DESTDIR=%{buildroot} &&
ln -v -sf tclsh8.6 %{buildroot}/usr/bin/tclsh &&
chmod -v 755 %{buildroot}/%{_libdir}/libtcl8.6.so

mkdir -v -p %{buildroot}/usr/share/doc/tcl-%{version} &&
cp -v -r  ../html/* %{buildroot}/usr/share/doc/tcl-%{version}

%files
%defattr(-,root,root,-)
%{_bindir}/tclsh
%{_bindir}/tclsh8.6
%{_includedir}/*.h
/usr/lib/tcl8*/*
%{_libdir}/libtcl8.6.so
%{_libdir}/libtclstub8.6.a
%{_libdir}/itcl4.0.1/*
%{_libdir}/pkgconfig/tcl.pc
%{_libdir}/sqlite3.8.6/libsqlite3.8.6.so
%{_libdir}/sqlite3.8.6/pkgIndex.tcl
%{_libdir}/tcl8/8.6/tdbc/sqlite3-1.0.1.tm
%{_libdir}/*.sh
%{_libdir}/tdbc1.0.1/*
%{_libdir}/tdbcmysql1.0.1/*
%{_libdir}/tdbcodbc1.0.1/*
%{_libdir}/tdbcpostgres1.0.1/*
%{_libdir}/thread2.7.1/*

%files doc
%defattr(-,root,root,-)
%{_docdir}/tcl-%{version}/*
%{_mandir}/man*/*.gz

%clean
rm -rf %{buildroot}

%changelog
* Fri Sep 19 2014 tanggeliang <tanggeliang@gmail.com>
- update from 8.6.1 to 8.6.2
* Thu Oct 3 2013 tanggeliang <tanggeliang@gmail.com>
- update from 8.6.0 to 8.6.1
* Wed Apr 10 2013 tanggeliang <tanggeliang@gmail.com>
- update from 8.5.12 to 8.6.0
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
