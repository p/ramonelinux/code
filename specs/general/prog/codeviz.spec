Name:       codeviz
Version:    1.0.12
Release:    1%{?dist}
Summary:    CodeViz - A CallGraph Visualiser

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.csn.ul.ie/~mel/projects/codeviz
Source0:    www.csn.ul.ie/~mel/projects/codeviz/#download/%{name}-%{version}.tar.gz
Source1:    gcc-4.6.2.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  graphviz

%description
CodeViz is a call graph generation utility for C/C++.

%prep
%setup -q -n %{name}-%{version}
ln -sv %{SOURCE1} compilers/

%build
./configure --prefix=/usr &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)

%clean
rm -rf %{buildroot}

%changelog
* Tue Jan 7 2014 tanggeliang <tanggeliang@gmail.com>
- create
