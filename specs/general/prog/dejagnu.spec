Name:       dejagnu
Version:    1.5.2
Release:    1%{?dist}
Summary:    DejaGnu

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.gnu.org
Source:     ftp://ftp.gnu.org/pub/gnu/dejagnu/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  expect

%description
DejaGnu is a framework for running test suites on GNU tools.
It is written in expect, which uses Tcl (Tool command language).
It was installed by LFS in the temprary /tools directory. These instructions install it permanently.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

rm -rf %{buildroot}/%{_infodir}/dir

%files
%defattr(-,root,root,-)
%{_bindir}/runtest
%{_includedir}/dejagnu.h
%{_datadir}/dejagnu/baseboards/README
%{_datadir}/dejagnu/baseboards/*.exp
%{_datadir}/dejagnu/config/README
%{_datadir}/dejagnu/config/*.exp
%{_datadir}/dejagnu/*.exp
%{_datadir}/dejagnu/*.c
%{_datadir}/dejagnu/libexec/config.guess

%files doc
%defattr(-,root,root,-)
%{_infodir}/dejagnu.info.gz
%{_mandir}/man1/runtest.1.gz

%clean
rm -rf %{buildroot}

%changelog
* Sat Mar 28 2015 tanggeliang <tanggeliang@gmail.com>
- update from 1.5.1 to 1.5.2
* Wed Aug 21 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.5 to 1.5.1
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
