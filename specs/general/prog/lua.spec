Name:       lua
Version:    5.2.3
Release:    1%{?dist}
Summary:    Powerful light-weight programming language

Group:      Development/Languages
License:    MIT
Url:        http://www.lua.com
Source:     http://www.lua.com/ftp/%{name}-%{version}.tar.gz
Patch:      lua-%{version}-shared_library-1.patch

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build

%description
Lua is a powerful light-weight programming language designed for extending applications.
Lua is also frequently used as a general-purpose, stand-alone language.

%prep
%setup -q -n %{name}-%{version}
%patch -p1

%build
sed -i '/#define LUA_ROOT/s:/usr/local/:/usr/:' src/luaconf.h &&
make linux %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make INSTALL_TOP=%{buildroot}/usr TO_LIB="liblua.so liblua.so.5.2 liblua.so.%{version}" \
     INSTALL_DATA="cp -d" INSTALL_MAN=%{buildroot}/usr/share/man/man1 INSTALL_LIB=%{buildroot}/%{_libdir} install &&
mkdir -pv %{buildroot}/usr/share/doc/lua-%{version} &&
cp -v doc/*.{html,css,gif,png} %{buildroot}/usr/share/doc/lua-%{version}

mkdir -p %{buildroot}/%{_libdir}/pkgconfig
cat > %{buildroot}/%{_libdir}/pkgconfig/lua.pc << "EOF"
V=5.2
R=%{version}

prefix=/usr
INSTALL_BIN=${prefix}/bin
INSTALL_INC=${prefix}/include
INSTALL_LIB=${prefix}/%{_lib}
INSTALL_MAN=${prefix}/man/man1
INSTALL_LMOD=${prefix}/share/lua/${V}
INSTALL_CMOD=${prefix}/%{_lib}/lua/${V}
exec_prefix=${prefix}
libdir=${exec_prefix}/%{_lib}
includedir=${prefix}/include

Name: Lua
Description: An Extensible Extension Language
Version: ${R}
Requires: 
Libs: -L${libdir} -llua -lm
Cflags: -I${includedir}
EOF

%files
%defattr(-,root,root,-)
%{_bindir}/lua
%{_bindir}/luac
%{_includedir}/lauxlib.h
%{_includedir}/lua*.h*
%{_libdir}/liblua.so*
%{_libdir}/pkgconfig/lua.pc
%{_docdir}/lua-%{version}/*
%{_mandir}/man1/lua*.1.gz

%clean
rm -rf %{buildroot}

%changelog
* Sun Dec 15 2013 tanggeliang <tanggeliang@gmail.com>
- update from 5.2.2 to 5.2.3
* Tue Oct 22 2013 tanggeliang <tanggeliang@gmail.com>
- update from 5.1.5 to 5.2.2
* Fri Aug 2 2013 tanggeliang <tanggeliang@gmail.com>
- update from 5.1.4 to 5.1.5
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
