Name:       swig
Version:    3.0.5
Release:    1%{?dist}
Summary:    swig

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://swig.sourceforge.net
Source:     http://downloads.sourceforge.net/project/swig/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  pcre tcl guile bison

%description
SWIG (Simplified Wrapper and Interface Generator) is a compiler that integrates C and C++ with languages including Perl, Python, Tcl, Ruby, PHP, Java, C#, D, Go, Lua, Octave, R, Scheme, Ocaml, Modula-3, Common Lisp, and Pike.
SWIG can also export its parse tree into Lisp s-expressions and XML.

%prep
%setup -q -n %{name}-%{version}

%build
sed -e 's/"\.")/"_")/' -i Source/Modules/go.cxx

./configure --prefix=/usr 		       \
            --without-clisp                    \
            --without-maximum-compile-warnings \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/ccache-swig
%{_bindir}/swig
%{_datadir}/swig/%{version}/*
%{_mandir}/man1/ccache-swig.1.gz

%clean
rm -rf %{buildroot}

%changelog
* Mon Mar 30 2015 tanggeliang <tanggeliang@gmail.com>
- update from 2.0.12 to 3.0.5
* Wed Feb 19 2014 tanggeliang <tanggeliang@gmail.com>
- update from 2.0.11 to 2.0.12
* Tue Oct 1 2013 tanggeliang <tanggeliang@gmail.com>
- create
