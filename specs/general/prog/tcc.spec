Name:       tcc
Version:    0.9.26
Release:    1%{?dist}
Summary:    Tiny C Compiler

Group:      System Environment/Tools
License:    GPLv2+
Url:        http://bellard.org/tcc
Source:     http://download.savannah.gnu.org/releases/tinycc/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build

%description
TinyCC (aka TCC) is a small but hyper fast C compiler.
Unlike other C compilers, it is meant to be self-relying: you do not need an external assembler or linker because TCC does that for you.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
	    --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/tcc
%{_includedir}/libtcc.h
%{_libdir}/libtcc.a
%{_libdir}/tcc/include/float.h
%{_libdir}/tcc/include/stdarg.h
%{_libdir}/tcc/include/stdbool.h
%{_libdir}/tcc/include/stddef.h
%{_libdir}/tcc/include/tcclib.h
%{_libdir}/tcc/include/varargs.h
%{_libdir}/tcc/libtcc1.a
%{_docdir}/tcc/tcc-doc.html
%{_infodir}/tcc-doc.info.gz
%{_mandir}/man1/tcc.1.gz

%clean
rm -rf %{buildroot}

%changelog
* Fri Sep 18 2015 tanggeliang <tanggeliang@gmail.com>
- create
