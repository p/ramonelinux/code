Name:       emacs
Version:    24.3
Release:    1%{?dist}
Summary:    Emacs

Group:      System Environment/tools
License:    GPLv2+
Url:        http://www.gnu.org
Source:     http://ftp.gnu.org/pub/gnu/emacs/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  libx11 libxaw libxft libjpeg-turbo libpng libtiff librsvg
BuildRequires:  fontconfig libxml freetype gnutls gconf gsettings-desktop-schemas dbus imagemagick
BuildRequires:  autoconf automake m4

%description
The Emacs package contains an extensible, customizable, self-documenting real-time display editor.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr           \
            --libexecdir=%{_libdir} \
            --with-gif=no           \
            --localstatedir=/var    \
            --libdir=%{_libdir} &&
make bootstrap %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}
#chown -v -R root:root %{buildroot}/usr/share/emacs/%{version}

rm -f %{buildroot}/%{_infodir}/dir
rm -f %{buildroot}/%{_infodir}/info.info.gz

%files
%defattr(-,root,root,-)
%{_bindir}/emacs-%{version}
%{_bindir}/ebrowse
%{_bindir}/ctags
%{_bindir}/emacsclient
%{_bindir}/emacs
%{_bindir}/grep-changelog
%{_bindir}/etags
%ifarch %{ix86}
%{_libdir}/emacs/%{version}/i686-pc-linux-gnu/rcs2log
%{_libdir}/emacs/%{version}/i686-pc-linux-gnu/movemail
%{_libdir}/emacs/%{version}/i686-pc-linux-gnu/profile
%{_libdir}/emacs/%{version}/i686-pc-linux-gnu/update-game-score
%{_libdir}/emacs/%{version}/i686-pc-linux-gnu/hexl
%else %ifarch x86_64
%{_libdir}/emacs/%{version}/x86_64-unknown-linux-gnu/hexl
%{_libdir}/emacs/%{version}/x86_64-unknown-linux-gnu/movemail
%{_libdir}/emacs/%{version}/x86_64-unknown-linux-gnu/profile
%{_libdir}/emacs/%{version}/x86_64-unknown-linux-gnu/rcs2log
%{_libdir}/emacs/%{version}/x86_64-unknown-linux-gnu/update-game-score
%endif
%{_datadir}/applications/emacs.desktop
%{_datadir}/emacs/site-lisp/subdirs.el
%{_datadir}/icons/hicolor/scalable/mimetypes/emacs-document.svg
%{_datadir}/icons/hicolor/scalable/apps/emacs.svg
%{_datadir}/icons/hicolor/*x*/apps/emacs.png
%{_datadir}/emacs/%{version}
%{_infodir}/*.info.gz
%{_mandir}/man1/*.1.gz
/var/games/emacs/tetris-scores
/var/games/emacs/snake-scores

%clean
rm -rf %{buildroot}

%changelog
* Thu Sep 12 2013 tanggeliang <tanggeliang@gmail.com>
- create
