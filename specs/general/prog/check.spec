Name:       check
Version:    0.9.9
Release:    2%{?dist}
Summary:    Check

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://check.sourceforge.net
Source:     http://downloads.sourceforge.net/check/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build

%description
Check is a unit testing framework for C.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --disable-static \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make docdir=/usr/share/doc/check-0.9.9 install DESTDIR=%{buildroot}

rm -rf %{buildroot}/%{_infodir}/dir

%files
%defattr(-,root,root,-)
%{_bindir}/checkmk
%{_includedir}/check.h
%{_libdir}/libcheck.*a
%{_libdir}/libcheck.so*
%{_libdir}/pkgconfig/check.pc
%{_datadir}/aclocal/check.m4

%files doc
%defattr(-,root,root,-)
%{_docdir}/check-0.9.9/*
%{_infodir}/check.info.gz
%{_mandir}/man1/checkmk.1.gz

%clean
rm -rf %{buildroot}

%changelog
* Fri Nov 2 2012 tanggeliang <tanggeliang@gmail.com>
- update from 0.9.8 to 0.9.9
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
