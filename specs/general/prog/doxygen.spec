Name:       doxygen
Version:    1.8.6
Release:    1%{?dist}
Summary:    Doxygen

Group:      System Environment/Base
License:    GPLv2+
Url:        http://tcl.sourceforge.net
Source:     http://ftp.stack.nl/pub/doxygen/%{name}-%{version}.src.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  flex bison

%description
The Doxygen package contains a documentation system for C++, C, Java, Objective-C, Corba IDL and to some extent PHP, C# and D. This is useful for generating HTML documentation and/or an off-line reference manual from a set of documented source files. There is also support for generating output in RTF, PostScript, hyperlinked PDF, compressed HTML, and Unix man pages. The documentation is extracted directly from the sources, which makes it much easier to keep the documentation consistent with the source code.

You can also configure Doxygen to extract the code structure from undocumented source files. This is very useful to quickly find your way in large source distributions. Used along with Graphviz, you can also visualize the relations between the various elements by means of include dependency graphs, inheritance diagrams, and collaboration diagrams, which are all generated automatically.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix /usr \
            --docdir /usr/share/doc/doxygen-%{version} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

%files
%defattr(-,root,root,-)
%{_bindir}/doxygen

%files doc
%defattr(-,root,root,-)
/usr/man/man1/doxygen.1.gz

%clean
rm -rf %{buildroot}

%changelog
* Thu Mar 20 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.8.5 to 1.8.6
* Tue Oct 22 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.8.2 to 1.8.5
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
