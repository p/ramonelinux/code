Name:       slang
Version:    2.2.4
Release:    5%{?dist}
Summary:    S-Lang

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.s-lang.org
Source:     ftp://space.mit.edu/pub/davis/slang/v2.2/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:	libpng pcre

%description
S-Lang is an interpreted language that maybe be embedded into an application to make the application extensible.
It provides facilities required by interactive applications such as display/screen management, keyboard input and keymaps.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --sysconfdir=/etc &&
make -j1

%check

%install
rm -rf %{buildroot}

make install_doc_dir=/usr/share/doc/slang-2.2.4   \
     SLSH_DOC_DIR=/usr/share/doc/slang-2.2.4/slsh \
     install-all DESTDIR=%{buildroot} &&

chmod -v 755 %{buildroot}/usr/lib/libslang.so.2.2.4 \
             %{buildroot}/usr/lib/slang/v2/modules/*.so

%files
%defattr(-,root,root,-)
%{_sysconfdir}/slsh.rc
%{_bindir}/slsh
%{_includedir}/sl*.h
%{_libdir}/libslang.a
%{_libdir}/libslang.so*
%{_libdir}/pkgconfig/slang.pc
%{_libdir}/slang/v2/modules/*-module.so
%{_datadir}/slsh/*

%files doc
%defattr(-,root,root,-)
%{_docdir}/slang-2.2.4/*
%{_mandir}/man1/slsh.1.gz

%clean
rm -rf %{buildroot}

%changelog
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
