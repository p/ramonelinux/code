Name:       sublimetext
Version:    2.0.2
Release:    1%{?dist}
Summary:    Sublime Text 2

Group:      System Environment/Base
License:    GPLv2+
Url:        http://www.sublimetext.com
Source0:    http://www.sublimetext.com/2/Sublime_Text_%{version}.tar.bz2
Source1:    http://www.sublimetext.com/2/Sublime_Text_%{version}_x64.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build

%description
Sublime Text is a sophisticated text editor for code, markup and prose.

%prep
%setup -c -T
%ifarch %{ix86}
tar xvf %{SOURCE0}
%else %ifarch x86_64
tar xvf %{SOURCE1}
%endif

%build

%check

%install
mkdir -p %{buildroot}/usr/{bin,share/%{name}} &&
cp -a Sublime\ Text\ 2/* %{buildroot}/usr/share/%{name}/
ln -s /usr/share/%{name}/sublime_text %{buildroot}/usr/bin/sublime_text

%files
%defattr(-,root,root,-)
%{_bindir}/sublime_text
%{_datadir}/%{name}/*

%clean
rm -rf %{buildroot}

%changelog
* Tue Nov 12 2013 tanggeliang <tanggeliang@gmail.com>
- create
