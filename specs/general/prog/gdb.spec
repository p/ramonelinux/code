Name:       gdb
Version:    7.9
Release:    1%{?dist}
Summary:    gdb

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.gnu.org
Source:     http://ftp.gnu.org/gnu/gdb/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  dejagnu

%description
GDB, the GNU Project debugger, allows you to see what is going on `inside' another program while it executes -- or what another program was doing at the moment it crashed.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --with-system-readline \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make -C gdb install DESTDIR=%{buildroot}

rm -f %{buildroot}/%{_infodir}/dir

%files
%defattr(-,root,root,-)
%{_bindir}/gcore
%{_bindir}/gdb
%{_bindir}/gdbserver
%{_includedir}/gdb/jit-reader.h
%{_libdir}/libinproctrace.so
%{_datadir}/gdb/system-gdbinit/*.py
%{_datadir}/gdb/python/gdb/*.py
%{_datadir}/gdb/python/gdb/command/*.py
%{_datadir}/gdb/python/gdb/function/*.py
%{_datadir}/gdb/python/gdb/printer/*.py
%{_datadir}/gdb/syscalls/*-linux.xml
%{_datadir}/gdb/syscalls/gdb-syscalls.dtd
%{_infodir}/*.info*.gz
%{_mandir}/man1/*.1.gz
%{_mandir}/man5/gdbinit.5.gz

%clean
rm -rf %{buildroot}

%changelog
* Sat Mar 28 2015 tanggeliang <tanggeliang@gmail.com>
- update from 7.8.1 to 7.9
* Tue Nov 18 2014 tanggeliang <tanggeliang@gmail.com>
- update from 7.7 to 7.8.1
* Tue Mar 18 2014 tanggeliang <tanggeliang@gmail.com>
- update from 7.6.2 to 7.7
* Sun Dec 15 2013 tanggeliang <tanggeliang@gmail.com>
- update from 7.6.1 to 7.6.2
* Thu Oct 3 2013 tanggeliang <tanggeliang@gmail.com>
- update from 7.6 to 7.6.1
* Tue May 14 2013 tanggeliang <tanggeliang@gmail.com>
- update from 7.5 to 7.6
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
