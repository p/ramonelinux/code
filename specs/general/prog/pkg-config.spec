Name:       pkg-config
Version:    0.28
Release:    2%{?dist}
Summary:    pkg-config

Group:      System Environment/core
License:    GPLv2+
Url:        http://www.freedesktop.org
Source:     http://pkgconfig.freedesktop.org/releases/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  gettext

%description
The pkg-config package contains a tool for passing the include path and/or library paths to build tools during the configure and make file execution.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr         \
            --with-internal-glib  \
            --disable-host-tool   \
            --libdir=%{_libdir}   \
            --docdir=%{_docdir}/pkg-config-%{version} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/pkg-config
%{_datadir}/aclocal/pkg.m4

%files doc
%defattr(-,root,root,-)
%{_docdir}/pkg-config-%{version}/pkg-config-guide.html
%{_mandir}/*/*

%clean
rm -rf %{buildroot}

%changelog
* Sun Mar 17 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.27.1 to 0.28
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
