Name:       gc
Version:    7.4.2
Release:    1%{?dist}
Summary:    GC

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.gc.org
Source:     http://www.hpl.hp.com/personal/Hans_Boehm/gc/gc_source/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  libatomic_ops autoconf automake m4 libtool

%description
The GC package contains the Boehm-Demers-Weiser conservative garbage collector, which can be used as a garbage collecting replacement for the C malloc function or C++ new operator.
It allows you to allocate memory basically as you normally would, without explicitly deallocating memory that is no longer useful.
The collector automatically recycles memory when it determines that it can no longer be otherwise accessed.
The collector is also used by a number of programming language implementations that either use C as intermediate code, want to facilitate easier interoperation with C libraries, or just prefer the simple collector interface.
Alternatively, the garbage collector may be used as a leak detector for C or C++ programs, though that is not its primary goal.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
sed -i 's#pkgdata#doc#' doc/doc.am &&
autoreconf -fi  &&
./configure --prefix=/usr      \
            --enable-cplusplus \
            --disable-static   \
            --docdir=/usr/share/doc/gc-%{version} \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot} &&

mkdir -pv %{buildroot}/%{_mandir}/man3 &&
install -v -m644 doc/gc.man %{buildroot}/usr/share/man/man3/gc_malloc.3 &&
ln -sfv gc_malloc.3 %{buildroot}/usr/share/man/man3/gc.3 

%files
%defattr(-,root,root,-)
%{_includedir}/gc*.h
%{_includedir}/gc/*.h
%{_libdir}/libcord.la
%{_libdir}/libcord.so*
%{_libdir}/libgc.la
%{_libdir}/libgc.so*
%{_libdir}/libgccpp.la
%{_libdir}/libgccpp.so*
%{_libdir}/pkgconfig/bdw-gc.pc

%files doc
%defattr(-,root,root,-)
%{_docdir}/gc-%{version}/*
%{_mandir}/man3/gc*.3.gz

%clean
rm -rf %{buildroot}

%changelog
* Sun Jul 20 2014 tanggeliang <tanggeliang@gmail.com>
- update from 7.4.0 to 7.4.2
* Wed Dec 4 2013 tanggeliang <tanggeliang@gmail.com>
- update from 7.2 to 7.4.0
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
