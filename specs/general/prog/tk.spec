Name:       tk
Version:    8.6.1
Release:    1%{?dist}
Summary:    Tk

Group:      System Environment/Base
License:    GPLv2+
Url:        http://tcl.sourceforge.net
Source:     http://downloads.sourceforge.net/tcl/%{name}%{version}-src.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  tcl libx11

%description
The Tk package contains a TCL GUI Toolkit.

%prep
%setup -q -n %{name}%{version}

%build
cd unix &&
./configure --prefix=/usr \
            --mandir=/usr/share/man \
            $([ $(uname -m) = x86_64 ] && echo --enable-64bit) \
            --libdir=%{_libdir} &&
make %{?_smp_mflags} &&

sed -e "s@^\(TK_SRC_DIR='\).*@\1/usr/include'@" \
    -e "/TK_B/s@='\(-L\)\?.*unix@='\1/usr/lib@" \
    -i tkConfig.sh

%check

%install
cd unix
make install DESTDIR=%{buildroot} &&
make install-private-headers DESTDIR=%{buildroot} &&
ln -v -sf wish8.6 %{buildroot}/usr/bin/wish &&
chmod -v 755 %{buildroot}/%{_libdir}/libtk8.6.so

%files
%defattr(-,root,root,-)
%{_bindir}/wish
%{_bindir}/wish8.6
%{_includedir}/*tk*.h
%{_libdir}/libtk8.6.so
%{_libdir}/libtkstub8.6.a
%{_libdir}/pkgconfig/tk.pc
%{_libdir}/tk8.6/*
%{_libdir}/tkConfig.sh
%{_mandir}/man*/*.gz
/usr/lib/tk8.6/*.tcl
/usr/lib/tk8.6/tclIndex
/usr/lib/tk8.6/tkAppInit.c
/usr/lib/tk8.6/demos/*
/usr/lib/tk8.6/images/*
/usr/lib/tk8.6/msgs/*
/usr/lib/tk8.6/ttk/*

%clean
rm -rf %{buildroot}

%changelog
* Thu Oct 3 2013 tanggeliang <tanggeliang@gmail.com>
- update from 8.6.0 to 8.6.1
* Sat Jul 6 2013 tanggeliang <tanggeliang@gmail.com>
- update from 8.5.12 to 8.6.0
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
