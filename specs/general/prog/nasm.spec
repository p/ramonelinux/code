Name:       nasm
Version:    2.11.05
Release:    1%{?dist}
Summary:    NASM

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.nasm.us
Source0:    http://www.nasm.us/pub/nasm/releasebuilds/%{version}/%{name}-%{version}.tar.xz
Source1:    http://www.nasm.us/pub/nasm/releasebuilds/%{version}/%{name}-%{version}-xdoc.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build

%description
NASM (Netwide Assembler) is an 80x86 assembler designed for portability and modularity. It includes a disassembler as well.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}
tar -xf %SOURCE1 --strip-components=1

%build
./configure --prefix=/usr &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make install INSTALLROOT=%{buildroot}

install -m755 -d         %{buildroot}/usr/share/doc/nasm-%{version}/html && 
cp -v doc/html/*.html    %{buildroot}/usr/share/doc/nasm-%{version}/html &&
cp -v doc/*.{txt,ps,pdf} %{buildroot}/usr/share/doc/nasm-%{version}      &&
mkdir -pv                %{buildroot}/usr/share/info                  &&
cp -v doc/info/*         %{buildroot}/usr/share/info

%files
%defattr(-,root,root,-)
%{_bindir}/nasm
%{_bindir}/ndisasm

%files doc
%defattr(-,root,root,-)
%{_docdir}/nasm-%{version}/*
%{_infodir}/nasm.info*.gz
%{_mandir}/man1/nasm.1.gz
%{_mandir}/man1/ndisasm.1.gz

%clean
rm -rf %{buildroot}

%changelog
* Sat Jul 26 2014 tanggeliang <tanggeliang@gmail.com>
- update from 2.11.02 to 2.11.05
* Wed Aug 21 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.10.05 to 2.10.09
* Sat Nov 3 2012 tanggeliang <tanggeliang@gmail.com>
- update from 2.10.04 to 2.10.05
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
