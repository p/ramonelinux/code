Name:       xml-parser
Version:    2.41
Release:    14%{?dist}
Summary:    XML::Parser

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://cpan.org
Source:     http://cpan.org/authors/id/M/MS/MSERGEANT/XML-Parser-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  expat
Requires:       expat
AutoReq:        no

%description
The XML::Parser module is a Perl extension interface to James Clark's XML parser, expat.
The module is installed using the standard Perl module build and installation instructions.

%prep
%setup -q -n XML-Parser-%{version}

%build
perl Makefile.PL &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{perl_archlib}/perllocal.pod
%{perl_sitearch}/XML/Parser.pm
%{perl_sitearch}/XML/Parser/Encodings/*
%{perl_sitearch}/XML/Parser/Expat.pm
%{perl_sitearch}/XML/Parser/LWPExternEnt.pl
%{perl_sitearch}/XML/Parser/Style/*.pm
%{perl_sitearch}/auto/XML/Parser/Expat/Expat.so
%{perl_sitearch}/auto/XML/Parser/.packlist
%{_mandir}/man3/XML::Parser*.3.gz

%clean
rm -rf %{buildroot}

%changelog
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
