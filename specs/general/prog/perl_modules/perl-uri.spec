Name:       perl-uri
Version:    1.60
Release:    10%{?dist}
Summary:    Perl URI module

Group:      Development/Libraries
License:    GPL+ or Artistic
Url:        http://search.cpan.org/dist/URI/
Source0:    http://www.cpan.org/authors/id/G/GA/GAAS/URI-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
Requires:       perl

%description
A Perl module implementing URI parsing and manipulation.

%prep
%setup -q -n URI-%{version}
chmod -c 644 uri-test

%build
perl Makefile.PL INSTALLDIRS=perl
make %{?_smp_mflags}

%install
make pure_install DESTDIR=%{buildroot}

%check

%files
%{perl_privlib}/URI.pm
%{perl_privlib}/URI/*
%{perl_archlib}/auto/URI/.packlist
%{_mandir}/man3/URI*

%changelog
* Wed Jun 19 2013 tanggeliang <tanggeliang@gmail.com>
- remove "BuildArch noarch"
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
