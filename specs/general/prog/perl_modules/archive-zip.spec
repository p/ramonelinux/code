Name:       archive-zip
Version:    1.37
Release:    1%{?dist}
Summary:    Archive::Zip

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://cpan.org
Source:     www.cpan.org/authors/id/A/AD/ADAMK/Archive-Zip-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildArch:      noarch

%description
The Archive::Zip module allows a Perl program to create, manipulate, read, and write Zip archive files.

%prep
%setup -q -n Archive-Zip-%{version}

%build
perl Makefile.PL &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

rm -rf %{buildroot}/%{perl_archlib}/perllocal.pod

%files
%defattr(-,root,root,-)
%{_bindir}/crc32
%{perl_sitelib}/Archive/Zip.pm
%{perl_sitelib}/Archive/Zip/*.pm
%{perl_sitelib}/Archive/Zip/FAQ.pod
%{perl_sitearch}/auto/Archive/Zip/.packlist
%{_mandir}/man3/Archive::Zip*.3.gz

%clean
rm -rf %{buildroot}

%changelog
* Sat Jul 26 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.30 to 1.37
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
