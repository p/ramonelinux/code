Name:       xml-simple
Version:    2.20
Release:    7%{?dist}
Summary:    XML::Simple

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.cpan.org
Source:     http://www.cpan.org/authors/id/G/GR/GRANTM/XML-Simple-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build

%description
The XML::Simple module is a Perl extension that provides an easy API to read and write XML (especially config files).
The module and all dependencies are installed using the standard Perl module build and installation instructions.

%prep
%setup -q -n XML-Simple-%{version}

%build
perl Makefile.PL &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

#conflicts XML-Parser
rm -rf %{buildroot}/%{perl_archlib}/perllocal.pod

%files
%defattr(-,root,root,-)
%{perl_sitelib}/XML/Simple.pm
%{perl_sitelib}/XML/Simple/FAQ.pod
%{perl_sitearch}/auto/XML/Simple/.packlist
%{_mandir}/man3/XML::Simple*.3.gz

%clean
rm -rf %{buildroot}

%changelog
* Sat Jul 27 2013 tanggeliang <tanggeliang@gmail.com>
- remove BuildArch noarch
* Sun Mar 17 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.18 to 2.20
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
