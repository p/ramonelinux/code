Name:       intel2gas
Version:    1.3.3
Release:    1%{?dist}
Summary:    intel2gas

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://freecode.com/projects/intel2gas
Source:     http://freecode.com/projects/intel2gas/%{name}-%{version}.tar.gz
Patch:      %{name}-%{version}-ram.diff

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build

%description
intel2gas is a converter between the source format of the NASM and GAS (GNU Assembler) assemblers. It was originally written for use with the Hermes project but with extensibility in mind. Syntax files can be written to allow conversion between any other assembly languages. Conversion between MASM and C inline assembly and GNU syntax is now available.

%prep
%setup -q -n %{name}-%{version}
%patch -p1

%build
./configure --prefix=/usr &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make install prefix=%{buildroot}/usr

%files
%defattr(-,root,root,-)
%{_bindir}/intel2gas
%{_datadir}/intel2gas/g2i/*
%{_datadir}/intel2gas/i2g/*
%{_datadir}/intel2gas/m2g/*

%clean
rm -rf %{buildroot}

%changelog
* Thu Dec 26 2013 tanggeliang <tanggeliang@gmail.com>
- create
