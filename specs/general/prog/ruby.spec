Name:       ruby
Version:    2.1.1
Release:    1%{?dist}
Summary:    Ruby

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.ruby-lang.org
Source:     ftp://ftp.ruby-lang.org/pub/ruby/2.1/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  yaml openssl gdbm libffi

%description
The Ruby package contains the Ruby development environment.
This is useful for object-oriented scripting.

%prep
%setup -q -n %{name}-%{version}

%build
sed -i ext/readline/readline.c \
    -e "s:(Function \*)readline:(rl_hook_func_t \*)readline:" &&
./configure --prefix=/usr   \
            --enable-shared \
            --docdir=%{_docdir}/ruby-%{version} \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

%files
%defattr(-,root,root,-)
%{_bindir}/erb
%{_bindir}/gem
%{_bindir}/irb
%{_bindir}/rake
%{_bindir}/rdoc
%{_bindir}/ri
%{_bindir}/ruby
%{_bindir}/testrb
%{_includedir}/ruby-2.1.0/*
%{_libdir}/libruby-static.a
%{_libdir}/libruby.so*
%{_libdir}/pkgconfig/ruby-2.1.pc
%{_libdir}/ruby/2.1.0/*
%{_libdir}/ruby/gems/2.1.0/*
%{_mandir}/man1/*.1.gz
%{_datadir}/ri/2.1.0/system/*

%clean
rm -rf %{buildroot}

%changelog
* Fri Mar 21 2014 tanggeliang <tanggeliang@gmail.com>
- update from 2.0.0 to 2.1.1
* Thu Jul 25 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.9.3 to 2.0.0
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
