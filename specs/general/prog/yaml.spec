Name:       yaml
Version:    0.1.4
Release:    7%{?dist}
Summary:    a YAML parser and emitter written in C

Group:      System Environment/Libraries
License:    MIT
Url:        http://pyyaml.org/
Source:     http://pyyaml.org/download/libyaml/%{name}-%{version}.tar.gz

BuildRoot:  %{_tmppath}/%{name}-%{version}-%{release}-build

%description
YAML is a data serialization format designed for human readability and interaction with scripting languages.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
%configure
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} INSTALL="install -p" install
rm -f %{buildroot}%{_libdir}/*.{la,a}

%check
make check

%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%{_includedir}/yaml.h
%{_libdir}/libyaml*.so.*
%{_libdir}/libyaml*.so
%{_libdir}/pkgconfig/yaml-0.1.pc

%files doc
%defattr(-,root,root,-)
%doc LICENSE README
%doc doc/html

%changelog
* Sun May 1 2011 tanggeliang <tanggeliang@gmail.com>
- porting from Fedora
