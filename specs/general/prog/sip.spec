%{!?python_sitearch:%global python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")}
%{!?python_inc:%global python_inc %(%{__python} -c "from distutils.sysconfig import get_python_inc; print get_python_inc(1)")}

Summary: SIP - Python/C++ Bindings Generator
Name: sip
Version: 4.13.3
Release: 4%{?dist}
# sipgen/parser.{c.h} is GPLv3+ with exceptions (bison)
License: GPLv2 or GPLv3 and (GPLv3+ with exceptions)
Group: Development/Tools
Url: http://www.riverbankcomputing.com/software/sip/intro 
Source0: http://www.riverbankcomputing.com/static/Downloads/sip4/sip-%{version}%{?snap:-snapshot-%{snap}}.tar.gz
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

## upstreamable patches
# make install should not strip (by default), kills -debuginfo
Patch50: sip-4.12.1-no_strip.patch
# try not to rpath the world
Patch51: sip-4.13.3-no_rpath.patch

# extracted from sip.h, SIP_API_MAJOR_NR SIP_API_MINOR_NR defines
Source1: macros.sip
%global _sip_api_major 8
%global _sip_api_minor 1
%global _sip_api %{_sip_api_major}.%{_sip_api_minor}

BuildRequires: python
BuildRequires: sed

%description
SIP is a tool for generating bindings for C++ classes so that they can be
accessed as normal Python classes. SIP takes many of its ideas from SWIG but,
because it is specifically designed for C++ and Python, is able to generate
tighter bindings. SIP is so called because it is a small SWIG.

SIP was originally designed to generate Python bindings for KDE and so has
explicit support for the signal slot mechanism used by the Qt/KDE class
libraries. However, SIP can be used to generate Python bindings for any C++
class library.

%prep

%setup -q -n %{name}-%{version}%{?snap:-snapshot-%{snap}}

%patch50 -p1 -b .no_strip
%patch51 -p1 -b .no_rpath

%build
%{__python} configure.py -d %{python_sitearch} CXXFLAGS="%{optflags}" CFLAGS="%{optflags}"
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

# Python 2 installation:
make install DESTDIR=%{buildroot}
mkdir -p %{buildroot}%{_datadir}/sip

# Macros used by -devel subpackages:
install -D -p -m644 %{SOURCE1} %{buildroot}%{_sysconfdir}/rpm/macros.sip

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc LICENSE LICENSE-GPL2 LICENSE-GPL3
%doc NEWS README
%{python_sitearch}/*
%{_bindir}/sip
%{_datadir}/sip/
%{python_inc}/*
%{_sysconfdir}/rpm/macros.sip

%changelog
