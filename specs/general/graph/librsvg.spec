Name:       librsvg
Version:    2.40.10
Release:    1%{?dist}
Summary:    librsvg

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/librsvg/2.40/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  gdk-pixbuf libcroco pango
BuildRequires:  gtk2 gtk+ gobject-introspection vala
BuildRequires:  gtk-doc libxml gnome-common

%description
The librsvg package contains librsvg libraries and tools used to manipulate, convert and view Scalable Vector Graphic (SVG) images.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --enable-vala \
            --disable-static \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%post
gdk-pixbuf-query-loaders --update-cache || :

%postun
gdk-pixbuf-query-loaders --update-cache || :

%files
%defattr(-,root,root,-)
%{_bindir}/rsvg*
%{_includedir}/librsvg-2.0/librsvg/*.h
%{_libdir}/pkgconfig/librsvg-2.0.pc
%{_libdir}/gdk-pixbuf-2.0/2.10.0/loaders/libpixbufloader-svg.*
%{_libdir}/girepository-1.0/Rsvg-2.0.typelib
%{_libdir}/librsvg-2.*
%{_datadir}/gir-1.0/Rsvg-2.0.gir
%{_datadir}/vala/vapi/librsvg-2.0.vapi

%files doc
%defattr(-,root,root,-)
%{_datadir}/gtk-doc/html/rsvg-2.0/*
%{_mandir}/*/*

%clean
rm -rf %{buildroot}

%changelog
* Wed Sep 23 2015 tanggeliang <tanggeliang@gmail.com>
- update from 2.40.2 to 2.40.10
* Thu Mar 20 2014 tanggeliang <tanggeliang@gmail.com>
- update from 2.40.1 to 2.40.2
* Tue Dec 3 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.40.0 to 2.40.1
* Wed Oct 23 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.37.0 to 2.40.0
* Sat Apr 13 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.36.4 to 2.37.0
* Sat Oct 27 2012 tanggeliang <tanggeliang@gmail.com>
- update from 2.36.3 to 2.36.4
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
