Name:       pixman
Version:    0.32.6
Release:    1%{?dist}
Summary:    pixman

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://cairographics.org
Source:     http://cairographics.org/releases/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  libpng

%description
The pixman package contains a library that provides low-level pixel manipulation features such as image compositing and trapezoid rasterization.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --disable-static \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_includedir}/pixman-1/pixman*.h
%{_libdir}/libpixman-1.*
%{_libdir}/pkgconfig/pixman-1.pc

%clean
rm -rf %{buildroot}

%changelog
* Mon Jul 21 2014 tanggeliang <tanggeliang@gmail.com>
- update from 0.32.4 to 0.32.6
* Tue Dec 3 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.30.2 to 0.32.4
* Sun Aug 18 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.30.0 to 0.30.2
* Thu Jul 25 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.28.2 to 0.30.0
* Sat Feb 23 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.26.2 to 0.28.2
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
