Name:       leptonica
Version:    1.69
Release:    1%{?dist}
Summary:    leptonica

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://code.google.com/p/leptonica
Source:     http://code.google.com/p/leptonica/%{name}-%{version}.tar.gz
Patch:      leptonica-1.69-gifio.diff

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  libpng libjpeg-turbo giflib libtiff libwebp
BuildRequires:  autoconf automake m4

%description
An open source C library for efficient image processing and image analysis operations.

%prep
%setup -q -n %{name}-%{version}
%patch -p1

%build
./configure --prefix=/usr \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/adaptmaptest
%{_bindir}/adaptnorm_reg
%{_bindir}/affine_reg
%{_bindir}/alltests_reg
%{_bindir}/alphaops_reg
%{_bindir}/alphaxform_reg
%{_bindir}/arithtest
%{_bindir}/barcodetest
%{_bindir}/baselinetest
%{_bindir}/bilinear_reg
%{_bindir}/binarize_reg
%{_bindir}/bincompare
%{_bindir}/binmorph1_reg
%{_bindir}/binmorph2_reg
%{_bindir}/binmorph3_reg
%{_bindir}/binmorph4_reg
%{_bindir}/binmorph5_reg
%{_bindir}/blend2_reg
%{_bindir}/blend_reg
%{_bindir}/blendcmaptest
%{_bindir}/blendtest1
%{_bindir}/buffertest
%{_bindir}/byteatest
%{_bindir}/ccbordtest
%{_bindir}/cctest1
%{_bindir}/ccthin1_reg
%{_bindir}/ccthin2_reg
%{_bindir}/cmapquant_reg
%{_bindir}/coloring_reg
%{_bindir}/colormask_reg
%{_bindir}/colormorphtest
%{_bindir}/colorquant_reg
%{_bindir}/colorseg_reg
%{_bindir}/colorsegtest
%{_bindir}/colorspacetest
%{_bindir}/compare_reg
%{_bindir}/comparepages
%{_bindir}/comparetest
%{_bindir}/compfilter_reg
%{_bindir}/conncomp_reg
%{_bindir}/contrasttest
%{_bindir}/conversion_reg
%{_bindir}/convertfilestopdf
%{_bindir}/convertfilestops
%{_bindir}/convertformat
%{_bindir}/convertsegfilestopdf
%{_bindir}/convertsegfilestops
%{_bindir}/converttogray
%{_bindir}/converttops
%{_bindir}/convolve_reg
%{_bindir}/convolvetest
%{_bindir}/cornertest
%{_bindir}/croptext
%{_bindir}/dewarp_reg
%{_bindir}/dewarptest1
%{_bindir}/dewarptest2
%{_bindir}/dewarptest3
%{_bindir}/digitprep1
%{_bindir}/distance_reg
%{_bindir}/dithertest
%{_bindir}/dna_reg
%{_bindir}/dwalineargen
%{_bindir}/dwamorph1_reg
%{_bindir}/dwamorph2_reg
%{_bindir}/edgetest
%{_bindir}/enhance_reg
%{_bindir}/equal_reg
%{_bindir}/expand_reg
%{_bindir}/extrema_reg
%{_bindir}/falsecolortest
%{_bindir}/fcombautogen
%{_bindir}/fhmtauto_reg
%{_bindir}/fhmtautogen
%{_bindir}/fileinfo
%{_bindir}/findpattern1
%{_bindir}/findpattern2
%{_bindir}/findpattern3
%{_bindir}/findpattern_reg
%{_bindir}/flipdetect_reg
%{_bindir}/flipselgen
%{_bindir}/fmorphauto_reg
%{_bindir}/fmorphautogen
%{_bindir}/fpix_reg
%{_bindir}/fpixcontours
%{_bindir}/gammatest
%{_bindir}/genfonts
%{_bindir}/gifio_reg
%{_bindir}/graphicstest
%{_bindir}/grayfill_reg
%{_bindir}/graymorph1_reg
%{_bindir}/graymorph2_reg
%{_bindir}/graymorphtest
%{_bindir}/grayquant_reg
%{_bindir}/hardlight_reg
%{_bindir}/heap_reg
%{_bindir}/histotest
%{_bindir}/inserttest
%{_bindir}/ioformats_reg
%{_bindir}/iotest
%{_bindir}/jbcorrelation
%{_bindir}/jbrankhaus
%{_bindir}/jbwords
%{_bindir}/kernel_reg
%{_bindir}/lineremoval
%{_bindir}/listtest
%{_bindir}/livre_adapt
%{_bindir}/livre_hmt
%{_bindir}/livre_makefigs
%{_bindir}/livre_orient
%{_bindir}/livre_pageseg
%{_bindir}/livre_seedgen
%{_bindir}/livre_tophat
%{_bindir}/locminmax_reg
%{_bindir}/logicops_reg
%{_bindir}/lowaccess_reg
%{_bindir}/maketile
%{_bindir}/maze_reg
%{_bindir}/misctest1
%{_bindir}/modifyhuesat
%{_bindir}/morphseq_reg
%{_bindir}/morphtest1
%{_bindir}/mtifftest
%{_bindir}/numa_reg
%{_bindir}/numaranktest
%{_bindir}/otsutest1
%{_bindir}/otsutest2
%{_bindir}/overlap_reg
%{_bindir}/pagesegtest1
%{_bindir}/pagesegtest2
%{_bindir}/paint_reg
%{_bindir}/paintmask_reg
%{_bindir}/partitiontest
%{_bindir}/pdfiotest
%{_bindir}/pdfseg_reg
%{_bindir}/pixa1_reg
%{_bindir}/pixa2_reg
%{_bindir}/pixaatest
%{_bindir}/pixadisp_reg
%{_bindir}/pixalloc_reg
%{_bindir}/pixcomp_reg
%{_bindir}/pixmem_reg
%{_bindir}/pixserial_reg
%{_bindir}/pixtile_reg
%{_bindir}/plottest
%{_bindir}/pngio_reg
%{_bindir}/printimage
%{_bindir}/printsplitimage
%{_bindir}/printtiff
%{_bindir}/projection_reg
%{_bindir}/projective_reg
%{_bindir}/psio_reg
%{_bindir}/psioseg_reg
%{_bindir}/pta_reg
%{_bindir}/ptra1_reg
%{_bindir}/ptra2_reg
%{_bindir}/quadtreetest
%{_bindir}/rank_reg
%{_bindir}/rankbin_reg
%{_bindir}/rankhisto_reg
%{_bindir}/ranktest
%{_bindir}/rasterop_reg
%{_bindir}/rasteropip_reg
%{_bindir}/reducetest
%{_bindir}/removecmap
%{_bindir}/renderfonts
%{_bindir}/rotate1_reg
%{_bindir}/rotate2_reg
%{_bindir}/rotatefastalt
%{_bindir}/rotateorth_reg
%{_bindir}/rotateorthtest1
%{_bindir}/rotatetest1
%{_bindir}/runlengthtest
%{_bindir}/scale_reg
%{_bindir}/scaleandtile
%{_bindir}/scaletest1
%{_bindir}/scaletest2
%{_bindir}/seedfilltest
%{_bindir}/seedspread_reg
%{_bindir}/selio_reg
%{_bindir}/sharptest
%{_bindir}/shear2_reg
%{_bindir}/shear_reg
%{_bindir}/sheartest
%{_bindir}/showedges
%{_bindir}/skew_reg
%{_bindir}/skewtest
%{_bindir}/smallpix_reg
%{_bindir}/smoothedge_reg
%{_bindir}/snapcolortest
%{_bindir}/sorttest
%{_bindir}/splitcomp_reg
%{_bindir}/splitimage2pdf
%{_bindir}/string_reg
%{_bindir}/subpixel_reg
%{_bindir}/sudokutest
%{_bindir}/textlinemask
%{_bindir}/threshnorm_reg
%{_bindir}/translate_reg
%{_bindir}/trctest
%{_bindir}/viewertest
%{_bindir}/warper_reg
%{_bindir}/warpertest
%{_bindir}/watershedtest
%{_bindir}/wordsinorder
%{_bindir}/writemtiff
%{_bindir}/writetext_reg
%{_bindir}/xformbox_reg
%{_bindir}/xtractprotos
%{_bindir}/xvdisp
%{_bindir}/yuvtest
%{_includedir}/leptonica/*.h
%{_libdir}/liblept.*a
%{_libdir}/liblept.so*

%clean
rm -rf %{buildroot}

%changelog
* Thu Dec 19 2013 tanggeliang <tanggeliang@gmail.com>
- create
