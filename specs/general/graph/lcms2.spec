Name:       lcms2
Version:    2.6
Release:    1%{?dist}
Summary:    lcms2

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.littlecms.com
Source:     http://www.littlecms.com/download/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  libjpeg-turbo libtiff

%description
The Little Color Management System is a small-footprint color management engine, with special focus on accuracy and performance.
It uses the International Color Consortium standard (ICC), which is the modern standard for color management.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --disable-static \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/jpgicc
%{_bindir}/linkicc
%{_bindir}/psicc
%{_bindir}/tificc
%{_bindir}/transicc
%{_libdir}/liblcms2.so
%{_includedir}/lcms2*.h
%{_libdir}/liblcms2.*
%{_libdir}/pkgconfig/lcms2.pc

%files doc
%defattr(-,root,root,-)
%{_mandir}/*

%clean
rm -rf %{buildroot}

%changelog
* Sat Mar 28 2015 tanggeliang <tanggeliang@gmail.com>
- update from 2.5 to 2.6
* Fri Oct 4 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.4 to 2.5
* Wed Nov 7 2012 tanggeliang <tanggeliang@gmail.com>
- update from 2.3 to 2.4
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
