Name:       graphviz
Version:    2.38.0
Release:    1%{?dist}
Summary:    Graphviz - Graph Visualization Software

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.graphviz.org
Source:     http://www.graphviz.org/pub/graphviz/stable/SOURCES/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  expat freetype fontconfig freeglut gdk-pixbuf libjpeg-turbo libpng librsvg pango libx11 libxpm libxaw libxt poppler mesa
BuildRequires:  gtk2 guile tcl tk
BuildRequires:  autoconf automake m4 bison flex libtool pkg-config

%description
The Graphviz package contains graph visualization software.
Graph visualization is a way of representing structural information as diagrams of abstract graphs and networks.
Graphviz has several main graph layout programs. It also has web and interactive graphical interfaces, auxiliary tools, libraries, and language bindings.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/acyclic
%{_bindir}/bcomps
%{_bindir}/ccomps
%{_bindir}/circo
%{_bindir}/cluster
%{_bindir}/dijkstra
%{_bindir}/dot
%{_bindir}/dot2gxl
%{_bindir}/dot_builtins
%{_bindir}/dotty
%{_bindir}/edgepaint
%{_bindir}/fdp
%{_bindir}/gc
%{_bindir}/gml2gv
%{_bindir}/graphml2gv
%{_bindir}/gv2gml
%{_bindir}/gv2gxl
%{_bindir}/gvcolor
%{_bindir}/gvgen
%{_bindir}/gvmap
%{_bindir}/gvmap.sh
%{_bindir}/gvpack
%{_bindir}/gvpr
%{_bindir}/gxl2dot
%{_bindir}/gxl2gv
%{_bindir}/lefty
%{_bindir}/lneato
%{_bindir}/mm2gv
%{_bindir}/neato
%{_bindir}/nop
%{_bindir}/osage
%{_bindir}/patchwork
%{_bindir}/prune
%{_bindir}/sccmap
%{_bindir}/sfdp
%{_bindir}/tred
%{_bindir}/twopi
%{_bindir}/unflatten
%{_bindir}/vimdot
%{_includedir}/graphviz/*.h
%{_includedir}/graphviz/gv.cpp
%{_includedir}/graphviz/gv.i
%{_libdir}/graphviz/libgvplugin_core.*
%{_libdir}/graphviz/libgvplugin_dot_layout.*
%{_libdir}/graphviz/libgvplugin_gdk.*
%{_libdir}/graphviz/libgvplugin_gtk.*
%{_libdir}/graphviz/libgvplugin_neato_layout.*
%{_libdir}/graphviz/libgvplugin_pango.*
%{_libdir}/graphviz/libgvplugin_poppler.*
%{_libdir}/graphviz/libgvplugin_rsvg.*
%{_libdir}/graphviz/libgvplugin_xlib.*
%{_libdir}/graphviz/tcl/libtcldot.*
%{_libdir}/graphviz/tcl/libtcldot_builtin.*
%{_libdir}/graphviz/tcl/libtclplan.*
%{_libdir}/graphviz/tcl/libtkspline.*
%{_libdir}/graphviz/tcl/pkgIndex.tcl
%{_libdir}/libcdt.*
%{_libdir}/libcgraph.*
%{_libdir}/libgvc.*
%{_libdir}/libgvpr.*
%{_libdir}/libpathplan.*
%{_libdir}/libxdot.*
%{_libdir}/pkgconfig/libcdt.pc
%{_libdir}/pkgconfig/libcgraph.pc
%{_libdir}/pkgconfig/libgvc.pc
%{_libdir}/pkgconfig/libgvpr.pc
%{_libdir}/pkgconfig/libpathplan.pc
%{_libdir}/pkgconfig/libxdot.pc
%{_libdir}/tcl8.6/graphviz
%{_datadir}/graphviz/demo/*
%{_datadir}/graphviz/graphs/directed/*.gv
%{_datadir}/graphviz/graphs/undirected/*.gv
%{_datadir}/graphviz/gvpr/*
%{_datadir}/graphviz/lefty/*

%files doc
%defattr(-,root,root,-)
%{_datadir}/graphviz/doc/*
%{_mandir}/man*/*.gz

%clean
rm -rf %{buildroot}

%changelog
* Sat Sep 26 2015 tanggeliang <tanggeliang@gmail.com>
- update from 2.36.0 to 2.38.0
* Thu Mar 20 2014 tanggeliang <tanggeliang@gmail.com>
- update from 2.34.0 to 2.36.0
* Tue Jan 7 2014 tanggeliang <tanggeliang@gmail.com>
- create
