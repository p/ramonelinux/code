Name:       openjpeg
Version:    1.5.2
Release:    1%{?dist}
Summary:    OpenJPEG

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://openjpeg.googlecode.com
Source:     http://openjpeg.googlecode.com/files/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  pkg-config
BuildRequires:  libtool automake autoconf m4

%description
OpenJPEG is an open-source implementation of the JPEG-2000 standard. OpenJPEG fully respects the JPEG-2000 specifications and can compress/decompress lossless 16-bit images.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
autoreconf -f -i &&
./configure --prefix=/usr --disable-static \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/image_to_j2k
%{_bindir}/j2k_dump
%{_bindir}/j2k_to_image
%{_includedir}/openjpeg-1.5/openjpeg.h
%{_libdir}/libopenjpeg.*
%{_libdir}/pkgconfig/libopenjpeg.pc
%{_libdir}/pkgconfig/libopenjpeg1.pc

%files doc
%defattr(-,root,root,-)
%{_docdir}/openjpeg-1.5/*
%{_mandir}/man1/*.gz
%{_mandir}/man3/*.gz

%clean
rm -rf %{buildroot}

%changelog
* Sat Jul 26 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.5.1 to 1.5.2
* Fri Nov 2 2012 tanggeliang <tanggeliang@gmail.com>
- update from 1.5.0 to 1.5.1
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
