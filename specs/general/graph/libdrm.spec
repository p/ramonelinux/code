Name:       libdrm
Version:    2.4.64
Release:    1%{?dist}
Summary:    libdrm

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://xorg.freedesktop.org
Source:     http://dri.freedesktop.org/libdrm/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  libx11 docbook-xml docbook-xsl libxslt
BuildRequires:  libpciaccess
BuildRequires:  systemd util-macros
BuildRequires:  autoconf automake m4 libtool

%description
libdrm provides core library routines for the X Window System to directly interface with video hardware using the Linux kernel's Direct Rendering Manager (DRM).

%prep
%setup -q -n %{name}-%{version}

%build
sed -e "/pthread-stubs/d" \
    -i configure.ac &&
autoreconf -fiv     &&

./configure --prefix=/usr \
            --enable-udev \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_includedir}/libdrm/*.h
%{_includedir}/libkms/libkms.h
%{_includedir}/xf86drm*.h
%{_libdir}/libdrm.*
%{_libdir}/libdrm_amdgpu.*
%{_libdir}/libdrm_intel.*
%{_libdir}/libdrm_nouveau.*
%{_libdir}/libdrm_radeon.*
%{_libdir}/libkms.*
%{_libdir}/pkgconfig/libdrm.pc
%{_libdir}/pkgconfig/libdrm_amdgpu.pc
%{_libdir}/pkgconfig/libdrm_intel.pc
%{_libdir}/pkgconfig/libdrm_nouveau.pc
%{_libdir}/pkgconfig/libdrm_radeon.pc
%{_libdir}/pkgconfig/libkms.pc
%{_mandir}/man3/drm*.3.gz
%{_mandir}/man7/drm*.7.gz

%clean
rm -rf %{buildroot}

%changelog
* Mon Sep 14 2015 tanggeliang <tanggeliang@gmail.com>
- update from 2.4.61 to 2.4.64
* Sat Jun 27 2015 tanggeliang <tanggeliang@gmail.com>
- update from 2.4.54 to 2.4.61
* Wed Jul 23 2014 tanggeliang <tanggeliang@gmail.com>
- update from 2.4.52 to 2.4.54
* Tue Mar 18 2014 tanggeliang <tanggeliang@gmail.com>
- update from 2.4.50 to 2.4.52
* Sat Dec 7 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.4.49 to 2.4.50
* Tue Dec 3 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.4.47 to 2.4.49
* Tue Oct 22 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.4.46 to 2.4.47
* Mon Jul 29 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.4.42 to 2.4.46
* Thu Mar 14 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.4.40 to 2.4.42
* Sat Dec 1 2012 tanggeliang <tanggeliang@gmail.com>
- update from 2.4.39 to 2.4.40
* Wed Oct 10 2012 tanggeliang <tanggeliang@gmail.com>
- update from 2.4.33 to 2.4.39
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
