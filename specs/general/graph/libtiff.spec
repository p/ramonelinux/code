Name:       libtiff
Version:    4.0.3
Release:    2%{?dist}
Summary:    LibTIFF

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://libtiff.org
Source:     http://download.osgeo.org/libtiff/tiff-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build

%description
The LibTIFF package contains the TIFF libraries and associated utilities.
The libraries are used by many programs for reading and writing TIFF files and the utilities are useful for general work with TIFF files.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n tiff-%{version}

%build
./configure --prefix=/usr --disable-static \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/bmp2tiff
%{_bindir}/fax2ps
%{_bindir}/fax2tiff
%{_bindir}/gif2tiff
%{_bindir}/pal2rgb
%{_bindir}/ppm2tiff
%{_bindir}/ras2tiff
%{_bindir}/raw2tiff
%{_bindir}/rgb2ycbcr
%{_bindir}/thumbnail
%{_bindir}/tiff2bw
%{_bindir}/tiff2pdf
%{_bindir}/tiff2ps
%{_bindir}/tiff2rgba
%{_bindir}/tiffcmp
%{_bindir}/tiffcp
%{_bindir}/tiffcrop
%{_bindir}/tiffdither
%{_bindir}/tiffdump
%{_bindir}/tiffinfo
%{_bindir}/tiffmedian
%{_bindir}/tiffset
%{_bindir}/tiffsplit
%{_includedir}/tiff*.h*
%{_libdir}/libtiff*.*
%{_libdir}/pkgconfig/libtiff-4.pc

%files doc
%defattr(-,root,root,-)
%{_docdir}/tiff-4.0.3/*
%{_mandir}/man*/*.gz

%clean
rm -rf %{buildroot}

%changelog
* Sun Oct 14 2012 tanggeliang <tanggeliang@gmail.com>
- update from 4.0.2 to 4.0.3
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
