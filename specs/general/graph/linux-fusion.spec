%define _kver 3.11.3

Name:       linux-fusion
Version:    9.0.2
Release:    2%{?dist}
Summary:    Linux Fusion

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://http://directfb.org
Source:     http://directfb.org/downloads/Core/%{name}/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  linux linux-devel
Requires:  kmod

%description

%package        header
Summary:        header
BuildArch:      noarch
%description    header

%prep
%setup -q -n %{name}-%{version}

%build
make INSTALL_MOD_PATH=%{buildroot} KERNELDIR=/lib/modules/`uname -r`/build modules %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make INSTALL_MOD_PATH=%{buildroot} KERNELDIR=/lib/modules/`uname -r`/build modules_install INSTALL_MOD_STRIP=1

rm -rf %{buildroot}/lib/modules/`uname -r`/modules.*

mkdir -p %{buildroot}/%{_includedir}
cp -a linux/include/* %{buildroot}/%{_includedir}

%files
%defattr(-,root,root,-)
/lib/modules/%{_kver}/drivers/char/fusion/fusion.ko
/lib/modules/%{_kver}/drivers/char/fusion/linux-one.ko

%files header
%defattr(-,root,root,-)
%{_includedir}/linux/fusion.h

%post
modprobe fusion

%clean
rm -rf %{buildroot}

%changelog
* Tue Jul 16 2013 tanggeliang <tanggeliang@gmail.com>
- update from 9.0.0 to 9.0.2
* Thu Jun 13 2013 tanggeliang <tanggeliang@gmail.com>
- create
