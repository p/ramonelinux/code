Name:       libpng
Version:    1.6.18
Release:    1%{?dist}
Summary:    libpng

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://libpng.sourceforge.net
Source0:    http://downloads.sourceforge.net/libpng/%{name}-%{version}.tar.xz
Source1:    libpng-%{version}-apng.patch.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build

%description
The libpng package contains libraries used by other programs for reading and writing PNG files.
The PNG format was designed as a replacement for GIF and, to a lesser extent, TIFF, with many improvements and extensions and lack of patent problems.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
gzip -cd %SOURCE1 | patch -p1
./configure --prefix=/usr --disable-static \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install &&
mkdir -pv %{buildroot}/usr/share/doc/libpng-%{version} &&
cp README libpng-manual.txt %{buildroot}/usr/share/doc/libpng-%{version}

%files
%defattr(-,root,root,-)
%{_bindir}/libpng*-config
%{_bindir}/png-fix-itxt
%{_bindir}/pngfix
%{_includedir}/*
%{_libdir}/libpng*.*
%{_libdir}/pkgconfig/libpng*.pc

%files doc
%defattr(-,root,root,-)
%{_docdir}/libpng-%{version}/*
%{_mandir}/*/*

%clean
rm -rf %{buildroot}

%changelog
* Sat Sep 26 2015 tanggeliang <tanggeliang@gmail.com>
- update from 1.6.12 to 1.6.18
* Sat Jul 26 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.6.10 to 1.6.12
* Thu Mar 20 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.6.8 to 1.6.10
* Sun Jan 5 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.6.7 to 1.6.8
* Tue Dec 3 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.6.6 to 1.6.7
* Wed Oct 2 2013 tanggeliang <tanggeliang@gamil.com>
- update from 1.6.3 to 1.6.6
* Sun Aug 18 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.6.2 to 1.6.3
* Wed Jul 24 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.5.14 to 1.6.2
* Sun Mar 17 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.5.13 to 1.5.14
* Fri Oct 12 2012 tanggeliang <tanggeliang@gmail.com>
- update from 1.5.12 to 1.5.13
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
