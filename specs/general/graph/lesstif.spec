Name:       lesstif
Version:    0.95.2
Release:    1%{?dist}
Summary:    LessTif

Group:      System Environment/libraries
License:    GPLv2+
Url:        www.lesstif.org
Source:     http://downloads.sourceforge.net/lesstif/%{name}-%{version}.tar.bz2
Patch:      lesstif-0.95.2-testsuite_fix-1.patch

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  libx11

%description
LessTif is the Hungry Programmers' version of OSF/Motif.
It aims to be source compatible meaning that the same source code should compile with both and work exactly the same!

%prep
%setup -q -n %{name}-%{version}
%patch -p1

%build
./configure --prefix=/usr \
            --sysconfdir=/etc/X11 \
            --disable-debug \
            --enable-production \
            --with-xdnd \
            --x-includes=/usr/include \
            --x-libraries=/usr/lib &&
sed -i "s@libdir)/X11/mwm@sysconfdir)/mwm@" clients/Motif-2.1/mwm/Makefile &&
make rootdir=/usr/share/doc/lesstif-0.95.2

%check

%install
rm -rf %{buildroot}
make rootdir=/usr/share/doc/lesstif-0.95.2 DESTDIR=%{buildroot} install

ln -v -sf /etc/X11/mwm %{buildroot}/usr/lib/X11

%files
%defattr(-,root,root,-)
/etc/X11/mwm/*
%{_bindir}/motif-config
%{_bindir}/mwm
%{_bindir}/mxmkmf
%{_bindir}/uil
%{_bindir}/xmbind
%{_includedir}/*
%{_libdir}/LessTif/config/*
%{_libdir}/libDtPrint.*
%{_libdir}/libMrm.*
%{_libdir}/libUil.*
%{_libdir}/libXm.*
%{_libdir}/X11/app-defaults/Mwm
%{_libdir}/X11/mwm
%{_datadir}/aclocal/*
%{_docdir}/*/*
%{_mandir}/*/*

%clean
rm -rf %{buildroot}

%changelog
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
