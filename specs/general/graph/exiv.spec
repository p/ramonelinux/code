Name:       exiv
Version:    0.23
Release:    6%{?dist}
Summary:    Exiv2

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.exiv2.org
Source:     http://www.exiv2.org/%{name}2-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  expat
%ifarch %{ix86}
Provides:       libexiv2.so.12
%endif
%ifarch x86_64
Provides:       libexiv2.so.12()(64bit)
%endif

%description
Exiv2 is a C++ library and a command line utility for managing image metadata.

%prep
%setup -q -n %{name}2-%{version}

%build
./configure --prefix=/usr --disable-static \
            --libdir=%{_libdir} &&
make  %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%doc
%{_bindir}/exiv2
%{_includedir}/exiv2/*.hpp
%{_includedir}/exiv2/exv_conf.h
%{_libdir}/libexiv2.la
%{_libdir}/libexiv2.so*
%{_libdir}/pkgconfig/exiv2.pc
%{_datadir}/locale/*/LC_MESSAGES/exiv2.mo
%{_mandir}/man1/exiv2.1.gz

%clean
rm -rf %{buildroot}

%changelog
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
