Name:       jasper
Version:    1.900.1
Release:    7%{?dist}
Summary:    JasPer

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://jasper.sourceforge.net
Source:     http://www.ece.uvic.ca/~mdadams/jasper/software/%{name}-%{version}.zip
Patch:      jasper-1.900.1-security_fixes-1.patch

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  unzip
BuildRequires:  libjpeg-turbo libx11 freeglut

%description
The JasPer Project is an open-source initiative to provide a free software-based reference implementation of the JPEG-2000 codec.

%prep
%setup -q -n %{name}-%{version}
%patch -p1

%build
./configure --prefix=/usr --enable-shared \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

install -v -m755 -d %{buildroot}/usr/share/doc/jasper-1.900.1 &&
install -v -m644 doc/*.pdf %{buildroot}/usr/share/doc/jasper-1.900.1

%files
%defattr(-,root,root,-)
%{_bindir}/imgcmp
%{_bindir}/imginfo
%{_bindir}/jasper
%{_bindir}/tmrdemo
%{_includedir}/jasper/jas_*.h
%{_includedir}/jasper/jasper.h
%{_libdir}/libjasper.*a
%{_libdir}/libjasper.so*
%{_docdir}/jasper-1.900.1/*.pdf
/usr/man/man1/*.1.gz

%clean
rm -rf %{buildroot}

%changelog
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
