Name:       imlib2
Version:    1.4.6
Release:    1%{?dist}
Summary:    Imlib2

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://enlightenment.sourceforge.net
Source:     http://downloads.sourceforge.net/enlightenment/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  freetype libpng libjpeg-turbo
BuildRequires:  libx11 xproto libxext libice libtiff giflib libid3tag

%description
Imlib2 is a graphics library for fast file loading, saving, rendering and manipulation. 

%prep
%setup -q -n %{name}-%{version}

%build
sed -i '/DGifOpen/s:fd:&, NULL:' src/modules/loaders/loader_gif.c &&
sed -i 's/@my_libs@//' imlib2-config.in &&
./configure --prefix=/usr --disable-static \
            --libdir=%{_libdir} &&
make  %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot} &&
install -v -m755 -d %{buildroot}/usr/share/doc/imlib2-%{version} &&
install -v -m644    doc/{*.gif,index.html} \
                    %{buildroot}/usr/share/doc/imlib2-%{version}

%files
%defattr(-,root,root,-)
%{_bindir}/imlib2-config
%{_bindir}/imlib2_bumpmap
%{_bindir}/imlib2_colorspace
%{_bindir}/imlib2_conv
%{_bindir}/imlib2_grab
%{_bindir}/imlib2_poly
%{_bindir}/imlib2_show
%{_bindir}/imlib2_test
%{_bindir}/imlib2_view
%{_includedir}/Imlib2.h
%{_libdir}/imlib2/filters/bumpmap.la
%{_libdir}/imlib2/filters/bumpmap.so
%{_libdir}/imlib2/filters/colormod.la
%{_libdir}/imlib2/filters/colormod.so
%{_libdir}/imlib2/filters/testfilter.la
%{_libdir}/imlib2/filters/testfilter.so
%{_libdir}/imlib2/loaders/argb.la
%{_libdir}/imlib2/loaders/argb.so
%{_libdir}/imlib2/loaders/bmp.la
%{_libdir}/imlib2/loaders/bmp.so
%{_libdir}/imlib2/loaders/bz2.la
%{_libdir}/imlib2/loaders/bz2.so
%{_libdir}/imlib2/loaders/gif.la
%{_libdir}/imlib2/loaders/gif.so
%{_libdir}/imlib2/loaders/id3.la
%{_libdir}/imlib2/loaders/id3.so
%{_libdir}/imlib2/loaders/jpeg.la
%{_libdir}/imlib2/loaders/jpeg.so
%{_libdir}/imlib2/loaders/lbm.la
%{_libdir}/imlib2/loaders/lbm.so
%{_libdir}/imlib2/loaders/png.la
%{_libdir}/imlib2/loaders/png.so
%{_libdir}/imlib2/loaders/pnm.la
%{_libdir}/imlib2/loaders/pnm.so
%{_libdir}/imlib2/loaders/tga.la
%{_libdir}/imlib2/loaders/tga.so
%{_libdir}/imlib2/loaders/tiff.la
%{_libdir}/imlib2/loaders/tiff.so
%{_libdir}/imlib2/loaders/xpm.la
%{_libdir}/imlib2/loaders/xpm.so
%{_libdir}/imlib2/loaders/zlib.la
%{_libdir}/imlib2/loaders/zlib.so
%{_libdir}/libImlib2.*a
%{_libdir}/libImlib2.so*
%{_libdir}/pkgconfig/imlib2.pc
%{_datadir}/imlib2/data/fonts/*.ttf
%{_datadir}/imlib2/data/images/*.png
%{_docdir}/imlib2-%{version}/*

%clean
rm -rf %{buildroot}

%changelog
* Sat Mar 22 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.4.5 to 1.4.6
* Wed Aug 14 2013 tanggeliang <tanggeliang@gmail.com>
- fix DGifOpenFileHandle() from giflib4 to giflib5.
* Mon Apr 15 2013 tanggeliang <tanggeliang@gmail.com>
- create
