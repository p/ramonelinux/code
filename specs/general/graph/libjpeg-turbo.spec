Name:       libjpeg-turbo
Version:    1.4.1
Release:    1%{?dist}
Summary:    libjpeg-turbo

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://libjpeg-turbo.sourceforge.net
Source:     http://downloads.sourceforge.net/libjpeg-turbo/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  nasm

%description
libjpeg-turbo is a fork of the original IJG libjpeg which uses SIMD to accelerate baseline JPEG compression and decompression.
libjpeg is a library that implements JPEG image encoding, decoding and transcoding.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
sed -i -e '/^docdir/ s:$:/libjpeg-turbo-%{version}:' Makefile.in &&

./configure --prefix=/usr \
            --mandir=/usr/share/man \
            --with-jpeg8 \
            --disable-static \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

%files
%defattr(-,root,root,-)
%{_bindir}/cjpeg
%{_bindir}/djpeg
%{_bindir}/jpegtran
%{_bindir}/rdjpgcom
%{_bindir}/tjbench
%{_bindir}/wrjpgcom
%{_includedir}/jconfig.h
%{_includedir}/jerror.h
%{_includedir}/jmorecfg.h
%{_includedir}/jpeglib.h
%{_includedir}/turbojpeg.h
%{_libdir}/libjpeg.*
%{_libdir}/libturbojpeg.*

%files doc
%{_mandir}/man1/*.gz
%{_docdir}/libjpeg-turbo-%{version}/*

%clean
rm -rf %{buildroot}

%changelog
* Sat Sep 26 2015 tanggeliang <tanggeliang@gmail.com>
- update from 1.3.1 to 1.4.1
* Sun Jul 27 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.3.0 to 1.3.1
* Wed Jun 19 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.2.1 to 1.3.0
* Sun Mar 17 2013 tanggeliang <tanggeliang@gmail.com>
- update from libjpeg to libjpeg-turbo
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
