Name:       libexif
Version:    0.6.21
Release:    7%{?dist}
Summary:    libexif

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://libexif.sourceforge.net
Source:     http://downloads.sourceforge.net/libexif/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build

%description
The libexif package contains a library for parsing, editing, and saving EXIF data. Most digital cameras produce EXIF files, which are JPEG files with extra tags that contain information about the image.
All EXIF tags described in EXIF standard 2.1 are supported.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --with-doc-dir=%{_docdir}/libexif-%{version} \
            --disable-static \
            --libdir=%{_libdir} &&
make  %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_includedir}/libexif/_stdint.h
%{_includedir}/libexif/exif-*.h
%{_libdir}/libexif.la
%{_libdir}/libexif.so*
%{_libdir}/pkgconfig/libexif.pc
%{_datadir}/locale/*/LC_MESSAGES/libexif-12.mo

%files doc
%defattr(-,root,root,-)
%{_docdir}/libexif-%{version}/*

%clean
rm -rf %{buildroot}

%changelog
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
