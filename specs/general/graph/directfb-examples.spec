Name:       directfb-examples
Version:    1.7.0
Release:    1%{?dist}
Summary:    DirectFB examples

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://http://directfb.org
Source:     http://directfb.org/downloads/Extras/DirectFB-examples-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  directfb

%description
DirectFB is a thin library that provides hardware graphics acceleration, input device handling and abstraction, integrated windowing system with support for translucent windows and multiple display layers, not only on top of the Linux Framebuffer Device.
It is a complete hardware abstraction layer with software fallbacks for every graphics operation that is not supported by the underlying hardware.
DirectFB adds graphical power to embedded systems and sets a new standard for graphics under Linux. 

%prep
%setup -q -n DirectFB-examples-%{version}

%build
./configure --prefix=/usr &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/df_andi
%{_bindir}/df_andi3d
%{_bindir}/df_bltload
%{_bindir}/df_cpuload
%{_bindir}/df_databuffer
%{_bindir}/df_dioload
%{_bindir}/df_dok
%{_bindir}/df_drivertest
%{_bindir}/df_drivertest2
%{_bindir}/df_fire
%{_bindir}/df_flip
%{_bindir}/df_fonts
%{_bindir}/df_input
%{_bindir}/df_joystick
%{_bindir}/df_knuckles
%{_bindir}/df_layer
%{_bindir}/df_matrix
%{_bindir}/df_matrix_water
%{_bindir}/df_neo
%{_bindir}/df_netload
%{_bindir}/df_palette
%{_bindir}/df_particle
%{_bindir}/df_porter
%{_bindir}/df_stereo3d
%{_bindir}/df_stress
%{_bindir}/df_texture
%{_bindir}/df_texture3d
%{_bindir}/df_video
%{_bindir}/df_video_particle
%{_bindir}/df_window
%{_bindir}/pss
%{_bindir}/spacedream
%{_datadir}/directfb-examples/*.png
%{_datadir}/directfb-examples/*.jpg
%{_datadir}/directfb-examples/*.gif
%{_datadir}/directfb-examples/melted_rgb*.dfiff
%{_datadir}/directfb-examples/fonts/decker.*
%{_datadir}/directfb-examples/spacedream/*.png

%clean
rm -rf %{buildroot}

%changelog
* Tue Jul 16 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.6.0 to 1.7.0
* Sat Jan 26 2013 tanggeliang <tanggeliang@gmail.com>
- create
