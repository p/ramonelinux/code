%define _ver1 1.7
%define _ver2 1

Name:       directfb
Version:    %{_ver1}.%{_ver2}
Release:    1%{?dist}
Summary:    DirectFB

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://http://directfb.org
Source:     http://directfb.org/downloads/Core/DirectFB-%{_ver1}/DirectFB-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  libpng libjpeg-turbo openjpeg libmng giflib
BuildRequires:  freetype
#BuildRequires:  linux-fusion-header
#Requires:       linux-fusion

%description
DirectFB is a thin library that provides hardware graphics acceleration, input device handling and abstraction, integrated windowing system with support for translucent windows and multiple display layers, not only on top of the Linux Framebuffer Device.
It is a complete hardware abstraction layer with software fallbacks for every graphics operation that is not supported by the underlying hardware.
DirectFB adds graphical power to embedded systems and sets a new standard for graphics under Linux. 

%prep
%setup -q -n DirectFB-%{version}

%build
./configure --prefix=/usr \
            --sysconfdir=/etc \
            --disable-multi \
            --disable-multicore \
            --disable-multi-kernel \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/dfbdump
%{_bindir}/dfbdumpinput
%{_bindir}/dfbfx
%{_bindir}/dfbg
%{_bindir}/dfbinfo
%{_bindir}/dfbinput
%{_bindir}/dfbinspector
%{_bindir}/dfblayer
%{_bindir}/dfbmaster
%{_bindir}/dfbpenmount
%{_bindir}/dfbplay
%{_bindir}/dfbscreen
%{_bindir}/dfbshow
%{_bindir}/dfbswitch
%{_bindir}/directfb-config
%{_bindir}/directfb-csource
%{_bindir}/mkdfiff
%{_bindir}/mkdgiff
%{_bindir}/mkdgifft
%{_bindir}/pxa3xx_dump
%{_includedir}/++dfb/*.h
%{_includedir}/directfb-internal/*
%{_includedir}/directfb/*
%{_libdir}/directfb-%{_ver1}-%{_ver2}/gfxdrivers/libdirectfb_*.la
%{_libdir}/directfb-%{_ver1}-%{_ver2}/gfxdrivers/libdirectfb_*.so
%{_libdir}/directfb-%{_ver1}-%{_ver2}/inputdrivers/libdirectfb_*.la
%{_libdir}/directfb-%{_ver1}-%{_ver2}/inputdrivers/libdirectfb_*.so
%{_libdir}/directfb-%{_ver1}-%{_ver2}/interfaces/*
%{_libdir}/directfb-%{_ver1}-%{_ver2}/systems/libdirectfb_*.la
%{_libdir}/directfb-%{_ver1}-%{_ver2}/systems/libdirectfb_*.so
%{_libdir}/directfb-%{_ver1}-%{_ver2}/wm/libdirectfbwm_default.la
%{_libdir}/directfb-%{_ver1}-%{_ver2}/wm/libdirectfbwm_default.so
%{_libdir}/lib++dfb-%{_ver1}.*
%{_libdir}/lib++dfb.la
%{_libdir}/lib++dfb.so
%{_libdir}/libdirect-%{_ver1}.so.%{_ver2}*
%{_libdir}/libdirect.la
%{_libdir}/libdirect.so
%{_libdir}/libdirectfb-%{_ver1}.so.%{_ver2}*
%{_libdir}/libdirectfb.la
%{_libdir}/libdirectfb.so
%{_libdir}/libfusion-%{_ver1}.so.%{_ver2}*
%{_libdir}/libfusion.la
%{_libdir}/libfusion.so
%{_libdir}/pkgconfig/++dfb.pc
%{_libdir}/pkgconfig/direct.pc
%{_libdir}/pkgconfig/directfb-internal.pc
%{_libdir}/pkgconfig/directfb.pc
%{_libdir}/pkgconfig/fusion.pc
%{_datadir}/directfb-%{version}/cursor.dat
%{_datadir}/directfb-%{version}/decker.dgiff
%{_datadir}/directfb-%{version}/cursor.png
%{_datadir}/directfb-%{version}/decker.ttf
%{_mandir}/man1/dfbg.1.gz
%{_mandir}/man1/directfb-csource.1.gz
%{_mandir}/man5/directfbrc.5.gz

%clean
rm -rf %{buildroot}

%changelog
* Fri Nov 1 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.7.0 to 1.7.1
* Tue Jul 16 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.6.3 to 1.7.0
* Sat Jan 26 2013 tanggeliang <tanggeliang@gmail.com>
- create
