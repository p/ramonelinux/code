Name:       giflib
Version:    5.1.1
Release:    1%{?dist}
Summary:    giflib

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://giflib.sourceforge.net
Source:     http://downloads.sourceforge.net/giflib/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build

%description
The giflib package contains libraries for reading and writing GIFs as well as programs for converting and working with GIF files.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --disable-static \
            --libdir=%{_libdir} &&
make  %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/gif2rgb
%{_bindir}/gifbuild
%{_bindir}/gifclrmp
%{_bindir}/gifecho
%{_bindir}/giffix
%{_bindir}/gifinto
%{_bindir}/giftext
%{_bindir}/giftool
%{_includedir}/gif_lib.h
%{_libdir}/libgif.*a
%{_libdir}/libgif.so*

%clean
rm -rf %{buildroot}

%changelog
* Sat Mar 28 2015 tanggeliang <tanggeliang@gmail.com>
- update from 5.0.5 to 5.1.1
* Thu Oct 3 2013 tanggeliang <tanggeliang@gmail.com>
- update from 5.0.4 to 5.0.5
* Wed Aug 14 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.2.1 to 5.0.4
* Mon Jul 29 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.1.6 to 4.2.1
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
