Name:       grx
Version:    249
Release:    1%{?dist}
Summary:    GRX graphics library

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://grx.gnu.de/index.html
Source:     grx.gnu.de/download/%{name}%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build

%description
 GRX is a 2D graphics library originaly written by Csaba Biegl for DJ Delorie's DOS port of the GCC compiler.
Now it support DOS (using the DJGPP v2 compiler), Linux console, X11 and Win32 (using the Mingw compiler).
On DOS it supports EGA, VGA and VESA compliant cards. On Linux console it uses svgalib or the framebuffer. On X11 only Linux is directly supported, but it must work on any X11R5 (or later) system after a few changes in a config file. From the 2.4 version, GRX comes with a Win32 driver, from 2.4.6 you can considere it stable. The Linux console framebuffer video driver was new in 2.4.2. From 2.4.7 it supports a SDL driver in Linux-X11 and Mingw32.. 

%prep
%setup -q -n %{name}%{version}

%build
./configure --prefix=/usr \
            --libdir=%{_libdir} \
%ifarch x86_64
            --enable-x86_64 \
%endif
&&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)


%clean
rm -rf %{buildroot}

%changelog
* Wed May 15 2013 tanggeliang <tanggeliang@gmail.com>
- create
