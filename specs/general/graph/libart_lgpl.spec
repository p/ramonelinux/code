Name:       libart_lgpl
Version:    2.3.21
Release:    5%{?dist}
Summary:    libart_lgpl

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/libart_lgpl/2.3/%{name}-%{version}.tar.bz2
Patch:      libart_lgpl-2.3.21-upstream_fixes-1.patch

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build

%description
The libart_lgpl package contains the libart libraries. These are useful for high-performance 2D graphics.

%prep
%setup -q -n %{name}-%{version}
%patch -p1

%build
./configure --prefix=/usr &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/libart2-config
%{_includedir}/libart-2.0/libart_lgpl/*.h
%{_libdir}/libart_lgpl_2.*
%{_libdir}/pkgconfig/libart-2.0.pc

%clean
rm -rf %{buildroot}

%changelog
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
