Name:       tesseract-ocr
Version:    3.02.02
Release:    1%{?dist}
Summary:    tesseract-ocr

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://code.google.com/p/tesseract-ocr
Source0:    http://code.google.com/p/tesseract-ocr/%{name}-%{version}.tar.gz
Source1:    %{name}-3.02.eng.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  leptonica

%description
Tesseract is probably the most accurate open source OCR engine available.

%package        eng
Summary:        English data
BuildArch:      noarch
%description    eng
English language data for Tesseract 3.02.

%prep
%setup -q -n %{name}

%build
./configure --prefix=/usr \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

tar xf %{SOURCE1} -C %{buildroot}/%{_datadir} --no-overwrite-dir --strip-components=1

%files
%defattr(-,root,root,-)
%{_bindir}/ambiguous_words
%{_bindir}/classifier_tester
%{_bindir}/cntraining
%{_bindir}/combine_tessdata
%{_bindir}/dawg2wordlist
%{_bindir}/mftraining
%{_bindir}/shapeclustering
%{_bindir}/tesseract
%{_bindir}/unicharset_extractor
%{_bindir}/wordlist2dawg
%{_includedir}/tesseract/*.h
%{_libdir}/libtesseract.*a
%{_libdir}/libtesseract.so*
%{_libdir}/pkgconfig/tesseract.pc
%{_datadir}/tessdata/configs/*
%{_datadir}/tessdata/tessconfigs/*
%{_mandir}/man1/*.1.gz
%{_mandir}/man5/*.5.gz

%files eng
%defattr(-,root,root,-)
%{_datadir}/tessdata/eng.cube.bigrams
%{_datadir}/tessdata/eng.cube.fold
%{_datadir}/tessdata/eng.cube.lm
%{_datadir}/tessdata/eng.cube.nn
%{_datadir}/tessdata/eng.cube.params
%{_datadir}/tessdata/eng.cube.size
%{_datadir}/tessdata/eng.cube.word-freq
%{_datadir}/tessdata/eng.tesseract_cube.nn
%{_datadir}/tessdata/eng.traineddata

%clean
rm -rf %{buildroot}

%changelog
* Thu Dec 19 2013 tanggeliang <tanggeliang@gmail.com>
- create
