Name:       opencv
Version:    2.4.8
Release:    1%{?dist}
Summary:    OpenCV - Open Source Computer Vision Library

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://opencv.org
Source:     http://sourceforge.net/projects/opencvlibrary/%{name}-%{version}.zip

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  cmake gtk2 libtheora libvorbis libpng libtiff libjpeg-turbo
BuildRequires:  libtool pkg-config jasper v4l-utils-libv4l swig
BuildRequires:  ffmpeg gstreamer gst-plugins-base

%description
OpenCV is released under a BSD license and hence it’s free for both academic and commercial use.
It has C++, C, Python and Java interfaces and supports Windows, Linux, Mac OS, iOS and Android.
OpenCV was designed for computational efficiency and with a strong focus on real-time applications.
Written in optimized C/C++, the library can take advantage of multi-core processing.
Adopted all around the world, OpenCV has more than 47 thousand people of user community and estimated number of downloads exceeding 6 million.
Usage ranges from interactive art, to mines inspection, stitching maps on the web or through advanced robotics.

%prep
%setup -q -n %{name}-%{version}

%build
mkdir build &&
cd build &&
cmake -DCMAKE_INSTALL_PREFIX=/usr \
      -DCMAKE_PREFIX_PATH=/usr \
      -DCMAKE_BUILD_TYPE=Release \
      -DINSTALL_QSQLITE_IN_QT_PREFIX=TRUE \
%ifarch x86_64
      -DLIB_SUFFIX=64 \
%endif
      .. &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
cd build
make install DESTDIR=%{buildroot}

%files
%defattr(-,root,root,-)
%{_bindir}/opencv_createsamples
%{_bindir}/opencv_haartraining
%{_bindir}/opencv_performance
%{_bindir}/opencv_traincascade
%{_includedir}/opencv/*.h
%{_includedir}/opencv/*.hpp
%{_includedir}/opencv2/*
%{_libdir}/libopencv_calib3d.so*
%{_libdir}/libopencv_contrib.so*
%{_libdir}/libopencv_core.so*
%{_libdir}/libopencv_features2d.so*
%{_libdir}/libopencv_flann.so*
%{_libdir}/libopencv_gpu.so*
%{_libdir}/libopencv_highgui.so*
%{_libdir}/libopencv_imgproc.so*
%{_libdir}/libopencv_legacy.so*
%{_libdir}/libopencv_ml.so*
%{_libdir}/libopencv_nonfree.so*
%{_libdir}/libopencv_objdetect.so*
%{_libdir}/libopencv_ocl.so*
%{_libdir}/libopencv_photo.so*
%{_libdir}/libopencv_stitching.so*
%{_libdir}/libopencv_superres.so*
%{_libdir}/libopencv_ts.a
%{_libdir}/libopencv_video.so*
%{_libdir}/libopencv_videostab.so*
%{_libdir}/pkgconfig/opencv.pc
%{_datadir}/OpenCV/OpenCVConfig-version.cmake
%{_datadir}/OpenCV/OpenCVConfig.cmake
%{_datadir}/OpenCV/OpenCVModules-release.cmake
%{_datadir}/OpenCV/OpenCVModules.cmake
%{_datadir}/OpenCV/haarcascades/haarcascade_*.xml
%{_datadir}/OpenCV/lbpcascades/lbpcascade_*.xml

%clean
rm -rf %{buildroot}

%changelog
* Wed Feb 19 2014 tanggeliang <tanggeliang@gmail.com>
- update from 2.4.7 to 2.4.8
* Sat Dec 21 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.4.6.1 to 2.4.7
* Fri Oct 18 2013 tanggeliang <tanggeliang@gamil.com>
- create
