Name:       imagemagick
Version:    6.8.8
Release:    1%{?dist}
Summary:    ImageMagick

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.imagemagick.org
Source:     ftp://ftp.imagemagick.org/pub/ImageMagick/ImageMagick-%{version}-9.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  libx11
BuildRequires:  cups freetype jasper lcms2 libexif libjpeg-turbo libpng librsvg libtiff pango libxml
BuildRequires:  libtool

%description
ImageMagick is a collection of tools and libraries to read, write, and manipulate an image in various image formats.
Image processing operations are available from the command line.
Bindings to various programming languages are also available.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n ImageMagick-%{version}-9

%build
./configure --prefix=/usr     \
            --sysconfdir=/etc \
            --with-modules    \
            --with-perl       \
            --disable-static  \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

#Conflicts with xml-parser
rm -rf %{buildroot}/%{perl_archlib}/perllocal.pod

%files
%defattr(-,root,root,-)
/etc/ImageMagick-6/*.xml
%{_bindir}/Magick++-config
%{_bindir}/Magick-config
%{_bindir}/MagickCore-config
%{_bindir}/MagickWand-config
%{_bindir}/Wand-config
%{_bindir}/animate
%{_bindir}/compare
%{_bindir}/composite
%{_bindir}/conjure
%{_bindir}/convert
%{_bindir}/display
%{_bindir}/identify
%{_bindir}/import
%{_bindir}/mogrify
%{_bindir}/montage
%{_bindir}/stream
%{_includedir}/ImageMagick-6/Magick++.h
%{_includedir}/ImageMagick-6/Magick++/*.h
%{_includedir}/ImageMagick-6/magick/*.h
%{_includedir}/ImageMagick-6/wand/*.h
%{_libdir}/ImageMagick-%{version}/config-Q16/configure.xml
%{_libdir}/ImageMagick-%{version}/modules-Q16/*
%{_libdir}/libMagick++-6.Q16.*
%{_libdir}/libMagickCore-6.Q16.*
%{_libdir}/libMagickWand-6.Q16.*
%{_libdir}/perl5/Image/Magick/Q16.pm
%{perl_sitearch}/Image/Magick.pm
%{perl_sitearch}/auto/Image/Magick/.packlist
%{perl_sitearch}/auto/Image/Magick/*
%{_libdir}/pkgconfig/ImageMagick*.pc
%{_libdir}/pkgconfig/Magick++*.pc
%{_libdir}/pkgconfig/MagickCore*.pc
%{_libdir}/pkgconfig/MagickWand*.pc
%{_libdir}/pkgconfig/Wand*.pc
%{_datadir}/ImageMagick-6/*.xml

%files doc
%defattr(-,root,root,-)
%{_docdir}/ImageMagick-6/*
%{_mandir}/man*/*.gz

%clean
rm -rf %{buildroot}

%changelog
* Thu Mar 20 2014 tanggeliang <tanggeliang@gmail.com>
- update from 6.8.7-8 to 6.8.8-9
* Tue Dec 3 2013 tanggeliang <tanggeliang@gmail.com>
- update from 6.8.7-0 to 6.8.7-8
* Thu Oct 3 2013 tanggeliang <tanggeliang@gmail.com>
- update from 6.8.6-7 to 6.8.7-0
* Wed Jul 31 2013 tanggeliang <tanggeliang@gmail.com>
- update from 6.8.6-0 to 6.8.6-7
* Wed Jun 19 2013 tanggeliang <tanggeliang@gmail.com>
- update from 6.8.4 to 6.8.6
* Sun Apr 14 2013 tanggeliang <tanggeliang@gmail.com>
- update from 6.8.2 to 6.8.4
* Tue Mar 19 2013 tanggeliang <tanggeliang@gmail.com>
- update from 6.7.8 to 6.8.2
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
