Name:       libmng
Version:    2.0.2
Release:    1%{?dist}
Summary:    libmng

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://libmng.sourceforge.net
Source:     http://downloads.sourceforge.net/libmng/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  libjpeg-turbo lcms

%description
The libmng libraries are used by programs wanting to read and write Multiple-image Network Graphics (MNG) files which are the animation equivalents to PNG files.

%prep
%setup -q -n %{name}-%{version}

%build
sed -i "s:#include <jpeg:#include <stdio.h>\n&:" libmng_types.h &&
./configure --prefix=/usr --disable-static \
            --libdir=%{_libdir} &&
make  %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot} &&
install -v -m755 -d /usr/share/doc/libmng-%{version} &&
install -v -m644 doc/*.txt /usr/share/doc/libmng-%{version}

%files
%defattr(-,root,root,-)
%{_includedir}/libmng*.h
%{_libdir}/libmng.so*
%{_libdir}/libmng.la
%{_libdir}/pkgconfig/libmng.pc
%{_mandir}/man3/libmng.3.gz
%{_mandir}/man5/jng.5.gz
%{_mandir}/man5/mng.5.gz

%clean
rm -rf %{buildroot}

%changelog
* Wed Oct 2 2013 tanggeliang <tanggeliang@gamil.com>
- update from 1.0.10 to 2.0.2
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
