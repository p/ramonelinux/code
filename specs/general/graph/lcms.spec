Name:       lcms
Version:    1.19
Release:    7%{?dist}
Summary:    little cms

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://lcms.sourceforge.net
Source:     http://sourceforge.net/projects/lcms/files/lcms/1.19/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  libtiff libjpeg-turbo

%description
The little cms library is used by other programs to provide color management facilities.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/icc2ps
%{_bindir}/icclink
%{_bindir}/icctrans
%{_bindir}/jpegicc
%{_bindir}/tiffdiff
%{_bindir}/tifficc
%{_bindir}/wtpt
%{_includedir}/icc34.h
%{_includedir}/lcms.h
%{_libdir}/liblcms.*
%{_libdir}/pkgconfig/lcms.pc
%{_mandir}/man1/*.1.gz

%clean
rm -rf %{buildroot}

%changelog
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
