Name:       exempi
Version:    2.2.1
Release:    1%{?dist}
Summary:    Exempi

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.freedesktop.org
Source:     http://libopenraw.freedesktop.org/download/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  boost
BuildRequires:  expat

%description
Exempi is an implementation of XMP (Adobe's Extensible Metadata Platform).

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --disable-static \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/exempi
%{_includedir}/exempi-2.0/exempi/xmp*.h*
%{_libdir}/libexempi.la
%{_libdir}/libexempi.so*
%{_libdir}/pkgconfig/exempi-2.0.pc
%{_mandir}/man1/exempi.1.gz

%clean
rm -rf %{buildroot}

%changelog
* Wed Aug 14 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.2.0 to 2.2.1
* Sat Sep 29 2012 tanggeliang <tanggeliang@gmail.com>
- create
