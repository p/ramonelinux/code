Name:       aspell
Version:    0.60.6.1
Release:    6%{?dist}
Summary:    Aspell

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.gnu.org
Source:     http://ftp.gnu.org/gnu/aspell/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  which

%description
The Aspell package contains an interactive spell checking program and the Aspell libraries.
Aspell can either be used as a library or as an independent spell checker.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install &&

install -v -m755 -d %{buildroot}/%{_docdir}/aspell-%{version}/aspell{,-dev}.html &&
install -v -m644 manual/aspell.html/* \
    %{buildroot}/%{_docdir}/aspell-%{version}/aspell.html &&
install -v -m644 manual/aspell-dev.html/* \
    %{buildroot}/%{_docdir}/aspell-%{version}/aspell-dev.html

install -v -m 755 scripts/ispell %{buildroot}/usr/bin/
install -v -m 755 scripts/spell %{buildroot}/usr/bin/

rm -rf %{buildroot}/%{_infodir}/dir

%files
%defattr(-,root,root,-)
%{_bindir}/aspell
%{_bindir}/aspell-import
%{_bindir}/ispell
%{_bindir}/precat
%{_bindir}/preunzip
%{_bindir}/prezip
%{_bindir}/prezip-bin
%{_bindir}/pspell-config
%{_bindir}/run-with-aspell
%{_bindir}/spell
%{_bindir}/word-list-compress
%{_includedir}/aspell.h
%{_includedir}/pspell/pspell.h
%{_libdir}/aspell-0.60/*
%{_libdir}/libaspell.la
%{_libdir}/libaspell.so*
%{_libdir}/libpspell.la
%{_libdir}/libpspell.so*
%{_datadir}/locale/*/LC_MESSAGES/aspell.mo

%files doc
%defattr(-,root,root,-)
%{_docdir}/aspell-%{version}/aspell-dev.html/*
%{_docdir}/aspell-%{version}/aspell.html/*
%{_infodir}/aspell*.info.gz
%{_mandir}/man1/*.1.gz

%clean
rm -rf %{buildroot}

%changelog
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
