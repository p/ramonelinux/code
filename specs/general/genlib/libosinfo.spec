Name:       libosinfo
Version:    0.2.12
Release:    1%{?dist}
Summary:    libosinfo

Group:      User Interface/GNOME
License:    GPLv2+
Url:        http://www.fedorahosted.org
Source:     https://fedorahosted.org/releases/l/i/libosinfo/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  libsoup glib gobject-introspection vala wget
BuildRequires:	intltool gettext xml-parser libsoup-gnome
BuildRequires:	libxslt check gtk-doc

%description

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/osinfo-db-validate
%{_bindir}/osinfo-detect
%{_bindir}/osinfo-install-script
%{_bindir}/osinfo-query
%{_includedir}/libosinfo-1.0/osinfo/osinfo*.h
%{_libdir}/girepository-1.0/Libosinfo-1.0.typelib
%{_libdir}/libosinfo-1.0.la
%{_libdir}/libosinfo-1.0.so*
%{_libdir}/pkgconfig/libosinfo-1.0.pc
%{_datadir}/gir-1.0/Libosinfo-1.0.gir
%{_datadir}/gtk-doc/html/Libosinfo/*
%{_datadir}/libosinfo/db/*/*.xml
%{_datadir}/libosinfo/db/pci.ids
%{_datadir}/libosinfo/db/usb.ids
%{_datadir}/libosinfo/schemas/libosinfo.rng
%{_datadir}/locale/*/LC_MESSAGES/libosinfo.mo
%{_datadir}/vala/vapi/libosinfo-1.0.vapi
%{_mandir}/man1/osinfo-*.1.gz

%clean
rm -rf %{buildroot}

%changelog
* Wed Sep 23 2015 tanggeliang <tanggeliang@gmail.com>
- create
