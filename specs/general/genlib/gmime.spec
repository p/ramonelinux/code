Name:       gmime
Version:    2.6.20
Release:    1%{?dist}
Summary:    GMime

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/gmime/2.6/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  glib libgpg-error
BuildRequires:  gobject-introspection vala gtk-doc

%description
The GMime package contains a set of utilities for parsing and creating messages using the Multipurpose Internet Mail Extension (MIME) as defined by the applicable RFCs.
See the GMime web site for the RFCs resourced.
This is useful as it provides an API which adheres to the MIME specification as closely as possible while also providing programmers with an extremely easy to use interface to the API functions.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --disable-static \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_includedir}/gmime-2.6/gmime/*.h
%{_libdir}/libgmime-2.6.*
%{_libdir}/pkgconfig/gmime-2.6.pc
%{_libdir}/girepository-1.0/GMime-2.6.typelib
%{_datadir}/gir-1.0/GMime-2.6.gir
%{_datadir}/vala/vapi/gmime-2.6.deps
%{_datadir}/vala/vapi/gmime-2.6.vapi

%files doc
%defattr(-,root,root,-)
%{_datadir}/gtk-doc/html/gmime-2.6/*

%clean
rm -rf %{buildroot}

%changelog
* Tue Mar 18 2014 tanggeliang <tanggeliang@gmail.com>
- update from 2.6.19 to 2.6.20
* Tue Dec 3 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.6.18 to 2.6.19
* Thu Oct 3 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.6.17 to 2.6.18
* Sun Aug 18 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.6.16 to 2.6.17
* Wed Jul 31 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.6.15 to 2.6.16
* Thu May 30 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.6.11 to 2.6.15
* Sun Oct 28 2012 tanggeliang <tanggeliang@gmail.com>
- update from 2.6.10 to 2.6.11
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
