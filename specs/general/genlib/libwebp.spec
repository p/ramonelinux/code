Name:       libwebp
Version:    0.4.0
Release:    1%{?dist}
Summary:    A new image format for the Web

Group:      System Environment/libraries
License:    GPLv2+
Url:        https://webp.googlecode.com
Source:     https://webp.googlecode.com/files/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  libjpeg-turbo libpng libtiff
BuildRequires:  freeglut giflib

%description
The libwebp package contains a library and support programs to encode and decode images in WebP format.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --disable-static \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/cwebp
%{_bindir}/dwebp
%{_includedir}/webp/*.h
%{_libdir}/libwebp.*a
%{_libdir}/libwebp.so*
%{_libdir}/pkgconfig/libwebp.pc

%files doc
%defattr(-,root,root,-)
%{_mandir}/man1/*webp.1.gz

%clean
rm -rf %{buildroot}

%changelog
* Tue Mar 18 2014 tanggeliang <tanggeliang@gmail.com>
- update from 0.3.1 to 0.4.0
* Wed Jul 24 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.3.0 to 0.3.1
* Thu Apr 11 2013 tanggeliang <tanggeliang@gmail.com>
- create
