Name:       libzeitgeist
Version:    0.3.18
Release:    4%{?dist}
Summary:    libzeitgeist

Group:      System Environment/libraries
License:    GPLv2+
Url:        https://launchpad.net/libzeitgeist
Source:     https://launchpad.net/libzeitgeist/0.3/0.3.18/+download/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  glib
BuildRequires:  gtk-doc

%description
The libzeitgeist package contains a client library used to access and manage the Zeitgeist event log from languages such as C and Vala.
Zeitgeist is a service which logs the user's activities and events (files opened, websites visited, conversations hold with other people, etc.) and makes the relevant information available to other applications.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --disable-static \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_includedir}/zeitgeist-1.0/zeitgeist-*.h
%{_includedir}/zeitgeist-1.0/zeitgeist.h
%{_libdir}/libzeitgeist-1.0.la
%{_libdir}/libzeitgeist-1.0.so*
%{_libdir}/pkgconfig/zeitgeist-1.0.pc
%{_datadir}/vala/vapi/zeitgeist-1.0.*

%files doc
%defattr(-,root,root,-)
%{_docdir}/libzeitgeist/*
%{_datadir}/gtk-doc/html/zeitgeist-1.0/*

%clean
rm -rf %{buildroot}

%changelog
* Fri Sep 28 2012 tanggeliang <tanggeliang@gmail.com>
- create
