Name:       enchant
Version:    1.6.0
Release:    6%{?dist}
Summary:    enchant

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.enchant.com
Source:     http://www.abisource.com/downloads/enchant/1.6.0/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  glib aspell dbus-glib

%description
 The enchant package provide a generic interface into various existing spell checking libaries.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/enchant
%{_bindir}/enchant-lsmod
%{_includedir}/enchant/enchant*.h
%{_libdir}/enchant/libenchant_*spell.*
%{_libdir}/libenchant.*a
%{_libdir}/libenchant.so*
%{_libdir}/pkgconfig/enchant.pc
%{_datadir}/enchant/enchant.ordering
%{_mandir}/man1/enchant.1.gz

%clean
rm -rf %{buildroot}

%changelog
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
