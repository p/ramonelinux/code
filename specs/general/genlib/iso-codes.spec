Name:       iso-codes
Version:    3.61
Release:    1%{?dist}
Summary:    ISO Codes

Group:      Applications/Multimedia
License:    GPLv2+
Url:        http://pkg-isocodes.alioth.debian.org
Source:     http://pkg-isocodes.alioth.debian.org/downloads/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildArch:      noarch
BuildRequires:  gettext

%description
The ISO Codes package contains a list of country, language and currency names.
This is useful when used as a central database for accessing this data. 

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_datadir}/locale/*
%{_datadir}/pkgconfig/iso-codes.pc
%{_datadir}/xml/iso-codes/iso_*.xml

%clean
rm -rf %{buildroot}

%changelog
* Wed Sep 16 2015 tanggeliang <tanggeliang@gmail.com>
- update from 3.38 to 3.61
* Fri Sep 28 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.30 to 3.38
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
