Name:       libatasmart
Version:    0.19
Release:    8%{?dist}
Summary:    libatasmart

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://0pointer.de
Source:     http://0pointer.de/public/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  systemd-libudev

%description
The libatasmart package is a disk reporting library.
It only supports a subset of the ATA S.M.A.R.T. functionality.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --disable-static \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_includedir}/atasmart.h
%{_libdir}/libatasmart.*
%{_libdir}/pkgconfig/libatasmart.pc
%{_sbindir}/skdump
%{_sbindir}/sktest
%{_datadir}/vala/vapi/atasmart.vapi

%files doc
%defattr(-,root,root,-)
%{_docdir}/libatasmart/README

%clean
rm -rf %{buildroot}

%changelog
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
