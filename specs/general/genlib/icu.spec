Name:       icu
Version:    53.1
Release:    1%{?dist}
Summary:    ICU International Components for Unicode

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.icu-project.org
Source:     http://download.icu-project.org/files/icu4c/%{version}/%{name}4c-53_1-src.tgz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build

%description
The International Components for Unicode (ICU) package is a mature, widely used set of C/C++ libraries providing Unicode and Globalization support for software applications.
ICU is widely portable and gives applications the same results on all platforms.

%prep
%setup -q -n %{name}

%build
cd source &&
CXX=g++ ./configure --prefix=/usr \
                    --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
cd source &&
make DESTDIR=%{buildroot} install

rm -f %{buildroot}/%{_infodir}/dir

%files
%defattr(-,root,root,-)
%{_bindir}/derb
%{_bindir}/genbrk
%{_bindir}/gencfu
%{_bindir}/gencnval
%{_bindir}/gendict
%{_bindir}/genrb
%{_bindir}/icu-config
%{_bindir}/icuinfo
%{_bindir}/makeconv
%{_bindir}/pkgdata
%{_bindir}/uconv
%{_includedir}/layout/*.h
%{_includedir}/unicode/*.h
%{_libdir}/icu/%{version}/Makefile.inc
%{_libdir}/icu/%{version}/pkgdata.inc
%{_libdir}/icu/Makefile.inc
%{_libdir}/icu/current
%{_libdir}/icu/pkgdata.inc
%{_libdir}/libicudata.so*
%{_libdir}/libicui18n.so*
%{_libdir}/libicuio.so*
%{_libdir}/libicule.so*
%{_libdir}/libiculx.so*
%{_libdir}/libicutest.so*
%{_libdir}/libicutu.so*
%{_libdir}/libicuuc.so*
%{_libdir}/pkgconfig/icu-i18n.pc
%{_libdir}/pkgconfig/icu-io.pc
%{_libdir}/pkgconfig/icu-le.pc
%{_libdir}/pkgconfig/icu-lx.pc
%{_libdir}/pkgconfig/icu-uc.pc
%{_sbindir}/genccode
%{_sbindir}/gencmn
%{_sbindir}/gennorm2
%{_sbindir}/gensprep
%{_sbindir}/icupkg
%{_datadir}/icu/%{version}/config/mh-linux
%{_datadir}/icu/%{version}/install-sh
%{_datadir}/icu/%{version}/license.html
%{_datadir}/icu/%{version}/mkinstalldirs
%{_mandir}/man1/*.1.gz
%{_mandir}/man8/*.8.gz

%clean
rm -rf %{buildroot}

%changelog
* Sun Jul 20 2014 tanggeliang <tanggeliang@gmail.com>
- update from 52.1 to 53.1
* Sun Oct 20 2013 tanggeliang <tanggeliang@gmail.com>
- update from 51.2 to 52.1
* Thu Jul 25 2013 tanggeliang <tanggeliang@gmail.com>
- update from 51.1 to 51.2
* Wed Apr 10 2013 tanggeliang <tanggeliang@gmail.com>
- update from 50.1.2 to 51.1
* Sun Mar 17 2013 tanggeliang <tanggeliang@gmail.com>
- update from 49.1.2 to 50.1.2
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
