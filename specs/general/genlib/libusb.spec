Name:       libusb
Version:    1.0.19
Release:    1%{?dist}
Summary:    libusb

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://libusb.sourceforge.net
Source:     http://downloads.sourceforge.net/libusb/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:	systemd-libudev

%description
The libusb package contains a library used by some applications for USB device access.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --disable-static \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_includedir}/libusb-1.0/libusb.h
%{_libdir}/libusb-1.0.*
%{_libdir}/pkgconfig/libusb-1.0.pc

%clean
rm -rf %{buildroot}

%changelog
* Fri Mar 27 2015 tanggeliang <tanggeliang@gmail.com>
- update from 1.0.9 to 1.0.19
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
