Name:       gsl
Version:    1.16
Release:    1%{?dist}
Summary:    Gsl

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.gnu.org
Source:     http://ftp.gnu.org/gnu/gsl/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build

%description
The GNU Scientific Library (GSL) is a numerical library for C and C++ programmers. It provides a wide range of mathematical routines such as random number generators, special functions and least-squares fitting.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --disable-static \
            --libdir=%{_libdir} &&
make %{?_smp_mflags} &&

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install &&

rm -f %{buildroot}/%{_infodir}/dir

%files
%defattr(-,root,root,-)
%{_bindir}/gsl-config
%{_bindir}/gsl-histogram
%{_bindir}/gsl-randist
%{_includedir}/gsl/gsl_*.h
%{_libdir}/libgsl.*
%{_libdir}/libgslcblas.*
%{_libdir}/pkgconfig/gsl.pc
%{_datadir}/aclocal/gsl.m4
%{_infodir}/gsl-ref.info-*.gz
%{_infodir}/gsl-ref.info.gz
%{_mandir}/man*/gsl*.gz

%clean
rm -rf %{buildroot}

%changelog
* Wed Aug 21 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.15 to 1.16
* Sun Jun 9 2013 tanggeliang <tanggeliang@gmail.com>
- create
