Name:       libidl
Version:    0.8.14
Release:    5%{?dist}
Summary:    libIDL

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/libIDL/0.8/libIDL-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  bison, flex, glib

%description
The libIDL package contains libraries for Interface Definition Language files. This is a specification for defining portable interfaces.

%prep
%setup -q -n libIDL-%{version}

%build
./configure --prefix=/usr &&
make %{?_smp_mflags} &&

makeinfo --plaintext -o libIDL2.txt libIDL2.texi

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

install -v -m755 -d %{buildroot}/usr/share/doc/libIDL-0.8.14 &&
install -v -m644    README libIDL2.{txt,texi} \
                    %{buildroot}/usr/share/doc/libIDL-0.8.14

rm -f %{buildroot}/%{_infodir}/dir

%files
%defattr(-,root,root,-)
%{_bindir}/libIDL-config-2
%{_includedir}/libIDL-2.0/libIDL/IDL.h
%{_libdir}/libIDL-2.*
%{_libdir}/pkgconfig/libIDL-2.0.pc
%{_docdir}/libIDL-0.8.14/*
%{_infodir}/*

%clean
rm -rf %{buildroot}

%changelog
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
