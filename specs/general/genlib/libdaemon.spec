Name:       libdaemon
Version:    0.14
Release:    1%{?dist}
Summary:    libdaemon

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.libdaemon.org
Source:     http://0pointer.de/lennart/projects/libdaemon/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build

%description
The libdaemon package is a lightweight C library that eases the writing of UNIX daemons.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --disable-static \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make docdir=/usr/share/doc/libdaemon-0.14 install DESTDIR=%{buildroot}

%files
%defattr(-,root,root,-)
%{_includedir}/libdaemon/d*.h
%{_libdir}/libdaemon.la
%{_libdir}/libdaemon.so*
%{_libdir}/pkgconfig/libdaemon.pc
%{_docdir}/libdaemon-0.14/*

%clean
rm -rf %{buildroot}

%changelog
* Sun Sep 30 2012 tanggeliang <tanggeliang@gmail.com>
- create
