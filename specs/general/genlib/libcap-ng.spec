Name:       libcap-ng
Version:    0.7.4
Release:    1%{?dist}
Summary:    An alternate posix capabilities library

Group:      System Environment/Libraries
License:    LGPLv2+
URL:        http://people.redhat.com/sgrubb/libcap-ng
Source:     http://people.redhat.com/sgrubb/libcap-ng/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  attr libtool

%description
Libcap-ng is a library that makes using posix capabilities easier

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --sysconfdir=/etc \
            --libdir=%{_libdir} \
            --libexecdir=%{_libdir} &&
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
make DESTDIR="%{buildroot}" install

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%{_bindir}/captest
%{_bindir}/filecap
%{_bindir}/netcap
%{_bindir}/pscap
%{_includedir}/cap-ng.h
%{_libdir}/libcap-ng.*a
%{_libdir}/libcap-ng.so*
%{_libdir}/pkgconfig/libcap-ng.pc
%{_datadir}/aclocal/cap-ng.m4
%{_mandir}/man*/*.gz

%changelog
* Mon Mar 30 2015 tanggeliang <tanggeliang@gmail.com>
- update from 0.6.6 to 0.7.4
