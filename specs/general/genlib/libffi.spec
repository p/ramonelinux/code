Name:       libffi
Version:    3.1
Release:    1%{?dist}
Summary:    libffi

Group:      System Environment/libraries
License:    GPLv2+
Url:        ftp://sourceware.org
Source:     ftp://sourceware.org/pub/libffi/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build

%description
The libffi library provides a portable, high level programming interface to various calling conventions. This allows a programmer to call any function specified by a call interface description at run time.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
sed -e '/^includesdir/ s:$(libdir)/@PACKAGE_NAME@-@PACKAGE_VERSION@/include:$(includedir):' \
    -i include/Makefile.in &&
sed -e '/^includedir/ s:${libdir}/@PACKAGE_NAME@-@PACKAGE_VERSION@/include:@includedir@:' \
    -e 's/^Cflags: -I${includedir}/Cflags:/' \
    -i libffi.pc.in        &&
./configure --prefix=/usr --disable-static \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

rm -f %{buildroot}/%{_infodir}/dir

%files
%defattr(-,root,root,-)
%{_includedir}/ffi.h
%{_includedir}/ffitarget.h
%{_libdir}/libffi.*
%{_libdir}/pkgconfig/libffi.pc

%files doc
%defattr(-,root,root,-)
%{_infodir}/libffi.info.gz
%{_mandir}/*/*

%clean
rm -rf %{buildroot}

%changelog
* Sat Jul 26 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.0.13 to 3.1
* Thu Apr 11 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.0.11 to 3.0.13
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
