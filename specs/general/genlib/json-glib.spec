Name:       json-glib
Version:    1.0.4
Release:    2%{?dist}
Summary:    JSON GLib

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/json-glib/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  glib pkg-config
BuildRequires:  gobject-introspection

%description
The JSON GLib package is a library providing serialization and deserialization support for the JavaScript Object Notation (JSON) format described by RFC 4627.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/json-glib-format
%{_bindir}/json-glib-validate
%{_includedir}/json-glib-1.0/json-glib/json-*.h
%{_libdir}/girepository-1.0/Json-1.0.typelib
%{_libdir}/libjson-glib-1.0.*
%{_libdir}/pkgconfig/json-glib-1.0.pc
%{_datadir}/gir-1.0/Json-1.0.gir
%{_datadir}/locale/*

%files doc
%defattr(-,root,root,-)
%{_datadir}/gtk-doc/html/json-glib/*

%clean
rm -rf %{buildroot}

%changelog
* Wed Sep 16 2015 tanggeliang <tanggeliang@gmail.com>
- update from 1.0.2 to 1.0.4
* Tue Jul 22 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.0.0 to 1.0.2
* Sun Mar 30 2014 tanggeliang <tanggeliang@gmail.com>
- update from 0.16.2 to 1.0.0
* Tue Sep 24 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.16.0 to 0.16.2
* Tue Jul 2 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.15.2 to 0.16.0
* Sun Oct 28 2012 tanggeliang <tanggeliang@gmail.com>
- update from 0.14.2 to 0.15.2
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
