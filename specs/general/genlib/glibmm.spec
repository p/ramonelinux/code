Name:       glibmm
Version:    2.45.80
Release:    1%{?dist}
Summary:    Glibmm

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/glibmm/2.45/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  glib libsigc++
BuildRequires:  gettext

%description
The GLibmm package is a set of C++ bindings for GLib.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_includedir}/giomm-2.4/*
%{_includedir}/glibmm-2.4/*
%{_libdir}/giomm-2.4/include/giommconfig.h 
%{_libdir}/glibmm-2.4/*
%{_libdir}/libgiomm-2.4.*
%{_libdir}/libglibmm-2.4.*
%{_libdir}/libglibmm_generate_extra_defs-2.4.*
%{_libdir}/pkgconfig/giomm-2.4.pc
%{_libdir}/pkgconfig/glibmm-2.4.pc

%files doc
%defattr(-,root,root,-)
%{_docdir}/*/*
%{_datadir}/devhelp/*

%clean
rm -rf %{buildroot}

%changelog
* Sun Sep 20 2015 tanggeliang <tanggeliang@gmail.com>
- update from 2.44.0 to 2.45.80
* Sat Mar 28 2015 tanggeliang <tanggeliang@gmail.com>
- update from 2.41.1 to 2.44.0
* Sat Jul 26 2014 tanggeliang <tanggeliang@gmail.com>
- update from 2.38.1 to 2.41.1
* Thu Dec 5 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.38.0 to 2.38.1
* Tue Oct 22 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.37.93 to 2.38.0
* Tue Oct 8 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.36.2 to 2.37.93
* Sun Jul 7 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.34.1 to 2.36.2
* Thu May 30 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.34.0 to 2.34.1
* Sat Nov 3 2012 tanggeliang <tanggeliang@gmail.com>
- update from 2.33.14 to 2.34.0
* Mon Oct 22 2012 tanggeliang <tanggeliang@gmail.com>
- update from 2.33.13 to 2.33.14
* Sun Oct 14 2012 tanggeliang <tanggeliang@gmail.com>
- update from 2.32.1 to 2.33.13
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
