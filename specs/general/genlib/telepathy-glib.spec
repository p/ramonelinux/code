Name:       telepathy-glib
Version:    0.24.1
Release:    1%{?dist}
Summary:    Telepathy GLib

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.freedesktop.org
Source:     http://telepathy.freedesktop.org/releases/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  dbus-glib libxslt
BuildRequires:  gobject-introspection vala
BuildRequires:  gtk-doc intltool gettext xml-parser
AutoReq: no

%description
The Telepathy GLib package is a library for GLib based Telepathy components.
Telepathy is a D-Bus framework for unifying real time communication, including instant messaging, voice calls and video calls.
It abstracts differences between protocols to provide a unified interface for applications.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --enable-vala-bindings \
            --disable-static \
            --libexecdir=%{_libdir}/telepathy-glib \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_includedir}/telepathy-1.0/telepathy-glib/_gen/*.h
%{_includedir}/telepathy-1.0/telepathy-glib/*.h
%{_libdir}/girepository-1.0/TelepathyGLib-0.12.typelib
%{_libdir}/libtelepathy-glib.la
%{_libdir}/libtelepathy-glib.so*
%{_libdir}/pkgconfig/telepathy-glib.pc
%{_datadir}/gir-1.0/TelepathyGLib-0.12.gir
%{_datadir}/vala/vapi/telepathy-glib.*

%files doc
%defattr(-,root,root,-)
%{_datadir}/gtk-doc/html/telepathy-glib/*

%clean
rm -rf %{buildroot}

%changelog
* Sun Sep 20 2015 tanggeliang <tanggeliang@gmail.com>
- update from 0.24.0 to 0.24.1
* Sat Jul 19 2014 tanggeliang <tanggeliang@gmail.com>
- update from 0.23.3 to 0.24.0
* Thu Mar 20 2014 tanggeliang <tanggeliang@gmail.com>
- update from 0.21.2 to 0.23.3
* Wed Sep 25 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.21.1 to 0.21.2
* Sat Aug 17 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.20.2 to 0.21.1
* Fri May 10 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.20.0 to 0.20.2
* Sun Oct 14 2012 tanggeliang <tanggeliang@gmail.com>
- update from 0.19.10 to 0.20.0
* Fri Sep 28 2012 tanggeliang <tanggeliang@gmail.com>
- update from 0.18.2 to 0.19.10
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
