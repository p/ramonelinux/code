Name:       aalib
Version:    1.4
Release:    6%{?dist}
Summary:    AAlib

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://aa-project.sourceforge.net
Source:     http://downloads.sourceforge.net/aa-project/%{name}-%{version}rc5.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build

%description
AAlib is a library to render any graphic into ASCII Art.

%prep
%setup -q -n %{name}-%{version}.0

%build
./configure --prefix=/usr \
            --infodir=%{_infodir} \
            --mandir=%{_mandir} \
            --disable-static \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

rm -rf %{buildroot}/%{_infodir}/dir

%files
%defattr(-,root,root,-)
%{_bindir}/aafire
%{_bindir}/aainfo
%{_bindir}/aalib-config
%{_bindir}/aasavefont
%{_bindir}/aatest
%{_includedir}/aalib.h
%{_libdir}/libaa.la
%{_libdir}/libaa.so*
%{_datadir}/aclocal/aalib.m4
%{_infodir}/aalib.info*.gz
%{_mandir}/man*/*.gz

%clean
rm -rf %{buildroot}

%changelog
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
