Name:       pcre
Version:    8.35
Release:    1%{?dist}
Summary:    PCRE

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://pcre.sourceforge.net
Source:     http://downloads.sourceforge.net/pcre/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build

%description
The PCRE package contains Perl Compatible Regular Expression libraries.
These are useful for implementing regular expression pattern matching using the same syntax and semantics as Perl 5.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr                       \
            --docdir=%{_docdir}/pcre-%{version} \
            --enable-unicode-properties         \
            --enable-pcre16                     \
            --enable-pcre32                     \
            --enable-pcregrep-libz              \
            --enable-pcregrep-libbz2            \
            --enable-pcretest-libreadline       \
            --disable-static                    \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

mkdir %{buildroot}/%{_lib}                                      &&
mv -v %{buildroot}/%{_libdir}/libpcre.so.* %{buildroot}/%{_lib} &&
ln -sfv ../../%{_lib}/$(readlink %{buildroot}/%{_libdir}/libpcre.so) %{buildroot}/%{_libdir}/libpcre.so

%files
%defattr(-,root,root,-)
/%{_lib}/libpcre.so.1*
%{_bindir}/pcre-config
%{_bindir}/pcregrep
%{_bindir}/pcretest
%{_includedir}/pcre*.h
%{_libdir}/libpcre.*
%{_libdir}/libpcre16.la
%{_libdir}/libpcre16.so*
%{_libdir}/libpcre32.la
%{_libdir}/libpcre32.so*
%{_libdir}/libpcrecpp.*
%{_libdir}/libpcreposix.*
%{_libdir}/pkgconfig/libpcre*.pc

%files doc
%defattr(-,root,root,-)
%{_docdir}/pcre-%{version}/*
%{_mandir}/man1/pcre*.1.gz
%{_mandir}/man3/pcre*.3.gz

%clean
rm -rf %{buildroot}

%changelog
* Sun Jul 20 2014 tanggeliang <tanggeliang@gmail.com>
- update from 8.34 to 8.35
* Tue Dec 17 2013 tanggeliang <tanggeliang@gmail.com>
- update from 8.33 to 8.34
* Thu Jul 25 2013 tanggeliang <tanggeliang@gmail.com>
- update from 8.31 to 8.33
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
