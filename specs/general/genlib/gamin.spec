Name:       gamin
Version:    0.1.10
Release:    1%{?dist}
Summary:    Gamin

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://www.gnome.org/~veillard/gamin/sources/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  glib python

%description
The Gamin package contains a File Alteration Monitor which is useful for notifying applications of changes to the file system. Gamin is compatible with FAM.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
sed -i 's/G_CONST_RETURN/const/' server/gam_{node,subscription}.{c,h} &&
./configure --prefix=/usr --libexecdir=/usr/sbin --disable-static \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install &&
install -v -m755 -d %{buildroot}/usr/share/doc/gamin-0.1.10 &&
install -v -m644 doc/*.{html,fig,gif,txt} %{buildroot}/usr/share/doc/gamin-0.1.10

%files
%defattr(-,root,root,-)
%{_includedir}/fam.h
%{_libdir}/libfam.la
%{_libdir}/libfam.so*
%{_libdir}/libgamin-1.la
%{_libdir}/libgamin-1.so*
%{_libdir}/libgamin_shared.a
%{_libdir}/pkgconfig/gamin.pc
%{_libdir}/python2.7/site-packages/_gamin.*
%{_libdir}/python2.7/site-packages/gamin.py*
%{_sbindir}/gam_server

%files doc
%defattr(-,root,root,-)
%{_docdir}/%{name}-%{version}/*

%clean
rm -rf %{buildroot}

%changelog
* Mon Apr 1 2013 tanggeliang <tanggeliang@gmail.com>
- create
