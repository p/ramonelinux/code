Name:       libtasn1
Version:    4.0
Release:    1%{?dist}
Summary:    libtasn1

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.gnu.org
Source:     ftp://ftp.gnu.org/pub/gnu/libtasn1/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build

%description
libtasn1 is a highly portable C library that encodes and decodes DER/BER data following an ASN.1 schema.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --disable-static \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

rm -f %{buildroot}/%{_infodir}/dir

%files
%defattr(-,root,root,-)
%{_bindir}/asn1*
%{_includedir}/libtasn1.h
%{_libdir}/libtasn1.*
%{_libdir}/pkgconfig/libtasn1.pc

%files doc
%defattr(-,root,root,-)
%{_infodir}/*
%{_mandir}/*/*

%clean
rm -rf %{buildroot}

%changelog
* Sat Jul 26 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.4 to 4.0
* Tue Mar 18 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.3 to 3.4
* Sun Aug 18 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.14 to 3.3
* Sun Oct 14 2012 tanggeliang <tanggeliang@gmail.com>
- update from 2.13 to 2.14
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
