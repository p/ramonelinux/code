Name:       libglade
Version:    2.6.4
Release:    7%{?dist}
Summary:    libglade

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/libglade/2.6/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  libxml gtk2

%description
The libglade package contains libglade libraries.
These are useful for loading Glade interface files in a program at runtime.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
sed -i '/DG_DISABLE_DEPRECATED/d' glade/Makefile.in &&
./configure --prefix=/usr \
            --libdir=%{_libdir} &&
make

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/libglade-convert
%{_includedir}/libglade-2.0/glade/glade*.h
%{_libdir}/libglade-2.0.*
%{_libdir}/pkgconfig/libglade-2.0.pc
%{_datadir}/xml/libglade/glade-2.0.dtd

%files doc
%defattr(-,root,root,-)
%{_datadir}/gtk-doc/html/libglade/*

%clean
rm -rf %{buildroot}

%changelog
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
