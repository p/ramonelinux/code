Name:		libaccounts-glib
Version:	1.8
Release:	2%{?dist}
Summary:	libaccounts-glib

Group:		System Environment/Libraries
License:	LGPLv2
URL:		https://code.google.com/accounts-sso
Source:     	https://accounts-sso.googlecode.com/files/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:	dbus-glib libxml sqlite check gobject-introspection

%description
Accounts framework for Linux and POSIX based platforms.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q %{name}-%{version}

%build
./configure --prefix=/usr --disable-static --disable-gtk-doc \
            --libdir=%{_libdir} &&
sed -i 's/-Werror//g' libaccounts-glib/Makefile &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

%files
%defattr(-,root,root,-)
%{_bindir}/ag-backup
%{_bindir}/ag-tool
%{_includedir}/libaccounts-glib/*.h
%{_libdir}/girepository-1.0/Accounts-1.0.typelib
%{_libdir}/libaccounts-glib.la
%{_libdir}/libaccounts-glib.so*
%{_libdir}/pkgconfig/libaccounts-glib.pc
%{_datadir}/backup-framework/applications/accounts.conf
%{_datadir}/gir-1.0/Accounts-1.0.gir
%{_datadir}/vala/vapi/accounts.*
%{_datadir}/xml/accounts/schema/dtd/accounts-*.dtd

%files doc
%defattr(-,root,root,-)
%{_datadir}/gtk-doc/html/libaccounts-glib/*

%changelog
* Fri Apr 12 2013 tanggeliang tanggeliang@gmail.com
- create
