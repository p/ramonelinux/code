Name:       libxslt
Version:    1.1.28
Release:    2%{?dist}
Summary:    libxslt

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://xmlsoft.org
Source:     http://xmlsoft.org/sources/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  libxml

%description
The libxslt package contains XSLT libraries. These are useful for extending libxml2 libraries to support XSLT files.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --disable-static \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/xslt-config
%{_bindir}/xsltproc
%{_includedir}/libexslt/*.h
%{_includedir}/libxslt/*.h
%{_libdir}/libexslt.*
%{_libdir}/libxslt.*
%{_libdir}/pkgconfig/libexslt.pc
%{_libdir}/pkgconfig/libxslt.pc
%{_libdir}/python2.7/site-packages/libxslt*
%{_libdir}/xsltConf.sh
%{_datadir}/aclocal/libxslt.m4

%files doc
%defattr(-,root,root,-)
%{_docdir}/libxslt-1.1.28/*
%{_docdir}/libxslt-python-1.1.28/*
%{_mandir}/man1/xsltproc.1.gz
%{_mandir}/man3/lib*xslt.3.gz

%clean
rm -rf %{buildroot}

%changelog
* Sat Dec 1 2012 tanggeliang <tanggeliang@gmail.com>
- update from 1.1.27 to 1.1.28
* Thu Oct 4 2012 tanggeliang <tanggeliang@gmail.com>
- update from 1.1.26 to 1.1.27
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
