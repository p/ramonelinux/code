Name:       json-c
Version:    0.12
Release:    1%{?dist}
Summary:    JSON-C

Group:      System Environment/libraries
License:    GPLv2+
Url:        https://github.com/json-c/json-c/wiki
Source:     https://s3.amazonaws.com/json-c_releases/releases/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  autoconf automake m4

%description
The JSON-C implements a reference counting object model that allows you to easily construct JSON objects in C, output them as JSON formatted strings and parse JSON formatted strings back into the C representation of JSON objects.

%prep
%setup -q -n %{name}-%{version}

%build
sed -i s/-Werror// Makefile.in             &&
./configure --prefix=/usr --disable-static \
            --libdir=%{_libdir} &&
make -j1

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_includedir}/json-c/*.h
%{_libdir}/libjson-c.*
%{_libdir}/pkgconfig/json-c.pc

%clean
rm -rf %{buildroot}

%changelog
* Tue Jul 22 2014 tanggeliang <tanggeliang@gmail.com>
- update from 0.11 to 0.12
* Tue Aug 27 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.9 to 0.11
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
