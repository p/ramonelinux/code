Name:       ustr
Version:    1.0.4
Release:    1%{?dist}
Summary:    ustr

Group:      Development/Libraries
License:    BSD and LGPLv2+
Url:        http://www.and.org/ustr
Source:     http://www.and.org/ustr/%{version}/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build

%description
µstr - Micro String API - for C.

%prep
%setup -q -n %{name}-%{version}

%build
make %{?_smp_mflags} all-shared

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install libdir=%{_libdir} mandir=%{_mandir}

%files
%defattr(-,root,root,-)
%{_bindir}/ustr-import
%{_includedir}/ustr-*.h
%{_includedir}/ustr.h
%{_libdir}/libustr-1.0.so.1*
%{_libdir}/libustr-debug-1.0.so.1*
%{_libdir}/libustr-debug.a
%{_libdir}/libustr-debug.so
%{_libdir}/libustr.a
%{_libdir}/libustr.so
%{_libdir}/pkgconfig/ustr-debug.pc
%{_libdir}/pkgconfig/ustr.pc
%{_mandir}/man*/ustr*.gz
%{_docdir}/ustr-devel-%{version}/*
%{_datadir}/ustr-%{version}/.gdbinit
%{_datadir}/ustr-%{version}/*

%changelog
* Mon Mar 30 2015 tanggeliang <tanggeliang@gmail.com>
- create
