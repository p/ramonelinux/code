Name:       lzo
Version:    2.09
Release:    1%{?dist}
Summary:    LZO

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.lzo.org
Source:     http://www.oberhumer.com/opensource/lzo/download/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build

%description
LZO is a data compression library which is suitable for data decompression and compression in real-time.
This means it favors speed over compression ratio.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr                    \
            --enable-shared                  \
            --disable-static                 \
            --libdir=%{_libdir}   	     \
            --docdir=/usr/share/doc/lzo-%{version} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_includedir}/lzo/lzo*.h
%{_libdir}/liblzo2.*a
%{_libdir}/liblzo2.so*
%{_docdir}/lzo-%{version}/*

%clean
rm -rf %{buildroot}

%changelog
* Thu Jan 7 2016 tanggeliang <tanggeliang@gmail.com>
- update from 2.08 to 2.09
* Wed Dec 31 2014 tanggeliang <tanggeliang@gmail.com>
- update from 2.06 to 2.08
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
