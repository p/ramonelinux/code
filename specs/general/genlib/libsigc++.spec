Name:       libsigc++
Version:    2.5.4
Release:    1%{?dist}
Summary:    libsigc++

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/libsigc++/2.5/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  m4

%description
The libsigc++ package implements a typesafe callback system for standard C++.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_includedir}/*
%{_libdir}/libsigc-2.0.*
%{_libdir}/pkgconfig/sigc++-2.0.pc
%{_libdir}/sigc++-2.0/include/sigc++config.h

%files doc
%defattr(-,root,root,-)
%{_datadir}/devhelp/books/libsigc++-2.0/libsigc++-2.0.devhelp2
%{_docdir}/*/*

%clean
rm -rf %{buildroot}

%changelog
* Sun Sep 20 2015 tanggeliang <tanggeliang@gmail.com>
- update from 2.3.1 to 2.5.4
* Tue Dec 3 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.2.11 to 2.3.1
* Sun Oct 14 2012 tanggeliang <tanggeliang@gmail.com>
- update from 2.2.10 to 2.2.11
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
