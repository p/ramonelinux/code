Name:       libidn
Version:    1.28
Release:    1%{?dist}
Summary:    libidn

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.gnu.org
Source:     http://ftp.gnu.org/gnu/libidn/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build

%description
libidn is a package designed for internationalized string handling based on the Stringprep, Punycode and IDNA specifications defined by the Internet Engineering Task Force (IETF) Internationalized Domain Names (IDN) working group, used for internationalized domain names.
This is useful for converting data from the system's native representation into UTF-8, transforming Unicode strings into ASCII strings, allowing applications to use certain ASCII name labels (beginning with a special prefix) to represent non-ASCII name labels, and converting entire domain names to and from the ASCII Compatible Encoding (ACE) form.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --disable-static \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install &&

find doc -name "Makefile*" -delete            &&
rm -rf -v doc/{gdoc,idn.1,stamp-vti,man,texi} &&
mkdir -pv      %{buildroot}/usr/share/doc/libidn-%{version} &&
cp -r -v doc/* %{buildroot}/usr/share/doc/libidn-%{version}

rm -rf %{buildroot}/%{_infodir}/dir

%files
%defattr(-,root,root,-)
%{_bindir}/idn
%{_includedir}/*.h
%{_libdir}/libidn.*
%{_libdir}/pkgconfig/libidn.pc
%{_datadir}/emacs/site-lisp/*.el
%{_datadir}/locale/*/LC_MESSAGES/libidn.mo

%files doc
%defattr(-,root,root,-)
%{_docdir}/libidn-%{version}/*
%{_infodir}/libidn*.gz
%{_mandir}/man*/*.gz

%clean
rm -rf %{buildroot}

%changelog
* Wed Aug 21 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.25 to 1.28
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
