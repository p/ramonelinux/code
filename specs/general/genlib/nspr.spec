Name:       nspr
Version:    4.10.9
Release:    1%{?dist}
Summary:    NSPR

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.mozilla.org
Source:     http://ftp.mozilla.org/pub/mozilla.org/nspr/releases/v%{version}/src/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build

%description
Netscape Portable Runtime (NSPR) provides a platform-neutral API for system level and libc like functions.

%prep
%setup -q -n %{name}-%{version}

%build
cd nspr                                                     &&
sed -ri 's#^(RELEASE_BINS =).*#\1#' pr/src/misc/Makefile.in &&
sed -i 's#$(LIBRARY) ##' config/rules.mk                    &&

./configure --prefix=/usr \
            --with-mozilla \
            --with-pthreads \
            --libdir=%{_libdir} \
            $([ $(uname -m) = x86_64 ] && echo --enable-64bit) &&
make %{?_smp_mflags}

%check

%install
cd nspr &&
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/nspr-config
%{_includedir}/nspr/md/_*.cfg
%{_includedir}/nspr/obsolete/*.h
%{_includedir}/nspr/private/*.h
%{_includedir}/nspr/*.h
%{_libdir}/libnspr4.so
%{_libdir}/libplc4.so
%{_libdir}/libplds4.so
%{_libdir}/pkgconfig/nspr.pc
%{_datadir}/aclocal/nspr.m4

%clean
rm -rf %{buildroot}

%changelog
* Sat Sep 26 2015 tanggeliang <tanggeliang@gmail.com>
- update from 4.10.6 to 4.10.9
* Sun Jul 20 2014 tanggeliang <tanggeliang@gmail.com>
- update from 4.10.4 to 4.10.6
* Tue Mar 18 2014 tanggeliang <tanggeliang@gmail.com>
- update from 4.10.2 to 4.10.4
* Tue Dec 3 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.10.1 to 4.10.2
* Mon Oct 7 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.10 to 4.10.1
* Mon Aug 19 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.9.6 to 4.10
* Fri Apr 5 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.9.4 to 4.9.6
* Sun Feb 24 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.9.3 to 4.9.4
* Sat Nov 3 2012 tanggeliang <tanggeliang@gmail.com>
- update from 4.9.2 to 4.9.3
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
