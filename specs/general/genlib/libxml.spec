Name:       libxml
Version:    2.9.0
Release:    6%{?dist}
Summary:    libxml2

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://xmlsoft.org
Source:     http://xmlsoft.org/sources/%{name}2-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  python

%description
The libxml2 package contains XML libraries. These are useful for parsing XML files.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}2-%{version}

%build
./configure --prefix=/usr --disable-static \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/xml2-config
%{_bindir}/xmlcatalog
%{_bindir}/xmllint
%{_includedir}/libxml2/libxml/*.h
%{_libdir}/libxml2.la
%{_libdir}/libxml2.so*
%{_libdir}/pkgconfig/libxml-2.0.pc
%{_libdir}/python2.7/site-packages/*libxml2*
%{_libdir}/xml2Conf.sh
%{_datadir}/aclocal/libxml.m4

%files doc
%defattr(-,root,root,-)
%{_datadir}/gtk-doc/html/libxml2/*
%{_docdir}/libxml2-%{version}/*
%{_docdir}/libxml2-python-%{version}/*
%{_mandir}/man*/*xml*.gz

%clean
rm -rf %{buildroot}

%changelog
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
