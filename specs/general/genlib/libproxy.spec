Name:       libproxy
Version:    0.4.11
Release:    1%{?dist}
Summary:    libproxy

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://libproxy.googlecode.com
Source:     http://libproxy.googlecode.com/files/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  cmake gconf

%description
A library handling all the details of proxy configuration.

%prep
%setup -q -n %{name}-%{version}

%build
cmake -DCMAKE_INSTALL_PREFIX=/usr \
      -DLIB_INSTALL_DIR:PATH=%{_libdir} . &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/proxy
%{_includedir}/proxy.h
%{_libdir}/libproxy.*
%{_libdir}/pkgconfig/libproxy-1.0.pc
%{perl_sitearch}/Net/Libproxy.pm
%{perl_sitearch}/auto/Net/Libproxy/Libproxy.so
%{_libdir}/libproxy/%{version}/modules/config_gnome3.so
/usr/libexec/pxgsettings
/usr/lib/python2.7/site-packages/libproxy.py
%{_datadir}/cmake/Modules/Findlibproxy.cmake

%clean
rm -rf %{buildroot}

%changelog
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
