Name:       clucene
Version:    2.3.3.4
Release:    1%{?dist}
Summary:    Clucene

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://clucene.sourceforge.net 
Source:     http://sourceforge.net/projects/clucene/files/%{name}-core-%{version}.tar.gz
Patch:      %{name}-%{version}-contribs_lib-1.patch

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  cmake boost

%description
CLucene is a C++ version of Lucene, a high performance text search engine.

%prep
%setup -q -n %{name}-core-%{version}
%patch -p1

%build
mkdir build &&
cd build &&
cmake -DCMAKE_INSTALL_PREFIX=/usr \
      -DBUILD_CONTRIBS_LIB=ON \
%ifarch x86_64
      -DLIB_SUFFIX=64 \
%endif
      .. &&
make %{?_smp_mflags}


%check

%install
rm -rf %{buildroot}
cd build &&
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_includedir}/CLucene.h
%{_includedir}/CLucene/*
%{_libdir}/CLuceneConfig.cmake/CLuceneConfig.cmake
%{_libdir}/libclucene-contribs-lib.so*
%{_libdir}/libclucene-core.so*
%{_libdir}/libclucene-shared.so*
%{_libdir}/pkgconfig/libclucene-core.pc

%clean
rm -rf %{buildroot}

%changelog
* Wed Oct 23 2013 tanggeliang <tanggeliang@gmail.com>
- create
