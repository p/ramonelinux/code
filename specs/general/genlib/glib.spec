Name:       glib
Version:    2.46.0
Release:    1%{?dist}
Summary:    GLib

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/glib/2.46/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  libffi pkg-config python
BuildRequires:  pcre elfutils
BuildRequires:  attr dbus libxml gettext gtk-doc
BuildRequires:  autoconf automake m4

%description
The GLib package contains a low-level libraries useful for providing data structure handling for C, portability wrappers and interfaces for such runtime functionality as an event loop, threads, dynamic loading and an object system. 

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --with-pcre=system \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/gapplication
%{_bindir}/gdbus
%{_bindir}/gdbus-codegen
%{_bindir}/gio-querymodules
%{_bindir}/glib-*
%{_bindir}/gobject-query
%{_bindir}/gresource
%{_bindir}/gtester*
%{_bindir}/gsettings
%{_includedir}/gio-unix-2.0/*
%{_includedir}/glib-2.0/*
%{_libdir}/gio/*
%{_libdir}/glib-2.0/*
%{_libdir}/libgio-2.0.*
%{_libdir}/libglib-2.0.*
%{_libdir}/libgmodule-2.0.*
%{_libdir}/libgobject-2.0.*
%{_libdir}/libgthread-2.0.*
%{_libdir}/pkgconfig/gio-*.pc
%{_libdir}/pkgconfig/glib-2.0.pc
%{_libdir}/pkgconfig/gmodule-*.pc
%{_libdir}/pkgconfig/gobject-2.0.pc
%{_libdir}/pkgconfig/gthread-2.0.pc
%{_datadir}/aclocal/*
%{_datadir}/bash-completion/completions/gapplication
%{_datadir}/bash-completion/completions/gdbus
%{_datadir}/bash-completion/completions/gresource
%{_datadir}/bash-completion/completions/gsettings
%{_datadir}/gdb/auto-load/%{_libdir}/libglib-2.0.so.*-gdb.py
%{_datadir}/gdb/auto-load/%{_libdir}/libgobject-2.0.so.*-gdb.py
%{_datadir}/glib-2.0/*
%{_datadir}/locale/*/LC_MESSAGES/glib20.mo

%files doc
%defattr(-,root,root,-)
%{_datadir}/gtk-doc/html/*

%clean
rm -rf %{buildroot}

%changelog
* Tue Sep 22 2015 tanggeliang <tanggeliang@gmail.com>
- update from 2.45.8 to 2.46.0
* Tue Sep 15 2015 tanggeliang <tanggeliang@gmail.com>
- update from 2.44.0 to 2.45.8
* Sat Mar 28 2015 tanggeliang <tanggeliang@gmail.com>
- update from 2.42.0 to 2.44.0
* Tue Sep 23 2014 tanggeliang <tanggeliang@gmail.com>
- update from 2.41.2 to 2.42.0
* Sat Jul 19 2014 tanggeliang <tanggeliang@gmail.com>
- update from 2.40.0 to 2.41.2
* Thu Mar 27 2014 tanggeliang <tanggeliang@gmail.com>
- update from 2.39.91 to 2.40.0
* Sat Mar 15 2014 tanggeliang <tanggeliang@gmail.com>
- update from 2.38.2 to 2.39.91
* Tue Dec 3 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.38.1 to 2.38.2
* Sun Oct 20 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.38.0 to 2.38.1
* Tue Sep 24 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.37.4 to 2.38.0
* Wed Jul 24 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.37.0 to 2.37.4
* Sat May 11 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.36.0 to 2.37.0
* Thu Apr 11 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.34.3 to 2.36.0
* Thu Mar 14 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.34.2 to 2.34.3
- remove Requires: shared-mime-info desktop-file-utils
* Mon Nov 26 2012 tanggeliang <tanggeliang@gmail.com>
- update from 2.34.1 to 2.34.2
* Mon Oct 22 2012 tanggeliang <tanggeliang@gmail.com>
- update from 2.34.0 to 2.34.1
* Fri Sep 28 2012 tanggeliang <tanggeliang@gmail.com>
- update from 2.32.4 to 2.34.0
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
