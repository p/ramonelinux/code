Name:       libinput
Version:    1.0.1
Release:    1%{?dist}
Summary:    libinput

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.freedesktop.org
Source:     http://www.freedesktop.org/software/libinput/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  mtdev systemd-libudev libevdev

%description
libinput is a library to handle input devices in Wayland compositors and to provide a generic X.Org input driver.
It provides device detection, device handling, input device event processing and abstraction so minimize the amount of custom input code compositors need to provide the common set of functionality that users expect.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_includedir}/libinput.h
%{_bindir}/libinput-debug-events
%{_bindir}/libinput-list-devices
%{_libdir}/libinput.*a
%{_libdir}/libinput.so*
%{_libdir}/pkgconfig/libinput.pc
%{_libdir}/udev/libinput-device-group
%{_libdir}/udev/rules.d/80-libinput-device-groups.rules
%{_libdir}/udev/hwdb.d/90-libinput-model-quirks.hwdb
%{_libdir}/udev/libinput-model-quirks
%{_libdir}/udev/rules.d/90-libinput-model-quirks.rules
%{_mandir}/man1/libinput-debug-events.1.gz
%{_mandir}/man1/libinput-list-devices.1.gz

%clean
rm -rf %{buildroot}

%changelog
* Wed Sep 23 2015 tanggeliang <tanggeliang@gmail.com>
- update from 0.99.1 to 1.0.1
* Sun Mar 29 2015 tanggeliang <tanggeliang@gmail.com>
- update from 0.6.0 to 0.13.0
* Tue Sep 23 2014 tanggeliang <tanggeliang@gmail.com>
- update from 0.5.0 to 0.6.0
* Thu Jul 24 2014 tanggeliang <tanggeliang@gmail.com>
- update from 0.1.0 to 0.5.0
* Sat Mar 15 2014 tanggeliang <tanggeliang@gmail.com>
- create
