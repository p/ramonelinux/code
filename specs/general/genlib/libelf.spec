Name:       libelf
Version:    0.8.9
Release:    3%{?dist}
Summary:    libelf

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.mr511.de/software/
Source:     http://www.mr511.de/software/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build

%description

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make prefix=%{buildroot}/usr install

%files
%defattr(-,root,root,-)
%{_includedir}/libelf/*.h
%{_libdir}/libelf.*
%{_libdir}/pkgconfig/libelf.pc
%{_datadir}/locale/*

%clean
rm -rf %{buildroot}

%changelog
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
