Name:      hyphen
Version:   2.8.8
Release:   1%{?dist}
Summary:   A text hyphenation library

Group:     System Environment/Libraries
License:   GPLv2 or LGPLv2+ or MPLv1.1
URL:       http://hunspell.sf.net
Source:    http://downloads.sourceforge.net/hunspell/hyphen-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires: perl patch autoconf, automake m4 libtool

%description
Hyphen is a library for high quality hyphenation and justification.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
	    --disable-static \
	    --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/substrings.pl
%{_includedir}/hyphen.h
%{_libdir}/libhyphen.la
%{_libdir}/libhyphen.so*
%{_datadir}/hyphen/hyph_en_US.dic

%clean
rm -rf %{buildroot}

%changelog
* Wed Sep 16 2015 tanggeliang <tanggeliang@gmail.com>
- create
