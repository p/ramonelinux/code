Name:       libgpg-error
Version:    1.13
Release:    1%{?dist}
Summary:    libgpg-error

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.gnupg.org/gcrypt
Source:     ftp://ftp.gnupg.org/gcrypt/libgpg-error/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build

%description
The libgpg-error package contains a library that defines common error values for all GnuPG components.
Among these are GPG, GPGSM, GPGME, GPG-Agent, libgcrypt, Libksba, DirMngr, Pinentry, SmartCard Daemon and more.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --disable-static \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/gpg-error*
%{_includedir}/gpg-error.h
%{_libdir}/libgpg-error.*
%{_datadir}/aclocal/*
%{_datadir}/common-lisp/*
%{_datadir}/locale/*

%clean
rm -rf %{buildroot}

%changelog
* Sun Jul 27 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.12 to 1.13
* Wed Jul 31 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.11 to 1.12
* Thu Apr 11 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.10 to 1.11
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
