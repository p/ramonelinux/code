Name:       telepathy-logger
Version:    0.8.0
Release:    1%{?dist}
Summary:    Telepathy Logger

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.freedesktop.org
Source:     http://telepathy.freedesktop.org/releases/telepathy-logger/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  intltool sqlite telepathy-glib
BuildRequires:  gobject-introspection
BuildRequires:  gettext xml-parser libxslt dbus-glib

%description
The Telepathy Logger package is a headless observer client that logs information received by the Telepathy framework.
It features pluggable backends to log different sorts of messages in different formats.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --libexecdir=%{_libdir}/telepathy \
            --disable-static \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_includedir}/telepathy-logger-0.2/telepathy-logger/*.h
%{_libdir}/girepository-1.0/TelepathyLogger-0.2.typelib
%{_libdir}/libtelepathy-logger.*
%{_libdir}/pkgconfig/telepathy-logger-0.2.pc
%{_libdir}/telepathy/telepathy-logger
%{_datadir}/dbus-1/services/org.freedesktop.Telepathy.Client.Logger.service
%{_datadir}/dbus-1/services/org.freedesktop.Telepathy.Logger.service
%{_datadir}/gir-1.0/TelepathyLogger-0.2.gir
%{_datadir}/glib-2.0/schemas/org.freedesktop.Telepathy.Logger.gschema.xml
%{_datadir}/telepathy/clients/Logger.client

%files doc
%defattr(-,root,root,-)
%{_datadir}/gtk-doc/html/telepathy-logger/*

%post
glib-compile-schemas %{_datadir}/glib-2.0/schemas &>/dev/null || :

%clean
rm -rf %{buildroot}

%changelog
* Fri May 10 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.4.0 to 0.8.0
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
