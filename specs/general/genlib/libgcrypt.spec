Name:       libgcrypt
Version:    1.6.1
Release:    1%{?dist}
Summary:    libgcrypt

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.gnupg.org/gcrypt
Source:     ftp://ftp.gnupg.org/gcrypt/libgcrypt/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  libgpg-error

%description
The libgcrypt package contains a general purpose crypto library based on the code used in GnuPG.
The library provides a high level interface to cryptographic building blocks using an extendable and flexible API.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

rm -f %{buildroot}/%{_infodir}/dir

%files
%defattr(-,root,root,-)
%{_bindir}/dumpsexp
%{_bindir}/hmac256
%{_bindir}/libgcrypt-config
%{_bindir}/mpicalc
%{_includedir}/gcrypt*.h
%{_libdir}/libgcrypt.*
%{_datadir}/aclocal/*
%{_infodir}/gcrypt.info*.gz
%{_mandir}/man1/hmac256.1.gz

%clean
rm -rf %{buildroot}

%changelog
* Thu Mar 20 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.6.0 to 1.6.1
* Sun Jan 5 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.5.3 to 1.6.0
* Thu Aug 22 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.5.2 to 1.5.3
* Thu May 30 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.5.1 to 1.5.2
* Thu Apr 11 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.5.0 to 1.5.1
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
