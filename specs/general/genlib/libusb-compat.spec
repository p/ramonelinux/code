Name:       libusb-compat
Version:    0.1.5
Release:    1%{?dist}
Summary:    libusb-compat

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://libusb.sourceforge.net
Source:     http://downloads.sourceforge.net/libusb/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  libusb

%description
The libusb-compat package aims to look, feel and behave exactly like libusb-0.1. It is a compatibility layer needed by packages that have not been upgraded to the libusb-1.0 API.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --disable-static \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/libusb-config
%{_includedir}/usb.h
%{_libdir}/libusb*.*
%{_libdir}/pkgconfig/libusb.pc

%clean
rm -rf %{buildroot}

%changelog
* Sun Aug 18 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.1.4 to 0.1.5
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
