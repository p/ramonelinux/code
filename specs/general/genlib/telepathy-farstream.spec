Name:       telepathy-farstream
Version:    0.6.2
Release:    1%{?dist}
Summary:    Telepathy Farstream

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.freedesktop.org
Source:     http://telepathy.freedesktop.org/releases/telepathy-farstream/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  farstream telepathy-glib
BuildRequires:  gtk-doc

%description
The Telepathy Farstream is a Telepathy client library that uses Farstream to handle Call channels.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --disable-static \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_includedir}/telepathy-1.0/telepathy-farstream/*.h
%{_libdir}/libtelepathy-farstream.*
%{_libdir}/pkgconfig/telepathy-farstream.pc

%files doc
%defattr(-,root,root,-)
%{_datadir}/gtk-doc/html/telepathy-farstream/*

%clean
rm -rf %{buildroot}

%changelog
* Wed Sep 23 2015 tanggeliang <tanggeliang@gmail.com>
- update from 0.6.0 to 0.6.2
* Sun Sep 30 2012 tanggeliang <tanggeliang@gmail.com>
- update from 0.4.0 to 0.6.0
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
