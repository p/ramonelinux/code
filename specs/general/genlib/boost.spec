Name:       boost
Version:    1.55.0b1
Release:    2%{?dist}
Summary:    Boost

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://boost.sourceforge.net
Source:     http://downloads.sourceforge.net/boost/%{name}_1_55_0b1.tar.bz2

Distribution: ramone linux
Vendor: ramone
Packager: tanggeliang <tanggeliang@gmail.com>

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires: icu python

%description
Boost provides free peer-reviewed portable C++ source libraries.

%prep
%setup -q -n %{name}_1_55_0b1

%build
./bootstrap.sh --prefix=/usr &&
./b2 stage threading=multi link=shared %{?_smp_mflags}

%check

%install
./b2 install threading=multi link=shared --prefix=%{buildroot}/usr \
                                         --libdir=%{buildroot}/%{_libdir}

%files
%defattr(-,root,root,-)
%{_includedir}/boost/*
%{_libdir}/libboost_atomic.so*
%{_libdir}/libboost_chrono.so*
%{_libdir}/libboost_context.*
%{_libdir}/libboost_coroutine.so*
%{_libdir}/libboost_date_time.so*
%{_libdir}/libboost_exception.a
%{_libdir}/libboost_filesystem.so*
%{_libdir}/libboost_graph.so*
%{_libdir}/libboost_iostreams.so*
%{_libdir}/libboost_locale.so*
%{_libdir}/libboost_log.so*
%{_libdir}/libboost_log_setup.so*
%{_libdir}/libboost_math_*.so*
%{_libdir}/libboost_prg_exec_monitor.so*
%{_libdir}/libboost_program_options.so*
%{_libdir}/libboost_python.so*
%{_libdir}/libboost_random.so*
%{_libdir}/libboost_regex.so*
%{_libdir}/libboost_serialization.so*
%{_libdir}/libboost_signals.so*
%{_libdir}/libboost_system.so*
%{_libdir}/libboost_test_exec_monitor.a
%{_libdir}/libboost_thread.so*
%{_libdir}/libboost_timer.so*
%{_libdir}/libboost_unit_test_framework.so*
%{_libdir}/libboost_wave.so*
%{_libdir}/libboost_wserialization.so*

%clean
rm -rf %{buildroot}

%changelog
* Thu Oct 10 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.54.0 to 1.55.0b1
* Wed Jul 31 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.53.0 to 1.54.0
* Sun Mar 17 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.52.0 to 1.53.0
* Sat Mar 2 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.51.0 to 1.52.0
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
