Name:       libgusb
Version:    0.2.7
Release:    1%{?dist}
Summary:    libgusb

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.freedesktop.org
Source:     http://people.freedesktop.org/~hughsient/releases/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  glib libusb
BuildRequires:  gobject-introspection systemd-gudev vala
BuildRequires:  gtk-doc

%description
The libgusb package contains the GObject wrappers for libusb-1.0 that makes it easy to do asynchronous control, bulk and interrupt transfers with proper cancellation and integration into a mainloop. 

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --disable-static \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_includedir}/gusb-1/gusb*.h
%{_includedir}/gusb-1/gusb/gusb-*.h
%{_libdir}/girepository-1.0/GUsb-1.0.typelib
%{_libdir}/libgusb.la
%{_libdir}/libgusb.so*
%{_libdir}/pkgconfig/gusb.pc
%{_datadir}/gir-1.0/GUsb-1.0.gir
%{_datadir}/vala/vapi/gusb.vapi

%files doc
%defattr(-,root,root,-)
%{_datadir}/gtk-doc/html/gusb/*

%clean
rm -rf %{buildroot}

%changelog
* Wed Sep 23 2015 tanggeliang <tanggeliang@gmail.com>
- update from 0.2.6 to 0.2.7
* Sat Mar 28 2015 tanggeliang <tanggeliang@gmail.com>
- update from 0.1.6 to 0.2.4
* Wed Apr 10 2013 tanggeliang <tanggeliang@gmail.com>
- create
