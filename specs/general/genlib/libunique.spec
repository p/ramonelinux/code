Name:       libunique
Version:    3.0.2
Release:    4%{?dist}
Summary:    libunique

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/libunique/3.0/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  gtk+ gobject-introspection

%description
The libunique package contains a library for writing single instance applications.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_includedir}/unique-3.0/unique/unique*.h
%{_libdir}/girepository-1.0/*
%{_libdir}/libunique-3.0.*
%{_libdir}/pkgconfig/unique-3.0.pc
%{_datadir}/gir-1.0/*
%{_datadir}/gtk-doc/*

%clean
rm -rf %{buildroot}

%changelog
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
