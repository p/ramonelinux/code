Name:       libgsf
Version:    1.14.34
Release:    1%{?dist}
Summary:    libgsf

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/libgsf/1.14/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  glib intltool libxml
BuildRequires:  gdk-pixbuf gobject-introspection
BuildRequires:  gettext xml-parser

%description
The libgsf package contains the library used for providing an extensible input/output abstraction layer for structured file formats.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --disable-static \
            --enable-introspection \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/gsf*
%{_includedir}/libgsf-1/gsf*
%{_libdir}/libgsf-*
%{_libdir}/girepository-1.0/Gsf-1.typelib
%{_libdir}/pkgconfig/libgsf-*.pc
%{_datadir}/gir-1.0/Gsf-1.gir
%{_datadir}/gtk-doc/*
%{_datadir}/locale/*
%{_datadir}/thumbnailers/gsf-office.thumbnailer
%{_mandir}/*/*

%clean
rm -rf %{buildroot}

%changelog
* Sat Mar 29 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.14.30 to 1.14.34
* Sat Mar 22 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.14.29 to 1.14.30
* Thu Mar 20 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.14.28 to 1.14.29
* Tue Oct 22 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.14.23 to 1.14.28
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
