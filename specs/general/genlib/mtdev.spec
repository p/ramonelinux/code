Name:       mtdev
Version:    1.1.5
Release:    1%{?dist}
Summary:    mtdev

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://bitmath.org
Source:     http://bitmath.org/code/mtdev/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build

%description
The mtdev package contains Multitouch Protocol Translation Library which is used to transform all variants of kernel MT (Multitouch) events to the slotted type B protocol.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --disable-static \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/mtdev-test
%{_includedir}/mtdev*.h
%{_libdir}/libmtdev.la
%{_libdir}/libmtdev.so*
%{_libdir}/pkgconfig/mtdev.pc

%clean
rm -rf %{buildroot}

%changelog
* Tue Mar 18 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.1.4 to 1.1.5
* Sun Aug 18 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.1.3 to 1.1.4
* Sun Oct 28 2012 tanggeliang <tanggeliang@gmail.com>
- update from 1.1.2 to 1.1.3
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
