Name:       talloc
Version:    2.0.7
Release:    4%{?dist}
Summary:    talloc

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://samba.org/ftp/talloc
Source:     http://samba.org/ftp/talloc/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build

%description
talloc provides a hierarchical, reference counted memory pool system with destructors. It is the core memory allocator used in Samba and MesaLib.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_includedir}/*
%{_libdir}/libtalloc.*
%{_libdir}/libpytalloc-util.*
%{_libdir}/pkgconfig/talloc.pc
%{_libdir}/pkgconfig/pytalloc-util.pc
%{_libdir}/python2.7/site-packages/talloc.so

%clean
rm -rf %{buildroot}

%changelog
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
