Name:       libevdev
Version:    1.4.4
Release:    1%{?dist}
Summary:    libevdev

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.freedesktop.org
Source:     http://www.freedesktop.org/software/libevdev/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build

%description
libevdev is a wrapper library for evdev devices.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/libevdev-tweak-device
%{_bindir}/mouse-dpi-tool
%{_bindir}/touchpad-edge-detector
%{_includedir}/libevdev-1.0/libevdev/libevdev*.h
%{_libdir}/libevdev.*a
%{_libdir}/libevdev.so*
%{_libdir}/pkgconfig/libevdev.pc
%{_mandir}/man3/libevdev.3.gz

%clean
rm -rf %{buildroot}

%changelog
* Mon Sep 14 2015 tanggeliang <tanggeliang@gmail.com>
- update from 1.2.2 to 1.4.4
* Wed Jul 23 2014 tanggeliang <tanggeliang@gmail.com>
- update from 0.4 to 1.2.2
* Sun Sep 29 2013 tanggeliang <tanggeliang@gmail.com>
- create
