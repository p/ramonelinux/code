Name:       libcroco
Version:    0.6.8
Release:    2%{?dist}
Summary:    libcroco

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/libcroco/0.6/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  glib libxml gtk-doc

%description
The libcroco package contains a standalone CSS2 parsing and manipulation library.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --disable-static \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/croco-0.6-config
%{_bindir}/csslint-0.6
%{_includedir}/libcroco-0.6/libcroco/*
%{_libdir}/libcroco-0.6.*
%{_libdir}/pkgconfig/libcroco-0.6.pc

%files doc
%defattr(-,root,root,-)
%{_datadir}/gtk-doc/html/libcroco/*

%clean
rm -rf %{buildroot}

%changelog
* Sat Nov 3 2012 tanggeliang <tanggeliang@gmail.com>
- update from 0.6.6 to 0.6.8
* Sun Oct 14 2012 tanggeliang <tanggeliang@gmail.com>
- update from 0.6.5 to 0.6.6
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
