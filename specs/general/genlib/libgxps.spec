Name:       libgxps
Version:    0.2.2
Release:    4%{?dist}
Summary:    libgxps

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/libgxps/0.2/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  cairo libarchive
BuildRequires:  lcms2 libjpeg libpng libtiff
BuildRequires:  gobject-introspection gtk-doc libxslt nettle

%description
The libgxps package contains GObject based library and utilities for handling and rendering XPS documents.

%prep
%setup -q -n %{name}-%{version}

%build
sed -i '/_gir_CFLAGS/s#$# -I/usr/include/cairo#' libgxps/Makefile.in &&
./configure --prefix=/usr --disable-static &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/xpstojpeg
%{_bindir}/xpstopdf
%{_bindir}/xpstopng
%{_bindir}/xpstops
%{_bindir}/xpstosvg
%{_includedir}/libgxps/gxps*.h
%{_libdir}/girepository-1.0/GXPS-0.1.typelib
%{_libdir}/libgxps.*
%{_libdir}/pkgconfig/libgxps.pc
%{_datadir}/gir-1.0/GXPS-0.1.gir
%{_datadir}/gtk-doc/html/libgxps/*

%clean
rm -rf %{buildroot}

%changelog
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
