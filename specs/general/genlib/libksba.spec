Name:       libksba
Version:    1.3.0
Release:    2%{?dist}
Summary:    Libksba

Group:      System Environment/libraries
License:    GPLv2+
Url:        ftp://www.gnupg.org
Source:     ftp://ftp.gnupg.org/gcrypt/libksba/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  libgpg-error

%description
The Libksba package contains a library used to make X.509 certificates as well as making the CMS (Cryptographic Message Syntax) easily accessible by other applications. Both specifications are building blocks of S/MIME and TLS. The library does not rely on another cryptographic library but provides hooks for easy integration with Libgcrypt.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

rm -rf %{buildroot}/%{_infodir}/dir

%files
%defattr(-,root,root,-)
%{_bindir}/ksba-config
%{_includedir}/ksba.h
%{_libdir}/libksba.la
%{_libdir}/libksba.so*
%{_datadir}/aclocal/ksba.m4

%files doc
%defattr(-,root,root,-)
%{_infodir}/ksba.info.gz

%clean
rm -rf %{buildroot}

%changelog
* Wed Nov 7 2012 tanggeliang <tanggeliang@gmail.com>
- update from 1.2.0 to 1.3.0
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
