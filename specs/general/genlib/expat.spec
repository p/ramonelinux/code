Name:       expat
Version:    2.1.0
Release:    8%{?dist}
Summary:    Expat

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://expat.sourceforge.net
Source:     http://downloads.sourceforge.net/expat/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build

%description
The Expat package contains a stream oriented C library for parsing XML. 

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/xmlwf
%{_includedir}/expat*.h
%{_libdir}/libexpat.*a
%{_libdir}/libexpat.so*
%{_libdir}/pkgconfig/expat.pc

%files doc
%defattr(-,root,root,-)
%{_mandir}/man1/xmlwf.1.gz

%clean
rm -rf %{buildroot}

%changelog
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
