Name:       libical
Version:    1.0.1
Release:    1%{?dist}
Summary:    libical

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.sourceforge.net
Source:     http://downloads.sourceforge.net/freeassociation/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  cmake

%description
The libical package is an implementation of iCalendar protocols and data formats.

%prep
%setup -q -n %{name}-%{version}

%build
mkdir build &&
cd build &&
cmake -DCMAKE_INSTALL_PREFIX=/usr \
      -DCMAKE_BUILD_TYPE=Release  \
%ifarch x86_64
      -DLIB_SUFFIX=64 \
%endif
      .. &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
cd build &&
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_includedir}/ical.h
%{_includedir}/libical/*.h
%{_libdir}/libical.*
%{_libdir}/libicalss.*
%{_libdir}/libicalvcal.*
%{_libdir}/pkgconfig/libical.pc
%{_libdir}/cmake/LibIcal/LibIcal*.cmake

%clean
rm -rf %{buildroot}

%changelog
* Wed Sep 16 2015 tanggeliang <tanggeliang@gmail.com>
- update from 1.0 to 1.0.1
* Wed Aug 21 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.48 to 1.0
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
