Name:       libatomic_ops
Version:    7.4.2
Release:    1%{?dist}
Summary:    libatomic_ops

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.hpl.hp.com/research/linux/atomic_ops
Source:     http://www.hpl.hp.com/research/linux/atomic_ops/download/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  autoconf automake m4 libtool

%description
libatomic_ops provides implementations for atomic memory update operations on a number of architectures. This allows direct use of these in reasonably portable code.
Unlike earlier similar packages, this one explicitly considers memory barrier semantics, and allows the construction of code that involves minimum overhead across a variety of architectures.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
sed -i 's#pkgdata#doc#' doc/Makefile.am &&
autoreconf -i &&
./configure --prefix=/usr    \
            --enable-shared  \
            --disable-static \
            --docdir=%{_datadir}/doc/libatomic_ops-%{version} \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_includedir}/*
%{_libdir}/libatomic_ops*.*
%{_libdir}/pkgconfig/atomic_ops.pc
%{_datadir}/libatomic_ops/*

%files doc
%defattr(-,root,root,-)
%{_docdir}/libatomic_ops-%{version}/*

%clean
rm -rf %{buildroot}

%changelog
* Sat Jul 26 2014 tanggeliang <tanggeliang@gmail.com>
- update from 7.4.0 to 7.4.2
* Tue Mar 18 2014 tanggeliang <tanggeliang@gmail.com>
- update from 7.2e to 7.4.0
* Thu Dec 5 2013 tanggeliang <tanggeliang@gmail.com>
- update from 7.2d to 7.2e
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
