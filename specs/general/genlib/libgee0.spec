Name:       libgee0
Version:    0.6.8
Release:    1%{?dist}
Summary:    libgee

Group:      User Interface/GNOME
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/%{name}/0.6/libgee-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  glib gobject-introspection vala

%description
The libgee package is a collection library providing GObject based interfaces and classes for commonly used data structures.

%prep
%setup -q -n libgee-%{version}

%build
./configure --prefix=/usr \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_includedir}/gee-1.0/gee.h
%{_libdir}/girepository-1.0/Gee-1.0.typelib
%{_libdir}/libgee.la
%{_libdir}/libgee.so*
%{_libdir}/pkgconfig/gee-1.0.pc
%{_datadir}/gir-1.0/Gee-1.0.gir
%{_datadir}/vala/vapi/gee-1.0.vapi

%clean
rm -rf %{buildroot}

%changelog
* Wed Oct 2 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.6.7 to 0.6.8
* Wed Jun 19 2013 tanggeliang <tanggeliang@gmail.com>
- create
