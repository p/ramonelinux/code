Name:       libqmi
Version:    1.12.6
Release:    1%{?dist}
Summary:    libqmi

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.freedesktop.org
Source:     http://www.freedesktop.org/software/libqmi/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  glib gtk-doc

%description
libqmi is a glib-based library for talking to WWAN modems and devices which speak the Qualcomm MSM Interface (QMI) protocol.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
	    --libexecdir=%{_libdir}/%{name} \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/qmi-network
%{_bindir}/qmicli
%{_includedir}/libqmi-glib/libqmi-glib.h
%{_includedir}/libqmi-glib/qmi-*.h
%{_libdir}/libqmi-glib.*
%{_libdir}/pkgconfig/qmi-glib.pc
%{_libdir}/%{name}/qmi-proxy
%{_datadir}/gtk-doc/html/libqmi-glib/*
%{_mandir}/man1/qmi-network.1.gz
%{_mandir}/man1/qmicli.1.gz

%clean
rm -rf %{buildroot}

%changelog
* Wed Sep 16 2015 tanggeliang <tanggeliang@gmail.com>
- create
