Name:       lloyd-yajl
Version:    2.1.0
Release:    1%{?dist}
Summary:    Yet Another JSON Library

Group:      Development/Libraries
License:    BSD and LGPLv2+
Url:        http://lloyd.github.io/yajl
Source:     http://github.com/lloyd/yajl/tarball/2.1.0/%{name}-%{version}-0-ga0ecdde.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:	cmake doxygen

%description
YAJL is a small event-driven (SAX-style) JSON parser written in ANSI C, and a small validating JSON generator.
YAJL is released under the ISC license.

%prep
%setup -q -n %{name}-66cb08c

%build
mkdir build &&
cd build &&
cmake -DCMAKE_INSTALL_PREFIX=/usr \
      -DCMAKE_BUILD_TYPE=Release \
%ifarch x86_64
      -DLIB_SUFFIX=64 \
%endif
      .. &&
make -j1

%install
rm -rf %{buildroot}
cd build
make install DESTDIR=%{buildroot}

%files
%defattr(-,root,root,-)
%{_bindir}/json_reformat
%{_bindir}/json_verify
%{_includedir}/yajl/yajl_*.h
%{_libdir}/libyajl.so*
%{_libdir}/libyajl_s.a
%{_datadir}/pkgconfig/yajl.pc

%changelog
* Thu Mar 26 2015 tanggeliang <tanggeliang@gmail.com>
- create
