Name:       popt
Version:    1.16
Release:    8%{?dist}
Summary:    Popt

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://rpm5.org/files/popt
Source:     http://rpm5.org/files/popt/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build

%description
The popt package contains the popt libraries which are used by some programs to parse command-line options.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_includedir}/popt.h
%{_libdir}/libpopt.*a
%{_libdir}/libpopt.so*
/usr/lib/pkgconfig/popt.pc
%{_datadir}/locale/*/LC_MESSAGES/popt.mo

%files doc
%defattr(-,root,root,-)
%{_mandir}/man3/popt.3.gz

%clean
rm -rf %{buildroot}

%changelog
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
