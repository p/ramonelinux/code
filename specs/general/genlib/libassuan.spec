Name:       libassuan
Version:    2.1.1
Release:    1%{?dist}
Summary:    Libassuan

Group:      System Environment/libraries
License:    GPLv2+
Url:        ftp://www.gnupg.org
Source:     ftp://ftp.gnupg.org/gcrypt/libassuan/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  libgpg-error

%description
The Libassuan package contains an inter process communication library used by some of the other GnuPG related packages. Libassuan's primary use is to allow a client to interact with a non-persistent server.
Libassuan is not, however, limited to use with GnuPG servers and clients.
It was designed to be flexible enough to meet the demands of many transaction based environments with non-persistent servers.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --libdir=%{_libdir} &&
make %{?_smp_mflags} &&

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

rm -rf %{buildroot}/%{_infodir}/dir

%files
%defattr(-,root,root,-)
%{_bindir}/libassuan-config
%{_includedir}/assuan.h
%{_libdir}/libassuan.la
%{_libdir}/libassuan.so*
%{_datadir}/aclocal/libassuan.m4
%{_infodir}/assuan.info.gz

%clean
rm -rf %{buildroot}

%changelog
* Wed Aug 21 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.1.0 to 2.1.1
* Mon Apr 1 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.0.3 to 2.1.0
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
