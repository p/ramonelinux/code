Name:       gobject-introspection
Version:    1.46.0
Release:    1%{?dist}
Summary:    GObject Introspection

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/gobject-introspection/1.46/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  glib
BuildRequires:  bison flex libffi

%description
The GObject Introspection is used to describe the program APIs and collect them in a uniform, machine readable format. 

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --disable-static \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/g-ir-compiler
%{_bindir}/g-ir-generate
%{_bindir}/g-ir-scanner
%{_bindir}/g-ir-annotation-tool
%{_includedir}/gobject-introspection-1.0/*
%{_libdir}/girepository-1.0/*
%{_libdir}/gobject-introspection/*
%{_libdir}/libgirepository-1.0.*
%{_libdir}/pkgconfig/gobject-introspection-*.pc
%{_datadir}/aclocal/*
%{_datadir}/gir-1.0/*
%{_datadir}/gobject-introspection-1.0/*

%files doc
%defattr(-,root,root,-)
%{_mandir}/*/*

%clean
rm -rf %{buildroot}

%changelog
* Tue Sep 22 2015 tanggeliang <tanggeliang@gmail.com>
- update from 1.45.4 to 1.46.0
* Tue Sep 15 2015 tanggeliang <tanggeliang@gmail.com>
- update from 1.42.0 to 1.45.4
* Tue Sep 23 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.41.4 to 1.42.0
* Tue Jul 22 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.40.0 to 1.41.4
* Thu Mar 27 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.39.90 to 1.40.0
* Sat Mar 15 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.38.0 to 1.39.90
* Mon Oct 7 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.37.6 to 1.38.0
* Tue Sep 24 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.37.4 to 1.37.6
* Wed Jul 24 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.36.0 to 1.37.4
* Fri Apr 12 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.34.2 to 1.36.0
* Mon Nov 26 2012 tanggeliang <tanggeliang@gmail.com>
- update from 1.34.1.1 to 1.34.2
* Mon Oct 22 2012 tanggeliang <tanggeliang@gmail.com>
- update from 1.34.0 to 1.34.1.1
* Fri Sep 28 2012 tanggeliang <tanggeliang@gmail.com>
- update from 1.32.1 to 1.34.0
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
