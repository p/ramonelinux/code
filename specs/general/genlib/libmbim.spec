Name:       libmbim
Version:    1.12.2
Release:    1%{?dist}
Summary:    libmbim

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.freedesktop.org
Source:     http://www.freedesktop.org/software/libmbim/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  glib gtk-doc systemd-gudev

%description
libmbim is a glib-based library for talking to WWAN modems and devices which speak the Mobile Interface Broadband Model (MBIM) protocol.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
	    --libexecdir=%{_libdir}/%{name} \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/mbim-network
%{_bindir}/mbimcli
%{_includedir}/libmbim-glib/libmbim-glib.h
%{_includedir}/libmbim-glib/mbim-*.h
%{_libdir}/libmbim-glib.*
%{_libdir}/pkgconfig/mbim-glib.pc
%{_libdir}/%{name}/mbim-proxy
%{_datadir}/gtk-doc/html/libmbim-glib/*
%{_mandir}/man1/mbim-network.1.gz
%{_mandir}/man1/mbimcli.1.gz

%clean
rm -rf %{buildroot}

%changelog
* Wed Sep 16 2015 tanggeliang <tanggeliang@gmail.com>
- create
