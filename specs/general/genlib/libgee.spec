Name:       libgee
Version:    0.18.0
Release:    1%{?dist}
Summary:    libgee

Group:      User Interface/GNOME
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/%{name}/0.18/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  glib gobject-introspection vala

%description
The libgee package is a collection library providing GObject based interfaces and classes for commonly used data structures.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_includedir}/gee-0.8/gee.h
%{_libdir}/girepository-1.0/Gee-0.8.typelib
%{_libdir}/libgee-0.8.*
%{_libdir}/pkgconfig/gee-0.8.pc
%{_datadir}/gir-1.0/Gee-0.8.gir
%{_datadir}/vala/vapi/gee-0.8.vapi

%clean
rm -rf %{buildroot}

%changelog
* Wed Sep 23 2015 tanggeliang <tanggeliang@gmail.com>
- update from 0.16.0 to 0.18.0
* Wed Sep 24 2014 tanggeliang <tanggeliang@gmail.com>
- update from 0.13.91 to 0.16.0
* Thu Mar 20 2014 tanggeliang <tanggeliang@gmail.com>
- update from 0.12.0 to 0.13.91
* Wed Sep 25 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.10.1 to 0.12.0
* Fri Apr 12 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.6.7 to 0.10.1
* Thu Apr 11 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.6.6.1 to 0.6.7
* Mon Nov 26 2012 tanggeliang <tanggeliang@gmail.com>
- update from 0.6.6 to 0.6.6.1
* Sat Nov 3 2012 tanggeliang <tanggeliang@gmail.com>
- update from 0.6.5 to 0.6.6
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
