Name:       pth
Version:    2.0.7
Release:    6%{?dist}
Summary:    Pth

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.gnu.org
Source:     http://ftp.gnu.org/gnu/pth/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build

%description
The Pth package contains a very portable POSIX/ANSI-C based library for Unix platforms which provides non-preemptive priority-based scheduling for multiple threads of execution (multithreading) inside event-driven applications.
All threads run in the same address space of the server application, but each thread has its own individual program-counter, run-time stack, signal mask and errno variable.

%prep
%setup -q -n %{name}-%{version}

%build
sed -i 's#$(LOBJS): Makefile#$(LOBJS): pth_p.h Makefile#' Makefile.in &&
./configure --prefix=/usr           \
            --disable-static        \
            --mandir=/usr/share/man \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install &&
install -v -m755 -d %{buildroot}/usr/share/doc/pth-2.0.7 &&
install -v -m644    README PORTING SUPPORT TESTS \
                    %{buildroot}/usr/share/doc/pth-2.0.7

%files
%defattr(-,root,root,-)
%{_bindir}/pth-config
%{_includedir}/pth.h
%{_libdir}/libpth.la
%{_libdir}/libpth.so*
%{_datadir}/aclocal/pth.m4
%{_docdir}/pth-2.0.7/*
%{_mandir}/man1/pth-config.1.gz
%{_mandir}/man3/pth.3.gz

%clean
rm -rf %{buildroot}

%changelog
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
