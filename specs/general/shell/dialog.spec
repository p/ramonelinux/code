Name:       dialog
Version:    1.1
Release:    1%{?dist}
Summary:    dialog

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://invisible-island.net/dialog/dialog.html
Source:     ftp://invisible-island.net/dialog/%{name}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  ncurses

%description

%prep
%setup -q -n %{name}-%{version}-20110302

%build
./configure --prefix=/usr --mandir=/usr/share/man
make

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/dialog
%{_mandir}/*/*

%clean
rm -rf %{buildroot}

%changelog
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
