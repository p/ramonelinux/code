Name:       dash
Version:    0.5.7
Release:    6%{?dist}
Summary:    Dash

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://gondor.apana.org.au
Source:     http://gondor.apana.org.au/~herbert/dash/files/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build

%description
Dash is a POSIX compliant shell.
It can be installed as /bin/sh or as the default shell for either root or a second user with a userid of 0.
It depends on fewer libraries than the Bash shell and is therefore less likely to be affected by an upgrade problem or disk failure.
Dash is also useful for checking that a script is completely compatible with POSIX syntax.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/dash
%{_mandir}/man1/dash.1.gz

%clean
rm -rf %{buildroot}

%changelog
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
