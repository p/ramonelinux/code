Name:       poppler
Version:    0.37.0
Release:    1%{?dist}
Summary:    Poppler

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.freedesktop.org
Source:     http://poppler.freedesktop.org/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  fontconfig
BuildRequires:  cairo libjpeg-turbo libpng
BuildRequires:  curl gtk2 lcms2 libtiff libxml openjpeg glib
BuildRequires:  autoconf automake m4 gettext gobject-introspection
Requires:       poppler-data

%description
The Poppler package contains a PDF rendering library and command line tools used to manipulate PDF files.
This is useful for providing PDF rendering functionality as a shared library.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --sysconfdir=/etc \
            --disable-static \
            --enable-xpdf-headers \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install &&
install -v -m755 -d      %{buildroot}/usr/share/doc/poppler-%{version} &&
install -v -m644 README* %{buildroot}/usr/share/doc/poppler-%{version}

%files
%defattr(-,root,root,-)
%{_bindir}/pdfdetach
%{_bindir}/pdffonts
%{_bindir}/pdfimages
%{_bindir}/pdfinfo
%{_bindir}/pdfseparate
%{_bindir}/pdftocairo
%{_bindir}/pdftohtml
%{_bindir}/pdftoppm
%{_bindir}/pdftops
%{_bindir}/pdftotext
%{_bindir}/pdfunite
%{_includedir}/poppler/*
%{_libdir}/girepository-1.0/Poppler-0.18.typelib
%{_libdir}/libpoppler-cpp.*
%{_libdir}/libpoppler-glib.*
%{_libdir}/libpoppler.*
%{_libdir}/pkgconfig/poppler-cairo.pc
%{_libdir}/pkgconfig/poppler-cpp.pc
%{_libdir}/pkgconfig/poppler-glib.pc
%{_libdir}/pkgconfig/poppler-splash.pc
%{_libdir}/pkgconfig/poppler.pc
%{_datadir}/gir-1.0/Poppler-0.18.gir

%files doc
%defattr(-,root,root,-)
%{_docdir}/poppler-%{version}/*
%{_datadir}/gtk-doc/html/poppler/*
%{_mandir}/man1/pdf*.1.gz

%clean
rm -rf %{buildroot}

%changelog
* Sat Oct 17 2015 tanggeliang <tanggeliang@gmail.com>
- update from 0.24.5 to 0.37.0
* Fri Mar 21 2014 tanggeliang <tanggeliang@gmail.com>
- update from 0.24.4 to 0.24.5
* Tue Dec 3 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.24.2 to 0.24.4
* Mon Oct 7 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.24.1 to 0.24.2
* Wed Oct 2 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.24.0 to 0.24.1
* Tue Aug 20 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.22.5 to 0.24.0
* Thu Jul 25 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.22.1 to 0.22.5
* Thu Mar 21 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.20.5 to 0.22.1
* Fri Nov 2 2012 tanggeliang <tanggeliang@gmail.com>
- update from 0.20.4 to 0.20.5
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
