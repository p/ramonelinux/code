Name:       cantarell-fonts
Version:    0.0.16
Release:    1%{?dist}
Summary:    Cantarell Fonts

Group:      User Interface/GNOME
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/%{name}/0.0/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildArch:      noarch
BuildRequires:  fontconfig

%description
The Cantarell typeface family is a contemporary Humanist sans serif, and is used by the GNOME project for its user interface.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_datadir}/fontconfig/conf.avail/31-cantarell.conf
%{_datadir}/fonts/cantarell/Cantarell-Bold.otf
%{_datadir}/fonts/cantarell/Cantarell-Regular.otf

%clean
rm -rf %{buildroot}

%changelog
* Wed Sep 24 2014 tanggeliang <tanggeliang@gmail.com>
- update from 0.0.15 to 0.0.16
* Thu Oct 10 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.0.13 to 0.0.15
* Wed Aug 7 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.0.9 to 0.0.13
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
