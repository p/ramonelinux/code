Name:       harfbuzz
Version:    1.0.3
Release:    1%{?dist}
Summary:    Harfbuzz

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.freedesktop.org
Source:     http://www.freedesktop.org/software/harfbuzz/release/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  cairo glib icu freetype

%description
The Harfbuzz package contains an OpenType text shaping engine.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --with-gobject \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/hb-ot-shape-closure
%{_bindir}/hb-shape
%{_bindir}/hb-view
%{_includedir}/harfbuzz/hb-*.h
%{_includedir}/harfbuzz/hb.h
%{_libdir}/libharfbuzz.la
%{_libdir}/libharfbuzz.so*
%{_libdir}/libharfbuzz-gobject.la
%{_libdir}/libharfbuzz-gobject.so*
%{_libdir}/libharfbuzz-icu.la
%{_libdir}/libharfbuzz-icu.so*
%{_libdir}/pkgconfig/harfbuzz.pc
%{_libdir}/pkgconfig/harfbuzz-gobject.pc
%{_libdir}/pkgconfig/harfbuzz-icu.pc
%{_datadir}/gtk-doc/html/harfbuzz/*

%clean
rm -rf %{buildroot}

%changelog
* Tue Sep 15 2015 tanggeliang <tanggeliang@gmail.com>
- update from 0.9.32 to 1.0.3
* Sun Jul 20 2014 tanggeliang <tanggeliang@gmail.com>
- update from 0.9.27 to 0.9.32
* Thu Mar 20 2014 tanggeliang <tanggeliang@gmail.com>
- update from 0.9.26 to 0.9.27
* Sun Mar 16 2014 tanggeliang <tanggeliang@gmail.com>
- update from 0.9.25 to 0.9.26
* Sun Dec 15 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.9.24 to 0.9.25
* Tue Dec 3 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.9.22 to 0.9.24
* Mon Oct 7 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.9.21 to 0.9.22
* Tue Sep 24 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.9.19 to 0.9.21
* Mon Jul 29 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.9.15 to 0.9.19
* Thu Apr 11 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.9.13 to 0.9.15
* Sun Mar 17 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.9.5 to 0.9.13
* Sat Nov 3 2012 tanggeliang <tanggeliang@gmail.com>
- update from 0.9.4 to 0.9.5
* Fri Sep 28 2012 tanggeliang <tanggeliang@gmail.com>
- create
