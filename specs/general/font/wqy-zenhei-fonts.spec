%global fontname wqy-zenhei
%global fontconf1 65-0-%{fontname}.conf
%global fontconf2 65-0-%{fontname}-sharp.conf

%define setscript zenheiset

Name:       %{fontname}-fonts
Version:    0.9.46
Release:    8%{?dist}
Summary:    WenQuanYi Zen Hei CJK Font

Group:      User Interface/X
License:    GPLv2 with exceptions
Url:        http://wenq.org/enindex.cgi
Source0:    http://downloads.sourceforge.net/wqy/%{fontname}-%{version}-May.tar.bz2
Source1:    %{fontconf1}
Source2:    %{fontconf2}
Source3:    %{setscript}

BuildRoot:      %(mktemp -ud %{_tmppath}/%{name}-%{version}-%{release}-XXXXXX)
BuildArch:      noarch
BuildRequires:  fontpackages
Requires:       fontpackages

%description
WenQuanYi Zen Hei is a Hei-Ti style (sans-serif type) Chinese outline font.
It is designed for general purpose text formatting and on-screen display of Chinese characters and symbols from many other languages.
The embolden strokes of the font glyphs produces enhanced screen contrast, making it easier to read recognize.
The embedded bitmap glyphs further enhance on-screen performance, which can be enabled with the provided configuration files.
WenQuanYi Zen Hei provides a rather complete coverage to Chinese Hanzi glyphs, including both simplified and traditional forms.
The total glyph number in this font is over 35,000, including over 21,000 Chinese Hanzi.
This font has full coverage to GBK(CP936) charset, CJK Unified Ideographs, as well as the code-points needed for zh_cn, zh_sg, zh_tw, zh_hk, zh_mo, ja (Japanese) and ko (Korean) locales for fontconfig.
Starting from version 0.8, this font package has contained two font families, i.e. the proportionally-spaced Zen Hei, and a mono-spaced face named "WenQuanYi Zen Hei Mono".

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{fontname}

%build
%{nil}

%install
rm -fr %{buildroot}

install -m 0755 -d %{buildroot}%{_fontdir}
install -m 0644 -p *.ttc %{buildroot}%{_fontdir}

install -m 0755 -d %{buildroot}%{_fontconfig_templatedir} \
                   %{buildroot}%{_fontconfig_confdir}

install -m 0644 -p %{SOURCE1} \
        %{buildroot}%{_fontconfig_templatedir}/%{fontconf1}
install -m 0644 -p %{SOURCE2} \
        %{buildroot}%{_fontconfig_templatedir}/%{fontconf2}

ln -s %{_fontconfig_templatedir}/%{fontconf2} \
      %{buildroot}%{_fontconfig_confdir}/%{fontconf2}

install -m 0755 -d %{buildroot}%{_bindir}

install -m 0744 -p %{SOURCE3} \
        %{buildroot}%{_bindir}/%{setscript}

%clean
rm -fr %{buildroot}

%files
%defattr(-,root,root,-)
/etc/fonts/conf.d/65-0-wqy-zenhei-sharp.conf
%attr(755, root, root) %{_bindir}/%{setscript}
%{_datadir}/fontconfig/conf.avail/65-0-wqy-zenhei-sharp.conf
%{_datadir}/fontconfig/conf.avail/65-0-wqy-zenhei.conf
%{_datadir}/fonts/wqy-zenhei/wqy-zenhei.ttc
%dir %{_fontdir}

%files doc
%defattr(-,root,root,-)
%doc AUTHORS ChangeLog COPYING README

%changelog
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
