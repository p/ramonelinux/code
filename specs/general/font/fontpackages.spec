%global spectemplatedir %{_sysconfdir}/rpmdevtools/
%global ftcgtemplatedir %{_datadir}/fontconfig/templates/
%global rpmmacrodir     %{_sysconfdir}/rpm/

Name:       fontpackages
Version:    1.44
Release:    8%{?dist}
Summary:    Common directory and macro definitions used by font packages

Group:      Development/System
License:    LGPLv3+
Url:        http://fedoraproject.org/wiki/fontpackages
Source:     http://fedorahosted.org/releases/f/o/%{name}/%{name}-%{version}.tar.xz
BuildArch:  noarch
AutoReq:    no

%description
This package contains the basic directory layout, spec templates, rpm macros and other materials used to create font packages.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q
%if 0%{?rhel}
sed -i 's|/usr/bin/fedoradev-pkgowners|""|g' bin/repo-font-audit
%endif

%build
for file in bin/repo-font-audit bin/compare-repo-font-audit ; do
sed -i "s|^DATADIR\([[:space:]]*\)\?=\(.*\)$|DATADIR=%{_datadir}/%{name}|g" \
  $file
done

%install
rm -fr %{buildroot}

# Pull macros out of macros.fonts and emulate them during install
for dir in fontbasedir        fontconfig_masterdir \
           fontconfig_confdir fontconfig_templatedir ; do
  export _${dir}=$(rpm --eval $(%{__grep} -E "^%_${dir}\b" \
    rpm/macros.fonts | %{__awk} '{ print $2 }'))
done

install -m 0755 -d %{buildroot}${_fontbasedir} \
                   %{buildroot}${_fontconfig_masterdir} \
                   %{buildroot}${_fontconfig_confdir} \
                   %{buildroot}${_fontconfig_templatedir} \
                   %{buildroot}%{spectemplatedir} \
                   %{buildroot}%{rpmmacrodir} \
                   %{buildroot}%{_datadir}/fontconfig/templates \
                   %{buildroot}/%_datadir/%{name} \
                   %{buildroot}%{_bindir}
install -m 0644 -p spec-templates/*.spec       %{buildroot}%{spectemplatedir}
install -m 0644 -p fontconfig-templates/*      %{buildroot}%{ftcgtemplatedir}
install -m 0644 -p rpm/macros*                 %{buildroot}%{rpmmacrodir}
install -m 0644 -p private/repo-font-audit.mk  %{buildroot}/%{_datadir}/%{name}
install -m 0755 -p private/core-fonts-report \
                   private/font-links-report \
                   private/fonts-report \
                   private/process-fc-query \
                   private/test-info           %{buildroot}/%{_datadir}/%{name}
install -m 0755 -p bin/*                       %{buildroot}%{_bindir}

cat <<EOF > %{name}-%{version}.files
%defattr(0644,root,root,0755)
%dir ${_fontbasedir}
%dir ${_fontconfig_masterdir}
%dir ${_fontconfig_confdir}
%dir ${_fontconfig_templatedir}
EOF

%clean
rm -fr %{buildroot}

%files
%defattr(0644,root,root,0755)
%{_bindir}/compare-repo-font-audit
%{_bindir}/filter-fontlint
%{_bindir}/filter-langcover
%{_bindir}/filter-unicover
%{_bindir}/fix-font-naming
%{_bindir}/repo-font-audit
%{_bindir}/ttfcoverage
%dir %{_datadir}/fontconfig
%config(noreplace) %{spectemplatedir}/*.spec
%config(noreplace) %{rpmmacrodir}/macros*
%dir %{ftcgtemplatedir}
%{ftcgtemplatedir}/*conf
%{ftcgtemplatedir}/*txt
%dir %{_datadir}/%{name}
%{_datadir}/%{name}/repo-font-audit.mk
%defattr(0755,root,root,0755)
%{_datadir}/%{name}/core-fonts-report
%{_datadir}/%{name}/font-links-report
%{_datadir}/%{name}/fonts-report
%{_datadir}/%{name}/process-fc-query
%{_datadir}/%{name}/test-info

%files doc
%defattr(0644,root,root,0755)
%doc license.txt readme.txt

%changelog
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
