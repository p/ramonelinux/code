Name:       poppler-data
Version:    0.4.7
Release:    1%{?dist}
Summary:    Poppler Encoding Data

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.freedesktop.org
Source:     http://poppler.freedesktop.org/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildArch:      noarch

%description
The additional package consists of encoding files for use with Poppler.
The encoding files are optional and Poppler will automatically read them if they are present.
When installed, they enable Poppler to render CJK and Cyrillic properly. 

%prep
%setup -q -n %{name}-%{version}

%build

%check

%install
rm -rf %{buildroot}
make prefix=/usr install DESTDIR=%{buildroot}

%files
%defattr(-,root,root,-)
%{_datadir}/poppler/cMap/Adobe-*/*
%{_datadir}/poppler/cidToUnicode/Adobe-*
%{_datadir}/poppler/nameToUnicode/Bulgarian
%{_datadir}/poppler/nameToUnicode/Greek
%{_datadir}/poppler/nameToUnicode/Thai
%{_datadir}/poppler/unicodeMap/Big5
%{_datadir}/poppler/unicodeMap/Big5ascii
%{_datadir}/poppler/unicodeMap/EUC-CN
%{_datadir}/poppler/unicodeMap/EUC-JP
%{_datadir}/poppler/unicodeMap/GBK
%{_datadir}/poppler/unicodeMap/ISO-*
%{_datadir}/poppler/unicodeMap/KOI8-R
%{_datadir}/poppler/unicodeMap/Latin2
%{_datadir}/poppler/unicodeMap/Shift-JIS
%{_datadir}/poppler/unicodeMap/TIS-620
%{_datadir}/poppler/unicodeMap/Windows-1255
%{_datadir}/pkgconfig/poppler-data.pc

%clean
rm -rf %{buildroot}

%changelog
* Sat Oct 17 2015 tanggeliang <tanggeliang@gmail.com>
- update from 0.4.6 to 0.4.7
* Mon Oct 7 2013 tanggeliang <tanggeliang@gmail.com>
- create
