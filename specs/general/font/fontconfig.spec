Name:       fontconfig
Version:    2.11.94
Release:    1%{?dist}
Summary:    Fontconfig

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.fontconfig.org
Source:     http://fontconfig.org/release/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  freetype expat libxml

%description
The Fontconfig package contains a library and support programs used for configuring and customizing font access.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --sysconfdir=/etc \
            --localstatedir=/var \
            --docdir=/usr/share/doc/fontconfig-%{version} \
            --disable-docs \
            --disable-static \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_sysconfdir}/fonts/conf.d/[0-9][0-9]-*.conf
%{_sysconfdir}/fonts/conf.d/README
%{_sysconfdir}/fonts/fonts.conf
%{_bindir}/fc-cache
%{_bindir}/fc-cat
%{_bindir}/fc-list
%{_bindir}/fc-match
%{_bindir}/fc-pattern
%{_bindir}/fc-query
%{_bindir}/fc-scan
%{_bindir}/fc-validate
%{_includedir}/fontconfig/*.h
%{_libdir}/libfontconfig.*a
%{_libdir}/libfontconfig.so*
%{_libdir}/pkgconfig/fontconfig.pc
%{_datadir}/fontconfig/conf.avail/[0-9][0-9]-*.conf
%{_datadir}/xml/fontconfig/fonts.dtd

%clean
rm -rf %{buildroot}

%changelog
* Tue Sep 15 2015 tanggeliang <tanggeliang@gmail.com>
- update from 2.11.1 to 2.11.94
* Sun Jul 20 2014 tanggeliang <tanggeliang@gmail.com>
- update from 2.11.0 to 2.11.1
* Sun Oct 20 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.10.93 to 2.11.0
* Tue Jul 2 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.10.92 to 2.10.93
* Thu Apr 11 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.10.2 to 2.10.92
* Wed Apr 10 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.10.1 to 2.10.2
* Fri Oct 12 2012 tanggeliang <tanggeliang@gmail.com>
- update from 2.9.0 to 2.10.1
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
