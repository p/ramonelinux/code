Name:       freetype
Version:    2.5.5
Release:    1%{?dist}
Summary:    FreeType2

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://freetype.sourceforge.net
Source:     http://downloads.sourceforge.net/freetype/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  libpng

%description
The FreeType2 package contains a library which allows applications to properly render TrueType fonts.

%prep
%setup -q -n %{name}-%{version}

%build
sed -i  -e "/AUX.*.gxvalid/s@^# @@" \
        -e "/AUX.*.otvalid/s@^# @@" \
        modules.cfg                         &&

sed -ri -e 's:.*(#.*SUBPIXEL.*) .*:\1:' \
        include/config/ftoption.h  &&

./configure --prefix=/usr --disable-static \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/freetype-config
%{_includedir}/freetype2/config/ft*.h
%{_includedir}/freetype2/*.h
%{_libdir}/libfreetype.*a
%{_libdir}/libfreetype.so*
%{_libdir}/pkgconfig/freetype2.pc
%{_datadir}/aclocal/freetype2.m4
%{_mandir}/man1/freetype-config.1.gz

%clean
rm -rf %{buildroot}

%changelog
* Sat Mar 28 2015 tanggeliang <tanggeliang@gmail.com>
- update from 2.5.3 to 2.5.5
* Sun Mar 16 2014 tanggeliang <tanggeliang@gmail.com>
- update from 2.5.2 to 2.5.3
* Sun Dec 15 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.5.1 to 2.5.2
* Sun Dec 1 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.5.0.1 to 2.5.1
* Tue Jul 2 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.4.12 to 2.5.0.1
* Wed May 29 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.4.11 to 2.4.12
* Sun Mar 17 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.4.9 to 2.4.11
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
