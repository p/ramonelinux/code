Name:       rarian
Version:    0.8.1
Release:    6%{?dist}
Summary:    Rarian

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.gnome.org
Source:     http://ftp.gnome.org/pub/gnome/sources/rarian/0.8/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  libxslt docbook-xsl

%description
The Rarian package is a documentation metadata library based on the proposed Freedesktop.org spec.
Rarian is designed to be a replacement for ScrollKeeper.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --localstatedir=/var \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/rarian-*
%{_bindir}/scrollkeeper-*
%{_includedir}/rarian/rarian*.h
%{_libdir}/librarian.*
%{_libdir}/pkgconfig/rarian.pc
%{_datadir}/help/*
%{_datadir}/librarian/*
%{_var}/lib/rarian

%clean
rm -rf %{buildroot}

%changelog
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
