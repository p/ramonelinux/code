Name:       pinentry
Version:    0.8.1
Release:    6%{?dist}
Summary:    PIN-Entry

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.gnupg.org
Source:     ftp://ftp.gnupg.org/gcrypt/pinentry/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build

%description
The PIN-Entry package contains a collection of simple PIN or pass-phrase entry dialogs which utilize the Assuan protocol as described by the ?gypten project.
PIN-Entry programs are usually invoked by the gpg-agent daemon, but can be run from the command line as well.
There are programs for various text-based and GUI environments, including interfaces designed for Ncurses (text-based), Gtk+, Gtk+-2, and Qt-3.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --disable-pinentry-gtk           \
            --disable-pinentry-gtk2          \
            --disable-pinentry-qt            \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install &&

rm -rf %{buildroot}/%{_infodir}/dir

%files
%defattr(-,root,root,-)
%{_bindir}/pinentry
%{_bindir}/pinentry-curses
%{_infodir}/pinentry.info.gz

%clean
rm -rf %{buildroot}

%changelog
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
