Name:       unrar
Version:    5.1.6
Release:    1%{?dist}
Summary:    UnRar

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.rarlab.com
Source:     http://www.rarlab.com/rar/%{name}src-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build

%description
The UnRar package contains a RAR extraction utility used for extracting files from RAR archives.
RAR archives are usually created with WinRAR, primarily in a Windows environment.

%prep
%setup -q -n %{name}

%build
make -f makefile %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
mkdir -pv %{buildroot}/usr/bin
install -v -m755 unrar %{buildroot}/usr/bin

%files
%defattr(-,root,root,-)
%{_bindir}/unrar

%clean
rm -rf %{buildroot}

%changelog
* Sat Jul 26 2014 tanggeliang <tanggeliang@gmail.com>
- update from 5.1.1 to 5.1.6
* Fri Mar 21 2014 tanggeliang <tanggeliang@gmail.com>
- update from 5.0.14 to 5.1.1
* Thu Dec 5 2013 tanggeliang <tanggeliang@gmail.com>
- update from 5.0.13 to 5.0.14
* Sun Dec 1 2013 tanggeliang <tanggeliang@gmail.com>
- update from 5.0.12 to 5.0.13
* Wed Oct 2 2013 tanggeliang <tanggeliang@gmail.com>
- update from 5.0.10 to 5.0.12
* Sat Aug 24 2013 tanggeliang <tanggeliang@gmail.com>
- update from 5.0.8 to 5.0.10
* Thu Jul 25 2013 tanggeliang <tanggeliang@gmail.com>
- update from 4.2.4 to 5.0.8
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
