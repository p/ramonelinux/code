Name:       parted
Version:    3.2
Release:    1%{?dist}
Summary:    Parted

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.gnu.org
Source:     http://ftp.gnu.org/gnu/parted/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  lvm

%description
The Parted package is a disk partitioning and partition resizing tool.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --disable-static \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

rm -rf %{buildroot}/%{_infodir}/dir

%files
%defattr(-,root,root,-)
%{_includedir}/parted/*.h
%{_libdir}/libparted-fs-resize.*
%{_libdir}/libparted.*
%{_libdir}/pkgconfig/libparted.pc
%{_sbindir}/parted
%{_sbindir}/partprobe
%{_infodir}/parted.info.gz
%{_datadir}/locale/*/LC_MESSAGES/parted.mo
%{_mandir}/man8/part*.8.gz

%clean
rm -rf %{buildroot}

%changelog
* Tue Oct 27 2015 tanggeliang <tanggeliang@gmail.com>
- update from 3.1 to 3.2
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
