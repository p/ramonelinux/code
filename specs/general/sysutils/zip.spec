Name:       zip
Version:    3.0
Release:    6%{?dist}
Summary:    Zip

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://infozip.sourceforge.net
Source:     http://downloads.sourceforge.net/infozip/%{name}30.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build

%description
The Zip package contains Zip utilities. These are useful for compressing files into ZIP archives.

%prep
%setup -q -n %{name}30

%build
make -f unix/Makefile generic_gcc %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make prefix=%{buildroot}/usr -f unix/Makefile install

pushd %{buildroot}
mkdir -pv usr/share
mv usr/man usr/share/man
popd

%files
%defattr(-,root,root,-)
%{_bindir}/zip
%{_bindir}/zipcloak
%{_bindir}/zipnote
%{_bindir}/zipsplit
%{_mandir}/man1/*

%clean
rm -rf %{buildroot}

%changelog
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
