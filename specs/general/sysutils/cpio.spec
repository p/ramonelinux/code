Name:       cpio
Version:    2.11
Release:    8%{?dist}
Summary:    cpio

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.gnu.org
Source:     http://ftp.gnu.org/pub/gnu/cpio/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build

%description
The cpio package contains tools for archiving.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
sed -i -e '/gets is a/d' gnu/stdio.in.h &&
./configure --prefix=/usr     \
            --bindir=/bin     \
            --libexecdir=/tmp \
            --enable-mt       \
            --with-rmt=/usr/sbin/rmt &&
make %{?_smp_mflags} &&
makeinfo --html            -o doc/html      doc/cpio.texi &&
makeinfo --html --no-split -o doc/cpio.html doc/cpio.texi &&
makeinfo --plaintext       -o doc/cpio.txt  doc/cpio.texi

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install &&
install -v -m755 -d %{buildroot}/usr/share/doc/cpio-2.11/html &&
install -v -m644    doc/html/* \
                    %{buildroot}/usr/share/doc/cpio-2.11/html &&
install -v -m644    doc/cpio.{html,txt} \
                    %{buildroot}/usr/share/doc/cpio-2.11

rm -f %{buildroot}/%{_infodir}/dir

%files
%defattr(-,root,root,-)
/bin/cpio
/bin/mt
%{_datadir}/locale/*/LC_MESSAGES/cpio.mo

%files doc
%defattr(-,root,root,-)
%{_docdir}/cpio-2.11/*
%{_infodir}/cpio.info.gz
%{_mandir}/man1/cpio.1.gz
%{_mandir}/man1/mt.1.gz

%clean
rm -rf %{buildroot}

%changelog
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
