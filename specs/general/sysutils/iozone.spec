Name:       iozone
Version:    429
Release:    1%{?dist}
Summary:    Iozone Filesystem Benchmark

Group:      System Environment/Base
License:    GPLv2+
Url:        http://www.iozone.org
Source:     http://www.iozone.org/src/current/%{name}3_%{version}.tar

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build

%description
IOzone is a filesystem benchmark tool.
The benchmark generates and measures a variety of file operations.
Iozone has been ported to many machines and runs under many operating systems.

%prep
%setup -q -n %{name}3_%{version}

%build
make linux -C src/current %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
mkdir -pv %{buildroot}/%{_bindir}
cp src/current/{iozone,fileop,pit_server} %{buildroot}/%{_bindir}

%files
%defattr(-,root,root,-)
%{_bindir}/iozone
%{_bindir}/fileop
%{_bindir}/pit_server

%clean
rm -rf %{buildroot}

%changelog
* Thu Dec 4 2014 tanggeliang <tanggeliang@gmail.com>
- create
