Name:       protobuf-c
Version:    2.5.0
Release:    1%{?dist}
Summary:    Protocol Buffers - Google's data interchange format

Group:      System Environment/libraries
License:    GPLv2+
Url:        https://github.com/protobuf-c
Source:     https://github.com/protobuf-c/%{name}-master.zip

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  automake autoconf m4 libtool
BuildRequires:  protobuf

%description
Protocol Buffers are a way of encoding structured data in an efficient yet extensible format.
Google uses Protocol Buffers for almost all of its internal RPC protocols and file formats.

%prep
%setup -q -n %{name}-master

%build
./autogen.sh
./configure --prefix=/usr \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/protoc-c
%{_includedir}/google/protobuf-c/protobuf-c.h
%{_includedir}/protobuf-c/protobuf-c.h
%{_libdir}/libprotobuf-c.*
%{_libdir}/pkgconfig/libprotobuf-c.pc

%clean
rm -rf %{buildroot}

%changelog
* Tue Aug 5 2014 tanggeliang <tanggeliang@gmail.com>
- create
