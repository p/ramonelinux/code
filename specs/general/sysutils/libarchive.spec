Name:       libarchive
Version:    3.1.2
Release:    1%{?dist}
Summary:    libarchive

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://libarchive.googlecode.com
Source:     http://libarchive.googlecode.com/files/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  libxml expat openssl nettle acl

%description
The libarchive library provides a single interface for reading/writing various compression formats.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --disable-static \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/bsdcpio
%{_bindir}/bsdtar
%{_includedir}/archive*.h
%{_libdir}/libarchive.*
%{_libdir}/pkgconfig/libarchive.pc
%{_mandir}/man1/*.1.gz
%{_mandir}/man3/*.3.gz
%{_mandir}/man5/*.5.gz

%clean
rm -rf %{buildroot}

%changelog
* Thu May 30 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.0.4 to 3.1.2
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
