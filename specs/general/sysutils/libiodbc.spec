Name:       libiodbc
Version:    3.52.8
Release:    8%{?dist}
Summary:    libiodbc

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.iodbc.org
Source:     http://www.iodbc.org/downloads/iODBC/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  gtk2

%description
libiodbc is an API to ODBC compatible databases.

%package        admin
Summary:        Gui administrator for iODBC development
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}
%description    admin
This package contains a Gui administrator program for maintaining DSN information in odbc.ini and odbcinst.ini files.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr                   \
            --with-iodbc-inidir=/etc/iodbc  \
            --includedir=/usr/include/iodbc \
            --disable-libodbc               \
            --libdir=%{_libdir} &&
make -j1

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/iodbc-config
%{_bindir}/iodbctest
%{_bindir}/iodbctestw
%{_includedir}/iodbc/*.h
%{_libdir}/libiodbc.*
%{_libdir}/libiodbcinst.*
%{_libdir}/pkgconfig/libiodbc.pc
%{_datadir}/libiodbc/samples/*
%{_mandir}/man1/iodbc*.1.gz

%files admin
%{_bindir}/iodbcadm-gtk
%{_libdir}/libdrvproxy.*
%{_libdir}/libiodbcadm.*
%{_mandir}/man1/iodbcadm-gtk.1.gz

%clean
rm -rf %{buildroot}

%changelog
* Wed Mar 13 2013 tanggeliang <tanggeliang@gmail.com>
- add subpackage admin
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
