Name:       btrfs-progs
Version:    4.2.3
Release:    1%{?dist}
Summary:    btrfs userspace utilities

Group:      System Environment/libraries
License:    GPLv2+
Url:        https://www.kernel.org/pub/linux/btrfs-progs
Source:     https://www.kernel.org/pub/linux/kernel/people/kdave/btrfs-progs/%{name}-v%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  asciidoc xmlto docbook-xml docbook-xsl

%description
Userspace tools for btrfs.

%prep
%setup -q -n %{name}-v%{version}

%build
./configure --prefix=/usr \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/btrfs
%{_bindir}/btrfs-convert
%{_bindir}/btrfs-debug-tree
%{_bindir}/btrfs-find-root
%{_bindir}/btrfs-image
%{_bindir}/btrfs-map-logical
%{_bindir}/btrfs-select-super
%{_bindir}/btrfs-show-super
%{_bindir}/btrfs-zero-log
%{_bindir}/btrfsck
%{_bindir}/btrfstune
%{_bindir}/fsck.btrfs
%{_bindir}/mkfs.btrfs
%{_includedir}/btrfs/*.h
%{_libdir}/libbtrfs.a
%{_libdir}/libbtrfs.so*
%{_mandir}/man*/*btrfs*.gz

%clean
rm -rf %{buildroot}

%changelog
* Tue Oct 27 2015 tanggeliang <tanggeliang@gmail.com>
- update from 3.18 to 4.2.3
* Wed Dec 31 2014 tanggeliang <tanggeliang@gmail.com>
- create
