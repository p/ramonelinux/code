Name:       unzip
Version:    6.0
Release:    8%{?dist}
Summary:    UnZip

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://infozip.sourceforge.net
Source:     http://downloads.sourceforge.net/infozip/%{name}60.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build

%description
The UnZip package contains ZIP extraction utilities.
These are useful for extracting files from ZIP archives.
ZIP archives are created with PKZIP or Info-ZIP utilities, primarily in a DOS environment.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}60

%build
case `uname -m` in
  i?86)
    make -f unix/Makefile linux %{?_smp_mflags}
    ;;
  *)
    make -f unix/Makefile linux_noasm %{?_smp_mflags}
    ;;
esac

%check

%install
rm -rf %{buildroot}
make prefix=%{buildroot}/usr install

mkdir -pv %{buildroot}/usr/share
mv %{buildroot}/usr/man %{buildroot}/usr/share/man

%files
%defattr(-,root,root,-)
%{_bindir}/unzip
%{_bindir}/funzip
%{_bindir}/unzipsfx
%{_bindir}/zipgrep
%{_bindir}/zipinfo

%files doc
%defattr(-,root,root,-)
%{_mandir}/man1/*

%clean
rm -rf %{buildroot}

%changelog
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
