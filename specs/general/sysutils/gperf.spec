Name:       gperf
Version:    3.0.4
Release:    7%{?dist}
Summary:    Gperf

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.gnu.org
Source:     http://ftp.gnu.org/gnu/gperf/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build

%description
Gperf generates a perfect hash function from a key set.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --docdir=/usr/share/doc/gperf-3.0.4 &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/gperf
%{_docdir}/gperf-3.0.4/gperf.html
%{_infodir}/gperf.info.gz
%{_mandir}/man1/gperf.1.gz

%clean
rm -rf %{buildroot}

%changelog
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
