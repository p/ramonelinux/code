Name:       js
Version:    24.2.0
Release:    1%{?dist}
Summary:    SpiderMonkey JavaScript

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.mozilla.org
Source:     http://ftp.mozilla.org/pub/mozilla.org/js/mozjs-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  libffi nspr python zip makedepend

%description
JS is Mozilla's JavaScript engine written in C/C++.

%prep
%setup -q -n mozjs-%{version}

%build
cd js/src &&
./configure --prefix=/usr       \
            --enable-readline   \
            --enable-threadsafe \
            --with-system-ffi   \
            --with-system-nspr  \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
cd js/src &&
make DESTDIR=%{buildroot} install

find %{buildroot}/usr/include/mozjs-24/            \
     %{buildroot}/%{_libdir}/libmozjs-24.a         \
     %{buildroot}/%{_libdir}/pkgconfig/mozjs-24.pc \
     -type f -exec chmod -v 644 {} +

%files
%defattr(-,root,root,-)
%{_bindir}/js24
%{_bindir}/js24-config
%{_includedir}/mozjs-24/*
%{_libdir}/libmozjs-24.*
%{_libdir}/pkgconfig/mozjs-24.pc

%clean
rm -rf %{buildroot}

%changelog
* Sat Mar 15 2014 tanggeliang <tanggeliang@gmail.com>
- update from 17.0.0 to 24.2.0
* Thu Jul 25 2013 tanggeliang <tanggeliang@gmail.com>
- update from js185-1.0.0 to mozjs17.0.0
