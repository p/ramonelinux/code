Name:       hexedit
Version:    1.2.13
Release:    1%{?dist}
Summary:    A hexadecimal file viewer and editor

Group:      System Environment/Base
License:    GPLv2+
Url:        http://rigaux.org/hexedit.html
Source:     http://rigaux.org/%{name}-%{version}.src.tgz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  ncurses autoconf automake m4

%description
Hexedit shows a file both in ASCII and in hexadecimal. The file can be a device as the file is read a piece at a time.
Hexedit can be used to modify the file and search through it.

%prep
%setup -q -n %{name}

%build
./configure --prefix=/usr &&
make %{?_smp_mflags}

%check

%install
make install \
  mandir=%{buildroot}%{_mandir} \
  bindir=%{buildroot}%{_bindir} \
  INSTALL='install -p'

%files
%defattr(-,root,root,-)
%{_bindir}/hexedit
%{_mandir}/man1/hexedit.1.gz

%clean
rm -rf %{buildroot}

%changelog
* Tue Nov 4 2014 tanggeliang <tanggeliang@gmail.com>
- create
