Name:       strace
Version:    4.8
Release:    1%{?dist}
Summary:    Tracks and displays system calls associated with a running process

Group:      Development/Debuggers
License:    GPLv2+
Url:        http://sourceforge.net/projects/strace/?source=navbar
Source:     http://sourceforge.net/projects/strace/files/%{name}-%{version}.tar.xz
Patch:      strace-fix-ftbfs.patch

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  acl libaio

%description
The strace program intercepts and records the system calls called and received by a running process.
Strace can print a record of each system call, its arguments and its return value.
Strace is useful for diagnosing problems and debugging, as well as for instructional purposes.

%prep
%setup -q -n %{name}-%{version}
%patch -p1

%build
./configure --prefix=/usr &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/strace
%{_bindir}/strace-graph
%{_bindir}/strace-log-merge
%{_mandir}/man1/strace.1.gz

%clean
rm -rf %{buildroot}

%changelog
* Fri Dec 13 2013 tanggeliang <tanggeliang@gmail.com>
- create
