Name:       usbutils
Version:    008
Release:    1%{?dist}
Summary:    USB Utils

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://linux-usb.sourceforge.net
Source0:    http://downloads.sourceforge.net/linux-usb/%{name}-%{version}.tar.xz
Source1:    http://www.linux-usb.org/usb.ids

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  libusb pkg-config

%description
The USB Utils package contains an utility used to display information about USB buses in the system and the devices connected to them.

%prep
%setup -q -n %{name}-%{version}

%build
sed -i '/^usbids/ s:usb.ids:hwdata/&:' lsusb.py &&

./configure --prefix=/usr --datadir=/usr/share/hwdata &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

install -dm755 %{buildroot}/usr/share/hwdata/ &&
cp %{SOURCE1} %{buildroot}/usr/share/hwdata/usb.ids

%files
%defattr(-,root,root,-)
%{_bindir}/usb-devices
%{_bindir}/lsusb
%{_bindir}/lsusb.py
%{_bindir}/usbhid-dump
%{_mandir}/*/*
%{_datadir}/pkgconfig/usbutils.pc
%{_datadir}/hwdata/usb.ids

%clean
rm -rf %{buildroot}

%changelog
* Fri Mar 27 2015 tanggeliang <tanggeliang@gmail.com>
- update from 007 to 008
* Thu Jul 25 2013 tanggeliang <tanggeliang@gmail.com>
- update from 005 to 007
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
