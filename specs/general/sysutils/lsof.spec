Name:       lsof
Version:    4.87
Release:    1%{?dist}
Summary:    lsof

Group:      System Environment/libraries
License:    GPLv2+
Url:        ftp://sunsite.ualberta.ca/pub/Mirror/lsof
Source:     ftp://sunsite.ualberta.ca/pub/Mirror/lsof/%{name}_%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  libtirpc

%description
The lsof package is useful to LiSt Open Files for a given running application or process.

%prep
%setup -q -n %{name}_%{version}

%build
tar -xf lsof_4.87_src.tar  &&
cd lsof_4.87_src           &&
./Configure -n linux       &&
make %{?_smp_mflags} CFGL="-L./lib -ltirpc"

%check

%install
rm -rf %{buildroot}

cd lsof_4.87_src           &&
mkdir -pv %{buildroot}/usr/bin
mkdir -pv %{buildroot}/usr/share/man/man8
install -v -m0755 lsof %{buildroot}/usr/bin &&
install -v lsof.8 %{buildroot}/usr/share/man/man8

%files
%defattr(-,root,root,-)
%{_bindir}/lsof
%{_mandir}/man8/lsof.8.gz

%clean
rm -rf %{buildroot}

%changelog
* Fri Jul 25 2014 tanggeliang <tanggeliang@gmail.com>
- create
