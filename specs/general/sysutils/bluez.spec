Name:       bluez
Version:    5.34
Release:    1%{?dist}
Summary:    BlueZ

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.kernel.org/pub/linux/bluetooth
Source:     http://www.kernel.org/pub/linux/bluetooth/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  dbus glib systemd systemd-udev libical
BuildRequires:  alsa-lib check cups gst-plugins-base libsndfile libusb-compat
Requires:       systemd

%description
The BlueZ package contains the Bluetooth protocol stack for Linux.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr         \
            --sysconfdir=/etc     \
            --localstatedir=/var  \
            --enable-library      \
            --with-systemdunitdir=/lib/systemd/system \
            --with-systemduserunitdir=/lib/systemd/user \
            --libexecdir=%{_libdir} \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

mkdir -pv %{buildroot}/usr/sbin
ln -svf ../libexec/bluetooth/bluetoothd %{buildroot}/usr/sbin

install -v -dm755 %{buildroot}/etc/bluetooth &&
install -v -m644 src/main.conf %{buildroot}/etc/bluetooth/main.conf

install -v -dm755 -d %{buildroot}%{_datadir}/doc/bluez-%{version} &&
install -v -m644 doc/*.txt %{buildroot}%{_datadir}/doc/bluez-%{version}

%files
%defattr(-,root,root,-)
/etc/bluetooth/main.conf
/etc/dbus-1/system.d/bluetooth.conf
%{_bindir}/bccmd
%{_bindir}/bluemoon
%{_bindir}/bluetoothctl
%{_bindir}/btmon
%{_bindir}/ciptool
%{_bindir}/hciattach
%{_bindir}/hciconfig
%{_bindir}/hcidump
%{_bindir}/hcitool
%{_bindir}/hex2hcd
%{_bindir}/l2ping
%{_bindir}/l2test
%{_bindir}/mpris-proxy
%{_bindir}/rctest
%{_bindir}/rfcomm
%{_bindir}/sdptool
%{_includedir}/bluetooth/*.h
%{_libdir}/bluetooth/bluetoothd
%{_libdir}/bluetooth/obexd
%{_libdir}/cups/backend/bluetooth
%{_libdir}/libbluetooth.*
%{_libdir}/pkgconfig/bluez.pc
%{_sbindir}/bluetoothd
%{_datadir}/dbus-1/system-services/org.bluez.service
%{_datadir}/dbus-1/services/org.bluez.obex.service
/lib/systemd/system/bluetooth.service
/lib/systemd/user/obex.service
/lib/udev/hid2hci
/lib/udev/rules.d/97-hid2hci.rules

%files doc
%defattr(-,root,root,-)
%{_docdir}/bluez-%{version}/*
%{_mandir}/man1/*.1.gz
%{_mandir}/man8/*.8.gz

%post
systemctl enable bluetooth

%postun
systemctl disable bluetooth

%clean
rm -rf %{buildroot}

%changelog
* Wed Sep 16 2015 tanggeliang <tanggeliang@gmail.com>
- update from 5.21 to 5.34
* Sun Jul 20 2014 tanggeliang <tanggeliang@gmail.com>
- update from 4.101 to 5.21
* Mon Oct 29 2012 tanggeliang <tanggeliang@gmail.com>
- create
