Name:       udisks
Version:    2.1.6
Release:    1%{?dist}
Summary:    UDisks2

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.freedesktop.org
Source:     http://udisks.freedesktop.org/releases/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  acl libatasmart libxslt polkit systemd systemd-gudev systemd-udev
BuildRequires:  gobject-introspection gtk-doc
BuildRequires:  intltool gettext xml-parser docbook-xsl

%description
The UDisks2 package provides a daemon, tools and libraries to access and manipulate disks and storage devices.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --sysconfdir=/etc \
            --localstatedir=/var \
            --disable-static \
	    --libexecdir=%{_libdir} \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
/etc/dbus-1/system.d/org.freedesktop.UDisks2.conf
/lib/udev/rules.d/80-udisks2.rules
/lib/systemd/system/udisks2.service
%{_bindir}/udisksctl
%{_includedir}/udisks2/udisks/udisks*.h
%{_libdir}/girepository-1.0/UDisks-2.0.typelib
%{_libdir}/libudisks2.*
%{_libdir}/pkgconfig/udisks2.pc
%{_libdir}/udisks2/udisksd
%{_sbindir}/umount.udisks2
%{_datadir}/bash-completion/completions/udisksctl
%{_datadir}/dbus-1/system-services/org.freedesktop.UDisks2.service
%{_datadir}/gir-1.0/UDisks-2.0.gir
%{_datadir}/locale/*
%{_datadir}/polkit-1/actions/org.freedesktop.udisks2.policy

%files doc
%defattr(-,root,root,-)
%{_datadir}/gtk-doc/html/udisks2/*
%{_mandir}/man*/*

%clean
rm -rf %{buildroot}

%changelog
* Wed Sep 23 2015 tanggeliang <tanggeliang@gmail.com>
- update from 2.1.3 to 2.1.6
* Tue Mar 18 2014 tanggeliang <tanggeliang@gmail.com>
- update from 2.1.1 to 2.1.3
* Sat Aug 24 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.1.0 to 2.1.1
* Sat May 11 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.0.0 to 2.1.0
* Sun Oct 14 2012 tanggeliang <tanggeliang@gmail.com>
- update from 1.99.0 to 2.0.0
* Mon Oct 1 2012 tanggeliang <tanggeliang@gmail.com>
- update from 1.98.0 to 1.99.0
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
