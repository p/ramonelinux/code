Name:       lzma
Version:    4.32.7
Release:    1%{?dist}
Summary:    lzma

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://tukaani.org/lzma
Source:     http://tukaani.org/lzma/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build

%description

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr
make

%check

%install
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/*
%{_includedir}/*
%{_libdir}/*
%{_mandir}/*/*

%clean
rm -rf %{buildroot}

%changelog
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
