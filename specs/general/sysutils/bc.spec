Name:       bc
Version:    1.06.95
Release:    1%{?dist}
Summary:    bc

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.gnu.org
Source:     http://alpha.gnu.org/gnu/bc/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  readline

%description
The bc package contains an arbitrary precision numeric processing language.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --with-readline \
            --mandir=%{_mandir} --infodir=%{_infodir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

rm -f %{buildroot}/%{_infodir}/dir

%files
%defattr(-,root,root,-)
%{_bindir}/bc
%{_bindir}/dc
%{_infodir}/bc.info.gz
%{_infodir}/dc.info.gz
%{_mandir}/man1/bc.1.gz
%{_mandir}/man1/dc.1.gz

%clean
rm -rf %{buildroot}

%changelog
* Mon Jul 15 2013 tanggeliang <tanggeliang@gmail.com>
- create
