Name:       strigi
Version:    0.7.8
Release:    4%{?dist}
Summary:    Strigi

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.strigi.com
Source:     http://www.vandenoever.info/software/strigi/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  cmake expat libxml
BuildRequires:  qt4 dbus boost
BuildRequires:  gamin ffmpeg exiv

%description
Strigi is a desktop search engine.

%prep
%setup -q -n %{name}-%{version}

%build
mkdir build &&
cd    build &&
cmake -DCMAKE_INSTALL_PREFIX=/usr .. &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
cd    build &&
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/deepfind
%{_bindir}/deepgrep
%{_bindir}/rdfindexer
%{_bindir}/strigiclient
%{_bindir}/strigicmd
%{_bindir}/strigidaemon
%{_bindir}/xmlindexer
%{_includedir}/strigi/*.h
%{_includedir}/strigi/qtdbus/*.h
%{_libdir}/libsearchclient.so*
%{_libdir}/libstreamanalyzer.so*
%{_libdir}/libstreams.so*
%{_libdir}/libstrigihtmlgui.so*
%{_libdir}/libstrigiqtdbusclient.so*
%{_libdir}/cmake/LibSearchClient/LibSearchClientConfig.cmake
%{_libdir}/cmake/LibStreamAnalyzer/LibStreamAnalyzerConfig*.cmake
%{_libdir}/cmake/LibStreams/LibStreams*.cmake
%{_libdir}/cmake/Strigi/StrigiConfig*.cmake
%{_libdir}/pkgconfig/libstreamanalyzer.pc
%{_libdir}/pkgconfig/libstreams.pc
%{_libdir}/strigi/strigiea_*.so
%{_libdir}/strigi/strigila_*.so
%{_libdir}/strigi/strigita_*.so
%{_datadir}/dbus-1/services/org.freedesktop.xesam.searcher.service
%{_datadir}/dbus-1/services/vandenoever.strigi.service
%{_datadir}/strigi/fieldproperties/*.rdfs

%clean
rm -rf %{buildroot}

%changelog
* Wed Feb 27 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.7.7 to 0.7.8
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
