Name:       raptor
Version:    2.0.9
Release:    1%{?dist}
Summary:    Raptor

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.librdf.org
Source:     http://download.librdf.org/source/%{name}2-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  expat libxml curl

%description
Raptor is a C library that provides a set of parsers and serializers that generate Resource Description Framework (RDF) triples.

%prep
%setup -q -n %{name}2-%{version}

%build
./configure --prefix=/usr --disable-static \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/rapper
%{_includedir}/raptor2/raptor*.h
%{_libdir}/libraptor2.*
%{_libdir}/pkgconfig/raptor2.pc
%{_datadir}/gtk-doc/html/raptor2/*
%{_mandir}/man1/rapper.1.gz
%{_mandir}/man3/libraptor2.3.gz

%clean
rm -rf %{buildroot}

%changelog
* Thu Apr 11 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.0.8 to 2.0.9
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
