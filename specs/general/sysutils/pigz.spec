Name:       pigz
Version:    2.2.4
Release:    6%{?dist}
Summary:    Parallel implementation of gzip

Group:      Applications/File
License:    zlib
Url:        http://www.zlib.net
Source:     http://www.zlib.net/%{name}/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build

%description
pigz, which stands for parallel implementation of gzip, is a fully functional replacement for gzip that exploits multiple processors and multiple cores to the hilt when compressing data.

%prep
%setup -q -n %{name}-%{version}

%build
make %{?_smp_mflags} CFLAGS='%{optflags}'

%install
rm -rf %{buildroot}
install -p -D pigz %{buildroot}%{_bindir}/pigz
install -p -D unpigz %{buildroot}%{_bindir}/unpigz
install -p -D pigz.1 -m 0644 %{buildroot}%{_datadir}/man/man1/pigz.1

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%{_bindir}/pigz
%{_bindir}/unpigz
%{_mandir}/man1/pigz.*

%changelog
* Sun May 1 2011 tanggeliang <tanggeliang@gmail.com>
- porting from Fedora
