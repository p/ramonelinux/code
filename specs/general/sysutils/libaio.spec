Name:       libaio
Version:    0.3.109
Release:    1%{?dist}
Summary:    Linux-native asynchronous I/O access library

License:    LGPLv2+
Group:      System Environment/Libraries
Source:     ftp://ftp.kernel.org/pub/linux/libs/aio/%{name}-%{version}.tar.gz
Patch1:     libaio-install-to-slash.patch

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-buil

%description
The Linux-native asynchronous I/O facility ("async I/O", or "aio") has a richer API and capability set than the simple POSIX async I/O facility.
This library, libaio, provides the Linux-native API for async I/O.

%prep
%setup -a 0
%patch1 -p1

%build
make soname='libaio.so.1.0.0' libname='libaio.so.1.0.0'
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
install -D -m 755 src/libaio.so.1.0.0 \
  %{buildroot}/%{_lib}/libaio.so.1.0.0
make destdir=%{buildroot} prefix=/ libdir=/%{_lib} usrlibdir=%{_libdir} \
	includedir=%{_includedir} install

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
/%{_lib}/libaio.so.1*
%{_includedir}/libaio.h
%{_libdir}/libaio.a
%{_libdir}/libaio.so

%changelog
* Fri Dec 13 2013 tanggeliang <tanggeliang@gmail.com>
- create
