Name:       js17
Version:    17.0.0
Release:    1%{?dist}
Summary:    SpiderMonkey JavaScript

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.mozilla.org
Source:     http://ftp.mozilla.org/pub/mozilla.org/js/mozjs%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  libffi nspr python zip makedepend

%description
JS is Mozilla's JavaScript engine written in C/C++.

%prep
%setup -q -n mozjs%{version}

%build
cd js/src &&
./configure --prefix=/usr       \
            --enable-readline   \
            --enable-threadsafe \
            --with-system-ffi   \
            --with-system-nspr  \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
cd js/src &&
make DESTDIR=%{buildroot} install

find %{buildroot}/usr/include/js-17.0/            \
     %{buildroot}/%{_libdir}/libmozjs-17.0.a         \
     %{buildroot}/%{_libdir}/pkgconfig/mozjs-17.0.pc \
     -type f -exec chmod -v 644 {} +

%files
%defattr(-,root,root,-)
%{_bindir}/js17
%{_bindir}/js17-config
%{_includedir}/js-17.0/*
%{_libdir}/libmozjs-17.0.*
%{_libdir}/pkgconfig/mozjs-17.0.pc

%clean
rm -rf %{buildroot}

%changelog
* Thu Jul 25 2013 tanggeliang <tanggeliang@gmail.com>
- update from js185-1.0.0 to mozjs17.0.0
