Name:       busybox
Version:    1.22.1
Release:    1%{?dist}
Summary:    BusyBox: The Swiss Army Knife of Embedded Linux

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.busybox.net
Source:     http://www.busybox.net/downloads/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build

%description
BusyBox combines tiny versions of many common UNIX utilities into a single small executable.
It provides replacements for most of the utilities you usually find in GNU fileutils, shellutils, etc. The utilities in BusyBox generally have fewer options than their full-featured GNU cousins; however, the options that are included provide the expected functionality and behave very much like their GNU counterparts.
BusyBox provides a fairly complete environment for any small or embedded system.

%package        setup
Summary:        setup
%description    setup

%package        libc
Summary:        libc
%description    libc

%package        initrd
Summary:        initrd
%description    initrd

%prep
%setup -q -n %{name}-%{version}

%build
make defconfig
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make CONFIG_PREFIX=%{buildroot} install

mkdir -pv %{buildroot}/{boot,dev,etc,lib,mnt,proc,root,sys,tmp,var}

cat > %{buildroot}/etc/fstab << "EOF"
# Begin /etc/fstab

proc  /proc proc  defaults  0 0
none  /tmp  ramfs defaults  0 0
none  /var  ramfs defaults  0 0
mdev  /dev  ramfs defaults  0 0
sysfs /sys  sysfs defaults  0 0

# End /etc/fstab
EOF

cat > %{buildroot}/etc/inittab << "EOF"
# Begin /etc/inittab

::sysinit:/etc/init.d/rcS
::respawn:-/bin/sh
::ctrlaltdel:/bin/umount -a -r
::shutdown:/bin/umount -a -r
::shutdown:/sbin/swapoff -a

# End /etc/inittab
EOF

mkdir %{buildroot}/etc/init.d
cat > %{buildroot}/etc/init.d/rcS << "EOF"
# Begin /etc/init.d/rcS

/bin/mount -a
echo /sbin/mdev > /proc/sys/kernel/hotplug
/sbin/mdev -s
mkdir /var/log

# End /etc/init.d/rcS
EOF
chmod a+x %{buildroot}/etc/init.d/rcS

cp -a /lib/ld-linux.so.2 %{buildroot}/lib/
cp -a /lib/libc-2.19.so %{buildroot}/lib/
cp -a /lib/libc.so.6 %{buildroot}/lib/
cp -a /lib/ld-2.19.so %{buildroot}/lib/
cp -a /lib/libm.so.6 %{buildroot}/lib/
cp -a /lib/libm-2.19.so %{buildroot}/lib/

cd %{buildroot}
find . | cpio -o -H newc --quiet | gzip -9  > /tmp/busybox-initrd.img
cp /tmp/busybox-initrd.img %{buildroot}/boot/

%files
%defattr(-,root,root,-)
/bin/busybox

%files setup
%defattr(-,root,root,-)
/etc/fstab
/etc/init.d/rcS
/etc/inittab
/bin/ash
/bin/base64
/bin/cat
/bin/catv
/bin/chattr
/bin/chgrp
/bin/chmod
/bin/chown
/bin/conspy
/bin/cp
/bin/cpio
/bin/cttyhack
/bin/date
/bin/dd
/bin/df
/bin/dmesg
/bin/dnsdomainname
/bin/dumpkmap
/bin/echo
/bin/ed
/bin/egrep
/bin/false
/bin/fdflush
/bin/fgrep
/bin/fsync
/bin/getopt
/bin/grep
/bin/gunzip
/bin/gzip
/bin/hostname
/bin/hush
/bin/ionice
/bin/iostat
/bin/ipcalc
/bin/kbd_mode
/bin/kill
/bin/linux32
/bin/linux64
/bin/ln
/bin/login
/bin/ls
/bin/lsattr
/bin/lzop
/bin/makemime
/bin/mkdir
/bin/mknod
/bin/mktemp
/bin/more
/bin/mount
/bin/mountpoint
/bin/mpstat
/bin/mt
/bin/mv
/bin/netstat
/bin/nice
/bin/pidof
/bin/ping
/bin/ping6
/bin/pipe_progress
/bin/printenv
/bin/ps
/bin/pwd
/bin/reformime
/bin/rev
/bin/rm
/bin/rmdir
/bin/rpm
/bin/run-parts
/bin/scriptreplay
/bin/sed
/bin/setarch
/bin/setserial
/bin/sh
/bin/sleep
/bin/stat
/bin/stty
/bin/su
/bin/sync
/bin/tar
/bin/touch
/bin/true
/bin/umount
/bin/uname
/bin/usleep
/bin/vi
/bin/watch
/bin/zcat
/linuxrc
/sbin/acpid
/sbin/adjtimex
/sbin/arp
/sbin/blkid
/sbin/blockdev
/sbin/bootchartd
/sbin/depmod
/sbin/devmem
/sbin/fbsplash
/sbin/fdisk
/sbin/findfs
/sbin/freeramdisk
/sbin/fsck
/sbin/fsck.minix
/sbin/fstrim
/sbin/getty
/sbin/halt
/sbin/hdparm
/sbin/hwclock
/sbin/ifconfig
/sbin/ifdown
/sbin/ifenslave
/sbin/ifup
/sbin/init
/sbin/insmod
/sbin/ip
/sbin/ipaddr
/sbin/iplink
/sbin/iproute
/sbin/iprule
/sbin/iptunnel
/sbin/klogd
/sbin/loadkmap
/sbin/logread
/sbin/losetup
/sbin/lsmod
/sbin/makedevs
/sbin/mdev
/sbin/mkdosfs
/sbin/mke2fs
/sbin/mkfs.ext2
/sbin/mkfs.minix
/sbin/mkfs.vfat
/sbin/mkswap
/sbin/modinfo
/sbin/modprobe
/sbin/nameif
/sbin/pivot_root
/sbin/poweroff
/sbin/raidautorun
/sbin/reboot
/sbin/rmmod
/sbin/route
/sbin/runlevel
/sbin/setconsole
/sbin/slattach
/sbin/start-stop-daemon
/sbin/sulogin
/sbin/swapoff
/sbin/swapon
/sbin/switch_root
/sbin/sysctl
/sbin/syslogd
/sbin/tunctl
/sbin/udhcpc
/sbin/vconfig
/sbin/watchdog
/sbin/zcip
%{_bindir}/[
%{_bindir}/[[
%{_bindir}/awk
%{_bindir}/basename
%{_bindir}/beep
%{_bindir}/bunzip2
%{_bindir}/bzcat
%{_bindir}/bzip2
%{_bindir}/cal
%{_bindir}/chpst
%{_bindir}/chrt
%{_bindir}/chvt
%{_bindir}/cksum
%{_bindir}/clear
%{_bindir}/cmp
%{_bindir}/comm
%{_bindir}/crontab
%{_bindir}/cryptpw
%{_bindir}/cut
%{_bindir}/dc
%{_bindir}/deallocvt
%{_bindir}/diff
%{_bindir}/dirname
%{_bindir}/dos2unix
%{_bindir}/du
%{_bindir}/dumpleases
%{_bindir}/eject
%{_bindir}/env
%{_bindir}/envdir
%{_bindir}/envuidgid
%{_bindir}/expand
%{_bindir}/expr
%{_bindir}/fgconsole
%{_bindir}/find
%{_bindir}/flock
%{_bindir}/fold
%{_bindir}/free
%{_bindir}/ftpget
%{_bindir}/ftpput
%{_bindir}/fuser
%{_bindir}/groups
%{_bindir}/hd
%{_bindir}/head
%{_bindir}/hexdump
%{_bindir}/hostid
%{_bindir}/id
%{_bindir}/install
%{_bindir}/ipcrm
%{_bindir}/ipcs
%{_bindir}/killall
%{_bindir}/last
%{_bindir}/less
%{_bindir}/logger
%{_bindir}/logname
%{_bindir}/lpq
%{_bindir}/lpr
%{_bindir}/lsof
%{_bindir}/lspci
%{_bindir}/lsusb
%{_bindir}/lzcat
%{_bindir}/lzma
%{_bindir}/lzopcat
%{_bindir}/man
%{_bindir}/md5sum
%{_bindir}/mesg
%{_bindir}/microcom
%{_bindir}/mkfifo
%{_bindir}/mkpasswd
%{_bindir}/nc
%{_bindir}/nmeter
%{_bindir}/nohup
%{_bindir}/nslookup
%{_bindir}/od
%{_bindir}/openvt
%{_bindir}/passwd
%{_bindir}/patch
%{_bindir}/pgrep
%{_bindir}/pkill
%{_bindir}/pmap
%{_bindir}/printf
%{_bindir}/pscan
%{_bindir}/pstree
%{_bindir}/pwdx
%{_bindir}/readlink
%{_bindir}/realpath
%{_bindir}/renice
%{_bindir}/reset
%{_bindir}/resize
%{_bindir}/rpm2cpio
%{_bindir}/runsv
%{_bindir}/runsvdir
%{_bindir}/rx
%{_bindir}/script
%{_bindir}/seq
%{_bindir}/setkeycodes
%{_bindir}/setsid
%{_bindir}/setuidgid
%{_bindir}/sha1sum
%{_bindir}/sha256sum
%{_bindir}/sha3sum
%{_bindir}/sha512sum
%{_bindir}/showkey
%{_bindir}/smemcap
%{_bindir}/softlimit
%{_bindir}/sort
%{_bindir}/split
%{_bindir}/strings
%{_bindir}/sum
%{_bindir}/sv
%{_bindir}/tac
%{_bindir}/tail
%{_bindir}/tcpsvd
%{_bindir}/tee
%{_bindir}/telnet
%{_bindir}/test
%{_bindir}/tftp
%{_bindir}/time
%{_bindir}/timeout
%{_bindir}/top
%{_bindir}/tr
%{_bindir}/traceroute
%{_bindir}/traceroute6
%{_bindir}/tty
%{_bindir}/ttysize
%{_bindir}/udpsvd
%{_bindir}/unexpand
%{_bindir}/uniq
%{_bindir}/unix2dos
%{_bindir}/unlzma
%{_bindir}/unlzop
%{_bindir}/unxz
%{_bindir}/unzip
%{_bindir}/uptime
%{_bindir}/users
%{_bindir}/uudecode
%{_bindir}/uuencode
%{_bindir}/vlock
%{_bindir}/volname
%{_bindir}/wall
%{_bindir}/wc
%{_bindir}/wget
%{_bindir}/which
%{_bindir}/who
%{_bindir}/whoami
%{_bindir}/whois
%{_bindir}/xargs
%{_bindir}/xz
%{_bindir}/xzcat
%{_bindir}/yes
%{_sbindir}/add-shell
%{_sbindir}/addgroup
%{_sbindir}/adduser
%{_sbindir}/arping
%{_sbindir}/brctl
%{_sbindir}/chat
%{_sbindir}/chpasswd
%{_sbindir}/chroot
%{_sbindir}/crond
%{_sbindir}/delgroup
%{_sbindir}/deluser
%{_sbindir}/dhcprelay
%{_sbindir}/dnsd
%{_sbindir}/ether-wake
%{_sbindir}/fakeidentd
%{_sbindir}/fbset
%{_sbindir}/fdformat
%{_sbindir}/ftpd
%{_sbindir}/httpd
%{_sbindir}/ifplugd
%{_sbindir}/inetd
%{_sbindir}/killall5
%{_sbindir}/loadfont
%{_sbindir}/lpd
%{_sbindir}/nanddump
%{_sbindir}/nandwrite
%{_sbindir}/nbd-client
%{_sbindir}/ntpd
%{_sbindir}/popmaildir
%{_sbindir}/powertop
%{_sbindir}/rdate
%{_sbindir}/rdev
%{_sbindir}/readahead
%{_sbindir}/readprofile
%{_sbindir}/remove-shell
%{_sbindir}/rtcwake
%{_sbindir}/sendmail
%{_sbindir}/setfont
%{_sbindir}/setlogcons
%{_sbindir}/svlogd
%{_sbindir}/telnetd
%{_sbindir}/tftpd
%{_sbindir}/ubiattach
%{_sbindir}/ubidetach
%{_sbindir}/ubimkvol
%{_sbindir}/ubirmvol
%{_sbindir}/ubirsvol
%{_sbindir}/ubiupdatevol
%{_sbindir}/udhcpd
%dir /dev
%dir /mnt
%dir /proc
%dir /root
%dir /sys
%dir /tmp
%dir /var

%files libc
%defattr(-,root,root,-)
/lib/ld-linux.so.2
/lib/ld-2.19.so
/lib/libc.so.6
/lib/libc-2.19.so
/lib/libm.so.6
/lib/libm-2.19.so

%files initrd
%defattr(-,root,root,-)
/boot/busybox-initrd.img

%clean
rm -rf %{buildroot}

%changelog
* Tue Aug 26 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.21.1 to 1.22.1
* Tue Nov 26 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.20.2 to 1.21.1
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
