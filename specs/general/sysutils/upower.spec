Name:       upower
Version:    0.99.3
Release:    1%{?dist}
Summary:    UPower

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.freedesktop.org
Source:     http://upower.freedesktop.org/releases/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  dbus-glib intltool libusb polkit systemd-udev systemd-gudev
BuildRequires:  gobject-introspection
BuildRequires:  gettext xml-parser
Requires:       polkit
Requires:       pm-utils

%description
The UPower package provides an interface to enumerating power devices, listening to device events and querying history and statistics.
Any application or service on the system can access the org.freedesktop.UPower service via the system message bus.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr        \
            --sysconfdir=/etc    \
            --localstatedir=/var \
            --disable-static     \
            --libexecdir=%{_libdir}/upower \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
/etc/UPower/UPower.conf
/etc/dbus-1/system.d/org.freedesktop.UPower.conf
/lib/udev/rules.d/95-upower-*.rules
%{_bindir}/upower
%{_includedir}/libupower-glib/up*.h
%{_libdir}/girepository-1.0/UPowerGlib-1.0.typelib
%{_libdir}/libupower-glib.*
%{_libdir}/pkgconfig/upower-glib.pc
%{_libdir}/upower/upowerd
%{_datadir}/dbus-1/interfaces/org.freedesktop.UPower*.xml
%{_datadir}/dbus-1/system-services/org.freedesktop.UPower.service
%{_datadir}/gir-1.0/UPowerGlib-1.0.gir
%{_datadir}/locale/*/LC_MESSAGES/upower.mo
/lib/systemd/system/upower.service

%clean
rm -rf %{buildroot}

%changelog
* Sat Jun 27 2015 tanggeliang <tanggeliang@gmail.com>
- update from 0.99.0 to 0.99.3
* Sat Mar 15 2014 tanggeliang <tanggeliang@gmail.com>
- update from 0.9.23 to 0.99.0
* Wed Oct 23 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.9.21 to 0.9.23
* Thu Aug 8 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.9.20 to 0.9.21
* Thu May 30 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.9.19 to 0.9.20
* Mon Apr 15 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.9.18 to 0.9.19
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
