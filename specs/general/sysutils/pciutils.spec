Name:       pciutils
Version:    3.2.1
Release:    2%{?dist}
Summary:    PCI Utils

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.kernel.org/pub/software/utils/pciutils
Source:     http://www.kernel.org/pub/software/utils/pciutils/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build

%description
The PCI Utils package contains a set of programs for listing PCI devices, inspecting their status and setting their configuration registers.

%prep
%setup -q -n %{name}-%{version}

%build
make PREFIX=/usr              \
     SHAREDIR=/usr/share/misc \
     SHARED=yes               \
     LIBDIR=%{_libdir}        \
     MANDIR=%{_mandir}
%check

%install
rm -rf %{buildroot}
make PREFIX=%{buildroot}/usr              \
     SHAREDIR=%{buildroot}/usr/share/misc \
     SHARED=yes                           \
     LIBDIR=%{buildroot}/%{_libdir}       \
     MANDIR=%{buildroot}/%{_mandir}       \
     install install-lib                  &&

chmod -v 755 %{buildroot}/%{_libdir}/libpci.so

%files
%defattr(-,root,root,-)
%{_includedir}/pci/*.h
%{_libdir}/pkgconfig/libpci.pc
%{_libdir}/libpci.so
%{_libdir}/libpci.so.3
%{_libdir}/libpci.so.%{version}
%{_sbindir}/lspci
%{_sbindir}/setpci
%{_sbindir}/update-pciids
%{_datadir}/misc/pci.ids.gz
%{_mandir}/man7/pcilib.7.gz
%{_mandir}/man8/lspci.8.gz
%{_mandir}/man8/setpci.8.gz
%{_mandir}/man8/update-pciids.8.gz

%clean
rm -rf %{buildroot}

%changelog
* Tue Dec 3 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.2.0 to 3.2.1
* Thu Jul 25 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.1.10 to 3.2.0
* Mon Apr 1 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.1.9 to 3.1.10
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
