Name:       rasqal
Version:    0.9.32
Release:    1%{?dist}
Summary:    Rasqal

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.librdf.org
Source:     http://download.librdf.org/source/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  raptor

%description
Rasqal is a C library that handles Resource Description Framework (RDF) query language syntaxes, query construction and execution of queries returning results as bindings, boolean, RDF graphs/triples or syntaxes.
It is required by Soprano to build Nepomuk.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --disable-static \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/rasqal-config
%{_bindir}/roqet
%{_includedir}/rasqal/rasqal.h
%{_libdir}/librasqal.*
%{_libdir}/pkgconfig/rasqal.pc
%{_datadir}/gtk-doc/html/rasqal/*
%{_mandir}/man*/*.gz

%clean
rm -rf %{buildroot}

%changelog
* Fri Mar 21 2014 tanggeliang <tanggeliang@gmail.com>
- update from 0.9.30 to 0.9.32
* Wed Feb 27 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.9.29 to 0.9.30
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
