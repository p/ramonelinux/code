Name:       protobuf
Version:    2.5.0
Release:    1%{?dist}
Summary:    Protocol Buffers - Google's data interchange format

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://protobuf.googlecode.com
Source:     http://protobuf.googlecode.com/files/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build

%description
Protocol Buffers are a way of encoding structured data in an efficient yet extensible format.
Google uses Protocol Buffers for almost all of its internal RPC protocols and file formats.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/protoc
%{_includedir}/google/protobuf/*
%{_libdir}/libprotobuf-lite.*
%{_libdir}/libprotobuf.*
%{_libdir}/libprotoc.*
%{_libdir}/pkgconfig/protobuf-lite.pc
%{_libdir}/pkgconfig/protobuf.pc

%clean
rm -rf %{buildroot}

%changelog
* Tue Aug 5 2014 tanggeliang <tanggeliang@gmail.com>
- create
