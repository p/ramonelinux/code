Name:       sparse
Version:    0.5.0
Release:    1%{?dist}
Summary:    Sparse - a Semantic Parser for C

Group:      System Environment/Base
License:    GPLv2+
Url:        https://sparse.wiki.kernel.org
Source:     https://www.kernel.org/pub/software/devel/sparse/dist/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:	llvm gtk2

%description
Sparse, the semantic parser, provides a compiler frontend capable of parsing most of ANSI C as well as many GCC extensions, and a collection of sample compiler backends, including a static analyzer also called "sparse".
Sparse provides a set of annotations designed to convey semantic information about types, such as what address space pointers point to, or what locks a function acquires or releases. 

%prep
%setup -q -n %{name}-%{version}

%build
make %{?_smp_mflags} \
    DESTDIR="%{buildroot}" PREFIX="%{_prefix}" \
    BINDIR="%{_bindir}" LIBDIR="%{_libdir}" \
    INCLUDEDIR="%{_includedir}" PKGCONFIGDIR="%{_libdir}/pkgconfig"

%check

%install
rm -rf %{buildroot}
make install \
    DESTDIR="%{buildroot}" PREFIX="%{_prefix}" \
    BINDIR="%{_bindir}" LIBDIR="%{_libdir}" \
    INCLUDEDIR="%{_includedir}" PKGCONFIGDIR="%{_libdir}/pkgconfig"

%files
%defattr(-,root,root,-)
%{_bindir}/c2xml
%{_bindir}/cgcc
%{_bindir}/sparse
%{_bindir}/sparse-llvm
%{_bindir}/sparsec
%{_bindir}/test-inspect
%{_includedir}/sparse/*.h
%{_libdir}/libsparse.a
%{_libdir}/pkgconfig/sparse.pc
%{_mandir}/man1/*.1.gz

%clean
rm -rf %{buildroot}

%changelog
* Fri Jun 19 2015 tanggeliang <tanggeliang@gmail.com>
- create
