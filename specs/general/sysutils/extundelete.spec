Name:       extundelete
Version:    0.2.4
Release:    1%{?dist}
Summary:    Extundelete

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://extundelete.sourceforge.net
Source:     http://downloads.sourceforge.net/project/extundelete/0.2.4/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  e2fsprogs

%description
Extundelete is a utility to undelete files from an ext3 or ext4 partition.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/extundelete

%clean
rm -rf %{buildroot}

%changelog
* Tue Aug 5 2014 tanggeliang <tanggeliang@gmail.com>
- create
