Name:       dbus-glib
Version:    0.104
Release:    1%{?dist}
Summary:    D-Bus GLib Bindings

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.freedesktop.org
Source:     http://dbus.freedesktop.org/releases/dbus-glib/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  dbus expat glib

%description
The D-Bus Bindings are a group of packages that contain programming language and platform interfaces to the D-Bus API.
This is useful for programmers to easily interface D-Bus with their supported platform or language of choice.
Some non-D-Bus packages will require one or more of the Bindings packages in order to build successfully.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --sysconfdir=/etc \
            --disable-static \
            --libexecdir=%{_libdir}/dbus-1.0 \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
/etc/bash_completion.d/dbus-bash-completion.sh
%{_bindir}/dbus-binding-tool
%{_includedir}/dbus-1.0/dbus/dbus-g*.h
%{_libdir}/dbus-1.0/*
%{_libdir}/libdbus-glib-1.*
%{_libdir}/pkgconfig/dbus-glib-1.pc

%files doc
%defattr(-,root,root,-)
%{_datadir}/gtk-doc/html/dbus-glib/*
%{_mandir}/*/*

%clean
rm -rf %{buildroot}

%changelog
* Sat Mar 28 2015 tanggeliang <tanggeliang@gmail.com>
- update from 0.102 to 0.104
* Sun Mar 16 2014 tanggeliang <tanggeliang@gmail.com>
- update from 0.100.2 to 0.102
* Wed Jul 24 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.100 to 0.100.2
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
