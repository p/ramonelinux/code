Name:       dmidecode
Version:    2.11
Release:    1%{?dist}
Summary:    Tool to analyse BIOS DMI data

Group:      System Environment/Base
License:    GPLv2+
URL:        http://www.nongnu.org
Source:     http://www.nongnu.org/dmidecode/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  automake autoconf

%description
dmidecode reports information about x86 & ia64 hardware as described in the system BIOS according to the SMBIOS/DMI standard.
This information typically includes system manufacturer, model name, serial number, BIOS version, asset tag as well as a lot of other details of varying level of interest and reliability depending on the manufacturer.
This will often include usage status for the CPU sockets, expansion slots (e.g. AGP, PCI, ISA) and memory module slots, and the list of I/O ports (e.g. serial, parallel, USB).

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
make %{?_smp_mflags} CFLAGS="$RPM_OPT_FLAGS"

%install
rm -rf ${buildroot}
make %{?_smp_mflags} DESTDIR=%{buildroot} prefix=%{_prefix} install-bin install-man

%clean
rm -rf ${buildroot}

%files
%defattr(-,root,root)
%{_sbindir}/dmidecode
%ifnarch ia64
%{_sbindir}/vpddecode
%{_sbindir}/ownership
%{_sbindir}/biosdecode
%endif

%files doc
%defattr(-,root,root)
%{_mandir}/man8/*

%changelog
* Mon Oct 15 2012 tanggeliang <tanggeliang@gmail.com>
- porting from Fedora
