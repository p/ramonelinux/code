Name:       which
Version:    2.21
Release:    1%{?dist}
Summary:    Which

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://ftp.gnu.org/gnu/which
Source:     http://ftp.gnu.org/gnu/which/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build

%description
The which command shows the full pathname of a specified program, if the specified program is in your PATH.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

rm -f %{buildroot}/%{_infodir}/dir

%files
%defattr(-,root,root,-)
%{_bindir}/which

%files doc
%defattr(-,root,root,-)
%{_infodir}/which.info.gz
%{_mandir}/man1/which.1.gz

%clean
rm -rf %{buildroot}

%changelog
* Sat Mar 28 2015 tanggeliang <tanggeliang@gmail.com>
- update from 2.20 to 2.21
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
