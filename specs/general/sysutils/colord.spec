Name:       colord
Version:    1.2.12
Release:    1%{?dist}
Summary:    Colord

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.freedesktop.org
Source:     http://www.freedesktop.org/software/colord/releases/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  dbus libusb libgusb lcms2 sqlite
BuildRequires:  gobject-introspection polkit systemd systemd-udev systemd-gudev vala
BuildRequires:  gtk-doc intltool gettext xml-parser

%description
Colord is a system activated daemon that maps devices to color profiles.
It is used by GNOME Color Manager for system integration and use when there are no users logged in.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr                  \
            --sysconfdir=/etc              \
            --localstatedir=/var           \
            --with-daemon-user=colord      \
            --enable-vala                  \
            --enable-systemd-login=yes     \
            --disable-argyllcms-sensor     \
            --disable-bash-completion      \
            --disable-static               \
            --libdir=%{_libdir}            \
            --libexecdir=%{_libdir}/colord \
            --with-systemdsystemunitdir=/lib/systemd/system &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
/etc/dbus-1/system.d/org.freedesktop.*.conf
/lib/udev/rules.d/69-cd-sensors.rules
/lib/udev/rules.d/95-cd-devices.rules
%{_bindir}/cd-create-profile
%{_bindir}/cd-fix-profile
%{_bindir}/cd-iccdump
%{_bindir}/cd-it8
%{_bindir}/colormgr
%{_includedir}/colord-1/*
%{_libdir}/colord/colord
%{_libdir}/colord/colord-session
%{_libdir}/colord-sensors/*
%{_libdir}/colord-plugins/libcd_plugin_camera.*
%{_libdir}/colord-plugins/libcd_plugin_scanner.*
%{_libdir}/girepository-1.0/Colord-1.0.typelib
%{_libdir}/girepository-1.0/ColorHug-1.0.typelib
%{_libdir}/libcolord.*
%{_libdir}/libcolordprivate.*
%{_libdir}/libcolorhug.*
%{_libdir}/pkgconfig/colorhug.pc
%{_libdir}/pkgconfig/colord.pc
%{_datadir}/color/icc/colord/*.icc
%{_datadir}/colord/cmf/CIE19*-XYZ.cmf
%{_datadir}/colord/icons/*.svg
%{_datadir}/colord/illuminant/CIE-*.sp
%{_datadir}/colord/ref/CIE-1986-daylight-SPD.cmf
%{_datadir}/colord/ref/CIE-TCS.sp
%{_datadir}/colord/ti1/*.ti1
%{_datadir}/dbus-1/interfaces/org.freedesktop.*.xml
%{_datadir}/dbus-1/services/org.freedesktop.ColorHelper.service
%{_datadir}/dbus-1/system-services/org.freedesktop.*.service
%{_datadir}/gir-1.0/Colord-1.0.gir
%{_datadir}/gir-1.0/ColorHug-1.0.gir
%{_datadir}/glib-2.0/schemas/org.freedesktop.ColorHelper.gschema.xml
%{_datadir}/locale/*
%{_datadir}/polkit-1/actions/org.freedesktop.color.policy
%{_datadir}/vala/vapi/colord.vapi
%dir /var/lib/colord
/lib/systemd/system/colord.service

%post
groupadd -g 71 colord &&
useradd -c "Color Daemon Owner" -d /var/lib/colord -u 71 \
        -g colord -s /bin/false colord

%clean
rm -rf %{buildroot}

%changelog
* Wed Sep 23 2015 tanggeliang <tanggeliang@gmail.com>
- update from 1.2.9 to 1.2.12
* Sat Mar 28 2015 tanggeliang <tanggeliang@gmail.com>
- update from 1.2.1 to 1.2.9
* Sun Jul 20 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.1.2 to 1.2.1
* Tue Oct 1 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.0.3 to 1.1.2
* Sat Aug 24 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.0.2 to 1.0.3
* Wed Jul 24 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.0.0 to 1.0.2
* Wed May 29 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.1.31 to 1.0.0
* Wed Apr 10 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.1.23 to 0.1.31
* Mon Oct 1 2012 tanggeliang <tanggeliang@gmail.com>
- update from 0.1.22 to 0.1.23
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
