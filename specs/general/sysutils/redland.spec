Name:       redland
Version:    1.0.17
Release:    1%{?dist}
Summary:    Redland

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.librdf.org
Source:     http://download.librdf.org/source/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  rasqal

%description
Redland is a set of free software C libraries that provide support for the Resource Description Framework (RDF).
It is required by Soprano to build Nepomuk.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --disable-static \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/rdfproc
%{_bindir}/redland-config
%{_bindir}/redland-db-upgrade
%{_includedir}/librdf.h
%{_includedir}/rdf_*.h
%{_includedir}/redland.h
%{_libdir}/librdf.*
%{_libdir}/pkgconfig/redland.pc
%{_libdir}/redland/librdf_storage_sqlite.*
%{_datadir}/gtk-doc/html/redland/*
%{_datadir}/redland/Redland.i
%{_mandir}/man*/*.gz

%clean
rm -rf %{buildroot}

%changelog
* Sat Mar 22 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.0.16 to 1.0.17
* Wed Feb 27 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.0.15 to 1.0.16
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
