Name:       pm-utils
Version:    1.4.1
Release:    9%{?dist}
Summary:    Power management utilities and scripts

Group:      System Environment/Base
License:    GPLv2
URL:        http://pm-utils.freedesktop.org
Source0:    http://pm-utils.freedesktop.org/releases/pm-utils-%{version}.tar.gz
Source1:    http://pm-utils.freedesktop.org/releases/pm-quirks-20100619.tar.gz
Source2:    pm-utils-bugreport-info.sh

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  xmlto

%description
The pm-utils package contains utilities and scripts useful for tasks related to power management.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}
tar -xzf %{SOURCE1}

%build
./configure --prefix=/usr --docdir=%{_docdir}/pm-utils-%{version} \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
make install DESTDIR=%{buildroot}

cp -r video-quirks %{buildroot}/%{_libdir}/pm-utils
install -D -m 0755 %{SOURCE2} %{buildroot}/%{_sbindir}/pm-utils-bugreport-info.sh

install -D -m 0600 /dev/null %{buildroot}/var/log/pm-suspend.log
mkdir -p %{buildroot}/var/run/pm-utils/{locks,pm-suspend,pm-powersave}
touch %{buildroot}/var/run/pm-utils/locks/{pm-suspend.lock,pm-powersave.lock}
mkdir -p %{buildroot}/var/run/pm-utils/{pm-suspend,pm-powersave}/storage

%preun
rm -rf /var/run/pm-utils/{pm-suspend,pm-powersave}/storage/*

%files
%defattr(-,root,root,-)
%{_bindir}/on_ac_power
%{_bindir}/pm-is-supported
%{_libdir}/pkgconfig/pm-utils.pc
%{_libdir}/pm-utils/bin/pm-action
%{_libdir}/pm-utils/bin/pm-pmu
%{_libdir}/pm-utils/bin/pm-reset-swap
%{_libdir}/pm-utils/bin/service
%{_libdir}/pm-utils/defaults
%{_libdir}/pm-utils/functions
%{_libdir}/pm-utils/pm-functions
%{_libdir}/pm-utils/module.d/*
%{_libdir}/pm-utils/power.d/*
%{_libdir}/pm-utils/sleep.d/*
%{_libdir}/pm-utils/video-quirks/20-video-quirk-pm-*.quirkdb
%{_sbindir}/pm-hibernate
%{_sbindir}/pm-powersave
%{_sbindir}/pm-suspend
%{_sbindir}/pm-suspend-hybrid
%{_sbindir}/pm-utils-bugreport-info.sh
/var/log/pm-suspend.log
/var/run/pm-utils/locks/pm-powersave.lock
/var/run/pm-utils/locks/pm-suspend.lock

%files doc
%defattr(-,root,root,-)
%{_docdir}/pm-utils-%{version}/*
%{_mandir}/man*/*.gz

%clean
rm -rf %{buildroot}

%changelog
* Wed Sep 26 2012 tanggeliang <tanggeliang@gmail.com>
- hibernate need swap in the fstab.
* Sun May 1 2011 tanggeliang <tanggeliang@gmail.com>
- porting from Fedora
