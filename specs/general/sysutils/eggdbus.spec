Name:       eggdbus
Version:    0.6
Release:    1%{?dist}
Summary:    eggdbus, D-Bus GObject Bindings

Group:      System Environment/libraries
License:    GPLv2+
Url:        https://fedorahosted.org
Source:     http://hal.freedesktop.org/releases/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  dbus-glib, docbook-xsl

%description

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr
make

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/eggdbus-binding-tool
%{_bindir}/eggdbus-glib-genmarshal
%{_includedir}/eggdbus-1/eggdbus/eggdbus*.h
%{_libdir}/libeggdbus-1.*
%{_libdir}/pkgconfig/eggdbus-1.pc
%{_mandir}/*/*

%clean
rm -rf %{buildroot}

%changelog
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
