Name:       sg3_utils
Version:    1.39
Release:    1%{?dist}
Summary:    sg3 utils

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://sg.danny.cz
Source:     http://sg.danny.cz/sg/p/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  libtool

%description
The sg3_utils package contains low level utilities for devices that use a SCSI command set.
Apart from SCSI parallel interface (SPI) devices, the SCSI command set is used by ATAPI devices (CD/DVDs and tapes), USB mass storage devices, Fibre Channel disks, IEEE 1394 storage devices (that use the "SBP" protocol), SAS, iSCSI and FCoE devices (amongst others).

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --disable-static \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/scsi_logging_level
%{_bindir}/scsi_mandat
%{_bindir}/scsi_readcap
%{_bindir}/scsi_ready
%{_bindir}/scsi_satl
%{_bindir}/scsi_start
%{_bindir}/scsi_stop
%{_bindir}/scsi_temperature
%{_bindir}/sg_compare_and_write
%{_bindir}/sg_copy_results
%{_bindir}/sg_dd
%{_bindir}/sg_decode_sense
%{_bindir}/sg_emc_trespass
%{_bindir}/sg_format
%{_bindir}/sg_get_config
%{_bindir}/sg_get_lba_status
%{_bindir}/sg_ident
%{_bindir}/sg_inq
%{_bindir}/sg_logs
%{_bindir}/sg_luns
%{_bindir}/sg_map
%{_bindir}/sg_map26
%{_bindir}/sg_modes
%{_bindir}/sg_opcodes
%{_bindir}/sg_persist
%{_bindir}/sg_prevent
%{_bindir}/sg_raw
%{_bindir}/sg_rbuf
%{_bindir}/sg_rdac
%{_bindir}/sg_read
%{_bindir}/sg_read_block_limits
%{_bindir}/sg_read_buffer
%{_bindir}/sg_read_long
%{_bindir}/sg_readcap
%{_bindir}/sg_reassign
%{_bindir}/sg_referrals
%{_bindir}/sg_rep_zones
%{_bindir}/sg_requests
%{_bindir}/sg_reset
%{_bindir}/sg_reset_wp
%{_bindir}/sg_rmsn
%{_bindir}/sg_rtpg
%{_bindir}/sg_safte
%{_bindir}/sg_sanitize
%{_bindir}/sg_sat_identify
%{_bindir}/sg_sat_phy_event
%{_bindir}/sg_sat_set_features
%{_bindir}/sg_scan
%{_bindir}/sg_senddiag
%{_bindir}/sg_ses
%{_bindir}/sg_start
%{_bindir}/sg_stpg
%{_bindir}/sg_sync
%{_bindir}/sg_test_rwbuf
%{_bindir}/sg_turs
%{_bindir}/sg_unmap
%{_bindir}/sg_verify
%{_bindir}/sg_vpd
%{_bindir}/sg_wr_mode
%{_bindir}/sg_write_buffer
%{_bindir}/sg_write_long
%{_bindir}/sg_write_same
%{_bindir}/sg_xcopy
%{_bindir}/sginfo
%{_bindir}/sgm_dd
%{_bindir}/sgp_dd
%{_includedir}/scsi/sg_*.h
%{_libdir}/libsgutils2.la
%{_libdir}/libsgutils2.so*
%{_mandir}/man8/*scsi*.8.gz
%{_mandir}/man8/sg*.8.gz

%clean
rm -rf %{buildroot}

%changelog
* Wed Oct 15 2014 tanggeliang <tanggeliang@gmail.com>
- create
