Name:       npapi-sdk
Version:    0.27.2
Release:    1%{?dist}
Summary:    NPAPI-SDK

Group:      System Environment/libraries
License:    GPLv2+
Url:        https://bitbucket.org/mgorny/npapi-sdk
Source:     https://bitbucket.org/mgorny/npapi-sdk/downloads/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build

%description
NPAPI-SDK is a bundle of Netscape Plugin Application Programming Interface headers by Mozilla.
This package provides a clear way to install those headers and depend on them.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --libdir=%{_libdir}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_includedir}/npapi-sdk/np*.h
%{_libdir}/pkgconfig/npapi-sdk.pc

%clean
rm -rf %{buildroot}

%changelog
* Tue Dec 17 2013 tanggeliang <tanggeliang@gmail.com>
- create
