Name:       apr
Version:    1.5.1
Release:    1%{?dist}
Summary:    Apr

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://archive.apache.org
Source:     http://archive.apache.org/dist/apr/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build

%description
The Apache Portable Runtime (APR) is a supporting library for the Apache web server.
It provides a set of application programming interfaces (APIs) that map to the underlying Operating System (OS).
Where the OS doesn't support a particular function, APR will provide an emulation.
Thus programmers can use the APR to make a program portable across different platforms. 

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr    \
            --disable-static \
            --with-installbuilddir=/usr/share/apr-1/build \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/apr-1-config
%{_includedir}/apr-1/apr*.h
%{_libdir}/apr.exp
%{_libdir}/libapr-1.*
%{_libdir}/pkgconfig/apr-1.pc
%{_datadir}/apr-1/build/apr_rules.mk
%{_datadir}/apr-1/build/libtool
%{_datadir}/apr-1/build/make_exports.awk
%{_datadir}/apr-1/build/make_var_export.awk
%{_datadir}/apr-1/build/mkdir.sh

%clean
rm -rf %{buildroot}

%changelog
* Sun Jul 20 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.5.0 to 1.5.1
* Tue Dec 3 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.4.8 to 1.5.0
* Wed Jul 31 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.4.6 to 1.4.8
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
