Name:       unixodbc
Version:    2.3.2
Release:    1%{?dist}
Summary:    unixODBC

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.unixodbc.org
Source:     http://www.unixodbc.org/unixODBC-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build

%description
The unixODBC package is an Open Source ODBC (Open DataBase Connectivity) sub-system and an ODBC SDK for Linux, Mac OSX, and UNIX.
ODBC is an open specification for providing application developers with a predictable API with which to access data sources.
Data sources include optional SQL Servers and any data source with an ODBC Driver.
unixODBC contains the following components used to assist with the manipulation of ODBC data sources: a driver manager, an installer library and command line tool, command line tools to help install a driver and work with SQL, drivers and driver setup libraries.

%prep
%setup -q -n unixODBC-%{version}

%build
./configure --prefix=/usr \
            --sysconfdir=/etc/unixODBC \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install &&

find doc -name "Makefile*" -exec rm {} \; &&
chmod 644 doc/{lst,ProgrammerManual/Tutorial}/* &&

install -v -m755 -d %{buildroot}/usr/share/doc/unixODBC-%{version} &&
cp -v -R doc/* %{buildroot}/usr/share/doc/unixODBC-%{version}

%files
%defattr(-,root,root,-)
/etc/unixODBC/odbc*.ini
%{_bindir}/dltest
%{_bindir}/isql
%{_bindir}/iusql
%{_bindir}/odbc_config
%{_bindir}/odbcinst
%{_bindir}/slencheck
%{_includedir}/*.h
%{_libdir}/libodbc.*
%{_libdir}/libodbccr.*
%{_libdir}/libodbcinst.*
%{_docdir}/unixODBC-%{version}/*
%{_mandir}/man1/*.1.gz
%{_mandir}/man5/*.5.gz

%clean
rm -rf %{buildroot}

%changelog
* Tue Dec 17 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.3.1 to 2.3.2
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
