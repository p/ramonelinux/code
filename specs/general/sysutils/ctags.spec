Name:       ctags
Version:    5.8
Release:    1%{?dist}
Summary:    A C programming language indexing and/or cross-reference tool

Group:      System Environment/Tools
License:    GPLv2+
Url:        http://ctags.sourceforge.net
Source:     http://downloads.sourceforge.net/%{name}/%{name}-%{version}.tar.gz
Patch:      %{name}-%{version}-destdir.patch

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build

%description
Ctags generates an index (or tag) file of C language objects found in C source and header files.

%prep
%setup -q -n %{name}-%{version}
%patch -p1

%build
./configure --prefix=/usr &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/ctags
%{_mandir}/man1/ctags.1.gz

%clean
rm -rf %{buildroot}

%changelog
* Sat Jun 22 2013 tanggeliang <tanggeliang@gmail.com>
- create
