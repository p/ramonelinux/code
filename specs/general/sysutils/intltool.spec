Name:       intltool
Version:    0.50.2
Release:    8%{?dist}
Summary:    Intltool

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.intltool.com
Source:     http://launchpad.net/intltool/trunk/0.41.1/download/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  xml-parser

%description
The intltool package contains an internationalization tool.
This is useful for extracting translatable strings from source files, collecting the extracted strings with messages from traditional source files (<source directory>/<package>/po) and merging the translations into .xml, .desktop and .oaf files.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/intltool-extract
%{_bindir}/intltool-merge
%{_bindir}/intltool-prepare
%{_bindir}/intltool-update
%{_bindir}/intltoolize
%{_datadir}/aclocal/intltool.m4
%{_datadir}/intltool/Makefile.in.in

%files doc
%defattr(-,root,root,-)
%{_mandir}/man8/intltool*.8.gz

%clean
rm -rf %{buildroot}

%changelog
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
