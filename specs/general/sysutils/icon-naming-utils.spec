Name:       icon-naming-utils
Version:    0.8.90
Release:    6%{?dist}
Summary:    icon-naming-utils

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.freedesktop.org
Source:     http://tango.freedesktop.org/releases/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  xml-simple

%description
The icon-naming-utils package contains a Perl script used for maintaining backwards compatibility with current desktop icon themes, while migrating to the names specified in the Icon Naming Specification.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --libexecdir=%{_libdir}/icon-naming-utils &&
make

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_libdir}/icon-naming-utils/icon-name-mapping
%{_datadir}/dtds/*
%{_datadir}/icon-naming-utils/*
%{_datadir}/pkgconfig/icon-naming-utils.pc

%clean
rm -rf %{buildroot}

%changelog
* Sat Jul 27 2013 tanggeliang <tanggeliang@gmail.com>
- remove BuildArch noarch
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
