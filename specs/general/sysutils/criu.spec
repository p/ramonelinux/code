Name:       criu
Version:    1.3
Release:    1%{?dist}
Summary:    CRIU, a project to implement checkpoint/restore functionality for Linux in userspace.

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.criu.org/Main_Page
Source:     http://download.openvz.org/criu/%{name}-%{version}-rc2.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  protobuf-c

%description
Checkpoint/Restore In Userspace, or CRIU (pronounced kree-oo, IPA: /krɪʊ/, Russian: криу), is a software tool for Linux operating system.
Using this tool, you can freeze a running application (or part of it) and checkpoint it to a hard drive as a collection of files.
You can then use the files to restore and run the application from the point it was frozen at.
The distinctive feature of the CRIU project is that it is mainly implemented in user space.

%prep
%setup -q -n %{name}-%{version}-rc2

%build
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
sed -i '248s/install-man//' Makefile
make PREFIX=/usr LOGROTATEDIR=/etc SYSTEMDUNITDIR=/lib/systemd/system DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
/etc/criu-service
%{_includedir}/criu/criu*.h
%{_includedir}/criu/rpc.proto
/lib/systemd/system/criu.service
/lib/systemd/system/criu.socket
%{_libdir}/libcriu.so*
%{_sbindir}/criu

%clean
rm -rf %{buildroot}

%changelog
* Tue Aug 5 2014 tanggeliang <tanggeliang@gmail.com>
- create
