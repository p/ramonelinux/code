Name:       dbus
Version:    1.8.16
Release:    1%{?dist}
Summary:    D-Bus

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.freedesktop.org
Source:     http://dbus.freedesktop.org/releases/dbus/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  expat libxml libxslt
BuildRequires:  libx11 libice libsm xproto
BuildRequires:  systemd
# for groupadd
Requires(pre):  shadow systemd

%description
D-Bus is a message bus system, a simple way for applications to talk to one another.
D-Bus supplies both a system daemon (for events such as "new hardware device added" or "printer queue changed") and a per-user-login-session daemon (for general IPC needs among user applications).
Also, the message bus is built on top of a general one-to-one message passing framework, which can be used by any two applications to communicate directly (without going through the message bus daemon).

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr 		   \
            --sysconfdir=/etc 		   \
            --localstatedir=/var	   \
            --disable-doxygen-docs         \
            --disable-xml-docs             \
            --disable-static 		   \
            --enable-systemd 		   \
            --with-systemdsystemunitdir=/lib/systemd/system \
            --with-console-auth-dir=/run/console/ 	    \
            --docdir=/usr/share/doc/dbus-%{version} 	    \
            --libexecdir=%{_libdir}/dbus-1.0 		    \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

cat > %{buildroot}/etc/dbus-1/session-local.conf << "EOF"
<!DOCTYPE busconfig PUBLIC
 "-//freedesktop//DTD D-BUS Bus Configuration 1.0//EN"
 "http://www.freedesktop.org/standards/dbus/1.0/busconfig.dtd">
<busconfig>

  <!-- Search for .service files in /usr/local -->
  <servicedir>/usr/local/share/dbus-1/services</servicedir>

</busconfig>
EOF

%files
%defattr(-,root,root,-)
%{_sysconfdir}/dbus-1/session-local.conf
%{_sysconfdir}/dbus-1/session.conf
%{_sysconfdir}/dbus-1/system.conf
%dir %{_sysconfdir}/dbus-1/system.d
%dir %{_sysconfdir}/dbus-1/session.d
%{_bindir}/dbus-cleanup-sockets
%{_bindir}/dbus-daemon
%{_bindir}/dbus-launch
%{_bindir}/dbus-monitor
%{_bindir}/dbus-run-session
%{_bindir}/dbus-send
%{_bindir}/dbus-uuidgen
%{_includedir}/dbus-1.0/dbus/dbus*.h
%attr(4750,root,messagebus) %{_libdir}/dbus-1.0/dbus-daemon-launch-helper
%{_libdir}/dbus-1.0/include/dbus/dbus-arch-deps.h
%{_libdir}/libdbus-1.*
%{_libdir}/pkgconfig/dbus-1.pc
%dir %{_localstatedir}/lib/dbus
%dir %{_localstatedir}/run/dbus
/lib/systemd/system/dbus.service
/lib/systemd/system/dbus.socket
/lib/systemd/system/dbus.target.wants/dbus.socket
/lib/systemd/system/multi-user.target.wants/dbus.service
/lib/systemd/system/sockets.target.wants/dbus.socket

%files doc
%defattr(-,root,root,-)
%{_docdir}/dbus-%{version}/*

%pre
groupadd -g 18 messagebus &&
useradd -c "D-Bus Message Daemon User" -d /var/run/dbus \
        -u 18 -g messagebus -s /bin/false messagebus

%post

%postun
userdel messagebus

%clean
rm -rf %{buildroot}

%changelog
* Sat Mar 28 2015 tanggeliang <tanggeliang@gmail.com>
- update from 1.8.6 to 1.8.16
* Sun Jul 20 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.8.0 to 1.8.6
* Sun Mar 16 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.7.4 to 1.8.0
* Mon Sep 23 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.6.12 to 1.7.4
* Wed Jul 24 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.6.8 to 1.6.12
* Sun Oct 21 2012 tanggeliang <tanggeliang@gmail.com>
- update from 1.6.4 to 1.6.8
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
