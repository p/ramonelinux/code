Name:       apr-util
Version:    1.5.3
Release:    1%{?dist}
Summary:    Apr-Util

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://archive.apache.org
Source:     http://archive.apache.org/dist/apr/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  apr openssl
BuildRequires:  db expat sqlite

%description
The Apache Portable Runtime Utility Library provides a predictable and consistent interface to underlying client library interfaces.
This application programming interface assures predictable if not identical behaviour regardless of which libraries are available on a given platform. 

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr       \
            --with-apr=/usr     \
            --with-gdbm=/usr    \
            --with-openssl=/usr \
            --with-crypto \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/apu-1-config
%{_includedir}/apr-1/apr_*.h
%{_includedir}/apr-1/apu*.h
%{_libdir}/apr-util-1/apr_crypto_openssl*
%{_libdir}/apr-util-1/apr_dbd_sqlite3*.*
%{_libdir}/apr-util-1/apr_dbm_gdbm*.*
%{_libdir}/aprutil.exp
%{_libdir}/libaprutil-1.*
%{_libdir}/pkgconfig/apr-util-1.pc

%clean
rm -rf %{buildroot}

%changelog
* Tue Dec 3 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.5.2 to 1.5.3
* Wed Jul 31 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.5.1 to 1.5.2
* Sat Oct 27 2012 tanggeliang <tanggeliang@gmail.com>
- update from 1.4.1 to 1.5.1
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
