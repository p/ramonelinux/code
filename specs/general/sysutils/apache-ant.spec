Name:       apache-ant
Version:    1.9.3
Release:    1%{?dist}
Summary:    Apache Ant

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.apache.org
Source:     http://archive.apache.org/dist/ant/source/%{name}-%{version}-src.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  openjdk-jre openjdk junit

%description
The Apache Ant package is a Java-based build tool.
In theory, it is kind of like make, but without make's wrinkles.
Ant is different.
Instead of a model that is extended with shell-based commands, Ant is extended using Java classes.
Instead of writing shell commands, the configuration files are XML-based, calling out a target tree that executes various tasks. Each task is run by an object that implements a particular task interface.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
sed -i 's;jars,test-jar;jars;' build.xml
cp -v /usr/share/java/junit-4.11.jar lib/optional/junit.jar

%check

%install
rm -rf %{buildroot}
./build.sh -Ddist.dir=%{buildroot}/%{_datadir}/ant-%{version} dist &&
ln -v -sfn ant-%{version} %{buildroot}/%{_datadir}/ant

mkdir -pv %{buildroot}/etc/profile.d
cat > %{buildroot}/etc/profile.d/ant.sh << "EOF"
# Begin /etc/profile.d/ant.sh

# Set ANT_HOME directory
ANT_HOME=/usr/share/ant

# Adjust PATH
pathappend $ANT_HOME/bin PATH

# End /etc/profile.d/ant.sh
EOF

%files
%defattr(-,root,root,-)
/etc/profile.d/ant.sh
%{_datadir}/ant-%{version}/bin/ant
%{_datadir}/ant-%{version}/bin/ant.bat
%{_datadir}/ant-%{version}/bin/ant.cmd
%{_datadir}/ant-%{version}/bin/antenv.cmd
%{_datadir}/ant-%{version}/bin/antRun
%{_datadir}/ant-%{version}/bin/antRun.bat
%{_datadir}/ant-%{version}/bin/antRun.pl
%{_datadir}/ant-%{version}/bin/complete-ant-cmd.pl
%{_datadir}/ant-%{version}/bin/envset.cmd
%{_datadir}/ant-%{version}/bin/lcp.bat
%{_datadir}/ant-%{version}/bin/runant.pl
%{_datadir}/ant-%{version}/bin/runant.py
%{_datadir}/ant-%{version}/bin/runrc.cmd
%{_datadir}/ant-%{version}/etc/ant-bootstrap.jar
%{_datadir}/ant-%{version}/etc/checkstyle/checkstyle-*.xsl
%{_datadir}/ant-%{version}/etc/*.xsl
%{_datadir}/ant-%{version}/KEYS
%{_datadir}/ant-%{version}/lib/ant*.*
%{_datadir}/ant-%{version}/lib/libraries.properties
%{_datadir}/ant

%files doc
%defattr(-,root,root,-)
%{_datadir}/ant-%{version}/fetch.xml
%{_datadir}/ant-%{version}/get-m2.xml
%{_datadir}/ant-%{version}/INSTALL
%{_datadir}/ant-%{version}/lib/README
%{_datadir}/ant-%{version}/LICENSE
%{_datadir}/ant-%{version}/NOTICE
%{_datadir}/ant-%{version}/README
%{_datadir}/ant-%{version}/WHATSNEW
%{_datadir}/ant-%{version}/manual/*

%clean
rm -rf %{buildroot}

%changelog
* Thu Mar 20 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.9.2 to 1.9.3
* Thu Jul 25 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.8.4 to 1.9.2
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
