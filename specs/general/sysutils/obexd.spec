Name:       obexd
Version:    0.48
Release:    2%{?dist}
Summary:    Obexd

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.kernel.org/pub/linux/bluetooth
Source:     http://www.kernel.org/pub/linux/bluetooth/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  bluez libical

%description
The Obexd package contains D-Bus services providing OBEX client and server functionality.
OBEX is a communications protocol that facilitates the exchange of binary objects between devices.

%prep
%setup -q -n %{name}-%{version}

%build
sed -i 's/#include <string.h>/&\n#include <stdio.h>/' plugins/mas.c &&
./configure --prefix=/usr --libexecdir=%{_libdir}/obex &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_libdir}/obex/obex-client
%{_libdir}/obex/obexd
%{_datadir}/dbus-1/services/obex-client.service
%{_datadir}/dbus-1/services/obexd.service

%clean
rm -rf %{buildroot}

%changelog
* Sun Mar 31 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.47 to 0.48
* Mon Oct 29 2012 tanggeliang <tanggeliang@gmail.com>
- create
