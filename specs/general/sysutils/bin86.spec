Name:       bin86
Version:    0.16.19
Release:    1%{?dist}
Summary:    Bin86

Group:      Development/Debuggers
License:    GPLv2+
Url:        http://freshmeat.net/projects/bin86/
Source:     http://rdebath.nfshost.com/dev86/%{name}-%{version}.tar.gz
Patch:      http://svn.cross-lfs.org/svn/repos/patches/bin86/%{name}-%{version}-x86_64-1.patch

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build

%description
The package contains a simple assember and linker for 8086 - 80386 machine code.

%prep
%setup -q -n %{name}-%{version}
%patch -p1

%build
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}/usr/{bin,lib,man/man1}
make PREFIX=%{buildroot}/usr install

%files
%defattr(-,root,root,-)
%{_bindir}/as86
%{_bindir}/ld86
%{_bindir}/nm86
%{_bindir}/objdump86
%{_bindir}/size86
/usr/man/man1/as86.1.gz
/usr/man/man1/ld86.1.gz

%clean
rm -rf %{buildroot}

%changelog
* Sat Dec 14 2013 tanggeliang <tanggeliang@gmail.com>
- create
