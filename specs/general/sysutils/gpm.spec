%define LIBVER 2.1.0

Name:       gpm
Version:    1.20.7
Release:    1%{?dist}
Summary:    gpm

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.nico.schottelius.org/software/gpm
Source:     http://www.nico.schottelius.org/software/gpm/archives/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  autoconf automake m4 libtool
BuildRequires:  bison

%description
The GPM (General Purpose Mouse daemon) package contains a mouse server for the console and xterm.
It not only provides cut and paste support generally, but its library component is used by various software such as Links to provide mouse support to the application.
It is useful on desktops, especially if following (Beyond) Linux From Scratch instructions; it's often much easier (and less error prone) to cut and paste between two console windows than to type everything by hand!

%prep
%setup -q -n %{name}-%{version}

%build
./autogen.sh                                &&
./configure --prefix=/usr --sysconfdir=/etc \
            --libdir=%{_libdir}             &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install &&

chmod 0755 %{buildroot}/%{_libdir}/libgpm.so.%{LIBVER}            &&
ln -v -sfn libgpm.so.%{LIBVER} %{buildroot}%{_libdir}/libgpm.so   &&
install -v -m644 conf/gpm-root.conf %{buildroot}/etc              &&

install -v -m755 -d %{buildroot}/usr/share/doc/gpm-1.20.7/support &&
install -v -m644    doc/support/*                     \
                    %{buildroot}/usr/share/doc/gpm-1.20.7/support &&
install -v -m644    doc/{FAQ,HACK_GPM,README*}        \
                    %{buildroot}/usr/share/doc/gpm-1.20.7

%post
#install-info --dir-file=/usr/share/info/dir           \
#             /usr/share/info/gpm.info                 &&

%files
%defattr(-,root,root,-)
/etc/gpm-root.conf
%{_bindir}/disable-paste
%{_bindir}/display-buttons
%{_bindir}/display-coords
%{_bindir}/get-versions
%{_bindir}/gpm-root
%{_bindir}/hltest
%{_bindir}/mev
%{_bindir}/mouse-test
%{_includedir}/gpm.h
%{_libdir}/libgpm.a
%{_libdir}/libgpm.so*
%{_sbindir}/gpm
%{_docdir}/gpm-1.20.7/*
%{_infodir}/gpm.info.gz
%{_mandir}/man*/*.gz

%clean
rm -rf %{buildroot}

%changelog
* Sun May 12 2013 tanggeliang <tanggeliang@gmail.com>
- create
