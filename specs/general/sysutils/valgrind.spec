Name:       valgrind
Version:    3.8.1
Release:    1%{?dist}
Summary:    valgrind

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.valgrind.org
Source:     http://www.valgrind.org/downloads/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  autoconf automake m4

%description
Valgrind is an instrumentation framework for building dynamic analysis tools. There are Valgrind tools that can automatically detect many memory management and threading bugs, and profile your programs in detail. You can also use Valgrind to build new tools.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
sed -i '902s/2.16/2.17/' configure.in
autoreconf -i
./configure --prefix=/usr \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/callgrind_annotate
%{_bindir}/callgrind_control
%{_bindir}/cg_annotate
%{_bindir}/cg_diff
%{_bindir}/cg_merge
%{_bindir}/ms_print
%{_bindir}/valgrind
%{_bindir}/valgrind-listener
%{_bindir}/vgdb
%{_includedir}/valgrind/*.h
%{_includedir}/valgrind/vki/vki-*.h
%{_libdir}/pkgconfig/valgrind.pc
%{_libdir}/valgrind/*-*-valgrind-s1.xml
%{_libdir}/valgrind/*-*-valgrind-s2.xml
%{_libdir}/valgrind/*-*.xml
%{_libdir}/valgrind/*-*-valgrind.xml
%{_libdir}/valgrind/*-x86-linux*
%{_libdir}/valgrind/default.supp
%{_libdir}/valgrind/vgpreload_*-x86-linux.so

%files doc
%defattr(-,root,root,-)
%{_docdir}/valgrind/*
%{_mandir}/man1/*.1.gz

%clean
rm -rf %{buildroot}

%changelog
* Fri Jul 12 2013 tanggeliang <tanggeliang@gmail.com>
- create
