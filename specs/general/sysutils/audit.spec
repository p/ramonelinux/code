Name:       audit
Version:    2.4.1
Release:    1%{?dist}
Summary:    User space tools for 2.6 kernel auditing

Group:      Development/Libraries
License:    BSD and LGPLv2+
Url:        http://people.redhat.com/sgrubb/audit
Source:     http://people.redhat.com/sgrubb/audit/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  systemd swig openldap

%description
The audit package contains the user space utilities for storing and searching the audit records generate by the audit subsystem in the Linux 2.6 kernel.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr       \
            --sysconfdir=/etc   \
            --libdir=%{_libdir} \
	    --enable-systemd &&
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install initdir=/lib/systemd/system

%files
%defattr(-,root,root,-)
/etc/audisp/audisp-remote.conf
/etc/audisp/audispd.conf
/etc/audisp/plugins.d/af_unix.conf
/etc/audisp/plugins.d/au-remote.conf
/etc/audisp/plugins.d/audispd-zos-remote.conf
/etc/audisp/plugins.d/syslog.conf
/etc/audisp/zos-remote.conf
/etc/audit/auditd.conf
/etc/audit/rules.d/audit.rules
/etc/libaudit.conf
/lib/systemd/system/auditd.service
%{_bindir}/aulast
%{_bindir}/aulastlog
%{_bindir}/ausyscall
%{_bindir}/auvirt
%{_includedir}/auparse-defs.h
%{_includedir}/auparse.h
%{_includedir}/libaudit.h
%{_libdir}/libaudit.*
%{_libdir}/libauparse.*
%{_libdir}/pkgconfig/audit.pc
%{_libdir}/python2.7/site-packages/*
/usr/libexec/initscripts/legacy-actions/auditd/*
%{_sbindir}/audisp-remote
%{_sbindir}/audispd
%{_sbindir}/audispd-zos-remote
%{_sbindir}/auditctl
%{_sbindir}/auditd
%{_sbindir}/augenrules
%{_sbindir}/aureport
%{_sbindir}/ausearch
%{_sbindir}/autrace
%{_mandir}/man*/*.gz

%changelog
* Mon Mar 30 2015 tanggeliang <tanggeliang@gmail.com>
- create
