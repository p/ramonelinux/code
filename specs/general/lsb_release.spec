Name:       lsb_release
Version:    1.4
Release:    5%{?dist}
Summary:    Linux Standard Base (LSB)

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://lsb.sourceforge.net
Source:     http://sourceforge.net/projects/lsb/files/lsb_release/1.4/lsb-release-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build

%description
The Linux Standard Base was created to lower the overall costs of supporting the Linux platform.
By reducing the differences between individual Linux distributions, the LSB greatly reduces the costs involved with porting applications to different distributions, as well as lowers the cost and effort involved in after-market support of those applications.

%prep
%setup -q -n lsb-release-%{version}

%build
./help2man -N --include ./lsb_release.examples \
              --alt_version_key=program_version ./lsb_release > lsb_release.1
%check

%install
mkdir -pv %{buildroot}/usr/share/man/man1
mkdir -pv %{buildroot}/usr/bin

install -v -m 644 lsb_release.1 %{buildroot}/usr/share/man/man1/lsb_release.1 &&
install -v -m 755 lsb_release %{buildroot}/usr/bin/lsb_release

%files
%defattr(-,root,root,-)
%{_bindir}/lsb_release
%{_mandir}/man1/lsb_release.1.gz

%clean
rm -rf %{buildroot}

%changelog
* Sun May 1 2011 tanggeliang <tanggeliang@gmail.com>
- porting from Fedora
