Name:       ibus-pinyin
Version:    1.5.0
Release:    1%{?dist}
Summary:    The Chinese Pinyin and Bopomofo engines for IBus input platform

License:    GPLv2+
Group:      System Environment/Libraries
Url:        http://code.google.com/p/ibus
Source:     http://ibus.googlecode.com/files/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build

BuildRequires:  gettext intltool libtool pkg-config sqlite
BuildRequires:  ibus autoconf automake m4 xml-parser opencc pyzy
Requires:       ibus

%description
The Chinese Pinyin and Bopomofo input methods for IBus platform.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --libexecdir=%{_libdir}/ibus \
            --disable-static --enable-db-open-phrase \
            --enable-opencc \
            --disable-boost &&
make %{?_smp_mflags}

%install
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_libdir}/ibus/ibus-engine-pinyin
%{_libdir}/ibus/ibus-setup-pinyin
%{_datadir}/ibus-pinyin/phrases.txt
%{_datadir}/ibus-pinyin/icons
%{_datadir}/ibus-pinyin/setup
%{_datadir}/ibus/component/*
%{_datadir}/ibus-pinyin/db/english.db
%{_datadir}/applications/ibus-setup-*.desktop
%{_datadir}/locale/*/LC_MESSAGES/ibus-pinyin.mo

%changelog
* Mon Dec 24 2012 tanggeliang <tanggeliang@gmail.com>
- update from 1.4.99.20120620 to 1.5.0
