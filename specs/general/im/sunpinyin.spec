%define _xinputconf %{_sysconfdir}/X11/xinit/xinput.d/xsunpinyin.conf
%define gitdate 20130710

Name:		sunpinyin
Version:	2.0.4
Release:	0.9%{?dist}
Summary:	A statistical language model based Chinese input method engine
Group:		System Environment/Libraries
License:	LGPLv2 or CDDL
URL:		http://code.google.com/p/sunpinyin/
Source0:	%{name}-%{gitdate}.tar.xz
Source2:	http://open-gram.googlecode.com/files/lm_sc.t3g.arpa-20121025.tar.bz2
Source3:	http://open-gram.googlecode.com/files/dict.utf8-20130220.tar.bz2
BuildRequires:	sqlite
BuildRequires:	gettext	
BuildRequires:	scons
BuildRequires:	perl(Pod::Man)
BuildRequires:	python

%description
Sunpinyin is an input method engine for Simplified Chinese. It is an SLM based
IM engine, and features full sentence input.

SunPinyin has been ported to various input method platforms and operating 
systems. The 2.0 release currently supports iBus, XIM, and Mac OS X. 

%prep
%setup -q -n %{name}-%{gitdate}
mkdir -p raw
cp %SOURCE2 raw
cp %SOURCE3 raw
pushd raw
tar xvf lm_sc.t3g.arpa-20121025.tar.bz2
tar xvf dict.utf8-20130220.tar.bz2
popd

%build
scons %{?_smp_mflags} --prefix=%{_prefix} --libdir=%{_libdir} --datadir=%{_datadir}
export PATH=`pwd`/src:$PATH
pushd raw
ln -sf ../doc/SLM-inst.mk Makefile
make %{?_smp_mflags} VERBOSE=1
popd

%install
scons %{?_smp_mflags} --prefix=%{_prefix} --libdir=%{_libdir} --datadir=%{_datadir} install --install-sandbox=%{buildroot}
pushd raw
make install DESTDIR=$RPM_BUILD_ROOT INSTALL="install -p"
popd

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING *.LICENSE
%doc README TODO
%{_libdir}/libsunpinyin*.so.*
%{_docdir}/%{name}/README
%{_libdir}/libsunpinyin*.so
%{_libdir}/pkgconfig/sunpinyin*.pc
%{_includedir}/sunpinyin*
%{_datadir}/%{name}
%{_bindir}/*
%{_mandir}/man1/*.1.gz
%{_docdir}/%{name}/SLM-*.mk

%changelog
