Name:       pyzy
Version:    0.1.0
Release:    1%{?dist}
Summary:    The Chinese PinYin and Bopomofo conversion library

Group:      System Environment/Libraries
License:    LGPLv2+
URL:        http://code.google.com/p/pyzy
Source0:    http://pyzy.googlecode.com/files/%{name}-%{version}.tar.gz
Source1:    http://pyzy.googlecode.com/files/pyzy-database-1.0.0.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  glib libtool pkg-config sqlite

%description
The Chinese Pinyin and Bopomofo conversion library.

%prep
%setup -q -n %{name}-%{version}
cp -p %{SOURCE1} data/db/open-phrase

%build
./configure --prefix=/usr \
            --libdir=%{_libdir} \
            --disable-static --enable-db-open-phrase &&
make %{?_smp_mflags}

%install
make install DESTDIR=%{buildroot} INSTALL="install -p"

%files
%defattr(-,root,root,-)
%{_libdir}/lib*.so.*
%{_libdir}/lib*.so
%{_libdir}/*.la
%{_libdir}/pkgconfig/*
%{_includedir}/*
%{_datadir}/pyzy/phrases.txt
%{_datadir}/pyzy/db/android.db
%{_datadir}/pyzy/db/create_index.sql
%{_datadir}/pyzy/db/open-phrase.db

%changelog
* Thu Aug 15 2013 tanggeliang <tanggeliang@gmail.com>
- create
