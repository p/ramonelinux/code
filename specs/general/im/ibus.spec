Name:       ibus
Version:    1.5.4
Release:    1%{?dist}
Summary:    IBus - Intelligent Input Bus for Linux / Unix OS

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://ibus.googlecode.com
Source:     http://ibus.googlecode.com/files/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  dconf iso-codes pyxdg
BuildRequires:  gobject-introspection vala
BuildRequires:  dbus-python gtk2 gtk-doc gconf
BuildRequires:  intltool gettext xml-parser libnotify
Requires:       pygobject

%description
IBus is an Intelligent Input Bus. It is a new input framework for Linux OS.
It provides full featured and user friendly input method user interface.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --sysconfdir=/etc \
            --libexecdir=%{_libdir}/ibus \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

rm -f %{buildroot}/%{_infodir}/dir

%files
%defattr(-,root,root,-)
/etc/dconf/db/ibus.d/00-upstream-settings
/etc/dconf/profile/ibus
%{_bindir}/ibus
%{_bindir}/ibus-daemon
%{_bindir}/ibus-setup
%{_includedir}/ibus-1.0/ibus*.h
%{_libdir}/girepository-1.0/IBus-1.0.typelib
%{_libdir}/gtk-2.0/2.10.0/immodules/im-ibus.*
%{_libdir}/gtk-3.0/3.0.0/immodules/im-ibus.*
%{_libdir}/ibus/ibus-dconf
%{_libdir}/ibus/ibus-engine-simple
%{_libdir}/ibus/ibus-ui-gtk3
%{_libdir}/ibus/ibus-x11
%{_libdir}/libibus-1.0.la
%{_libdir}/libibus-1.0.so*
%{_libdir}/pkgconfig/ibus-1.0.pc
%{_datadir}/GConf/gsettings/ibus.convert
%{_datadir}/applications/ibus-setup.desktop
%{_datadir}/bash-completion/completions/ibus.bash
%{_datadir}/gir-1.0/IBus-1.0.gir
%{_datadir}/glib-2.0/schemas/org.freedesktop.ibus.gschema.xml
%{_datadir}/ibus/component/*.xml
%{_datadir}/ibus/keymaps/*
%{_datadir}/ibus/setup/*.py*
%{_datadir}/ibus/setup/setup.ui
%{_datadir}/icons/hicolor/*/apps/ibus-keyboard.png
%{_datadir}/icons/hicolor/scalable/apps/ibus*.svg
%{_datadir}/locale/*/LC_MESSAGES/ibus10.mo
%{_datadir}/vala/vapi/ibus-1.0.*

%files doc
%defattr(-,root,root,-)
%{_datadir}/gtk-doc/html/ibus/*
%{_mandir}/man1/ibus*.1.gz

%post
gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
glib-compile-schemas %{_datadir}/glib-2.0/schemas &>/dev/null || :
gtk-query-immodules-3.0 --update-cache

%postun

%clean
rm -rf %{buildroot}

%changelog
* Wed Oct 2 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.5.3 to 1.5.4
* Thu Aug 15 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.5.2 to 1.5.3
* Wed Jul 24 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.5.1 to 1.5.2
* Mon Dec 24 2012 tanggeliang <tanggeliang@gmail.com>
- update from 1.4.99.20121006 to 1.5.1
* Sat Sep 29 2012 tanggeliang <tanggeliang@gmail.com>
- create
