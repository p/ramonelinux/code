Name:		fcitx-configtool
Version:	0.4.7
Release:	2%{?dist}
Summary:	Gtk configuretool for Fcitx
Group:		System Environment/Libraries
License:	GPLv2+
URL:		https://fcitx-im.org/wiki/Fcitx
Source0:	http://download.fcitx-im.org/fcitx-configtool/%{name}-%{version}.tar.xz
#Patch0:		fix_segenttation_fault_on_gtk3.patch

BuildRequires:	cmake, fcitx, gettext, intltool, libxml
BuildRequires:	gtk2, iso-codes, libtool, libunique
BuildRequires:	gtk+
Requires:	fcitx


%description
Fcitx-config Gtk based configure tool for fcitx.

KDE Version see fcitx-config-kde4 or kcm-fcitx.


%prep
%setup -q -n %{name}-%{version}
#%patch0 -p1

%build
mkdir -pv build
pushd build
cmake -DCMAKE_INSTALL_PREFIX=/usr \
      -DSYSCONF_INSTALL_DIR=/etc \
      -DCMAKE_BUILD_TYPE=Release \
%ifarch x86_64
      -DLIB_SUFFIX=64 \
%endif \
    -DENABLE_GTK3=ON -DENABLE_GTK2=ON ..
make %{?_smp_mflags} VERBOSE=1

%install
rm -rf $RPM_BUILD_ROOT
pushd build
make install DESTDIR=$RPM_BUILD_ROOT INSTALL="install -p"
popd

%find_lang %{name}

%clean
rm -rf $RPM_BUILD_ROOT

%files -f %{name}.lang
%defattr(-,root,root,-)
%doc
%{_bindir}/*


%changelog
