Name:		libgxim
Version:	0.5.0
Release:	1%{?dist}
Summary:	GObject-based XIM protocol library

Group:		System Environment/Libraries
License:	LGPLv2+
URL:		http://code.google.com/p/libgxim/
Source:     http://libgxim.googlecode.com/files/%{name}-%{version}.tar.bz2
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:	intltool gettext ruby xml-parser
BuildRequires:	dbus > 0.23, dbus-glib >= 0.74, glib >= 2.16, gtk2

%description
libgxim is a X Input Method protocol library that is implemented by GObject.
this library helps you to implement XIM servers or client applications to
communicate through XIM protocol without using Xlib API directly, particularly
if your application uses GObject-based main loop.

This package contains the shared library.

%prep
%setup -q

%build
%configure --disable-static --disable-rebuilds
make %{?_smp_mflags}

%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT INSTALL="install -p"

# clean up the unnecessary files
rm -f $RPM_BUILD_ROOT%{_libdir}/*.la

%files
%defattr(-, root, root, -)
%doc AUTHORS COPYING ChangeLog README
%{_libdir}/libgxim.so.*
%{_libdir}/libgxim.so
%{_libdir}/pkgconfig/*.pc
%{_includedir}/libgxim
%{_datadir}/gtk-doc/html/libgxim
%{_datadir}/locale/*/LC_MESSAGES/libgxim.mo

%clean
rm -rf $RPM_BUILD_ROOT

%changelog
