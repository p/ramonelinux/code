%define _xinputconf %{_sysconfdir}/X11/xinit/xinput.d/fcitx.conf
%{!?gtk2_binary_version: %define gtk2_binary_version %(pkg-config  --variable=gtk_binary_version gtk+-2.0)}
%{!?gtk3_binary_version: %define gtk3_binary_version %(pkg-config  --variable=gtk_binary_version gtk+-3.0)}

Name:			fcitx
Summary:		Free Chinese Input Toy for X (XIM)
Version:		4.2.8
Release:		2%{?dist}
License:		GPLv2+
Group:			User Interface/Desktops
URL:			http://code.google.com/p/%{name}/
Source0:		http://%{name}.googlecode.com/files/%{name}-%{version}_dict.tar.xz
Source1:		xinput-%{name}
BuildRequires:		pango, dbus, opencc
BuildRequires:		wget, intltool
#chrpath, sysconftool
BuildRequires:      opencc icu
BuildRequires:		cmake, libtool
BuildRequires:		qt, gtk+, gtk2
BuildRequires:		xproto, xtrans
BuildRequires:		gobject-introspection, libxkbfile
BuildRequires:		enchant, iso-codes
BuildRequires:		libx11, dbus-glib libxkbfile gettext
#, dbus-x11
BuildRequires:		desktop-file-utils, libxml
BuildRequires:		lua
Requires:		imsettings

%description
FCITX(Free Chinese Input Toy of X) is a collection of Simplified Chinese 
input methods for Linux. It supports Pinyin, QuWei and Table-based input 
methods. It's small and fast.

%prep
%setup -q

%build
mkdir -p build
pushd build
cmake -DCMAKE_INSTALL_PREFIX=/usr \
      -DSYSCONF_INSTALL_DIR=/etc \
      -DCMAKE_BUILD_TYPE=Release \
%ifarch x86_64
      -DLIB_SUFFIX=64 \
%endif \
 .. -DENABLE_GTK3_IM_MODULE=On -DENABLE_QT_IM_MODULE=On -DENABLE_OPENCC=On -DENABLE_LUA=On -DENABLE_GIR=On -DENABLE_XDGAUTOSTART=Off
make VERBOSE=1 %{?_smp_mflags}

%install
pushd build
make install INSTALL="install -p" DESTDIR=$RPM_BUILD_ROOT
rm -f %{buildroot}%{_libdir}/*.la
popd

install -pm 644 -D %{SOURCE1} %{buildroot}%{_xinputconf}

# patch fcitx4-config to use pkg-config to solve libdir to avoid multiarch
# confilict
sed -i -e 's:%{_libdir}:`pkg-config --variable=libdir fcitx`:g' \
  ${RPM_BUILD_ROOT}%{_bindir}/fcitx4-config

%find_lang %{name}

desktop-file-install --delete-original \
  --dir ${RPM_BUILD_ROOT}%{_datadir}/applications \
  ${RPM_BUILD_ROOT}%{_datadir}/applications/%{name}-skin-installer.desktop

desktop-file-install --delete-original \
  --dir ${RPM_BUILD_ROOT}%{_datadir}/applications \
  ${RPM_BUILD_ROOT}%{_datadir}/applications/%{name}-configtool.desktop

desktop-file-install --delete-original \
  --dir ${RPM_BUILD_ROOT}%{_datadir}/applications \
  ${RPM_BUILD_ROOT}%{_datadir}/applications/%{name}.desktop

%clean
rm -rf %{buildroot}

%post 
update-desktop-database %{_datadir}/applications &>/dev/null || :
/bin/touch --no-create %{_datadir}/icons/hicolor &>/dev/null || :
%{_bindir}/update-gtk-immodules %{_host} || :
%{_bindir}/gtk-query-immodules-3.0-%{__isa_bits} --update-cache || :

%postun  
update-desktop-database %{_datadir}/applications &>/dev/null || :
if [ $1 -eq 0 ] ; then
    /bin/touch --no-create %{_datadir}/icons/hicolor &>/dev/null
    /usr/bin/gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
fi
%{_bindir}/update-gtk-immodules %{_host} || :
%{_bindir}/gtk-query-immodules-3.0-%{__isa_bits} --update-cache || :

%files -f %{name}.lang
%defattr(-,root,root,-)
%doc AUTHORS ChangeLog THANKS TODO COPYING
%config %{_xinputconf}
%{_bindir}/fcitx-*
%{_bindir}/fcitx
%{_bindir}/createPYMB
%{_bindir}/mb2org
%{_bindir}/mb2txt
%{_bindir}/readPYBase
%{_bindir}/readPYMB
%{_bindir}/scel2org
%{_bindir}/txt2mb
%{_datadir}/applications/%{name}-skin-installer.desktop
%{_datadir}/%{name}/dbus/daemon.conf
%{_datadir}/applications/%{name}-configtool.desktop
%{_datadir}/applications/%{name}.desktop
%{_datadir}/mime/packages/x-fskin.xml
%{_docdir}/%{name}/
%{_mandir}/man1/createPYMB.1.gz
%{_mandir}/man1/fcitx-remote.1.gz
%{_mandir}/man1/fcitx.1.gz
%{_mandir}/man1/mb2org.1.gz
%{_mandir}/man1/mb2txt.1.gz
%{_mandir}/man1/readPYBase.1.gz
%{_mandir}/man1/readPYMB.1.gz
%{_mandir}/man1/scel2org.1.gz
%{_mandir}/man1/txt2mb.1.gz

%{_libdir}/libfcitx*.so.*
%dir %{_libdir}/%{name}/
%{_libdir}/%{name}/%{name}-[!pqt]*.so
%{_libdir}/%{name}/%{name}-punc.so
%{_libdir}/%{name}/%{name}-quickphrase.so
%{_libdir}/%{name}/qt/
%{_libdir}/%{name}/libexec/
%dir %{_libdir}/girepository-1.0/
%{_libdir}/girepository-1.0/Fcitx-1.0.typelib

%{_datadir}/icons/hicolor/16x16/apps/%{name}.png
%{_datadir}/icons/hicolor/22x22/apps/%{name}.png
%{_datadir}/icons/hicolor/24x24/apps/%{name}.png
%{_datadir}/icons/hicolor/32x32/apps/%{name}.png
%{_datadir}/icons/hicolor/48x48/apps/%{name}.png
%{_datadir}/icons/hicolor/128x128/apps/%{name}.png
%{_datadir}/icons/hicolor/16x16/status/*.png
%{_datadir}/icons/hicolor/22x22/status/*.png
%{_datadir}/icons/hicolor/24x24/status/*.png
%{_datadir}/icons/hicolor/48x48/status/*.png
%{_datadir}/icons/hicolor/scalable/status/*.svg
%{_datadir}/icons/hicolor/scalable/apps/*.svg
# co-own directories also owned by gnome-icon-theme
%dir %{_datadir}/icons/gnome
%dir %{_datadir}/icons/gnome/scalable
%dir %{_datadir}/icons/gnome/scalable/status
%dir %{_datadir}/icons/gnome/scalable/apps
%{_datadir}/icons/gnome/scalable/status/*.svg
%{_datadir}/icons/gnome/scalable/apps/*.svg
%dir %{_datadir}/%{name}
%{_datadir}/%{name}/skin/
%dir %{_datadir}/%{name}/addon
%{_datadir}/%{name}/addon/%{name}-[!pqt]*.conf
%{_datadir}/%{name}/addon/%{name}-punc.conf
%{_datadir}/%{name}/addon/%{name}-quickphrase.conf
%{_datadir}/%{name}/data/
%{_datadir}/%{name}/spell/
%dir %{_datadir}/%{name}/configdesc
%{_datadir}/%{name}/configdesc/[!ft]*.desc
%{_datadir}/%{name}/configdesc/fcitx-[!p]*.desc

%{_bindir}/fcitx4-config
%{_libdir}/libfcitx*.so
%{_libdir}/pkgconfig/fcitx*.pc
%{_includedir}/fcitx*
%{_datadir}/cmake/%{name}/
%dir %{_datadir}/gir-1.0
%{_datadir}/gir-1.0/Fcitx-1.0.gir

%{_datadir}/%{name}/table/
%{_datadir}/%{name}/imicon/[!ps]*.png

%{_datadir}/%{name}/inputmethod/pinyin.conf
%{_datadir}/%{name}/inputmethod/shuangpin.conf
%{_datadir}/%{name}/pinyin/
%{_datadir}/%{name}/configdesc/fcitx-pinyin.desc
%{_datadir}/%{name}/configdesc/fcitx-pinyin-enhance.desc
%{_datadir}/%{name}/addon/fcitx-pinyin.conf
%{_datadir}/%{name}/addon/fcitx-pinyin-enhance.conf
%{_datadir}/%{name}/imicon/pinyin.png
%{_datadir}/%{name}/imicon/shuangpin.png
%{_libdir}/%{name}/%{name}-pinyin.so
%{_libdir}/%{name}/%{name}-pinyin-enhance.so
%{_datadir}/%{name}/py-enhance/

%{_datadir}/%{name}/inputmethod/qw.conf
%{_libdir}/%{name}/%{name}-qw.so
%{_datadir}/%{name}/addon/fcitx-qw.conf

%{_datadir}/%{name}/configdesc/table.desc
%{_libdir}/%{name}/%{name}-table.so
%{_datadir}/%{name}/addon/fcitx-table.conf

%{_libdir}/gtk-2.0/%{gtk2_binary_version}/immodules/im-fcitx.so

%{_libdir}/gtk-3.0/%{gtk3_binary_version}/immodules/im-fcitx.so

%{_libdir}/qt4/plugins/inputmethods/qtim-fcitx.so

%changelog
