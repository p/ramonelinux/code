Name:		im-chooser
Version:	1.6.0
Release:	3%{?dist}
Summary:	Desktop Input Method configuration tool

Group:		Applications/System
License:	GPLv2+
URL:		http://fedorahosted.org/im-chooser/
Source0:	http://fedorahosted.org/releases/i/m/%{name}/%{name}-%{version}.tar.bz2
Buildroot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:	gtk+ libsm imsettings >= 1.3.0
BuildRequires:	desktop-file-utils intltool gettext xml-parser

%description
im-chooser is a GUI configuration tool to choose the Input Method
to be used or disable Input Method usage on the desktop.

%prep
%setup -q

%build
%configure
make %{?_smp_mflags}

%install
rm -rf $RPM_BUILD_ROOT

make install DESTDIR=$RPM_BUILD_ROOT

desktop-file-install	--vendor=fedora				\
	--add-category=X-GNOME-PersonalSettings			\
	--delete-original					\
	--dir=$RPM_BUILD_ROOT%{_datadir}/applications		\
	$RPM_BUILD_ROOT%{_datadir}/applications/im-chooser.desktop

rm -rf $RPM_BUILD_ROOT%{_libdir}/libimchooseui.{so,la,a}

# disable panel so far
rm -rf $RPM_BUILD_ROOT%{_libdir}/control-center-1/panels/libim-chooser.so
rm -rf $RPM_BUILD_ROOT%{_datadir}/applications/im-chooser-panel.desktop

%find_lang %{name}

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr (-, root, root)
%{_bindir}/im-chooser
%{_datadir}/applications/fedora-im-chooser.desktop
%doc AUTHORS COPYING ChangeLog README
%{_libdir}/libimchooseui.so.*
%{_datadir}/icons/hicolor/*/apps/im-chooser.png
%dir %{_datadir}/imchooseui
%{_datadir}/imchooseui/imchoose.ui

%if 0
%doc AUTHORS COPYING ChangeLog README
%{_libdir}/control-center-1/panels/libim-chooser.so
%{_datadir}/applications/im-chooser-panel.desktop
%endif

%{_datadir}/locale/*/LC_MESSAGES/im-chooser.mo

%changelog
