Name:       ca-certificates
Version:    2.4.0
Release:    6%{?dist}
Summary:    ca-certificates

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://ftp.gnome.org
Source1:    certdata.txt

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildArch:      noarch
BuildRequires:  openssl ca-tools

%description

%prep

%build
mkdir ca-certificates
cd ca-certificates

%check

%install
cd ca-certificates
cp -v %SOURCE1 .
make-ca.sh &&
remove-expired-certs.sh certs

SSLDIR=%{buildroot}/etc/ssl &&
install -d ${SSLDIR}/certs &&
cp -v certs/*.pem ${SSLDIR}/certs &&
c_rehash &&
install BLFS-ca-bundle*.crt ${SSLDIR}/ca-bundle.crt &&
unset SSLDIR

%files
%defattr(-,root,root,-)
/etc/ssl/ca-bundle.crt
/etc/ssl/certs/*.pem

%clean
rm -rf %{buildroot}

%changelog
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
