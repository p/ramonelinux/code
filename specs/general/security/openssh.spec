Name:       openssh
Version:    6.6p1
Release:    1%{?dist}
Summary:    OpenSSH

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.openssh.org
Source:     http://ftp.openbsd.org/pub/OpenBSD/OpenSSH/portable/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  openssl
Requires:       coreutils shadow systemd

%description
The OpenSSH package contains ssh clients and the sshd daemon.
This is useful for encrypting authentication and subsequent traffic over a network.
The ssh and scp commands are secure implementions of telnet and rcp respectively.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr                       \
            --sysconfdir=/etc/ssh               \
            --with-md5-passwords                \
            --with-privsep-path=/var/lib/sshd   \
            --libexecdir=%{_libdir}/openssh     &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot} &&

mkdir -p %{buildroot}/usr/share/man/man1 &&
install -v -m755 contrib/ssh-copy-id %{buildroot}/usr/bin &&
install -v -m644 contrib/ssh-copy-id.1 %{buildroot}/usr/share/man/man1 &&
install -v -m755 -d %{buildroot}/usr/share/doc/openssh-%{version} &&
install -v -m644 INSTALL LICENCE OVERVIEW README* \
    %{buildroot}/usr/share/doc/openssh-%{version}

mkdir -p %{buildroot}/lib/systemd/system
cat > %{buildroot}/lib/systemd/system/sshd.service << "EOF"
[Unit]
Description=OpenSSH server daemon
After=syslog.target network.target auditd.service

[Service]
ExecStart=/usr/sbin/sshd -D -e
ExecReload=/bin/kill -HUP $MAINPID

[Install]
WantedBy=multi-user.target
EOF

cat > %{buildroot}/lib/systemd/system/sshd.socket << "EOF"
[Unit]
Description=OpenSSH Server Socket
Conflicts=sshd.service

[Socket]
ListenStream=22
Accept=yes

[Install]
WantedBy=sockets.target
EOF

cat > %{buildroot}/lib/systemd/system/sshd@.service << "EOF"
[Unit]
Description=OpenSSH per-connection server daemon
After=syslog.target auditd.service

[Service]
ExecStart=-/usr/sbin/sshd -i -e
StandardInput=socket
StandardError=syslog
EOF

%post
systemctl enable sshd
install -m700 -d /var/lib/sshd &&
chown root:sys /var/lib/sshd &&
groupadd -g 50 sshd &&
useradd -c 'sshd PrivSep' -d /var/lib/sshd -g sshd \
    -s /bin/false -u 50 sshd
if [ ! -e /dev/random ]; then
    mknod /dev/random c 1 8
fi
if [ ! -e /dev/urandom ]; then
    mknod /dev/urandom c 1 9
fi
echo y | ssh-keygen -f /etc/ssh/ssh_host_dsa_key &>/dev/null || :
echo y | ssh-keygen -f /etc/ssh/ssh_host_ecdsa_key &>/dev/null || :
echo y | ssh-keygen -f /etc/ssh/ssh_host_rsa_key &>/dev/null || :

%postun
systemctl disable sshd
userdel sshd
rm -rf /etc/ssh/{ssh_host_dsa_key,ssh_host_ecdsa_key,ssh_host_rsa_key}
rm -rf /etc/ssh/{ssh_host_dsa_key.pub,ssh_host_ecdsa_key.pub,ssh_host_rsa_key.pub}

%files
%defattr(-,root,root,-)
/etc/ssh/moduli
/etc/ssh/ssh_config
/etc/ssh/sshd_config
%{_bindir}/scp
%{_bindir}/sftp
%{_bindir}/slogin
%{_bindir}/ssh
%{_bindir}/ssh-add
%{_bindir}/ssh-agent
%{_bindir}/ssh-copy-id
%{_bindir}/ssh-keygen
%{_bindir}/ssh-keyscan
%{_libdir}/openssh/sftp-server
%{_libdir}/openssh/ssh-keysign
%{_libdir}/openssh/ssh-pkcs11-helper
%{_sbindir}/sshd
%{_docdir}/openssh-%{version}/*
%{_mandir}/*
/lib/systemd/system/sshd.service
/lib/systemd/system/sshd.socket
/lib/systemd/system/sshd@.service

%clean
rm -rf %{buildroot}

%changelog
* Tue Mar 18 2014 tanggeliang <tanggeliang@gmail.com>
- update from 6.4p1 to 6.6p1
* Thu Dec 5 2013 tanggeliang <tanggeliang@gmail.com>
- update from 6.3p1 to 6.4p1
* Thu Oct 3 2013 tanggeliang <tanggeliang@gmail.com>
- update from 6.2p2 to 6.3p1
* Thu Jul 25 2013 tanggeliang <tanggeliang@gmail.com>
- update from 6.1p1 to 6.2p2
* Sun Mar 17 2013 tanggeliang <tanggeliang@gmail.com>
- update from 6.0p1 to 6.1p1
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
