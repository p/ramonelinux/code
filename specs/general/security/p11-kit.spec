Name:       p11-kit
Version:    0.20.3
Release:    1%{?dist}
Summary:    p11-kit

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.freedesktop.org
Source:     http://p11-glue.freedesktop.org/releases/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  ca-certificates libtasn1 gtk-doc libxslt

%description
The p11-kit package Provides a way to load and enumerate PKCS #11 (a Cryptographic Token Interface Standard) modules. 

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --sysconfdir=/etc \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
/etc/pkcs11/pkcs11.conf.example
%{_bindir}/p11-kit
%{_bindir}/trust
%{_includedir}/p11-kit-1/p11-kit/*.h
%{_libdir}/pkcs11/p11-kit-trust.la
%{_libdir}/pkcs11/p11-kit-trust.so
%{_libdir}/libp11-kit.*
%{_libdir}/p11-kit-proxy.so
%{_libdir}/p11-kit/trust-extract-compat
%{_libdir}/pkgconfig/p11-kit-1.pc
%{_datadir}/p11-kit/modules/p11-kit-trust.module

%files doc
%defattr(-,root,root,-)
%{_datadir}/gtk-doc/html/p11-kit/*

%clean
rm -rf %{buildroot}

%changelog
* Sun Jul 20 2014 tanggeliang <tanggeliang@gmail.com>
- update from 0.20.2 to 0.20.3
* Tue Mar 18 2014 tanggeliang <tanggeliang@gmail.com>
- update from 0.20.1 to 0.20.2
* Tue Sep 24 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.18.5 to 0.20.1
* Mon Jul 29 2013 tanggeliang <tanggeliang@gmail.com>
- update 0.18.4 to 0.18.5
* Thu Jul 25 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.14 to 0.18.4
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
