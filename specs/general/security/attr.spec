Name:       attr
Version:    2.4.47
Release:    1%{?dist}
Summary:    attr

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://download.savannah.gnu.org/releases/attr
Source:     http://download.savannah.gnu.org/releases/attr/%{name}-%{version}.src.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  libtool autoconf automake m4 gettext

%description
The attr package contains utilities to administer the extended attributes on filesystem objects.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
sed -i -e 's|/@pkg_name@|&-@pkg_version@|' include/builddefs.in &&

INSTALL_USER=root \
INSTALL_GROUP=root \
./configure --prefix=/usr --disable-static \
             --libdir=/%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make install install-dev install-lib DESTDIR=%{buildroot}

chmod -v 755 %{buildroot}/%{_libdir}/libattr.so.1.1.0 &&
mkdir -p %{buildroot}/%{_lib}
mv -v %{buildroot}/%{_libdir}/libattr.so.* %{buildroot}/%{_lib} &&
ln -sfv ../../%{_lib}/libattr.so.1 %{buildroot}/%{_libdir}/libattr.so

%files
%defattr(-,root,root,-)
%{_includedir}/attr/*.h
%{_bindir}/attr
%{_bindir}/getfattr
%{_bindir}/setfattr
/%{_lib}/libattr.so.1*
%{_libdir}/libattr.la
%{_libdir}/libattr.so*
%{_datadir}/locale/*/LC_MESSAGES/attr.mo

%files doc
%defattr(-,root,root,-)
%{_docdir}/attr-2.4.47/*
%{_mandir}/man*/*attr*.gz

%clean
rm -rf %{buildroot}

%changelog
* Sat Jul 6 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.4.46 to 2.4.47
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
