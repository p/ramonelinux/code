Name:       openssl
Version:    1.0.1h
Release:    1%{?dist}
Summary:    OpenSSL

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.openssl.org
Source:     http://www.openssl.org/source/%{name}-%{version}.tar.gz
Patch:      %{name}-%{version}-fix_parallel_build-1-ram.patch

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build

%description
The OpenSSL package contains management tools and libraries relating to cryptography.
These are useful for providing cryptography functions to other packages, such as OpenSSH, email applications and web browsers (for accessing HTTPS sites). 

%package        perl
Summary:        Perl scripts provided with OpenSSL
%description    perl

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}
%patch -p1

%build
./config --prefix=/usr         \
         --openssldir=/etc/ssl \
         --libdir=%{_lib}      \
         shared                \
         zlib-dynamic          &&
make -j1

%check

%install
rm -rf %{buildroot}
make INSTALL_PREFIX=%{buildroot} MANDIR=/usr/share/man install

%files
%defattr(-,root,root,-)
/etc/ssl/certs
/etc/ssl/misc/CA.sh
/etc/ssl/misc/c_hash
/etc/ssl/misc/c_info
/etc/ssl/misc/c_issuer
/etc/ssl/misc/c_name
/etc/ssl/openssl.cnf
/etc/ssl/private
%{_bindir}/openssl
%{_bindir}/c_rehash
%{_includedir}/openssl/*.h
%{_libdir}/engines/lib*.so
%{_libdir}/libcrypto.a
%{_libdir}/libcrypto.so*
%{_libdir}/libssl.a
%{_libdir}/libssl.so*
%{_libdir}/pkgconfig/libcrypto.pc
%{_libdir}/pkgconfig/libssl.pc
%{_libdir}/pkgconfig/openssl.pc

%files perl
%defattr(-,root,root,-)
/etc/ssl/misc/CA.pl
/etc/ssl/misc/tsget

%files doc
%defattr(-,root,root,-)
%{_mandir}/man*/*.gz

%clean
rm -rf %{buildroot}

%changelog
* Sat Jul 26 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.0.1f to 1.0.1h
* Tue Mar 18 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.0.1e to 1.0.1f
* Sun Mar 17 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.0.1c to 1.0.1e
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
