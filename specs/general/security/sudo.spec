Name:       sudo
Version:    1.8.8
Release:    1%{?dist}
Summary:    Sudo

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.sudo.ws
Source:     http://www.sudo.ws/sudo/dist/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  linux-pam krb

%description
The Sudo package allows a system administrator to give certain users (or groups of users) the ability to run some (or all) commands as root or another user while logging the commands and arguments. 

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr                       \
            --libexecdir=%{_libdir}/sudo        \
            --docdir=%{_docdir}/sudo-%{version} \
            --with-timedir=/var/lib/sudo        \
            --with-all-insults                  \
            --with-env-editor                   &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

mkdir -v %{buildroot}/etc/pam.d
cat > %{buildroot}/etc/pam.d/sudo << "EOF" &&
# Begin /etc/pam.d/sudo

# include the default auth settings
auth      include     system-auth

# include the default account settings
account   include     system-account

# Set default environment variables for the service user
session   required    pam_env.so

# include system session defaults
session   include     system-session

# End /etc/pam.d/sudo
EOF
chmod 644 %{buildroot}/etc/pam.d/sudo

%files
%defattr(-,root,root,-)
/etc/pam.d/sudo
/etc/sudoers
%attr(4111,root,root) %{_bindir}/sudo
%attr(4111,root,root) %{_bindir}/sudoedit
%attr(0111,root,root) %{_bindir}/sudoreplay
%{_includedir}/sudo_plugin.h
%{_libdir}/sudo/group_file.so
%{_libdir}/sudo/sudo_noexec.so
%{_libdir}/sudo/sudoers.so
%{_libdir}/sudo/system_group.so
%attr(0755,root,root) %{_sbindir}/visudo
%{_datadir}/locale/*

%files doc
%defattr(-,root,root,-)
%{_docdir}/sudo-%{version}/*
%{_mandir}/*

%clean
rm -rf %{buildroot}

%post
chmod 0440 /etc/sudoers || :

%changelog
* Mon Oct 7 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.8.7 to 1.8.8
* Sun Aug 18 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.8.6 to 1.8.7
* Mon Apr 1 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.8.5 to 1.8.6
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
