Name:       gnupg
Version:    2.0.22
Release:    1%{?dist}
Summary:    GnuPG 2

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.gnupg.org
Source:     ftp://ftp.gnupg.org/gcrypt/gnupg/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  pth libassuan libgcrypt libksba
BuildRequires:  openldap libusb-compat curl
Requires:       pinentry

%description
The GnuPG 2 package is GNU's tool for secure communication and data storage.
It can be used to encrypt data and to create digital signatures.
It includes an advanced key management facility and is compliant with the proposed OpenPGP Internet standard as described in RFC2440 and the S/MIME standard as described by several RFCs.
GnuPG 2 is the stable version of GnuPG integrating support for OpenPGP and S/MIME.
It does not conflict with an installed GnuPG-1.4.12 OpenPGP-only version.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --libexecdir=%{_libdir}/gnupg2 \
            --docdir=/usr/share/doc/gnupg-%{version} \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

rm -rf %{buildroot}/%{_infodir}/dir

%files
%defattr(-,root,root,-)
%doc
%{_bindir}/gpg-agent
%{_bindir}/gpg-connect-agent
%{_bindir}/gpg2
%{_bindir}/gpgconf
%{_bindir}/gpgkey2ssh
%{_bindir}/gpgparsemail
%{_bindir}/gpgsm
%{_bindir}/gpgsm-gencert.sh
%{_bindir}/gpgv2
%{_bindir}/kbxutil
%{_bindir}/watchgnupg
%{_libdir}/gnupg2/gnupg-pcsc-wrapper
%{_libdir}/gnupg2/gpg-check-pattern
%{_libdir}/gnupg2/gpg-preset-passphrase
%{_libdir}/gnupg2/gpg-protect-tool
%{_libdir}/gnupg2/gpg2keys_curl
%{_libdir}/gnupg2/gpg2keys_finger
%{_libdir}/gnupg2/gpg2keys_hkp
%{_libdir}/gnupg2/gpg2keys_ldap
%{_libdir}/gnupg2/scdaemon
%{_sbindir}/addgnupghome
%{_sbindir}/applygnupgdefaults
%{_datadir}/gnupg/com-certs.pem
%{_datadir}/gnupg/gpg-conf.skel
%{_datadir}/gnupg/help.*.txt
%{_datadir}/gnupg/help.txt
%{_datadir}/gnupg/qualified.txt
%{_datadir}/locale/*/LC_MESSAGES/gnupg2.mo
%{_docdir}/gnupg-%{version}/*
%{_infodir}/gnupg.info*.gz
%{_mandir}/man*/*.gz

%clean
rm -rf %{buildroot}

%changelog
* Tue Oct 22 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.0.21 to 2.0.22
* Sat Aug 24 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.0.20 to 2.0.21
* Mon Jun 10 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.0.19 to 2.0.20
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
