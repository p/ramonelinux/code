Name:       cyrus-sasl
Version:    2.1.26
Release:    4%{?dist}
Summary:    Cyrus SASL

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.cyrus-mail.org
Source:     http://ftp.andrew.cmu.edu/pub/cyrus-mail/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  openssl db
BuildRequires:  groff libtool
BuildRequires:  autoconf automake m4

%description
The Cyrus SASL package contains a Simple Authentication and Security Layer, a method for adding authentication support to connection-based protocols.
To use SASL, a protocol includes a command for identifying and authenticating a user to a server and for optionally negotiating protection of subsequent protocol interactions.
If its use is negotiated, a security layer is inserted between the protocol and the connection.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr        \
            --sysconfdir=/etc    \
            --enable-auth-sasldb \
            --with-dbpath=/var/lib/sasl/sasldb2 \
            --with-saslauthd=/var/run/saslauthd \
            --libdir=%{_libdir} \
            CFLAGS=-fPIC &&
make -j1 sasldir=%{_libdir}/sasl2

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install sasldir=%{_libdir}/sasl2 &&

install -v -m755 -d %{buildroot}/usr/share/doc/cyrus-sasl-%{version} &&
install -v -m644 doc/{*.{html,txt,fig},ONEWS,TODO} \
    saslauthd/LDAP_SASLAUTHD %{buildroot}/usr/share/doc/cyrus-sasl-%{version} &&
install -v -m700 -d %{buildroot}/var/lib/sasl

%files
%defattr(-,root,root,-)
%{_includedir}/sasl/*.h
%{_libdir}/libsasl2.*
%{_libdir}/pkgconfig/libsasl2.pc
%{_libdir}/sasl2/libanonymous.*
%{_libdir}/sasl2/libcrammd5.*
%{_libdir}/sasl2/libdigestmd5.*
%{_libdir}/sasl2/libotp.*
%{_libdir}/sasl2/libplain.*
%{_libdir}/sasl2/libsasldb.*
%{_libdir}/sasl2/libscram.*
%{_sbindir}/pluginviewer
%{_sbindir}/saslauthd
%{_sbindir}/sasldblistusers2
%{_sbindir}/saslpasswd2
%{_sbindir}/testsaslauthd
%dir /var/lib/sasl

%files doc
%defattr(-,root,root,-)
%{_docdir}/cyrus-sasl-%{version}/*
%{_mandir}/man3/sasl*.3.gz
%{_mandir}/man8/*.8.gz

%clean
rm -rf %{buildroot}

%changelog
* Tue Aug 6 2013 tanggeliang <tanggeliang@gmail.com>
- remove fixes-1.patch and autoreconf
- add "#include <sys/types.h>" to fix kdepimlibs build error " error: 'size_t' was not declared in this scope"
* Mon Jun 10 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.1.25 to 2.1.26
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
