Name:       nss
Version:    3.20
Release:    1%{?dist}
Summary:    NSS Network Security Services

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://ftp.mozilla.org
Source:     http://ftp.mozilla.org/pub/mozilla.org/security/nss/releases/NSS_3_15_5_RTM/src/%{name}-%{version}.tar.gz
Patch:      %{name}-%{version}-standalone-1.patch

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  nspr sqlite

%description
The Network Security Services (NSS) package is a set of libraries designed to support cross-platform development of security-enabled client and server applications.
Applications built with NSS can support SSL v2 and v3, TLS, PKCS #5, PKCS #7, PKCS #11, PKCS #12, S/MIME, X.509 v3 certificates, and other security standards.
This is useful for implementing SSL and S/MIME or other Internet security standards into an application.

%prep
%setup -q -n %{name}-%{version}
%patch -p1

%build
cd nss &&
make BUILD_OPT=1                                \
  NSPR_INCLUDE_DIR=/usr/include/nspr            \
  USE_SYSTEM_ZLIB=1                             \
  ZLIB_LIBS=-lz                                 \
  $([ $(uname -m) = x86_64 ] && echo USE_64=1)  \
  $([ -f /usr/include/sqlite3.h ] && echo NSS_USE_SYSTEM_SQLITE=1)

%check

%install
rm -rf %{buildroot}
mkdir -pv %{buildroot}/usr/{bin,include,%{_lib}/pkgconfig}

cd dist &&
install -v -m755 Linux*/lib/*.so %{buildroot}/usr/%{_lib} &&
install -v -m644 Linux*/lib/{*.chk,libcrmf.a} %{buildroot}/usr/%{_lib} &&
install -v -m755 -d %{buildroot}/usr/include/nss &&
cp -v -RL {public,private}/nss/* %{buildroot}/usr/include/nss &&
chmod 644 %{buildroot}/usr/include/nss/* &&
install -v -m755 Linux*/bin/{certutil,nss-config,pk12util} %{buildroot}/usr/bin &&
install -v -m644 Linux*/lib/pkgconfig/nss.pc %{buildroot}/usr/%{_lib}/pkgconfig

%files
%defattr(-,root,root,-)
%{_bindir}/nss-config
%{_bindir}/pk12util
%{_bindir}/certutil
%{_includedir}/nss/*
%{_libdir}/libcrmf.a
%{_libdir}/libfreebl3.*
%{_libdir}/libnss3.so
%{_libdir}/libnssckbi.so
%{_libdir}/libnssdbm3.*
%{_libdir}/libnsssysinit.so
%{_libdir}/libnssutil3.so
%{_libdir}/libsmime3.so
%{_libdir}/libsoftokn3.*
%{_libdir}/libssl3.so
%{_libdir}/pkgconfig/nss.pc

%clean
rm -rf %{buildroot}

%changelog
* Sat Sep 26 2015 tanggeliang <tanggeliang@gmail.com>
- update from 3.16.3 to 3.20
* Sun Jul 20 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.15.5 to 3.16.3
* Tue Mar 18 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.15.3.1 to 3.15.5
* Sun Dec 15 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.15.3 to 3.15.3.1
* Tue Dec 3 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.15.2 to 3.15.3
* Tue Oct 22 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.15.1 to 3.15.2
* Mon Aug 19 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.14.3 to 3.15.1
* Fri Apr 5 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.14.1 to 3.14.3
* Sun Feb 24 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.14 to 3.14.1
* Sat Nov 3 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.13.6 to 3.14
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
