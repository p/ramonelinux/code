Name:       gnutls
Version:    3.3.6
Release:    1%{?dist}
Summary:    GnuTLS

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://mirrors.kernel.org/gnu/gnutls
Source:     http://mirrors.kernel.org/gnu/gnutls/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  nettle ca-certificates libtasn1
BuildRequires:  gtk-doc guile libidn p11-kit glib
BuildRequires:  unbound

%description
The GnuTLS package contains libraries and userspace tools which provide a secure layer over a reliable transport layer. Currently the GnuTLS library implements the proposed standards by the IETF's TLS working group. Quoting from the TLS protocol specification:

"The TLS protocol provides communications privacy over the Internet. The protocol allows client/server applications to communicate in a way that is designed to prevent eavesdropping, tampering, or message forgery."

GnuTLS provides support for TLS 1.1, TLS 1.0 and SSL 3.0 protocols, TLS extensions, including server name and max record size. Additionally, the library supports authentication using the SRP protocol, X.509 certificates and OpenPGP keys, along with support for the TLS Pre-Shared-Keys (PSK) extension, the Inner Application (TLS/IA) extension and X.509 and OpenPGP certificate handling. 

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --with-default-trust-store-file=/etc/ssl/ca-bundle.crt \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}
make -C doc/reference install-data-local DESTDIR=%{buildroot}

rm -rf %{buildroot}/%{_infodir}/dir

%files
%defattr(-,root,root,-)
%{_bindir}/certtool
%{_bindir}/crywrap
%{_bindir}/danetool
%{_bindir}/gnutls-cli
%{_bindir}/gnutls-cli-debug
%{_bindir}/gnutls-serv
%{_bindir}/ocsptool
%{_bindir}/p11tool
%{_bindir}/psktool
%{_bindir}/srptool
%{_includedir}/gnutls/*.h
%{_libdir}/libgnutls*.*
%{_libdir}/guile/2.0/guile-gnutls-v-2.*
%{_libdir}/pkgconfig/gnutls*.pc
%{_datadir}/guile/site/gnutls.scm
%{_datadir}/guile/site/gnutls/extra.scm
%{_datadir}/locale/*

%files doc
%defattr(-,root,root,-)
%{_datadir}/gtk-doc/html/gnutls/*
%{_infodir}/gnutls*.gz
%{_infodir}/pkcs11-vision.png.gz
%{_mandir}/man1/*.1.gz
%{_mandir}/man3/*.3.gz

%clean
rm -rf %{buildroot}

%changelog
* Sat Jul 26 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.3.5 to 3.3.6
* Sun Jul 20 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.2.12.1 to 3.3.5
* Tue Mar 18 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.2.7 to 3.2.12.1
* Wed Dec 4 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.2.4 to 3.2.7
* Mon Oct 7 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.2.3 to 3.2.4
* Wed Aug 21 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.2.1 to 3.2.3
* Sat Jul 6 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.0.25 to 3.2.1
* Sat Nov 3 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.0.24 to 3.0.25
* Sun Oct 14 2012 tanggeliang <tanggeliang@gmail.com>
- update from 3.0.22 to 3.0.24
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
