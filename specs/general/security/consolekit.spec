Name:       consolekit
Version:    0.4.6
Release:    12%{?dist}
Summary:    ConsoleKit

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.freedesktop.org
Source:     http://www.freedesktop.org/software/ConsoleKit/dist/ConsoleKit-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  acl dbus-glib libx11
BuildRequires:  linux-pam polkit systemd systemd-libudev
BuildRequires:  automake autoconf libtool libxslt

%description
The ConsoleKit package is a framework for keeping track of the various users, sessions, and seats present on a system.
It provides a mechanism for software to react to changes of any of these items or of any of the metadata associated with them.

%prep
%setup -q -n ConsoleKit-%{version}

%build
./configure --prefix=/usr \
            --sysconfdir=/etc \
            --localstatedir=/var \
            --libexecdir=%{_libdir}/ConsoleKit \
            --enable-udev-acl \
            --enable-pam-module \
            --with-systemdsystemunitdir=/lib/systemd/system \
            --with-pam-module-dir=/%{_lib}/security \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

mkdir -pv %{buildroot}/etc/pam.d
cat >> %{buildroot}/etc/pam.d/system-session << "EOF"
# Begin ConsoleKit addition

session     optional    pam_loginuid.so
session     optional    pam_ck_connector.so nox11
session     optional    pam_systemd.so

# End ConsoleKit addition
EOF

cat > %{buildroot}/usr/lib/ConsoleKit/run-session.d/pam-foreground-compat.ck << "EOF"
#!/bin/sh
TAGDIR=/var/run/console

[ -n "$CK_SESSION_USER_UID" ] || exit 1
[ "$CK_SESSION_IS_LOCAL" = "true" ] || exit 0

TAGFILE="$TAGDIR/`getent passwd $CK_SESSION_USER_UID | cut -f 1 -d:`"

if [ "$1" = "session_added" ]; then
    mkdir -p "$TAGDIR"
    echo "$CK_SESSION_ID" >> "$TAGFILE"
fi

if [ "$1" = "session_removed" ] && [ -e "$TAGFILE" ]; then
    sed -i "\%^$CK_SESSION_ID\$%d" "$TAGFILE"
    [ -s "$TAGFILE" ] || rm -f "$TAGFILE"
fi
EOF
chmod -v 755 %{buildroot}/usr/lib/ConsoleKit/run-session.d/pam-foreground-compat.ck

%files
%defattr(-,root,root,-)
/%{_sysconfdir}/ConsoleKit/*
/%{_sysconfdir}/dbus-1/*
/%{_sysconfdir}/pam.d/system-session
/%{_lib}/security/pam_ck_connector.*
/lib/udev/rules.d/70-udev-acl.rules
/lib/udev/udev-acl
%{_bindir}/ck-*
%{_includedir}/ConsoleKit/ck-connector/ck-connector.h
%{_libdir}/ConsoleKit/ck-collect-session-info
%{_libdir}/ConsoleKit/ck-get-x11-display-device
%{_libdir}/ConsoleKit/ck-get-x11-server-pid
/usr/lib/ConsoleKit/run-seat.d/udev-acl.ck
/usr/lib/ConsoleKit/run-session.d/pam-foreground-compat.ck
/usr/lib/ConsoleKit/scripts/ck-system-restart
/usr/lib/ConsoleKit/scripts/ck-system-stop
%{_libdir}/libck-connector.*
%{_libdir}/pkgconfig/ck-connector.pc
%{_sbindir}/ck-log-system-*
%{_sbindir}/console-kit-daemon
%{_datadir}/dbus-1/*
%{_datadir}/polkit-1/*
%attr(755,root,root) %dir %{_var}/log/ConsoleKit
%dir %{_var}/run/ConsoleKit
%{_mandir}/*/*
/lib/systemd/system/basic.target.wants/console-kit-log-system-start.service
/lib/systemd/system/console-kit-daemon.service
/lib/systemd/system/console-kit-log-system-restart.service
/lib/systemd/system/console-kit-log-system-start.service
/lib/systemd/system/console-kit-log-system-stop.service
/lib/systemd/system/halt.target.wants/console-kit-log-system-stop.service
/lib/systemd/system/kexec.target.wants/console-kit-log-system-restart.service
/lib/systemd/system/poweroff.target.wants/console-kit-log-system-stop.service
/lib/systemd/system/reboot.target.wants/console-kit-log-system-restart.service

%clean
rm -rf %{buildroot}

%changelog
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
