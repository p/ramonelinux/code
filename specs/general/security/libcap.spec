Name:       libcap
Version:    2.24
Release:    1%{?dist}
Summary:    libcap

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://ftp.kernel.org/pub/linux/libs/security/linux-privs/libcap2
Source:     http://www.kernel.org/pub/linux/libs/security/linux-privs/libcap2/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  attr

%description
The libcap package implements the user-space interfaces to the POSIX 1003.1e capabilities available in Linux kernels.
These capabilities are a partitioning of the all powerful root privilege into a set of distinct privileges. 

%prep
%setup -q -n %{name}-%{version}

%build
sed -i 's:LIBDIR:PAM_&:g' pam_cap/Makefile &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make prefix=%{buildroot}/usr \
     SBINDIR=%{buildroot}/sbin \
     PAM_LIBDIR=%{buildroot}/%{_lib} \
     LIBDIR=%{buildroot}/%{_libdir} \
     PKGCONFIGDIR=%{buildroot}/%{_libdir}/pkgconfig \
     RAISE_SETFCAP=no install

#mkdir -p %{buildroot}/%{_lib}
chmod -v 755 %{buildroot}/%{_libdir}/libcap.so &&
mv -v %{buildroot}/%{_libdir}/libcap.so.* %{buildroot}/%{_lib} &&
ln -sfv ../../%{_lib}/libcap.so.2 %{buildroot}/%{_libdir}/libcap.so

%files
%defattr(-,root,root,-)
/%{_lib}/libcap.so*
/%{_lib}/security/pam_cap.so
/sbin/capsh
/sbin/getcap
/sbin/getpcaps
/sbin/setcap
%{_includedir}/sys/capability.h
%{_libdir}/libcap.a
%{_libdir}/libcap.so
%{_libdir}/pkgconfig/libcap.pc
%{_mandir}/man*/*.gz

%clean
rm -rf %{buildroot}

%changelog
* Tue Mar 18 2014 tanggeliang <tanggeliang@gmail.com>
- update from 2.23 to 2.24
* Sun Jan 5 2014 tanggeliang <tanggeliang@gmail.com>
- update from 2.22 to 2.23
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
