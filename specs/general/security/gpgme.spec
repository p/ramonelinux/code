Name:       gpgme
Version:    1.4.3
Release:    1%{?dist}
Summary:    GPGME

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.gnupg.org
Source:     ftp://ftp.gnupg.org/gcrypt/gpgme/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  libassuan
BuildRequires:  gnupg pth

%description
The GPGME package is a C language library that allows to add support for cryptography to a program.
It is designed to make access to public key crypto engines like GnuPG or GpgSM easier for applications.
GPGME provides a high-level crypto API for encryption, decryption, signing, signature verification and key management.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --disable-fd-passing \
            --libdir=%{_libdir}     &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

rm -rf %{buildroot}/%{_infodir}/dir

%files
%defattr(-,root,root,-)
%doc
%{_bindir}/gpgme-config
%{_includedir}/gpgme.h
%{_libdir}/libgpgme-pthread.la
%{_libdir}/libgpgme-pthread.so*
%{_libdir}/libgpgme.la
%{_libdir}/libgpgme.so*
%{_datadir}/aclocal/gpgme.m4
%{_datadir}/common-lisp/source/gpgme/gpgme-package.lisp
%{_datadir}/common-lisp/source/gpgme/gpgme.asd
%{_datadir}/common-lisp/source/gpgme/gpgme.lisp
%{_infodir}/gpgme.info*.gz

%clean
rm -rf %{buildroot}

%changelog
* Thu Aug 22 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.4.2 to 1.4.3
* Sat Jul 6 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.3.2 to 1.4.2
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
