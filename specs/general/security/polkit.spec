Name:       polkit
Version:    0.112
Release:    2%{?dist}
Summary:    Polkit

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://polkit.freedesktop.org
Source:     http://www.freedesktop.org/software/polkit/releases/%{name}-%{version}.tar.gz
Patch:      %{name}-%{version}-systemd-215.diff

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  glib intltool js17 gobject-introspection 
BuildRequires:  gtk-doc libxslt docbook-xsl docbook-xml
BuildRequires:  gettext xml-parser systemd
# for groupadd
Requires(post): shadow
#TODO

%description
Polkit is a toolkit for defining and handling authorizations.
It is used for allowing unprivileged processes to speak to privileged processes. 

%prep
%setup -q -n %{name}-%{version}
%patch -p1

%build
./configure --prefix=/usr \
            --sysconfdir=/etc \
            --localstatedir=/var \
            --with-authfw=shadow \
            --disable-static \
            --libexecdir=/usr/lib/polkit-1 \
            --enable-libsystemd-login=yes \
            --with-systemdsystemunitdir=/lib/systemd/system \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

pushd %{buildroot}
mkdir etc/pam.d
cat > etc/pam.d/polkit-1 << "EOF"
# Begin /etc/pam.d/polkit-1

auth     include    system-auth
account  include    system-account
password include    system-password
session  include    system-session

# End /etc/pam.d/polkit-1
EOF
popd

%files
%defattr(-,root,root,-)
/etc/dbus-1/*
/etc/polkit-1/rules.d/50-default.rules
/etc/pam.d/polkit-1
%{_bindir}/pkaction
%{_bindir}/pkcheck
%{_bindir}/pk-example-frobnicate
%attr(4755,root,root) %{_bindir}/pkexec
%{_bindir}/pkttyagent
%{_includedir}/polkit-1/polkit*/*.h
%{_libdir}/girepository-1.0/*
%{_libdir}/libpolkit-*.*
%{_libdir}/pkgconfig/polkit-*.pc
%attr(4755,root,root) /usr/lib/polkit-1/polkit-agent-helper-1
/usr/lib/polkit-1/polkitd
%{_datadir}/dbus-1/*
%{_datadir}/gir-1.0/*
%{_datadir}/locale/*
%{_datadir}/polkit-1/actions/org.freedesktop.policykit*.policy
%{_mandir}/*/*
%attr(0755,root,root) %{_datadir}/polkit-1/rules.d
/lib/systemd/system/polkit.service

%post
groupadd -fg 27 polkitd &&
useradd -c "PolicyKit Daemon Owner" -d /etc/polkit-1 -u 27 \
        -g polkitd -s /bin/false polkitd

%postun

%clean
rm -rf %{buildroot}

%changelog
* Sun Sep 29 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.111 to 0.112
* Mon Jul 22 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.110 to 0.111
* Mon Apr 15 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.107 to 0.110
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
