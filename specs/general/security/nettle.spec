Name:       nettle
Version:    2.7.1
Release:    1%{?dist}
Summary:    Nettle

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.gnu.org
Source:     http://ftp.gnu.org/gnu/nettle/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  m4 openssl

%description
The Nettle package contains the low-level cryptographic library that is designed to fit easily in many contexts. 

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

chmod -v 755 %{buildroot}/%{_libdir}/libhogweed.so.2.5 %{buildroot}/%{_libdir}/libnettle.so.4.7 &&
install -v -m755 -d %{buildroot}/%{_docdir}/nettle-%{version} &&
install -v -m644 nettle.html %{buildroot}/%{_docdir}/nettle-%{version}

rm -f %{buildroot}/%{_infodir}/dir

%files
%defattr(-,root,root,-)
%{_bindir}/nettle-hash
%{_bindir}/nettle-lfib-stream
%{_bindir}/pkcs1-conv
%{_bindir}/sexp-conv
%{_includedir}/nettle/*.h
%{_libdir}/libhogweed.a
%{_libdir}/libhogweed.so*
%{_libdir}/libnettle.a
%{_libdir}/libnettle.so*
%{_libdir}/pkgconfig/hogweed.pc
%{_libdir}/pkgconfig/nettle.pc

%files doc
%defattr(-,root,root,-)
%{_docdir}/nettle-%{version}/nettle.html
%{_infodir}/nettle.info.gz

%clean
rm -rf %{buildroot}

%changelog
* Wed Jul 31 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.7 to 2.7.1
* Sat Jul 6 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.6 to 2.7
* Thu Apr 11 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.5 to 2.6
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
