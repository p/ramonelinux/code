Name:       cracklib
Version:    2.9.1
Release:    1%{?dist}
Summary:    CrackLib

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://downloads.sourceforge.net/cracklib
Source0:    http://downloads.sourceforge.net/cracklib/%{name}-%{version}.tar.gz
Source1:    %{name}-words-20080507.gz

Distribution: ramone linux
Vendor: ramone
Packager: tanggeliang <tanggeliang@gmail.com>

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-build

%description

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --with-default-dict=/lib/cracklib/pw_dict \
            --disable-static \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

mv -v %{buildroot}/%{_libdir}/libcrack.so.2* %{buildroot}/%{_lib} &&
ln -v -sf ../../%{_lib}/libcrack.so.2.9.0 %{buildroot}/%{_libdir}/libcrack.so

install -v -m644 -D %SOURCE1 \
    %{buildroot}/usr/share/dict/cracklib-words.gz &&
gunzip -v %{buildroot}/usr/share/dict/cracklib-words.gz &&
ln -v -s cracklib-words %{buildroot}/usr/share/dict/words &&
echo $(hostname) >>%{buildroot}/usr/share/dict/cracklib-extra-words &&
install -v -m755 -d %{buildroot}/lib/cracklib

%post
#create-cracklib-dict /usr/share/dict/cracklib-words \
#                     /usr/share/dict/cracklib-extra-words

%files
%defattr(-,root,root,-)
%{_includedir}/crack.h
%{_includedir}/packer.h
%{_libdir}/libcrack.*a
%{_libdir}/libcrack.so*
%{_libdir}/python2.7/site-packages/_cracklib.*
/usr/lib/python2.7/site-packages/cracklib.py*
/usr/lib/python2.7/site-packages/test_cracklib.py*
%{_sbindir}/cracklib-*
%{_sbindir}/create-cracklib-dict
%{_datadir}/cracklib/cracklib*
%{_datadir}/dict/cracklib-extra-words
%{_datadir}/dict/cracklib-words
%{_datadir}/dict/words
%{_datadir}/locale/*/LC_MESSAGES/cracklib.mo

%clean
rm -rf %{buildroot}

%changelog
* Tue Mar 18 2014 tanggeliang <tanggeliang@gmail.com>
- update from 2.9.0 to 2.9.1
* Wed Aug 14 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.8.22 to 2.9.0
* Thu May 30 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.8.19 to 2.8.22
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
