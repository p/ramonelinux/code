Name:       acl
Version:    2.2.52
Release:    2%{?dist}
Summary:    Access Control Lists

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://mirrors.zerg.biz/nongnu/acl
Source:     http://mirrors.zerg.biz/nongnu/acl/%{name}-%{version}.src.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  attr gettext

%description
The acl package contains utilities to administer Access Control Lists, 
which are used to define more fine-grained discretionary access rights for files and directories. 

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
sed -i -e 's|/@pkg_name@|&-@pkg_version@|' \
     include/builddefs.in &&

INSTALL_USER=root \
INSTALL_GROUP=root \
./configure --prefix=/usr --libdir=/%{_lib} --libexecdir=/usr/%{_lib} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make install install-dev install-lib DESTDIR=%{buildroot}

chmod -v 755 %{buildroot}/%{_lib}/libacl.so                         &&
ln -sfv ../../%{_lib}/libacl.so.1 %{buildroot}/%{_libdir}/libacl.so &&
install -v -m644 doc/*.txt %{buildroot}/%{_docdir}/acl-%{version}

%files
%defattr(-,root,root,-)
%{_bindir}/chacl
%{_bindir}/getfacl
%{_bindir}/setfacl
%{_includedir}/acl/*.h
%{_includedir}/sys/*.h
/%{_lib}/libacl.so*
/%{_lib}/libacl.*a
%{_libdir}/libacl.*a
%{_libdir}/libacl.so
%{_datadir}/locale/*/LC_MESSAGES/acl.mo

%files doc
%defattr(-,root,root,-)
%{_docdir}/acl-%{version}/*
%{_mandir}/man1/*acl.1.gz
%{_mandir}/man3/acl*.3.gz
%{_mandir}/man5/acl.5.gz

%clean
rm -rf %{buildroot}

%changelog
* Sat Jul 6 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.2.51 to 2.2.52
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
