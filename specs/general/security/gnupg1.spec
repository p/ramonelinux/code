Name:       gnupg1
Version:    1.4.12
Release:    5%{?dist}
Summary:    GnuPG

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.gnupg.org
Source:     ftp://ftp.gnupg.org/gcrypt/gnupg/gnupg-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  openldap libusb-compat curl

%description
The GnuPG package contains a public/private key encryptor.
This is becoming useful for signing files or emails as proof of identity and preventing tampering with the contents of the file or email.
For a more enhanced version of GnuPG which supports S/MIME, see the GnuPG-2.0.19 package.

%prep
%setup -q -n gnupg-%{version}

%build
./configure --prefix=/usr --libexecdir=/usr/lib &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot} &&

install -v -m755 -d %{buildroot}/usr/share/doc/gnupg-1.4.12 &&
mv -v %{buildroot}/usr/share/gnupg/FAQ %{buildroot}/usr/share/doc/gnupg-1.4.12 &&
install -v -m644 \
    doc/{highlights-1.4.txt,OpenPGP,samplekeys.asc,DETAILS,*.texi} \
    %{buildroot}/usr/share/doc/gnupg-1.4.12

rm -rf %{buildroot}/%{_infodir}/dir
rm -rf %{buildroot}/%{_mandir}/man1/gpg-zip.*

%files
%defattr(-,root,root,-)
%{_bindir}/gpg
%{_bindir}/gpg-zip
%{_bindir}/gpgsplit
%{_bindir}/gpgv
%{_libdir}/gnupg/gpgkeys_curl
%{_libdir}/gnupg/gpgkeys_finger
%{_libdir}/gnupg/gpgkeys_hkp
%{_libdir}/gnupg/gpgkeys_ldap
%{_datadir}/gnupg/options.skel
%{_datadir}/locale/*/LC_MESSAGES/gnupg.mo
%{_docdir}/gnupg-1.4.12/*
%{_infodir}/gnupg1.info.gz
%{_mandir}/man*/*.gz

%clean
rm -rf %{buildroot}

%changelog
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
