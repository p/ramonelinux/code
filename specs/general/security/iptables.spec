Name:       iptables
Version:    1.4.21
Release:    1%{?dist}
Summary:    Iptables

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.netfilter.org
Source:     http://www.netfilter.org/projects/iptables/files/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build

%description
The next part of this chapter deals with firewalls.
The principal firewall tool for Linux is Iptables.
You will need to install Iptables if you intend on using any form of a firewall.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr                            \
            --exec-prefix=                           \
            --bindir=/usr/bin                        \
            --with-xtlibdir=/%{_lib}/xtables         \
            --with-pkgconfigdir=%{_libdir}/pkgconfig \
            --enable-libipq                          \
            --enable-devel                           \
            --libdir=/%{_lib} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

ln -sfv xtables-multi %{buildroot}/sbin/iptables-xml

%files
%defattr(-,root,root,-)
/%{_lib}/libip4tc.*
/%{_lib}/libip6tc.*
/%{_lib}/libipq.*
/%{_lib}/libiptc.*
/%{_lib}/libxtables.*
/%{_lib}/xtables/libip6t_*.so
/%{_lib}/xtables/libipt_*.so
/%{_lib}/xtables/libxt_*.so
%{_includedir}/libiptc/*.h
%{_includedir}/libipq.h
%{_includedir}/xtables*.h
%{_bindir}/iptables*
/sbin/ip6tables*
/sbin/iptables
/sbin/iptables-restore
/sbin/iptables-save
/sbin/iptables-xml
/sbin/xtables-multi
%{_libdir}/pkgconfig/libip4tc.pc
%{_libdir}/pkgconfig/libip6tc.pc
%{_libdir}/pkgconfig/libipq.pc
%{_libdir}/pkgconfig/libiptc.pc
%{_libdir}/pkgconfig/xtables.pc

%files doc
%defattr(-,root,root,-)
%{_mandir}/*

%clean
rm -rf %{buildroot}

%changelog
* Wed Dec 4 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.4.20 to 1.4.21
* Wed Aug 21 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.4.16.2 to 1.4.20
* Sat Nov 3 2012 tanggeliang <tanggeliang@gmail.com>
- update from 1.4.15 to 1.4.16.2
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
