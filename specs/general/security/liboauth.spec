Name:       liboauth
Version:    1.0.0
Release:    1%{?dist}
Summary:    OAuth

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://liboauth.sourceforge.net
Source:     http://downloads.sourceforge.net/liboauth/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  curl openssl nss

%description
Liboauth is a collection of POSIX-C functions implementing the OAuth Core RFC 5849 standard.
Liboauth provides functions to escape and encode parameters according to OAuth specification and offers high-level functionality to sign requests or verify OAuth signatures as well as perform HTTP requests. 

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --disable-static \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_includedir}/oauth.h
%{_libdir}/liboauth.*
%{_libdir}/pkgconfig/oauth.pc
%{_mandir}/man3/oauth.3.gz

%clean
rm -rf %{buildroot}

%changelog
* Thu Apr 11 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.9.7 to 1.0.0
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
