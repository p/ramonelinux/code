Name:       libpwquality
Version:    1.2.3
Release:    1%{?dist}
Summary:    libpwquality

Group:      System Environment/libraries
License:    GPLv2+
Url:        https://fedorahosted.org
Source:     https://fedorahosted.org/releases/l/i/libpwquality/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  cracklib
BuildRequires:  linux-pam python

%description
The libpwquality package contains a library used for password quality checking and generation of random passwords that pass the checks.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --sysconfdir=/etc \
            --with-securedir=/%{_lib}/security \
            --disable-python-bindings \
            --disable-static \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
/etc/security/pwquality.conf
/%{_lib}/security/pam_pwquality.*
%{_bindir}/pwmake
%{_bindir}/pwscore
%{_includedir}/pwquality.h
%{_libdir}/libpwquality.la
%{_libdir}/libpwquality.so*
%{_libdir}/pkgconfig/pwquality.pc
%{_datadir}/locale/*/LC_MESSAGES/libpwquality.mo
%{_mandir}/man*/*.gz

%clean
rm -rf %{buildroot}

%changelog
* Wed Sep 25 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.2.1 to 1.2.3
* Sat Apr 13 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.2.0 to 1.2.1
* Sat Sep 29 2012 tanggeliang <tanggeliang@gmail.com>
- create
