Name:       krb
Version:    1.11.1
Release:    2%{?dist}
Summary:    MIT Kerberos V5

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://mirrors.zerg.biz/nongnu/acl
Source:     http://web.mit.edu/kerberos/www/dist/krb5/1.11/%{name}5-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  e2fsprogs bison flex autoconf automake m4
Requires:       e2fsprogs
AutoReq:        no

%description
MIT Kerberos V5 is a free implementation of Kerberos 5.
Kerberos is a network authentication protocol.
It centralizes the authentication database and uses kerberized applications to work with servers or services that support Kerberos allowing single logins and encrypted communication over internal networks or the Internet.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}5-%{version}

%build
cd src &&
sed -e "s@python2.5/Python.h@& python2.7/Python.h@g" \
    -e "s@-lpython2.5]@&,\n  AC_CHECK_LIB(python2.7,main,[PYTHON_LIB=-lpython2.7])@g" \
    -i configure.in &&
sed -e "s@interp->result@Tcl_GetStringResult(interp)@g" \
    -i kadmin/testing/util/tcl_kadm5.c &&
autoconf &&
./configure CPPFLAGS="-I/usr/include/et -I/usr/include/ss" \
            --prefix=/usr                                  \
            --localstatedir=/var/lib                       \
            --with-system-et                               \
            --with-system-ss                               \
            --enable-dns-for-realm                         \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
cd src &&
make install DESTDIR=%{buildroot} &&

mkdir -pv %{buildroot}/{etc,bin,%{_lib}}

for LIBRARY in gssapi_krb5 gssrpc k5crypto kadm5clnt_mit kadm5srv_mit \
               kdb5 kdb_ldap krb5 krb5support verto ; do
    [ -e  %{buildroot}/%{_libdir}/lib$LIBRARY.so.*.* ] && chmod -v 755 /%{buildroot}/%{_libdir}/lib$LIBRARY.so.*.*
done &&

mv -v %{buildroot}/%{_libdir}/libkrb5.so.3*        %{buildroot}/%{_lib} &&
mv -v %{buildroot}/%{_libdir}/libk5crypto.so.3*    %{buildroot}/%{_lib} &&
mv -v %{buildroot}/%{_libdir}/libkrb5support.so.0* %{buildroot}/%{_lib} &&

ln -v -sf ../../%{_lib}/libkrb5.so.3.3        %{buildroot}/%{_libdir}/libkrb5.so        &&
ln -v -sf ../../%{_lib}/libk5crypto.so.3.1    %{buildroot}/%{_libdir}/libk5crypto.so    &&
ln -v -sf ../../%{_lib}/libkrb5support.so.0.1 %{buildroot}/%{_libdir}/libkrb5support.so &&

mv -v %{buildroot}/usr/bin/ksu %{buildroot}/bin &&
chmod -v 755 %{buildroot}/bin/ksu   &&

install -v -dm755 %{buildroot}/usr/share/doc/krb5-%{version} &&
cp -vfr ../doc/*  %{buildroot}/usr/share/doc/krb5-%{version} &&

unset LIBRARY

cat > %{buildroot}/etc/krb5.conf << "EOF"
# Begin /etc/krb5.conf

[libdefaults]
    default_realm = <LFS.ORG>
    encrypt = true

[realms]
    <LFS.ORG> = {
        kdc = <belgarath.lfs.org>
        admin_server = <belgarath.lfs.org>
        dict_file = /usr/share/dict/words
    }

[domain_realm]
    .<lfs.org> = <LFS.ORG>

[logging]
    kdc = SYSLOG[:INFO[:AUTH]]
    admin_server = SYSLOG[INFO[:AUTH]]
    default = SYSLOG[[:SYS]]

# End /etc/krb5.conf
EOF

%files
%defattr(-,root,root,-)
/bin/ksu
/etc/krb5.conf
/%{_lib}/libk5crypto.so.3*
/%{_lib}/libkrb5.so.3*
/%{_lib}/libkrb5support.so.0*
%{_bindir}/gss-client
%{_bindir}/k5srvutil
%{_bindir}/kadmin
%{_bindir}/kdestroy
%{_bindir}/kinit
%{_bindir}/klist
%{_bindir}/kpasswd
%{_bindir}/krb5-config
%{_bindir}/kswitch
%{_bindir}/ktutil
%{_bindir}/kvno
%{_bindir}/sclient
%{_bindir}/sim_client
%{_bindir}/uuclient
%{_includedir}/*.h
%{_includedir}/gssapi/*.h
%{_includedir}/gssrpc/*.h
%{_includedir}/kadm5/*.h
%{_includedir}/krb5/*.h
%{_libdir}/krb5/plugins/*
%{_libdir}/libgssapi_krb5.so*
%{_libdir}/libgssrpc.so*
%{_libdir}/libk5crypto.so
%{_libdir}/libkadm5*.so*
%{_libdir}/libkdb5.so*
%{_libdir}/libkrb5*.so
%{_libdir}/libverto*.so*
%{_sbindir}/gss-server
%{_sbindir}/kadmin.local
%{_sbindir}/kadmind
%{_sbindir}/kdb5_util
%{_sbindir}/kprop
%{_sbindir}/kpropd
%{_sbindir}/kproplog
%{_sbindir}/krb5-send-pr
%{_sbindir}/krb5kdc
%{_sbindir}/sim_server
%{_sbindir}/sserver
%{_sbindir}/uuserver
%{_datadir}/examples/krb5/*
%{_datadir}/gnats/mit

%files doc
%defattr(-,root,root,-)
%{_docdir}/krb5-%{version}/*
%{_mandir}/man*/*.gz
%{_mandir}/man5/.k5*.5.gz

%clean
rm -rf %{buildroot}

%changelog
* Tue Mar 19 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.10.3 to 1.11.1
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
