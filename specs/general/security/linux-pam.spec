Name:       linux-pam
Version:    1.1.8
Release:    2%{?dist}
Summary:    Linux-PAM

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://linux-pam.org
Source:     http://linux-pam.org/library/Linux-PAM-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  bison flex
BuildRequires:  gettext libxslt docbook-xml docbook-xsl db

%description
The Linux-PAM package contains Pluggable Authentication Modules.
This is useful to enable the local system administrator to choose how applications authenticate users.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n Linux-PAM-%{version}

%build
./configure --prefix=/usr \
            --sysconfdir=/etc \
            --docdir=/usr/share/doc/Linux-PAM-%{version} \
            --disable-nis \
            --libdir=/%{_lib} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

chmod -v 4755 %{buildroot}/sbin/unix_chkpwd

%files
%defattr(-,root,root,-)
%{_sysconfdir}/environment
%{_sysconfdir}/security/access.conf
%{_sysconfdir}/security/group.conf
%{_sysconfdir}/security/limits.conf
%dir %{_sysconfdir}/security/limits.d
%{_sysconfdir}/security/namespace.conf
%dir %{_sysconfdir}/security/namespace.d
%{_sysconfdir}/security/namespace.init
%{_sysconfdir}/security/pam_env.conf
%{_sysconfdir}/security/time.conf
/%{_lib}/libpam.*
/%{_lib}/libpam_misc.*
/%{_lib}/libpamc.*
/%{_lib}/security/*
/sbin/mkhomedir_helper
/sbin/pam_tally
/sbin/pam_tally2
/sbin/pam_timestamp_check
/sbin/unix_chkpwd
/sbin/unix_update
%{_includedir}/security/*pam*.h
%{_datadir}/locale/*/LC_MESSAGES/Linux-PAM.mo

%files doc
%defattr(-,root,root,-)
%{_docdir}/Linux-PAM-%{version}/*
%{_mandir}/man*/*

%clean
rm -rf %{buildroot}

%changelog
* Wed Oct 2 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.1.6 to 1.1.8
* Wed Sep 19 2012 tanggeliang <tanggeliang@gmail.com>
- add "sed -i '305s ..." to fix "cannot create directory ‘/etc/security/namespace.d’: Permission denied".
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
