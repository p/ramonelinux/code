Name:       lxc
Version:    1.1.1
Release:    1%{?dist}
Summary:    LXC - Linux Containers

Group:      Development/Libraries
License:    BSD and LGPLv2+
Url:        http://linuxcontainers.org
Source:     http://linuxcontainers.org/downloads/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  systemd gnutls lua

%description
LXC is a userspace interface for the Linux kernel containment features.
Through a powerful API and simple tools, it lets Linux users easily create and manage system or application containers.

%package        python3
Summary:        python3
BuildRequires:  python3
%description    python3

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --sysconfdir=/etc \
            --localstatedir=/var \
            --with-init-script=systemd \
            --libdir=%{_libdir} \
	    --libexecdir=%{_libdir} &&
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} SYSTEMD_UNIT_DIR=/lib/systemd/system install

%files
%defattr(-,root,root,-)
/etc/bash_completion.d/lxc
/etc/default/lxc
/etc/lxc/default.conf
%{_bindir}/lxc-attach
%{_bindir}/lxc-autostart
%{_bindir}/lxc-cgroup
%{_bindir}/lxc-checkconfig
%{_bindir}/lxc-checkpoint
%{_bindir}/lxc-clone
%{_bindir}/lxc-config
%{_bindir}/lxc-console
%{_bindir}/lxc-create
%{_bindir}/lxc-destroy
%{_bindir}/lxc-device
%{_bindir}/lxc-execute
%{_bindir}/lxc-freeze
%{_bindir}/lxc-info
%{_bindir}/lxc-ls
%{_bindir}/lxc-monitor
%{_bindir}/lxc-snapshot
%{_bindir}/lxc-start
%{_bindir}/lxc-stop
%{_bindir}/lxc-top
%{_bindir}/lxc-unfreeze
%{_bindir}/lxc-unshare
%{_bindir}/lxc-usernsexec
%{_bindir}/lxc-wait
%{_includedir}/lxc/attach_options.h
%{_includedir}/lxc/lxccontainer.h
%{_includedir}/lxc/version.h
%{_libdir}/liblxc.so*
%{_libdir}/lua/5.2/lxc/core.so
%{_libdir}/lxc/lxc-devsetup
%{_libdir}/lxc/lxc-monitord
%{_libdir}/lxc/lxc-user-nic
%{_libdir}/lxc/lxc-apparmor-load
%{_libdir}/lxc/lxc-containers
%{_libdir}/lxc/lxc-net
%{_libdir}/lxc/rootfs/README
%{_libdir}/pkgconfig/lxc.pc
%{_sbindir}/init.lxc
%{_sbindir}/init.lxc.static
%{_datadir}/lua/5.2/lxc.lua
%{_datadir}/lxc/config/common.conf.d/README
%{_datadir}/lxc/config/common.seccomp
%{_datadir}/lxc/config/*.conf
%{_datadir}/lxc/hooks/clonehostname
%{_datadir}/lxc/hooks/mountecryptfsroot
%{_datadir}/lxc/hooks/squid-deb-proxy-client
%{_datadir}/lxc/hooks/ubuntu-cloud-prep
%{_datadir}/lxc/lxc.functions
%{_datadir}/lxc/templates/lxc-*
%{_datadir}/lxc/lxc-patch.py
%{_datadir}/lxc/lxc-restore-net
%{_datadir}/lxc/selinux/lxc.if
%{_datadir}/lxc/selinux/lxc.te
%{_docdir}/lxc/examples/lxc-*.conf
%{_docdir}/lxc/examples/seccomp-*.conf
%dir /var/lib/lxc
%dir /var/cache/lxc
/lib/systemd/system/lxc.service
/lib/systemd/system/lxc-net.service

%files python3
%defattr(-,root,root,-)
%{_bindir}/lxc-start-ephemeral
/usr/lib/python3.3/site-packages/_lxc-0.1-py3.3.egg-info
/usr/lib/python3.3/site-packages/_lxc.cpython-33m.so
/usr/lib/python3.3/site-packages/lxc/__init__.py

%post
systemctl enable lxc

%postun
systemctl disable lxc

%changelog
* Thu Mar 26 2015 tanggeliang <tanggeliang@gmail.com>
- update from 1.0.0 to 1.1.1
* Tue Feb 25 2014 tanggeliang <tanggeliang@gmail.com>
- create
