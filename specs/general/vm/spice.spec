Name:       spice
Version:    0.12.4
Release:    1%{?dist}
Summary:    Spice

Group:      Development/Libraries
License:    BSD and LGPLv2+
Url:        http://www.spice-space.org/
Source:     http://www.spice-space.org/download/releases/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  pixman celt pyparsing alsa-lib openssl
BuildRequires:  libx11 libxrandr libxfixes libxinerama
BuildRequires:  libjpeg-turbo cyrus-sasl libcacard
BuildRequires:  spice-protocol glib
BuildRequires:  autoconf automake m4 libtool

%description
In computing, SPICE (the Simple Protocol for Independent Computing Environments) is a remote-display system built for virtual environments
which allows users to view a computing "desktop" environment
- not only on its computer-server machine,
but also from anywhere on the Internet and using a wide variety of machine architectures.

%prep
%setup -q -n %{name}-%{version}

%build
%configure --disable-client
make WARN_CFLAGS='' %{?_smp_mflags}

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%doc
%{_includedir}/spice-server/spice-experimental.h
%{_includedir}/spice-server/spice.h
%{_libdir}/libspice-server.*a
%{_libdir}/libspice-server.so*
%{_libdir}/pkgconfig/spice-server.pc

%changelog
* Wed Aug 14 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.12.0 to 0.12.4
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
