Name:       libvirt
Version:    1.2.13
Release:    1%{?dist}
Summary:    libvirt

Group:      Development/Libraries
License:    BSD and LGPLv2+
Url:        http://libvirt.org
Source:     http://www.libvirt.org/sources/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  lloyd-yajl lvm systemd libpciaccess libnl

%description
A toolkit to interact with the virtualization capabilities of recent versions of Linux (and other OSes).

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --libdir=%{_libdir} \
	    --libexecdir=%{_libdir}/%{name} &&
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
/etc/libvirt/nwfilter/*.xml
/etc/libvirt/*.conf
/etc/libvirt/qemu/networks/autostart/default.xml
/etc/libvirt/qemu/networks/default.xml
/etc/logrotate.d/libvirtd*
%{_bindir}/virsh
%{_bindir}/virt-host-validate
%{_bindir}/virt-login-shell
%{_bindir}/virt-pki-validate
%{_bindir}/virt-xml-validate
%{_includedir}/libvirt/libvirt*.h
%{_includedir}/libvirt/virterror.h
%{_libdir}/libvirt-lxc.la
%{_libdir}/libvirt-lxc.so*
%{_libdir}/libvirt-qemu.la
%{_libdir}/libvirt-qemu.so*
%{_libdir}/libvirt.la
%{_libdir}/libvirt.so*
%{_libdir}/libvirt/connection-driver/libvirt_driver_interface.*
%{_libdir}/libvirt/connection-driver/libvirt_driver_lxc.*
%{_libdir}/libvirt/connection-driver/libvirt_driver_network.*
%{_libdir}/libvirt/connection-driver/libvirt_driver_nodedev.*
%{_libdir}/libvirt/connection-driver/libvirt_driver_nwfilter.*
%{_libdir}/libvirt/connection-driver/libvirt_driver_qemu.*
%{_libdir}/libvirt/connection-driver/libvirt_driver_secret.*
%{_libdir}/libvirt/connection-driver/libvirt_driver_storage.*
%{_libdir}/libvirt/connection-driver/libvirt_driver_uml.*
%{_libdir}/libvirt/connection-driver/libvirt_driver_vbox.*
%{_libdir}/libvirt/lock-driver/lockd.*
%{_libdir}/pkgconfig/libvirt*.pc
/usr/lib/sysctl.d/libvirtd.conf
%{_libdir}/libvirt/libvirt-guests.sh
%{_libdir}/libvirt/libvirt_*
%{_sbindir}/libvirtd
%{_sbindir}/virtlockd
%{_datadir}/augeas/lenses/*virt*.aug
%{_datadir}/augeas/lenses/tests/test_*.aug
%{_docdir}/libvirt-%{version}/html/*
%{_datadir}/gtk-doc/html/libvirt/*
%{_datadir}/libvirt/api/libvirt-*.xml
%{_datadir}/libvirt/cpu_map.xml
%{_datadir}/libvirt/libvirtLogo.png
%{_datadir}/libvirt/schemas/basictypes.rng
%{_datadir}/libvirt/schemas/capability.rng
%{_datadir}/libvirt/schemas/domain.rng
%{_datadir}/libvirt/schemas/domaincaps.rng
%{_datadir}/libvirt/schemas/domaincommon.rng
%{_datadir}/libvirt/schemas/domainsnapshot.rng
%{_datadir}/libvirt/schemas/interface.rng
%{_datadir}/libvirt/schemas/network.rng
%{_datadir}/libvirt/schemas/networkcommon.rng
%{_datadir}/libvirt/schemas/nodedev.rng
%{_datadir}/libvirt/schemas/nwfilter.rng
%{_datadir}/libvirt/schemas/secret.rng
%{_datadir}/libvirt/schemas/storagecommon.rng
%{_datadir}/libvirt/schemas/storagepool.rng
%{_datadir}/libvirt/schemas/storagevol.rng
%{_datadir}/locale/*/LC_MESSAGES/libvirt.mo
%{_mandir}/man*/*.gz

%changelog
* Thu Mar 26 2015 tanggeliang <tanggeliang@gmail.com>
- create
