Name:       xen
Version:    4.3.1
Release:    1%{?dist}
Summary:    Xen is a virtual machine monitor

Group:      Applications/Emulators
License:    LGPLv2+
Url:        http://www.xen.org
Source:     http://bits.xensource.com/oss-xen/release/%{version}/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  gettext gnutls

%description
This package contains the XenD daemon and xm command line tools, needed to manage virtual machines running under the Xen hypervisor.

%prep
%setup -q -n %{name}-%{version}

%build
#make %{?_smp_mflags} prefix=/usr dist-xen
./configure --libdir=%{_libdir}
#make %{?_smp_mflags} prefix=/usr dist-tools
make %{?_smp_mflags} prefix=/usr dist-docs
#make %{?_smp_mflags} dist-stubdom

%check

%install
rm -rf %{buildroot}
#make DESTDIR=%{buildroot} prefix=/usr install-xen
#make DESTDIR=%{buildroot} prefix=/usr install-tools
make DESTDIR=%{buildroot} prefix=/usr install-docs
#make DESTDIR=%{buildroot} prefix=/usr install-stubdom

%files
%defattr(-,root,root,-)
/boot/xen-4*.gz
/boot/xen-syms-4.3.1
/boot/xen.gz

%clean
rm -rf %{buildroot}

%changelog
* Thu Dec 19 2013 tanggeliang <tanggeliang@gmail.com>
- create
