Name:           celt
Version:        0.5.1.3
Release:        5%{?dist}
Summary:        An audio codec for use in low-delay speech and audio communication

Group:          System Environment/Libraries
License:        BSD
URL:            http://www.celt-codec.org/
Source:         http://downloads.us.xiph.org/releases/celt/%{name}-%{version}.tar.gz

BuildRequires: libogg

%description
CELT (Constrained Energy Lapped Transform) is an ultra-low delay audio
codec designed for realtime transmission of high quality speech and audio.
This is meant to close the gap between traditional speech codecs
(such as Speex) and traditional audio codecs (such as Vorbis).

The CELT bitstream format is not yet stable, this package is a special
version of 0.5.1 that has the same bitstream format, but symbols and files
renamed from 'celt*' to 'celt051*' so that it is parallel installable with
the normal celt for packages requiring this particular bitstream format.

%prep
%setup -q -n %{name}-%{version}

%build
%configure --disable-static
# Remove rpath as per https://fedoraproject.org/wiki/Packaging/Guidelines#Beware_of_Rpath
sed -i 's|^hardcode_libdir_flag_spec=.*|hardcode_libdir_flag_spec=""|g' libtool
sed -i 's|^runpath_var=LD_RUN_PATH|runpath_var=DIE_RPATH_DIE|g' libtool

make %{?_smp_mflags}

%install
make install DESTDIR=%{buildroot}

%files
%defattr(-,root,root,-)
%doc COPYING README TODO
%{_bindir}/celtenc051
%{_bindir}/celtdec051
%{_includedir}/celt051
%{_libdir}/libcelt051.so*
%{_libdir}/libcelt051.la
%{_libdir}/pkgconfig/celt051.pc

%changelog
