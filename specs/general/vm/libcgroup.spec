Name:       libcgroup
Version:    0.41
Release:    1%{?dist}
Summary:    Control Group Configuration

Group:      Development/Libraries
License:    BSD and LGPLv2+
Url:        http://libcg.sourceforge.net
Source:     http://sourceforge.net/projects/libcg/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  bison flex

%description
Control groups, a new kernel feature in Linux 2.6.24 provides a file system interface to manipulate and control the details on task grouping including creation of new task groups (control groups), permission handling and task assignment.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --sysconfdir=/etc \
            --libdir=%{_libdir} \
            --enable-pam-module-dir=/lib/security \
            --enable-initscript-install &&
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
/etc/rc.d/init.d/cgconfig
/etc/rc.d/init.d/cgred
%{_bindir}/cgclassify
%{_bindir}/cgcreate
%{_bindir}/cgdelete
%{_bindir}/cgexec
%{_bindir}/cgget
%{_bindir}/cgset
%{_bindir}/cgsnapshot
%{_bindir}/lscgroup
%{_bindir}/lssubsys
/lib/security/pam_cgroup.la
/lib/security/pam_cgroup.so*
%{_includedir}/libcgroup.h
%{_includedir}/libcgroup/*.h
%{_libdir}/libcgroup.la
%{_libdir}/libcgroup.so*
%{_libdir}/pkgconfig/libcgroup.pc
%{_sbindir}/cgclear
%{_sbindir}/cgconfigparser
%{_sbindir}/cgrulesengd
%{_mandir}/man*/*

%changelog
* Thu Mar 13 2014 tanggeliang <tanggeliang@gmail.com>
- create
