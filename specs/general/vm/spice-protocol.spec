Name:       spice-protocol
Version:    0.12.6
Release:    1%{?dist}
Summary:    Spice protocol header files

Group:      Development/Libraries
License:    BSD and LGPLv2+
Url:        http://www.spice-space.org/
Source:     http://www.spice-space.org/download/releases/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build

%description
In computing, SPICE (the Simple Protocol for Independent Computing Environments) is a remote-display system built for virtual environments
which allows users to view a computing "desktop" environment
- not only on its computer-server machine,
but also from anywhere on the Internet and using a wide variety of machine architectures.

%prep
%setup -q -n %{name}-%{version}

%build
%configure
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%doc COPYING NEWS
%{_includedir}/spice-1
%{_datadir}/pkgconfig/spice-protocol.pc

%changelog
* Wed Aug 14 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.12.2 to 0.12.6
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
