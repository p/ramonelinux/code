Name:       spice-xpi
Version:    2.8.90
Release:    1%{?dist}
Summary:    Spice xpi

Group:      Development/Libraries
License:    BSD and LGPLv2+
Url:        http://www.spice-space.org/
Source:     http://www.spice-space.org/download/releases/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  glib

%description
In computing, SPICE (the Simple Protocol for Independent Computing Environments) is a remote-display system built for virtual environments
which allows users to view a computing "desktop" environment
- not only on its computer-server machine,
but also from anywhere on the Internet and using a wide variety of machine architectures.

%prep
%setup -q -n %{name}-%{version}

%build
%configure
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_libdir}/mozilla/install.rdf
%{_libdir}/mozilla/plugins/npSpiceConsole.*a
%{_libdir}/mozilla/plugins/npSpiceConsole.so

%changelog
* Wed Aug 14 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.7 to 2.8.90
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
