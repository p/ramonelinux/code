Name:       wine
Version:    1.5.12
Release:    5%{?dist}
Summary:    A compatibility layer for windows applications

Group:      Applications/Emulators
License:    LGPLv2+
Url:        http://www.winehq.org
Source:     http://downloads.sourceforge.net/wine/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  bison flex autoconf m4 desktop-file-utils
BuildRequires:  freeglut lcms libjpeg libpng librsvg libtiff libusb libxml libxslt
BuildRequires:  openldap unixodbc openssl sane-backends freetype
BuildRequires:  libx11 libxxf86dga libxxf86vm libxrandr libxrender libxext libxinerama libxcomposite libxmu libxi libxcursor
BuildRequires:  mesalib fontconfig cups dbus gnutls gettext
BuildRequires:  pulseaudio imagemagick gstreamer gst-plugins-base alsa-lib audiofile

%description
Wine as a compatibility layer for UNIX to run Windows applications.

%prep
%setup -q -n %{name}-%{version}
autoreconf

%build
export CFLAGS="`echo $RPM_OPT_FLAGS | sed -e 's/-Wp,-D_FORTIFY_SOURCE=2//'` -Wno-error"
%configure \
    --sysconfdir=%{_sysconfdir}/wine \
    --x-includes=%{_includedir} --x-libraries=%{_libdir} \
    --without-hal --with-dbus \
    --with-x \
    --with-pulse \
    --disable-tests &&

make TARGETFLAGS="" %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}

%makeinstall \
    includedir=%{buildroot}%{_includedir}/wine \
    sysconfdir=%{buildroot}%{_sysconfdir}/wine \
    dlldir=%{buildroot}%{_libdir}/wine \
    LDCONFIG=/bin/true \
    UPDATE_DESKTOP_DATABASE=/bin/true

%files
%defattr(-,root,root,-)
%{_bindir}/function_grep.pl
%{_bindir}/msiexec
%{_bindir}/notepad
%{_bindir}/regedit
%{_bindir}/regsvr32
%{_bindir}/widl
%{_bindir}/wine
%{_bindir}/wine-preloader
%{_bindir}/wineboot
%{_bindir}/winebuild
%{_bindir}/winecfg
%{_bindir}/wineconsole
%{_bindir}/winecpp
%{_bindir}/winedbg
%{_bindir}/winedump
%{_bindir}/winefile
%{_bindir}/wineg++
%{_bindir}/winegcc
%{_bindir}/winemaker
%{_bindir}/winemine
%{_bindir}/winepath
%{_bindir}/wineserver
%{_bindir}/wmc
%{_bindir}/wrc
%{_includedir}/wine/*
%{_libdir}/libwine.so*
%{_libdir}/wine/*
%{_datadir}/applications/wine.desktop
%{_datadir}/wine/*
%{_mandir}/*/man1/*.1.gz
%{_mandir}/man1/*.1.gz

%clean
rm -rf %{buildroot}

%changelog
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
