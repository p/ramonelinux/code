Name:       spice-vdagent
Version:    0.14.0
Release:    1%{?dist}
Summary:    Spice vdagent

Group:      Development/Libraries
License:    BSD and LGPLv2+
Url:        http://www.spice-space.org/
Source:     http://www.spice-space.org/download/releases/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  libx11 libxfixes libxrandr libxinerama libpciaccess
BuildRequires:  spice-protocol dbus

%description
In computing, SPICE (the Simple Protocol for Independent Computing Environments) is a remote-display system built for virtual environments
which allows users to view a computing "desktop" environment
- not only on its computer-server machine,
but also from anywhere on the Internet and using a wide variety of machine architectures.

%prep
%setup -q -n %{name}-%{version}

%build
%configure
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%doc
/etc/rsyslog.d/spice-vdagentd.conf
/etc/xdg/autostart/spice-vdagent.desktop
%{_bindir}/spice-vdagent
%{_sbindir}/spice-vdagentd
%{_datadir}/gdm/autostart/LoginWindow/spice-vdagent.desktop
%{_mandir}/man1/spice-vdagent*.1.gz

%changelog
* Wed Aug 14 2013 tanggeliang <tanggeliang@gmail.com>
- update from 0.12.0 to 0.14.0
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
