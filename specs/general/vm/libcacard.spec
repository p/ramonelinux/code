Name:           libcacard
Version:        0.1.2
Release:        3%{?dist}
Summary:        Common Access Card (CAC) Emulation

Group:          System Environment/Libraries
License:        LGPLv2+
URL:            http://www.spice-space.org/download
Source:         http://www.spice-space.org/download/libcacard/%{name}-%{version}.tar.bz2
BuildRequires:  nss

%description
Common Access Card (CAC) emulation library.

%prep
%setup -q

%build
CFLAGS+="-lpthread"
%configure
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}
find %{buildroot} -name '*.la' -or -name '*.a' | xargs rm -f

%files
%defattr(-,root,root,-)
%doc COPYING README
%{_libdir}/libcacard.so.*
%{_includedir}/cacard
%{_libdir}/pkgconfig/libcacard.pc
%{_libdir}/libcacard.so
%{_bindir}/vscclient

%changelog
