%define with_guestfs               0
%define stable_defaults            0
%define askpass_package            "openssh-askpass"
%define qemu_user                  "qemu"
%define libvirt_packages           "libvirt-daemon-kvm,libvirt-daemon-config-network"
%define preferred_distros          "fedora,rhel"
%define kvm_packages               "qemu-system-x86"

%if 0%{?rhel}
%define preferred_distros          "rhel,fedora"
%define kvm_packages               "qemu-kvm"
%define stable_defaults            1
%endif

Name:		virt-manager
Version:	1.1.0
Release:	1%{?dist}
Summary:	Virtual Machine Manager

Group:		Applications/Emulators
License:	GPLv2+
URL:		http://virt-manager.org/
Source:		http://virt-manager.org/download/sources/%{name}/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildArch: 	noarch
BuildRequires:	intltool glib gettext perl
Requires: pygobject3
Requires: gtk3
Requires: libxml2-python
Requires: vte3
Requires: dconf
Requires: dbus-x11
Requires: libosinfo >= 0.2.10
# For console widget
Requires: gtk-vnc
Requires: spice-gtk3
# This version not strictly required: virt-manager should work with older,
# however varying amounts of functionality will not be enabled.
Requires: libxml2-python
Requires: python-urlgrabber
Requires: python-ipaddr
Requires: virt-manager-common = %{verrel}

%description
The virt-manager application is a desktop user interface for managing virtual machines through libvirt.
It primarily targets KVM VMs, but also manages Xen and LXC (linux containers).
It presents a summary view of running domains, their live performance & resource utilization statistics.
Wizards enable the creation of new domains, and configuration & adjustment of a domain’s resource allocation & virtual hardware.
An embedded VNC and SPICE client viewer presents a full graphical console to the guest domain.

%prep
%setup -q -n %{name}-%{version}

%build
%if %{qemu_user}
%define _qemu_user --qemu-user=%{qemu_user}
%endif

%if %{kvm_packages}
%define _kvm_packages --kvm-package-names=%{kvm_packages}
%endif

%if %{preferred_distros}
%define _preferred_distros --preferred-distros=%{preferred_distros}
%endif

%if %{libvirt_packages}
%define _libvirt_packages --libvirt-package-names=%{libvirt_packages}
%endif

%if %{askpass_package}
%define _askpass_package --askpass-package-names=%{askpass_package}
%endif

%if %{stable_defaults}
%define _stable_defaults --stable-defaults
%endif

python setup.py configure \
    --pkgversion="%{version}" \
    %{?_qemu_user} \
    %{?_kvm_packages} \
    %{?_libvirt_packages} \
    %{?_askpass_package} \
    %{?_preferred_distros} \
    %{?_stable_defaults}

%install
python setup.py install -O1 --root=$RPM_BUILD_ROOT

%find_lang %{name}

%post
/bin/touch --no-create %{_datadir}/icons/hicolor &>/dev/null || :
/usr/bin/update-desktop-database &> /dev/null || :

%postun
if [ $1 -eq 0 ] ; then
    /bin/touch --no-create %{_datadir}/icons/hicolor &>/dev/null
    /usr/bin/gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
    /usr/bin/glib-compile-schemas %{_datadir}/glib-2.0/schemas &> /dev/null || :
fi
/usr/bin/update-desktop-database &> /dev/null || :

%posttrans
/usr/bin/gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
/usr/bin/glib-compile-schemas %{_datadir}/glib-2.0/schemas &> /dev/null || :

%files
%defattr(-,root,root,-)
%{_bindir}/virt-clone
%{_bindir}/virt-convert
%{_bindir}/virt-install
%{_bindir}/virt-manager
%{_bindir}/virt-xml
%{_datadir}/appdata/virt-manager.appdata.xml
%{_datadir}/applications/virt-manager.desktop
%{_datadir}/glib-2.0/schemas/org.virt-manager.virt-manager.gschema.xml
%{_datadir}/icons/hicolor/*/apps/virt-manager.png
%{_datadir}/locale/*/LC_MESSAGES/virt-manager.mo
%{_datadir}/virt-manager/icons/hicolor/*/*/*.png
%{_datadir}/virt-manager/ui/*.ui
%{_datadir}/virt-manager/virt-clone
%{_datadir}/virt-manager/virt-convert
%{_datadir}/virt-manager/virt-install
%{_datadir}/virt-manager/virt-manager
%{_datadir}/virt-manager/virt-xml
%{_datadir}/virt-manager/virtManager/*.py
%{_datadir}/virt-manager/virtcli/*.py
%{_datadir}/virt-manager/virtcli/cli.cfg
%{_datadir}/virt-manager/virtconv/*.py
%{_datadir}/virt-manager/virtinst/*.py
%{_mandir}/man1/virt-*.1.gz

%changelog
* Thu Mar 26 2015 tanggeliang <tanggeliang@gmail.com>
- create
