Name:       qemu
Version:    2.2.0
Release:    1%{?dist}
Summary:    QEMU

Group:      Applications/Emulators
License:    GPLv2+
Url:        http://www.qemu.org
Source:     http://wiki.qemu.org/download/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  glib python sdl libx11
BuildRequires:  alsa-lib attr check curl esound mesalib cyrus-sasl
BuildRequires:  libtool libjpeg-turbo libpng gnutls usbutils xfsprogs
BuildRequires:  bison flex autoconf automake m4
Requires:       bridge-utils
AutoReq:        no

%description
QEMU is a generic and open source machine emulator and virtualizer.

%prep
%setup -q -n %{name}-%{version}

%build
export ARFLAGS="rv"
./configure --prefix=/usr                \
            --sysconfdir=/etc            \
            --libexecdir=%{_libdir}/qemu \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

%files
%defattr(-,root,root,-)
/etc/qemu/target-x86_64.conf
%{_bindir}/qemu-aarch64
%{_bindir}/qemu-alpha
%{_bindir}/qemu-arm
%{_bindir}/qemu-armeb
%{_bindir}/qemu-cris
%{_bindir}/qemu-ga
%{_bindir}/qemu-i386
%{_bindir}/qemu-img
%{_bindir}/qemu-io
%{_bindir}/qemu-m68k
%{_bindir}/qemu-microblaze
%{_bindir}/qemu-microblazeel
%{_bindir}/qemu-mips
%{_bindir}/qemu-mips64
%{_bindir}/qemu-mips64el
%{_bindir}/qemu-mipsn32
%{_bindir}/qemu-mipsn32el
%{_bindir}/qemu-mipsel
%{_bindir}/qemu-nbd
%{_bindir}/qemu-or32
%{_bindir}/qemu-ppc
%{_bindir}/qemu-ppc64
%{_bindir}/qemu-ppc64abi32
%{_bindir}/qemu-ppc64le
%{_bindir}/qemu-s390x
%{_bindir}/qemu-sh4
%{_bindir}/qemu-sh4eb
%{_bindir}/qemu-sparc
%{_bindir}/qemu-sparc32plus
%{_bindir}/qemu-sparc64
%{_bindir}/qemu-system-aarch64
%{_bindir}/qemu-system-alpha
%{_bindir}/qemu-system-arm
%{_bindir}/qemu-system-cris
%{_bindir}/qemu-system-i386
%{_bindir}/qemu-system-lm32
%{_bindir}/qemu-system-m68k
%{_bindir}/qemu-system-microblaze
%{_bindir}/qemu-system-microblazeel
%{_bindir}/qemu-system-mips
%{_bindir}/qemu-system-mips64
%{_bindir}/qemu-system-mips64el
%{_bindir}/qemu-system-mipsel
%{_bindir}/qemu-system-moxie
%{_bindir}/qemu-system-or32
%{_bindir}/qemu-system-ppc
%{_bindir}/qemu-system-ppc64
%{_bindir}/qemu-system-ppcemb
%{_bindir}/qemu-system-s390x
%{_bindir}/qemu-system-sh4
%{_bindir}/qemu-system-sh4eb
%{_bindir}/qemu-system-sparc
%{_bindir}/qemu-system-sparc64
%{_bindir}/qemu-system-tricore
%{_bindir}/qemu-system-unicore32
%{_bindir}/qemu-system-x86_64
%{_bindir}/qemu-system-xtensa
%{_bindir}/qemu-system-xtensaeb
%{_bindir}/qemu-unicore32
%{_bindir}/qemu-x86_64
%{_bindir}/vscclient
%{_includedir}/cacard/*.h
%{_libdir}/libcacard.a
%{_libdir}/libcacard.la
%{_libdir}/libcacard.so*
%{_libdir}/pkgconfig/libcacard.pc
%{_libdir}/qemu/qemu-bridge-helper
%{_datadir}/qemu/*
%{_docdir}/qemu/*
%{_mandir}/man*/*.gz

%clean
rm -rf %{buildroot}

%changelog
* Wed Mar 18 2015 tanggeliang <tanggeliang@gmail.com>
- update from 1.7.0 to 2.2.0
* Fri Dec 6 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.6.1 to 1.7.0
* Tue Oct 22 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.6.0 to 1.6.1
* Thu Oct 3 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.2.0 to 1.6.0
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
