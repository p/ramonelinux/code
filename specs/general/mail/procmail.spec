Name:       procmail
Version:    3.22
Release:    1%{?dist}
Summary:    Procmail

Group:      Applications/Internet
License:    GPLv2+
Url:        http://www.procmail.org
Source:     http://www.ring.gr.jp/archives/net/mail/procmail/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build

%description
The Procmail package contains an autonomous mail processor.
This is useful for filtering and sorting incoming mail.

%prep
%setup -q -n %{name}-%{version}
mkdir -pv /var/mail

%build
sed -i 's/getline/get_line/' src/*.[ch] &&

%check

%install
rm -rf %{buildroot}
make LOCKINGTEST=/tmp MANDIR=%{buildroot}/usr/share/man BASENAME=%{buildroot}/usr install &&
make install-suid BASENAME=%{buildroot}/usr

%files
%defattr(-,root,root,-)
%{_bindir}/formail
%{_bindir}/lockfile
%{_bindir}/mailstat
%{_bindir}/procmail
%{_mandir}/man1/formail.1.gz
%{_mandir}/man1/lockfile.1.gz
%{_mandir}/man1/procmail.1.gz
%{_mandir}/man5/procmailex.5.gz
%{_mandir}/man5/procmailrc.5.gz
%{_mandir}/man5/procmailsc.5.gz

%clean
rm -rf %{buildroot}

%changelog
* Thu Jan 15 2015 tanggeliang <tanggeliang@gmail.com>
- create
