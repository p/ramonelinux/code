Name:       mutt
Version:    1.5.24
Release:    1%{?dist}
Summary:    mutt

Group:      Applications/Internet
License:    GPLv2+
Url:        http://www.mutt.org
Source:     ftp://ftp.mutt.org/pub/mutt/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  openssl gnutls
Requires:       ca-certificates

%description
The Mutt package contains a Mail User Agent.
This is useful for reading, writing, replying to, saving, and deleting your email.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}
mkdir -p /var/mail

%build
./configure --prefix=/usr     \
            --sysconfdir=/etc \
            --with-docdir=/usr/share/doc/mutt-%{version} \
            --enable-pop      \
            --enable-imap     \
            --enable-hcache   \
            --without-qdbm    \
            --with-gdbm       \
            --without-bdb     \
            --without-tokyocabinet &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
/etc/Muttrc
/etc/Muttrc.dist
/etc/mime.types
/etc/mime.types.dist
%{_bindir}/flea
%{_bindir}/mutt
%{_bindir}/muttbug
%{_bindir}/pgpewrap
%{_bindir}/pgpring
%{_bindir}/smime_keys
%{_datadir}/locale/*/LC_MESSAGES/mutt.mo

%files doc
%defattr(-,root,root,-)
%{_docdir}/mutt-%{version}/*
%{_mandir}/man*/*.gz

%post
groupadd -g 34 mail
chgrp -v mail /var/mail

%clean
rm -rf %{buildroot}

%changelog
* Mon Sep 14 2015 tanggeliang <tanggeliang@gmail.com>
- update from 1.5.23 to 1.5.24
* Tue Jan 6 2015 tanggeliang <tanggeliang@gmail.com>
- create
