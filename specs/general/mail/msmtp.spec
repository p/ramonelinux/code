Name:       msmtp
Version:    1.6.2
Release:    1%{?dist}
Summary:    msmtp

Group:      Applications/Internet
License:    GPLv2+
Url:        http://msmtp.sourceforge.net
Source:     http://sourceforge.net/projects/msmtp/files/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  openssl gnutls libidn libsecret

%description
msmtp is an SMTP client with a sendmail compatible interface.
It can be used with Mutt and other mail user agents.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

rm -f %{buildroot}/%{_infodir}/dir

%files
%defattr(-,root,root,-)
%{_bindir}/msmtp
%{_datadir}/locale/de/LC_MESSAGES/msmtp.mo

%files doc
%defattr(-,root,root,-)
%{_infodir}/msmtp.info.gz
%{_mandir}/man1/msmtp.1.gz

%clean
rm -rf %{buildroot}

%changelog
* Mon Sep 14 2015 tanggeliang <tanggeliang@gmail.com>
- update from 1.6.1 to 1.6.2
* Tue Jan 6 2015 tanggeliang <tanggeliang@gmail.com>
- create
