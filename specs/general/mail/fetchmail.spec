Name:       fetchmail
Version:    6.3.26
Release:    1%{?dist}
Summary:    Fetchmail - the mail-retrieval daemon

Group:      Applications/Internet
License:    GPLv2+
Url:        http://fetchmail.sourceforge.net
Source:     http://sourceforge.net/projects/fetchmail/files/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  openssl procmail

%description
The Fetchmail package contains a mail retrieval program.
It retrieves mail from remote mail servers and forwards it to the local (client) machine's delivery system, so it can then be read by normal mail user agents.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --with-ssl --enable-fallback=procmail &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/fetchmail
%{_bindir}/fetchmailconf
/usr/lib/python2.7/site-packages/fetchmailconf.py*
%{_datadir}/locale/*/LC_MESSAGES/fetchmail.mo
%{_mandir}/man1/fetchmail.1.gz
%{_mandir}/man1/fetchmailconf.1.gz

%clean
rm -rf %{buildroot}

%changelog
* Thu Jan 15 2015 tanggeliang <tanggeliang@gmail.com>
- create
