Name:       sane-backends
Version:    1.0.22
Release:    5%{?dist}
Summary:    SANE Back Ends

Group:      System Environment/libraries
License:    GPLv2+
Url:        https://www.sane-project.org
Source:     ftp://ftp2.sane-project.org/pub/sane/sane-backends-1.0.22/%{name}-%{version}.tar.gz
Patch:      sane-backends-1.0.22-v4l-1.patch

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  libjpeg libtiff libusb-compat
#libieee1284 libgphoto2 texlive

%description
SANE is short for Scanner Access Now Easy. Scanner access, however, is far from easy, since every vendor has their own protocols. The only known protocol that should bring some unity into this chaos is the TWAIN interface, but this is too imprecise to allow a stable scanning framework. Therefore, SANE comes with its own protocol, and the vendor drivers can't be used.
SANE is split into back ends and front ends. The back ends are drivers for the supported scanners and cameras. The front ends are user interfaces to access the backends.

%prep
%setup -q -n %{name}-%{version}
%patch -p1

%build
./configure --prefix=/usr        \
            --sysconfdir=/etc    \
            --with-group=scanner \
            --localstatedir=/var &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

mkdir -pv %{buildroot}/etc/udev/rules.d
install -m 644 -v tools/udev/libsane.rules %{buildroot}/etc/udev/rules.d/65-scanner.rules

%files
%defattr(-,root,root,-)
/etc/sane.d/*.conf
/etc/udev/rules.d/65-scanner.rules
%{_bindir}/gamma4scanimage
%{_bindir}/sane-config
%{_bindir}/sane-find-scanner
%{_bindir}/scanimage
%{_includedir}/sane/sane*.h
%{_libdir}/libsane.*
%{_libdir}/sane/libsane-*.la
%{_libdir}/sane/libsane-*.so*
%{_sbindir}/saned
/usr/doc/sane-1.0.22/*
%{_mandir}/man*/*.gz

%clean
rm -rf %{buildroot}

%changelog
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
