Name:       itstool
Version:    2.0.2
Release:    1%{?dist}
Summary:    Itstool

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.itstool.org
Source:     http://files.itstool.org/itstool/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  libxml

%description
Itstool extracts messages from XML files and outputs PO template files, then merges translations from MO files to create translated XML files.
It determines what to translate and how to chunk it into messages using the W3C Internationalization Tag Set (ITS).

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/itstool
%{_datadir}/itstool/its/*.its
%{_mandir}/man1/itstool.1.gz

%clean
rm -rf %{buildroot}

%changelog
* Thu Mar 20 2014 tanggeliang <tanggeliang@gmail.com>
- update from 2.0.0 to 2.0.2
* Wed Dec 4 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.2.0 to 2.0.0
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
