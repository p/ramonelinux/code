Name:       qpdf
Version:    5.0.1
Release:    1%{?dist}
Summary:    Qpdf

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://qpdf.sourceforge.net
Source:     http://downloads.sourceforge.net/qpdf/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  pcre

%description
The Qpdf package contains command-line programs and library that do structural, content-preserving transformations on PDF files.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr    \
            --disable-static \
            --docdir=/usr/share/doc/qpdf-%{version} \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/fix-qdf
%{_bindir}/qpdf
%{_bindir}/zlib-flate
%{_includedir}/qpdf/*.hh
%{_includedir}/qpdf/*.h
%{_libdir}/libqpdf.la
%{_libdir}/libqpdf.so*
%{_libdir}/pkgconfig/libqpdf.pc

%files doc
%defattr(-,root,root,-)
%{_docdir}/qpdf-%{version}/*
%{_mandir}/man1/*.1.gz

%clean
rm -rf %{buildroot}

%changelog
* Thu Dec 5 2013 tanggeliang <tanggeliang@gmail.com>
- update from 5.0.0 to 5.0.1
* Wed Oct 2 2013 tanggeliang <tanggeliang@gmail.com>
- create
