Name:       docbook3
Version:    3.1
Release:    5%{?dist}
Summary:    DocBook SGML DTD

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.docbook.org
Source:     http://www.docbook.org/sgml/3.1/docbk31.zip

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  sgml-common unzip

%description
The DocBook SGML DTD package contains document type definitions for verification of SGML data files against the DocBook rule set.
These are useful for structuring books and software documentation to a standard allowing you to utilize transformations already written for that standard.

%prep
%setup -c -T
unzip %{SOURCE0}

%build
sed -i -e '/ISO 8879/d' \
    -e 's|DTDDECL "-//OASIS//DTD DocBook V3.1//EN"|SGMLDECL|g' \
    docbook.cat

%check

%install
install -v -d -m755 %{buildroot}/usr/share/sgml/docbook/sgml-dtd-3.1 &&
install -v docbook.cat %{buildroot}/usr/share/sgml/docbook/sgml-dtd-3.1/catalog &&
cp -v -af *.dtd *.mod *.dcl %{buildroot}/usr/share/sgml/docbook/sgml-dtd-3.1 &&
install-catalog --add /etc/sgml/sgml-docbook-dtd-3.1.cat \
    %{buildroot}/usr/share/sgml/docbook/sgml-dtd-3.1/catalog &&
install-catalog --add /etc/sgml/sgml-docbook-dtd-3.1.cat \
    %{buildroot}/etc/sgml/sgml-docbook.cat

cat >> %{buildroot}/usr/share/sgml/docbook/sgml-dtd-3.1/catalog << "EOF"
  -- Begin Single Major Version catalog changes --

PUBLIC "-//Davenport//DTD DocBook V3.0//EN" "docbook.dtd"

  -- End Single Major Version catalog changes --
EOF

%files
%defattr(-,root,root,-)
%{_datadir}/sgml/docbook/sgml-dtd-3.1/*.dtd
%{_datadir}/sgml/docbook/sgml-dtd-3.1/catalog
%{_datadir}/sgml/docbook/sgml-dtd-3.1/*.mod
%{_datadir}/sgml/docbook/sgml-dtd-3.1/docbook.dcl

%clean
rm -rf %{buildroot}

%changelog
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
