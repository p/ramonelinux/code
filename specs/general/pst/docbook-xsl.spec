Name:       docbook-xsl
Version:    1.78.1
Release:    1%{?dist}
Summary:    DocBook XSL Stylesheets

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://docbook.sourceforge.net
Source:     http://downloads.sourceforge.net/docbook/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildArch:      noarch
BuildRequires:  libxml
Requires:       libxml

%description
The DocBook XSL Stylesheets package contains XSL stylesheets.
These are useful for performing transformations on XML DocBook files.

%prep
%setup -q -n %{name}-%{version}

%build

%check

%install
rm -rf %{buildroot}
install -v -m755 -d %{buildroot}/usr/share/xml/docbook/xsl-stylesheets-%{version} &&
cp -v -R VERSION common eclipse epub extensions fo highlighting html \
         htmlhelp images javahelp lib manpages params profiling \
         roundtrip slides template tests tools webhelp website \
         xhtml xhtml-1_1 \
    %{buildroot}/usr/share/xml/docbook/xsl-stylesheets-%{version} &&

ln -s VERSION %{buildroot}/usr/share/xml/docbook/xsl-stylesheets-%{version}/VERSION.xsl &&

install -v -m644 -D README \
    %{buildroot}/usr/share/doc/docbook-xsl-%{version}/README.XSL &&
install -v -m755 RELEASE-NOTES* NEWS* \
    %{buildroot}/usr/share/doc/docbook-xsl-%{version}

%files
%defattr(-,root,root,-)
%{_docdir}/docbook-xsl-%{version}/*
%{_datadir}/xml/docbook/xsl-stylesheets-%{version}/*

%post
if [ ! -d /etc/xml ]; then install -v -m755 -d /etc/xml; fi &&
if [ ! -f /etc/xml/catalog ]; then
    xmlcatalog --noout --create /etc/xml/catalog
fi &&

xmlcatalog --noout --add "rewriteSystem" \
           "http://docbook.sourceforge.net/release/xsl/%{version}" \
           "/usr/share/xml/docbook/xsl-stylesheets-%{version}" \
    /etc/xml/catalog &&

xmlcatalog --noout --add "rewriteURI" \
           "http://docbook.sourceforge.net/release/xsl/%{version}" \
           "/usr/share/xml/docbook/xsl-stylesheets-%{version}" \
    /etc/xml/catalog &&

xmlcatalog --noout --add "rewriteSystem" \
           "http://docbook.sourceforge.net/release/xsl/current" \
           "/usr/share/xml/docbook/xsl-stylesheets-%{version}" \
    /etc/xml/catalog &&

xmlcatalog --noout --add "rewriteURI" \
           "http://docbook.sourceforge.net/release/xsl/current" \
           "/usr/share/xml/docbook/xsl-stylesheets-%{version}" \
    /etc/xml/catalog

%postun

%clean
rm -rf %{buildroot}

%changelog
* Mon Jul 22 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.77.0 to 1.78.1
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
