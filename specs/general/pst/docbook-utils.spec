Name:       docbook-utils
Version:    0.6.14
Release:    5%{?dist}
Summary:    DocBook-utils

Group:      Applications/Text
License:    GPLv2+
Url:        http://www.docbook.org
Source:     http://sources-redhat.mirrors.redwire.net/docbook-tools/new-trials/SOURCES/%{name}-%{version}.tar.gz
Patch:      docbook-utils-0.6.14-grep_fix-1.patch

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  texlive openjade docbook-dsssl docbook3 docbook docbook-xml docbook-xsl

%description
The DocBook-utils package is a collection of utility scripts used to convert and analyze SGML documents in general, and DocBook files in particular.
The scripts are used to convert from DocBook or other SGML formats into "classical" file formats like HTML, man, info, RTF and many more.
There's also a utility to compare two SGML files and only display the differences in markup.
This is useful for comparing documents prepared for different languages.

%prep
%setup -q -n %{name}-%{version}
%patch -p1

%build
sed -i 's:/html::' doc/HTML/Makefile.in                &&
./configure --prefix=/usr                              &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot} &&

for doctype in html ps dvi man pdf rtf tex texi txt
do
    ln -s docbook2$doctype %{buildroot}/usr/bin/db2$doctype
done

%files
%defattr(-,root,root,-)

%clean
rm -rf %{buildroot}

%changelog
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
