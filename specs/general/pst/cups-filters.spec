Name:       cups-filters
Version:    1.0.48
Release:    1%{?dist}
Summary:    CUPS Filters

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.openprinting.org
Source:     http://www.openprinting.org/download/cups-filters/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  cups ijs lcms poppler qpdf
BuildRequires:  libjpeg-turbo libpng libtiff

%description
The CUPS Filters package contains backends, filters and other software that was once part of the core CUPS distribution but is no longer maintained by Apple Inc.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr                               \
            --sysconfdir=/etc                           \
            --docdir=%{_docdir}/cups-filters-%{version} \
            --without-rcdir                             \
            --with-gs-path=/usr/bin/gs                  \
            --with-pdftops-path=/usr/bin/gs             \
            --disable-static                            \
            --libdir=%{_libdir}                         &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

rm -rf %{buildroot}/%{_datadir}/cups/*

%files
%defattr(-,root,root,-)
/etc/cups/cups-browsed.conf
/etc/fonts/conf.d/99pdftoopvp.conf
%{_bindir}/ttfread
%{_includedir}/cupsfilters/*.h
%{_includedir}/fontembed/*.h
/usr/lib/cups/backend/*
/usr/lib/cups/filter/*
%{_libdir}/libcupsfilters.la
%{_libdir}/libcupsfilters.so*
%{_libdir}/libfontembed.la
%{_libdir}/libfontembed.so*
%{_libdir}/pkgconfig/libcupsfilters.pc
%{_libdir}/pkgconfig/libfontembed.pc
%{_sbindir}/cups-browsed
%{_datadir}/ppd/cupsfilters/*.ppd

%files doc
%defattr(-,root,root,-)
%{_docdir}/cups-filters-%{version}/*
%{_mandir}/man1/foomatic-rip.1.gz
%{_mandir}/man5/cups-browsed.conf.5.gz
%{_mandir}/man8/cups-browsed.8.gz

%clean
rm -rf %{buildroot}

%changelog
* Thu Mar 20 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.0.42 to 1.0.48
* Sun Dec 15 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.0.41 to 1.0.42
* Thu Dec 5 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.0.38 to 1.0.41
* Wed Oct 2 2013 tanggeliang <tanggeliang@gmail.com>
- create
