Name:       fop
Version:    1.1
Release:    1%{?dist}
Summary:    Formatting Objects Processor

Group:      System Environment/Text
License:    GPLv2+
Url:        http://archive.apache.org
Source:     http://archive.apache.org/dist/xmlgraphics/fop/source/%{name}-%{version}-src.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  libx11 apache-ant

%description
The FOP (Formatting Objects Processor) package contains a print formatter driven by XSL formatting objects (XSL-FO).
It is a Java application that reads a formatting object tree and renders the resulting pages to a specified output.
Output formats currently supported include PDF, PCL, PostScript, SVG, XML (area tree representation), print, AWT, MIF and ASCII text. The primary output target is PDF. 

%prep
%setup -q -n %{name}-%{version}
case `uname -m` in
  i?86)
    tar -xf ../jai-1_1_3-lib-linux-i586.tar.gz
    cp -v jai-1_1_3/lib/{jai*,mlibwrapper_jai.jar} $JAVA_HOME/jre/lib/ext/
    cp -v jai-1_1_3/lib/libmlib_jai.so             $JAVA_HOME/jre/lib/i386/
    ;;

  x86_64)
    tar -xf ../jai-1_1_3-lib-linux-amd64.tar.gz
    cp -v jai-1_1_3/lib/{jai*,mlibwrapper_jai.jar} $JAVA_HOME/jre/lib/ext/
    cp -v jai-1_1_3/lib/libmlib_jai.so             $JAVA_HOME/jre/lib/amd64/
    ;;
esac

%build
ant compile &&
ant jar-main &&
ant javadocs &&
mv build/javadocs .

ant docs

%check

%install
rm -rf %{buildroot}
install -v -d -m755                                     %{buildroot}/usr/share/fop-1.1 &&
cp -v  KEYS LICENSE NOTICE README                       %{buildroot}/usr/share/fop-1.1 &&
cp -va build conf examples fop* javadocs lib status.xml %{buildroot}/usr/share/fop-1.1 &&

ln -v -sf fop-1.1 %{buildroot}/usr/share/fop

%files
%defattr(-,root,root,-)

%clean
rm -rf %{buildroot}

%changelog
* Mon Dec 2 2013 tanggeliang <tanggeliang@gmail.com>
- create
