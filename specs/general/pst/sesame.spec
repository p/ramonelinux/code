Name:       sesame
Version:    2.7.7
Release:    1%{?dist}
Summary:    Sesame

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.openrdf.org
Source:     http://sourceforge.net/projects/sesame/files/Sesame 2/2.7.7/openrdf-%{name}-%{version}-sdk.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
Requires:       openjdk

%description
Java RDF Framework and Database System.

%prep
%setup -q -n openrdf-%{name}-%{version}

%build

%check

%install
rm -rf %{buildroot}
mkdir -pv %{buildroot}/usr/share/jdk
cp -a * %{buildroot}/usr/share/jdk

%files
%defattr(-,root,root,-)
%{_datadir}/jdk/bin/*
%{_datadir}/jdk/docs/*
%{_datadir}/jdk/lib/*
%{_datadir}/jdk/LICENSE.txt
%{_datadir}/jdk/NOTICE.txt
%{_datadir}/jdk/war/*

%clean
rm -rf %{buildroot}

%changelog
* Tue Oct 22 2013 tanggeliang <tanggeliang@gmail.com>
- create
