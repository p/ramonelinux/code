Name:       ijs
Version:    0.35
Release:    1%{?dist}
Summary:    IJS

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.openprinting.org
Source:     http://www.openprinting.org/download/ijs/download/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build

%description
The IJS package contains a library which implements a protocol for transmission of raster page images.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --mandir=/usr/share/man \
            --enable-shared \
            --disable-static \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%ifarch x86_64
mkdir -p %{buildroot}/%{_libdir}/pkgconfig
mv %{buildroot}/usr/lib/pkgconfig/ijs.pc %{buildroot}/%{_libdir}/pkgconfig/ijs.pc
%endif

%files
%defattr(-,root,root,-)
%{_bindir}/ijs-config
%{_bindir}/ijs_client_example
%{_bindir}/ijs_server_example
%{_includedir}/ijs/ijs*.h
%{_libdir}/libijs*.so
%{_libdir}/libijs.la
%{_libdir}/pkgconfig/ijs.pc

%files doc
%defattr(-,root,root,-)
%{_mandir}/man1/ijs-config.1.gz

%clean
rm -rf %{buildroot}

%changelog
* Wed Oct 2 2013 tanggeliang <tanggeliang@gmail.com>
- create
