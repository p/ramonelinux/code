%define snap 20101110

Name:       tidy
Version:    0.99.0
Release:    1%{?dist}
Summary:    HTML Tidy Utility to clean up and pretty print HTML/XHTML/XML

Group:      Applications/Text
License:    W3C
URL:        http://tidy.sourceforge.net
Source:     tidy-cvs_%{snap}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  libtool doxygen libxslt

%description
The HTML Tidy package contains a command line tool and libraries used to read HTML, XHTML and XML files and write cleaned up markup.
It detects and corrects many common coding errors and strives to produce visually equivalent markup that is both W3C compliant and compatible with most browsers.

%prep
%setup -q -n %{name}-cvs_%{snap}

%build
./configure --prefix=/usr --disable-static \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

install -v -m644 -D htmldoc/tidy.1 \
                    %{buildroot}/usr/share/man/man1/tidy.1 &&
install -v -m755 -d %{buildroot}/usr/share/doc/tidy-cvs_20101110 &&
install -v -m644    htmldoc/*.{html,gif,css} \
                    %{buildroot}/usr/share/doc/tidy-cvs_20101110

%clean
rm -rf %{buildroot}

%check

%files
%defattr(-,root,root,-)
%{_bindir}/tab2space
%{_bindir}/tidy
%{_includedir}/*.h
%{_libdir}/libtidy.so
%{_libdir}/libtidy.la
%{_libdir}/libtidy-0.99.so.0*
%{_docdir}/tidy-cvs_%{snap}/*
%{_mandir}/man1/tidy.1.gz

%changelog
* Mon Dec 2 2013 tanggeliang <tanggeliang@gmail.com>
- create
