Name:       cups
Version:    1.7.4
Release:    1%{?dist}
Summary:    Cups

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.cups.com
Source:     http://www.cups.org/software/%{version}/%{name}-%{version}-source.tar.bz2
Patch0:     %{name}-%{version}-blfs-1.patch
Patch1:     %{name}-%{version}-content_type-1.patch

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  colord dbus libusb
BuildRequires:  gnome-menus gnome-icon-theme
BuildRequires:  autoconf automake m4
#Requires:       cups-filters

%description
The Common Unix Printing System (CUPS) is a print spooler and associated utilities.
It is based on the "Internet Printing Protocol" and provides printing services to most PostScript and raster printers.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}
%patch0 -p1
%patch1 -p1

%build
aclocal -I config-scripts &&
autoconf -I config-scripts &&

CC=gcc \
./configure --libdir=%{_libdir} \
            --with-rcdir=/tmp/cupsinit \
            --with-docdir=/usr/share/cups/doc \
            --with-system-groups=lpadmin &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make BUILDROOT=%{buildroot} install

rm -rf %{buildroot}/tmp/cupsinit &&
mkdir -pv %{buildroot}/usr/share/doc
ln -sv ../cups/doc %{buildroot}/usr/share/doc/cups-%{version}

%files
%defattr(-,root,root,-)
%{_bindir}/lp
%{_bindir}/cupstestdsc
%{_bindir}/lpr
%{_bindir}/cupstestppd
%{_bindir}/ppdmerge
%{_bindir}/ipptool
%{_bindir}/ppdc
%{_bindir}/ppdpo
%{_bindir}/ppdhtml
%{_bindir}/cups-config
%{_bindir}/ppdi
%{_bindir}/lpstat
%{_bindir}/lppasswd
%{_bindir}/lpq
%{_bindir}/lpoptions
%{_bindir}/cancel
%{_bindir}/lprm
%{_sbindir}/cupsaccept
%{_sbindir}/cupsd
%{_sbindir}/cupsreject
%{_sbindir}/cupsenable
%{_sbindir}/cupsaddsmb
%{_sbindir}/lpadmin
%{_sbindir}/cupsctl
%{_sbindir}/lpmove
%{_sbindir}/accept
%{_sbindir}/lpc
%{_sbindir}/cupsdisable
%{_sbindir}/reject
%{_sbindir}/cupsfilter
%{_sbindir}/lpinfo
%{_includedir}/cups/*.h
%{_libdir}/libcups*.so*
/usr/lib/cups/*
%{_datadir}/icons/*
%{_datadir}/cups/*
%{_datadir}/locale/*
/etc/cups/*
/etc/dbus-1/*
/etc/pam.d/cups
%dir /var/cache/cups/rss
%dir /var/run/cups/certs
%dir /var/spool/cups/tmp
%dir /var/log/cups

%files doc
%defattr(-,root,root,-)
%{_docdir}/cups-%{version}
%{_mandir}/*

%clean
rm -rf %{buildroot}

%changelog
* Sat Jul 26 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.7.1 to 1.7.4
* Tue Mar 18 2014 tanggeliang <tanggeliang@gmail.com>
- update from 1.7.0 to 1.7.1
* Wed Dec 4 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.6.4 to 1.7.0
* Wed Oct 2 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.6.3 to 1.6.4
* Wed Aug 14 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.6.2 to 1.6.3
* Thu May 30 2013 tanggeliang <tanggeliang@gmail.com>
- update from 1.6.1 to 1.6.2
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
