Name:       opensp
Version:    1.5.2
Release:    5%{?dist}
Summary:    OpenSP

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://openjade.sourceforge.net
Source:     http://downloads.sourceforge.net/openjade/OpenSP-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  sgml-common

%description
The OpenSP package contains a C++ library for using SGML/XML files.
This is useful for validating, parsing and manipulating SGML and XML documents.

%prep
%setup -q -n OpenSP-%{version}

%build
sed -i 's/32,/253,/' lib/Syntax.cxx &&
sed -i 's/LITLEN          240 /LITLEN          8092/' \
    unicode/{gensyntax.pl,unicode.syn} &&
./configure --prefix=/usr                              \
            --disable-static                           \
            --disable-doc-build                        \
            --enable-default-catalog=/etc/sgml/catalog \
            --enable-http                              \
            --enable-default-search-path=/usr/share/sgml \
            --libdir=%{_libdir} &&
make pkgdatadir=/usr/share/sgml/OpenSP-1.5.2 %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make pkgdatadir=/usr/share/sgml/OpenSP-1.5.2 install DESTDIR=%{buildroot} &&
ln -v -sf onsgmls %{buildroot}/usr/bin/nsgmls &&
ln -v -sf osgmlnorm %{buildroot}/usr/bin/sgmlnorm &&
ln -v -sf ospam %{buildroot}/usr/bin/spam &&
ln -v -sf ospcat %{buildroot}/usr/bin/spcat &&
ln -v -sf ospent %{buildroot}/usr/bin/spent &&
ln -v -sf osx %{buildroot}/usr/bin/sx &&
ln -v -sf osx %{buildroot}/usr/bin/sgml2xml &&
ln -v -sf libosp.so %{buildroot}/%{_libdir}/libsp.so

%files
%defattr(-,root,root,-)
%{_bindir}/nsgmls
%{_bindir}/onsgmls
%{_bindir}/osgmlnorm
%{_bindir}/ospam
%{_bindir}/ospcat
%{_bindir}/ospent
%{_bindir}/osx
%{_bindir}/sgml2xml
%{_bindir}/sgmlnorm
%{_bindir}/spam
%{_bindir}/spcat
%{_bindir}/spent
%{_bindir}/sx
%{_includedir}/OpenSP/*.h
%{_includedir}/OpenSP/*.cxx
%{_libdir}/libosp.la
%{_libdir}/libosp.so*
%{_libdir}/libsp.so
%{_docdir}/OpenSP/*
%{_datadir}/locale/*/LC_MESSAGES/sp5.mo
%{_datadir}/sgml/OpenSP-1.5.2/*

%clean
rm -rf %{buildroot}

%changelog
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
