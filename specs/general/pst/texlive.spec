Name:       texlive
Version:    20130530
Release:    1%{?dist}
Summary:    xmlto

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.tug.org/texlive
Source:     ftp://tug.org/texlive/historic/2013/%{name}-%{version}-source.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  poppler freetype fontconfig libpng icu
BuildRequires:  libx11 libxmu libxp libxaw
AutoReq:        no

%description
The TeX Live package is a comprehensive TeX document production system.
It includes TEX, LaTeX2e, ConTEXt, Metafont, MetaPost, BibTeX and many other programs; an extensive collection of macros, fonts and documentation; and support for typesetting in many different scripts from around the world.

%prep
%setup -q -n %{name}-%{version}-source

%build
./configure --prefix=/usr                  \
            --libdir=%{_libdir}            \
            --disable-native-texlive-build \
            --enable-build-in-source-tree  \
            --without-luatex               \
            --enable-mktextex-default      \
            --with-banner-add=" - BLFS"    &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

rm -f %{buildroot}/%{_infodir}/dir

%files
%defattr(-,root,root,-)
%{_bindir}/a2ping
%{_bindir}/a5toa4
%{_bindir}/adhocfilelist
%{_bindir}/afm2afm
%{_bindir}/afm2pl
%{_bindir}/afm2tfm
%{_bindir}/aleph
%{_bindir}/allcm
%{_bindir}/allec
%{_bindir}/allneeded
%{_bindir}/arara
%{_bindir}/arlatex
%{_bindir}/authorindex
%{_bindir}/autoinst
%{_bindir}/bbox
%{_bindir}/bg5+latex
%{_bindir}/bg5+pdflatex
%{_bindir}/bg5conv
%{_bindir}/bg5latex
%{_bindir}/bg5pdflatex
%{_bindir}/biber
%{_bindir}/bibexport
%{_bindir}/bibtex
%{_bindir}/bibtex8
%{_bindir}/bibtexu
%{_bindir}/bundledoc
%{_bindir}/cachepic
%{_bindir}/cef5conv
%{_bindir}/cef5latex
%{_bindir}/cef5pdflatex
%{_bindir}/cefconv
%{_bindir}/ceflatex
%{_bindir}/cefpdflatex
%{_bindir}/cefsconv
%{_bindir}/cefslatex
%{_bindir}/cefspdflatex
%{_bindir}/cfftot1
%{_bindir}/checkcites
%{_bindir}/chktex
%{_bindir}/chkweb
%{_bindir}/context
%{_bindir}/convbkmk
%{_bindir}/ctangle
%{_bindir}/ctanify
%{_bindir}/ctanupload
%{_bindir}/ctie
%{_bindir}/ctxtools
%{_bindir}/cweave
%{_bindir}/de-macro
%{_bindir}/detex
%{_bindir}/devnag
%{_bindir}/deweb
%{_bindir}/disdvi
%{_bindir}/dosepsbin
%{_bindir}/dt2dv
%{_bindir}/dtxgen
%{_bindir}/dv2dt
%{_bindir}/dvi2fax
%{_bindir}/dvi2tty
%{_bindir}/dviasm
%{_bindir}/dvibook
%{_bindir}/dviconcat
%{_bindir}/dvicopy
%{_bindir}/dvidvi
%{_bindir}/dvigif
%{_bindir}/dvihp
%{_bindir}/dvilj
%{_bindir}/dvilj2p
%{_bindir}/dvilj4
%{_bindir}/dvilj4l
%{_bindir}/dvilj6
%{_bindir}/dvipdfm
%{_bindir}/dvipdfmx
%{_bindir}/dvipdft
%{_bindir}/dvipng
%{_bindir}/dvipos
%{_bindir}/dvips
%{_bindir}/dvired
%{_bindir}/dviselect
%{_bindir}/dvisvgm
%{_bindir}/dvitodvi
%{_bindir}/dvitomp
%{_bindir}/dvitype
%{_bindir}/e2pall
%{_bindir}/ebb
%{_bindir}/ebong
%{_bindir}/epsffit
%{_bindir}/epspdf
%{_bindir}/epspdftk
%{_bindir}/epstopdf
%{_bindir}/eptex
%{_bindir}/euptex
%{_bindir}/exceltex
%{_bindir}/extconv
%{_bindir}/extractbb
%{_bindir}/extractres
%{_bindir}/fig4latex
%{_bindir}/findhyph
%{_bindir}/fixdlsrps
%{_bindir}/fixfmps
%{_bindir}/fixpsditps
%{_bindir}/fixpspps
%{_bindir}/fixscribeps
%{_bindir}/fixtpps
%{_bindir}/fixwfwps
%{_bindir}/fixwpps
%{_bindir}/fixwwps
%{_bindir}/fmtutil
%{_bindir}/fmtutil-sys
%{_bindir}/fontinst
%{_bindir}/fragmaster
%{_bindir}/gbklatex
%{_bindir}/gbkpdflatex
%{_bindir}/getafm
%{_bindir}/gftodvi
%{_bindir}/gftopk
%{_bindir}/gftype
%{_bindir}/gsftopk
%{_bindir}/hbf2gf
%{_bindir}/ht
%{_bindir}/htcontext
%{_bindir}/htlatex
%{_bindir}/htmex
%{_bindir}/httex
%{_bindir}/httexi
%{_bindir}/htxelatex
%{_bindir}/htxetex
%{_bindir}/includeres
%{_bindir}/inimf
%{_bindir}/initex
%{_bindir}/installfont-tl
%{_bindir}/kanji-config-updmap
%{_bindir}/kanji-config-updmap-sys
%{_bindir}/kanji-fontmap-creator
%{_bindir}/kpseaccess
%{_bindir}/kpsepath
%{_bindir}/kpsereadlink
%{_bindir}/kpsestat
%{_bindir}/kpsetool
%{_bindir}/kpsewhere
%{_bindir}/kpsewhich
%{_bindir}/kpsexpand
%{_bindir}/lacheck
%{_bindir}/latex2man
%{_bindir}/latexdiff
%{_bindir}/latexdiff-vc
%{_bindir}/latexfileversion
%{_bindir}/latexmk
%{_bindir}/latexpand
%{_bindir}/latexrevise
%{_bindir}/listbib
%{_bindir}/listings-ext.sh
%{_bindir}/ltxfileinfo
%{_bindir}/lua2dox_filter
%{_bindir}/luaotfload-tool
%{_bindir}/luatex
%{_bindir}/luatools
%{_bindir}/m-tx
%{_bindir}/mag
%{_bindir}/makeglossaries
%{_bindir}/makeindex
%{_bindir}/makejvf
%{_bindir}/match_parens
%{_bindir}/mathspic
%{_bindir}/mendex
%{_bindir}/mf
%{_bindir}/mf-nowin
%{_bindir}/mf2pt1
%{_bindir}/mfplain
%{_bindir}/mft
%{_bindir}/mk4ht
%{_bindir}/mkgrkindex
%{_bindir}/mkindex
%{_bindir}/mkjobtexmf
%{_bindir}/mkluatexfontdb
%{_bindir}/mkocp
%{_bindir}/mkofm
%{_bindir}/mkt1font
%{_bindir}/mktexfmt
%{_bindir}/mktexlsr
%{_bindir}/mktexmf
%{_bindir}/mktexpk
%{_bindir}/mktextfm
%{_bindir}/mmafm
%{_bindir}/mmpfb
%{_bindir}/mpost
%{_bindir}/mptopdf
%{_bindir}/mtxrun
%{_bindir}/multibibliography
%{_bindir}/musixflx
%{_bindir}/musixtex
%{_bindir}/odvicopy
%{_bindir}/odvitype
%{_bindir}/ofm2opl
%{_bindir}/omfonts
%{_bindir}/opl2ofm
%{_bindir}/ot2kpx
%{_bindir}/otangle
%{_bindir}/otfinfo
%{_bindir}/otftotfm
%{_bindir}/otp2ocp
%{_bindir}/outocp
%{_bindir}/ovf2ovp
%{_bindir}/ovp2ovf
%{_bindir}/patgen
%{_bindir}/pbibtex
%{_bindir}/pdf180
%{_bindir}/pdf270
%{_bindir}/pdf90
%{_bindir}/pdfannotextractor
%{_bindir}/pdfatfi
%{_bindir}/pdfbook
%{_bindir}/pdfclose
%{_bindir}/pdfcrop
%{_bindir}/pdfflip
%{_bindir}/pdfjam
%{_bindir}/pdfjam-pocketmod
%{_bindir}/pdfjam-slides3up
%{_bindir}/pdfjam-slides6up
%{_bindir}/pdfjoin
%{_bindir}/pdfnup
%{_bindir}/pdfopen
%{_bindir}/pdfpun
%{_bindir}/pdftex
%{_bindir}/pdftosrc
%{_bindir}/pdvitype
%{_bindir}/pedigree
%{_bindir}/perltex
%{_bindir}/pfarrei
%{_bindir}/pfb2pfa
%{_bindir}/pk2bm
%{_bindir}/pkfix
%{_bindir}/pkfix-helper
%{_bindir}/pktogf
%{_bindir}/pktype
%{_bindir}/pltotf
%{_bindir}/pmx2pdf
%{_bindir}/pmxab
%{_bindir}/pooltype
%{_bindir}/ppltotf
%{_bindir}/prepmx
%{_bindir}/ps2eps
%{_bindir}/ps2frag
%{_bindir}/ps2pk
%{_bindir}/ps4pdf
%{_bindir}/psbook
%{_bindir}/pslatex
%{_bindir}/psmerge
%{_bindir}/psnup
%{_bindir}/psresize
%{_bindir}/psselect
%{_bindir}/pst2pdf
%{_bindir}/pstopdf
%{_bindir}/pstops
%{_bindir}/ptex
%{_bindir}/ptex2pdf
%{_bindir}/ptftopl
%{_bindir}/purifyeps
%{_bindir}/repstopdf
%{_bindir}/rpdfcrop
%{_bindir}/rubibtex
%{_bindir}/rumakeindex
%{_bindir}/rungs
%{_bindir}/scor2prt
%{_bindir}/showchar
%{_bindir}/simpdftex
%{_bindir}/sjisconv
%{_bindir}/sjislatex
%{_bindir}/sjispdflatex
%{_bindir}/splitindex
%{_bindir}/sty2dtx
%{_bindir}/svn-multi
%{_bindir}/synctex
%{_bindir}/t1ascii
%{_bindir}/t1asm
%{_bindir}/t1binary
%{_bindir}/t1disasm
%{_bindir}/t1dotlessj
%{_bindir}/t1lint
%{_bindir}/t1mac
%{_bindir}/t1rawafm
%{_bindir}/t1reencode
%{_bindir}/t1testpage
%{_bindir}/t1unmac
%{_bindir}/t4ht
%{_bindir}/tangle
%{_bindir}/teckit_compile
%{_bindir}/tex
%{_bindir}/tex4ht
%{_bindir}/texconfig
%{_bindir}/texconfig-dialog
%{_bindir}/texconfig-sys
%{_bindir}/texcount
%{_bindir}/texdef
%{_bindir}/texdiff
%{_bindir}/texdirflatten
%{_bindir}/texdoc
%{_bindir}/texdoctk
%{_bindir}/texexec
%{_bindir}/texhash
%{_bindir}/texlinks
%{_bindir}/texliveonfly
%{_bindir}/texloganalyser
%{_bindir}/texlua
%{_bindir}/texluac
%{_bindir}/texmfstart
%{_bindir}/tftopl
%{_bindir}/thumbpdf
%{_bindir}/tie
%{_bindir}/tlmgr
%{_bindir}/tpic2pdftex
%{_bindir}/ttf2afm
%{_bindir}/ttf2pk
%{_bindir}/ttf2tfm
%{_bindir}/ttfdump
%{_bindir}/ttftotype42
%{_bindir}/typeoutfileinfo
%{_bindir}/ulqda
%{_bindir}/upbibtex
%{_bindir}/updmap
%{_bindir}/updmap-sys
%{_bindir}/updvitype
%{_bindir}/uppltotf
%{_bindir}/uptex
%{_bindir}/uptftopl
%{_bindir}/urlbst
%{_bindir}/vftovp
%{_bindir}/vlna
%{_bindir}/vpe
%{_bindir}/vpl2ovp
%{_bindir}/vpl2vpl
%{_bindir}/vptovf
%{_bindir}/weave
%{_bindir}/wovp2ovf
%{_bindir}/xdvi
%{_bindir}/xdvi-xaw
%{_bindir}/xdvipdfmx
%{_bindir}/xetex
%{_includedir}/kpathsea/*.h
%{_includedir}/ptexenc/*.h
%{_libdir}/libkpathsea.*a
%{_libdir}/libptexenc.*a
%{_infodir}/*.info.gz
%{_mandir}/man1/*.1.gz
%{_mandir}/man5/*.5.gz
%{_datadir}/texmf-dist/*

%clean
rm -rf %{buildroot}

%changelog
* Mon Jul 22 2013 tanggeliang <tanggeliang@gmail.com>
- create
