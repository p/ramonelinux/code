Name:       sgml-common
Version:    0.6.3
Release:    5%{?dist}
Summary:    SGML Common

Group:      Applications/Text
License:    GPLv2+
Url:        http://www.w3.org/2003/entities/
Source:     http://gd.tuwien.ac.at/hci/kde/devel/docbook/SOURCES/%{name}-%{version}.tgz
Patch:      sgml-common-0.6.3-manpage-1.patch

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  autoconf automake m4

%description
The SGML Common package contains install-catalog.
This is useful for creating and maintaining centralized SGML catalogs.

%prep
%setup -q -n %{name}-%{version}
%patch -p1

%build
autoreconf -f -i
./configure --prefix=/usr --sysconfdir=/etc &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot} &&
install-catalog --add /etc/sgml/sgml-ent.cat \
    %{buildroot}/usr/share/sgml/sgml-iso-entities-8879.1986/catalog &&
install-catalog --add /etc/sgml/sgml-docbook.cat \
    %{buildroot}/etc/sgml/sgml-ent.cat

%files
%defattr(-,root,root,-)
/etc/sgml/sgml.conf
%{_bindir}/install-catalog
%{_bindir}/sgmlwhich
%{_datadir}/sgml/*ml-iso-entities-8879.1986/ISO*.ent
%{_datadir}/sgml/*ml-iso-entities-8879.1986/catalog
%{_datadir}/sgml/xml.dcl
/usr/doc/sgml-common-0.6.3/html/*.html
%{_mandir}/man8/install-catalog.8.gz

%clean
rm -rf %{buildroot}

%changelog
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
