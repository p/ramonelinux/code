Name:       sane-frontends
Version:    1.0.14
Release:    5%{?dist}
Summary:    SANE Front Ends

Group:      System Environment/libraries
License:    GPLv2+
Url:        https://www.sane-project.org
Source:     ftp://ftp2.sane-project.org/pub/sane/sane-frontends-1.0.22/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  sane-backends
BuildRequires:  libx11 gtk2
#gimp

%description
SANE is short for Scanner Access Now Easy. Scanner access, however, is far from easy, since every vendor has their own protocols. The only known protocol that should bring some unity into this chaos is the TWAIN interface, but this is too imprecise to allow a stable scanning framework. Therefore, SANE comes with its own protocol, and the vendor drivers can't be used.
SANE is split into back ends and front ends. The back ends are drivers for the supported scanners and cameras. The front ends are user interfaces to access the backends.

%prep
%setup -q -n %{name}-%{version}

%build
sed -i -e "/SANE_CAP_ALWAYS_SETTABLE/d" src/gtkglue.c &&
./configure --prefix=/usr &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

install -v -m644 doc/sane.png xscanimage-icon-48x48-2.png \
    %{buildroot}/usr/share/sane

mkdir -pv %{buildroot}/usr/lib/gimp/2.0/plug-ins
ln -v -s ../../../../bin/xscanimage %{buildroot}/usr/lib/gimp/2.0/plug-ins

%files
%defattr(-,root,root,-)
%{_bindir}/scanadf
%{_bindir}/xcam
%{_bindir}/xscanimage
%{_libdir}/gimp/2.0/plug-ins/xscanimage
%{_datadir}/sane/sane-style.rc
%{_datadir}/sane/sane.png
%{_datadir}/sane/xscanimage-icon-48x48-2.png
/usr/man/man1/*.1.gz

%clean
rm -rf %{buildroot}

%changelog
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
