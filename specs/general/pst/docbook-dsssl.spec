Name:       docbook-dsssl
Version:    1.79
Release:    5%{?dist}
Summary:    DocBook DSSSL Stylesheets 

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.docbook.org
Source0:    http://downloads.sourceforge.net/docbook/%{name}-%{version}.tar.bz2
Source1:    docbook-dsssl-doc-1.79.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  sgml-common
BuildRequires:  docbook3 docbook opensp openjade

%description
The DocBook DSSSL Stylesheets package contains DSSSL stylesheets.
These are used by OpenJade or other tools to transform SGML and XML DocBook files.

%prep
%setup -q -n %{name}-%{version}

%build
tar -xf %{SOURCE1}

%check

%install
rm -rf %{buildroot}
mkdir -pv %{buildroot}/usr/{bin,share/man/man1}
install -v -m755 bin/collateindex.pl %{buildroot}/usr/bin                      &&
install -v -m644 bin/collateindex.pl.1 %{buildroot}/usr/share/man/man1         &&
install -v -d -m755 %{buildroot}/usr/share/sgml/docbook/dsssl-stylesheets-1.79 &&
cp -v -R * %{buildroot}/usr/share/sgml/docbook/dsssl-stylesheets-1.79          &&

install-catalog --add /etc/sgml/dsssl-docbook-stylesheets.cat \
    %{buildroot}/usr/share/sgml/docbook/dsssl-stylesheets-1.79/catalog         &&

install-catalog --add /etc/sgml/dsssl-docbook-stylesheets.cat \
    %{buildroot}/usr/share/sgml/docbook/dsssl-stylesheets-1.79/common/catalog  &&

install-catalog --add /etc/sgml/sgml-docbook.cat              \
    %{buildroot}/etc/sgml/dsssl-docbook-stylesheets.cat

%files
%defattr(-,root,root,-)
%{_bindir}/collateindex.pl
%{_datadir}/sgml/docbook/dsssl-stylesheets-1.79/*
%{_mandir}/man1/collateindex.pl.1.gz

%clean
rm -rf %{buildroot}

%changelog
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
