Name:       ghostscript
Version:    9.10
Release:    1%{?dist}
Summary:    Ghostscript

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.ghostscript.com
Source:     http://downloads.ghostscript.com/public/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  expat freetype libjpeg-turbo libpng libtiff lcms2
BuildRequires:  cairo fontconfig gtk2 cups lcms libidn libx11

%description
Ghostscript is a versatile processor for PostScript data with the ability to render PostScript to different targets.
It used to be part of the cups printing stack, but is no longer used for that.

%prep
%setup -q -n %{name}-%{version}

%build
rm -rf expat freetype lcms2 jpeg libpng
rm -rf zlib &&
./configure --prefix=/usr --disable-compile-inits \
 --enable-dynamic --with-system-libtiff &&
make %{?_smp_mflags}
make so

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install
make soinstall &&
install -v -m644 base/*.h /usr/include/ghostscript &&
ln -v -s ghostscript /usr/include/ps

%files
%defattr(-,root,root,-)

%clean
rm -rf %{buildroot}

%changelog
* Fri Nov 1 2013 tanggeliang <tanggeliang@gmail.com>
- create
