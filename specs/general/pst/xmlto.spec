Name:       xmlto
Version:    0.0.25
Release:    4%{?dist}
Summary:    xmlto

Group:      System Environment/libraries
License:    GPLv2+
Url:        https://fedorahosted.org/releases/x/m/xmlto
Source:     https://fedorahosted.org/releases/x/m/xmlto/%{name}-%{version}.tar.bz2

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  flex, libxslt, docbook-xml, docbook-xsl

%description
The xmlto is a front-end to an XSL toolchain.
It chooses an appropriate stylesheet for the conversion you want and applies it using an external XSL-T processor.
It also performs any necessary post-processing.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_bindir}/xmlif
%{_bindir}/xmlto
%{_mandir}/*/*
%{_datadir}/xmlto/*

%clean
rm -rf %{buildroot}

%changelog
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
