Name:       linuxdoc-tools
Version:    0.9.66
Release:    5%{?dist}
Summary:    A text formatting package based on SGML

Group:      Applications/Publishing
License:    Copyright only
Url:        http://packages.qa.debian.org/l/linuxdoc-tools.html
Source:     http://http.us.debian.org/debian/pool/main/l/linuxdoc-tools/%{name}_%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  flex sgml-common gawk groff autoconf texinfo opensp m4

%description
Linuxdoc-tools is a text formatting suite based on SGML (Standard Generalized Markup Language), using the LinuxDoc document type.
Linuxdoc-tools allows you to produce LaTeX, HTML, GNU info, LyX, RTF, plain text (via groff), and other format outputs from a single SGML source.
Linuxdoc-tools is intended for writing technical software documentation.

%prep
%setup -q -n %{name}-%{version}

%build
sed -i "75s/^/\/\//" rtf-fix/rtf2rtf.l
./configure --prefix=/usr &&
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

%clean
rm -rf %{buildroot}

%files
%defattr (-,root,root,-)
%{_bindir}/linuxdoc
%{_bindir}/rtf2rtf
%{_bindir}/sgml2html
%{_bindir}/sgml2info
%{_bindir}/sgml2latex
%{_bindir}/sgml2lyx
%{_bindir}/sgml2rtf
%{_bindir}/sgml2txt
%{_bindir}/sgmlcheck
%{_bindir}/sgmlpre
%{_bindir}/sgmlsasp
%{_docdir}/linuxdoc-tools/*
%{_datadir}/entity-map/0.1.0/*
%{_datadir}/linuxdoc-tools/*
%{_datadir}/sgml/iso-entities-8879.1986/ISO*
%{_datadir}/sgml/iso-entities-8879.1986/iso-entities.cat
/usr/man/man1/*.1.gz

%changelog
* Mon Aug 27 2012 tanggeliang <tanggeliang@gmail.com>
- add "sed -i "75s/^/\/\//" rtf-fix/rtf2rtf.l" fix "conflicting types for 'yyleng'"
* Sun May 1 2011 tanggeliang <tanggeliang@gmail.com>
- porting from Fedora
