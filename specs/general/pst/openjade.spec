Name:       openjade
Version:    1.3.2
Release:    5%{?dist}
Summary:    OpenJade

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.docbook.org
Source:     http://downloads.sourceforge.net/openjade/%{name}-%{version}.tar.gz
Patch:      openjade-1.3.2-gcc_4.6-1.patch

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  opensp

%description
The OpenJade package contains a DSSSL engine.
This is useful for SGML and XML transformations into RTF, TeX, SGML and XML.

%prep
%setup -q -n %{name}-%{version}
%patch -p1

%build
sed -i -e '/getopts/{N;s#&G#g#;s#do .getopts.pl.;##;}' \
       -e '/use POSIX/ause Getopt::Std;' msggen.pl
./configure --prefix=/usr                                \
            --enable-http                                \
            --disable-static                             \
            --enable-default-catalog=/etc/sgml/catalog   \
            --enable-default-search-path=/usr/share/sgml \
            --datadir=/usr/share/sgml/openjade-1.3.2   &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}                                          &&
make install-man DESTDIR=%{buildroot}                                      &&
ln -v -sf openjade %{buildroot}/usr/bin/jade                               &&
ln -v -sf libogrove.so %{buildroot}/usr/lib/libgrove.so                    &&
ln -v -sf libospgrove.so %{buildroot}/usr/lib/libspgrove.so                &&
ln -v -sf libostyle.so %{buildroot}/usr/lib/libstyle.so                    &&

install -v -m644 dsssl/catalog %{buildroot}/usr/share/sgml/openjade-1.3.2/ &&

install -v -m644 dsssl/*.{dtd,dsl,sgm}              \
    %{buildroot}/usr/share/sgml/openjade-1.3.2                             &&

install-catalog --add /etc/sgml/openjade-1.3.2.cat  \
    %{buildroot}/usr/share/sgml/openjade-1.3.2/catalog                     &&

install-catalog --add /etc/sgml/sgml-docbook.cat    \
    %{buildroot}/etc/sgml/openjade-1.3.2.cat

%files
%defattr(-,root,root,-)
%{_bindir}/jade
%{_bindir}/openjade
%{_libdir}/libgrove.so
%{_libdir}/libogrove.*
%{_libdir}/libospgrove.*
%{_libdir}/libostyle.*
%{_libdir}/libspgrove.so
%{_libdir}/libstyle.so
%{_datadir}/sgml/openjade-1.3.2/*
/usr/man/man1/openjade.1.gz

%clean
rm -rf %{buildroot}

%changelog
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
