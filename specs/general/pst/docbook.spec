Name:       docbook
Version:    4.5
Release:    5%{?dist}
Summary:    DocBook SGML DTD

Group:      System Environment/libraries
License:    GPLv2+
Url:        http://www.docbook.org
Source:     http://www.docbook.org/sgml/4.5/%{name}-%{version}.zip

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  sgml-common unzip

%description
The DocBook SGML DTD package contains document type definitions for verification of SGML data files against the DocBook rule set.
These are useful for structuring books and software documentation to a standard allowing you to utilize transformations already written for that standard.

%prep
%setup -c -T
unzip %{SOURCE0}

%build
sed -i -e '/ISO 8879/d' \
    -e '/gml/d' docbook.cat

%check

%install
install -v -d %{buildroot}/usr/share/sgml/docbook/sgml-dtd-4.5 &&
#chown -R root:root . &&
install -v docbook.cat %{buildroot}/usr/share/sgml/docbook/sgml-dtd-4.5/catalog &&
cp -v -af *.dtd *.mod *.dcl %{buildroot}/usr/share/sgml/docbook/sgml-dtd-4.5 &&
install-catalog --add /etc/sgml/sgml-docbook-dtd-4.5.cat \
    %{buildroot}/usr/share/sgml/docbook/sgml-dtd-4.5/catalog &&
install-catalog --add /etc/sgml/sgml-docbook-dtd-4.5.cat \
    %{buildroot}/etc/sgml/sgml-docbook.cat

cat >> %{buildroot}/usr/share/sgml/docbook/sgml-dtd-4.5/catalog << "EOF"
  -- Begin Single Major Version catalog changes --

PUBLIC "-//OASIS//DTD DocBook V4.4//EN" "docbook.dtd"
PUBLIC "-//OASIS//DTD DocBook V4.3//EN" "docbook.dtd"
PUBLIC "-//OASIS//DTD DocBook V4.2//EN" "docbook.dtd"
PUBLIC "-//OASIS//DTD DocBook V4.1//EN" "docbook.dtd"
PUBLIC "-//OASIS//DTD DocBook V4.0//EN" "docbook.dtd"

  -- End Single Major Version catalog changes --
EOF

%files
%defattr(-,root,root,-)
%{_datadir}/sgml/docbook/sgml-dtd-4.5/*.dcl
%{_datadir}/sgml/docbook/sgml-dtd-4.5/*.dtd
%{_datadir}/sgml/docbook/sgml-dtd-4.5/*.mod
%{_datadir}/sgml/docbook/sgml-dtd-4.5/catalog

%clean
rm -rf %{buildroot}

%changelog
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
