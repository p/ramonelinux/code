Name:       dosfstools
Version:    3.0.27
Release:    1%{?dist}
Summary:    utilities for making and checking MS-DOS FAT filesystems

Group:      System Environment/Filesystem
License:    GPLv3+
Url:        http://daniel-baumann.ch/software/dosfstools
Source:     http://daniel-baumann.ch/software/dosfstools/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build

%description
The dosfstools package includes the mkfs.fat (aka mkfs.vfat and mkfs.msdos) and fsck.fat (aka fsck.vfat and fsck.msdos) utilities, which respectively make and check MS-DOS FAT filesystems.

%prep
%setup -q -n %{name}-%{version}

%build
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install-bin install-man PREFIX=%{_prefix} SBINDIR=/sbin

%files
%defattr(-,root,root,-)
/sbin/fatlabel
/sbin/fsck.fat
/sbin/mkfs.fat
%{_mandir}/de/man8/fatlabel.8.gz
%{_mandir}/de/man8/fsck.fat.8.gz
%{_mandir}/de/man8/mkfs.fat.8.gz
%{_mandir}/man8/fatlabel.8.gz
%{_mandir}/man8/fsck.fat.8.gz
%{_mandir}/man8/mkfs.fat.8.gz

%clean
rm -rf %{buildroot}

%changelog
* Mon Aug 3 2015 tanggeliang <tanggeliang@gmail.com>
- create
