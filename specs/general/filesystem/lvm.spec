Name:       lvm
Version:    2.02.132
Release:    1%{?dist}
Summary:    lvm2

Group:      System Environment/Filesystem
License:    GPLv2+
Url:        http://www.lvm2.com
Source:     ftp://sources.redhat.com/pub/lvm2/LVM2.%{version}.tgz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  kmod systemd-udev systemd-libudev

%description
The lvm2 package is a package that manages logical partitions.
It allows spanning of file systems across multiple physical disks and disk partitions and provides for dynamic growing or shrinking of logical partitions.

%prep
%setup -q -n LVM2.%{version}

%build
./configure --prefix=/usr       \
            --exec-prefix=      \
            --with-confdir=/etc \
            --enable-applib     \
            --enable-cmdlib     \
            --enable-pkgconfig  \
            --enable-udev_sync  \
            --libdir=/%{_lib}   \
            --with-usrlibdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
/etc/lvm/lvm.conf
/etc/lvm/lvmlocal.conf
/etc/lvm/profile/*.profile
/%{_lib}/libdevmapper.so.1.02
/%{_lib}/liblvm2app.so.2.2
/%{_lib}/liblvm2cmd.so.2.02
/lib/udev/rules.d/10-dm.rules
/lib/udev/rules.d/11-dm-lvm.rules
/lib/udev/rules.d/13-dm-disk.rules
/lib/udev/rules.d/95-dm-notify.rules
/sbin/blkdeactivate
/sbin/dmsetup
/sbin/dmstats
/sbin/fsadm
/sbin/lvchange
/sbin/lvconvert
/sbin/lvcreate
/sbin/lvdisplay
/sbin/lvextend
/sbin/lvm
/sbin/lvmchange
/sbin/lvmconf
/sbin/lvmconfig
/sbin/lvmdiskscan
/sbin/lvmdump
/sbin/lvmsadc
/sbin/lvmsar
/sbin/lvreduce
/sbin/lvremove
/sbin/lvrename
/sbin/lvresize
/sbin/lvs
/sbin/lvscan
/sbin/pvchange
/sbin/pvck
/sbin/pvcreate
/sbin/pvdisplay
/sbin/pvmove
/sbin/pvremove
/sbin/pvresize
/sbin/pvs
/sbin/pvscan
/sbin/vgcfgbackup
/sbin/vgcfgrestore
/sbin/vgchange
/sbin/vgck
/sbin/vgconvert
/sbin/vgcreate
/sbin/vgdisplay
/sbin/vgexport
/sbin/vgextend
/sbin/vgimport
/sbin/vgimportclone
/sbin/vgmerge
/sbin/vgmknodes
/sbin/vgreduce
/sbin/vgremove
/sbin/vgrename
/sbin/vgs
/sbin/vgscan
/sbin/vgsplit
%{_includedir}/libdevmapper.h
%{_includedir}/lvm2app.h
%{_includedir}/lvm2cmd.h
%{_libdir}/libdevmapper.so
%{_libdir}/liblvm2app.so
%{_libdir}/liblvm2cmd.so
%{_libdir}/pkgconfig/devmapper.pc
%{_libdir}/pkgconfig/lvm2app.pc
%{_mandir}/man5/*.5.gz
%{_mandir}/man7/*.7.gz
%{_mandir}/man8/*.8.gz

%clean
rm -rf %{buildroot}

%changelog
* Wed Oct 28 2015 tanggeliang <tanggeliang@gmail.com>
- update from 2.02.107 to 2.02.132
* Sun Jul 27 2014 tanggeliang <tanggeliang@gmail.com>
- update from 2.02.98 to 2.02.107
* Mon Apr 1 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.02.97 to 2.02.98
* Thu Aug 30 2012 tanggeliang <tanggeliang@gmail.com>
- add --enable-dmeventd enable devmapper-event.
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
