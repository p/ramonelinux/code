Name:       mdadm
Version:    3.3.4
Release:    1%{?dist}
Summary:    administration tools for software RAID

Group:      Development/Libraries
License:    BSD and LGPLv2+
Url:        http://neil.brown.name/blog/mdadm
Source:     http://www.kernel.org/pub/linux/utils/raid/mdadm/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build

%description
The mdadm package contains administration tools for software RAID.

%prep
%setup -q -n %{name}-%{version}

%build
make CHECK_RUN_DIR=0 %{?_smp_mflags}

%install
rm -rf %{buildroot}
make CHECK_RUN_DIR=0 DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
/lib/udev/rules.d/63-md-raid-arrays.rules
/lib/udev/rules.d/64-md-raid-assembly.rules
/sbin/mdadm
/sbin/mdmon
%{_mandir}/man*/md*.*.gz

%changelog
* Tue Oct 27 2015 tanggeliang <tanggeliang@gmail.com>
- update from 3.3 to 3.3.4
* Thu Mar 13 2014 tanggeliang <tanggeliang@gmail.com>
- create
