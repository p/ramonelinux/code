Name:       xfsprogs
Version:    3.2.1
Release:    1%{?dist}
Summary:    XFS progs

Group:      System Environment/Filesystem
License:    GPLv2+
Url:        http://www.xfsprogs.com
Source:     http://pkgs.fedoraproject.org/repo/pkgs/xfsprogs/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  gettext

%description
The xfsprogs package contains administration and debugging tools for the XFS file system.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
make DEBUG=-DNDEBUG INSTALL_USER=root INSTALL_GROUP=root \
    LOCAL_CONFIGURE_OPTIONS="--enable-readline" %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make install DIST_ROOT=%{buildroot} &&
make install-dev DIST_ROOT=%{buildroot} &&
chmod -v 755 %{buildroot}/%{_lib}/libhandle.so.1.*

%files
%defattr(-,root,root,-)
/%{_lib}/libhandle.*
/sbin/fsck.xfs
/sbin/mkfs.xfs
/sbin/xfs_repair
%{_includedir}/xfs/*.h
%{_libdir}/libhandle.*
%{_sbindir}/xfs_admin
%{_sbindir}/xfs_bmap
%{_sbindir}/xfs_copy
%{_sbindir}/xfs_db
%{_sbindir}/xfs_estimate
%{_sbindir}/xfs_freeze
%{_sbindir}/xfs_fsr
%{_sbindir}/xfs_growfs
%{_sbindir}/xfs_info
%{_sbindir}/xfs_io
%{_sbindir}/xfs_logprint
%{_sbindir}/xfs_mdrestore
%{_sbindir}/xfs_metadump
%{_sbindir}/xfs_mkfile
%{_sbindir}/xfs_ncheck
%{_sbindir}/xfs_quota
%{_sbindir}/xfs_rtcp
%{_datadir}/locale/*/LC_MESSAGES/xfsprogs.mo

%files doc
%defattr(-,root,root,-)
%{_docdir}/xfsprogs/*
%{_mandir}/man*/*.gz

%clean
rm -rf %{buildroot}

%changelog
* Sat Jul 26 2014 tanggeliang <tanggeliang@gmail.com>
- update from 3.1.8 to 3.2.1
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
