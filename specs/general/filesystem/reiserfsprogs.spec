Name:       reiserfsprogs
Version:    3.6.24
Release:    1%{?dist}
Summary:    reiserfs progs

Group:      System Environment/Filesystem
License:    GPLv2+
Url:        http://www.reiserfsprogs.com
Source:     http://ftp.kernel.org/pub/linux/kernel/people/jeffm/reiserfsprogs/v3.6.24/%{name}-%{version}.tar.xz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build

%description
The reiserfsprogs package contains various utilities for use with the Reiser file system.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --sbindir=/sbin &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
/sbin/debugreiserfs
/sbin/debugfs.reiserfs
/sbin/fsck.reiserfs
/sbin/mkfs.reiserfs
/sbin/tunefs.reiserfs
/sbin/mkreiserfs
/sbin/reiserfsck
/sbin/reiserfstune
/sbin/resize_reiserfs
%{_mandir}/man8/*reiserfs*.8.gz

%clean
rm -rf %{buildroot}

%changelog
* Thu Oct 3 2013 tanggeliang <tanggeliang@gmail.com>
- update from 3.6.21 to 3.6.24
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
