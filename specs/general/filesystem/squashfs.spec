Name:       squashfs
Version:    4.3
Release:    1%{?dist}
Summary:    Squashfs tools

Group:      System Environment/Filesystem
License:    GPLv2+
Url:        https://squashfs.sourceforge.net
Source:     http://cdnetworks-kr-2.dl.sourceforge.net/project/squashfs/squashfs/squashfs4.2/%{name}%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build

%description
Squashfs is a compressed read-only filesystem for Linux.
Squashfs is intended for general read-only filesystem use, for archival use (i.e. in cases where a .tar.gz file may be used), and in constrained block device/memory systems (e.g. embedded systems) where low overhead is needed. 

%prep
%setup -q -n %{name}%{version}

%build
cd %{name}-tools
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
cd %{name}-tools
make INSTALL_DIR=%{buildroot}/usr/bin install

%files
%defattr(-,root,root,-)
%{_bindir}/mksquashfs
%{_bindir}/unsquashfs

%clean
rm -rf %{buildroot}

%changelog
* Fri Mar 27 2015 tanggeliang <tanggeliang@gmail.com>
- update from 4.2 to 4.3
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
