Name:       fuse
Version:    2.9.3
Release:    1%{?dist}
Summary:    Fuse

Group:      System Environment/Filesystem
License:    GPLv2+
Url:        http://downloads.sourceforge.net/fuse
Source:     http://downloads.sourceforge.net/fuse/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build

%description
FUSE (Filesystem in Userspace) is a simple interface for userspace programs to export a virtual filesystem to the Linux kernel.
Fuse also aims to provide a secure method for non privileged users to create and mount their own filesystem implementations.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --disable-static INIT_D_PATH=/tmp/init.d \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

mkdir %{buildroot}/%{_lib}
mv -v %{buildroot}/%{_libdir}/libfuse.so.* %{buildroot}/%{_lib} &&
ln -sfv ../../%{_lib}/libfuse.so.%{version} %{buildroot}/%{_libdir}/libfuse.so &&
rm -rf %{buildroot}/tmp/init.d

cat > %{buildroot}/etc/fuse.conf << "EOF"
# Set the maximum number of FUSE mounts allowed to non-root users.
# The default is 1000.
#
#mount_max = 1000

# Allow non-root users to specify the 'allow_other' or 'allow_root'
# mount options.
#
#user_allow_other
EOF

%files
%defattr(-,root,root,-)
/etc/fuse.conf
/etc/udev/rules.d/99-fuse.rules
/%{_lib}/libfuse.so.2*
/sbin/mount.fuse
%{_bindir}/fusermount
%{_bindir}/ulockmgr_server
%{_includedir}/fuse.h
%{_includedir}/ulockmgr.h
%{_includedir}/fuse/*.h
%{_libdir}/libfuse.*
%{_libdir}/libulockmgr.*
%{_libdir}/pkgconfig/fuse.pc

%files doc
%defattr(-,root,root,-)
%{_mandir}/man*/*

%clean
rm -rf %{buildroot}

%changelog
* Sat Aug 24 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2.9.2 to 2.9.3
* Sat Nov 3 2012 tanggeliang <tanggeliang@gmail.com>
- update from 2.9.1 to 2.9.2
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
