Name:       ntfs-3g
Version:    2014.2.15
Release:    1%{?dist}
Summary:    Ntfs-3g

Group:      System Environment/Filesystem
License:    GPLv2+
Url:        http://tuxera.com/opensource
Source:     http://tuxera.com/opensource/%{name}_ntfsprogs-%{version}.tgz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  fuse

%description
The Ntfs-3g package contains an open source, driver for Windows NTFS file system.
This can mount Windows partitions so that they are writeable and allows you edit or delete Windows files from Linux.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}_ntfsprogs-%{version}

%build
./configure --prefix=/usr --disable-static \
            --libdir=%{_libdir} &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
/bin/lowntfs-3g
/bin/ntfs-3g
/%{_lib}/libntfs-3g.so*
/sbin/mkfs.ntfs
/sbin/mount.lowntfs-3g
/sbin/mount.ntfs-3g
%{_bindir}/ntfs-3g.probe
%{_bindir}/ntfs-3g.secaudit
%{_bindir}/ntfs-3g.usermap
%{_bindir}/ntfscat
%{_bindir}/ntfscluster
%{_bindir}/ntfscmp
%{_bindir}/ntfsfix
%{_bindir}/ntfsinfo
%{_bindir}/ntfsls
%{_includedir}/ntfs-3g/*.h
%{_libdir}/libntfs-3g.*
%{_libdir}/pkgconfig/libntfs-3g.pc
%{_sbindir}/mkntfs
%{_sbindir}/ntfsclone
%{_sbindir}/ntfscp
%{_sbindir}/ntfslabel
%{_sbindir}/ntfsresize
%{_sbindir}/ntfsundelete

%files doc
%defattr(-,root,root,-)
%{_docdir}/ntfs-3g/README
%{_mandir}/man8/*.8.gz

%post
ln -sv ../bin/ntfs-3g /sbin/mount.ntfs &&
chmod 4755 /sbin/mount.ntfs

%postun
rm -rf /sbin/mount.ntfs

%post doc
ln /usr/share/man/man8/{ntfs-3g,mount.ntfs}.8.gz

%postun doc
rm -rf /usr/share/man/man8/mount.ntfs.8.gz

%clean
rm -rf %{buildroot}

%changelog
* Thu Mar 13 2014 tanggeliang <tanggeliang@gmail.com>
- update from 2013.1.13 to 2014.2.15
* Mon Apr 1 2013 tanggeliang <tanggeliang@gmail.com>
- update from 2012.1.15 to 2013.1.13
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
