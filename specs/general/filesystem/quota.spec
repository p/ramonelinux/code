Name:       quota
Version:    4.02
Release:    1%{?dist}
Summary:    Linux DiskQuota

Group:      Development/Libraries
License:    BSD and LGPLv2+
Url:        http://linuxquota.sourceforge.net
Source:     http://sourceforge.net/projects/linuxquota/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  libtirpc gettext

%description
Tools and patches for the Linux Diskquota system as part of the Linux kernel.

%prep
%setup -q -n %{name}-tools

%build
export LDFLAGS+=-ltirpc
./configure --prefix=/usr \
            --sysconfdir=/etc \
            --enable-strip-binaries=no &&
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
make ROOTDIR=%{buildroot} install

rm -rf %{buildroot}/%{_includedir}/rpcsvc/rquota.h
rm -rf %{buildroot}/%{_includedir}/rpcsvc/rquota.x

%files
%defattr(-,root,root,-)
/etc/quotagrpadmins
/etc/quotatab
/etc/warnquota.conf
%{_bindir}/quota
%{_bindir}/quotasync
%{_sbindir}/convertquota
%{_sbindir}/edquota
%{_sbindir}/quot
%{_sbindir}/quotacheck
%{_sbindir}/quotaoff
%{_sbindir}/quotaon
%{_sbindir}/quotastats
%{_sbindir}/repquota
%{_sbindir}/rpc.rquotad
%{_sbindir}/setquota
%{_sbindir}/warnquota
%{_sbindir}/xqmstats
%{_datadir}/locale/*/LC_MESSAGES/quota.mo
%{_mandir}/man*/*.gz

%changelog
* Fri Mar 27 2015 tanggeliang <tanggeliang@gmail.com>
- update from 4.01 to 4.02
* Thu Mar 13 2014 tanggeliang <tanggeliang@gmail.com>
- create
