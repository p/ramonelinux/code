Name:       glusterfs
Version:    3.6.2
Release:    1%{?dist}
Summary:    GlusterFS

Group:      Development/Libraries
License:    BSD and LGPLv2+
Url:        http://www.gluster.org
Source:     http://www.gluster.org/download/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build
BuildRequires:  bison flex
BuildRequires:  fuse systemd

%description
GlusterFS is an open source, distributed file system capable of scaling to several petabytes (actually, 72 brontobytes!) and handling thousands of clients.
GlusterFS clusters together storage building blocks over Infiniband RDMA or TCP/IP interconnect, aggregating disk and memory resources and managing data in a single global namespace.
GlusterFS is based on a stackable user space design and can deliver exceptional performance for diverse workloads.

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr \
            --sysconfdir=/etc \
	    --localstatedir=/var \
            --libdir=%{_libdir} \
            --libexecdir=%{_libdir} \
            --with-systemddir=/lib/systemd/system &&
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
/etc/glusterfs/gluster-rsyslog-5.8.conf
/etc/glusterfs/gluster-rsyslog-7.2.conf
/etc/glusterfs/glusterd.vol
/etc/glusterfs/glusterfs-georep-logrotate
/etc/glusterfs/glusterfs-logrotate
/etc/glusterfs/group-virt.example
/etc/glusterfs/logger.conf.example
/sbin/mount.glusterfs
%{_bindir}/fusermount-glusterfs
%{_includedir}/glusterfs/api/glfs*.h
%{_includedir}/glusterfs/gfchangelog/changelog.h
%{_libdir}/glusterfs/%{version}/auth/addr.*
%{_libdir}/glusterfs/%{version}/auth/login.*
%{_libdir}/glusterfs/%{version}/rpc-transport/socket.*
%{_libdir}/glusterfs/%{version}/xlator/*
%{_libdir}/libgfapi.la
%{_libdir}/libgfapi.so*
%{_libdir}/libgfchangelog.*
%{_libdir}/libgfrpc.la
%{_libdir}/libgfrpc.so*
%{_libdir}/libgfxdr.la
%{_libdir}/libgfxdr.so*
%{_libdir}/libglusterfs.la
%{_libdir}/libglusterfs.so*
/usr/lib/ocf/resource.d/glusterfs/glusterd
/usr/lib/ocf/resource.d/glusterfs/volume
%{_libdir}/pkgconfig/glusterfs-api.pc
%{_libdir}/pkgconfig/libgfchangelog.pc
%{_libdir}/glusterfs/gsyncd
%{_libdir}/glusterfs/python/syncdaemon/*
%{_libdir}/glusterfs/gverify.sh
%{_libdir}/glusterfs/peer_add_secret_pub
%{_libdir}/glusterfs/peer_gsec_create
%{_libdir}/glusterfs/set_geo_rep_pem_keys.sh
/usr/lib/python2.7/site-packages/gluster/*.py*
%{_sbindir}/glfsheal
%{_sbindir}/gluster
%{_sbindir}/glusterd
%{_sbindir}/glusterfs
%{_sbindir}/glusterfsd
%{_datadir}/glusterfs/scripts/*.sh
%{_datadir}/glusterfs/scripts/gsync-sync-gfid
%{_docdir}/glusterfs/*
%{_mandir}/man8/*.8.gz
/lib/systemd/system/glusterd.service
/var/lib/glusterd/groups/virt

%changelog
* Fri Mar 27 2015 tanggeliang <tanggeliang@gmail.com>
- update from 3.4.2 to 3.6.2
* Thu Mar 13 2014 tanggeliang <tanggeliang@gmail.com>
- create
