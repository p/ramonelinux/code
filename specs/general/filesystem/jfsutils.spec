Name:       jfsutils
Version:    1.1.15
Release:    6%{?dist}
Summary:    jfs utils

Group:      System Environment/Filesystem
License:    GPLv2+
Url:        http://jfs.sourceforge.net
Source:     jfs.sourceforge.net/project/pub/%{name}-%{version}.tar.gz

Distribution:   ramone linux
Vendor:         ramone
Packager:       tanggeliang <tanggeliang@gmail.com>

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-build

%description
The jfsutils package contains administration and debugging tools for the jfs file system.

%package        doc
Summary:        Documentation
BuildArch:      noarch
%description    doc

%prep
%setup -q -n %{name}-%{version}

%build
sed "s@<unistd.h>@&\n#include <sys/types.h>@g" -i fscklog/extract.c &&
./configure &&
make %{?_smp_mflags}

%check

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
/sbin/fsck.jfs
/sbin/jfs_debugfs
/sbin/jfs_fsck
/sbin/jfs_fscklog
/sbin/jfs_logdump
/sbin/jfs_mkfs
/sbin/jfs_tune
/sbin/mkfs.jfs

%files doc
%defattr(-,root,root,-)
%{_mandir}/man8/*.8.gz

%clean
rm -rf %{buildroot}

%changelog
* Fri Apr 30 2010 tanggeliang <tanggeliang@gmail.com>
- create
