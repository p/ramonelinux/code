#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/select.h>
#include <netinet/in.h>
#include <netdb.h>

int ret_status(char *recv_buf)
{
	int http_status = 0;
	char *rest = strstr(recv_buf, "\r\n");
	if(rest != NULL) {
		char line[256] = {0};
		memcpy(line, recv_buf, rest - recv_buf);
		if(strstr(line, "200")) /* success */
			http_status = 200;
		else
			http_status = -1;
	}
	return http_status;
}

long ret_filesize(char *recv_buf)
{
	if(strstr(recv_buf, "Content-Length") == NULL)
		exit(EXIT_FAILURE);
	char *rest = strstr(recv_buf, "Content-Length:") + strlen("Content-Length: ");
	char *line = strstr(rest, "\r\n");
	char actual_size[BUFSIZ] = {0};
	memcpy(actual_size, rest, line-rest);
	return atoi(actual_size);
}

int parse_url(char *url, char domain[], char **file_name)
{
	char *url_dress = url + strlen("http://");
	char *file_dress = strchr(url_dress, '/');
	if(file_dress) {
		memcpy(domain, url_dress, file_dress - url_dress);
	}
	char *p = file_dress;
	*file_name = ++p;
	return 0;
}

int send_request(int sockfd, char *file_name, char *domain)
{
	char *request = (char *)malloc(BUFSIZ);
	sprintf(request, "GET /%s HTTP/1.1\r\nHost:%s\r\n\
			Connection: keep-alive\r\n\r\n", file_name, domain);
	int totalsend = 0;
	int nbytes = strlen(request);
	while(totalsend < nbytes) {
		write(sockfd, request + totalsend, nbytes - totalsend);
		totalsend += nbytes;
		printf("%d bytes send OK!\n", totalsend);
	}
	free(request);
}

int download(int sockfd, char *file_name)
{
	FILE *fp = NULL;
	long filesize = 0;
	int headmark = 1;
	int count = 0;
	int nbytes;
	char buffer[BUFSIZ];

	while((nbytes = recv(sockfd, buffer, BUFSIZ, 0)) > 0) {
		if(headmark == 1) { /* only once */
			char *head_buffer = strstr(buffer, "\r\n\r\n") + 4;
			char header[BUFSIZ] = {0};
			memcpy(header, buffer, head_buffer - buffer);
			printf("%s", header);
			filesize = ret_filesize(header);
			if(ret_status(header) == 200) {
				fp = fopen(file_name, "w");
				count = fwrite(head_buffer, 1, nbytes - (head_buffer - buffer), fp);
			}
			headmark = 0;
		} else {
			fwrite(buffer, 1, nbytes, fp);
			fflush(fp);
			count += nbytes;
		}

		if(count == filesize) {
			break;
		}
	}
	printf("total: %d\n", count);

	fclose(fp);
}

int main(int argc, char *argv[])
{
	char domain[256] = {0};
	char *file_name;

	if(argc != 2)
		return EXIT_FAILURE;

	parse_url(argv[1], domain, &file_name);

	int sockfd = socket(AF_INET, SOCK_STREAM, 0);

	struct sockaddr_in server_addr;
	bzero(&server_addr, sizeof(server_addr));
	server_addr.sin_family = AF_INET;
	server_addr.sin_port = htons(80);
	struct hostent *host = gethostbyname(domain);
	server_addr.sin_addr = *((struct in_addr *)host->h_addr);
	if(connect(sockfd, (struct sockaddr *)(&server_addr), sizeof(struct sockaddr)) < 0) {
		perror("connect error");
		exit(EXIT_FAILURE);
	}

	send_request(sockfd, file_name, domain);
	download(sockfd, file_name);

	close(sockfd);
	return 0; 
}
