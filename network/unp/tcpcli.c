#include "unp.h"

static void str_cli(int sockfd)
{
	char sendline[MAXLINE], recvline[MAXLINE];

	while (fgets(sendline, MAXLINE, stdin) != NULL) {
		debug("send to server: %s", sendline);
		if (write(sockfd, sendline, strlen(sendline)) < 0)
			err_sys("write");

		if (readline(sockfd, recvline, MAXLINE) == 0)
			err_sys("readline");
		debug("read from server: %s", recvline);

		fputs(recvline, stdout);
	}
}

int main(int argc, char *argv[])
{
	int sockfd;
	struct sockaddr_in servaddr;

	if (argc != 2) {
		printf("usage: tcpcli <IPaddress>\n");
		exit(1);
	}

	sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if (sockfd == -1)
		err_sys("socket");
	debug("sockfd = %d\n", sockfd);

	bzero(&servaddr, sizeof(servaddr));
	servaddr.sin_family = AF_INET;
	servaddr.sin_port = htons(SERV_PORT);
	inet_pton(AF_INET, argv[1], &servaddr.sin_addr);

	if (connect(sockfd, (SA *)&servaddr, sizeof(servaddr)) == -1)
		err_sys("connect");

	str_cli(sockfd);		/* do it all */
	exit(0);
}
