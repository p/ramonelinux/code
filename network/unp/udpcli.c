#include "unp.h"

static void str_cli(int sockfd, const SA *pservaddr, socklen_t servlen)
{
	int n;
	char sendline[MAXLINE], recvline[MAXLINE];

	while (fgets(sendline, MAXLINE, stdin) != NULL) {
		debug("send to server: %s", sendline);
		if (sendto(sockfd, sendline, strlen(sendline), 0, pservaddr, servlen) < 0)
			err_sys("sendto");

		n = recvfrom(sockfd, recvline, MAXLINE, 0, NULL, NULL);
		recvline[n] = 0;

		debug("read from server: %s", recvline);

		fputs(recvline, stdout);
	}
}

int main(int argc, char *argv[])
{
	int sockfd;
	struct sockaddr_in servaddr;

	if (argc != 2) {
		printf("usage: tcpcli <IPaddress>\n");
		exit(1);
	}

	sockfd = socket(AF_INET, SOCK_DGRAM, 0);
	if (sockfd == -1)
		err_sys("socket");
	debug("sockfd = %d\n", sockfd);

	bzero(&servaddr, sizeof(servaddr));
	servaddr.sin_family = AF_INET;
	servaddr.sin_port = htons(SERV_PORT);
	inet_pton(AF_INET, argv[1], &servaddr.sin_addr);

	str_cli(sockfd, (SA *)&servaddr, sizeof(servaddr));

	exit(0);
}
