#ifndef __UNP_H__
#define __UNP_H__

#include <string.h>
#include <strings.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>

#define LISTENQ		1024	/* 2nd argument to listen() */
#define SERV_PORT	9877	/* TCP and UDP */
#define MAXLINE		4096	/* max text line length */

#define SA		struct sockaddr

#define __DEBUG__ 0

#if __DEBUG__
#define debug printf
#else
#define debug
#endif

void err_sys(char *str)
{
	perror(str);
	exit(1);
}

ssize_t readline(int fd, void *vptr, size_t maxlen)
{
	ssize_t n, rc;
	char c, *ptr;

	ptr = vptr;
	for (n = 1; n < maxlen; n++) {
	again:
		if ((rc = read(fd, &c, 1)) == 1) {
			*ptr++ = c;
			if (c == '\n')
				break;
		} else if (rc == 0) {
			*ptr = 0;
			return n - 1;
		} else {
			if (errno == EINTR)
				goto again;
			return -1;
		}
	}
	*ptr = 0;
	return n;
}

#endif /* __UNP_H__ */
