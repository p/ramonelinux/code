#include "unp.h"

void str_echo(int sockfd, SA *pcliaddr, socklen_t clilen)
{
	int n;
	socklen_t len;
	char mesg[MAXLINE];

	for (;;) {
		len = clilen;
		n = recvfrom(sockfd, mesg, MAXLINE, 0, pcliaddr, &len);
		debug("read from client, and write it to client: [%s]\n", mesg);
		sendto(sockfd, mesg, n, 0, pcliaddr, len);
	}
}

int main(void)
{
	int sockfd;
	struct sockaddr_in cliaddr, servaddr;

	sockfd = socket(AF_INET, SOCK_DGRAM, 0);
	if (sockfd == -1)
		err_sys("socket");

	bzero(&servaddr, sizeof(servaddr));
	servaddr.sin_family = AF_INET;
	servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
	servaddr.sin_port = htons(SERV_PORT);

	if (bind(sockfd, (SA *)&servaddr, sizeof(servaddr)) == -1)
		err_sys("bind");

	str_echo(sockfd, (SA *)&cliaddr, sizeof(cliaddr));
}
