#include "unp.h"
#include <sys/time.h>
#include <signal.h>
#include <netinet/ip_icmp.h>

int datalen = 56;
int nsent;
int sockfd;
char str[128];
struct addrinfo *ai;

uint16_t in_cksum(uint16_t *addr, int len)
{
	int nleft = len;
	uint32_t sum = 0;
	uint16_t *w = addr;
	uint16_t answer = 0;

	/*
	 * Our algorithm is simple, using a 32 bit accumulator (sum), we add
	 * sequential 16 bit words to it, and at the end, fold back all the
	 * carry bits from the top 16 bits into the lower 16 bits.
	 */
	while (nleft > 1) {
		sum += *w++;
		nleft -= 2;
	}

	/* 4mop up an odd byte, if necessary */
	if (nleft == 1) {
		*(unsigned char *)(&answer) = *(unsigned char *)w ;
		sum += answer;
	}

	/* 4add back carry outs from top 16 bits to low 16 bits */
	sum = (sum >> 16) + (sum & 0xffff);     /* add hi 16 to low 16 */
	sum += (sum >> 16);                     /* add carry */
	answer = ~sum;                          /* truncate to 16 bits */
	return(answer);
}

void send_v4(int fd)
{
	int len;
	struct icmp *icmp;
	char sendbuf[BUFSIZ];
	pid_t pid;
	ssize_t res;

	pid = getpid() & 0xffff;
	icmp = (struct icmp *)sendbuf;
	icmp->icmp_type = ICMP_ECHO;
	icmp->icmp_code = 0;
	icmp->icmp_id = pid;
	icmp->icmp_seq = nsent++;
	memset(icmp->icmp_data, 0xa5, datalen);
	gettimeofday((struct timeval *)icmp->icmp_data, NULL);

	len = 8 + datalen;
	icmp->icmp_cksum = 0;
	icmp->icmp_cksum = in_cksum((u_short *)icmp, len);

	res = sendto(fd, sendbuf, len, 0, ai->ai_addr, ai->ai_addrlen);
	if (res < 0)
		perror("sendto");
}

void sig_alrm(int signo)
{
	send_v4(sockfd);
	alarm(1);
	return;
}

void tv_sub(struct timeval *out, struct timeval *in)
{
	if ((out->tv_usec -= in->tv_usec) < 0) {
		--out->tv_sec;
		out->tv_usec += 1000000;
	}
	out->tv_sec -= in->tv_sec;
}

void proc_v4(char *ptr, ssize_t len, struct msghdr *msg, struct timeval *tvrecv)
{
	int hlen, icmplen;
	double rtt;
	struct ip *ip;
	struct icmp *icmp;
	struct timeval *tvsend;
	pid_t pid = getpid() & 0xffff;

	ip = (struct ip *)ptr;
	hlen = ip->ip_hl << 2;
	if (ip->ip_p != IPPROTO_ICMP)
		return;

	icmp = (struct icmp *)(ptr + hlen);
	if ((icmplen = len - hlen) < 8)
		return;

	if (icmp->icmp_type == ICMP_ECHOREPLY) {
		if (icmp->icmp_id != pid)
			return;
		if (icmplen < 16)
			return;

		tvsend = (struct timeval *)icmp->icmp_data;
		tv_sub(tvrecv, tvsend);
		rtt = tvrecv->tv_sec * 1000.0 + tvrecv->tv_usec / 1000.0;
		printf("%d bytes from %s: seq=%u, ttl=%d, rtt=%.3f ms\n",
			icmplen, str, icmp->icmp_seq, ip->ip_ttl, rtt);
	}
}

void readloop(int fd)
{
	char recvbuf[BUFSIZ];
	struct msghdr msg;
	struct iovec iov;
	int size;
	char controlbuf[BUFSIZ];
	struct timeval tval;
	ssize_t n;

	size = 60 * 1024;
	setsockopt(fd, SOL_SOCKET, SO_RCVBUF, &size, sizeof(size));

	sig_alrm(SIGALRM);

	iov.iov_base = recvbuf;
	iov.iov_len = sizeof(recvbuf);
	msg.msg_name = calloc(1, ai->ai_addrlen);
	msg.msg_iov = &iov;
	msg.msg_iovlen = 1;
	msg.msg_control = controlbuf;

	for (;;) {
		msg.msg_namelen = ai->ai_addrlen;
		msg.msg_controllen = sizeof(controlbuf);
		n = recvmsg(fd, &msg, 0);
		if (n < 0) {
			if (errno == EINTR)
				continue;
			else {
				perror("recvmsg");
				return;
			}
		}

		gettimeofday(&tval, NULL);
		proc_v4(recvbuf, n, &msg, &tval);
	}
}

int main(int argc, char *argv[])
{
	struct sockaddr_in *sin;
	struct addrinfo hints, *res;

	if (argc != 2)
		return -1;

	signal(SIGALRM, sig_alrm);

	bzero(&hints, sizeof(struct addrinfo));
	hints.ai_flags = AI_CANONNAME;
	hints.ai_family = AF_INET;
	hints.ai_socktype = 0;
	getaddrinfo(argv[1], NULL, &hints, &res);
	sin = (struct sockaddr_in *)res->ai_addr;
	inet_ntop(AF_INET, &sin->sin_addr, str, sizeof(str));

	printf("PING %s (%s): %d data bytes\n",
		res->ai_canonname ? : str, str, datalen);

	ai = res;

	sockfd = socket(res->ai_family, SOCK_RAW, IPPROTO_ICMP);

	readloop(sockfd);
}
