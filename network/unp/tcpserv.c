#include "unp.h"

void str_echo(int sockfd)
{
	ssize_t n;
	char buf[MAXLINE];

again:
	while ((n = read(sockfd, buf, MAXLINE)) > 0) {
		debug("read from client, and write it to client: [%s]\n", buf);
		write(sockfd, buf, n);
	}

	if (n < 0 && errno == EINTR)
		goto again;
	else if (n < 0)
		err_sys("str_echo: read error");
}

int main(void)
{
	int listenfd, connfd;
	pid_t childpid;
	socklen_t clilen;
	struct sockaddr_in cliaddr, servaddr;
	int reuse = 1;

	listenfd = socket(AF_INET, SOCK_STREAM, 0);
	if (listenfd == -1)
		err_sys("socket");

	debug("listenfd = %d\n", listenfd);

	bzero(&servaddr, sizeof(servaddr));
	servaddr.sin_family = AF_INET;
	servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
	servaddr.sin_port = htons(SERV_PORT);

	if (setsockopt(listenfd, SOL_SOCKET, SO_REUSEADDR, &reuse, sizeof(reuse)) < 0)
		err_sys("setsockopet");

	if (bind(listenfd, (SA *)&servaddr, sizeof(servaddr)) == -1)
		err_sys("bind");

	if (listen(listenfd, LISTENQ) == -1)
		err_sys("listen");

	for (;;) {
		clilen = sizeof(cliaddr);
		connfd = accept(listenfd, (SA *)&cliaddr, &clilen);
		if (connfd == -1)
			err_sys("accept");
		debug("connfd = %d\n", connfd);

		debug("serv address: %s\n", inet_ntoa(servaddr.sin_addr));
		debug("client address: %s\n", inet_ntoa(cliaddr.sin_addr));

		childpid = fork();
		if (childpid == 0) {
			close(listenfd);	/* close listening socket */
			str_echo(connfd);	/* process the request */
			exit(0);
		}

		close(connfd);			/* parent closes connected socket */
	}
}
