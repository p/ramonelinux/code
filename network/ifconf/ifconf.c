#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <net/if.h>
#include <netinet/in.h>
#include <arpa/inet.h>

int main(int argc, char *argv[])
{
	int sockfd;
	struct ifreq ifr;
	struct sockaddr_in *addr;
	unsigned char arp[6];

	if(argc != 2) {
		printf("usage: %s eth0.\n", argv[0]);
		exit(EXIT_FAILURE);
	}

	if((sockfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0) {
		perror("socket error");
		exit(EXIT_FAILURE);
	}

	strncpy(ifr.ifr_name, argv[1], IFNAMSIZ - 1);

	if(ioctl(sockfd, SIOCGIFADDR, &ifr) < 0) {
		perror("ioctl SIOCGIFADDR error");
		exit(EXIT_FAILURE);
	} else {
		addr = (struct sockaddr_in *)&ifr.ifr_addr;
		printf("addr:\t%s\n", inet_ntoa(addr->sin_addr));
	}

	if(ioctl(sockfd, SIOCGIFBRDADDR, &ifr) < 0) {
		perror("ioctl SIOCGIFBRDADDR error");
		exit(EXIT_FAILURE);
	} else {
		addr = (struct sockaddr_in *)&ifr.ifr_broadaddr;
		printf("Bcast:\t%s\n", inet_ntoa(addr->sin_addr));
	}

	if(ioctl(sockfd, SIOCGIFNETMASK, &ifr) < 0) {
		perror("ioctl SIOCGIFNETMASK error");
		exit(EXIT_FAILURE);
	} else {
		addr = (struct sockaddr_in *)&ifr.ifr_addr;
		printf("Mask:\t%s\n", inet_ntoa(addr->sin_addr));
	}

	if(ioctl(sockfd, SIOCGIFHWADDR, &ifr) < 0) {
		perror("ioctl SIOCGIFHWADDR error");
		exit(EXIT_FAILURE);
	} else {
		memcpy(arp, ifr.ifr_hwaddr.sa_data, 6);
		printf("HWaddr:\t%02x:%02x:%02x:%02x:%02x:%02x\n",
				arp[0], arp[1], arp[2], arp[3], arp[4], arp[5]);
	}

	return 0;
}
