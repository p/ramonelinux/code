#include <linux/module.h>
#include <net/tcp.h>

static struct proto Caesar_cipher_base_prot;
static struct proto ulp_Caesar_cipher_prot;

#define TCP_NONE        100

struct Caesar_cipher_context {
	// 简陋的密钥
	int key;
	// 保留的原始的proto结构的setsockopt函数
	int  (*setsockopt)(struct sock *sk, int level, int optname, char __user *optval, unsigned int optlen);
	// 保留的原始的proto结构的getsockopt函数
	int  (*getsockopt)(struct sock *sk, int level, int optname, char __user *optval, int __user *optlen);
	// 保留的原始的proto结构的sendmsg函数
	int  (*sendmsg)(struct sock *sk, struct msghdr *msg, size_t len);
	// 保留的原始的proto结构的recvmsg函数
	int  (*recvmsg)(struct sock *sk, struct msghdr *msg, size_t len, int noblock, int flags, int *addr_len);
	// 保留的原始的proto结构的close函数
	void (*close)(struct sock *sk, long timeout);
};

static inline struct Caesar_cipher_context *Caesar_cipher_get_ctx(const struct sock *sk)
{
	struct inet_connection_sock *icsk = inet_csk(sk);
	return icsk->icsk_ulp_data;
}

// 感觉实现这个没有什么意义，暂不实现
static int Caesar_cipher_getsockopt(struct sock *sk, int level, int optname,
		char __user *optval, int __user *optlen)
{
	struct Caesar_cipher_context *ctx = Caesar_cipher_get_ctx(sk);
	return ctx->getsockopt(sk, level, optname, optval, optlen);
}

static int Caesar_cipher_setsockopt(struct sock *sk, int level, int optname,
		char __user *optval, unsigned int optlen)
{
	struct Caesar_cipher_context *ctx = Caesar_cipher_get_ctx(sk);
	struct proto *prot = NULL;
	int val;


	if (level != TCP_NONE) {
		pr_info("%s level=%d\n", __func__, level);
		return ctx->setsockopt(sk, level, optname, optval, optlen);
	}

	if (get_user(val, (int __user *)optval)) {
		ctx->key = 0;
	} else {
		ctx->key = val;
	}
	pr_info("%s val=%d\n", __func__, val);

	// 替换socket的proto并保留原始的close回调
	prot = &ulp_Caesar_cipher_prot;
	ctx->close = sk->sk_prot->close;
	sk->sk_prot = prot;

	return 0;
}

int Caesar_cipher_sendmsg(struct sock *sk, struct msghdr *msg, size_t size)
{
	struct Caesar_cipher_context *ctx = Caesar_cipher_get_ctx(sk);
	char *userbuf;
	struct msghdr newmsg;
	struct iovec iov[1];
	int err;

	mm_segment_t oldfs = get_fs();

	// 分配一个新的buffer，这是为了用户空间和内核空间冲突
	userbuf = vmalloc(size);
	if (!userbuf)  {
		return -ENOMEM;
	}

	iov[0].iov_base = userbuf;
	iov[0].iov_len = size;
	err = memcpy_from_msg(userbuf, msg, size);
	if (err) {
		vfree(userbuf);
		return -EINVAL;
	}

	// inline encrypt  凯撒加密操作
	{ 
		int i = 0;
		for (i = 0; i < size; i++) {
			userbuf[i] = userbuf[i] + ctx->key;
		}
	}

	iov_iter_init(&newmsg.msg_iter, WRITE, iov, 1, size);
	newmsg.msg_name = NULL;
	newmsg.msg_namelen = 0;
	newmsg.msg_control = NULL;
	newmsg.msg_controllen = 0;
	newmsg.msg_flags = msg->msg_flags;

	// 申明现在是在内核态发送数据
	set_fs(KERNEL_DS);
	// 调用原始的sendmsg发送新分配的buffer
	err = ctx->sendmsg(sk, &newmsg, size);
	set_fs(oldfs);

	vfree(userbuf);

	return err;
}

// 本回调函数的实现和sendmsg思路无异
int Caesar_cipher_recvmsg(struct sock *sk, struct msghdr *msg, size_t len, int nonblock,
		int flags, int *addr_len)
{
	struct Caesar_cipher_context *ctx = Caesar_cipher_get_ctx(sk);
	int ret, err;

	char *userbuf;
	struct msghdr newmsg;
	struct iovec iov[1];

	mm_segment_t oldfs = get_fs();

	userbuf = vmalloc(len);
	if (!userbuf)  {
		return -ENOMEM;
	}

	iov[0].iov_base = userbuf;
	iov[0].iov_len = len;

	iov_iter_init(&newmsg.msg_iter, READ, iov, 1, len);
	newmsg.msg_name = NULL;
	newmsg.msg_namelen = 0;
	newmsg.msg_control = NULL;
	newmsg.msg_controllen = 0;
	newmsg.msg_flags = msg->msg_flags;

	set_fs(KERNEL_DS);
	ret = ctx->recvmsg(sk, &newmsg, len, nonblock, flags, addr_len);
	set_fs(oldfs);

	// inline decrypt 凯撒解密操作
	{
		int i = 0;
		for (i = 0; i < ret; i++) {
			userbuf[i] = userbuf[i] - ctx->key;
		}
	}

	err = memcpy_to_msg(msg, userbuf, ret);
	if (err) {
		ret = -EINVAL;
	}

	vfree(userbuf);
	return ret;
}

static int Caesar_cipher_init(struct sock *sk)
{
	struct inet_connection_sock *icsk = inet_csk(sk);
	struct Caesar_cipher_context *ctx;
	int rc = 0;

	ctx = kzalloc(sizeof(*ctx), GFP_KERNEL);
	if (!ctx) {
		rc = -ENOMEM;
		goto out;
	}

	icsk->icsk_ulp_data = ctx;

	// 保留set/getsockopt的核心操作
	ctx->setsockopt = sk->sk_prot->setsockopt;
	ctx->getsockopt = sk->sk_prot->getsockopt;
	// 保留send/recvmsg的核心操作
	ctx->sendmsg = sk->sk_prot->sendmsg;
	ctx->recvmsg = sk->sk_prot->recvmsg;

	// 整体替换socket的proto操作回调，但事实上就是复制了原始的proto操作回调，真正替换操作在setsockopt中进行
	sk->sk_prot = &Caesar_cipher_base_prot;
out:
	return rc;
}

static void Caesar_cipher_close(struct sock *sk, long timeout)
{
	struct Caesar_cipher_context *ctx = Caesar_cipher_get_ctx(sk);
	kfree (ctx);
	ctx->close(sk, timeout);
}

static struct tcp_ulp_ops tcp_Caesar_cipher_ulp_ops __read_mostly = {
	.name   = "Caesar cipher",
	.owner  = THIS_MODULE,
	.init   = Caesar_cipher_init,
};

static int __init Caesar_cipher_register(void)
{
	Caesar_cipher_base_prot                 = tcp_prot;
	Caesar_cipher_base_prot.setsockopt      = Caesar_cipher_setsockopt;
	Caesar_cipher_base_prot.getsockopt      = Caesar_cipher_getsockopt;

	// 替换个别的回调函数
	ulp_Caesar_cipher_prot                  = Caesar_cipher_base_prot;
	ulp_Caesar_cipher_prot.sendmsg          = Caesar_cipher_sendmsg;
	ulp_Caesar_cipher_prot.recvmsg          = Caesar_cipher_recvmsg;
	ulp_Caesar_cipher_prot.close            = Caesar_cipher_close;

	// 这个注册不再解释，无非就是把一个结构体链入一个链表，等到setsockopt执行ULP绑定时再查找而已
	tcp_register_ulp(&tcp_Caesar_cipher_ulp_ops);

	return 0;
}

static void __exit Caesar_cipher_unregister(void)
{
	tcp_unregister_ulp(&tcp_Caesar_cipher_ulp_ops);
}

module_init(Caesar_cipher_register);
module_exit(Caesar_cipher_unregister);

MODULE_AUTHOR("Zhao Ya <marywangran@126.com>");
MODULE_DESCRIPTION("Linux 4.14 ULP Demo");
MODULE_LICENSE("GPL");
