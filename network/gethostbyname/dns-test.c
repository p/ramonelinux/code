#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>

typedef struct _DNS_HDR {
	unsigned short id;
	unsigned short tag;
	unsigned short numq;
	unsigned short numa;
	unsigned short numa1;
	unsigned short numa2;
} DNS_HDR;

typedef struct _DNS_QER {
	unsigned short type;
	unsigned short classes;
} DNS_QER;

int main(int argc, char* argv[])
{
	int clifd, len = 0, i;
	socklen_t socklen = 0;
	char buf[BUFSIZ] = { 0 };
	char *p = NULL;
	char *ipaddr = "127.0.0.53"; // cat /etc/resolv.conf
	struct sockaddr_in servaddr;
	char *str;

	if (argc != 2)
		return -1;
	else
		str = argv[1];

	clifd = socket(AF_INET, SOCK_DGRAM, 0);
	if (clifd < 0) {
		printf( "create socket error!\n " );
		return -1;
	}
	bzero(&servaddr, sizeof(servaddr));
	servaddr.sin_family = AF_INET;
	servaddr.sin_port = htons(53); // DNS_PORT 53
	inet_pton(AF_INET, ipaddr, &servaddr.sin_addr);

	DNS_HDR *dnshdr = (DNS_HDR *)buf;
	DNS_QER *dnsqer = (DNS_QER *)(buf + sizeof(DNS_HDR));
	memset(buf, 0, BUFSIZ);
	dnshdr->id = (unsigned short)1;
	dnshdr->tag = htons(0x0100);
	dnshdr->numq = htons(1);
	dnshdr->numa = 0;

	strncpy(buf + sizeof(DNS_HDR) + 1, str, strlen(str));

	p = buf + sizeof(DNS_HDR) + 1;
	i = 0;
	while (p < (buf + sizeof(DNS_HDR) + 1 + strlen(str))) {
		if (*p == '.') {
			*(p - i - 1) = i;
			i = 0;
		} else {
			i++;
		}
		p++;
	}
	*(p - i - 1) = i;

	dnsqer = (DNS_QER *)(buf + sizeof(DNS_HDR) + 2 + strlen(str));
	dnsqer->classes = htons(1);
	dnsqer->type = htons(1);

	socklen = sizeof(struct sockaddr_in);

	len = sendto(clifd, buf, sizeof(DNS_HDR) + sizeof(DNS_QER) + strlen(str) + 2, 0, (struct sockaddr *)&servaddr, socklen);
	if (len < 0) {
		printf("send error\n");
		return -1;
	}

	len = recvfrom(clifd, buf, BUFSIZ, 0, (struct sockaddr *)&servaddr, &socklen);
	if (len < 0) {
		if (errno == EAGAIN)
			printf("recvfrom timeout!\n");
	}

	printf("answer: %d\n", dnshdr->numa);
	if (dnshdr->numa == 0) {
		printf("ack error\n");
		return -1;
	}

	p = buf + len - 4;
	printf("%s ==> %u.%u.%u.%u\n", str, (unsigned char)*p, (unsigned char)*(p + 1), (unsigned char)*(p + 2), (unsigned char)*(p + 3));

	close(clifd);
	return 0;
}
