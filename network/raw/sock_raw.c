#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <linux/in.h>

int main()
{
	int fd = socket(AF_INET, SOCK_RAW, IPPROTO_IP);//Protocol not supported
	if (fd < 0)
		perror("socket");
	else
		printf("fd = %d\n", fd);
}
