#include "unpicmpd.h"

void dg_cli(FILE *fp, int sockfd, const SA *pservaddr, socklen_t servlen)
{
	int icmpfd, maxfdp1;
	char sendline[MAXLINE], recvline[MAXLINE + 1];
	fd_set rset;
	ssize_t n;
	struct timeval tv;
	struct icmpd_err icmpd_err;
	struct sockaddr_un sun;

	sock_bind_wild(sockfd, pservaddr->sa_family);

	icmpfd = socket(AF_LOCAL, SOCK_STREAM, 0);
	sun.sun_family = AF_LOCAL;
	strcpy(sun.sun_path, ICMPD_PATH);
	connect(icmpfd, (SA *)&sun, sizeof(sun));
	write_fd(icmpfd, "1", 1, sockfd);
	n = read(icmpfd, recvline, 1);
	if (n != 1 || recvline[0] != '1')
		err_quit("error creating icmp socket, n = %ld, char = %c", n, recvline[0]);

	FD_ZERO(&rset);
	maxfdp1 = max(sockfd, icmpfd) + 1;

	while (fgets(sendline, MAXLINE, fp) != NULL) {
		sendto(sockfd, sendline, strlen(sendline), 0, pservaddr, servlen);

		tv.tv_sec = 5;
		tv.tv_usec = 0;
		FD_SET(sockfd, &rset);
		FD_SET(icmpfd, &rset);
		if ((n = select(maxfdp1, &rset, NULL, NULL, &tv)) == 0) {
			fprintf(stderr, "socket timeout\n");
			continue;
		}

		if (FD_ISSET(sockfd, &rset)) {
			n = recvfrom(sockfd, recvline, MAXLINE, 0, NULL, NULL);
			recvline[n] = 0;	/* null terminate */
			fputs(recvline, stdout);
		}

		if (FD_ISSET(icmpfd, &rset)) {
			if ((n = read(icmpfd, &icmpd_err, sizeof(icmpd_err))) == 0)
				err_quit("ICMP daemon terminated");
			else if (n != sizeof(icmpd_err))
				err_quit("n = %ld, expected %ld", n, sizeof(icmpd_err));
			printf("ICMP error: dest = %s, %s, type = %d, code = %d\n",
			       sock_ntop((SA *)&icmpd_err.icmpd_dest, icmpd_err.icmpd_len),
			       strerror(icmpd_err.icmpd_errno),
			       icmpd_err.icmpd_type, icmpd_err.icmpd_code);
		}
	}
}

int udp_client(const char *host, const char *serv, SA **saptr, socklen_t *lenp)
{
	int sockfd, n;
	struct addrinfo	hints, *res, *ressave;

	bzero(&hints, sizeof(struct addrinfo));
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_DGRAM;

	if ((n = getaddrinfo(host, serv, &hints, &res)) != 0)
		err_quit("udp_client error for %s, %s: %s\n", host, serv, gai_strerror(n));

	ressave = res;

	do {
		sockfd = socket(res->ai_family, res->ai_socktype, res->ai_protocol);
		if (sockfd >= 0)
			break;		/* success */
	} while ((res = res->ai_next) != NULL);

	if (res == NULL)	/* errno set from final socket() */
		err_quit("udp_client error for %s, %s", host, serv);

	*saptr = malloc(res->ai_addrlen);
	memcpy(*saptr, res->ai_addr, res->ai_addrlen);
	*lenp = res->ai_addrlen;

	freeaddrinfo(ressave);

	return(sockfd);
}

int main(int argc, char **argv)
{
	int		sockfd;
	socklen_t	salen;
	struct sockaddr	*sa;

	if (argc != 3)
		err_quit("usage: udpcli01 <hostname> <service>\n");

	sockfd = udp_client(argv[1], argv[2], &sa, &salen);

	dg_cli(stdin, sockfd, sa, salen);

	exit(0);
}
