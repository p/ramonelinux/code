#include <netinet/in_systm.h>
#include <netinet/ip.h>
#include <netinet/ip_icmp.h>
#include <netinet/udp.h>
#include "unpicmpd.h"

struct client {
  int	connfd;	/* Unix domain stream socket to client */
  int	family;	/* AF_INET or AF_INET6 */
  int	lport;	/* local port bound to client's UDP socket */
		/* network byte ordered */
} client[FD_SETSIZE];

int fd4, listenfd, maxi, maxfd, nready;
fd_set allset;

int readable_listen(void)
{
	int i, connfd;
	struct sockaddr_un cliaddr;
	socklen_t clilen;

	clilen = sizeof(cliaddr);
	connfd = accept(listenfd, (SA *)&cliaddr, &clilen);

	/* 4find first available client[] structure */
	for (i = 0; i < FD_SETSIZE; i++) {
		if (client[i].connfd < 0) {
			client[i].connfd = connfd;	/* save descriptor */
			break;
		}
	}

	if (i == FD_SETSIZE) {
		close(connfd);		/* can't handle new client, */
		return(--nready);	/* rudely close the new connection */
	}
	printf("new connection, i = %d, connfd = %d\n", i, connfd);

	FD_SET(connfd, &allset);	/* add new descriptor to set */
	if (connfd > maxfd)
		maxfd = connfd;			/* for select() */
	if (i > maxi)
		maxi = i;				/* max index in client[] array */

	return(--nready);
}

int readable_v4(void)
{
	int			i, hlen1, hlen2, icmplen, sport;
	char			buf[MAXLINE];
	char			srcstr[INET_ADDRSTRLEN], dststr[INET_ADDRSTRLEN];
	ssize_t			n;
	socklen_t		len;
	struct ip		*ip, *hip;
	struct icmp		*icmp;
	struct udphdr		*udp;
	struct sockaddr_in	from, dest;
	struct icmpd_err	icmpd_err;

	len = sizeof(from);
	n = recvfrom(fd4, buf, MAXLINE, 0, (SA *) &from, &len);

	printf("%ld bytes ICMPv4 from %s:", n, sock_ntop_host((SA *) &from, len));

	ip = (struct ip *) buf;		/* start of IP header */
	hlen1 = ip->ip_hl << 2;		/* length of IP header */

	icmp = (struct icmp *) (buf + hlen1);	/* start of ICMP header */
	if ((icmplen = n - hlen1) < 8)
		err_quit("icmplen (%d) < 8", icmplen);

	printf(" type = %d, code = %d\n", icmp->icmp_type, icmp->icmp_code);

	if (icmp->icmp_type == ICMP_UNREACH ||
		icmp->icmp_type == ICMP_TIMXCEED ||
		icmp->icmp_type == ICMP_SOURCEQUENCH) {
		if (icmplen < 8 + 20 + 8)
			err_quit("icmplen (%d) < 8 + 20 + 8", icmplen);

		hip = (struct ip *) (buf + hlen1 + 8);
		hlen2 = hip->ip_hl << 2;
		printf("\tsrcip = %s, dstip = %s, proto = %d\n",
		       inet_ntop(AF_INET, &hip->ip_src, srcstr, sizeof(srcstr)),
		       inet_ntop(AF_INET, &hip->ip_dst, dststr, sizeof(dststr)),
		       hip->ip_p);
 		if (hip->ip_p == IPPROTO_UDP) {
			udp = (struct udphdr *) (buf + hlen1 + 8 + hlen2);
			sport = udp->uh_sport;

			/* 4find client's Unix domain socket, send headers */
			for (i = 0; i <= maxi; i++) {
				if (client[i].connfd >= 0 &&
					client[i].family == AF_INET &&
					client[i].lport == sport) {

					bzero(&dest, sizeof(dest));
					dest.sin_family = AF_INET;
					memcpy(&dest.sin_addr, &hip->ip_dst,
						   sizeof(struct in_addr));
					dest.sin_port = udp->uh_dport;

					icmpd_err.icmpd_type = icmp->icmp_type;
					icmpd_err.icmpd_code = icmp->icmp_code;
					icmpd_err.icmpd_len = sizeof(struct sockaddr_in);
					memcpy(&icmpd_err.icmpd_dest, &dest, sizeof(dest));

					/* 4convert type & code to reasonable errno value */
					icmpd_err.icmpd_errno = EHOSTUNREACH;	/* default */
					if (icmp->icmp_type == ICMP_UNREACH) {
						if (icmp->icmp_code == ICMP_UNREACH_PORT)
							icmpd_err.icmpd_errno = ECONNREFUSED;
						else if (icmp->icmp_code == ICMP_UNREACH_NEEDFRAG)
							icmpd_err.icmpd_errno = EMSGSIZE;
					}
					write(client[i].connfd, &icmpd_err, sizeof(icmpd_err));
				}
			}
		}
	}
	return(--nready);
}

int readable_conn(int i)
{
	int			unixfd, recvfd;
	char			c;
	ssize_t			n;
	socklen_t		len;
	struct sockaddr_storage	ss;

	unixfd = client[i].connfd;
	recvfd = -1;
	if ( (n = read_fd(unixfd, &c, 1, &recvfd)) == 0) {
		printf("client %d terminated, recvfd = %d", i, recvfd);
		goto clientdone;	/* client probably terminated */
	}

	/* 4data from client; should be descriptor */
	if (recvfd < 0) {
		err_sys("read_fd did not return descriptor");
		goto clienterr;
	}

	len = sizeof(ss);
	if (getsockname(recvfd, (SA *) &ss, &len) < 0) {
		err_msg("getsockname error");
		goto clienterr;
	}

	client[i].family = ss.ss_family;
	if ( (client[i].lport = sock_get_port((SA *)&ss, len)) == 0) {
		client[i].lport = sock_bind_wild(recvfd, client[i].family);
		if (client[i].lport <= 0) {
			err_msg("error binding ephemeral port");
			goto clienterr;
		}
	}
	write(unixfd, "1", 1);	/* tell client all OK */
	close(recvfd);			/* all done with client's UDP socket */
	return(--nready);

clienterr:
	write(unixfd, "0", 1);	/* tell client error occurred */
clientdone:
	close(unixfd);
	if (recvfd >= 0)
		close(recvfd);
	FD_CLR(unixfd, &allset);
	client[i].connfd = -1;
	return(--nready);
}

int main(int argc, char **argv)
{
	int i, sockfd;
	struct sockaddr_un sun;
	fd_set rset;

	if (argc != 1)
		err_sys("usage: icmpd");

	maxi = -1;					/* index into client[] array */
	for (i = 0; i < FD_SETSIZE; i++)
		client[i].connfd = -1;	/* -1 indicates available entry */
	FD_ZERO(&allset);

	fd4 = socket(AF_INET, SOCK_RAW, IPPROTO_ICMP);
	FD_SET(fd4, &allset);
	maxfd = fd4;

	listenfd = socket(AF_UNIX, SOCK_STREAM, 0);
	sun.sun_family = AF_LOCAL;
	strcpy(sun.sun_path, ICMPD_PATH);
	unlink(ICMPD_PATH);
	bind(listenfd, (SA *)&sun, sizeof(sun));
	listen(listenfd, LISTENQ);
	FD_SET(listenfd, &allset);
	maxfd = max(maxfd, listenfd);

	for ( ; ; ) {
		rset = allset;
		nready = select(maxfd + 1, &rset, NULL, NULL, NULL);

		if (FD_ISSET(listenfd, &rset))
			if (readable_listen() <= 0)
				continue;

		if (FD_ISSET(fd4, &rset))
			if (readable_v4() <= 0)
				continue;

		for (i = 0; i <= maxi; i++) {	/* check all clients for data */
			if ((sockfd = client[i].connfd) < 0)
				continue;
			if (FD_ISSET(sockfd, &rset))
				if (readable_conn(i) <= 0)
					break;				/* no more readable descriptors */
		}
	}
	exit(0);
}
