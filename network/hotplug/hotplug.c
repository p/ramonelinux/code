/* http://www.cnblogs.com/hoys/archive/2011/04/09/2010759.html */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <sys/un.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <linux/types.h>
#include <linux/netlink.h>
#include <errno.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <netinet/in.h>

static int init_hotplug_sock()
{
	const int buffersize = 1024;

	struct sockaddr_nl snl;
	bzero(&snl, sizeof(struct sockaddr_nl));
	snl.nl_family = AF_NETLINK;
	snl.nl_pid = getpid();
	snl.nl_groups = 1;

	int s = socket(PF_NETLINK, SOCK_DGRAM, NETLINK_KOBJECT_UEVENT);
	if(s == -1) {
		perror("socket error");
		exit(EXIT_FAILURE);
	}
	setsockopt(s, SOL_SOCKET, SO_RCVBUF, &buffersize, sizeof(buffersize));

	int ret = bind(s, (struct sockaddr *)&snl, sizeof(struct sockaddr_nl));
	if(ret < 0) {
		perror("bind error");
		close(s);
		exit(EXIT_FAILURE);
	}

	return s;
}

int main(int argc, char* argv[])
{
	int hotplug_sock = init_hotplug_sock();

	while(1) {
		/* Netlink message buffer */
		char buf[BUFSIZ] = {0};
		recv(hotplug_sock, &buf, sizeof(buf), 0);
		printf("%s\n", buf);

		/* TODO */
	}
	return 0;
}
